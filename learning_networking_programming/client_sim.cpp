#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part fprintf(stderr, "-----------------------------------------\n");
#define part2 fprintf(stderr, "====================================================\n");
#define part3 fprintf(stderr, "############################################################\n");
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define MAX_CLIENTS 4
#define THREAD_POOL_SIZE 2
#define SERVER_PORT 8000
#define PORT_ARG 8000

////////////////////////////////////
pthread_mutex_t print_mutex = PTHREAD_MUTEX_INITIALIZER;

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received < 0)
    {
        cerr << "Failed to read data from socket.\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s);
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA from socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

vector<string> fetch_tokens(string s)
{
    vector<string> ans;
    string now = "";
    for (auto &x : s)
    {
        if (x == ' ')
        {
            if (now != "")
            {
                ans.pb(now);
                now = "";
            }
        }
        else
        {
            now += x;
        }
    }

    if (now != "")
    {
        ans.pb(now);
        now = "";
    }

    return ans;
}
///////////////////////////////
struct cmd_class
{
    int k1, k2;
    int t;
    string v1, v2;
    string cmd_type;
};
vector<struct cmd_class> cmds_list;
/////////////////////////////

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int port_num = PORT_ARG;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////

    /* fill in server address in sockaddr_in datastructure */
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    // https://stackoverflow.com/a/20778887/6427607

    if (inet_pton(AF_INET, "127.0.0.1", &server_obj.sin_addr) <= 0)
    {
        printf(BRED "\nInvalid address/ Address not supported \n" ANSI_RESET);
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    // part;
    //  printf(BGRN "Connected to server\n" ANSI_RESET);
    //  part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *exec_cmd(void *idx_ptr)
{
    // https://stackoverflow.com/a/2439921/6427607 : client also making multiple connections
    int cmd_idx = *(int *)idx_ptr;
    // debug(cmd_idx);
    struct cmd_class curr_cmd = cmds_list[cmd_idx];
    sleep(curr_cmd.t);
    string to_send = "";
    to_send += curr_cmd.cmd_type + " ";
    // debug(curr_cmd.cmd_type);
    // debug(to_send);
    if (curr_cmd.cmd_type == "insert")
    {
        to_send += to_string(curr_cmd.k1) + " " + curr_cmd.v1;
    }
    else if (curr_cmd.cmd_type == "delete")
    {
        to_send += to_string(curr_cmd.k1);
    }
    else if (curr_cmd.cmd_type == "update")
    {
        to_send += to_string(curr_cmd.k1) + " " + curr_cmd.v1;
    }
    else if (curr_cmd.cmd_type == "concat")
    {
        to_send += to_string(curr_cmd.k1) + " " + to_string(curr_cmd.k2);
    }
    else
    {
        to_send += to_string(curr_cmd.k1);
    }

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&print_mutex);
    cout << cmd_idx << ":" << output_msg << endl;
    pthread_mutex_unlock(&print_mutex);

    // part;
}

///////////////////////////////

void take_inputs()
{
    for (auto &x : cmds_list)
    {
        cin >> x.t;
        cin >> x.cmd_type;
        // debug(x.cmd_type);
        if (x.cmd_type == "insert")
        {
            cin >> x.k1 >> x.v1;
        }
        else if (x.cmd_type == "delete")
        {
            cin >> x.k1;
        }
        else if (x.cmd_type == "update")
        {
            cin >> x.k1 >> x.v1;
        }
        else if (x.cmd_type == "concat")
        {
            cin >> x.k1 >> x.k2;
        }
        else
        {
            cin >> x.k1;
        }
    }
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    LL num_cmds;
    cin >> num_cmds;
    cmds_list.resize(num_cmds);
    take_inputs();
    // return 0;
    pthread_t thread_pool[num_cmds];

    for (i = 0; i < num_cmds; i++)
    {
        int *ptr_now = (int *)malloc(sizeof(int));
        *ptr_now = i;
        pthread_create(&thread_pool[i], NULL, exec_cmd, ptr_now);
    }

    for (int i = 0; i < num_cmds; i++)
    {
        pthread_join(thread_pool[i], NULL);
    }
    return 0;
}