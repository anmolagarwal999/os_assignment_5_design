#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part fprintf(stderr, "-----------------------------------------\n");
#define part2 fprintf(stderr, "====================================================\n");
#define part3 fprintf(stderr, "############################################################\n");
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define MAX_CLIENTS 8
#define THREAD_POOL_SIZE 20
#define PORT_ARG 8000
pthread_t thread_pool[THREAD_POOL_SIZE];
pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t client_pending_sem;
//   /  part;
const int initial_msg_len = 256;
#define max_key_val 101

queue<int> client_fds;
vector<string> dict(max_key_val, "");
vector<pthread_mutex_t> dict_mutexes(max_key_val, PTHREAD_MUTEX_INITIALIZER);
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received < 0)
    {
        cerr << "Failed to read data from socket.\n";
        // return "
        exit(-1);
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(s);
    // debug(bytes_sent);

    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA from socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

vector<string> fetch_tokens(string s)
{
    vector<string> ans;
    string now = "";
    for (auto &x : s)
    {
        if (x == ' ')
        {
            if (now != "")
            {
                ans.pb(now);
                now = "";
            }
        }
        else
        {
            now += x;
        }
    }

    if (now != "")
    {
        ans.pb(now);
        now = "";
    }

    return ans;
}
///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    // part2;
    // receive file_names_from_client

    // resetting string which holds file_name whose content needs to be sent
    //  memset(file_path, 0, initial_msg_len);
    // resetting string which server sends to client regarding the availability of asked file
    //  memset(msg_to_client, 0, initial_msg_len);
    //  printf("RAttempt read\n");

    // part 1 -> getting file which needs to be transferred
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    debug(cmd);
    ret_val = received_num;
    // debug(ret_val);
    // printf("Read something\n");
    if (ret_val < 0)
    {
        perror("Error read()");
        printf("Server could not read msg sent from client\n");
        exit(-1);
    }

    vector<string> in_tokens = fetch_tokens(cmd);
    int num_tokens = in_tokens.size();
    debug(num_tokens);
    // string msg_to_send_back = "";
    string msg_to_send_back = to_string(pthread_self()) + ":";
    if (in_tokens[0] == "insert")
    {
        int k1 = stoi(in_tokens[1]);
        string v1 = in_tokens[2];
        pthread_mutex_lock(&dict_mutexes[k1]);
        if (dict[k1] == "")
        {
            dict[k1] = v1;
            msg_to_send_back += "Insertion successful";
        }
        else
        {
            msg_to_send_back += "Key already exists";
        }
        pthread_mutex_unlock(&dict_mutexes[k1]);
    }
    else if (in_tokens[0] == "delete")
    {
        int k1 = stoi(in_tokens[1]);
        pthread_mutex_lock(&dict_mutexes[k1]);
        if (dict[k1] == "")
        {
            // dict[k1] = v1;
            msg_to_send_back += "No such key exists";
        }
        else
        {
            dict[k1] = "";
            msg_to_send_back += "Deletion successful";
        }
        pthread_mutex_unlock(&dict_mutexes[k1]);
    }
    else if (in_tokens[0] == "update")
    {
        int k1 = stoi(in_tokens[1]);
        string v1 = in_tokens[2];
        pthread_mutex_lock(&dict_mutexes[k1]);
        if (dict[k1] == "")
        {
            msg_to_send_back += "Updation failed as no such key exists";
        }
        else
        {
            dict[k1] = v1;
            msg_to_send_back += v1;
        }
        pthread_mutex_unlock(&dict_mutexes[k1]);
    }
    else if (in_tokens[0] == "concat")
    {
        int k1 = stoi(in_tokens[1]);
        int k2 = stoi(in_tokens[2]);
        debug(k1);
        assert(k1 != k2);
        pthread_mutex_lock(&dict_mutexes[min(k1, k2)]);
        pthread_mutex_lock(&dict_mutexes[max(k1, k2)]);
        if (dict[k1] == "" || dict[k2] == "")
        {
            msg_to_send_back += "Concat failed as one of the keys do not exist";
        }
        else
        {
            string backup = dict[k2];
            dict[k2] += dict[k1];
            dict[k1] += backup;
            msg_to_send_back += dict[k2];
            debug(dict[k2].length());
            debug(dict[k1].length());
        }
        pthread_mutex_unlock(&dict_mutexes[min(k1, k2)]);
        pthread_mutex_unlock(&dict_mutexes[max(k1, k2)]);
    }
    else
    {
        // fetch
        int k1 = stoi(in_tokens[1]);
        if (dict[k1] == "")
        {
            msg_to_send_back += "Key does not exist";
        }
        else
        {
            msg_to_send_back += dict[k1];
        }
    }

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"

    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
    sleep(2);
    // int sent_to_client = write(client_socket_fd, buffer_ptr, strlen(buffer_ptr));
    // debug(msg_to_send_back);
    // debug(msg_to_send_back.length());
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    // debug(sent_to_client);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client: ");
        // decide course of action
        // seems that the socket connection isn't well-established
        goto close_client_socket_ceremony;
        // exit(-1);
    }
    // close the file
    //  printf(BYEL "File descriptor closed\n" ANSI_RESET);
    //  close(fd1);
    // part2;

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *thread_function(void *args)
{
    while (true)
    {

        sem_wait(&client_pending_sem);
        pthread_mutex_lock(&queue_mutex);
        int curr_socket = client_fds.front();
        client_fds.pop();
        pthread_mutex_unlock(&queue_mutex);
        handle_connection(curr_socket);
    }
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    sem_init(&client_pending_sem, 0, 0);

    for (i = 0; i < THREAD_POOL_SIZE; i++)
    {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    ////////////////////////////////////////////////////
    // https://stackoverflow.com/q/15198834/6427607
    // https://stackoverflow.com/q/5106674/6427607
    // https://stackoverflow.com/q/4233598/6427607
    // http://www.softlab.ntua.gr/facilities/documentation/unix/unix-socket-faq/unix-socket-faq-2.html#time_wait

    int option = 1;

    if (setsockopt(wel_socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &option, sizeof(option)))
    {
        perror("setsockopt()");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    // CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        // part3;
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client.
        */
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&queue_mutex);
        client_fds.push(client_socket_fd);
        pthread_mutex_unlock(&queue_mutex);
        sem_post(&client_pending_sem);

        // pthread_t t;
        // int *ptr_now = (int *)malloc(sizeof(int));
        // *ptr_now = client_socket_fd;
        // pthread_create(&t, NULL, handle_connection, ptr_now);
        // handle_connection(ptr_now);

        //####################################################
    }

    close(wel_socket_fd);
    return 0;
}