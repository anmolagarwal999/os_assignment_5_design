#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const LL MOD = 1000000007;
#define pb push_back
#define all(c) (c).begin(), (c).end()
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "----------------------------------\n";
#include <iostream>

int dx[] = {1, 1, 0, -1, -1, -1, 0, 1}; // trick to explore an implicit 2D grid
int dy[] = {0, 1, 1, 1, 0, -1, -1, -1}; // S,SE,E,NE,N,NW,W,SW neighbors  //GO FOR EVEN FOR 4 moves

#define fastinput                     \
    ios_base::sync_with_stdio(false); \
    cin.tie(NULL);                    \
    cout.tie(NULL);

LL POW(LL x, LL y, LL mod_give)
{
    LL ans = 1;
    LL base = x;
    while (y)
    {
        if (y & 1)
        {
            ans *= base;
            ans %= mod_give;
        }
        base = base * base;
        base %= mod_give;
        y = y >> 1;
    }
    return ans;
}

int main()
{
    fastinput;
    LL n, i, j, k, t, temp, tc;

    ///////////////////////////////////
    // make all 100 keys
    // n = 100;
    // for (i = 1; i <= n; i++)
    // {
    //     cout << "1 insert " << i << " " << i << endl;
    // }
    // //////////////////////////////
    // // concatenate all pairs
    // for (i = 1; i <= n; i += 2)
    // {
    //     cout << "10 concat " << i << " " << i + 1 << endl;
    // }

    // //////////////////////////////
    // for (i = 1; i <= n; i += 2)
    // {
    //     cout << "15 concat " << i+1 << " " << i << endl;
    // }

    // for (i = 1; i <= n; i++)
    // {
    //     cout << "40 fetch " << i << endl;
    // }
    ///////////////////////////////////

    ///////////?TC 4 ####################
    // n = 50;
    // for (i = 1; i <= n; i += 2)
    // {
    //     cout << "1 insert " << i << " " << i << endl;
    // }

    // for (i = 1; i <= n; i += 2)
    // {
    //     cout << "5 update " << i << " " << i << "a" << endl;
    // }

    // for (i = 2; i <= n; i += 2)
    // {
    //     cout << "45 insert " << i << " " << i << endl;
    // }

    // for (i = 1; i <= n+4; i += 4)
    // {
    //     cout << "55 delete " << i+1 << endl;
    //     cout << "55 concat " << i  << " " << i + 2 << endl;
    //     // cout<<60
    //     // cout << "60 update " << i+1<<" "<<i + 1 << "b" << endl;
    // }
    // //################################################

    n = 100;
    for (i = 1; i <= n; i++)
    {
        cout << "1 insert " << i << " " << i%10 << endl;
    }

    for (i = 1; i <= n; i += 2)
    {
        int t = (i + 1) / 2+10;
        for (j = 1; j <= 5; j++)
        {
            cout << t << " concat " << i << " " << i + 1 << endl;
            cout << t << " concat " << i+1 << " " << i << endl;
        }
    }

    return 0;
}
