#!/usr/bin/env python
# coding: utf-8

# In[122]:


import os
import json
import sys


# In[ ]:





# ### Take relevant inputs

# In[123]:


tc_num=int(sys.argv[1])


# In[124]:


NUM_COMMANDS=-1


# In[125]:


FILE_NAME=""
with open(f"./tc_{tc_num}.txt",'r') as fd:
    input_lines=[x.strip() for x in fd.readlines()]
    NUM_COMMANDS=int(input_lines[0])
    input_lines=input_lines[1:]


# In[126]:


print("Num cmds in INPUT: ",NUM_COMMANDS)
#input_lines


# ### Make sure output file format is correct

# In[127]:


with open(f"./ans_{tc_num}.txt",'r') as fd:
    output_lines=[x.strip() for x in fd.readlines()]
    if len(output_lines)!=NUM_COMMANDS:
        print("[ERROR] num commands in output file: ",len(output_lines) )
        sys.exit(0)


# In[128]:


#output_lines


# ##### Parse the entire input commands

# In[129]:


def tokenize_line(x,separator=" "):
    tokens=x.split(separator)
    tokens[0]=int(tokens[0])
    return tokens


# In[130]:


output_tokens=[tokenize_line(x,":") for x in output_lines]


# In[131]:


#output_tokens


# In[132]:


input_tokens=[tokenize_line(x) for x in input_lines]


# In[133]:


#input_tokens


# In[134]:


all_cmds=set([x for x in range(NUM_COMMANDS)])
#all_cmds


# In[135]:


for curr_token in output_tokens:
    all_cmds.discard(curr_token[0])
if len(all_cmds)!=0:
    print("[ERROR] Cmd IDs not executed : ", all_cmds)
    sys.exit(0)


# In[ ]:





# #### Create a dictionary using those input commands

# In[136]:


ans_dict={}


# In[137]:


def handle_insert(*now_args):
    k1=int(now_args[0])
    new_val=now_args[1]
    if k1 in ans_dict:
        return False, "already"
    else:
        ans_dict[k1]=new_val
        return True, "success"

def handle_delete(*now_args):
    k1=int(now_args[0])
    if k1 not in ans_dict:
        return False, "no"
    else:
        del ans_dict[k1]
        return True, "success"

def handle_update(*now_args):
    k1=int(now_args[0])
    update_val=now_args[1]
    if k1 not in ans_dict:
        return False, "no"
    else:
        ans_dict[k1]=update_val
        return True, update_val

def handle_fetch(*now_args):
    k1=int(now_args[0])
    if k1 not in ans_dict:
        return False, "not"
    else:
        return True, ans_dict[k1]

def handle_concat(*now_args):
    k1=int(now_args[0])
    k2=int(now_args[1])
    if (k1 not in ans_dict) or (k2 not in ans_dict):
        return False, "fail"
    else:
        tmp1=ans_dict[k1]
        tmp2=ans_dict[k2]
        #print(f"args are {k1}:{tmp1} === {k2}:{tmp2}")

        ans_dict[k1]=tmp1+tmp2
        ans_dict[k2]=tmp2+tmp1
        return True, ans_dict[k2]


# In[138]:


func_dict={
    "insert":handle_insert, 
    "delete":handle_delete, 
    "update":handle_update, 
    "fetch":handle_fetch, 
    "concat":handle_concat
}


# In[139]:


if tc_num<5:
    for output_line_idx, out_line in enumerate(output_tokens):
        cmd_id_executed=out_line[0]
        cmd_name=input_tokens[cmd_id_executed][1]
        person_res=out_line[-1].lower()
        #print("person res is ", person_res)
        cmd_stat, cmd_res=func_dict[cmd_name](*input_tokens[cmd_id_executed][2:])
        #print(cmd_res)
        print(f"Executing command ID {cmd_id_executed} : {input_lines[cmd_id_executed]}")
        # we expect a successful output
        if cmd_stat:
            if cmd_name in ['update','fetch','concat']:
                if cmd_res!=person_res:
                    print("current state of DICT is ", ans_dict)

                    print(f"[ERROR]: \nExpected -> {cmd_res}, \nFound: {person_res}")
                    sys.exit(0)
                else:
                    print("Verified")
            else:
                if "success" in person_res:
                    print("Verified")
                else:
                    print(f"[ERROR]: We expected a successful operation BUT we received: {person_res}")
                    print("current state of DICT is ", ans_dict)
                    sys.exit(0)
        else:
            if cmd_res.lower() in person_res:
                print("Verified")
            else:
                print(f"Operation was expected to fail BUT instead got {person_res}")
                print("[ERROR]")
                print("current state of DICT is ", ans_dict)
                sys.exit(0)
        print("############################")


# In[140]:


if tc_num==5:
    for output_line_idx, out_line in enumerate(output_tokens):
        if output_line_idx>=100:
            break
        cmd_id_executed=out_line[0]
        cmd_name=input_tokens[cmd_id_executed][1]
        ans_dict[output_line_idx+1]=str((output_line_idx+1)%10)
        if cmd_name!="insert":
            print("[ERROR] WE EXPECTED INSERT OPERATION HERE, clearly something seems wrong")
            sys.exit(0)
    
    output_tokens=output_tokens[100:]
    ########################################
    for curr_token_arr in output_tokens:
        if len(curr_token_arr)!=3:
            print(f"[ERROR] Output line for command {curr_token_arr[0]} does not have 3 tokens ")
            sys.exit(0)
    #print("dict after just insert is ", ans_dict)
    output_tokens=sorted(output_tokens, key=lambda x:len(x[2]))
    #######################################
    #print("output tokens are ", output_tokens)
    for output_line_idx, out_line in enumerate(output_tokens):
        cmd_id_executed=out_line[0]
        cmd_name=input_tokens[cmd_id_executed][1]
        person_res=out_line[-1].lower()
        #print("person res is ", person_res)
        cmd_stat, cmd_res=func_dict[cmd_name](*input_tokens[cmd_id_executed][2:])
        #print(cmd_res)
        print(f"Executing command ID {cmd_id_executed} : {input_lines[cmd_id_executed]}")
        # we expect a successful output
        if cmd_stat:
            if cmd_name in ['update','fetch','concat']:
                if cmd_res!=person_res:
                    print("current state of DICT is ", ans_dict)

                    print(f"[ERROR]: \nExpected -> {cmd_res}, \nFound: {person_res}")
                    sys.exit(0)
                else:
                    print("Verified")
            else:
                if "success" in person_res:
                    print("Verified")
                else:
                    print(f"[ERROR]: We expected a successful operation BUT we received: {res}")
                    print("current state of DICT is ", ans_dict)
                    sys.exit(0)
        else:
            if cmd_res.lower() in person_res:
                print("Verified")
            else:
                print(f"Operation was expected to fail BUT instead got {person_res}")
                print("[ERROR]")
                print("current state of DICT is ", ans_dict)
                sys.exit(0)
        print("############################")
    
        


# In[141]:


thread_ids=set([x[1] for x in output_tokens])
print("Threads used is : ", len(thread_ids))
del thread_ids


# In[142]:


print("ALL OVER SUCCESFULLY")


# #### Manually check that requests are getting satisfied properly

# In[ ]:





# In[ ]:





# ### Parse output file and as output, print number of unique threads used and FIRST ERROR/SUCCESS

# In[ ]:




