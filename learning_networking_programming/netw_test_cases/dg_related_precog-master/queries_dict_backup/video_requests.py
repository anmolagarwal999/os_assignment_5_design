requests_dict={}
# --------------------------------------------------------

START_DATE="2008-01-01"
END_DATE="2020-12-31"
CHANNEL_ID='MINE'
TIME_GRANULARITY="day"

# --------------------------------------------------------
# This report provides statistics related to users' actions on a channel.

KEY_NAME="Basic_stats_non_daywise"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,comments,likes,dislikes,videosAddedToPlaylists,videosRemovedFromPlaylists,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,cardClickRate,cardTeaserClickRate,cardImpressions,cardTeaserImpressions,cardClicks,cardTeaserClicks,subscribersGained,subscribersLost",
        startDate=f"{START_DATE}",
        filters=f"video=={VIDEO_ID}"
    )

#######
# Top 10 – Third-party sites that generate the most views for an embedded video
# This query retrieves view counts and estimated watch time for a particular video or group of videos. Results are grouped by the third-party site where the video was embedded. Results are sorted in descending order by view count. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with a comma-separated list of one or more video IDs for your uploaded videos.

# estimatedMinutesWatched: The number of minutes that users watched videos
# insightPlaybackLocationDetail:  identifying the URLs or applications associated with the top embedded players.
KEY_NAME="third_party_view_stats_for_a_video"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightPlaybackLocationDetail",
        endDate=f"{END_DATE}",
        filters=f"video=={VIDEO_ID};insightPlaybackLocationType==EMBEDDED",
        ids=f"channel=={CHANNEL_ID}",
        maxResults=10,
        metrics="views,estimatedMinutesWatched",
        sort="-views",
        startDate=f"{START_DATE}"
    )

#####
# This report identifies the embedded video players that generated the most views or viewing time for a channel's videos.
# NOTE API will return at max only top 25 results.
# NOTE Global query,doesn't use video ID filter currently,but can use it.
KEY_NAME="Embedded_Video_playback_location"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightPlaybackLocationDetail",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        filters="insightPlaybackLocationType==EMBEDDED",
        maxResults=25,
        startDate=f"{START_DATE}",
        sort="-views"
    )

######
# This report provides statistics related to user actions on a channel for specific time intervals day by day.
KEY_NAME="Time_based_stats_daywise"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="day",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,comments,likes,dislikes,videosAddedToPlaylists,videosRemovedFromPlaylists,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained,subscribersLost",
        startDate=f"{START_DATE}",
        filters=f"video=={VIDEO_ID}"
    )

######
# Top 25 – YouTube search terms that generate the most traffic for a video
# This query retrieves the 10 search terms that generated the most views from YouTube search results for one or more specific videos. Results are sorted by view count in descending order. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with a comma-separated list of up to 500 video IDs.

# For example,you could run the query for a single video or replace VIDEO_ID to identify the search terms that generate the most traffic for that video. You could also list all of the videos in a particular playlist to determine which search terms generate the most traffic for any of the videos in that playlist.
# insightTrafficSourceType
KEY_NAME="top_10_YT_search_terms_which_led_to_video"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightTrafficSourceDetail",
        endDate=f"{END_DATE}",
        filters=f"video=={VIDEO_ID};insightTrafficSourceType==YT_SEARCH",
        ids=f"channel=={CHANNEL_ID}",
        maxResults=25,
        metrics="estimatedMinutesWatched,views",
        sort="-views",
        startDate=f"{START_DATE}"
    )

#######

KEY_NAME="Country_wise_stats-for_a_video"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="country",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,comments,likes,dislikes,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained,subscribersLost",
        startDate=f"{START_DATE}",
        filters=f"video=={VIDEO_ID}"
    )

#######
# Top 25 – External websites that generate the most traffic for a video
# This query retrieves the 25 external websites that generated the most views for a specific video or group of videos. Results are sorted by estimated watch time in descending order. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with a comma-separated list of up to 500 video IDs for your uploaded videos.
KEY_NAME="external_websites_that_generated_traffic_for_video"

requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightTrafficSourceDetail",
        endDate=f"{END_DATE}",
        filters=f"video=={VIDEO_ID};insightTrafficSourceType==EXT_URL",
        ids=f"channel=={CHANNEL_ID}",
        maxResults=25,
        metrics="estimatedMinutesWatched,views",
        sort="-views",
        startDate=f"{START_DATE}"
    )

#####
#TODO: TRY LATER AFTER INTERNAL ERROR IS RESOLVED
# # This report provides user activity metrics for subscribed and unsubscribed viewers.
# # Was the viewer subscribed to the channel that owns the video?

# KEY_NAME="Subscribed_vs_unsubscribed_viewers_data"
# requests_dict[KEY_NAME]=youtube_analytics.reports().query(
#         dimensions="subscribedStatus",
#         endDate=f"{END_DATE}",
#         ids=f"channel=={CHANNEL_ID}",
#         metrics="views,likes,dislikes,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,cardClickRate,cardTeaserClickRate,cardImpressions,cardTeaserImpressions,cardClicks,cardTeaserClicks",
#         startDate=f"{START_DATE}",
#         filters=f"video=={VIDEO_ID}"
#     )

##############
# Audience retention metrics for a video
# This report measures a video's ability to retain its audience. The report can only be retrieved for a single video at a time. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with the video ID for one of your uploaded videos. Note that audience retention reports do not support the ability to specify multiple values for the video filter.

# In this example,the report uses the audienceType filter to restrict the response so that it only contains data for organic views,which are the result of a direct user action,such as a search for a video or a click on a suggested video. As such,the report does not include data for views from TrueView in-stream or TrueView in-display ads. You can remove the filter to retrieve the video's audience retention data from all audience types.

# NOTE that data for the audienceType filter is available as of September 25,2013. The API will not return data for queries that use the filter to try to retrieve data from earlier dates. Queries that do not use the filter work for any date after July 1,2008.

# NOTE ORGAINC filter: These are views that are the direct result of user intention. For example,traffic is considered organic if a viewer takes an action like searching for a video,clicking on a suggested video,or browsing a channel.

# NOTE Paid Traffic: These are views that result from paid placement. The filters are AD_INSTREAM,and AD_INDISPLAY

# Skippable video ad: Views for ads that are auto-played before a video and that viewers can skip after five seconds.
# Display ads: Views that resulted from a viewer clicking a display ad,including ads shown in search results or on other video watch pages.

# TODO: ONLY ONE VIDEO ID CAN BE GIVEN
KEY_NAME="audience_retention_per_video"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="elapsedVideoTimeRatio",
        endDate=f"{END_DATE}",
        filters=f"video=={VIDEO_ID};audienceType==ORGANIC",
        ids=f"channel=={CHANNEL_ID}",
        metrics="audienceWatchRatio,relativeRetentionPerformance",
        startDate=f"{START_DATE}"
    )

#####
# This report provides statistics related to the type of page or application where video playbacks occurred.

KEY_NAME="Video_playback_location"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightPlaybackLocationType",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        startDate=f"{START_DATE}",
        filters=f"video=={VIDEO_ID}"
    )
    


#####
# This report aggregates viewing statistics based on the manner in which viewers reached your video content.
KEY_NAME="Viewers_reached_location"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightTrafficSourceType",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        filters=f"video=={VIDEO_ID}",
        startDate=f"{START_DATE}",
    )

#####
# This report aggregates viewing statistics based on the manner in which viewers reached your video content.
KEY_NAME="Device_type_stats"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="deviceType",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        filters=f"video=={VIDEO_ID}",
        startDate=f"{START_DATE}",
    )

#####
# This report aggregates viewing statistics based on viewers' age group and gender.
KEY_NAME="Age_group_gender_stats"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="ageGroup,gender",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="viewerPercentage",
        filters=f"video=={VIDEO_ID}",
        startDate=f"{START_DATE}",
    )

#####
# This report provides statistics showing how frequently the channel's videos were shared on different social platforms.
KEY_NAME="content_sharing_stats"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="sharingService",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="shares",
        filters=f"video=={VIDEO_ID}",
        startDate=f"{START_DATE}",
    )

#####
# This report provides statistics showing how frequently the channel's videos were shared on different social platforms.
KEY_NAME="Audience_retention_stats"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="elapsedVideoTimeRatio",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="audienceWatchRatio,relativeRetentionPerformance",
        filters=f"video=={VIDEO_ID}",
        startDate=f"{START_DATE}",
    )