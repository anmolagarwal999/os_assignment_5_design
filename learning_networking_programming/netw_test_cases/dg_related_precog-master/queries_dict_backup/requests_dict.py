requests_dict={}

START_DATE="2008-01-01"
END_DATE="2020-12-31"
CHANNEL_ID='MINE'

########################################################
# This query retrieves aggregated metrics for the channel's content.
# The report returns a single row of data that contains totals for each requested metric during the specified date range.
KEY_NAME="channel_aggregate_stats"
# TODO: change dates
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,comments,likes,dislikes,estimatedMinutesWatched,averageViewDuration",
        startDate=f"{START_DATE}"
    )
# ########################################################
# # This query retrieves metrics for a specific country for a channel's videos. The report returns a single row of data that contains totals for each requested metric during the specified date range.
# # TODO: new countries
# KEY_NAME="channel_country_INDIA"
# requests_dict[KEY_NAME]=youtube_analytics.reports().query(
#         endDate=f"{END_DATE}",
#         filters="country==IN",
#         ids=f"channel=={CHANNEL_ID}",
#         metrics="views,comments,likes,dislikes,estimatedMinutesWatched",
#         startDate=f"{START_DATE}"
#     )
########################################################
# This query retrieves a channel's 10 most watched videos, as measured by estimated minutes watched during the specified date range. Results are sorted by estimated minutes watched in descending order.
KEY_NAME="top_10_most_watched_for_channel"
# TODO: date change
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="video",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        maxResults=10,
        metrics="estimatedMinutesWatched,views,likes,subscribersGained",
        sort="-estimatedMinutesWatched",
        startDate=f"{START_DATE}"
    )
########################################################
# Top 10 – Annotation click-through rates for a channel's most viewed videos
# This request retrieves view counts, annotation click-through rates, annotation close rates, and annotation impressions for the channel's 10 most viewed videos. Results are sorted by view count in descending order, which means that the most viewed video will be listed first.
KEY_NAME="top_10_annonation_clickthrough_rates"

requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="video",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        maxResults=10,
        metrics="views,likes,annotationClickThroughRate,annotationCloseRate,annotationImpressions",
        sort="-views",
        startDate=f"{START_DATE}"
    )
########################################################
# Daily watch time metrics for a channel's videos
# This query retrieves daily view counts, watch time metrics, and new subscriber counts for a channel's videos. The report returns one row of data for each day in the requested date range. Rows are sorted in chronological order.
KEY_NAME="daywise_report_for_a_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="day",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained, subscribersLost",
        sort="day",
        startDate=f"{START_DATE}"
    )
########################################################
# Daily annotation metrics for a channel's videos
# This request retrieves daily view counts, annotation click-through rates, annotation close rates, and annotation impressions for the channel's content. Results are sorted in chronological order.
KEY_NAME="daywise_annonation_metrics_for_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="day",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,annotationClickThroughRate,annotationCloseRate,annotationImpressions",
        sort="day",
        startDate=f"{START_DATE}"
    )
########################################################
# Country-specific watch time metrics for a channel's videos
# This query retrieves country-specific view counts, watch time metrics, and subscription figures for a channel's videos. The report returns one row of data for each country where the channel's videos were watched. Rows are sorted in descending order of number of minutes watched.
KEY_NAME="countrywise_report_for_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="country",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained",
        sort="-estimatedMinutesWatched",
        startDate=f"{START_DATE}"
    )
########################################################
KEY_NAME="countrywise_annonation_stats_for_channel"
# Country-specific annotation metrics for a channel's videos
# This request retrieves country-specific view counts, annotation click-through rates, annotation close rates, and annotation impressions for the channel's videos. Results are sorted by annotation click-through rate in descending order, which means that the country with the highest annotation click-through rate will be listed first.
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="country",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,annotationClickThroughRate,annotationCloseRate,annotationImpressions",
        sort="-annotationClickThroughRate",
        startDate=f"{START_DATE}"
    )
########################################################
# Viewcounts and watch time from different playback locations
# This query retrieves the number of views and estimated watch time for a particular channel's videos. Results are aggregated based on the type of page or application where video playbacks occurred, and results are sorted in descending order by view count.
KEY_NAME="playback_location_for_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightPlaybackLocationType",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="estimatedMinutesWatched,views",
        sort="-views",
        startDate=f"{START_DATE}"
    )
########################################################
# Daily view counts and watch time from different playback locations
# This query retrieves daily view counts and estimated watch time in the United States for a particular channel's videos. Results are grouped by day for each type of page or application where video playbacks occurred. Results are sorted in chronological order.
KEY_NAME="views_etc_per_playback_for_channel"
# TODO: replace US with India
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="day,insightPlaybackLocationType",
        endDate=f"{END_DATE}",
        filters="country==IN",
        ids=f"channel=={CHANNEL_ID}",
        metrics="estimatedMinutesWatched,views",
        sort="day",
        startDate=f"{START_DATE}"
    )
########################################################
########################################################
# Viewcounts and watch time from different traffic sources in a country
# This request retrieves the number of views and estimated watch time for the channel's videos in a specified country. The metrics are aggregated by traffic source, which describes the manner in which users reached the video.
# TODO: replace US with India
KEY_NAME="traffic_sources_stats_for_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="insightTrafficSourceType",
        endDate=f"{END_DATE}",
        filters="country==IN",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        startDate=f"{START_DATE}"
    )
######################################################
########################################################
# Daily view counts and watch time from different traffic sources
# This request retrieves daily view counts and daily estimated watch time for the channel's videos. The metrics are aggregated on a daily basis by traffic source and sorted in chronological order.
KEY_NAME="views_per_traffic_source_for_channel"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="day,insightTrafficSourceType",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="views,estimatedMinutesWatched",
        sort="day",
        startDate=f"{START_DATE}"
    )
######################################################
########################################################
# Sharing metrics, aggregated by service where videos were shared
# This request retrieves the number of times that users used the Share button to share a channel's videos. Results are aggregated by sharing service (Google+, Twitter, etc.) and sorted in descending order of number of shares.
KEY_NAME="where_videos_of_this_channel_were_shared_google+_twitter"
requests_dict[KEY_NAME]=youtube_analytics.reports().query(
        dimensions="sharingService",
        endDate=f"{END_DATE}",
        ids=f"channel=={CHANNEL_ID}",
        metrics="shares",
        sort="-shares",
        startDate=f"{START_DATE}"
    )
