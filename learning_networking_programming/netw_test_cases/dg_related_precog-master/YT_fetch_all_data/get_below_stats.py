#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import os
import sys
import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from datetime import datetime


# In[2]:


lb,ub=int(sys.argv[1]), int(sys.argv[2])
'''lb=1
ub=5'''


# In[3]:


import os, pickle

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
API_SERVICE_NAME = 'youtubeAnalytics'

def pickle_file_name(
        api_name = API_SERVICE_NAME,
        api_version = 'v2'):
    return f'token_{api_name}_{api_version}.pickle'

def load_credentials(
        api_name = API_SERVICE_NAME,
        api_version = 'v2'):
    pickle_file = pickle_file_name(
        api_name, api_version)

    if not os.path.exists(pickle_file):
        return None

    with open(pickle_file, 'rb') as token:
        return pickle.load(token)

def save_credentials(
        cred, api_name = API_SERVICE_NAME,
        api_version = 'v2'):
    pickle_file = pickle_file_name(
        api_name, api_version)

    with open(pickle_file, 'wb') as token:
        pickle.dump(cred, token)
        

def create_service(
        client_secret_file, scopes,
        api_name = API_SERVICE_NAME,
        api_version = 'v2'):
    print(client_secret_file, scopes,
        api_name, api_version,
        sep = ', ')
    print("print 1 checkpoint")

    cred = load_credentials(api_name, api_version)

    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                    client_secret_file, scopes)
            cred = flow.run_console()

    save_credentials(cred, api_name, api_version)

    try:
        service = build(api_name, api_version, credentials = cred)
        print(api_name, 'service created successfully')
        return service
    except Exception as e:
        print(api_name, 'service creation failed:', e)
        return None
    
def execute_api_request(client_library_function, **kwargs):
    response = client_library_function(
          **kwargs
            ).execute()
  
    #print(response)
    a=json.dumps(response, indent=4)
    print(a)
    
    
def main():
    
    youtube_analytics = create_service("my_keys.json",
         ['https://www.googleapis.com/auth/yt-analytics.readonly'])
    
    print(youtube_analytics)
    if not youtube_analytics: return

    #response = request.execute()

    #print(response)
    
    '''execute_api_request(
       youtube_analytics.reports().query,
       ids='channel==MINE',
       startDate='2021-05-31',
       endDate='2021-06-03',
       metrics='estimatedMinutesWatched,views,likes,subscribersGained',
       dimensions='day',
       sort='day'
    )
'''
    return youtube_analytics


# api_name = API_SERVICE_NAME
# api_version = 'v2'
# res = load_credentials(api_name, api_version)
# res.valid
# if res and res.expired and res.refresh_token:
#     print(True)
#     res.refresh(Request())

# In[4]:


youtube_analytics=main()


# In[5]:


print(youtube_analytics)


# In[6]:


def get_requests(VIDEO_ID, start_day, time_granularity="day"):
    requests_dict={}

    START_DATE=start_day
    END_DATE="2021-07-20"
    CHANNEL_ID='MINE'
    TIME_GRANULARITY=time_granularity
    
    
    
    
        ######
    # Top 25 – YouTube search terms that generate the most traffic for a video
    # This query retrieves the 10 search terms that generated the most views from YouTube search results for one or more specific videos. Results are sorted by view count in descending order. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with a comma-separated list of up to 500 video IDs.

    # For example,you could run the query for a single video or replace VIDEO_ID to identify the search terms that generate the most traffic for that video. You could also list all of the videos in a particular playlist to determine which search terms generate the most traffic for any of the videos in that playlist.
    # insightTrafficSourceType
    KEY_NAME="top_10_YT_search_terms_which_led_to_video"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="insightTrafficSourceDetail",
            endDate=f"{END_DATE}",
            filters=f"video=={VIDEO_ID};insightTrafficSourceType==YT_SEARCH",
            ids=f"channel=={CHANNEL_ID}",
            maxResults=25,
            metrics="estimatedMinutesWatched,views",
            sort="-views",
            startDate=f"{START_DATE}"
        )

    #######

    KEY_NAME="Country_wise_stats-for_a_video"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="country",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="views,comments,likes,dislikes,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained,subscribersLost",
            startDate=f"{START_DATE}",
            filters=f"video=={VIDEO_ID}"
        )

    #######
    # Top 25 – External websites that generate the most traffic for a video
    # This query retrieves the 25 external websites that generated the most views for a specific video or group of videos. Results are sorted by estimated watch time in descending order. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with a comma-separated list of up to 500 video IDs for your uploaded videos.
    KEY_NAME="external_websites_that_generated_traffic_for_video"

    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="insightTrafficSourceDetail",
            endDate=f"{END_DATE}",
            filters=f"video=={VIDEO_ID};insightTrafficSourceType==EXT_URL",
            ids=f"channel=={CHANNEL_ID}",
            maxResults=25,
            metrics="estimatedMinutesWatched,views",
            sort="-views",
            startDate=f"{START_DATE}"
        )

    #####
    #TODO: TRY LATER AFTER INTERNAL ERROR IS RESOLVED
    # # This report provides user activity metrics for subscribed and unsubscribed viewers.
    # # Was the viewer subscribed to the channel that owns the video?

    # KEY_NAME="Subscribed_vs_unsubscribed_viewers_data"
    # requests_dict[KEY_NAME]=youtube_analytics.reports().query(
    #         dimensions="subscribedStatus",
    #         endDate=f"{END_DATE}",
    #         ids=f"channel=={CHANNEL_ID}",
    #         metrics="views,likes,dislikes,shares,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,cardClickRate,cardTeaserClickRate,cardImpressions,cardTeaserImpressions,cardClicks,cardTeaserClicks",
    #         startDate=f"{START_DATE}",
    #         filters=f"video=={VIDEO_ID}"
    #     )

    ##############
    # Audience retention metrics for a video
    # This report measures a video's ability to retain its audience. The report can only be retrieved for a single video at a time. Note that to run this query in the APIs Explorer,you must replace the string VIDEO_ID in the filters parameter value with the video ID for one of your uploaded videos. Note that audience retention reports do not support the ability to specify multiple values for the video filter.

    # In this example,the report uses the audienceType filter to restrict the response so that it only contains data for organic views,which are the result of a direct user action,such as a search for a video or a click on a suggested video. As such,the report does not include data for views from TrueView in-stream or TrueView in-display ads. You can remove the filter to retrieve the video's audience retention data from all audience types.

    # NOTE that data for the audienceType filter is available as of September 25,2013. The API will not return data for queries that use the filter to try to retrieve data from earlier dates. Queries that do not use the filter work for any date after July 1,2008.

    # NOTE ORGAINC filter: These are views that are the direct result of user intention. For example,traffic is considered organic if a viewer takes an action like searching for a video,clicking on a suggested video,or browsing a channel.

    # NOTE Paid Traffic: These are views that result from paid placement. The filters are AD_INSTREAM,and AD_INDISPLAY

    # Skippable video ad: Views for ads that are auto-played before a video and that viewers can skip after five seconds.
    # Display ads: Views that resulted from a viewer clicking a display ad,including ads shown in search results or on other video watch pages.

    
    
    #####################################################################################################################
    '''  # TODO: ONLY ONE VIDEO ID CAN BE GIVEN
    KEY_NAME="audience_retention_per_video"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="elapsedVideoTimeRatio",
            endDate=f"{END_DATE}",
            filters=f"video=={VIDEO_ID};audienceType==ORGANIC",
            ids=f"channel=={CHANNEL_ID}",
            metrics="audienceWatchRatio,relativeRetentionPerformance",
            startDate=f"{START_DATE}"
        )'''

    #####
    # This report provides statistics related to the type of page or application where video playbacks occurred.

    KEY_NAME="Video_playback_location"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="insightPlaybackLocationType",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="views,estimatedMinutesWatched",
            startDate=f"{START_DATE}",
            filters=f"video=={VIDEO_ID}"
        )



    #####
    # This report aggregates viewing statistics based on the manner in which viewers reached your video content.
    KEY_NAME="Viewers_reached_location"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="insightTrafficSourceType",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="views,estimatedMinutesWatched",
            filters=f"video=={VIDEO_ID}",
            startDate=f"{START_DATE}",
        )

    #####
    # This report aggregates viewing statistics based on the manner in which viewers reached your video content.
    KEY_NAME="Device_type_stats"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="deviceType",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="views,estimatedMinutesWatched",
            filters=f"video=={VIDEO_ID}",
            startDate=f"{START_DATE}",
        )

    #####
    # This report aggregates viewing statistics based on viewers' age group and gender.
    KEY_NAME="Age_group_gender_stats"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="ageGroup,gender",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="viewerPercentage",
            filters=f"video=={VIDEO_ID}",
            startDate=f"{START_DATE}",
        )

    #####
    # This report provides statistics showing how frequently the channel's videos were shared on different social platforms.
    KEY_NAME="content_sharing_stats"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="sharingService",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="shares",
            filters=f"video=={VIDEO_ID}",
            startDate=f"{START_DATE}",
        )

    #####
    KEY_NAME="Audience_retention_stats"
    requests_dict[KEY_NAME]=youtube_analytics.reports().query(
            dimensions="elapsedVideoTimeRatio",
            endDate=f"{END_DATE}",
            ids=f"channel=={CHANNEL_ID}",
            metrics="audienceWatchRatio,relativeRetentionPerformance",
            filters=f"video=={VIDEO_ID}",
            startDate=f"{START_DATE}",
        )

   
    return requests_dict


# In[7]:


videos_data = []
with open('videos_video.json','r') as file:
    file_data = json.load(file)
    for data in file_data:
        video_data = {}
        date = data['production_date']
        date_obj = datetime.strptime(date, '%m/%d/%Y')
        date = date_obj.strftime("%Y-%m-%d")
        video_data['id'] = data['youtubeid']
        video_data['upload_date'] = date
        videos_data.append(video_data)


# In[8]:


len(videos_data)


# In[9]:


count = lb
all_videos_data = []
error_id = []
START_DATE_DEFAULT="2007-07-20"
final_obj=dict()
for vid_data in videos_data[lb:ub+1]:
    try:
        
        ## The video number
        print(count)
        count+=1
        #requests_dict=get_requests(vid_data['id'], vid_data['upload_date'])
        requests_dict=get_requests(vid_data['id'],START_DATE_DEFAULT)
        data_array = []
        curr_obj=dict()
        curr_obj['VIDEO_ID_IN_COCO']=vid_data['id']
        curr_obj['VIDEO_PRECOG_SEQ_ID']=count-1
        for k in requests_dict.keys():
    #         print("Report type is ", k)
            response=requests_dict[k].execute()
            response["Report Type"]=k
            response["VideoID"] = vid_data['id']
            data_array.append(response)
        curr_obj['stats']=data_array
        all_videos_data.append(curr_obj)
    except:
        error_id.append(vid_data['id'])

final_obj['successful_files']=all_videos_data
final_obj['erroneous_video_IDs']=error_id


# In[10]:


with open(f'./below_1_stuff/below_stats_{lb}_to_{ub}.json', 'w') as outfile:
    json.dump(final_obj, outfile, indent=2)    

