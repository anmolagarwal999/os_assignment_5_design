## Metrics Table

|Metric|Explanation|
|---|---|
|views||
|comments||
|likes||
|dislikes||
|videosAddedToPlaylists |The number of times that videos were added to any YouTube playlists. The videos could have been added to the video owner's playlist or to other channels' playlists|
|videosRemovedFromPlaylists |The number of times that videos were removed from any YouTube playlists. The videos could have been removed from the video owner's playlist or from other channels' playlists|
|shares |The number of times that users shared a video through the Share button|
|estimatedMinutesWatched |The number of minutes that users watched videos for the specified channel, content owner, video, or playlist|
|averageViewDuration |The average length, in seconds, of video playbacks. In a playlist report, the metric indicates the average length, in seconds, of video playbacks that occurred in the context of a playlist|
|averageViewPercentage |The average percentage of a video watched during a video playback|
|annotationClickThroughRate |The ratio of annotations that viewers clicked to the total number of clickable annotation impressions
|annotationCloseRate |The ratio of annotations that viewers closed to the total number of annotation impressions|
|annotationImpressions |The total number of annotation impressions|
|annotationClickableImpressions |The number of annotations that appeared and could be clicked|
|annotationClosableImpressions |The number of annotations that appeared and could be closed|
|annotationClicks |The number of clicked annotations|
|annotationCloses |The number of closed annotations|
|cardClickRate |The click-through-rate for cards, which is calculated as the ratio of card clicks to card impressions|
|cardTeaserClickRate |The click-through-rate for card teasers, which is calculated as the ratio of clicks on card teasers to the total number of card teaser impressions|
|cardImpressions |The number of times cards were displayed. When the card panel is opened, a card impression is logged for each of the video's cards|
|cardTeaserImpressions |The number of times that card teasers were displayed. A video view can generate multiple teaser impressions|
|cardClicks |The number of times that cards were clicked|
|cardTeaserClicks |The number of clicks on card teasers. Card icon clicks are attributed to the last teaser displayed to the user|
|subscribersGained |The number of times that users subscribed to a channel|
|subscribersLost |The number of times that users unsubscribed from a channel|

> Annotations: These are the clickable images like the **"i"** button which is displayed during the video, they are only shown to user on desktop applications|