
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q3/src/thread_safe_queue.cpp ####################//

#include <bits/stdc++.h>

using namespace std;

template <class T>
class thread_safe_queue {

public:
	thread_safe_queue(){
		pthread_mutex_init(&lock, NULL);
		pthread_cond_init(&wakeup, NULL);
	}

	~thread_safe_queue(){
		pthread_mutex_destroy(&lock);
		pthread_cond_destroy(&wakeup);
	}

	T pop(){
		T ret;
		bool found = false;
		pthread_mutex_lock(&lock);
		while(found == false){
			if(!q.empty()){
				found = true;
				ret = q.front();
				q.pop();
			}
			if(!found)
				pthread_cond_wait(&wakeup, &lock);
		}
		pthread_mutex_unlock(&lock);
		return ret;
	}

	void push(T val){
		pthread_mutex_lock(&lock);
		q.push(val);
		pthread_cond_signal(&wakeup);
		pthread_mutex_unlock(&lock);
	}

private:
	queue<T> q;
	pthread_mutex_t lock;
	pthread_cond_t wakeup;
};

//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q3/src/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

#include "thread_safe_queue.cpp"
#include "thread_safe_dict.cpp"

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

vector<pthread_t> thread_pool;
thread_safe_queue<int> cqueue;
thread_safe_map<int, string> smap;

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
        return {"", -1};
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        // cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        
        auto iss = std::istringstream{cmd};
		auto str = std::string{};
		iss >> str;
		string k1s, temp;
		iss >> k1s;
		int k1 = stoll(k1s);

		string str_to_return = "";
		bool success;

		if(str == "insert"){
			iss >> temp;
			success = smap.insert_t({k1, temp});
			if(!success)
				str_to_return = "Key already exists";
			else 
				str_to_return = "Insertion successful";
		}
		else if(str == "concat"){
			iss >> temp;
			int k2 = stoll(temp);
			pair<bool, string> retval;
			retval = smap.cat_t(k1, k2);
			if(!retval.first)
				str_to_return = "Concat failed as at least one of the keys does not exist";
			else
				str_to_return = retval.second;
		}
		else if(str == "delete"){
			success = smap.erase_t(k1);
			if(!success)
				str_to_return = "No such key exists";
			else 
				str_to_return = "Deletion successful";
		}
		else if(str == "update"){
			iss >> temp;
			success = smap.update_t(k1, temp);
			if(!success)
				str_to_return = "Key does not exist";
			else
				str_to_return = temp;
		}
		else if(str == "fetch"){
			if(smap.exists(k1))
				str_to_return = smap.read_t(k1);
			else
				str_to_return = "Key does not exist";
		}


        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        pthread_t id = pthread_self();
        str_to_return = to_string(id) + ":" + str_to_return;
        int sent_to_client = send_string_on_socket(client_socket_fd, str_to_return);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *wait_on_client(void *arg){
	while(true){
		int client_socket_fd = cqueue.pop();
		pid_t tid = gettid();
		cout<<"Socket id: "<<client_socket_fd<<" Thread id: "<<tid<<endl;
		handle_connection(client_socket_fd);
	}
}

int main(int argc, char *argv[])
{

	if(argc != 2){
		cout<<"Usage ./server [number of worker threads in the thread pool]"<<endl;
		exit(1);
	}

	int num_threads = stoll(argv[1]);
	if(num_threads == -1){
		cout<<"Usage ./server [number of worker threads in the thread pool]"<<endl;
		exit(1);
	}

	thread_pool.resize(num_threads);
	smap.resize(101);

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        close(wel_socket_fd);
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        close(wel_socket_fd);
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    for(auto &x:thread_pool)
    	pthread_create(&x, NULL, wait_on_client, NULL);
    

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            close(wel_socket_fd);
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        cqueue.push(client_socket_fd);
        // handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q3/src/thread_safe_dict.cpp ####################//

#include <bits/stdc++.h>

using namespace std;

template <class Key, class T>
class thread_safe_map {

public:
	thread_safe_map() = default;
	thread_safe_map(int n){
		locks.resize(n);
		arr.assign(n, {false, ""});
		for(auto &x:locks) pthread_mutex_init(&x, NULL);
	}
	~thread_safe_map(){
		for(auto &x:locks) pthread_mutex_destroy(&x);
	}

	void resize(int n){
		arr.assign(n, {false, ""});
		for(auto &x:locks) pthread_mutex_destroy(&x);
		locks.resize(n);	
		for(auto &x:locks) pthread_mutex_init(&x, NULL);
	}

	bool update_t(Key k, T val){
		bool ret = true;
		pthread_mutex_lock(&locks[k]);
		if(arr[k].first != true) ret = false;
		else arr[k].second = val;
		pthread_mutex_unlock(&locks[k]);
		return ret;
	}

	bool insert_t(pair<Key, T> val){
		bool ret = true;
		pthread_mutex_lock(&locks[val.first]);
		if(arr[val.first].first == true) ret = false;
		else arr[val.first] = {true, val.second};
		pthread_mutex_unlock(&locks[val.first]);
		return ret;
	}

	pair<bool, string> cat_t(Key ff, Key ss){
		if(ff == ss) return {false, ""};
		
		T ff_val, ss_val;
		Key lff = ff, lss = ss;
		bool ret = true;
		string ret_str = "";

		if(lff > lss) swap(lff, lss);

		pthread_mutex_lock(&locks[lff]);
		pthread_mutex_lock(&locks[lss]);
		if(!arr[ff].first || !arr[ss].first) ret = false;
		else{
			ff_val = arr[ff].second;
			ss_val = arr[ss].second;
			arr[ff].second = ff_val + ss_val;
			arr[ss].second = ss_val + ff_val;
			ret_str = arr[ss].second;
		}
		pthread_mutex_unlock(&locks[lss]);
		pthread_mutex_unlock(&locks[lff]);
		return {ret, ret_str};
	}

	bool erase_t(Key k){
		bool ret = true;
		pthread_mutex_lock(&locks[k]);
		if(arr[k].first == false) ret = false;
		else arr[k] = {false, ""};
		pthread_mutex_unlock(&locks[k]);
		return ret;
	}

	T read_t(Key k){
		pthread_mutex_lock(&locks[k]);
		T val = arr[k].second;
		pthread_mutex_unlock(&locks[k]);
		return val;
	}

	bool exists(Key k){
		bool ret;
		pthread_mutex_lock(&locks[k]);
		ret = arr[k].first;
		pthread_mutex_unlock(&locks[k]);
		return ret;
	}

private:
	vector<pthread_mutex_t> locks;
	vector<pair<bool, string>> arr;
};
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q3/src/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

vector<pair<int, string>> cmds;

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *arg)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    int *iarg = (int*) arg;
    int index = *iarg;
    // cout << "Connection to server successful" << endl;
    
    string cmd = cmds[index].second + '\n';
    send_string_on_socket(socket_fd, cmd);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    // cout << "Received: " << output_msg << endl;
    // cout << "====" << endl;
    output_msg += '\n';
    output_msg = to_string(index) + ":" + output_msg;
    cout<<output_msg;
    send_string_on_socket(socket_fd, "exit");
    return NULL;
}

int main(int argc, char *argv[])
{
    int n; cin>>n;
    cmds.resize(n);

    for(int i=0; i<n; i++){
        cin>>cmds[i].first;
        getline(cin, cmds[i].second);
    }

    int cur = 0;
    for(int i=0; i<n; cur++, sleep(1)){

        while(i<n && cur == cmds[i].first){
            pthread_t *newthread = (pthread_t*) malloc(sizeof(pthread_t));
            int *arg = (int*) malloc(sizeof(int));
            *arg = i;
            pthread_create(newthread, NULL, begin_process, arg);
            i++;
        }
    }
    return 0;
}