
//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include "headers.h"
#include <queue>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define MAX_WORKERS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;
int num_workers;
pthread_t worker_pool[MAX_WORKERS];
queue<struct client> client_Q;
pthread_mutex_t queueLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t waitforclient = PTHREAD_COND_INITIALIZER;
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

// modifies map accordingto the request made
string handle_map(string message)
{
    size_t pos = 0;
    int i = 0;
    std::string token;
    string arr[10];
    //----------extract the request-----------------
    while ((pos = message.find(" ")) != std::string::npos)
    {
        token = message.substr(0, pos);
        arr[i] = token;
        i++;
        message.erase(0, pos + 1);
    }
    if (message.length()!= 0)
    {
        arr[i] = message;
        i++;
    }

    //-----------------------------------------------
    //accordingly react
    if (arr[0].compare("insert") == 0)
    {
        if (i != 3)
        {
            cout << i;
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (insert(key, arr[2]))
        {
            return "Insertion successful";
        }

        return "Key already exists";
    }
    else if (arr[0].compare("delete") == 0)
    {
        if (i != 2)
        {
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (deleteKey(key))
        {
            return "Deletion successful";
        }
        else
        {
            return "No such key exists";
        }
    }
    else if (arr[0].compare("update") == 0)
    {
        if (i != 3)
        {
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (update(key, arr[2]))
        {
            return arr[2];
        }
        else
        {
            return "Key does not exist";
        }
    }
    else if (arr[0].compare("fetch") == 0)
    {
        if (i != 2)
        {
            cout << i;
            return "invalid command";
        }
        int key = stoi(arr[1]);
        string x = fetch(key);
        if (x == "")
        {
            return "Key does not exist";
        }

        return x;
    }
    else if (arr[0].compare("concat") == 0)
    {
        if (i != 3)
        {
            return "invalid command";
        }
        int key1 = stoi(arr[1]);
        int key2 = stoi(arr[2]);
        string x = concat(key1, key2);
        if (x == "")
        {
            return "Concat failed as at least one of the keys does not exist";
        }

        return x;
    }
    else
    {
        cout << arr[0];
        return "invalid command";
    }
}

void handle_connection(int client_socket_fd, int id)
{
    int received_num, sent_num;
    /* read message from client */
    int ret_val = 1;
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;

    //handle error
    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //printout what client send
    cout << "Client sent : " << cmd << endl;
    //if client send an exit comd close the socket
    if (cmd == "exit")
    {
        cout << "Exit pressed by client" << endl;
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //manipulate map according to the request made
    string feedback = handle_map(cmd);
    string subpart = ":" + to_string(id);
    string msg_to_send_back = subpart + ":" + feedback;

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read.
    //Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

    sleep(2); //asked to do in question
    //send back the verdict
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);

    //handle error
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //close the socket
    close(client_socket_fd);
    printf(BRED "Disconnected from client" RESET "\n");
    return;
}

//thread function
void *workersJob(void *inp)
{
    int id = ((struct worker *)inp)->id;
    while (true)
    {
        //lock to modify shared queue
        pthreadMutexLock(&queueLock);
        while (client_Q.empty())
        { //wait till new client arrive and UNlock till then
            pthreadCondWait(&waitforclient, &queueLock);
        }
        struct client pclient = client_Q.front();
        client_Q.pop();
        //unlock since modification is done
        pthreadMutexUnlock(&queueLock);

        handle_connection(pclient.client_socket_fd, id);
    }
}

int main(int argc, char *argv[])
{

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    if (argc != 2)
    {
        return 0;
    }
    else
    {

        num_workers = atoi(argv[1]);
    }
    worker_pool[num_workers];

    //spawn these ‘n’ worker threads.
    for (int i = 0; i < num_workers; i++)
    {
        struct worker w;
        w.id = i;
        //cout << i;
        pthreadCreate(&worker_pool[i], NULL, workersJob, &w);
    }
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door of the server process. 
        When the server “hears” the knocking, it creates a new door—more precisely,
         a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(GREEN "New client connected from port number %d and IP %s \n" RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        struct client c;

        c.client_socket_fd = client_socket_fd;
        //lock the queue to modify saving it from race condition
        pthreadMutexLock(&queueLock);
        client_Q.push(c);
        //queue is modified successfully unlock the queuelock
        pthreadMutexUnlock(&queueLock);
        //signal the worker waiting for client
        pthreadCondSignal(&waitforclient);
    }

    close(wel_socket_fd);
    return 0;
}


//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q3/map.cpp ####################//

#include <iostream>
using namespace std;
#include "headers.h"
pthread_mutex_t treeLock = PTHREAD_MUTEX_INITIALIZER;

struct node
{
    int key;           //key
    string value;      //value
    pthread_mutex_t m; //mutex
    struct node *left, *right;
};
//======================
node *root; //main root
//======================

// Create a node
struct node *newNode(int key, string value)
{
    struct node *temp = new node();
    temp->key = key;
    temp->value = value;
    pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
    temp->left = temp->right = NULL;

    return temp;
}


//fetch a node
string fetch(int key)
{ //lock whole tree to see if empty
  
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    { //unlock before returning
        pthreadMutexUnlock(&treeLock);
        return "";
    }
    //confirmed not empty so unlock
    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose value is to fetched
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return "";
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            { //unlock before returning
                pthreadMutexUnlock(&current->m);
                return "";
            }
            else
            {
                next = current->right;
            }
        }
        else
        { //unlock before returning
            pthreadMutexUnlock(&current->m);
            return current->value;
        }
        //unlock current node since we confired it is not the one
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}
//update
bool update(int key, string value)
{ //lock whole tree to see if empty
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    { //unlock before returning
        pthreadMutexUnlock(&treeLock);
        return false;
    }
    //confirmed not empty so unlock
    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose left or right node needs to be modifed
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return false;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return false;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {
            current->value = value;
            //unlock before returning
            pthreadMutexUnlock(&current->m);

            return true;
        }
        //unlock current node since we confired it is not the one to be modifed
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}

// Insert a node
bool insert(int key, string value)
{
    // Create a new Node containing
    // the new key value pair
    node *newnode = newNode(key, value);
    //lock whole tree to see if we want to insert at root
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        root = newnode;
        pthreadMutexUnlock(&treeLock);
        return true;
    }
    //confirmed not want to insert at root so unlock
    pthreadMutexUnlock(&treeLock);

    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose left or right node needs to be modifed
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                current->left = newnode;
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return true;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree
            if (current->right == NULL)
            {
                current->right = newnode;
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return true;
            }
            else
            {
                next = current->right;
            }
        }
        else
        { //unlock before returning
            pthreadMutexUnlock(&current->m);
            return false;
        }
        //unlock current node since we confired it is not the one to be modifed
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}

bool removeCurrentNode(node *current, node *parent)
{
    //no children
    if (current->left == NULL && current->right == NULL)
    {
        if (parent)
        {
            if (current == parent->left)
            {
                parent->left = NULL;
            }
            else
            {
                parent->right = NULL;
            }

            pthreadMutexUnlock(&parent->m);
        }
        else{
            root =NULL;
        }
        pthreadMutexUnlock(&current->m);
        delete current;

        return true;
    }

    // only has a single child
    if (current->left == NULL || current->right == NULL)
    {

        node *shift = (current->right == NULL ? current->left : current->right);
        if (!parent)
        {
            root = shift;
            pthreadMutexUnlock(&current->m);
            delete current;
            return true;
        }
        else
        {
            if (current == parent->left)
            {
                parent->left = shift;
            }
            else
            {
                parent->right = shift;
            }
            pthreadMutexUnlock(&current->m);
            delete current;
            pthreadMutexUnlock(&parent->m);
            return true;
        }
    }

    else
    {
        // Two children
        node *min_parent = current;
        node *min = current->right;
        pthreadMutexUnlock(&parent->m);

        //  lock on current, min_parent , and min
        while (true)
        {
            if (min->left == NULL)
            { // Minimum element
                current->key = min->key;
                current->value = min->value;
                node *shift = min->right;
                if (min_parent == current)
                {
                    min_parent->right = shift;
                    delete min;
                }
                else
                {
                    min_parent->left = shift;
                    delete min;
                    pthreadMutexUnlock(&min_parent->m);
                }

                pthreadMutexUnlock(&current->m);
                return true;
            }
            // traverse to find minimum in right sub tree
            pthreadMutexLock(&min->left->m);
            if (min_parent != current)
            {
                pthreadMutexUnlock(&min_parent->m);
            }
            min_parent = min;
            min = min->left;
        }
    }
}

bool deleteKey(int key)
{
    //lock whole tree to see if its empty
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        pthreadMutexUnlock(&treeLock);
        return false;
    }

    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    node *parent = NULL;
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {

                pthreadMutexUnlock(&current->m);
                if (parent)
                {
                    pthreadMutexUnlock(&parent->m);
                }
                return false;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                pthreadMutexUnlock(&current->m);
                if (parent)
                {
                    pthreadMutexUnlock(&parent->m);
                }
                return false;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {
            removeCurrentNode(current, parent);
            return true;
        }
        if (parent)
        {
            pthreadMutexUnlock(&parent->m);
        }
        parent = current;
        current = next;
        pthreadMutexLock(&current->m);
    }
}

//fetch a node
node *fetchToConcat(int key)
{
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        pthreadMutexUnlock(&treeLock);
        return NULL;
    }

    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {

                pthreadMutexUnlock(&current->m);
                return NULL;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                pthreadMutexUnlock(&current->m);
                return NULL;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {

            pthreadMutexUnlock(&current->m);
            return current;
        }

        pthreadMutexUnlock(&current->m);
        current = next;
        //printf("okay");
        pthreadMutexLock(&current->m);
    }
}

string concat(int key1, int key2)
{
    int f = 0;
    if (key2 < key1)
    {
        f = 1;
        int t = key1;
        key1 = key2;
        key1 = t;
    }
    node *a = fetchToConcat(key1);
    node *b = fetchToConcat(key2);
    if (a && b)
    {
        pthreadMutexLock(&a->m);
        pthreadMutexLock(&b->m);
        string x = a->value;
        a->value += b->value;
        pthreadMutexUnlock(&a->m);
        b->value += x;
        pthreadMutexUnlock(&b->m);
        if (!f)
            return b->value;
        return a->value;
    }

    return "";
}

//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include "headers.h"
using namespace std;
/////////////////////////////

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
struct timespec start; //time when simulation started
int num_client;
pthread_cond_t timec = PTHREAD_COND_INITIALIZER;
pthread_mutex_t timeLock = PTHREAD_MUTEX_INITIALIZER;
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *client_function(void *inp)
{
    struct timespec to;
    struct timespec now;
    long time = ((struct client_attr *)inp)->time;
    string message = ((struct client_attr *)inp)->message;
    int id = ((struct client_attr *)inp)->id;
    pthreadMutexLock(&timeLock); //FOR CONDITION

    to.tv_sec = start.tv_sec + time;
    pthread_cond_timedwait(&timec, &timeLock, &to);
    pthreadMutexUnlock(&timeLock);
    /* clock_gettime(CLOCK_REALTIME, &now);
    cout<< message;
    printf("%ld",now.tv_sec-start.tv_sec);
    cout << endl; */
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, message);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << id << output_msg << endl;
    pthread_exit(0);
}
int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    cin >> num_client;
    string x;
    pthread_t client_threads[num_client];
    clock_gettime(CLOCK_REALTIME, &start);

    for (i = 0; i < num_client; i++)
    {  
        struct client_attr *a = new client_attr();
        a->id = i;
        cin >> a->time;
        cin >> ws;
        
        getline(cin, a->message);

        pthreadCreate(&client_threads[i], NULL, client_function, (void *)(a));
    }
    for (i = 0; i < num_client; i++)
    {   
        pthreadJoin(client_threads[i],NULL);
    }
    return 0;
}




//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q3/errorHandle.cpp ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "headers.h"

void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg)
{
    if (pthread_create(thread, attr, start_routine, arg) != 0)
    {
        perror("Thread was not created");
    }
}

void pthreadJoin(pthread_t thread, void **retval)
{
    if (pthread_join(thread, retval) != 0)
    {
        perror("Thread could not join");
    }
}

void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *m)
{
    if (pthread_cond_wait(cv, m) != 0)
    {
        perror("Cannot cond wait on cv");
    }
}
void pthreadCondSignal(pthread_cond_t *cv)
{
    if (pthread_cond_signal(cv) != 0)
    {
        perror("Cannot cond signal on cv");
    }
}

void pthreadMutexLock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_lock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadMutexUnlock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_unlock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadCondBroadcast(pthread_cond_t *cv)
{
    if (pthread_cond_broadcast(cv) != 0)
    {
        perror("Cannot broadcast");
    }
}
void pthreadCondTimeWait(pthread_cond_t *cv,pthread_mutex_t *m,const struct timespec *time)
{
  
  
    if ( pthread_cond_timedwait(cv,m,time)!= 0)
    {
        perror("Cannot cond wait on cv");
    }
}

void pthreadMutexInit(pthread_mutex_t *m)
{
    if (pthread_mutex_init(m,0) != 0)
    {
        perror("Cannot initialize mutex");
    }
}


void pthreadMutexDestroy(pthread_mutex_t *m)
{
    fflush(stdout);
    int f = pthread_mutex_destroy(m);
    if ( f!= 0)
    {   printf("%d",f);
        perror("Cannot destroy mutex");
    }
}

void pthreadCondDestroy(pthread_cond_t *cv)
{
    fflush(stdout);
    if ( pthread_cond_destroy(cv)!= 0)
    {
        perror("Cannot destroy cv");
    }
}


//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q3/headers.h ####################//

#ifndef HEADERS_H /* This is an "include guard" */
#define HEADERS_H
#include <string>
#define RED "\x1b[31m"
#define BRED "\x1b[1;31m"
#define GREEN "\x1b[1;32m"
#define YELLOW "\x1b[1;33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define node_max
extern struct node* root; //main root
extern pthread_mutex_t treeLock; //tree lock

struct client_attr{
    long time;
   std::string  message;
   int id;
};
struct client
{
    int client_socket_fd;
};
struct worker
{
    int id;
};

//functions
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void pthreadJoin(pthread_t thread, void **retval);
void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *mutex);
void pthreadMutexLock(pthread_mutex_t *mutex);
void pthreadMutexUnlock(pthread_mutex_t *mutex);
void pthreadCondBroadcast(pthread_cond_t *cv);
void pthreadMutexDestroy(pthread_mutex_t *m);
void pthreadCondDestroy(pthread_cond_t *cv);
void pthreadMutexInit(pthread_mutex_t *m);
void pthreadCondSignal(pthread_cond_t *cv);
std::string fetch(int key);
bool update(int key, std::string value);
bool insert(int key, std::string value);
bool deleteKey(int key);
std::string concat(int key1, int key2);
#endif
