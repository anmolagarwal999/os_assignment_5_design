
//###########FILE CHANGE ./main_folder/Manas Yendluri_305810_assignsubmission_file_/2019113008_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <queue>
#include <map>
#include <pthread.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 6
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////


// command queue
queue<int> commandQueue;
pthread_t* threadsPool;
pthread_mutex_t commandQueueMutex;
pthread_mutex_t dictMutex;
pthread_cond_t commandQueueCond;
// dict for server 
pair<int, string> dictDb[200];
int dictSize=0;
string handleQuery(string command);


pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = to_string(pthread_self()) + ":" + handleQuery(cmd);

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        
        // delay b4 response
        sleep(3);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}


// utility function to tokenize_input string given delimiter
vector<string> tokenizeInput(string str) {
    vector<string> output = {};
    int n=str.length();
    char cstr[n+1];
    strcpy(cstr,str.c_str());
    char * token= (char *)malloc(4000);
    token=strtok(cstr," ");
    while(token!=NULL)
    {
        string temp="";
        for(int i=0;i<strlen(token);i++)
        {
            temp=temp+token[i];
        }
        if(temp.length()>0)
        {
            output.push_back(temp);
        }
        token=strtok(NULL," ");
    }
    // TODO : remove this comment and below comment.
    // size_t pos = 0;
    // while ((pos = str.find(delim)) != string::npos) {
    //     string token = str.substr(0, pos);
    //     if (token.length() > 0) output.push_back(token);
    //     str.erase(0, pos + delim.length());
    // }
    // if (str.length() > 0) output.push_back(str);
    return output;
}

// insert key-value pair into dictionary
string handleInsert(vector<string> args) {
    int key=stoi(args[1]);
    string msg = "";
    string value=args[2];
    pthread_mutex_lock(&dictMutex);
    int flag=0;
    for(int i=0;i<dictSize;i++)
    {
        if(dictDb[i].first==key)
        {
            flag=1;
            msg="Key already exists";
        }
    }
    if(flag==0)
    {
        dictDb[dictSize++]={key,value};
        msg = "Insertion successful";
    }
    pthread_mutex_unlock(&dictMutex);
    return msg;
}

// delete key from dictionary
string handleDelete(vector<string> args) {
    int key=stoi(args[1]);
    int found=-1;
    string msg = "";
    pthread_mutex_lock(&dictMutex);
    for(int i=0;i<dictSize;i++)
    {
        if(dictDb[i].first==key)
        {
            found=i;
            break;
        }
    }
    if(found==-1)
    {
        msg="No such key exists";
    }
    else
    {
        for(int i=found;i<dictSize-1;i++)
        {
            dictDb[i]={dictDb[i+1].first,dictDb[i+1].second};
        }
        msg = "Deletion successful";
    }
    pthread_mutex_unlock(&dictMutex);
    return msg;
}

// update value of given key in dictionary
string handleUpdate(vector<string> args) {
    int key=stoi(args[1]);
    int found=-1;
    string msg = "";
    string value=args[2];
    pthread_mutex_lock(&dictMutex);
    for (int i=0;i<dictSize;i++)
    {
        if(dictDb[i].first==key)
        {
            found=i;
            break;
        }
    }
    if(found==-1)
    {
        msg="Key does not exist";
    }
    else
    {
        dictDb[found].second = value;
        msg = value;
    }
    pthread_mutex_unlock(&dictMutex);
    return msg;
}

// fetch concatenation of values at specified keys in the dictionary
string handleConcat(vector<string> args) 
{
    int key1=stoi(args[1]);
    int key2=stoi(args[2]);
    string msg = "";
    int found1=-1;
    int found2=-1;
    pthread_mutex_lock(&dictMutex);
    for(int i=0;i<dictSize;i++)
    {
        if(dictDb[i].first==key1)
            found1=i;
        else if(dictDb[i].first==key2)
            found2=i;        
    }
    if(found1==-1 || found2==-1)
    {
        msg="Concat failed as atleast one of the keys does not exist";
    }
    else
    {
        string temp = dictDb[found2].second;
        dictDb[found2].second += dictDb[found1].second;
        dictDb[found1].second += temp;
        msg = dictDb[found2].second;
    }
    pthread_mutex_unlock(&dictMutex);
    return msg;
}

// fetch value of given key from dictionary
string handleFetch(vector<string> args) {
    int key=stoi(args[1]);
    string msg = "";
    int found=-1;
    pthread_mutex_lock(&dictMutex);
    // check if key exists
    for(int i=0;i<dictSize;i++)
    {
        if(dictDb[i].first==key)
        {
            found=i;
            break;
        }
    }
    if(found==-1)
    {
        msg="Key does not exist";
    }
    else 
        msg = dictDb[found].second;
    pthread_mutex_unlock(&dictMutex);
    return msg;
}
// --- end of handling functions.

string handleQuery(string command) {
    vector<string> args = tokenizeInput(command);
    string msg = "command not found";
    if (args.size()>0)
    {
        if (args[0] == "insert")
        {
            if (args.size() < 3)
            {
                msg = args[0] + ": not enough args";
            }
            else
            {
                msg = handleInsert(args);
            }                 
        }
        else if (args[0] == "concat")
        {
            if (args.size() < 3) 
            {
                msg = args[0] + ": not enough args";
            }
            else
            { 
                msg = handleConcat(args);
            }
        }
        else if (args[0] == "update")
        {
            if (args.size() < 3) 
            {
                msg = args[0] + ": not enough args";
            }
            else
            {
                msg = handleUpdate(args);
            }
        }
        else if(args[0] == "delete")
        {
            if (args.size() < 2) 
            {
                msg = args[0] + ": not enough args";
            }
            else
            {
                msg = handleDelete(args);
            }
        }        
        else if (args[0] == "fetch")
        {
            if (args.size() < 2)
            { 
                msg = args[0] + ": not enough args";
            }
            else
            {
                msg = handleFetch(args);
            }
        }
        else
        {
            msg = args[0] + ": command not found";
        }
    }
    return msg;
}



void* consumerRoutine(void* args)
{
    while(1)
    {
        int client_socket_fd;
        pthread_mutex_lock(&commandQueueMutex);
        while(commandQueue.empty())
            pthread_cond_wait(&commandQueueCond, &commandQueueMutex);
        client_socket_fd = commandQueue.front();
        commandQueue.pop();
        pthread_mutex_unlock(&commandQueueMutex);

        handle_connection(client_socket_fd);
    }
}

int main(int argc, char *argv[])
{
    int threadsPoolSize = 6    ;
    if(argc > 1) 
    {
        threadsPoolSize = atoi(argv[1]);
    }
    threadsPool = (pthread_t*)malloc(threadsPoolSize*sizeof(pthread_t));

    // create consumer threads
    for (int i = 0; i < threadsPoolSize; i++) 
    {
        if (pthread_create(&threadsPool[i], NULL, &consumerRoutine, NULL)) 
            cerr<<"couldnt assign a thread in the thread pool ";
    }

    // init the mutex and cond
    pthread_mutex_init(&commandQueueMutex, NULL);
    pthread_cond_init(&commandQueueCond, NULL);
    pthread_mutex_init(&dictMutex, NULL);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT"<< endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
    of the server process. When the server “hears” the knocking, it creates a new door—
    ore precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        pthread_mutex_lock(&commandQueueMutex);
        commandQueue.push(client_socket_fd);
        pthread_mutex_unlock(&commandQueueMutex);
        pthread_cond_signal(&commandQueueCond);
    }

    //freeing malloc
    free(threadsPool);
    // destroy mutexes and conds
    pthread_mutex_destroy(&commandQueueMutex);
    pthread_mutex_destroy(&dictMutex);
    pthread_cond_destroy(&commandQueueCond);

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Manas Yendluri_305810_assignsubmission_file_/2019113008_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
struct commandStruct {
    int id;
    int delayTime;
    string command;
};
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////




// client routine ---
void* clientExecute(void* args) {
    struct commandStruct *request = (struct commandStruct*)args;

    // delay time
    sleep(request->delayTime);

    struct sockaddr_in serverAddress;
    //get socket address

    int socketFd = get_socket_fd(&serverAddress);

    //send the command from client using fd
    if (send_string_on_socket(socketFd, request->command)==-1)
    {
        cerr << "couldnt write to the server socket. socket issue ig .\n";
    }

    // read server response
    pair<string, int> outputPair;
    outputPair = read_string_from_socket(socketFd, buff_sz);
    if (outputPair.second <= 0) 
    {
        cerr << "couldnt read server msg\n";
    }
    cout << request->id << ":" << outputPair.first << "\n";

    // close connection
    close(socketFd);

    return NULL;
}
// }}}

int main(int argc, char* argv[]) {

    int commandCount;
    cin >> commandCount;
    struct commandStruct commandList[commandCount];
    // take input
    for (int i = 0; i < commandCount; i++) {
        commandList[i].id = i;
        cin >> commandList[i].delayTime; 
        getline(cin, commandList[i].command);
    }

    // initialize threads
    pthread_t clientThreads[commandCount];
    for (int i = 0; i < commandCount; i++) {
        struct commandStruct* command = &commandList[i];
        pthread_t* thread = &clientThreads[i];        
        if (pthread_create(thread, NULL, &clientExecute, command)) 
        {
            cerr<<"Error creating client threads\n";
        }
    }

    // join threads
    for (int i = 0; i < commandCount; i++) 
    {
        if (pthread_join(clientThreads[i], NULL))
        {
            cerr<<"Error joining client threads\n";
        }
    }
    return 0;
}