
//###########FILE CHANGE ./main_folder/Meka Sai Mukund_305837_assignsubmission_file_/2020101043_assignment_5/q3/q3_server.h ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>

// threads
#include <pthread.h>
#include <semaphore.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>

#include <vector>
#include <queue>  // std::queue
#include <sstream>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 44
#define PORT_ARG 8001
#define MAX_SZ 100

const int initial_msg_len = 256;
const LL buff_sz = 1048576;
////////////////////////////////////
string dictionary[ 2*MAX_SZ+5 ] = {""}; 
pthread_mutex_t dict_lock[ 2*MAX_SZ+5 ] = {PTHREAD_MUTEX_INITIALIZER};

typedef struct thread_info {
    pthread_t thread_id;
    int id;
}threads;

void *waiting(void *arg);

queue<int *> client_q;
pthread_mutex_t lock_q = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t signal_q = PTHREAD_COND_INITIALIZER;

//###########FILE CHANGE ./main_folder/Meka Sai Mukund_305837_assignsubmission_file_/2020101043_assignment_5/q3/q3_client.cpp ####################//

#include "q3_client.h"
///////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void begin_process(string &strs)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    // pthread_mutex_lock(&print_lock);
    // cout << "Connection to server successful" << endl;
    // pthread_mutex_unlock(&print_lock);

    send_string_on_socket(socket_fd, strs);
    // cout << "here\n";
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    istringstream iss(output_msg);
    string ack;
    iss >> ack;
    if( ack != "Ack:")
    {
        cout << "error: server has not acknowledged msg" << endl;
        return;
    }

    pthread_mutex_lock(&print_lock);
    while( iss >> ack )
        cout << ack << " ";
    cout << endl;
    pthread_mutex_unlock(&print_lock);
}   

void *thread_func(void *args)
{
    c_info *cli = (c_info *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += cli->time;

    sem_timedwait(&imaginary_sem, &ts);     //acts as a timer

    // cout << "then " << cli->msg << endl;
    begin_process(cli->msg);

    return NULL;
}

int main(int argc, char *argv[])
{
    int m;
    cin >> m;

    client_list = new c_info[m];
    sem_init(&imaginary_sem, 0, 0);

    for (int i = 0; i < m; i++)
    {
        int time;
        string cmd, key;
        cin >> time >> cmd >> key;

        string key2;
        if( cmd != "delete" && cmd != "fetch" )
        {
            cin >> key2;
            client_list[i].msg = to_string(i) + " " + cmd + " " + key + " " + key2;
        }

        else
        {
            client_list[i].msg = to_string(i) + " " + cmd + " " + key;
        }
        client_list[i].time = time;
    }

    for( int i = 0; i < m; i++)
    {
        pthread_create(&client_list[i].thread_id, NULL, thread_func, (void *)&client_list[i]);
    }
    
    for( int i = 0; i < m; i++)
    {
        pthread_join(client_list[i].thread_id, NULL);
    }
    // cout << "finished" << endl;
    
    return 0;
}
//###########FILE CHANGE ./main_folder/Meka Sai Mukund_305837_assignsubmission_file_/2020101043_assignment_5/q3/q3_server.cpp ####################//

#include "q3_server.h"
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////
void handle_connection(int *p_client_socket_fd, pthread_t worker_thr_id)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################
    int client_socket_fd = *p_client_socket_fd;
    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;
    int someth = 1;

    while (someth)
    {
        someth = 0;
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            break;
        }
        // cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            break;
        }

        string msg_to_send_back = "";
        istringstream args(cmd);
        string key_str;
        // cout << "before_keystr\n";
        args >> key_str;
        // cout << "after_ " << key_str << "\n";
        int index = stoi(key_str);
        // debug(index);

        string cmd_name;
        string value;
        args >> cmd_name;
        args >> key_str;
        int key = stoi(key_str);
        int stat = 0;
        if( key < 0 || key > 100 )
        {
            msg_to_send_back += "arguments: Invalid key";
            goto send_msg_to_client;
        }

        if( cmd_name != "delete" && cmd_name != "fetch" )
            args >> value;
        
        msg_to_send_back = "Ack: " + to_string(index) + ":" + to_string(worker_thr_id) + ":";

        if( cmd_name == "insert" )
        {
            stat = 0;
            pthread_mutex_lock(&dict_lock[key-1]);
            if( dictionary[key-1].empty() )
            {
                dictionary[key-1] = value;
                stat = 1;
            }
            pthread_mutex_unlock(&dict_lock[key-1]);
            
            if( stat )
                msg_to_send_back += "Insertion successful" ;
            else
                msg_to_send_back += "key already exists";
        }

        else if( cmd_name == "concat" )
        {
            stat = 0;
            int key2 = stoi(value);
            if( key2 < 0 || key2 > 100 )
            {
                msg_to_send_back += "arguments: Invalid key" ;
                goto send_msg_to_client;
            }
            string s1, s2; 
            int t = key, t2 = key2;
            
            if( t > t2 )        // always acquire locks in ascending order
            {
                t = key2;
                t2 = key;
            }

            pthread_mutex_lock(&dict_lock[t-1]);   
            pthread_mutex_lock(&dict_lock[t2-1]);
            if( !dictionary[key-1].empty() && !dictionary[key2-1].empty() )
            {
                s1 = dictionary[key-1] + dictionary[key2-1];
                s2 = dictionary[key2-1] + dictionary[key-1];
                dictionary[key-1] = s1;
                dictionary[key2-1] = s2;
                stat = 1;
            }
            pthread_mutex_unlock(&dict_lock[t-1]);
            pthread_mutex_unlock(&dict_lock[t2-1]);
            
            if( stat )
                msg_to_send_back += s2;
            else
                msg_to_send_back += "concat failed as at least one of the keys does not exist";
        }

        else if( cmd_name == "update" )
        { 
            stat = 0;          
            pthread_mutex_lock(&dict_lock[key-1]);
            if( !dictionary[key-1].empty() )
            {
                dictionary[key-1] = value;
                stat = 1;
            }
            pthread_mutex_unlock(&dict_lock[key-1]);
            
            if( stat )
                msg_to_send_back += value;
            else
                msg_to_send_back += "key already exists";
        }

        if( cmd_name == "delete" )
        {
            stat = 0;
            pthread_mutex_lock(&dict_lock[key-1]);
            if( !dictionary[key-1].empty() )      // not empty
            {
                dictionary[key-1] = "";
                stat = 1;
            }
            pthread_mutex_unlock(&dict_lock[key-1]);
            
            if( stat )
                msg_to_send_back += "Deletion successful";
            else
                msg_to_send_back += "No such key exists";
        }

        else if( cmd_name == "fetch" )
        {
            string val;
            stat = 0;
            pthread_mutex_lock(&dict_lock[key-1]);
            if( !dictionary[key-1].empty() )      // not empty
            {
                val = dictionary[key-1];
                stat = 1;
            }
            pthread_mutex_unlock(&dict_lock[key-1]);
            
            if( stat )
                msg_to_send_back += val ;
            else
                msg_to_send_back += "Key does not exist";
        }

        ////////////////////////////////////////
           // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
           // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        send_msg_to_client:
        // cout << "\n" << "here " << msg_to_send_back << endl;
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
        }
    }
    
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

void *waiting(void *arg)
{
    threads *t = (threads *)arg;
    while (true)
    {
        int *tmp = NULL;
        pthread_mutex_lock(&lock_q);
        if( client_q.empty() )
        {
            pthread_cond_wait(&signal_q, &lock_q);
        }
        tmp = client_q.front();
        client_q.pop();
        pthread_mutex_unlock(&lock_q);
        
        handle_connection(tmp, t->thread_id);
    }
}

int main(int argc, char *argv[])
{
    if( argc != 2 )
    {
        printf("Usage: %s <number of worker threads in the thread pool>\n", argv[0]);
        return 1;
    }
    int num_threads = atoi(argv[1]);
    if( num_threads <= 0 )
    {
        printf("Number of threads must be positive\n");
        return 2;
    }

    threads worker[num_threads];
    int cur_thread_id = 0;

    for( int i = 0; i < num_threads; i++ )
    {
        worker[i].id = cur_thread_id++;
        pthread_create(&(worker[i].thread_id), NULL, waiting, (void *)&worker[i]);
    }


    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);



    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        int *client_fd = new int;
        *client_fd = client_socket_fd;

        pthread_mutex_lock(&lock_q);
        client_q.push(client_fd);
        pthread_cond_signal(&signal_q);
        pthread_mutex_unlock(&lock_q);
    }

    for( int i = 0; i < num_threads; i++ )
        pthread_join(worker[1].thread_id, NULL);

    close(wel_socket_fd);
    return 0;
}

/**
11
1 insert 1 hello
2 insert 1 hello
2 insert 2 yes
2 insert 3 no
3 concat 1 2
3 concat 1 3 
4 delete 3
5 delete 4
6 concat 1 4
7 update 1 final
8 concat 1 2

**/
//###########FILE CHANGE ./main_folder/Meka Sai Mukund_305837_assignsubmission_file_/2020101043_assignment_5/q3/q3_client.h ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <sstream>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
//////////////////////////////////////////////////
typedef struct client_info {
    pthread_t thread_id;
    string msg;
    int time;
} c_info;

c_info *client_list;    // points to list of all client threads
sem_t imaginary_sem;
pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;