
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/globals.h ####################//

#ifndef GLOBALS_H
#define GLOBALS_H

#define MAX_SIZE 101


#include <bits/stdc++.h>
#include <pthread.h>

using namespace std;
typedef struct dict dict;

struct dict
{
    int key;
    string value;
};

extern dict diks[MAX_SIZE];
extern pthread_mutex_t locks[MAX_SIZE];
extern pthread_mutex_t dict_lock;
extern int dict_locked;
extern int thread_pool_size;
extern pthread_t *thread_pool;

string concat(int key1, int key2);
string insert(int key, string value);
int check_key(int key);
string delet(int key);
string fetch(int key);
string update(int key, string value);

#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

#include "globals.h"
#include "myqueue.h"
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

int thread_pool_size;
pthread_t *thread_pool;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;

void *thread_function(void *arg);
void *handle_connection(void *arg);

queue<int *> thread_q;

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;

void *thread_function(void *arg)
{
    while (1)
    {
        int *pclient;
        pthread_mutex_lock(&m);
        while (thread_q.empty())
        {
            pthread_cond_wait(&c, &m);
        }
        pclient = thread_q.front();
        thread_q.pop();
        pthread_mutex_unlock(&m);
        handle_connection(pclient);

        /*if ((pclient = dequeue()) == NULL)
        {
            pthread_cond_wait(&c, &m);
            pclient = dequeue();
        }*/
    }
}

///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *arg)
{
    int client_socket_fd = *(int *)arg;
    free(arg);
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        //cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = cmd;
        vector<string> tokens;
        stringstream check1(cmd);
        string intermediate;
        while (getline(check1, intermediate, ' '))
        {
            tokens.push_back(intermediate);
        }
        if (tokens[0] == string("insert"))
        {
            int key;
            sscanf(tokens[1].c_str(), "%d", &key);
            msg_to_send_back = insert(key, tokens[2]);
        }
        else if (tokens[0] == string("delete"))
        {
            int key;
            sscanf(tokens[1].c_str(), "%d", &key);
            msg_to_send_back = delet(key);
        }
        else if (tokens[0] == string("update"))
        {
            int key;
            sscanf(tokens[1].c_str(), "%d", &key);
            msg_to_send_back = update(key, tokens[2]);
        }
        else if (tokens[0] == string("concat"))
        {
            int key, key2;
            sscanf(tokens[1].c_str(), "%d", &key);
            sscanf(tokens[2].c_str(), "%d", &key2);

            msg_to_send_back = concat(key, key2);
        }
        else if (tokens[0] == string("fetch"))
        {
            int key;
            sscanf(tokens[1].c_str(), "%d", &key);
            msg_to_send_back = fetch(key);
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        sleep(2);

        msg_to_send_back = string(":") + to_string(pthread_self()) + string(":") + msg_to_send_back;
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        else
        {
            cout << msg_to_send_back << endl;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    //printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}

int main(int argc, char *argv[])
{
    for (int i = 0; i < MAX_SIZE; i++)
    {
        diks[i].key = -1;
        locks[i] = PTHREAD_MUTEX_INITIALIZER;
    }
    if (argc > 1)
    {
        sscanf(argv[1], "%d", &thread_pool_size);
    }
    else
        thread_pool_size = 1;

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    thread_pool = (pthread_t *)malloc(sizeof(pthread_t) * thread_pool_size);
    for (int l = 0; l < thread_pool_size; l++)
    {
        pthread_create(&thread_pool[l], NULL, thread_function, NULL);
    }
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, thread_pool_size);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        //printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        //printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        //pthread_t t;
        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;
        pthread_mutex_lock(&m);
        //enqueue(pclient);
        thread_q.push(pclient);
        pthread_cond_signal(&c);
        pthread_mutex_unlock(&m);

        //pthread_create(&t, NULL, handle_connection, pclient);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/dictionary.cpp ####################//

#include "globals.h"

dict diks[MAX_SIZE];
pthread_mutex_t locks[MAX_SIZE];
pthread_mutex_t dict_lock;
int dict_locked;

string insert(int key, string value)
{
    int cond = 0;
    pthread_mutex_lock(&locks[key]);
    if (diks[key].key == -1)
    {
        diks[key].key = key;
        diks[key].value = value;
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key]);
    if (cond == 1)
    {
        return string("Insertion successful");
    }
    else
        return string("Key already exists");
}

string concat(int key1, int key2)
{
    int cond = 0;
    string val1, val2;
    pthread_mutex_lock(&locks[key1]);
    if (diks[key1].key != -1)
    {

        val1 = diks[key1].value;
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key1]);
    if (cond == 0)
    {
        return string("Concat failed as at least one of the keys does not exist");
    }
    cond = 0;
    pthread_mutex_lock(&locks[key2]);
    if (diks[key2].key != -1)
    {

        val2 = diks[key2].value;
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key2]);
    if (cond == 0)
    {
        return string("Concat failed as at least one of the keys does not exist");
    }
    else if (cond == 1)
    {
        diks[key1].value = val1 + val2;
        diks[key2].value = val2 + val1;
    }
    return diks[key2].value;
}

string update(int key, string value)
{
    int cond = 0;
    pthread_mutex_lock(&locks[key]);
    if (diks[key].key != -1)
    {
        diks[key].value = value;
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key]);
    if (cond == 1)
    {
        return diks[key].value;
    }
    else
        return string("Key does not exist");
}

string delet(int key)
{
    int cond = 0;
    pthread_mutex_lock(&locks[key]);
    if (diks[key].key != -1)
    {
        diks[key].key = -1;
        diks[key].value.clear();
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key]);
    if (cond == 1)
    {
        return string("Deletion successful");
    }
    else
        return string("No such key exists");
}

string fetch(int key)
{
    int cond = 0;
    pthread_mutex_lock(&locks[key]);
    if (diks[key].key != -1)
    {
        cond = 1;
    }
    pthread_mutex_unlock(&locks[key]);
    if (cond == 1)
    {
        return diks[key].value;
    }
    else
        return string("Key does not exist");
}

//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
typedef struct argu argu;
pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;
struct argu
{
    int time;
    int id;
    char cmd[100];
};

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void begin_process(int request_n, string to_send)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    //cout << "Connection to server successful" << endl;

    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&print_lock);
    cout << request_n << output_msg << endl;
    pthread_mutex_unlock(&print_lock);
    close(socket_fd);

    // part;
}

void *init_thread(void *arg)
{
    argu argument = *(argu *)arg;
    sleep(argument.time);
    begin_process(argument.id, string(argument.cmd));
    //pthread_exit(NULL);
    return NULL;
}

int main(int argc, char *argv[])
{
    int m;
    scanf("%d", &m);
    int time[m];
    string command[m];
    pthread_t t[m];
    string temp;

    getline(cin, temp);
    argu *arg = (argu *)malloc(sizeof(argu));

    for (int i = 0; i < m; i++)
    {
        getline(cin, temp);

        vector<string> tokens;
        stringstream check1(temp);
        string intermediate;
        while (getline(check1, intermediate, ' '))
        {
            tokens.push_back(intermediate);
        }
        sscanf(tokens[0].c_str(), "%d", &time[i]);
        for (int j = 1; j < tokens.size(); j++)
        {
            command[i] += tokens[j];
            if (j != tokens.size() - 1)
            {
                command[i] += " ";
            }
        }

        arg->time = time[i];
        arg->id = i;
        strcpy(arg->cmd, command[i].c_str());
        pthread_create(&t[i], NULL, init_thread, arg);
        //init_thread(arg);
    }
    
    for (int i = 0; i < m; i++)
    {
        pthread_join(t[i], NULL);
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/myqueue.cpp ####################//

#include "myqueue.h"

node *head = NULL;
node *tail = NULL;

void enqueue(int *client_socket)
{
    node *newnode = (node *)malloc(sizeof(node));
    newnode->client_socket = client_socket;
    newnode->next = NULL;
    if (tail == NULL)
    {
        head = newnode;
    }
    else
    {
        tail->next = newnode;
    }
    tail = newnode;
}

int *dequeue()
{
    if (head == NULL)
        return NULL;
    else
    {
        int *result = head->client_socket;
        node *temp = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
        }
        free(temp);
        return result;
    }
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q3/myqueue.h ####################//

#include <stdio.h>
#include <stdlib.h>

#ifndef MYQUEUE_H
#define MYQUEUE_H
typedef struct node node;

struct node
{
    node *next;
    int *client_socket;
};

int *dequeue();
void enqueue(int *client_socket);
#endif