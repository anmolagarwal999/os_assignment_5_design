
//###########FILE CHANGE ./main_folder/Manav Chaudhary_305829_assignsubmission_file_/2021121003/q3/q3_client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

typedef struct insert_args
{
	int req_type;	//0 for insert
	int key;
	char value[256];
	unsigned int time;
} i_args;

typedef struct delete_args
{
	int req_type;	//1 for delete
	int key;
	unsigned int time;
} d_args;

typedef struct update_args
{
	int req_type;	//2 for update
	int key;
	char value[256];
	unsigned int time;
} u_args;

typedef struct concat_args
{
	int req_type;	//3 for concat
	int key1;
	int key2;
	unsigned int time;
} c_args;

typedef struct fetch_args
{
	int req_type;	//4 for fetch
	int key;
	unsigned int time;
} f_args;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);	//TCP
    if(socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if(connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////
void *client_request(void *inp)
{
	sleep(((struct goal_args*)inp)->time);	//Time in sec after which the request to connect to the server is to be made
	
	struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
//    cout << "Connection to server successful" << endl;
	
	if(((struct goal_args*)inp)->req_type==0)	//insert
	{
		
	}
	else if(((struct goal_args*)inp)->req_type==1)	//delete
	{
		
	}
	else if(((struct goal_args*)inp)->req_type==2)	//update
	{
		
	}
	else if(((struct goal_args*)inp)->req_type==3)	//concat
	{
		
	}
	else if(((struct goal_args*)inp)->req_type==4)	//fetch
	{
		
	}
	
	
    while(true)
    {
        string to_send;
        cout << "Enter msg: ";
        getline(cin, to_send);
        send_string_on_socket(socket_fd, to_send);
        int num_bytes_read;
        string output_msg;
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        cout << "Received: " << output_msg << endl;
        cout << "====" << endl;
    }
	
}

int main(int argc, char *argv[])
{
    int m, i;
    scanf("%d", &m);	//number of user requests 
    pthread_t client_tid[m];
    for(i = 0;i<m;i++)
    {
    	int temp_time = 0;
    	string str;
    	cin>>time>>str;
    	if(strcmp(str, "insert")==0)
    	{
    		i_args obj;
    		obj.time = temp_time;
    		obj.req_type = 0;
    		cin>>obj.key;
    		cin>>obj.value;
    		pthread_create(&client_tid[i],NULL,client_request,&obj);	
		}
		else if(strcmp(str, "delete")==0)
    	{
    		d_args obj;
    		obj.time = temp_time;
    		obj.req_type = 1;
    		cin>>obj.key;
    		pthread_create(&client_tid[i],NULL,client_request,&obj);
		}
		else if(strcmp(str, "update")==0)
    	{
    		u_args obj;
    		obj.time = temp_time;
    		obj.req_type = 2;
    		cin>>obj.key;
    		cin>>obj.value;
    		pthread_create(&client_tid[i],NULL,client_request,&obj);
		}
		else if(strcmp(str, "concat")==0)
    	{
    		c_args obj;
    		obj.time = temp_time;
    		obj.req_type = 3;
    		cin>>obj.key1;
    		cin>>obj.key2;
    		pthread_create(&client_tid[i],NULL,client_request,&obj);
		}
		if(strcmp(str, "fetch")==0)
    	{
    		f_args obj;
    		obj.time = temp_time;
    		obj.req_type = 4;
    		cin>>obj.key;
    		pthread_create(&client_tid[i],NULL,client_request,&obj);
		}
	}
	for(i = 0;i<m;i++)
	{
		pthread_join(client_tid[i], NULL);
	}
	
    return 0;
}

//resources
//https://www.gta.ufrj.br/ensino/eel878/sockets/sockaddr_inman.html
//multithreaded server dictionary in c
//https://www.youtube.com/watch?v=Pg_4Jz8ZIH4

//###########FILE CHANGE ./main_folder/Manav Chaudhary_305829_assignsubmission_file_/2021121003/q3/q3_server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part printf("-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = "Ack: " + cmd;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

int main(int argc, char *argv[])
{
	int worker_threads_num = 0;
	scanf("%d", &worker_threads_num);
    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if(bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}
