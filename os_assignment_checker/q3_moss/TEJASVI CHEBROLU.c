
//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q3/src/all.h ####################//

#ifndef _ALL_
#define _ALL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define USE_PORT 1337
#define USE_BUFFER_SIZE 256
#define USE_BACKLOG 5
#define EXIT_ONE 1
#define EXIT_ZERO 0
#define USE_REQUESTS 256
#define USE_ARGS 256

using namespace std;
#define USE_DICT_SIZE 101
extern pthread_mutex_t my_dict_mutex[USE_DICT_SIZE];
extern vector<string> dict;

#endif

//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q3/src/commands.h ####################//

#ifndef _COMMANDS_
#include "all.h"
string perform_commands(char *buffer);
#define _COMMANDS_
#endif
//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q3/src/server.cpp ####################//

#include "all.h"
vector<string> dict(USE_DICT_SIZE, "");
#include "commands.h"

pthread_mutex_t my_dict_mutex[USE_DICT_SIZE];

vector<pthread_t> thread_pool;
pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;
queue<pair<int, int *>> q;
pthread_cond_t my_condition = PTHREAD_COND_INITIALIZER;

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(EXIT_ONE);
    }
    return;
}

void *perform_connection(void *ptr_client_socket, int request_index)
{
    char user_input[USE_BUFFER_SIZE];
    int client_socket = *((int *)ptr_client_socket);
    bzero(user_input, USE_BUFFER_SIZE);
    int b_size = USE_BUFFER_SIZE - 1;

    check(read(client_socket, user_input, b_size), "cant read from sock ;-;\n");
    user_input[b_size] = '\0';

    string response = perform_commands(user_input);
    int bytes;
    char *c_response = strdup(response.c_str());
    bytes = strlen(c_response);
    check(write(client_socket, c_response, bytes), "y u dont listen client :/\n");

    pid_t tid = gettid();
    bytes = 0;
    cout << request_index << ":" << tid << ":" << c_response << flush;
    int ch_size;
    close(client_socket);

    return NULL;
}

void *perform_thread(void *arg)
{
    int bytes;
    while (true)
    {
        int request_index = -1;
        int *client_socket = NULL;

        pthread_mutex_lock(&my_mutex);
        if (q.empty())
        {
            pthread_cond_wait(&my_condition, &my_mutex);
        }
        if (!q.empty())
        {
            // pair<int, int *> data;
            // data = q.front();
            client_socket = q.front().second;
            request_index = q.front().first;
            q.pop();
        }
        pthread_mutex_unlock(&my_mutex);

        if (client_socket)
            perform_connection(client_socket, request_index);
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    bool arg_check = argc != 2;
    if (arg_check)
    {
        printf("need one param to specify #client threads");
        exit(EXIT_ONE);
    }

    struct sockaddr_in server_addr, client_addr;
    int num_clients = atoi(argv[1]);
    int sockfd;
    thread_pool.resize(num_clients);
    int client_socket;
    for (int i = 0; i < num_clients; i++)
        pthread_create(&thread_pool[i], NULL, perform_thread, NULL);

    socklen_t client_len = sizeof(client_addr);
    int enable = 1;
    check(sockfd = socket(AF_INET, SOCK_STREAM, 0), "cant open sock");
    int int_size = sizeof(int);
    check(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, int_size), "cant reuse addrs");
    int server_address_size = sizeof(server_addr);
    bzero((char *)&server_addr, server_address_size);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(USE_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    check(bind(sockfd, (struct sockaddr *)&server_addr, server_address_size), "cant bind :0");
    int i = 0;
    check(listen(sockfd, USE_BACKLOG), "cant listen :/");
    while (true)
    {

        int *client_on_heap = (int *)malloc(sizeof(int));
        check(client_socket = accept(sockfd, (struct sockaddr *)&client_addr, &client_len), "can't accept :pensive:");
        *client_on_heap = client_socket;
        int bytes = 0;
        pthread_mutex_lock(&my_mutex);
        bytes += i;
        q.push(make_pair(i++, client_on_heap));
        bytes -= server_address_size;
        pthread_cond_signal(&my_condition);
        pthread_mutex_unlock(&my_mutex);
    }

    close(sockfd);

    return 0;
}

//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q3/src/client.cpp ####################//

#include "all.h"

void check_code(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(EXIT_ONE);
    }
    return;
}

void *perform_connections(void *input_ptr)
{
    int socket_fd;
    string input = *((string *)input_ptr);
    char rbuff[USE_BUFFER_SIZE];
    struct sockaddr_in server_addr;

    check_code((socket_fd = socket(AF_INET, SOCK_STREAM, EXIT_ZERO)), "cant create sock\n");
    int server_address_size = sizeof(server_addr);
    bzero((char *)&server_addr, server_address_size);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(USE_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    check_code(connect(socket_fd, (struct sockaddr *)&server_addr, server_address_size), "cant connect to server\n");
    int c_len = strlen(input.c_str());
    send(socket_fd, input.c_str(), c_len, 0);
    check_code(recv(socket_fd, rbuff, USE_BUFFER_SIZE, 0) - 1, "can't recieve\n");
    int b_size = sizeof(rbuff);
    fputs(rbuff, stdout);
    bzero((char *)&rbuff, b_size);

    close(socket_fd);

    return NULL;
}

int main()
{
    string temp;
    int last_time = 0;
    getline(cin, temp);
    vector<vector<string>> inputs(USE_REQUESTS);
    int m = stoi(temp);
    for (int i = 0; i < m; i++)
    {
        string temp, time;
        getline(cin, temp);
        int t_l = temp.length() - EXIT_ONE;
        time = temp.substr(0, temp.find(' '));
        string remaining = temp.substr(temp.find(' ') + EXIT_ONE, t_l);
        int time_int = stoi(time);
        inputs[time_int].push_back(remaining);
        last_time = max(last_time, stoi(time));
    }

    for (int i = 0; i <= last_time; i++)
    {
        int i_len = inputs[i].size();
        for (int j = 0; j < i_len; j++)
        {
            pthread_t thread;
            pthread_create(&thread, NULL, perform_connections, &inputs[i][j]);
        }
        sleep(EXIT_ONE);
    }

    return EXIT_ZERO;
}

//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q3/src/commands.cpp ####################//

#include "commands.h"
#define INF 1e9
#include "all.h"

string perform_insert(char *args[USE_ARGS])
{
    bool arg_nums = !(args[0] && args[1] && args[2]);
    if (arg_nums)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    int dic_len = dict[key].length();
    string value = args[2];

    if (dic_len)
        return "Key already exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return "Insertion Successful\n";
}

string test()
{
    return "testing 123!\n";
}

string perform_fetch(char *args[USE_ARGS])
{
    bool arg_nums = !(args[0] && args[1]);
    if (arg_nums)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool k_len_check = !dict[key].length();
    if (k_len_check)
        return "Key does not exist\n";
    else
        return dict[key] + "\n";
}

string perform_update(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1] && args[2]);
    if (arg_len)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool n_key_len = !dict[key].length();
    string value = args[2];

    if (n_key_len)
        return "Key dones not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    key_len = !key_len;
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return value + "\n";
}

string perform_concat(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1] && args[2]);
    if (arg_len)
        return "Invalid #args\n";

    int key1 = atoi(args[1]);
    bool key1_len = !(0 <= key1 && key1 <= 100);
    if (key1_len)
        return "Invalid key1\n";
    int key2 = atoi(args[2]);
    bool key2_len = !(0 <= key2 && key2 <= 100);
    if (key2_len)
        return "Invalid key2\n";

    string temp1 = dict[key1];
    string temp2 = dict[key2];

    bool t_lens = !(temp1.length() and temp2.length());
    if (t_lens)
        return "Concat failed as at least one of the keys does not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key1]);
    int index = key1;
    pthread_mutex_lock(&my_dict_mutex[key2]);
    dict[index] = temp1 + temp2;
    index = key2;
    dict[index] = temp2 + temp1;
    pthread_mutex_unlock(&my_dict_mutex[key2]);
    key1_len = !key1_len;
    pthread_mutex_unlock(&my_dict_mutex[key1]);

    return dict[key2] + "\n";
}

string perform_delete(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1]);
    if (arg_len)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool n_key_len = !dict[key].length();
    if (n_key_len)
        return "No such key exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    key_len = !key_len;
    dict[key] = "";
    pthread_mutex_unlock(&my_dict_mutex[key]);
    return "Deletion successful\n";
}

string perform_commands(char *user_input)
{

    char **args = (char **)malloc(USE_ARGS * sizeof(char *));
    int i = 0;

    args[i] = strtok(user_input, " \t");
    string response = "\n";
    while (args[i])
        args[++i] = strtok(NULL, " \t");

    if (!strncmp(args[0], "test", 4))
        return test();
    else if (!strncmp(args[0], "insert", 7))
        return perform_insert(args);
    else if (!strncmp(args[0], "fetch", 6))
        return perform_fetch(args);
    else if (!strncmp(args[0], "delete", 6))
        return perform_delete(args);
    else if (!strncmp(args[0], "update", 6))
        return perform_update(args);
    else if (!strncmp(args[0], "concat", 6))
        return perform_concat(args);
    return response;
}
