
//###########FILE CHANGE ./main_folder/TANISHQ CHAUDHARY_305912_assignsubmission_file_/2019114007/q3/headers/all.h ####################//

#ifndef _ALL_
#define _ALL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define PORT 1337
#define BUFFER_SIZE 256
#define MAX_DICT_SIZE 101
#define MAX_STRING_SIZE 256
#define BACKLOG 5
#define MAX_RESPONSE_SIZE 256
#define MAX_REQUESTS 256

using namespace std;

extern vector<string> dict;
extern pthread_mutex_t my_dict_mutex[MAX_DICT_SIZE];

#endif

//###########FILE CHANGE ./main_folder/TANISHQ CHAUDHARY_305912_assignsubmission_file_/2019114007/q3/headers/commands.h ####################//

#ifndef _COMMANDS_
#define _COMMANDS_

#include "all.h"

#define MAX_ARGS 256

using namespace std;

string handle_commands(char *buffer);

#endif

//###########FILE CHANGE ./main_folder/TANISHQ CHAUDHARY_305912_assignsubmission_file_/2019114007/q3/src/server.cpp ####################//

#include "all.h"
#include "commands.h"

using namespace std;

// char dict[MAX_DICT_SIZE][MAX_STRING_SIZE];
vector<string> dict(MAX_DICT_SIZE, "");
pthread_mutex_t my_dict_mutex[MAX_DICT_SIZE];

queue<pair<int, int *>> q;
vector<pthread_t> thread_pool;
pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t my_condition = PTHREAD_COND_INITIALIZER;

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(1);
    }
    return;
}

void *handle_connection(void *ptr_client_socket, int request_index)
{
    int client_socket = *((int *)ptr_client_socket);

    // steup buffer
    char user_input[BUFFER_SIZE];
    int bytes;
    bzero(user_input, BUFFER_SIZE);

    // read data from client
    check(read(client_socket, user_input, BUFFER_SIZE - 1), "cant read from sock ;-;\n");
    user_input[BUFFER_SIZE - 1] = '\0'; // null terminating it, just in case

    // test();
    string response = handle_commands(user_input);
    char *c_response = strdup(response.c_str());

    // // print data from client
    // printf("Here is the message: %s\n", buffer);

    // write ack to client
    check(write(client_socket, c_response, strlen(c_response)), "y u dont listen client :/\n");

    pid_t tid = gettid();
    cout << request_index << ":" << tid << ":" << c_response << flush;

    // close connection
    close(client_socket);

    return NULL;
}

void *handle_thread(void *arg)
{
    while (true)
    {
        int *client_socket = NULL;
        int request_index = -1;

        // PROTECC THE QUEUE OPERATIONS
        pthread_mutex_lock(&my_mutex);
        if (q.empty())
        {
            pthread_cond_wait(&my_condition, &my_mutex);
        }
        // NOT SURE IF I SHOULD JUST `else` OR PUT ANOTHER IF
        // WILL GO WITH ANOTHER IF, SINCE SAFER XD
        if (!q.empty())
        {
            pair<int, int *> data;
            data = q.front(); //get the first element
            request_index = data.first;
            client_socket = data.second;

            // cout << "pre yeet\n";
            q.pop(); // YEET
            // cout << q.size() << "\tpost yeet\n";
        }
        pthread_mutex_unlock(&my_mutex);

        // cout << "off to connection\n";
        if (client_socket)
            handle_connection(client_socket, request_index);
        // cout << "connection done\n";
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    //////////////////////// THREAD HANDLING //////////////////////////////////
    // handle args
    if (argc != 2)
    {
        printf("need one param to specify #client threads");
        exit(1);
    }
    int num_clients = atoi(argv[1]);
    thread_pool.resize(num_clients);
    for (int i = 0; i < num_clients; i++)
        pthread_create(&thread_pool[i], NULL, handle_thread, NULL);
    ///////////////////////////////////////////////////////////////////////////

    //////////////////////// SOCKET HANDLING //////////////////////////////////
    // setup data structures and shit
    int sockfd, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_len = sizeof(client_addr);
    // setup socket
    check(sockfd = socket(AF_INET, SOCK_STREAM, 0), "cant open sock");
    // setup reuse address option for socket
    int enable = 1;
    check(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)), "cant reuse addrs");
    // server addr struct setup
    bzero((char *)&server_addr, sizeof(server_addr)); // for resetting server_addr
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);
    // bind the socket fd with server address
    check(bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)), "cant bind :0");
    // listen!
    check(listen(sockfd, BACKLOG), "cant listen :/");
    ///////////////////////////////////////////////////////////////////////////
    // cout << "yo1" << '\n';
    int i = 0;
    while (true)
    {
        // accept connection request from client
        // cout << "yo2" << '\n';

        check(client_socket = accept(sockfd, (struct sockaddr *)&client_addr, &client_len), "can't accept :pensive:");

        // DUMB METHOD: handle connection from client
        // handle_connection(client_socket); // for one thread only

        // LESSER DUMB METHOD: A THREAD A CLIENT
        // pthread_t t;
        // pthread_create(&t, NULL, handle_connection, &client_socket);

        // NON-DUMB METHOD
        // cout << "yo3" << '\n';
        int *client_on_heap = (int *)malloc(sizeof(int));
        *client_on_heap = client_socket;

        pthread_mutex_lock(&my_mutex);
        q.push(make_pair(i++, client_on_heap));
        pthread_cond_signal(&my_condition);
        pthread_mutex_unlock(&my_mutex);
    }

    // close server socket
    close(sockfd);

    return 0;
}

//###########FILE CHANGE ./main_folder/TANISHQ CHAUDHARY_305912_assignsubmission_file_/2019114007/q3/src/client.cpp ####################//

#include "all.h"

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(1);
    }
    return;
}

void *handle_connect(void *input_ptr)
{
    string input = *((string *)input_ptr);

    int sockfd;
    struct sockaddr_in server_addr;
    char rbuff[BUFFER_SIZE];

    check((sockfd = socket(AF_INET, SOCK_STREAM, 0)), "cant create sock\n");

    bzero((char *)&server_addr, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    check(connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)), "cant connect to server\n");
    send(sockfd, input.c_str(), strlen(input.c_str()), 0);
    check(recv(sockfd, rbuff, BUFFER_SIZE, 0) - 1, "can't recevie\n");
    fputs(rbuff, stdout);
    bzero((char *)&rbuff, sizeof(rbuff));

    close(sockfd);

    return NULL;
}

int main()
{
    ///////////////////////////// HANDLING ALL THE INPUTS /////////////////////
    string temp;
    getline(cin, temp);
    int m = stoi(temp);
    int last_time = 0;
    vector<vector<string>> inputs(MAX_REQUESTS);
    for (int i = 0; i < m; i++)
    {
        string temp;
        getline(cin, temp);
        string time = temp.substr(0, temp.find(' '));
        string remaining = temp.substr(temp.find(' ') + 1, temp.length() - 1);

        inputs[stoi(time)].push_back(remaining);
        last_time = max(last_time, stoi(time));
    }
    ///////////////////////////// HANDLING ALL THE INPUTS /////////////////////

    for (int i = 0; i <= last_time; i++)
    {
        for (int j = 0; j < inputs[i].size(); j++)
        {
            // handle_connect(inputs[i][j]);
            pthread_t thread;
            pthread_create(&thread, NULL, handle_connect, &inputs[i][j]);
        }
        sleep(1);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/TANISHQ CHAUDHARY_305912_assignsubmission_file_/2019114007/q3/src/commands.cpp ####################//

#include "commands.h"
#include "all.h"

using namespace std;

string test()
{
    return "testing 123!\n";
}

string handle_insert(char *args[MAX_ARGS])
{
    if (!(args[0] && args[1] && args[2]))
        return "Invalid #args\n";

    int key = atoi(args[1]);
    if (!(0 <= key && key <= 100))
        return "Invalid key\n";
    string value = args[2];

    if (dict[key].length())
        return "Key already exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return "Insertion Successful\n";
}

string handle_fetch(char *args[MAX_ARGS])
{
    if (!(args[0] && args[1]))
        return "Invalid #args\n";

    int key = atoi(args[1]);
    if (!(0 <= key && key <= 100))
        return "Invalid key\n";

    if (!dict[key].length())
        return "Key does not exist\n";
    else
        return dict[key] + "\n";
}

string handle_delete(char *args[MAX_ARGS])
{
    if (!(args[0] && args[1]))
        return "Invalid #args\n";

    int key = atoi(args[1]);
    if (!(0 <= key && key <= 100))
        return "Invalid key\n";

    if (!dict[key].length())
        return "No such key exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    dict[key] = "";
    pthread_mutex_unlock(&my_dict_mutex[key]);
    return "Deletion successful\n";
}

string handle_update(char *args[MAX_ARGS])
{
    if (!(args[0] && args[1] && args[2]))
        return "Invalid #args\n";

    int key = atoi(args[1]);
    if (!(0 <= key && key <= 100))
        return "Invalid key\n";
    string value = args[2];

    if (!dict[key].length())
        return "Key dones not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return value + "\n";
}

string handle_concat(char *args[MAX_ARGS])
{
    if (!(args[0] && args[1] && args[2]))
        return "Invalid #args\n";

    int key1 = atoi(args[1]);
    if (!(0 <= key1 && key1 <= 100))
        return "Invalid key1\n";
    int key2 = atoi(args[2]);
    if (!(0 <= key2 && key2 <= 100))
        return "Invalid key2\n";

    string temp1 = dict[key1];
    string temp2 = dict[key2];

    if (!(temp1.length() and temp2.length()))
        return "Concat failed as at least one of the keys does not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key1]);
    pthread_mutex_lock(&my_dict_mutex[key2]);
    // pid_t tid = gettid();
    // cout << "MESSING WITH CONCAT: " << tid << '\n';
    dict[key1] = temp1 + temp2;
    dict[key2] = temp2 + temp1;
    pthread_mutex_unlock(&my_dict_mutex[key2]);
    pthread_mutex_unlock(&my_dict_mutex[key1]);

    return dict[key2] + "\n";
}

string handle_clear()
{
    for (int i = 0; i < MAX_DICT_SIZE; i++)
    {
        pthread_mutex_lock(&my_dict_mutex[i]);
        dict[i] = "";
        pthread_mutex_unlock(&my_dict_mutex[i]);
    }
    return "ALL KEYS PURGED\n";
}

string handle_commands(char *user_input)
{
    // cout << user_input << '\n';

    int i = 0;
    char **args = (char **)malloc(MAX_ARGS * sizeof(char *));

    args[i] = strtok(user_input, " \t");
    while (args[i])
        args[++i] = strtok(NULL, " \t");

    // for (int j = 0; j < i; j++)
    //     cout << "args: " << args[j] << '\n';

    string response = "\n";
    if (!strncmp(args[0], "test", 4))
        response = test();
    else if (!strncmp(args[0], "insert", 7))
        response = handle_insert(args);
    else if (!strncmp(args[0], "fetch", 6))
        response = handle_fetch(args);
    else if (!strncmp(args[0], "delete", 6))
        response = handle_delete(args);
    else if (!strncmp(args[0], "update", 6))
        response = handle_update(args);
    else if (!strncmp(args[0], "concat", 6))
        response = handle_concat(args);
    else if (!strncmp(args[0], "CLEAR", 6))
        response = handle_clear();
    return response;
}
