
//###########FILE CHANGE ./main_folder/Anurag Gupta_305791_assignsubmission_file_/Final/q3/clientme.cpp ####################//

#include "headers.h"
using namespace std;

#define server_port 8001
#define buffer_max_sz 1048576

typedef struct request request;

struct request
{
    int i;
    pthread_t tid;
    int client_fd;
    int req_time;
    string command;
    pthread_mutex_t mutex;
};

vector<request> clients;
pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER;

int get_client_socket_fd()
{
    struct sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = server_port;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    return socket_fd;
}

void *client_thread(void *(arg))
{
    int idx = *(int *)arg;
    clients[idx].client_fd = get_client_socket_fd();
    int client_fd = clients[idx].client_fd;
    int fd_copy = client_fd;
    int fd_client = clients[idx].client_fd;
    int client_copy = fd_client;
    int req_time = clients[idx].req_time;
    int required_time = req_time;
    string command = clients[idx].command;
    string command_copy = command;
    sleep(req_time);
    required_time = req_time;
    pthread_mutex_lock(&clients[idx].mutex);
    int x = write(client_fd, command.c_str(), command.length());
    if (x <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }
    pthread_mutex_unlock(&clients[idx].mutex);
    pthread_mutex_lock(&clients[idx].mutex);

    string buffer;
    buffer.resize(buffer_max_sz);
    int byte_read = read(client_fd, &buffer[0], buffer_max_sz - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);
    if (byte_read <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }
    pthread_mutex_lock(&terminal);
    cout << clients[idx].i << " : " << gettid() << " : " << buffer << endl;
    pthread_mutex_unlock(&terminal);

    pthread_mutex_unlock(&clients[idx].mutex);

    return NULL;
}

int main()
{
    int num_clients;
    cin >> num_clients;
    string x;
    getline(cin, x);
    for (int i = 0; i < num_clients; i++)
    {
        string str;
        string time_req = "";
        getline(cin, str);
        auto itr = str.begin();
        while (*itr != ' ')
        {
            time_req += *itr;
            itr++;
        }
        itr = itr+1;
        request tmp;
        request temp2;
        tmp.req_time = stoi(time_req);
        temp2.req_time = tmp.req_time;
        string command(itr, str.end());
        temp2.command = command;
        tmp.command = command;
        clients.push_back(tmp);
    }
    // get the connection sockets
    int i = 0;
    while(i<num_clients)
    {
         clients[i].i = i;
        clients[i].mutex = PTHREAD_MUTEX_INITIALIZER;
        i++;
    }
    i = 0;
    while(i<num_clients)
    {
        int *idx = new int;
        *idx = i;
        pthread_create(&clients[i].tid, NULL, client_thread, (void *)idx);
        i++;
    }
   
   i = 0;

    while(i<num_clients)
    {
        pthread_join(clients[i].tid, NULL);
        i++;
    }
    return 0;
}
//###########FILE CHANGE ./main_folder/Anurag Gupta_305791_assignsubmission_file_/Final/q3/serverme.cpp ####################//

#include "headers.h"
using namespace std;

int max_clients = 100;
#define port_number 8001
#define buff_sz 1048576
#define MAX_DICT_SIZE 100
int num_worker_threads;
pthread_mutex_t map_lock = PTHREAD_MUTEX_INITIALIZER;
queue<int *> q;
pthread_mutex_t queue_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;

struct dictionary_node
{
    string str;
    int id;
    int is_active;
    pthread_mutex_t mutex;
};
vector<struct dictionary_node> dict(MAX_DICT_SIZE);


int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    pthread_mutex_lock(&map_lock);
    // int idx = user_idx[fd];
    pthread_mutex_unlock(&map_lock);
    // pthread_mutex_lock(&client_lock[idx]);
    int bytes_sent = write(fd, s.c_str(), s.length());
    // pthread_mutex_unlock(&client_lock[idx]);

    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void function_handler(string s, int client_socket_fd)
{
    char *cpy_in = (char *)malloc((s.length() + 1) * sizeof(char));
    strcpy(cpy_in, s.c_str());
    char copy[1000];
    strcpy(copy,s.c_str());
    char copy2[1000];
    strcpy(copy2,s.c_str());
    cpy_in[s.length()] = '\0';
    char *str = strtok_r(cpy_in, " ", &cpy_in);
    int len = s.length();
    int length_of_string = len;
    char *arguments[length_of_string];
    int arg_count = 0;
    while (str != NULL)
    {
        arguments[arg_count] = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(arguments[arg_count], str);
        arg_count = arg_count + 1;
        str = strtok_r(cpy_in, " ", &cpy_in);
    }
    if (!arg_count)
    {
        return;
    }
    arguments[arg_count] = NULL;
    if (!strcmp(arguments[0], "insert"))
    {
        if (arg_count - 3!=0)
        {
            cout << "Invalid Arguments for insert" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (!dict[key].is_active)
        {
            dict[key].str = arguments[2];
            dict[key].is_active = 1;
            cout << "Insertion Succesful" << arguments[2] << " at " << key << endl;
            send_string_on_socket(client_socket_fd, "Insertion Succesful");
        }
        else
        {
            cout << "Key already present" << endl;
            send_string_on_socket(client_socket_fd, "Key already present");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (!strcmp(arguments[0], "concat"))
    {
        if (!(arg_count -3))
        {
            cout << "Invalid Arguments for concat" << endl;
            return;
        }
        int key1 = atoi(arguments[1]);
        int key2 = atoi(arguments[2]);
        pthread_mutex_lock(&dict[key1].mutex);
        pthread_mutex_lock(&dict[key2].mutex);
        if (dict[key1].is_active  && dict[key2].is_active)
        {
            string tmp = dict[key1].str;
            dict[key1].str = dict[key1].str + dict[key2].str;
            dict[key2].str = dict[key2].str + tmp;
            cout << "Concatenated" << endl;
            send_string_on_socket(client_socket_fd, dict[key2].str);
        }
        else
        {
            cout << "Either one of the keys is not present" << endl;
            send_string_on_socket(client_socket_fd, "Concatination Failed as Either one of the keys is not present");
        }
        pthread_mutex_unlock(&dict[key1].mutex);
        pthread_mutex_unlock(&dict[key2].mutex);
    }
    else if (!strcmp(arguments[0], "fetch"))
    {
        if (arg_count -2!=0)
        {
            cout << "Invalid Arguments for fetch" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active)
        {
            cout << "Fetch Succesful" << endl;
            send_string_on_socket(client_socket_fd, dict[key].str);
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (!strcmp(arguments[0], "update"))
    {
        if (arg_count -3!= 0)
        {
            cout << "Invalid Arguments for update" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active)
        {
            dict[key].str = arguments[2];
            cout << "Updated" << endl;
            send_string_on_socket(client_socket_fd, dict[key].str);
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (!strcmp(arguments[0], "delete"))
    {
        if (arg_count -2!= 0)
        {
            cout << "Invalid Arguments for delete" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active)
        {
            dict[key].is_active = 0;
            dict[key].str = "";
            cout << "Deletion Succesful" << endl;
            send_string_on_socket(client_socket_fd, "Deletion Succesful");
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "No such Key");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
}

void handle_client(int client_fd)
{
    string buffer;
    buffer.resize(buff_sz);
    pthread_mutex_lock(&map_lock);
    // int idx = user_idx[client_fd];
    pthread_mutex_unlock(&map_lock);

    // pthread_mutex_lock(&client_lock[idx]);
    int byte_read = read(client_fd, &buffer[0], buff_sz - 1);
    // pthread_mutex_unlock(&client_lock[idx]);

    //cout << "Read Value" << buffer << endl;
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);
    if (byte_read <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        return;
    }
    function_handler(buffer, client_fd);
}
void *worker_thread(void *arg)
{
    // int sockfd = *((int *)arg);
    char buffer[buff_sz];
    int n;
    while (true)
    {
        pthread_mutex_lock(&queue_lock);
        while (q.empty())
        {
            pthread_cond_wait(&cond_var, &queue_lock);
        }
        int *client_sockfd = q.front();
        q.pop();
        pthread_mutex_unlock(&queue_lock);
        handle_client(*client_sockfd);
    }
}
void init_server_socket()
{
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    int server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    int PORT_NUMBER = port_number;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(PORT_NUMBER);

    if (bind(server_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(server_socket_fd, max_clients);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    socklen_t clilen = sizeof(client_addr_obj);

    while (true)
    {
        printf("Waiting for a new client to request for a connection\n");
        int client_socket_fd = accept(server_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        cout << "ok" << endl;
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf("New client connected from port number %d and IP %s \n", ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;

        pthread_mutex_lock(&queue_lock);
        q.push(pclient);
        pthread_mutex_unlock(&queue_lock);
        pthread_cond_signal(&cond_var);
    }

    close(server_socket_fd);
}

int main(int argc, char *argv[])
{
    num_worker_threads = stoi(argv[1]);

    pthread_t wkr_thread[num_worker_threads];
    int itr = 0;
    while(itr  < MAX_DICT_SIZE)
    {
        dict[itr].id = itr;
        dict[itr].is_active = 0;
        pthread_mutex_init(&dict[itr].mutex, NULL);
        itr++;
    }
    int i = 0;
    while(i<num_worker_threads)
    {
        pthread_create(&wkr_thread[i], NULL, worker_thread, NULL);
        i++;
    }
   
    init_server_socket();
}

//###########FILE CHANGE ./main_folder/Anurag Gupta_305791_assignsubmission_file_/Final/q3/headers.h ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include<bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <tuple>
