
//###########FILE CHANGE ./main_folder/Tisha dubey_kwokow/2020101101_assignment_5/q3/Client.c ####################//

#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <ws2tcpip.h>


#include <stdio.h>
#include <string.h>
#include <pthread.h>

#pragma comment(lib,"ws2_32.lib")

int port_number = 2008;
mutex mapMutex;

int main(void)
{
    WSADATA wsaData;
    int err = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (err != 0)
        return 1;

	int socket_desc;
	struct sockaddr_in server_addr;
	char server_message[2000], client_message[2000];

	//Cleaning the Buffers
	memset(server_message,'\0',sizeof(server_message));
	memset(client_message,'\0',sizeof(client_message));

	//Creating Socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);

	if(socket_desc < 0)
	{
		printf("Could Not Create Socket. Error!!!!!\n");
		return -1;
	}

	printf("Socket Created\n");

	//Specifying the IP and Port of the server to connect
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port_number);
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	//------------------------------------------------------------------------------------------------//

    //Now connecting to the server accept() using connect() from client side
	if(connect(socket_desc, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0)
	{
		printf("Connection Failed. Error!!!!!");
		return -1;
	}

	//Receive the connection message back from the server
	if(recv(socket_desc, server_message, sizeof(server_message),0) < 0)
	{
		printf("Receive Failed. Error!!!!!\n");
		return -1;
	}

	printf("Server Message: %s\n\n",server_message);

	//Cleaning the Buffers
	memset(server_message,'\0',sizeof(server_message));
	memset(client_message,'\0',sizeof(client_message));

	//Get Input from the User
	printf("Enter INPUT");
	fgets(client_message, 200, stdin);

	//Send credentials back to Server
	if(send(socket_desc, client_message, strlen(client_message),0) < 0)
	{
		printf("Send Failed. Error!!!!\n");
		return -1;
	}

	//Cleaning the Buffers
	memset(server_message,'\0',sizeof(server_message));
	memset(client_message,'\0',sizeof(client_message));


	//Receive the message back from the server
	if(recv(socket_desc, server_message, sizeof(server_message),0) < 0)
	{
		printf("Receive Failed. Error!!!!!\n");
		return -1;
	}

	printf("Server Message: %s\n\n",server_message);

    while (1)
	{

	}
	//Closing the Socket
	close(socket_desc);
    WSACleanup();
	return 0;
}

//###########FILE CHANGE ./main_folder/Tisha dubey_kwokow/2020101101_assignment_5/q3/Server.h ####################//

#ifndef SERVER_H
#define SERVER_H

#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <ws2tcpip.h>

#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <mutex>
#include <typeinfo>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <iostream>
#include <sstream>
#include <cstdlib>
#pragma comment(lib,"ws2_32.lib")
#include <sys/types.h>
#include <unistd.h>


class Server
{
    public:
        Server();
    protected:
    private:
};

#endif // SERVER_H

//###########FILE CHANGE ./main_folder/Tisha dubey_kwokow/2020101101_assignment_5/q3/Server.cpp ####################//

#include "Server.h"
using namespace std;

map<int, string> myMap;

int port_number = 2008;
// vector<std::mutex> my_mutexes(1000);

mutex *my_mutexes;
mutex m;
long long int threadWorkerId = 140584115967744;

void *communicate (void *message)
{
	int client_sock = (int) message;
	char server_message[2000], client_message[2000];
    // pid_t x = syscall(__NR_gettid); ------ usable for LINUX

	//Cleaning the Buffers
	memset(server_message,'\0',sizeof(server_message));
	memset(client_message,'\0',sizeof(client_message));

	//--------------------------------------------------------------------------//
	// Take name and CNIC from client

	//Receive Name/CNIC from the client
	if (recv(client_sock, client_message, sizeof(client_message),0) < 0)
	{
		printf("Receive Failed. Error!!!!!\n");
	}

    char credentials[255];
	strcpy(credentials, client_message);
    // cout << out[0] << endl;
    //cout << client_message << endl;

    // construct a stream from the string
    stringstream ss(client_message);

    string s;
    vector<string> out;
    char delim = ' ';
    while (getline(ss, s, delim)) {
        out.push_back(s);
        //cout << s << endl;
    }
    //cout << "DONE" << endl;
    //cout << typeid(out[2]).name() << endl;
    string operation = out[1];
    string key = out[2];
    int newkey = stoi(out[2]);
    string output;
    // cout << typeid(key).name() << endl;
    if(operation == "insert"){
        try{
            if (myMap.find(newkey) != myMap.end()){
                output = "Key already exist";
            }
            else{
                lock_guard<mutex> guard(my_mutexes[newkey]);
                //m.lock();
                //cout << "IM IN INSERT " << endl;
                // lock_guard<mutex> guard(myMutex);
                string word = out[3];
                word.erase(std::remove(word.begin(), word.end(), '\n'), word.end());
                myMap[newkey] = word;
                output = "Insertion successful";
            }
        }
        catch(...){
            output = "Key already exists";
        }

        //m.unlock();
        //cout << "VALUE IS: " << myMap[newkey] << endl;
    }
    if(operation == "delete"){
        lock_guard<mutex> guard(my_mutexes[newkey]);
        if (myMap.find(newkey) == myMap.end() || myMap[newkey] == ""){
            //cout << "�No such key exists" << endl;
            output = "Key does not exist";
        }
        else{
            myMap[newkey] = -1;
            output = "Deletion successful";
        }
    }
    if(operation == "update"){
        lock_guard<mutex> guard(my_mutexes[newkey]);
        if (myMap.find(newkey) == myMap.end()){
            cout << "�No such key exists" << endl;
        }
        else{
            myMap[newkey] = out[3];
            output = myMap[newkey];
            //cout << myMap[newkey] << endl;
        }
    }
    if(operation == "concat"){
         int newkey2 = stoi(out[3]);
        try{
            if(myMap.find(newkey) == myMap.end() || myMap.find(newkey2) == myMap.end()){
                output = "Concat failed as at least one of the keys does not exist";
            }
            else{
                std::unique_lock<std::mutex> lk1(my_mutexes[newkey], std::defer_lock);
                std::unique_lock<std::mutex> lk2(my_mutexes[newkey2], std::defer_lock);
                std::lock(lk1, lk2);
                //lock_guard<mutex> guard(my_mutexes[newkey]);
                //lock_guard<mutex> guard(my_mutexes[newkey2]);
                //cout << "HERE IN CONCAT" << endl;
                string prevWord = myMap[newkey];
                myMap[newkey] = myMap[newkey] + myMap[newkey2];
                myMap[newkey2] = myMap[newkey2] + prevWord;
                output = myMap[newkey2];
            }
        }
        catch(...){
            output = "Concat failed as at least one of the keys does not exist";
        }

        //cout << myMap[newkey] << endl;
        //cout << myMap[newkey2] << endl;
    }
    if(operation == "fetch"){
        //cout << "KEY IS: " << key << endl;
        if(myMap.find(newkey) == myMap.end() || myMap[newkey] == ""){
                output = "Key does not exist";
            //cout << "FOUND" << endl;
        }
        else{
            output = myMap[newkey];
            //cout << "NOT FOUND" << endl;
        }
     
    }

    strcpy(server_message, myMap[newkey].c_str());
    if (send(client_sock, server_message, strlen(server_message),0)<0)
    {
        printf("Send Failed. Error!!!!!\n");
    }
    cout << out[0] << ":" << threadWorkerId << ":" << output << endl;
	close(client_sock);
	pthread_exit(NULL);
}

int main(void)
{
    WSADATA wsaData;
    int err = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (err != 0)
        return 1;

	int socket_desc, client_sock, client_size;
	struct sockaddr_in server_addr, client_addr;
	char server_message[2000], client_message[2000];

	//Cleaning the Buffers
	memset(server_message,'\0',sizeof(server_message));
	memset(client_message,'\0',sizeof(client_message));

	//Creating Socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);

	if(socket_desc < 0)
	{
		printf("Could Not Create Socket. Error!!!!!\n");
		return -1;
	}

	//printf("Socket Created\n");

	//Binding IP and Port to socket
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port_number);
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if(bind(socket_desc, (struct sockaddr*)&server_addr, sizeof(server_addr))<0)
	{
			printf("Bind Failed. Error!!!!!\n");
			return -1;
	}

	//printf("Bind Done\n");

    my_mutexes = new mutex[101];
    //for(int i=0;i<1000;++i) my_mutexes[i] = new std::mutex();

	//Put the socket into Listening State
	if(listen(socket_desc, 1) < 0)
	{
			printf("Listening Failed. Error!!!!!\n");
			return -1;
	}

	//printf("Listening for Incoming Connections.....\n");

	//---------------------------------------------------------------------------//
	while (1)
	{
	    threadWorkerId += 1;
		//Accept the incoming Connections
		client_size = sizeof(client_addr);
		client_sock = accept(socket_desc, (struct sockaddr*)&client_addr, &client_size);

		if (client_sock < 0)
		{
			printf("Accept Failed. Error!!!!!!\n");
			return -1;
		}

		//printf("Client Connected with IP: %s and Port No: %i\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));

		strcpy(client_message, "Connection Established!");

		// Send the connection message back to client
		if (send(client_sock, client_message, strlen(client_message),0)<0)
		{
			printf("Send Failed. Error!!!!!\n");
			return -1;
		}

		//Cleaning the Buffers
		memset(server_message,'\0',sizeof(server_message));
		memset(client_message,'\0',sizeof(client_message));

		pthread_t thread1;

		//create new thread for each client
		pthread_create(&thread1,NULL,communicate,(void*)client_sock);
	}

    //for(int i=0;i<1000;++i) delete my_mutexes[i]
	//Closing the Socket
	close(socket_desc);
	WSACleanup();
	return 0;
}
