
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/server_simulation/main.cpp ####################//

#include "server_sim.h"

map<int, string> dictionary;
queue<client> q;
int MAX_CLIENTS;

pthread_mutex_t q_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t q_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t dict_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t dict_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t read_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t write_cond = PTHREAD_COND_INITIALIZER;

int main(int argc, char *argv[])
{
    int num_worker_threads = atoi(argv[1]);
    MAX_CLIENTS = num_worker_threads;
    printf("%s\n", argv[1]);
    cout << num_worker_threads << "\n";
    // make handle threads
    pthread_t worker_tid[num_worker_threads];
    for (int i = 0; i < num_worker_threads; i++)
    {
        pthread_create(&worker_tid[i], NULL, handle_connection, NULL);
        cout << worker_tid[i] << "\n";
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    client client_x;
    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client.
        */
        //accept is a blocking call

        printf("Waiting for a new client to request for a connection\n");
        client_x.client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_x.client_addr_obj, &clilen);
        if (client_x.client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGREEN "New client connected from port number %d and IP %s \n" RESET_COLOR, ntohs(client_x.client_addr_obj.sin_port), inet_ntoa(client_x.client_addr_obj.sin_addr));

        pthread_mutex_lock(&q_lock);
        q.push(client_x);
        pthread_cond_signal(&q_cond);
        pthread_mutex_unlock(&q_lock);
        // sleep(2);
        // handle_connection(client_socket_fd);
        // cout << "client---\n";
    }

    close(wel_socket_fd);
    return 0;
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/server_simulation/server_sim.h ####################//

#ifndef __SERVER_SIM_H__
#define __SERVER_SIM_H__

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <map>
#include <pthread.h>
#include <queue>
#include <ctype.h>
#include <string>

using namespace std;

#define BBLACK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGREEN "\e[1;32m"
#define BYELLOW "\e[1;33m"
#define BBLUE "\e[1;34m"
#define BMAGNETA "\e[1;35m"
#define BCYAN "\e[1;36m"
#define RESET_COLOR "\x1b[0m"

typedef long long ll;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const ll buff_sz = 1048576;
///////////////////////////////////////////////////
typedef struct client
{
    int client_socket_fd;
    struct sockaddr_in client_addr_obj;
} client;

extern map<int, string> dictionary;
extern queue<client> q;
extern pthread_mutex_t q_lock;
extern pthread_cond_t q_cond;
extern pthread_mutex_t dict_lock;
extern pthread_cond_t dict_cond;
extern pthread_mutex_t read_lock;
extern pthread_cond_t read_cond;

extern pthread_mutex_t write_lock;
extern pthread_cond_t write_cond;

///////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes);
int send_string_on_socket(int fd, const string &s);

void *handle_connection(void *arg);
// void handle_connection(int client_socket_fd);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/server_simulation/server_sim.cpp ####################//

#include "server_sim.h"

////////////////////////// START OF TUTORIAL CODE WITH ADDITION OF MUTEX LOCKS ////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    pthread_mutex_lock(&read_lock);
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    pthread_mutex_unlock(&read_lock);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    pthread_mutex_lock(&write_lock);
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }
    pthread_mutex_unlock(&write_lock);
    return bytes_sent;
}

////////////////////////// END OF TUTORIAL CODE ///////////////////////////////

void *handle_connection(void *arg)
{

    int received_num, sent_num, client_socket_fd;

    int ret_val = 1;
    while (true)
    {
        // check for queue empty or not
        pthread_mutex_lock(&q_lock);
        while (q.empty())
        {
            pthread_cond_wait(&q_cond, &q_lock);
        }
        client_socket_fd = q.front().client_socket_fd;
        q.pop();
        pthread_mutex_unlock(&q_lock);

        while (true)
        {
            string cmd;
            tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
            ret_val = received_num;

            // printf("Read something\n");
            if (ret_val <= 0)
            {
                // perror("Error read()");
                printf("Server could not read msg sent from client\n");
                break;
            }
            cout << "Client sent : " << cmd << endl;
            if (cmd == "exit")
            {
                cout << "Exit pressed by client" << endl;
                break;
            }

            // process the command
            string res = "";
            int i = cmd.find(' ');
            string cmd1 = cmd.substr(0, i);
            if (cmd1 == "insert")
            {
                string cmd2 = cmd.substr(i + 1);
                cout << cmd2 << "\n";
                int j = cmd2.find(' ');
                string k = cmd2.substr(0, j);
                string v = cmd2.substr(j + 1);
                int key = stoi(k);
                // cout<<key<<"\n";
                pthread_mutex_lock(&dict_lock);
                if (dictionary.find(key) == dictionary.end())
                {
                    res = "Insertion successful";
                    dictionary[key] = v;
                }
                else
                {
                    res = "Key already exists";
                }
                pthread_mutex_unlock(&dict_lock);
            }
            else if (cmd1 == "delete")
            {
                string cmd2 = cmd.substr(i + 1);
                int key = stoi(cmd2);
                pthread_mutex_lock(&dict_lock);
                if (dictionary.find(key) == dictionary.end())
                {
                    res = "No such key exists";
                }
                else
                {
                    dictionary.erase(key);
                    res = "Deletion successful";
                }
                pthread_mutex_unlock(&dict_lock);
            }
            else if (cmd1 == "update")
            {
                string cmd2 = cmd.substr(i + 1);
                int j = cmd2.find(' ');
                string k = cmd2.substr(0, j);
                string v = cmd2.substr(j + 1);
                int key = stoi(k);

                pthread_mutex_lock(&dict_lock);
                if (dictionary.find(key) == dictionary.end())
                {
                    res = "Key does not exist";
                }
                else
                {
                    res = v;
                    dictionary[key] = v;
                }
                pthread_mutex_unlock(&dict_lock);
            }
            else if (cmd1 == "concat")
            {
                string cmd2 = cmd.substr(i + 1);
                int j = cmd2.find(' ');
                string k1 = cmd2.substr(0, j);
                string k2 = cmd2.substr(j + 1);
                int key1 = stoi(k1);
                int key2 = stoi(k2);
                // printf("concat Keys %d %d\n", key1, key2);
                pthread_mutex_lock(&dict_lock);
                if (dictionary.find(key1) == dictionary.end() || dictionary.find(key2) == dictionary.end())
                {
                    res = "Concat failed as at least one of the keys does not exist";
                }
                else
                {
                    string v1 = dictionary[key1], v2 = dictionary[key2];
                    dictionary[key1] = v1 + v2;
                    dictionary[key2] = v2 + v1;
                    res = dictionary[key2];
                }
                pthread_mutex_unlock(&dict_lock);
            }
            else if (cmd1 == "fetch")
            {
                string cmd2 = cmd.substr(i + 1);
                int key = stoi(cmd2);
                pthread_mutex_lock(&dict_lock);
                if (dictionary.find(key) == dictionary.end())
                {
                    res = "Key does not exist";
                }
                else
                {
                    res = dictionary[key];
                }
                pthread_mutex_unlock(&dict_lock);
            }
            // wait for 2 second
            sleep(2);
            string msg_to_send_back = to_string(pthread_self()) + " " + res;

            ////////////////////////////////////////
            // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
            // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

            int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
            // debug(sent_to_client);
            if (sent_to_client == -1)
            {
                perror("Error while writing to client. Seems socket has been closed");
                break;
            }
        }
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET_COLOR "\n");
        // return NULL;
    }
}


//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/client_simulation/main.cpp ####################//

#include "client_sim.h"
#include "stimer.h"

int stimer = -1;
pthread_mutex_t stimer_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t stimer_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t get_fd_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t get_fd_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t read_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t write_cond = PTHREAD_COND_INITIALIZER;

int main()
{
    // struct sockaddr_in server_obj;
    // SOCKET_FD = get_socket_fd(&server_obj);

    int m;
    string temp;
    cin >> m;
    getline(cin, temp);

    vector<client> client_list(m);
    for (int i = 0; i < m; i++)
    {
        client_list[i].indx = i;
        cin >> client_list[i].time; // time
        getline(cin, client_list[i].msg);
        client_list[i].msg.erase(0, 1); // remove the space at the begining
        // cout << "-" << client_list[i].msg << "-\n";
        pthread_create(&client_list[i].tid, NULL, simulate_client, (void *)&client_list[i]);
        // sleep(3);
    }

    // cout << "Starting the Simulation of clients...\n";
    simulate_timer(client_list[m - 1].time + 2);
    for (int i = 0; i < m; i++)
    {
        pthread_join(client_list[i].tid, NULL);
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/client_simulation/stimer.h ####################//

#ifndef __STIMER_H__
#define __STIMER_H__

#include <iostream>
#include <ctime>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

using namespace std;

extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;

void increament_timer(int arg);
void simulate_timer(int time);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/client_simulation/client_sim.h ####################//

#ifndef __CLIENT_SIM_H__
#define __CLIENT_SIM_H__

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

#include "stimer.h"

using namespace std;

/////////////////////////////

#define BBLACK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGREEN "\e[1;32m"
#define BYELLOW "\e[1;33m"
#define BBLUE "\e[1;34m"
#define BMAGNETA "\e[1;35m"
#define BCYAN "\e[1;36m"
#define RESET_COLOR "\x1b[0m"

typedef long long ll;
const ll MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const ll buff_sz = 1048576;
///////////////////////////////////////////////////

typedef struct client
{
    int indx;
    int time;
    string msg;
    pthread_t tid;
    int socket_fd;
    struct sockaddr_in server_obj;
} client;

extern sem_t read_mutex;
extern sem_t write_mutex;

extern pthread_mutex_t get_fd_lock;
extern pthread_cond_t get_fd_cond;

extern pthread_mutex_t read_lock;
extern pthread_cond_t read_cond;

extern pthread_mutex_t write_lock;
extern pthread_cond_t write_cond;

pair<string, int> read_string_from_socket(int fd, int bytes);
int send_string_on_socket(int fd, const string &s);
// int get_socket_fd(struct sockaddr_in *ptr, int port_num);
int get_socket_fd(struct sockaddr_in *ptr);
void *simulate_client(void *arg);

#endif

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/client_simulation/client_sim.cpp ####################//

#include "client_sim.h"

////////////////////////// START OF TUTORIAL CODE WITH ADDITION OF MUTEX LOCKS ////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    pthread_mutex_lock(&read_lock);
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);
    pthread_mutex_unlock(&read_lock);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{

    pthread_mutex_lock(&write_lock);
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }
    pthread_mutex_unlock(&write_lock);
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    pthread_mutex_lock(&get_fd_lock);
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGREEN "Connected to server\n" RESET_COLOR);
    // part;
    pthread_mutex_unlock(&get_fd_lock);
    return socket_fd;
}
////////////////////////// END OF TUTORIAL CODE ///////////////////////////////

void *simulate_client(void *arg)
{
    client *client_x = (client *)arg;
    pthread_mutex_lock(&stimer_lock);
    while (stimer != client_x->time)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);

    client_x->socket_fd = get_socket_fd(&client_x->server_obj);
    // cout << "Connection by client " << client_x->indx << " to server successful" << endl;
    // cout << "Enter msg: ";

    send_string_on_socket(client_x->socket_fd, client_x->msg);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(client_x->socket_fd, buff_sz);
    cout << BRED << client_x->indx << " : " << RESET_COLOR << BGREEN << output_msg << RESET_COLOR << "\n";
    // part;
    send_string_on_socket(client_x->socket_fd, "exit");
    return NULL;
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q3/client_simulation/stimer.cpp ####################//

#include "stimer.h"

void increament_timer(int arg)
{
    pthread_mutex_lock(&stimer_lock);
    stimer++;
    pthread_cond_broadcast(&stimer_cond);
    pthread_mutex_unlock(&stimer_lock);
    // printf("timer  = %d\n", stimer);
}

void simulate_timer(int time)
{
    signal(SIGALRM, increament_timer);
    while (stimer <= time)
    {
        alarm(1);
        pause();
    }
}
