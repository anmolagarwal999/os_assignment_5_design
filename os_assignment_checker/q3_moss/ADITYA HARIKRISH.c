
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q3/header.h ####################//

#ifndef HEADER_H
#define HEADER_H

#include <arpa/inet.h>
#include <assert.h>
#include <bits/stdc++.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <queue>
#include <tuple>
#include <vector>

#ifndef DEBUG
#define DEBUG 0
#endif

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q3/server.cpp ####################//

#include "utils.h"

#define SERVER_PORT 8001
#define MAX_DICT_SIZE 1000
#define MAX_BUFFER_SIZE 1048576
#define MAX_CLIENT_COUNT 100

pthread_mutex_t map_lock, queue_lock;
std::queue<int *> queue;

struct node {
    std::string String;
    int id;
    bool isOccupied;
    pthread_mutex_t mutex;
};
std::vector<node> dict;
pthread_cond_t cond_var;

int send_string_on_socket(int fd, const std::string &s) {
    int bytes_sent = write(fd, s.c_str(), s.length());

    if (bytes_sent < 0) {
        std::cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void clientHandler(int client_fd) {
    std::string buffer(MAX_BUFFER_SIZE, '.');
    pthread_mutex_lock(&map_lock);
    pthread_mutex_unlock(&map_lock);

    ssize_t readSize = read(client_fd, &buffer[0], MAX_BUFFER_SIZE - 1);

    buffer[readSize] = '\0';
    buffer.resize(readSize);
    if (readSize <= 0) {
        std::cerr << "Failed To communicate with the server\n";
        return;
    }
    // function_handler(buffer, client_fd);
    char *duplicateBuffer = (char *)malloc((buffer.length() + 1) * sizeof(char));
    strcpy(duplicateBuffer, buffer.c_str());
    duplicateBuffer[buffer.length()] = '\0';
    char *String = strtok_r(duplicateBuffer, " ", &duplicateBuffer);
    int len = buffer.length();
    char *arguments[len];
    int argCount = 0;
    while (String != NULL) {
        arguments[argCount] = (char *)malloc((strlen(String) + 1) * sizeof(char));
        strcpy(arguments[argCount], String);
        String = strtok_r(duplicateBuffer, " ", &duplicateBuffer);
        ++argCount;
    }
    if (argCount == 0)
        return;
    arguments[argCount] = NULL;
    if (strcmp(arguments[0], "insert") == 0) {
        if (argCount != 3) {
            std::cerr << "Invalid number of arguments for insert!\n";
            return;
        }
        int key = atoi(arguments[1]);

        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].isOccupied == false) {
            dict[key].isOccupied = true;
            dict[key].String = arguments[2];
#if DEBUG >= 1
            std::cout << "Inserted " << arguments[2] << " at " << key << "\n";
#endif
            send_string_on_socket(client_fd, "Insertion successful");
        } else {
#if DEBUG >= 1
            std::cout << "Key already exists\n";
#endif
            send_string_on_socket(client_fd, "Key already exists");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    } else if (strcmp(arguments[0], "concat") == 0) {
        if (argCount != 3) {
            std::cout << "Invalid Arguments for concat\n";
            return;
        }
        int key1 = atoi(arguments[1]);
        int key2 = atoi(arguments[2]);
        pthread_mutex_lock(&dict[key1].mutex);
        pthread_mutex_lock(&dict[key2].mutex);
        if (dict[key1].isOccupied == true && dict[key2].isOccupied == true) {
            std::string tmp = dict[key1].String;
            dict[key1].String = dict[key1].String + dict[key2].String;
            dict[key2].String += tmp;
#if DEBUG >= 1
            std::cout << "Concatenation successful\n";
#endif
            send_string_on_socket(client_fd, dict[key2].String);
        } else {
#if DEBUG >= 1
            std::cout << "Either one of the keys is not present\n";
#endif
            send_string_on_socket(client_fd, "Concat failed as at least one of the keys " MAGENTA "does" RESET " not exist");
        }
        pthread_mutex_unlock(&dict[key1].mutex);
        pthread_mutex_unlock(&dict[key2].mutex);
    } else if (strcmp(arguments[0], "fetch") == 0) {
        if (argCount != 2) {
            std::cout << "Invalid Arguments for fetch\n";
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].isOccupied == true) {
            std::cout << "Fetch successful\n";
            send_string_on_socket(client_fd, dict[key].String);
        } else {
            std::cout << "Key doesn't exist\n";
            send_string_on_socket(client_fd, "Key doesn't exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    } else if (strcmp(arguments[0], "update") == 0) {
        if (argCount != 3) {
            std::cerr << "Invalid Arguments for update\n";
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].isOccupied == true) {
            dict[key].String = arguments[2];
#if DEBUG >= 1
            std::cout << "Update successful\n";
#endif
            send_string_on_socket(client_fd, dict[key].String);
        } else {
            std::cout << "Key doesn't exist\n";
            send_string_on_socket(client_fd, "Key doesn't exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    } else if (strcmp(arguments[0], "delete") == 0) {
        if (argCount != 2) {
            std::cerr << "Invalid number of arguments for delete\n";
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].isOccupied == true) {
            dict[key].isOccupied = false;
            dict[key].String.clear();
#if DEBUG >= 1
            std::cout << "Deletion successful\n";
#endif
            send_string_on_socket(client_fd, "Deletion successful");
        } else {
#if DEBUG >= 1
            std::cout << "Key doesn't exist\n";
#endif
            send_string_on_socket(client_fd, "No such key exists");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
}
void *threadFunc(void *arg) {
    int n;
    while (1) {
        pthread_mutex_lock(&queue_lock);
        while (queue.empty()) {
            pthread_cond_wait(&cond_var, &queue_lock);
        }
        int *client_sockfd = queue.front();
        queue.pop();
        pthread_mutex_unlock(&queue_lock);
        clientHandler(*client_sockfd);
    }
}
void socketInit() {
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    int server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd < 0) {
        perror("ERROR creating welcoming socket");
        exit(EXIT_FAILURE);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    int PORT_NUMBER = SERVER_PORT;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(PORT_NUMBER);

    if (bind(server_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0) {
        perror("Error on bind on welcome socket: ");
        exit(EXIT_FAILURE);
    }

    listen(server_socket_fd, MAX_CLIENT_COUNT);
    std::cout << "Server has started listening on the LISTEN PORT\n";
    socklen_t clientLength = sizeof(client_addr_obj);

    for (;;) {
        printf("Waiting for a new client to request for a connection\n");
        int client_socket_fd = accept(server_socket_fd, (struct sockaddr *)&client_addr_obj, &clientLength);
        if (client_socket_fd < 0) {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(EXIT_FAILURE);
        }

        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;

        pthread_mutex_lock(&queue_lock);
        queue.push(pclient);
        pthread_mutex_unlock(&queue_lock);
        pthread_cond_signal(&cond_var);
    }

    close(server_socket_fd);
}

int main(int argc, char *argv[]) {
    pthread_mutex_init(&map_lock, NULL);
    pthread_mutex_init(&queue_lock, NULL);
    pthread_cond_init(&cond_var, NULL);

    int numThreads = std::stoi(argv[1]);

    pthread_t *thread = (pthread_t *)malloc(sizeof(pthread_t) * numThreads);
    after_malloc_check(thread);

    dict.resize(MAX_DICT_SIZE);

    for (auto itr = 0; itr < MAX_DICT_SIZE; itr++) {
        dict[itr].id = itr;
        dict[itr].isOccupied = false;
        pthread_mutex_init(&dict[itr].mutex, NULL);
    }

    for (auto i = 0; i < numThreads; i++) {
        pthread_create(&thread[i], NULL, threadFunc, NULL);
    }
    socketInit();

    free(thread);
    return 0;
}

//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q3/utils.h ####################//

#ifndef UTILS_H
#define UTILS_H

#include "header.h"

#ifndef DEBUG
#define DEBUG 0
#endif

// Defining colours
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BRIGHT_GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define BRIGHT_BLUE "\x1b[94m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

// Some macros
#define PthreadCreate(newthread, attr, funcPtr, arg)          \
    if (pthread_create(newthread, attr, funcPtr, arg) != 0) { \
        perror("pthread_create() error");                     \
        exit(1);                                              \
    }

#define PthreadCondInit(cond, attr)            \
    if (pthread_cond_init(&cond, NULL) != 0) { \
        perror("pthread_cond_init() error");   \
        exit(1);                               \
    }

#define after_malloc_check(x)                  \
    if (x == NULL) {                           \
        perror("Failed to allocate memory\n"); \
        exit(EXIT_FAILURE);                    \
    }

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q3/client.cpp ####################//

#include "utils.h"

#define SERVER_PORT 8001
#define MAX_BUFFER_SIZE 10000

struct thread_details {
    int id;
};

struct Request {
    pthread_t tid;
    int64_t timeOfRequest;
    int i, client_fd;
    std::string command;
    pthread_mutex_t mutex;
};

std::vector<Request> client;
pthread_mutex_t stdoutLock;

int get_client_socket_fd() {
    sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        perror("Error in socket creation for CLIENT");
        exit(EXIT_FAILURE);
    }
    int port_num = SERVER_PORT;
    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);
    if (connect(socket_fd, (sockaddr *)&server_obj, sizeof(server_obj)) < 0) {
        perror("Problem in connecting to the server");
        exit(EXIT_FAILURE);
    }
    return socket_fd;
}

void *clientThread(void *(arg)) {
    int id = ((thread_details *)arg)->id;
    client[id].client_fd = get_client_socket_fd();
    std::string command = client[id].command;
    sleep(client[id].timeOfRequest);
    pthread_mutex_lock(&client[id].mutex);
    ssize_t writeSize = write(client[id].client_fd, command.c_str(), command.length());

    if (writeSize < 0) {
        std::cerr << "Failed To communicate with the server\n";
        pthread_mutex_unlock(&client[id].mutex);
        return NULL;
    }
    pthread_mutex_unlock(&client[id].mutex);

    std::string buffer(MAX_BUFFER_SIZE, '.');
    int byte_read = read(client[id].client_fd, &buffer[0], MAX_BUFFER_SIZE - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);

    pthread_mutex_lock(&client[id].mutex);
    if (byte_read <= 0) {
        std::cerr << "Failed To communicate with the server\n";
        pthread_mutex_unlock(&client[id].mutex);
        return NULL;
    }
    pthread_mutex_lock(&stdoutLock);
    std::cout << client[id].i << " : " << gettid() << " : " << buffer << std::endl;
    pthread_mutex_unlock(&stdoutLock);

    pthread_mutex_unlock(&client[id].mutex);

    return NULL;
}

int main() {
    pthread_mutex_init(&stdoutLock, NULL);

    int num_clients;
    std::cin >> num_clients;
    std::string inputLine;
    getline(std::cin, inputLine);
    for (int i = 0; i < num_clients; i++) {
        std::string str, time_req;
        str.clear();
        time_req.clear();
        getline(std::cin, str);
        std::string::iterator itr = str.begin();

        for (itr = str.begin(); *itr != ' '; ++itr) {
            time_req += *itr;
        }

        itr++;
        Request temp;
        temp.timeOfRequest = stoi(time_req);
        std::string command(itr, str.end());
        temp.command = command;
        client.push_back(temp);
    }
    for (int i = 0; i < num_clients; i++) {
        client[i].i = i;
        pthread_mutex_init(&(client[i].mutex), NULL);
    }
    for (int i = 0; i < num_clients; i++) {
        thread_details *temp = new thread_details;
        temp->id = i;
        pthread_create(&client[i].tid, NULL, clientThread, (void *)temp);
    }

    for (int i = 0; i < num_clients; i++) {
        pthread_join(client[i].tid, NULL);
    }

    return 0;
}