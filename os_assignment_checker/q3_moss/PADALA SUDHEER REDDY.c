
//###########FILE CHANGE ./main_folder/PADALA SUDHEER REDDY_305892_assignsubmission_file_/OS-Assignement-5/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <map>
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;
pthread_t thread_pool[100];
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
map<int, string> dict;
int x=0,m=0;

typedef struct queue
{
    struct queue *next;
    int *client_socket;
} queue;

queue *head = NULL;
queue *tail = NULL;

const LL buff_sz = 1048576;

void insert_into_queue(int *client_socket)
{
    queue *newnode = (queue *)malloc(sizeof(queue));
    newnode->client_socket = client_socket;
    newnode->next = NULL;
    if (tail == NULL)
    {
        head = newnode;
    }
    else
    {
        tail->next = newnode;
    }
    tail = newnode;
}

int *delete_from_queue()
{
    if (head == NULL)
    {
        return NULL;
    }
    else
    {
        int *result = head->client_socket;
        queue *temp = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
        }
        free(temp);
        return result;
    }
}
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *client_socket_fd)
{
    x++;
    int client_socket = *((int *)client_socket_fd);
    int received_num, sent_num;
    int ret_val = 1;
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket, buff_sz);
    ret_val = received_num;
    if (ret_val <= 0)
    {
        printf("Server could not read msg sent from client\n");
        goto close_client_socket_ceremony;
    }
    pthread_mutex_unlock(&mutex1);
    if (cmd[1] == 'i')
    {
        int key = cmd[7] - 48;
        string r = cmd.substr(8, cmd.length() - 8);
        auto itr = dict.find(key);
        string msg_to_send_back;
        if (itr != dict.end())
        {
            msg_to_send_back = "if";
        }
        else
        {
           // cout << key << " " << r << " " << endl;
            dict[key] = r;
            msg_to_send_back = "insert";
        }
        pthread_mutex_unlock(&mutex1);
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket, msg_to_send_back);
    }
    if (cmd[1] == 'd')
    {
       // printf("Hello\n");
       int key = cmd[7]-48;
       // printf("%d\n",key);
        auto itr = dict.find(key);
        string msg_to_send_back;
        if (itr == dict.end())
        {
            msg_to_send_back = "df";
        }
        else
        {
            dict.erase(key);
            msg_to_send_back = "delete";
        }
        pthread_mutex_unlock(&mutex1);
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket, msg_to_send_back);
    }
    if(cmd[1] == 'u')
    {
        int key = cmd[7]-48;
        auto itr = dict.find(key);
        string r = cmd.substr(8, cmd.length() - 8);
        string msg_to_send_back;
        if (itr == dict.end())
        {
            msg_to_send_back = "u";
            msg_to_send_back += "\n";
        }
        else
        {
             dict[key] = r;
             msg_to_send_back = dict[key];
            
        }
        pthread_mutex_unlock(&mutex1);
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket, msg_to_send_back);
    }

    if(cmd[1] == 'f')
    {
        int key = cmd[5]-48;
        auto itr = dict.find(key);
        string msg_to_send_back;
        if (itr == dict.end())
        {
            msg_to_send_back = "ff";
        }
        else
        {
             msg_to_send_back = key;
 
        }
        pthread_mutex_unlock(&mutex1);
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket, msg_to_send_back);
    }
    if(cmd[1] == 'c')
    {
        int key1 = cmd[7]-48;
        int key2 = cmd[8]-48;
        auto itr1 = dict.find(key1);
        auto itr2 = dict.find(key2);
        string msg_to_send_back;
        if (itr1 == dict.end() || itr2 == dict.end())
        {
            msg_to_send_back = "c";
            msg_to_send_back += "\n";
        }
        else
        {
             string s = dict[key1];
             dict[key1] = dict[key1] + dict[key2];
             dict[key2] = dict[key2] + s;
             msg_to_send_back = dict[key2];
            
        }
        pthread_mutex_unlock(&mutex1);
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket, msg_to_send_back);
    }

close_client_socket_ceremony:
    if(x==m)
    {
    close(client_socket);
    }
  //  printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}

void *func(void *args)
{
    while (true)
    {
        pthread_mutex_lock(&mutex1);
        int *request_client = delete_from_queue();
        pthread_mutex_unlock(&mutex1);
        if (request_client != NULL)
        {
            handle_connection(request_client);
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    //cout<<argc<<endl;
    n = atoi(argv[1]);
    m = n;
    //printf("%d\n",n);
    for (int i = 0; i < n; i++)
    {
        pthread_create(&thread_pool[i], NULL, func, NULL);
    }
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        int *client_req;
        client_req = (int *)malloc(sizeof(int));
        *client_req = client_socket_fd;
        pthread_mutex_lock(&mutex1);
        //printf("Hi\n");
        insert_into_queue(client_req);
        pthread_mutex_unlock(&mutex1);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/PADALA SUDHEER REDDY_305892_assignsubmission_file_/OS-Assignement-5/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef struct Client
{
    int id;
    string command;
    pthread_t Client_thread_id;
} Client;

pthread_mutex_t mutex;
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */
    // printf("hey bro\n");
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    // printf("wht bro\n");
    //
    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    Client *C = (Client *)args;
     string comm = C->command;
     int time = comm[0]-48;
     sleep(time);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
  //  cout << "Connection succesful for the thread with thread id " << C->id << endl;
    sleep(1);
    pthread_mutex_lock(&mutex);
    int x = send_string_on_socket(socket_fd, C->command);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    string s=C->command;
    if (output_msg[0] == 'i')
    {
        if (output_msg[1] == 'f')
        {
            printf("%d:%ld:key already exists on the server\n", C->id, C->Client_thread_id);
        }
        else
        {
            printf("%d:%ld:Insertion successful\n", C->id, C->Client_thread_id);
        }
    }
    if (s[1] == 'd')
    {
        if (output_msg[1] == 'f')
        {
            printf("%d:%ld:No such key exists\n", C->id, C->Client_thread_id);
        }
        else
        {
            printf("%d:%ld:Deletion successful\n", C->id, C->Client_thread_id);
        }
    }
    if (s[1] == 'u')
    {
        if (output_msg[1] == '\n')
        {
            printf("%d:%ld:Key does not exist”\n", C->id, C->Client_thread_id);
        }
        else
        {
            printf("%d:%ld:", C->id, C->Client_thread_id);
            cout<<output_msg<<endl;
        }
    }
    if (s[1] == 'c')
    {
        if (output_msg[1] == '\n')
        {
            printf("%d:%ld:Concat failed as at least one of the keys does not exist”\n", C->id, C->Client_thread_id);
        }
        else
        {
            printf("%d:%ld:", C->id, C->Client_thread_id);
            cout<<output_msg<<endl;
        }
    }
    if (s[0] == 'f')
    {
        if (output_msg[1] == 'f')
        {
            printf("%d:%ld:Key does not exist”\n", C->id, C->Client_thread_id);
        }
        else
        {
            printf("%d:%ld:", C->id, C->Client_thread_id);
            cout<<output_msg<<endl;
        }
    }

    pthread_mutex_unlock(&mutex);
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    int m;
    cin >> m;
    Client user_request[1000];
    pthread_mutex_init(&mutex, NULL);
    for (int i = 0; i < m; i++)
    {
        user_request[i].id = i;
        string time, function;
        cin >> time >> function;
        user_request[i].command = user_request[i].command + time;
        user_request[i].command = user_request[i].command + function;
        if (function == "insert" || function == "update" || function == "concat")
        {
            string key, value;
            cin >> key >> value;
            user_request[i].command = user_request[i].command + key + value;
        }
        if (function == "delete" || function == "fetch")
        {
            string key;
            cin>>key;
            user_request[i].command = user_request[i].command + key;
        }
       // cout<<user_request[i].command<<endl;
    }
    for (int i = 0; i < m; i++)
    {
        pthread_create(&(user_request[i].Client_thread_id), NULL, begin_process, &user_request[i]);
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(user_request[i].Client_thread_id, NULL);
    }
    return 0;
}