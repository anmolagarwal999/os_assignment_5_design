
//###########FILE CHANGE ./main_folder/Polakampalli Namrath_305814_assignsubmission_file_/2020101063_assignment_5/q3/q3_client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <bits/stdc++.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
int m; 
pthread_t threads[1000];

pthread_mutex_t mutex_arr;
///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

typedef struct Client
{
    int id;
    string command;
    pthread_t Client_thread_id;
} Client_Details;

Client_Details commands[100];

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void* begin_process( void* arg)
{
     Client_Details *C = (Client_Details *)arg;
     string cmd = C->command;
     int time = cmd[0]-48;
     int i = C->id;
     sleep(time);
    //sleep
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);


    cout << "Connection to server successful" << endl;
    
    //while (true)
        pthread_mutex_lock(&mutex_arr);
        string to_send;
        to_send = C->command;
        send_string_on_socket(socket_fd, to_send);
        //cout<<"Done send on "<<i<<endl;
        pthread_mutex_unlock(&mutex_arr);
        
        //cout<<output_msg<<endl;
        //cout << "====" << endl;
    
    // part;
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&mutex_arr,NULL);
    int i, j, k, t, n;
    //INPUT
    cin>>m;
    
    for(int i=0;i<m;i++){
       string time,cmd;
       cin>>time>>cmd;
       if(cmd=="insert" || cmd=="update" || cmd=="concat"){
           string a,b; cin>>a>>b;
           string final = time +" "+ cmd+" " + a +" "+ b;
           commands[i].command=final;
           commands[i].id=i;
       }
       else{
           string a; cin>>a;
           string final = time +" "+ cmd+ " " + a;
           commands[i].command = final;
           commands[i].id=i;
       }
    }
    for(int i=0;i<m;i++){
        pthread_create(&threads[i],NULL,begin_process,&commands[i]);
    }
     for(int i=0;i<m;i++){
        pthread_join(threads[i],NULL);
    }
    //begin_process();
    return 0;
}

//###########FILE CHANGE ./main_folder/Polakampalli Namrath_305814_assignsubmission_file_/2020101063_assignment_5/q3/q3_serv.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include<semaphore.h>
/////////////////////////////
#include <bits/stdc++.h>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;
vector<pair<string, string>> dictionary;
map<int,string> dict;
queue<int*> q;
sem_t semaph[100];
pthread_mutex_t lock_val;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

string evaluate(const string &my_string)
{
    string return_msg_from_eval;
    stringstream ss(my_string); //convert my_string into string stream

    vector<string> tokens;
    string temp_str;

    while (getline(ss, temp_str, ','))
    { //use comma as delim for cutting string
        tokens.push_back(temp_str);
    }
    //all tokens in the vector
    string key = tokens[2];
    string value = tokens[3];
    //cout<<"key is "<<key<<" value is"<<value<<endl;
    if (tokens[0] == "insert")
    {
        bool found = false;
        for (int i = 0; i < dictionary.size(); i++)
        {
            if (dictionary[i].first == key)
            {
                return_msg_from_eval = "Key already exists";
                found = true;
                break;
            }
            else
            {
                return_msg_from_eval = "Insertion Successful";
            }
        }
        if (!found)
        {
            dictionary.pb(make_pair(key, value));
            if(dictionary.size()==0) return_msg_from_eval="Insertion Successful";
            //we inserted successfully
        }
    }
    else if (tokens[0] == "delete")
    {
        bool found = false;
        int index = 0;
        
         if(dictionary.size()==0) return_msg_from_eval="Key doesn't exist";

        for (int i = 0; i < dictionary.size(); i++)
        {
            if (dictionary[i].first == key)
            {
                return_msg_from_eval = "Deletion Successful";
                found = true;
                index = i;
                break;
            }
            else
            {
                return_msg_from_eval = "Key doesn't exist";
            }
        }
        if (found)
        {
            dictionary.erase(dictionary.begin() + index); //delete the position on dict
        }
    }
    else if (tokens[0] == "update")
    {
        bool found = false;
        int index = 0;
         if(dictionary.size()==0) return_msg_from_eval="Key doesn't exist";

        for (int i = 0; i < dictionary.size(); i++)
        {
            if (dictionary[i].first == key)
            {
                return_msg_from_eval = value;
                found = true;
                index = i;
                break;
            }
            else
            {
                return_msg_from_eval = "Key doesn't exist";
            }
        }
        if (found)
        {
            dictionary[index].second = value;
        }
    }
    else if (tokens[0] == "concat")
    {

    }
    else if(tokens[0]=="fetch"){
        bool found=false;
        int index=0;

        if(dictionary.size()==0) return_msg_from_eval="Key doesn't exist";

        for (int i = 0; i < dictionary.size(); i++)
        {
            if (dictionary[i].first == key)
            {
                return_msg_from_eval = value;
                found = true;
                index = i;
                break;
            }
            else
            {
                return_msg_from_eval = "Key doesn't exist";
            }
        }
        if(found){
            return_msg_from_eval=dictionary[index].second;
        }
    }
    return return_msg_from_eval;
}

void handle_connection(void* arg)
{
    int received_num, sent_num;
    int client_socket_fd = *((int*)arg);
    /* read message from client */
    int ret_val = 1;

    //while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        //cout<<"cmd is "<<cmd<<endl;
        //cmd has string input from client
        //1 insert 1 hello
        string ret = evaluate(cmd);
        //-------------


        //--------------
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
  
        string msg_to_send_back = ret;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

       // int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        //if (sent_to_client == -1)
        //{
          //  perror("Error while writing to client. Seems socket has been closed");
            //goto close_client_socket_ceremony;
        //}
        cout<<msg_to_send_back<<endl;
    }

    close_client_socket_ceremony:
    //sleep(2);
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *worker_function(void *arg)
{
    int i = *((int*)(arg));
    sleep(1);
    while(1){
        if(!q.empty()){
            //lovk
            pthread_mutex_lock(&lock_val);
            if(q.empty()) goto HERE;
            printf("Not empty!\n");
            int* fd = q.front();
            q.pop();
            pthread_mutex_unlock(&lock_val);
            //unlock
            //cout<<"Reached handle"<<endl;
            printf("Reached handle\n");
            handle_connection(fd);
        }
        else{ //queue empty
        HERE:
        //printf("queue empty!\n");
            sem_wait(&semaph[i]);
            //return NULL;
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int i, j, k, t, n;

    printf("%s",argv[1]);
    int thread_count= atoi(argv[1]);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    pthread_t worker_threads[100];
    pthread_mutex_init(&lock_val,NULL);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    for (int i = 0; i < thread_count; i++)
    {
        pthread_create(&worker_threads[i], NULL, worker_function, ((void*)&i));
    }
    
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port


    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    //pthread create worker thread
    // for (int i = 0; i < thread_count; i++)
    // {
    //     pthread_join(worker_threads[i], NULL);
    // }

    while (1)
    {
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        int *ptr;
        ptr = (int *)malloc(sizeof(int));
        *ptr = client_socket_fd;
        pthread_mutex_lock(&lock_val);
        q.push(ptr); //ADD TO QUEUE
        pthread_mutex_unlock(&lock_val);
        for(int j=0;j<thread_count;j++)
        sem_post(&semaph[j]);
        //sem_post(&semaph); //tell to not wait everytime there is a new request
        //handle_connection(client_socket_fd);
        //maintain a queue of client_socket_fd's
    }
    //sleep(2);
    close(wel_socket_fd);
    return 0;
}
//worker above while loop
// queue up client )socket _fd
// queu pop everytime
