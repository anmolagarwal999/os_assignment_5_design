
//###########FILE CHANGE ./main_folder/KONDURU NITHIN_305970_assignsubmission_file_/q3/Nserver.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <queue>

/////////////////////////////

#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;

/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

long long int glo = 0;

struct name
{
    int roll_no;
    string peru;
};

struct name abcd[1000];

queue<int> threadpool;

const int initial_msg_len = 256;

pthread_mutex_t pushingmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t poppingmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t holdingjob = PTHREAD_COND_INITIALIZER;
pthread_mutex_t structmutex = PTHREAD_MUTEX_INITIALIZER;



////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

int iteratore(int gg)
{
    int f = 0;
    int x = 0;
    int i;
    pthread_mutex_lock(&structmutex);
    for (i = 0; i < glo; i++)
    {
        if (abcd[i].roll_no == gg)
        {
            pthread_mutex_unlock(&structmutex);
            return i;
        }
    }
    pthread_mutex_unlock(&structmutex);

    return -2;
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        char timesnow[1000]={'\0'};
        char hais[1000][1000]={'\0'};

        for (int e = 0; e < cmd.length(); e++)
        {
            timesnow[e] = cmd[e];
        }
        char *tokenize = strtok(timesnow, " ");
        int df = 0;
        while (tokenize != NULL)
        {
            strcpy(hais[df], tokenize);
            //printf("\n%s\n",abc[gru]);
            tokenize = strtok(NULL, " ");
            df++;
        }
        // cout << "waiting :" << endl;
        // cout << hais[0] << endl;
        // cout << hais[1] << endl;
        // cout << hais[2] << endl;
        string msg_to_send_back;

        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        if (strcmp(hais[0], "insert") == 0)
        {

            if (df == 3)
            {
                int bb = atoi(hais[1]);
                int re = iteratore(bb);
                if (re == -2)
                {
                    pthread_mutex_lock(&structmutex);
                    abcd[glo].roll_no = bb;
                    string temp = "";
                    for (int z = 0; z < strlen(hais[2]); z++)
                    {
                        temp = temp + hais[2][z];
                    }
                    abcd[glo].peru = temp;
                    glo++;

                    //commander.push_back({bb, hais[2]});
                    pthread_mutex_unlock(&structmutex);
                    msg_to_send_back = "Insertion successful";
                }
                else
                {
                    msg_to_send_back = "Key already exists";
                }
            }
            else
            {
                msg_to_send_back = "Wrong command";
            }
        }
        else if (strcmp(hais[0], "update") == 0)
        {
            if (df == 3)
            {

                int bb = atoi(hais[1]);
                int re = iteratore(bb);

                if (re == -2)
                {

                    msg_to_send_back = "No such key exists";
                }
                else
                {

                    pthread_mutex_lock(&structmutex);
                    // cout<<commander[bb].second<<endl;
                    //  cout<<"hi"<<endl;
                    //commander[bb].second = "deccan";
                    string temp = "";
                    for (int z = 0; z < strlen(hais[2]); z++)
                    {
                        temp = temp + hais[2][z];
                    }
                    abcd[re].peru = temp;

                    // for(int bg=0;bg<strlen(hais[2]);bg++){
                    //     commander[bb].second= commander[bb].second+hais[2][bg];

                    // }
                    pthread_mutex_unlock(&structmutex);

                    msg_to_send_back = temp;
                }
            }
            else
            {
                msg_to_send_back = "Wrong command";
            }
        }
        else if (strcmp(hais[0], "delete") == 0)
        {
            if (df == 2)
            {
                int bb = atoi(hais[1]);
                int re = iteratore(bb);
                if (re == -2)
                {

                    msg_to_send_back = "No such key exists";
                }
                else
                {
                    pthread_mutex_lock(&structmutex);
                    // commander.erase(commander.begin() + re);
                    abcd[re].roll_no = -90;
                    abcd[re].peru = " ";
                    pthread_mutex_unlock(&structmutex);
                    msg_to_send_back = "Deletion successful";
                }
            }
            else
            {
                msg_to_send_back = "Wrong command";
            }
        }
        else if (strcmp(hais[0], "concat") == 0)
        {
            if (df == 3)
            {
                int bb = atoi(hais[1]);
                int rr = atoi(hais[2]);
                int re = iteratore(bb);
                int ff = iteratore(rr);
                if (re == -2)
                {

                    msg_to_send_back = "Concat failed as at least one of the keys does not exist";
                }
                else if (ff == -2)
                {

                    msg_to_send_back = "Concat failed as at least one of the keys does not exist";
                }
                else
                {
                    pthread_mutex_lock(&structmutex);

                    string merge1 = abcd[re].peru + abcd[ff].peru;

                    string merge2 = abcd[ff].peru + abcd[re].peru;

                    //cout<<"Output is : "<< merge1 <<endl;
                    abcd[re].peru = merge1;
                    abcd[ff].peru = merge2;

                    pthread_mutex_unlock(&structmutex);

                    msg_to_send_back = abcd[ff].peru;
                }
            }
            else
            {
                msg_to_send_back = "Wrong command";
            }
        }
        else if(strcmp(hais[0],"fetch")==0){
            if(df==2){
                int bb=atoi(hais[1]);
                int re=iteratore(bb);
                if(re==-2){
                    msg_to_send_back="No such key exists";


                }
                else{
                    msg_to_send_back=abcd[re].peru;
                }

            }
            else{
                msg_to_send_back="Wrong command";
            }
        }

        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        break;
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *threadfunc(void *arg)
{

    pthread_mutex_lock(&poppingmutex);
    while (1)
    {
        if (threadpool.empty())
        {
            pthread_cond_wait(&holdingjob, &poppingmutex);
        }
        else if (!threadpool.empty())
        {
            int hu = -1;

            hu = threadpool.front();

            //pthread_mutex_lock(&poppingmutex);
            threadpool.pop();
            pthread_mutex_unlock(&poppingmutex);
            handle_connection(hu);

            //
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    int i;

    for(int i=0;i<1000;i++){
        abcd[i].peru="";
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    int hehe;
    int threaders = atoi(argv[1]);
    pthread_t threads[threaders];
    for (i = 0; i < threaders; i++)
    {
        pthread_t curr_tid;

        hehe = pthread_create(&curr_tid, NULL, threadfunc, NULL);

        if (hehe)
        {

            cout << "Sorry ,error in creating the thread" << endl;
        }
    }

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        else if (client_socket_fd >= 0)
        {

            pthread_mutex_lock(&pushingmutex);
            threadpool.push(client_socket_fd);

            pthread_mutex_unlock(&pushingmutex);
            pthread_cond_signal(&holdingjob);
            printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        }
        //handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/KONDURU NITHIN_305970_assignsubmission_file_/q3/Nclient.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <thread>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

typedef struct nithin *point;

struct nithin
{
    int id;
    int time;
    char command[500];
};

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *gro)
{
    int idi = ((point)gro)->id;
    int timei = ((point)gro)->time;
    char data[1000];
    strcpy(data, ((point)gro)->command);
    // cout << "ID is:" << idi<< endl;
    // cout << "TIme is:" << timei << endl;
    // cout << "data is:" << data<< endl;

    string data1=" ";

    for(int i=0;i<strlen(data);i++){
        data1=data1+data[i];
    }
    // cout << "data :"<<data1<<endl;

    sleep(timei);

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    //cout << "Connection to server successful" << endl;

    send_string_on_socket(socket_fd, data1);

    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    std::thread::id xa = std::this_thread::get_id();
    cout << idi <<":"<< xa << ":Received:" << output_msg << endl;
    

    
    return NULL;
}

int main(int argc, char *argv[])
{

    int n, m;
    cin >> m;
    char array[1000][1000] = {'\0'};

    string dummy;
    getline(cin, dummy);

    pthread_t uthreads[m];
    for (int i = 0; i < m; i++)
    {

        fgets(array[i], 1000, stdin);
        array[i][strlen(array[i]) - 1] = '\0';
    }

    for (int i = 0; i < m; i++)
    {
        char abc[1000][1000];
        int gru = 0;
        //cout<<array[i]<<endl;
        char dek[1000];
        strcpy(dek, array[i]);
        char *tokenize = strtok(array[i], " ");

        while (tokenize != NULL)
        {
            strcpy(abc[gru], tokenize);
            //printf("\n%s\n",abc[gru]);
            tokenize = strtok(NULL, " ");
            gru++;
        }
        // for(int l=0;l<gru;l++){
        //     cout<<abc[l]<<endl;
        // }
        char hellk[1000] = {'\0'};
        int sals = 0;
        int qwe = -1;

        for (int vv = 0; vv < strlen(dek); vv++)
        {
            if (dek[vv] == ' ' && qwe == -1)
            {
                qwe = 1;
                continue;
            }
            if (qwe == 1)
            {

                hellk[sals] = dek[vv];
                sals++;
            }
        }

        point welc = (point)malloc(sizeof(point));
        pthread_t curid;
        welc->id = i;
        welc->time = atoi(abc[0]);
        strcpy(welc->command, hellk);

        pthread_create(&curid, NULL, begin_process, (void *)(welc));
        uthreads[i] = curid;
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(uthreads[i], NULL);
    }

    return 0;
}