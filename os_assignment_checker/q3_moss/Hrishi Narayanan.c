
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q3/server_side.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <thread>
#include <sstream>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;

//////////////////////////////////

typedef struct locks_type 
{
    pthread_mutex_t client_socket;
    pthread_mutex_t dictionary[MAX_CLIENTS+1];
} locks_type;

locks_type locks;

typedef struct status_type 
{
    queue <int> client_sockets;
    string dictionary[MAX_CLIENTS+1];
} status_type;

status_type status;

sem_t requests;

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd, int thread_id)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            break;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            break;
        }

        string msg_to_send_back;

        vector <string> pieces;

        stringstream stream(cmd);
        string str;

        while (getline(stream, str, ' '))
            pieces.push_back(str);

        int id = stoi(pieces[1]);

        if (pieces[0] == "insert")
        {   
            pthread_mutex_lock(&locks.dictionary[id]);

            if (status.dictionary[id].empty())
            {
                status.dictionary[id] = pieces[2];
                msg_to_send_back = "Insertion sucessfull";
            }

            else
                msg_to_send_back = "Key already exists";

            pthread_mutex_unlock(&locks.dictionary[id]);
        }

        else if (pieces[0] == "delete")
        {
            pthread_mutex_lock(&locks.dictionary[id]);

            if (status.dictionary[id].empty())
                msg_to_send_back = "No such key exists";
            else 
            {
                status.dictionary[id].clear();
                msg_to_send_back = "Deletion successful";
            }

            pthread_mutex_unlock(&locks.dictionary[id]);
        }

        else if (pieces[0] == "update")
        {
            pthread_mutex_lock(&locks.dictionary[id]);

            if (status.dictionary[id].empty())
                msg_to_send_back = "Key does not exist";
            else 
            {
                status.dictionary[id] = pieces[2];
                msg_to_send_back = status.dictionary[id];
            }

            pthread_mutex_unlock(&locks.dictionary[id]);
        }

        else if (pieces[0] == "concat")
        {
            int id_second = stoi(pieces[2]);

            pthread_mutex_lock(&locks.dictionary[id]);
            string str1 = status.dictionary[id];
            pthread_mutex_unlock(&locks.dictionary[id]);

            pthread_mutex_lock(&locks.dictionary[id_second]);
            string str2 = status.dictionary[id_second];
            pthread_mutex_unlock(&locks.dictionary[id_second]);

            if (str1.empty() || str2.empty())
                msg_to_send_back = "Concat failed as at least one of the keys does exist";
            else
            {
                pthread_mutex_lock(&locks.dictionary[id]);
                status.dictionary[id] = str1+str2;
                pthread_mutex_unlock(&locks.dictionary[id]);

                pthread_mutex_lock(&locks.dictionary[id_second]);
                status.dictionary[id_second] = str2+str1;
                pthread_mutex_unlock(&locks.dictionary[id_second]);

                msg_to_send_back = str2+str1;
            }
        }
        
        else if (pieces[0] == "fetch")
        {
            pthread_mutex_lock(&locks.dictionary[id]);

            if (status.dictionary[id].empty())
                msg_to_send_back = "Key does not exist";
            else 
                msg_to_send_back = status.dictionary[id];

            pthread_mutex_unlock(&locks.dictionary[id]);
        }

        sleep(2);

        auto temp = this_thread::get_id();
        
        stringstream st;
        st << temp;

        msg_to_send_back = ":" + st.str() + ":" + msg_to_send_back;

        cout << msg_to_send_back << endl;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            break;
        }
    }

    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *work_on_thread(void *args) {

    int thread_id = pthread_self();

    while(1)
    {
        int client_socket_fd = -1;

        sem_wait(&requests);

        pthread_mutex_lock(&locks.client_socket);
        
        if(status.client_sockets.empty() == 0)
        {
            client_socket_fd = status.client_sockets.front();
            status.client_sockets.pop();
        }

        pthread_mutex_unlock(&locks.client_socket);

        if(client_socket_fd != -1)
            handle_connection(client_socket_fd, thread_id);
        else
            printf("Error: client_socket returned invalid value for thread %d\n", thread_id);
    }

    return 0;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    int thread_value = atoi(argv[1]);
    printf("Running server with %d worker threads\n", thread_value);

    sem_init(&requests, 0, 0);

    for (int i = 0; i < MAX_CLIENTS+1; i++)
    {
        status.dictionary[i] = "";
        pthread_mutex_init(&locks.dictionary[i], NULL);
    }

    pthread_mutex_init(&locks.client_socket, NULL);
    
    pthread_t *threads = (pthread_t *) malloc(sizeof(pthread_t) * thread_value);

    for (int i = 0; i < thread_value; i++)
        pthread_create(&threads[i], NULL, work_on_thread, NULL);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        pthread_mutex_lock(&locks.client_socket);
        status.client_sockets.push(client_socket_fd);
        pthread_mutex_unlock(&locks.client_socket);

        sem_post(&requests);
    }

    close(wel_socket_fd);
    
    exit(0);
}
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q3/client_side.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////

typedef struct request_type {
    int id;
    int time;
    string message;
} request_type;

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    request_type request = *(request_type *) args;
    sleep(request.time);

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    send_string_on_socket(socket_fd, request.message);
    
    string output;
    int read;

    tie(output, read) = read_string_from_socket(socket_fd, buff_sz);
    cout << request.id << output << endl;

    close(socket_fd);

    return 0;
}

int main(int argc, char *argv[])
{

    string temp;
    getline(cin, temp);
    int value = stoi(temp);

    pthread_t *threads = (pthread_t *) malloc(sizeof(pthread_t) * value);
    request_type *requests = (request_type *) malloc(sizeof(request_type) * value);

    for(int i=0; i<value; i++)
    {
        getline(cin, temp);

        cout << temp << endl;
        requests[i].id = i;
        requests[i].time = stoi(temp.substr(0, temp.find(' ')));
        temp = temp.substr(temp.find(' ')+1);
        requests[i].message=temp;

        pthread_create(&threads[i], NULL, begin_process, &requests[i]);
    }

    for(int i=0; i<value; i++)
        pthread_join(threads[i], NULL);

    exit(0);
}