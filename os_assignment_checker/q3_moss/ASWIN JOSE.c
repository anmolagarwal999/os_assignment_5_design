
//###########FILE CHANGE ./main_folder/ASWIN JOSE_305904_assignsubmission_file_/2019113016_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <queue>
#include <map>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

map<int, string> dictionary;
const int initial_msg_len = 256;
////////////////////////////////
#define POOL_SIZE 10
pthread_t* pool;
pthread_mutex_t mutex_count;
pthread_cond_t cond_count;
////////////////////////////////////
queue<pair<int, string>> request_queue;
const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes = write(fd, s.c_str(), s.length());
    if (bytes < 0)
    {
        cerr << "Failed to send DATA via socket.\n";
    }

    return bytes;
}

///////////////////////////////

string run_commands(string command) {

    string ret_str = "";
    int error = 0;
    vector<string> args = {};
    size_t pos = 0;
    while (1) {
        pos = command.find(" ");
        
        if (pos == string::npos){
            break;
        }
        string tok = command.substr(0, pos);
        
        if (tok.length() > 0){
        args.push_back(tok);
        }
        command.erase(0, pos + 1);
    }

    if (command.length() > 0){ 
        args.push_back(command);
    }
    string comm = args[0];
    int com_len = args.size();
    pthread_mutex_lock(&mutex_count);
    if (com_len > 0) {
        if (comm == "insert"){
            if (com_len == 3){
                if (dictionary.find(stoi(args[1])) != dictionary.end()){
                    ret_str = "Key already exists";
                }else{
                    dictionary.insert({stoi(args[1]), args[2]});
                    ret_str = "Insertion successful";
            }
            } else {
                error ++;
            }
        } else if (comm == "delete") {
            if (com_len == 2) {
                if (dictionary.find(stoi(args[1])) == dictionary.end()) {
                    ret_str = "No such key exists";
                } else {
                    dictionary.erase(stoi(args[1]));
                    ret_str = "Deletion successful";
                }
            } else {
                error ++;
            }
        } else if (comm == "update") {
            if (com_len == 3) {
                auto itr = dictionary.find(stoi(args[1]));
                if (itr == dictionary.end()) {
                    ret_str = "Key does not exist";
                } else {
                    itr->second = args[2];
                    ret_str = args[2];
                }
            } else {
                error ++;
            }
        } else if (comm == "concat") {
            if (com_len == 3) {
                auto temp1 = dictionary.find(stoi(args[1]));
                auto temp2 = dictionary.find(stoi(args[2]));
                if ((temp1 == dictionary.end()) || (temp2 == dictionary.end())) {
                    ret_str = "Concat failed as at least one of the keys does not exist";
                } else {
                    string temp = temp2->second;
                    temp2->second += temp1->second;
                    temp1->second += temp;
                    ret_str = temp2->second;
                }
            } else {
                error ++;
            }
        } else if (comm == "fetch") {
            if (com_len == 2) {
                auto itr = dictionary.find(stoi(args[1]));
                if (itr == dictionary.end()) {
                    ret_str = "Key does not exist";
                } else {
                    ret_str = itr->second;
                }
            } else {
                error ++;
            }
        } else {
            ret_str = comm + ": command error";
        }
        if (error > 0){
            ret_str = comm + ": argument error";
        }
    }
    pthread_mutex_unlock(&mutex_count);
    return ret_str;
}

void handle_connection(int client_sk_t)
{
    // int client_sk_t = *((int *)client_sk_t_ptr);
    //####################################################

//     int received_num, sent_num;

//     /* read message from client */
//     int ret_val = 1;

//     while (true)
//     {
//         string cmd;
//         tie(cmd, received_num) = read_string_from_socket(client_sk_t, buff_sz);
//         ret_val = received_num;
//         // debug(ret_val);
//         // printf("Read something\n");
//         if (ret_val <= 0)
//         {
//             // perror("Error read()");
//             printf("Server could not read msg sent from client\n");
//             goto close_client_socket_ceremony;
//         }
//         cout << "Client sent : " << cmd << endl;
//         if (cmd == "exit")
//         {
//             cout << "Exit pressed by client" << endl;
//             goto close_client_socket_ceremony;
//         }
//         string msg_to_send_back = "Ack: " + cmd;

//         ////////////////////////////////////////
//         // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
//         // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

//         int sent_to_client = send_string_on_socket(client_sk_t, msg_to_send_back);
//         // debug(sent_to_client);
//         if (sent_to_client == -1)
//         {
//             perror("Error while writing to client. Seems socket has been closed");
//             goto close_client_socket_ceremony;
//         }
//     }

// close_client_socket_ceremony:
//     close(client_sk_t);
//     printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;

    string request;
    int bytes_received = 0;
    tie(request, bytes_received) = read_string_from_socket(client_sk_t, buff_sz);
    pthread_mutex_lock(&mutex_count);
    cout<<"request : "<<request<<endl;
    request_queue.push(make_pair(client_sk_t, request));
    pthread_mutex_unlock(&mutex_count);
    pthread_cond_broadcast(&cond_count);
}


void *begin_process(void* args) {
    while (1) {
        pthread_mutex_lock(&mutex_count);
        while (request_queue.empty()) {
            pthread_cond_wait(&cond_count, &mutex_count);
        }

        int client_socket_fd = -1;
        
        string command = "";
        tie(client_socket_fd, command) = request_queue.front();
        request_queue.pop();
        pthread_mutex_unlock(&mutex_count);
        sleep(1);

        string msg_to_send_back = to_string(pthread_self())+":"+run_commands(command);
        int bytes = 0;
        bytes = send_string_on_socket(client_socket_fd, msg_to_send_back);
        if (bytes == -1) {
            cerr << "Error writing \n";
        }

        close(client_socket_fd);
        printf(BRED "Disconnected from client" ANSI_RESET "\n");

    }
    return NULL;
}

int main(int argc, char *argv[])
{

    // int i, j, k, t, n;
    int pool_size= 8;
    if (argc <= 1){
        cout<<"Incorrect number of arguments, taking default pool size 8"<<endl;
        // return -1;
    }else{
        pool_size = atoi(argv[1]);
    }

    // allocate memory for worker thread pool
    pool = (pthread_t*)calloc(pool_size, sizeof(pthread_t));

    // initialize worker threads
    for (int i = 0; i < pool_size; i++) {
        if (pthread_create(&pool[i], NULL, &begin_process, NULL)) {
            perror("Error creating worker thread");
        }
    }

    pthread_mutex_init(&mutex_count, NULL);
    pthread_cond_init(&cond_count, NULL);

    int wel_sk_t, client_sk_t, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_sk_t = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_sk_t < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_sk_t, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_sk_t, MAX_CLIENTS);
    cout << "Server has started listening on the "<<PORT_ARG<< endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_sk_t */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_sk_t = accept(wel_sk_t, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_sk_t < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        cout<<"hi"<<endl;

        handle_connection(client_sk_t);
    }
    pthread_mutex_destroy(&mutex_count);
    pthread_cond_destroy(&cond_count);
    close(wel_sk_t);
    return 0;
}


//###########FILE CHANGE ./main_folder/ASWIN JOSE_305904_assignsubmission_file_/2019113016_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

typedef struct Task {
    int task_index;
    int del;
    string comm;
    string res;

} Task;

Task taskQueue[256];
int taskCount = 0;
pthread_mutex_t mutex_count;
pthread_cond_t cond_count;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);


    // cout << "Connection to server successful" << endl;
    
    // while (true)
    // {
        Task *task = (Task *)args;
        sleep(task->del);
        // printf("here\n");

        // while (taskCount == 0) {
        //     pthread_cond_wait(&cond_count, &mutex_count);
        // }
        // task = taskQueue[0];
        // cout<<"index : "<<task.task_index<<" "<<taskCount<<endl;
        // int i;
        // for (i = 0; i < taskCount - 1; i++) {
        //     taskQueue[i] = taskQueue[i + 1];
        // }
        // taskCount--;
        
        // string to_send;
        // cout << "Enter msg: ";
        // getline(cin, to_send);
        send_string_on_socket(socket_fd, task->res);
        int num_bytes_read;
        string output_msg;
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        // cout << "Received: " << output_msg << endl;
        // cout << "====" << endl;
        pthread_mutex_lock(&mutex_count);
        string output = to_string(task->task_index) + ":" + output_msg + "\n";
        cout<<output;
        pthread_mutex_unlock(&mutex_count);
    // }
    // part;
    return NULL;
}




int main(int argc, char *argv[]){
    
    /////////////////////////////////////////////////////////////////////
    // getting the input of the counts of the requests
    int request_count;
    cin>>request_count;
    int thread_num = request_count;
    taskCount =request_count;
    // setting up Task structures
    Task tasks[request_count];

    // getting each tasks
    int i;
    for (i=0;i < request_count; i++){
        tasks[i].task_index = i;
        string temp;
        cin>>tasks[i].del;
        getline(cin, temp);
        tasks[i].res = temp;
        // cout<<"task :"<<tasks[i].task_index<<" "<<tasks[i].res<<endl;
    }
    /////////////////////////////////////////////////////////////////////
    // initialising threads
    pthread_t th[thread_num];
    pthread_mutex_init(&mutex_count, NULL);
    pthread_cond_init(&cond_count, NULL);

    // creating threads
    for (i = 0; i < thread_num; i++) {
        // cout<<"here in create threads"<<endl;
        if (pthread_create(&th[i], NULL, begin_process, (void *)(&tasks[i])) != 0) {
            perror("Failed to create the thread");
        }
    }

    //joining threads
    for (i = 0; i < thread_num; i++) {
        if (pthread_join(th[i], NULL) != 0) {
            perror("Failed to join the thread");
        }
    }
    pthread_mutex_destroy(&mutex_count);
    pthread_cond_destroy(&cond_count);
    /////////////////////////////////////////////////////////////////////////
    return 0;
}
