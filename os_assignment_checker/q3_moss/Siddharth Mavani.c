
//###########FILE CHANGE ./main_folder/Siddharth Mavani_305895_assignsubmission_file_/2020101122/q3/._client.cpp ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

long long int init_time;
int goals_a;
int goals_h;

typedef struct grp_member
{
	char name[50];
	char area;
	int t_reach, t_patience, goal_patience, waiting_or_not, left_or_not, reached_or_not;
	long long int seat_alloc_time;

} grp_member;

typedef struct zone_stat
{
	int zone_cap_n, zone_cap_h, zone_cap_a;
	int zone_cap_n_curr, zone_cap_h_curr, zone_cap_a_curr;

} zone_stat;

typedef struct goal_chance
{
	int chance_no;
	char team[50];
	int time_of_goal;
	float probability;
} goal_chance;

typedef struct tot_stats
{
	int spec_thresh, tot_groups, total_people, tot_chance;

} tot_stats;

grp_member grp_arr[1000];
goal_chance goal_c[1000];
zone_stat zones;
tot_stats total_stats;

void *goalscorer(void *inp)
{
	while (1)
	{
		int curr_time = time(NULL);
		for (int i = 0; i < total_stats.tot_chance; i++)
		{
			if (curr_time - init_time >= goal_c[i].time_of_goal && goal_c[i].chance_no > 0)
			{
				pthread_mutex_lock(&mutex);
				curr_time = time(NULL);
				srand(time(0));
				int num = rand() % 6;
				if (goal_c[i].probability >= 0.75 && num >= 1)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.50 && num >= 2)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.25 && num >= 3)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability < 0.25 && num >= 4)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else
				{
					printf(BCYN "%s missed their %d chance to score\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
				}
				goal_c[i].chance_no = 0;
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *bad_perf(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].goal_patience <= goals_h && grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team A\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].goal_patience <= goals_a && grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team H\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *exit_sim(void *inp)
{
	while (1)
	{
		int count = 0;
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].left_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				count++;
				pthread_mutex_unlock(&mutex);
			}
		}
		if (count == total_stats.total_people)
		{
			pthread_mutex_lock(&mutex);
			printf(BBLK"All the people left the stadium\n"ANSI_RESET);
			exit(0);
			pthread_mutex_unlock(&mutex);
		}
	}
}

void *person_timeout(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (grp_arr[i].t_patience < curr_time - init_time + grp_arr[i].t_reach && grp_arr[i].waiting_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s didnt get a seat after %d second and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].t_patience < curr_time - grp_arr[i].seat_alloc_time && grp_arr[i].waiting_or_not == 0)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s watched the match for %d seconds and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *reach_stadium(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 0 && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				grp_arr[i].waiting_or_not = 1;
				grp_arr[i].reached_or_not = 1;
				printf(BRED "%s reached the stadium\n" ANSI_RESET, grp_arr[i].name);
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *alloc_zone(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 1)
			{
				if (grp_arr[i].waiting_or_not == 1)
				{
					pthread_mutex_lock(&mutex);
					if (grp_arr[i].area == 'N' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					pthread_mutex_unlock(&mutex);
				}
			}
		}
	}
}

int main(void)
{

	time_t seconds;
	init_time = time(NULL);

	int tot_people;
	scanf("%d %d %d", &zones.zone_cap_h, &zones.zone_cap_a, &zones.zone_cap_n);
	zones.zone_cap_h_curr = zones.zone_cap_h;
	zones.zone_cap_a_curr = zones.zone_cap_a;
	zones.zone_cap_n_curr = zones.zone_cap_n;
	scanf("%d", &total_stats.spec_thresh);
	scanf("%d", &total_stats.tot_groups);
	for (int i = 0; i < total_stats.tot_groups; i++)
	{
		scanf("%d", &tot_people);
		for (int j = 0; j < tot_people; j++)
		{
			grp_arr[total_stats.total_people].waiting_or_not = -1;
			grp_arr[total_stats.total_people].left_or_not = -1;
			grp_arr[total_stats.total_people].reached_or_not = 0;
			scanf("%s %c %d %d %d", grp_arr[total_stats.total_people].name, &grp_arr[total_stats.total_people].area, &grp_arr[total_stats.total_people].t_reach, &grp_arr[total_stats.total_people].t_patience, &grp_arr[total_stats.total_people].goal_patience);
			total_stats.total_people++;
		}
	}
	scanf("%d", &total_stats.tot_chance);
	for (int i = 0; i < total_stats.tot_chance; i++)
	{
		goal_c[i].chance_no = i + 1;
		scanf("%s %d %f", goal_c[i].team, &goal_c[i].time_of_goal, &goal_c[i].probability);
	}

	// printf("1 %d %d %d\n", zones.zone_cap_h, zones.zone_cap_a, zones.zone_cap_n);
	// printf("2 %d\n", total_stats.spec_thresh);
	// printf("3 %d\n", total_stats.tot_groups);
	// printf("4 %d\n", tot_people);
	// printf("5 %d\n", total_stats.tot_chance);
	// printf("6 %d\n", total_stats.total_people);
	// for (int i = 0; i < total_stats.total_people;i++)
	// {
	// 	printf("7 %s\t %c\t %d\t %d\t %d\t %d\t %d\t %d\n", grp_arr[i].name, grp_arr[i].area, grp_arr[i].t_reach, grp_arr[i].t_patience, grp_arr[i].waiting_or_not,grp_arr[i].goal_patience,grp_arr[i].left_or_not,grp_arr[i].reached_or_not);
	// }
	// for (size_t i = 0; i < total_stats.tot_chance; i++)
	// {
	// 	printf("8 %s %d %f\n", goal_c[i].team, goal_c[i].time_of_goal, goal_c[i].probability);
	// }

	pthread_t tid1;
	pthread_t tid2;
	pthread_t tid3;
	pthread_t tid4;
	pthread_t tid5;
	pthread_t tid6;
	void *a;
	pthread_create(&tid1, NULL, alloc_zone, &a);
	pthread_create(&tid2, NULL, reach_stadium, &a);
	pthread_create(&tid3, NULL, person_timeout, &a);
	pthread_create(&tid4, NULL, exit_sim, &a);
	pthread_create(&tid5, NULL, bad_perf, &a);
	pthread_create(&tid6, NULL, goalscorer, &a);
	pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/Siddharth Mavani_305895_assignsubmission_file_/2020101122/q3/._server.cpp ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

long long int init_time;
int goals_a;
int goals_h;

typedef struct grp_member
{
	char name[50];
	char area;
	int t_reach, t_patience, goal_patience, waiting_or_not, left_or_not, reached_or_not;
	long long int seat_alloc_time;

} grp_member;

typedef struct zone_stat
{
	int zone_cap_n, zone_cap_h, zone_cap_a;
	int zone_cap_n_curr, zone_cap_h_curr, zone_cap_a_curr;

} zone_stat;

typedef struct goal_chance
{
	int chance_no;
	char team[50];
	int time_of_goal;
	float probability;
} goal_chance;

typedef struct tot_stats
{
	int spec_thresh, tot_groups, total_people, tot_chance;

} tot_stats;

grp_member grp_arr[1000];
goal_chance goal_c[1000];
zone_stat zones;
tot_stats total_stats;

void *goalscorer(void *inp)
{
	while (1)
	{
		int curr_time = time(NULL);
		for (int i = 0; i < total_stats.tot_chance; i++)
		{
			if (curr_time - init_time >= goal_c[i].time_of_goal && goal_c[i].chance_no > 0)
			{
				pthread_mutex_lock(&mutex);
				curr_time = time(NULL);
				srand(time(0));
				int num = rand() % 6;
				if (goal_c[i].probability >= 0.75 && num >= 1)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.50 && num >= 2)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.25 && num >= 3)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability < 0.25 && num >= 4)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else
				{
					printf(BCYN "%s missed their %d chance to score\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
				}
				goal_c[i].chance_no = 0;
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *bad_perf(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].goal_patience <= goals_h && grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team A\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].goal_patience <= goals_a && grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team H\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *exit_sim(void *inp)
{
	while (1)
	{
		int count = 0;
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].left_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				count++;
				pthread_mutex_unlock(&mutex);
			}
		}
		if (count == total_stats.total_people)
		{
			pthread_mutex_lock(&mutex);
			printf(BBLK"All the people left the stadium\n"ANSI_RESET);
			exit(0);
			pthread_mutex_unlock(&mutex);
		}
	}
}

void *person_timeout(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (grp_arr[i].t_patience < curr_time - init_time + grp_arr[i].t_reach && grp_arr[i].waiting_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s didnt get a seat after %d second and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].t_patience < curr_time - grp_arr[i].seat_alloc_time && grp_arr[i].waiting_or_not == 0)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s watched the match for %d seconds and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *reach_stadium(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 0 && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				grp_arr[i].waiting_or_not = 1;
				grp_arr[i].reached_or_not = 1;
				printf(BRED "%s reached the stadium\n" ANSI_RESET, grp_arr[i].name);
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *alloc_zone(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 1)
			{
				if (grp_arr[i].waiting_or_not == 1)
				{
					pthread_mutex_lock(&mutex);
					if (grp_arr[i].area == 'N' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					pthread_mutex_unlock(&mutex);
				}
			}
		}
	}
}

int main(void)
{

	time_t seconds;
	init_time = time(NULL);

	int tot_people;
	scanf("%d %d %d", &zones.zone_cap_h, &zones.zone_cap_a, &zones.zone_cap_n);
	zones.zone_cap_h_curr = zones.zone_cap_h;
	zones.zone_cap_a_curr = zones.zone_cap_a;
	zones.zone_cap_n_curr = zones.zone_cap_n;
	scanf("%d", &total_stats.spec_thresh);
	scanf("%d", &total_stats.tot_groups);
	for (int i = 0; i < total_stats.tot_groups; i++)
	{
		scanf("%d", &tot_people);
		for (int j = 0; j < tot_people; j++)
		{
			grp_arr[total_stats.total_people].waiting_or_not = -1;
			grp_arr[total_stats.total_people].left_or_not = -1;
			grp_arr[total_stats.total_people].reached_or_not = 0;
			scanf("%s %c %d %d %d", grp_arr[total_stats.total_people].name, &grp_arr[total_stats.total_people].area, &grp_arr[total_stats.total_people].t_reach, &grp_arr[total_stats.total_people].t_patience, &grp_arr[total_stats.total_people].goal_patience);
			total_stats.total_people++;
		}
	}
	scanf("%d", &total_stats.tot_chance);
	for (int i = 0; i < total_stats.tot_chance; i++)
	{
		goal_c[i].chance_no = i + 1;
		scanf("%s %d %f", goal_c[i].team, &goal_c[i].time_of_goal, &goal_c[i].probability);
	}

	// printf("1 %d %d %d\n", zones.zone_cap_h, zones.zone_cap_a, zones.zone_cap_n);
	// printf("2 %d\n", total_stats.spec_thresh);
	// printf("3 %d\n", total_stats.tot_groups);
	// printf("4 %d\n", tot_people);
	// printf("5 %d\n", total_stats.tot_chance);
	// printf("6 %d\n", total_stats.total_people);
	// for (int i = 0; i < total_stats.total_people;i++)
	// {
	// 	printf("7 %s\t %c\t %d\t %d\t %d\t %d\t %d\t %d\n", grp_arr[i].name, grp_arr[i].area, grp_arr[i].t_reach, grp_arr[i].t_patience, grp_arr[i].waiting_or_not,grp_arr[i].goal_patience,grp_arr[i].left_or_not,grp_arr[i].reached_or_not);
	// }
	// for (size_t i = 0; i < total_stats.tot_chance; i++)
	// {
	// 	printf("8 %s %d %f\n", goal_c[i].team, goal_c[i].time_of_goal, goal_c[i].probability);
	// }

	pthread_t tid1;
	pthread_t tid2;
	pthread_t tid3;
	pthread_t tid4;
	pthread_t tid5;
	pthread_t tid6;
	void *a;
	pthread_create(&tid1, NULL, alloc_zone, &a);
	pthread_create(&tid2, NULL, reach_stadium, &a);
	pthread_create(&tid3, NULL, person_timeout, &a);
	pthread_create(&tid4, NULL, exit_sim, &a);
	pthread_create(&tid5, NULL, bad_perf, &a);
	pthread_create(&tid6, NULL, goalscorer, &a);
	pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/Siddharth Mavani_305895_assignsubmission_file_/2020101122/q3/server.cpp ####################//

#include <cstdio>
#include <iostream>
#include <sstream>
#include <netdb.h>
#include <vector>
#include <string>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>


// Initialising all the global variables and Locks, Semaphores
typedef sockaddr SockAddress;

std::map<int, std::string> dictionary;

std::queue<int> Socket;
std::vector<pthread_mutex_t> mutexLock(100, PTHREAD_MUTEX_INITIALIZER);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;


// Inserting into Dictionary
void insert(std::string value, int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
		dictionary[key] = value;
        str = str + "Insertion Successful";
	}
    else{
        str = str + "Key already exists";
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Deleting From Dictionary
void Delete (int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
        str = str + "No such key exists";
    }
    else{
        dictionary.erase(key);
        str = str + "Deletion Successful";
	}

    pthread_mutex_unlock(&mutexLock[key]);
}

// Updating The Dictionary
void update(std::string value, int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
	if (dict_cout){
		dictionary[key] = value;
        str = str + value;
	}
	else{
        str = str + "No such key exists";
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Concatenating values in dictionary
void concat(int key1, int key2, std::string& str){
    pthread_mutex_lock(&mutexLock[key1]);
    pthread_mutex_lock(&mutexLock[key2]);

    int dict_cout1 = dictionary.count(key1);
    int dict_cout2 = dictionary.count(key2);
    if (!dict_cout1 || !dict_cout2){
        str = str + "Concat failed as a least one of the keys does not exist";
    }
    else{
        std::string value1 = dictionary[key1];
        std::string value2 = dictionary[key2];
        dictionary[key1] = dictionary[key1] + value2;
        dictionary[key2] = dictionary[key2] + value1;
        str = str + value2 + value1;
    }

    pthread_mutex_unlock(&mutexLock[key1]);
    pthread_mutex_unlock(&mutexLock[key2]);
}

// Fetches valus of a given Key, and appends it to str
void fetch(int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
        str = str + "Key does not exist";
    }
    else{
        str = str + dictionary[key];
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Handling clients using multiple threads
void server(int sockfd){
	// Reading command 
    char buffer[1024];
    for(int i=0; i<1024; i++){
        buffer[i] = 0;
    }
    
	read(sockfd, buffer, 1024);

    std::string str;
    str = "";
    std::vector<std::string> tokens;

    // Converting input to string
    std::stringstream temp(buffer);
    std::string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' '))
        tokens.push_back(intermediate);

    str = str + std::to_string(pthread_self());
    str = str + ":";
    int nParts = tokens.size();

    // Driver Function
    if (tokens.size() > 1){
        if (tokens[1] == "insert"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                insert(tokens[3], token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "delete"){
            if (nParts == 3){
                int token_int = stoi(tokens[2]);
                Delete(token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "concat"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                int token_int_1 = stoi(tokens[3]);
                concat(token_int, token_int_1, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "update"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                update(tokens[3], token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "fetch"){
            if (nParts == 3){
                int token_int = stoi(tokens[2]);
                fetch(token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else{
            str = str + "Incorrect command";
        }
    }
    else{
        str = str + "Incorrect number of arguments";
    }

    // Writing
    int str_len = strlen(str.c_str());
    write(sockfd, str.c_str(), str_len);
}

// Handles Worker Threads
void *worker_thread(void *arg){

	while (1){
		pthread_mutex_lock(&lock);

		while (!Socket.size())
			pthread_cond_wait(&cond, &lock);

        int sockfd = Socket.front();
		Socket.pop();

		pthread_mutex_unlock(&lock);
		server(sockfd);
		close(sockfd);
	}
	return NULL;
}

// Looks for Clients
void client_search()
{
	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1){
		exit(0);
    }

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(8080);

    int bind_check = bind(sockfd, (SockAddress *)&servaddr, sizeof(servaddr));
	if (bind_check){
		exit(0);
    }

    int listen_check = listen(sockfd, 5);
	if (listen_check){
		exit(0);
    }

	socklen_t len = sizeof(cli);

	while (1){
		connfd = accept(sockfd, (SockAddress *)&cli, &len);
		if (connfd < 0)
			exit(0);

		pthread_mutex_lock(&lock);
        Socket.push(connfd);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
	}
}

// Creates Worker Threads
void create_thread(int num_worker_threads, std::vector<pthread_t>& worker_threads){

    for(int i = 0; i < num_worker_threads; i++){
        pthread_create(&worker_threads[i], NULL, worker_thread, NULL);
    }

}

// Joins Worker Threads
void join_thread(int num_worker_threads, std::vector<pthread_t>& worker_threads){

    for(int i = 0; i < num_worker_threads; i++){
        pthread_join(worker_threads[i], NULL);
    }

}

int main(int argc, char *argv[]){
	
    if (argc > 1){
		int i, num_worker_threads = atoi(argv[1]);
        std::vector<pthread_t> worker_threads(num_worker_threads);

        create_thread(num_worker_threads, worker_threads);

		client_search();

        join_thread(num_worker_threads, worker_threads);
	}
	else{
        std::cout << "Missing number of threads as argument";
    }
	return 0;
}
//###########FILE CHANGE ./main_folder/Siddharth Mavani_305895_assignsubmission_file_/2020101122/q3/client.cpp ####################//

#include <cstdio>
#include <netdb.h>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

// Creating Class that stores all the data
class Data{
public:
    Data(int client_id, std::string command)
        : client_id(client_id), command(command) {}

    Data() = default;

	int client_id;
	std::string command;
};

void func(Data& data, int sockfd){
    const char* ptr_to_command = data.command.c_str();
    int command_len = strlen(data.command.c_str());

	write(sockfd, ptr_to_command, command_len);
	char buff[1024] = {0};
	read(sockfd, buff, 1024);

    std::cout << data.client_id << ": " << buff << std::endl;
}

void* client(void* args)
{
    // Tokenizing the given string
    std::vector<std::string> tokens;

    Data data = *(Data*)args;

    std::stringstream temp(data.command);
    std::string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' ')){
        tokens.push_back(intermediate);
    }

    // Printing the token vector
	if(tokens.size() < 2){
        std::cout << "Command doesn't exist\n";
		return NULL;
	}

    // Convert to integer
    int sleep_time = stoi(tokens[0]);
    sleep(sleep_time);

	int sockfd, connfd, check;
	sockaddr_in servaddr, cli;

	// Creating and verfying the socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1){
		exit(0);
    }

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(8080);

	// Connecting the client socket to server socket
    check = connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr));
	if (check != 0){
		exit(0);
    }

	func(data, sockfd);
	close(sockfd);

    return NULL;
}

// Joining the Client Threads
void join_thread(int num_clients, std::vector<pthread_t>& client_threads){

    for(int i = 0; i < num_clients; i++){
        pthread_join(client_threads[i], NULL);
    }

}

int main()
{   
    // Inputting number of clients
	int num_clients;
    std::cin >> num_clients;
    getchar();

    // Create a thread for each client
    std::vector<pthread_t> client_threads(num_clients);
    std::vector<Data> datas(num_clients);

    // Reading input and creating threads
	for(int i = 0, j = 0; i < num_clients; i++, j = 0)
    {
        getline(std::cin, datas[i].command);
        datas[i].client_id = i;
        pthread_create(&client_threads[i], NULL, client, &datas[i]);
    }

    join_thread(num_clients, client_threads);

	return 0;
}
