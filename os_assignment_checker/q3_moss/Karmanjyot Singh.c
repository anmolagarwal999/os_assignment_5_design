
//###########FILE CHANGE ./main_folder/Karmanjyot Singh_305838_assignsubmission_file_/2020101062_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
using namespace std;

#define max_clients 100
#define port_number 8001
#define buff_sz 1048576
#define MAX_DICT_SIZE 101
int num_worker_threads;

struct dictionary_node
{
    string str;
    int id;
    int is_active;
    pthread_mutex_t mutex;
};

vector<struct dictionary_node> dict(MAX_DICT_SIZE);
queue<int *> q;
pthread_mutex_t queue_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;

int send_string_on_socket(int fd, const string &s)
{
    string str = to_string(gettid()) + " : " + s;
    int bytes_sent = write(fd, str.c_str(), str.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void function_handler(string s, int client_socket_fd)
{
    char *cpy_in = (char *)malloc((s.length() + 1) * sizeof(char));
    strcpy(cpy_in, s.c_str());
    cpy_in[s.length()] = '\0';
    char *str = strtok_r(cpy_in, " ", &cpy_in);
    int len = s.length();
    char *arguments[len];
    int arg_count = 0;
    while (str != NULL)
    {
        arguments[arg_count] = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(arguments[arg_count], str);
        arg_count++;
        str = strtok_r(cpy_in, " ", &cpy_in);
    }

    if (arg_count == 0)
        return;
    arguments[arg_count] = NULL;
    if (strcmp(arguments[0], "insert") == 0)
    {
        if (arg_count != 3)
        {
            cout << "Invalid Arguments for insert" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active == 0)
        {
            dict[key].str = arguments[2];
            dict[key].is_active = 1;
            cout << "Insertion Succesful" << arguments[2] << " at " << key << endl;
            send_string_on_socket(client_socket_fd, "Insertion Succesful");
        }
        else
        {
            cout << "Key already present" << endl;
            send_string_on_socket(client_socket_fd, "Key already exists");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (strcmp(arguments[0], "concat") == 0)
    {
        if (arg_count != 3)
        {
            cout << "Invalid Arguments for concat" << endl;
            return;
        }
        int key1 = atoi(arguments[1]);
        int key2 = atoi(arguments[2]);
        pthread_mutex_lock(&dict[key1].mutex);
        pthread_mutex_lock(&dict[key2].mutex);
        if (dict[key1].is_active == 1 && dict[key2].is_active == 1)
        {
            string tmp = dict[key1].str;
            dict[key1].str = dict[key1].str + dict[key2].str;
            dict[key2].str += tmp;
            cout << "Concatenation Succesful" << endl;
            send_string_on_socket(client_socket_fd, dict[key2].str);
        }
        else
        {
            cout << "Either one of the keys is not present" << endl;
            send_string_on_socket(client_socket_fd, "Concat failed as atleast one of the keys does not exist");
        }
        pthread_mutex_unlock(&dict[key1].mutex);
        pthread_mutex_unlock(&dict[key2].mutex);
    }
    else if (strcmp(arguments[0], "fetch") == 0)
    {
        if (arg_count != 2)
        {
            cout << "Invalid Arguments for fetch" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active == 1)
        {
            cout << "Fetch Succesful" << endl;
            send_string_on_socket(client_socket_fd, dict[key].str);
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (strcmp(arguments[0], "update") == 0)
    {
        if (arg_count != 3)
        {
            cout << "Invalid Arguments for update" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active == 1)
        {
            dict[key].str = arguments[2];
            cout << "Update Succesful" << endl;
            send_string_on_socket(client_socket_fd, dict[key].str);
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
    else if (strcmp(arguments[0], "delete") == 0)
    {
        if (arg_count != 2)
        {
            cout << "Invalid Arguments for delete" << endl;
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);
        if (dict[key].is_active == 1)
        {
            dict[key].is_active = 0;
            dict[key].str = "";
            cout << "Deletion Succesful" << endl;
            send_string_on_socket(client_socket_fd, "Deletion succesful");
        }
        else
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_socket_fd, "No such Key");
        }
        pthread_mutex_unlock(&dict[key].mutex);
    }
}

void handle_client(int client_fd)
{
    string buffer;
    buffer.resize(buff_sz);

    // pthread_mutex_lock(&client_lock[idx]);
    int byte_read = read(client_fd, &buffer[0], buff_sz - 1);
    // pthread_mutex_unlock(&client_lock[idx]);

    //cout << "Read Value" << buffer << endl;
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);
    if (byte_read <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        return;
    }

    function_handler(buffer, client_fd);
}

void *worker_thread(void *arg)
{
    // int sockfd = *((int *)arg);
    char buffer[buff_sz];
    int n;
    while (1)
    {
        pthread_mutex_lock(&queue_lock);
        while (q.empty())
        {
            pthread_cond_wait(&cond_var, &queue_lock);
        }
        int *client_sockfd = q.front();
        q.pop();
        pthread_mutex_unlock(&queue_lock);
        handle_client(*client_sockfd);
    }
    return NULL;
}

void init_server_socket()
{
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    int server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    int PORT_NUMBER = port_number;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(PORT_NUMBER);

    if (bind(server_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(server_socket_fd, max_clients);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    socklen_t clilen = sizeof(client_addr_obj);

    while (1)
    {
        printf("Waiting for a new client to request for a connection\n");
        int client_socket_fd = accept(server_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        cout << "ok" << endl;
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf("New client connected from port number %d and IP %s \n", ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;

        pthread_mutex_lock(&queue_lock);
        q.push(pclient);
        pthread_mutex_unlock(&queue_lock);
        pthread_cond_signal(&cond_var);
    }

    close(server_socket_fd);
}

int main(int argc, char *argv[])
{
    switch (argc)
    {
    case 1:
        printf("Too less arguments!!!\n");
        return 1;
        break;
    case 2:
        num_worker_threads = stoi(argv[1]);
        break;
    default:
        printf("Too many arguments!!!\n");
        return 1;
        break;
    }

    pthread_t wkr_thread[num_worker_threads];
    for (auto itr = 0; itr < MAX_DICT_SIZE; itr++)
    {
        dict[itr].id = itr;
        dict[itr].is_active = 0;
        dict[itr].str = "";
        pthread_mutex_init(&dict[itr].mutex, NULL);
    }
    for (auto i = 0; i < num_worker_threads; i++)
    {
        pthread_create(&wkr_thread[i], NULL, worker_thread, NULL);
    }
    init_server_socket();
}

//###########FILE CHANGE ./main_folder/Karmanjyot Singh_305838_assignsubmission_file_/2020101062_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
using namespace std;

#define server_port 8001
#define buffer_max_sz 1048576

typedef struct client_request
{
    int i;
    pthread_t tid;
    int client_fd;
    int req_time;
    string command;
    pthread_mutex_t mutex;
} client_request;

vector<client_request> clients;

pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER;

int get_client_socket_fd()
{
    struct sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = server_port;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    return socket_fd;
}

void *client_thread(void *(arg))
{
    int idx = *(int *)arg;
    // cout << "CLIENT MEIN" << endl;
    clients[idx].client_fd = get_client_socket_fd();
    int client_fd = clients[idx].client_fd;
    // pthread_mutex_lock(&map_lock);
    // user_idx.insert({client_fd, idx});
    // pthread_mutex_unlock(&map_lock);
    int req_time = clients[idx].req_time;
    string command = clients[idx].command;
    // cout << "going_to_sleep" << endl;

    sleep(req_time);

    pthread_mutex_lock(&clients[idx].mutex);
    // send request to server
    int x = write(client_fd, command.c_str(), command.length());
    // cout << "DATA sent" << endl;
    // cout << "COmmand :  " << command << endl;
    if (x < 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }

    pthread_mutex_unlock(&clients[idx].mutex);

    // read response from the server

    pthread_mutex_lock(&clients[idx].mutex);

    string buffer;
    buffer.resize(buffer_max_sz);
    int byte_read = read(client_fd, &buffer[0], buffer_max_sz - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);
    if (byte_read <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }
    pthread_mutex_lock(&terminal);
    cout << clients[idx].i << " : " << buffer << endl;
    pthread_mutex_unlock(&terminal);

    pthread_mutex_unlock(&clients[idx].mutex);

    return NULL;
}

int main()
{
    int num_clients;
    cin >> num_clients;
    // map_lock = PTHREAD_MUTEX_INITIALIZER;
    //cout << "NUm clients " << num_clients << endl;
    // get the input
    string x;
    getline(cin, x);
    for (int i = 0; i < num_clients; i++)
    {
        string str, time_req = "";
        getline(cin, str);
        auto itr = str.begin();
        while (*itr != ' ')
        {
            time_req += *itr;
            itr++;
        }
        itr++;
        client_request tmp;
        tmp.req_time = stoi(time_req);
        string command(itr, str.end());
        tmp.command = command;
        clients.push_back(tmp);
    }
    // get the connection sockets
    for (auto i = 0; i < num_clients; i++)
    {
        clients[i].i = i;
        clients[i].mutex = PTHREAD_MUTEX_INITIALIZER;
    }
    for (auto i = 0; i < num_clients; i++)
    {
        int *idx = new int;
        *idx = i;
        pthread_create(&clients[i].tid, NULL, client_thread, (void *)idx);
    }

    for (auto i = 0; i < num_clients; i++)
    {
        pthread_join(clients[i].tid, NULL);
    }

    for (int i = 0; i < num_clients; i++)
    {
        pthread_mutex_destroy(&clients[i].mutex);
    }
    pthread_mutex_destroy(&terminal);
    return 0;
}