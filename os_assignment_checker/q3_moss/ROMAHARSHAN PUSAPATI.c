
//###########FILE CHANGE ./main_folder/ROMAHARSHAN PUSAPATI_305781_assignsubmission_file_/Assignment_5/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <semaphore.h>
#include <bits/stdc++.h>
using namespace std;
/////////////////////////////
//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;
#define MAX_CLIENTS 4
#define PORT_ARG 8001
const int initial_msg_len = 256;
const LL buff_sz = 1048576;
typedef struct args args;
int the_string_len = 0;
int the_bytes_input = 0;
int len_maximum;
int len_minimum;
struct args
{
    int i;
    string command;
};
///////////////////////////////////////////////////
map<int, string> dict;
sem_t key_lock[101];
#define MAXIMUM 100
#define SMALLEST 3
#define NOT 0
#define YE 1
int ml;
sem_t read_lock;
pthread_t threads[1000];

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *ptrclient_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;
    int client_socket_fd = *((int *)ptrclient_socket_fd);

    /* read message from client */
    int ret_val = 1;

    string cmd;
    sem_wait(&read_lock);
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
        len_maximum = the_string_len - 1;
        len_minimum = the_bytes_input - 1;
    }
    len_minimum = the_string_len + YE + 1;
    string msg_to_send_back = "done";
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
    }
    the_bytes_input++;
    sem_post(&read_lock);
    ret_val = received_num;
    if (the_string_len == 5)
    {
        the_string_len = 0;
        the_bytes_input = the_bytes_input + YE;
        the_string_len = the_string_len + NOT;
    }
    if (ret_val <= 0)
    {
        printf("Server could not read msg sent from client\n");
        return NULL;
    }
    the_bytes_input++;
    string temp;
    vector<string> parts;
    int n = cmd.size();
    for (int i = 0; i < n; i++)
    {
        if (the_string_len == 5)
        {
            the_string_len = 0;
            the_bytes_input = the_bytes_input + YE;
            the_string_len = the_string_len + NOT;
        }
        if (cmd[i] != ' ')
        {
            temp.pb(cmd[i]);
        }
        else
        {
            parts.pb(temp);
            temp.clear();
        }
        the_bytes_input++;
    }
    if (the_string_len == 5)
    {
        the_string_len = 0;
        the_bytes_input = the_bytes_input + YE;
        the_string_len = the_string_len + NOT;
    }
    parts.pb(temp);
    int num_commands = parts.size();
    int key = stoi(parts[1]);
    the_bytes_input++;
    int index = stoi(parts[num_commands - 1]);
    cout << index << ":" << pthread_self() << ":";
    the_string_len = (YE * NOT) + YE;
    if (parts[0] == "insert")
    {
        sem_wait(&key_lock[key]);
        string value = parts[2];
        the_string_len = (YE * NOT) + YE + 1;
        if (dict.find(key) != dict.end())
        {
            printf("Key aldready exists\n");
        }
        else
        {
            the_bytes_input++;
            dict.insert({key, value});
            printf("Insertion successful\n");
        }
        the_bytes_input++;
        sem_post(&key_lock[key]);
        len_minimum = the_bytes_input - 1;
    }
    else if (parts[0] == "delete")
    {
        len_maximum = NOT + YE + 1;
        sem_wait(&key_lock[key]);
        the_bytes_input++;
        if (dict.find(key) != dict.end())
        {
            len_minimum = the_bytes_input - 1;
            dict.erase(key);
            len_maximum = NOT + YE + 1;
            printf("Deletion successful\n");
        }
        else
        {
            len_minimum = the_bytes_input - 1;
            printf("No such key exists\n");
        }
        the_string_len++;
        sem_post(&key_lock[key]);
    }
    else if (parts[0] == "update")
    {
        sem_wait(&key_lock[key]);
        the_string_len++;
        string value = parts[2];
        if (dict.find(key) != dict.end())
        {
            len_minimum = the_bytes_input - 1;
            dict[key] = value;
            cout << dict[key] << "\n";
            the_string_len++;
        }
        else
        {
            cout << "Key does not exist\n";
        }
        len_minimum = the_bytes_input - 1;
        sem_post(&key_lock[key]);
    }
    else if (parts[0] == "concat")
    {
        len_maximum = the_string_len - 1;
        int key1 = stoi(parts[1]);
        int key2 = stoi(parts[2]);
        len_minimum = the_bytes_input - 1;
        sem_wait(&key_lock[key1]);
        sem_wait(&key_lock[key2]);
        len_maximum = the_string_len - 1;
        if (dict.find(key1) == dict.end() || dict.find(key2) == dict.end())
        {
            len_minimum = the_bytes_input - 1;
            cout << "Concat failed as at least one of the keys does not exist\n";
        }
        else
        {
            string value1 = dict[key1];
            string value2 = dict[key2];
            the_string_len = the_string_len + NOT;
            dict[key1] = value1 + value2;
            len_maximum = NOT + YE + 1;
            dict[key2] = value2 + value1;
            cout << dict[key2] << "\n";
        }
        len_minimum = the_string_len + YE + 1;
        sem_post(&key_lock[key1]);
        sem_post(&key_lock[key2]);
    }
    else if (parts[0] == "fetch")
    {
        sem_wait(&key_lock[key]);
        if (dict.find(key) != dict.end())
        {
            len_minimum = the_string_len + YE + 1;
            cout << dict[key] << "\n";
        }
        else
        {
            cout << "Key does not exist\n";
        }
        the_bytes_input++;
        sem_post(&key_lock[key]);
        len_maximum = the_string_len - 1;
    }
}

int main(int argc, char *argv[])
{
    sem_init(&read_lock, 0, 1);
    int i, j, k, t, n;
    int wel_socket_fd, client_socket_fd, port_number;
    the_string_len++;
    socklen_t clilen;

    for (int i = 0; i <= 100; i++)
    {
        the_string_len++;
        sem_init(&key_lock[i], 0, 1);
    }

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    len_maximum = NOT + YE + 1;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        len_maximum = NOT + YE + 1;
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    the_bytes_input = NOT + YE - 1;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    the_bytes_input = NOT + YE - 1;
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    the_bytes_input = the_bytes_input + YE;
    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    clilen = sizeof(client_addr_obj);
    if (argc != 2)
    {
        the_bytes_input = the_bytes_input + YE;
        cout << "wrong input format\n";
        return 0;
    }
    else
    {
        the_string_len++;
        n = stoi(argv[1]);
    }
    client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
    if (client_socket_fd < 0)
    {
        len_maximum = NOT + YE + 1;
        perror("ERROR while accept() system call occurred in SERVER");
        exit(-1);
    }
    for (int i = 0; i < n; i++)
    {
        len_minimum = the_string_len + YE + 1;
        pthread_create(&threads[i], NULL, handle_connection, (void *)&client_socket_fd);
    }
    for (int i = 0; i < n; i++)
    {
        the_string_len++;
        pthread_join(threads[i], NULL);
    }
    for (int i = 0; i <= 100; i++)
    {
        len_maximum = NOT + YE + 1;
        sem_destroy(&key_lock[i]);
    }
    close(client_socket_fd);
    the_string_len++;
    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/ROMAHARSHAN PUSAPATI_305781_assignsubmission_file_/Assignment_5/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define SERVER_PORT 8001
const LL buff_sz = 1048576;
#define MAXIMUM 100
#define SMALLEST 3
#define NOT 0
#define YE 1
int present_time;
int max_time;
int ml;
int socket_fd;
int the_string_len = 0;
pthread_t threads[1000];
sem_t write_lock;
pthread_t time_control;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int the_bytes_input = 0;
typedef struct request request;
struct request
{
    int time;
    string command;
};
typedef struct args2 args2;
struct args2
{
    int i;
    int time;
    string command;
};
int len_maximum;
typedef struct args1 args1;
struct args1
{
    int m;
    request *A;
};
int len_minimum;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *timer(void *)
{
    the_bytes_input = the_bytes_input + YE;
    while (1)
    {
        sleep(1);
        the_string_len = the_string_len + NOT;
        present_time++;
        if (present_time >= max_time)
        {
            len_maximum = the_string_len - 1;
            return NULL;
        }
    }
}

void *exec_command(void *arguments)
{
    int time = ((args2 *)arguments)->time;
    string command = ((args2 *)arguments)->command;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    int i = ((args2 *)arguments)->i;
    while (present_time < time)
    {
        sleep(1);
    }
    sem_wait(&write_lock);
    send_string_on_socket(socket_fd, command);
    int num_bytes_read;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    sem_post(&write_lock);
    return NULL;
}

void begin_process()
{
    struct sockaddr_in server_obj;
    socket_fd = get_socket_fd(&server_obj);
    if (the_string_len == 5)
    {
        the_string_len = 0;
    }
    cout << "Connection to server successful" << endl;
    int m;
    string input;
    getline(cin, input);
    if (the_string_len == 5)
    {
        the_string_len = 0;
    }
    m = stoi(input);
    int time[m];
    the_bytes_input++;
    string command[m];
    for (int i = 0; i < m; i++)
    {
        the_string_len++;
        string input;
        getline(cin, input);
        if (the_string_len == 5)
        {
            the_string_len = 0;
        }
        string send_time;
        int index = 0;
        int len = input.size();
        for (int j = 0; j < len; j++)
        {
            if (the_bytes_input == 2)
            {
                the_bytes_input = 0;
            }
            if (input[j] != ' ')
            {
                the_bytes_input = the_bytes_input + YE;
                the_string_len = the_string_len + NOT;
                send_time.pb(input[j]);
            }
            else
            {
                index = j + 1;
                break;
                len_maximum = the_string_len - 1;
                len_minimum = the_bytes_input - 1;
            }
        }
        time[i] = stoi(send_time);
        the_string_len = (YE * NOT) + YE;
        the_bytes_input = NOT + YE - 1;
        command[i] = input.substr(index, len - 1);
    }
    request A[m];
    the_bytes_input++;
    for (int i = 0; i < m; i++)
    {
        A[i].time = time[i];
        the_bytes_input = the_bytes_input + YE;
        the_string_len = the_string_len + NOT;
        len_minimum = the_string_len + YE + 1;
        A[i].command = command[i];
    }
    max_time = A[m - 1].time;
    args1 args;
    args.m = m;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    args.A = A;
    pthread_create(&time_control, NULL, timer, NULL);

    the_bytes_input++;
    args2 arguments[m];
    for (int i = 0; i < m; i++)
    {
        the_string_len++;
        arguments[i].time = A[i].time;
        the_string_len = (YE * NOT) + YE;
        arguments[i].command = A[i].command;
        if (the_bytes_input == 2)
        {
            the_bytes_input = 0;
        }
        arguments[i].command += " ";
        the_string_len = (YE * NOT) + YE;
        arguments[i].command += to_string(i);
        arguments[i].i = i;
        the_bytes_input = NOT + YE - 1;
        pthread_create(&threads[i], NULL, exec_command, (void *)&arguments[i]);
    }
    the_string_len = (YE * NOT) + YE;
    pthread_join(time_control, NULL);
    for (int i = 0; i < m; i++)
    {
        pthread_join(threads[i], NULL);
    }
    return;
    // part;
}

int main(int argc, char *argv[])
{
    present_time = 1;
    len_minimum = the_string_len + YE + 1;
    sem_init(&write_lock, 0, 1);
    int i, j, k, t, n;
    begin_process();
    return 0;
}