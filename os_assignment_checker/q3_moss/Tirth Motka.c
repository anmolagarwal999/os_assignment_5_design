
//###########FILE CHANGE ./main_folder/Tirth Motka_305889_assignsubmission_file_/q3/q3client.c ####################//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

void *clienthread(void *args)
{
    int sock = *(int *)args;
    char buffer[1024];
    int read_size;
    while (1)
    {
        read_size = recv(sock, buffer, 1024, 0);
        if (read_size == 0)
        {
            puts("Client disconnected");
            fflush(stdout);
            break;
        }
        else if (read_size == -1)
        {
            perror("recv failed");
            break;
        }
        printf("%s\n", buffer);
        fflush(stdout);
    }
    close(sock);
    return 0;
}
int main(int argc, char *argv[])
{
    int serverSocket, newSocket, portNumber, addr_size, i;
    struct sockaddr_in serverStorage, clientStorage;
    portNumber = atoi(argv[1]);
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
    {
        puts("Socket failed");
        exit(1);
    }
    memset(&serverStorage, 0, sizeof(serverStorage));
    serverStorage.sin_family = AF_INET;
    serverStorage.sin_port = htons(portNumber);
    serverStorage.sin_addr.s_addr = INADDR_ANY;
    if (bind(serverSocket, (struct sockaddr *)&serverStorage, sizeof(serverStorage)) == -1)
    {
        puts("Bind failed");
        exit(1);
    }
    if (listen(serverSocket, 3) == -1)
    {
        puts("Listen failed");
        exit(1);
    }
    addr_size = sizeof(clientStorage);
    while (1)
    {
        newSocket = accept(serverSocket, (struct sockaddr *)&clientStorage, &addr_size);
        if (newSocket == -1)
        {
            puts("Accept failed");
            exit(1);
        }
        puts("Connection accepted");
        pthread_t tid;
        pthread_create(&tid, NULL, clienthread, &newSocket);
        pthread_join(tid, NULL);

    }
    return 0;
    
}
//###########FILE CHANGE ./main_folder/Tirth Motka_305889_assignsubmission_file_/q3/q3server.c ####################//

#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
sem_t x, y;
pthread_t tid;
pthread_t writerthreads[100];
pthread_t readerthreads[100];
int readercount = 0;
void* reader(void* param){
    int i = *(int*)param;
    sem_wait(&y);
    sem_wait(&x);
    printf("Reader %d is reading\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);

}
void* writer(void* param){
    int i = *(int*)param;
    sem_wait(&x);
    sem_wait(&y);
    printf("Writer %d is writing\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);
}
int main()
{
    int i, j, k, n, m, portNumber, addr_size, sock, newSocket, *newSocketPtr;
    struct sockaddr_in serverStorage, clientStorage;
    portNumber = atoi("8080");
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        puts("Socket failed");
        exit(1);
    }
    serverStorage.sin_family = AF_INET;
    serverStorage.sin_port = htons(portNumber);
    serverStorage.sin_addr.s_addr = INADDR_ANY;
    memset(serverStorage.sin_zero, '\0', sizeof(serverStorage.sin_zero));
    addr_size = sizeof(struct sockaddr_in);
    if (bind(sock, (struct sockaddr *)&serverStorage, sizeof(serverStorage)) == -1)
    {
        puts("Bind failed");
        exit(1);
    }
    if (listen(sock, 5) == -1)
    {
        puts("Listen failed");
        exit(1);
    }
    while (1)
    {
        newSocket = accept(sock, (struct sockaddr *)&clientStorage, &addr_size);
        if (newSocket == -1)
        {
            puts("Accept failed");
            exit(1);
        }
        newSocketPtr = malloc(sizeof(int));
        *newSocketPtr = newSocket;
        pthread_create(&tid, NULL, reader, newSocketPtr);
        pthread_join(tid, NULL);
        pthread_create(&tid, NULL, writer, newSocketPtr);
        pthread_join(tid, NULL);
    }
    close(sock);
    return 0;
	
}
