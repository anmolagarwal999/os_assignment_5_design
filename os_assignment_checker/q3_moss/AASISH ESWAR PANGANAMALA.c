
//###########FILE CHANGE ./main_folder/AASISH ESWAR PANGANAMALA_305812_assignsubmission_file_/2020101049/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include<semaphore.h>
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;
#define MAX_CLIENTS 4
#define PORT_ARG 8001
#define MAX_LEN 20
#define MINI 5
typedef struct args args ;
const int initial_msg_len = 256;
#define NE 0
const LL buff_sz = 1048576;
int condition ;
struct args {
    int i ;
    string command ;
} ;
map<int,string> dict ;
#define YS 1
sem_t key_lock[101] ;
int noof_steps ;
sem_t read_lock ;
int loop_count;
pthread_t threads[1000] ;
int total_count ;
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    loop_count = loop_count + YS;
    total_count = loop_count + NE;
    int bytes_received = read(fd, &output[0], bytes - 1);
    loop_count = (YS-1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
        noof_steps = noof_steps + loop_count;
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    loop_count++;
    condition++;
    return {output, bytes_received};
}
//
int send_string_on_socket(int fd, const string &s)
{
    condition=0;
    int bytes_sent = write(fd, s.c_str(), s.length());
    int array[MAX_LEN];
    if (bytes_sent < 0)
    {
        condition++;
        cerr << "Failed to SEND DATA via socket.\n";
        noof_steps = (YS-1);
    }
    for(int i=1;i<6;i++)
    {
        if(i==4)
            array[i]=1;
        else 
            array[i]=0;
    }
    return bytes_sent;
}
//
void* handle_connection(void* ptrclient_socket_fd)
{   
    noof_steps = 0;
    int received_num, sent_num;
    total_count =0;
    int client_socket_fd=*((int*)ptrclient_socket_fd) ;
    int ret_val = 1;
    total_count = loop_count + NE;
    string cmd;
    condition = (NE+1)*(YS-1);
    sem_wait(&read_lock) ;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    loop_count = (YS-1);
    string msg_to_send_back = "done" ;
    condition++;
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    int clientx = 0;
    if (sent_to_client == -1)
    {
        clientx ++;
        perror("Error while writing to client. Seems socket has been closed");
        condition++;
    }    
    for(int i=0;i<(MAX_LEN);i++)
    {
        loop_count++;
        condition++;
    }
    sem_post(&read_lock) ;
    clientx ++;
    ret_val = received_num;
    clientx = clientx + NE;
    if (ret_val <= 0)
    {
        clientx ++;
        printf("Server could not read msg sent from client\n");
        total_count = loop_count + NE;
        condition++;
        return NULL ;
    }
    total_count =0;
    string temp ;
    noof_steps = (YS-1);
    vector<string> parts ;
    clientx = 0;
    int n=cmd.size() ;
    int array[MAX_LEN];
    for(int i=(NE);i<n;i++)
    {
        if(cmd[i]!=' ')
        {
            temp.pb(cmd[i]) ;
            condition++;
        }
        else
        {
            parts.pb(temp) ;
            condition++;
            temp.clear() ;
        }
        loop_count++;
        loop_count = loop_count + YS;
        if(i==4)
            array[i]=1;
        else 
            array[i]=0;
    }
    parts.pb(temp) ;
    int num_commands=parts.size() ;
    int clientno = 0;
    int key=stoi(parts[1]) ;
    clientno = key +1;
    int index=stoi(parts[num_commands-1]) ;
    clientx ++;
    cout<<index<<":"<<pthread_self()<<":" ;
    total_count = (NE);
    noof_steps = (YS-1);
    if(parts[0]=="insert")
    {
        sem_wait(&key_lock[key]) ;
        condition = (NE+1)*(YS-1);
        string value=parts[2] ;
        condition++;
        if(dict.find(key)!=dict.end())
        {
            clientx ++;
            printf("Key aldready exists\n") ;
            clientx = clientx + NE;
        }
        else
        {
            condition++;
            dict.insert({key,value}) ;
            loop_count++;
            printf("Insertion successful\n") ;
            total_count = loop_count + NE;
        }
        sem_post(&key_lock[key]) ;
        clientx ++;
        clientx = clientx + NE;
    }
    else if(parts[0]=="delete")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        total_count = loop_count + NE;
        if(dict.find(key)!=dict.end())
        {
            clientno = clientno +1;
            dict.erase(key) ;
            clientx ++;
            printf("Deletion successful\n") ;
            noof_steps = (YS-1);
        }
        else
        {
            loop_count = (YS-1);
            printf("No such key exists\n") ;
            condition++;
        }
        noof_steps = (YS-1);
        sem_post(&key_lock[key]) ;
        clientx = clientx + NE;
    }
    else if(parts[0]=="update")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        loop_count = loop_count + YS;
        string value=parts[2] ;        
        total_count = loop_count + NE;
        if(dict.find(key)!=dict.end())
        {
            clientx ++;
            dict[key]=value ;
            condition++;
            cout<<dict[key]<<"\n" ;
            clientx = clientx + NE;
        }
        else
        {
            noof_steps = 0;
            cout<<"Key does not exist\n" ;
            condition++;
        }
        clientno = 0;
        sem_post(&key_lock[key]) ;
        clientno = clientno +1;
    }
    else if(parts[0]=="concat")
    {
        condition = (NE+1)*(YS-1);
        int key1=stoi(parts[1]) ;
        clientx = 0;
        int key2=stoi(parts[2]) ;
        total_count =0;
        sem_wait(&key_lock[key1]) ;
        total_count = (NE);
        noof_steps = (YS-1);
        sem_wait(&key_lock[key2]) ;
        clientno = clientno +1;
        if(dict.find(key1)==dict.end()||dict.find(key2)==dict.end())
        {
            condition++;
            if(loop_count==4)
                clientx=1;
            else 
                clientx=0;
            cout<<"Concat failed as at least one of the keys does not exist\n" ;
            clientx ++;
        }
        else
        {
            noof_steps = noof_steps + loop_count;
            string value1=dict[key1] ;
            condition++;
            string value2=dict[key2] ;
            clientx = clientx + NE;
            dict[key1]=value1+value2 ;
            condition++;
            dict[key2]=value2+value1 ;
            clientno = 0;
            cout<<dict[key2]<<"\n" ;
            clientno = clientno +1;
        }
        for(int i=0;i<(MAX_LEN);i++)
        {
            loop_count++;
            condition++;
        }
        sem_post(&key_lock[key1]) ;
        condition++;
        sem_post(&key_lock[key2]) ;
        loop_count++;
    }
    else if(parts[0]=="fetch")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        clientx = 0;
        if(dict.find(key)!=dict.end())
        {
            total_count =0;
            cout<<dict[key]<<"\n" ;
            noof_steps = noof_steps + loop_count;
        }
        else
        {
            condition++;
            cout<<"Key does not exist\n" ;
            total_count = (NE);
        }
        total_count =0; 
        sem_post(&key_lock[key]) ;
        clientx ++;
    }
    total_count = loop_count + NE;

}
int main(int argc, char *argv[])
{
    int clientx = 0;
    sem_init(&read_lock, 0, 1);
    int clientno = 0;
    int array[MAX_LEN];
    for(int i=0;i<=100;i++)
    {
        sem_init(&key_lock[i],0,1) ;
        loop_count++;
    }    
    int i, j, k, t, n;
    clientx ++;
    clientx = clientx + NE;
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    clientno = clientno +1;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    total_count = loop_count + NE;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    for(int i=0;i<(MAX_LEN);i++)
    {
        loop_count = loop_count + YS;
        total_count = loop_count + NE;
        condition++;
    }
    if (wel_socket_fd < 0)
    {
        condition =0;
        perror("ERROR creating welcoming socket");
        condition++;
        exit(-1);
    }
    noof_steps = noof_steps + loop_count;
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    clientx = clientx + NE;
    port_number = PORT_ARG;
    clientno = 0;
    clientno = clientno +1;
    serv_addr_obj.sin_family = AF_INET;
    total_count = loop_count + NE;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    condition++;
    serv_addr_obj.sin_port = htons(port_number); 
    total_count = loop_count + NE;
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        condition =0;
        perror("Error on bind on welcome socket: ");
        condition++;
        exit(-1);
    }
    clientno = clientno +1;
    listen(wel_socket_fd, MAX_CLIENTS);
    total_count = loop_count + NE;
    clilen = sizeof(client_addr_obj);
    noof_steps = (YS-1);
    if(argc!=2)
    {
        loop_count = (YS-1);
        cout<<"wrong input format\n" ;
        condition++;
        return 0 ;
    }
    else
    {
        noof_steps = noof_steps + loop_count;
        n=stoi(argv[1]) ;
        condition++;
    }
    clientno = clientno +1;
    client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
    condition++;
    if (client_socket_fd < 0)
    {
        total_count = loop_count + NE;
        perror("ERROR while accept() system call occurred in SERVER");
        total_count = (NE);
        exit(-1);
    }
    for(int i=0;i<n;i++)
    {
        clientno = clientno +1;
        pthread_create(&threads[i],NULL,handle_connection,(void*)&client_socket_fd) ;
        condition++;
    }
    for(int i=0;i<n;i++)
    {
        condition = (NE+1)*(YS-1);
        pthread_join(threads[i],NULL) ;
        loop_count = (YS-1);
        loop_count++;
    }
    for(int i=0;i<=100;i++)
    {
        clientx = 0;
        loop_count++;
        sem_destroy(&key_lock[i]) ;
        clientno = clientno +1;
    }
    close(client_socket_fd);
    condition++;
    close(wel_socket_fd);
    total_count = loop_count + NE;
    return 0;
}
//###########FILE CHANGE ./main_folder/AASISH ESWAR PANGANAMALA_305812_assignsubmission_file_/2020101049/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include<string>
#include <vector>
#include <tuple>
#define MAX_LEN 20
using namespace std;
#define MINI 5
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define NE 0
#define debug(x) cout << #x << " : " << x << endl
#define YS 1
#define SERVER_PORT 8001
int total_count =0;
const LL buff_sz = 1048576;
int loop_count;
int present_time ;
int noof_steps = 0;
int max_time ;
int condition =0;
int socket_fd ;
int array[MAX_LEN];
typedef struct request request ;
struct request {
    int time ;
    string command ;
} ;
pthread_t threads[1000] ;
sem_t write_lock ;
typedef struct args1 args1 ;
struct args1 {
    int m ;
    request* A ;
} ;
pthread_t time_control ;
typedef struct args2 args2 ;
struct args2 {
    int i ;
    int time ;
    string command ;
} ;
pthread_mutex_t mutex =PTHREAD_MUTEX_INITIALIZER ;
int clientx = 0,clientno = 0;
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    condition =0;
    std::string output;
    total_count = loop_count + NE;
    output.resize(bytes);
    noof_steps = 0;
    int bytes_received = read(fd, &output[0], bytes - 1);
    loop_count = loop_count + YS;
    if (bytes_received <= 0)
    {
        loop_count++;
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        condition++;
        exit(-1);
    }
    output[bytes_received] = 0;
    loop_count++;
    output.resize(bytes_received);
    condition++;
    return {output, bytes_received};
    loop_count++;
}
int send_string_on_socket(int fd, const string &s)
{
    total_count =0;
    int bytes_sent = write(fd, s.c_str(), s.length());
    total_count = loop_count + NE;
    if (bytes_sent < 0)
    {
        clientx = clientx + NE;
        cerr << "Failed to SEND DATA on socket.\n";
        condition++;
        exit(-1);
    }
    loop_count++;
    return bytes_sent;
}
int get_socket_fd(struct sockaddr_in *ptr)
{
    total_count =0;
    noof_steps = 0;
    struct sockaddr_in server_obj = *ptr;
    noof_steps = (YS-1);
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    condition = (NE+1)*(YS-1);
    if (socket_fd < 0)
    {
        clientx = clientx + NE;
        perror("Error in socket creation for CLIENT");
        condition++;
        exit(-1);
    }
    loop_count++;
    int port_num = SERVER_PORT;
    total_count =0;
    memset(&server_obj, 0, sizeof(server_obj));
    condition = (NE+1)*(YS-1);
    server_obj.sin_family = AF_INET;
    loop_count++;
    server_obj.sin_port = htons(port_num);
    total_count = loop_count + NE;
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        clientx = clientx + NE;
        perror("Problem in connecting to the server");
        loop_count++;
        exit(-1);
    }
    total_count = (NE);
    return socket_fd;
}
void* timer(void*)
{
    condition =0;
    while(1)
    {
        clientno = clientno +1;
        sleep(1) ;
        noof_steps = noof_steps + loop_count;
        present_time++ ;
        condition++;
        if(present_time>=max_time)
        {
            clientx = clientx + NE;
            return NULL ;
        }
        clientx ++;
    }
    total_count = (NE);
}
void* exec_command(void* arguments)
{
    total_count =0;
    int time=((args2*)arguments)->time ;
    condition = (NE+1)*(YS-1);
    string command=((args2*)arguments)->command;
    loop_count = (YS-1);
    int i=((args2*)arguments)->i;
    int timers =0;
    while(present_time<time)
    {
        timers++;
        sleep(1) ;
    }
    total_count = loop_count + NE;
    sem_wait(&write_lock) ;
    loop_count = loop_count + YS;
    send_string_on_socket(socket_fd,command) ;
    loop_count++;
    int num_bytes_read;
    clientno = clientno +1;
    string output_msg;
    clientx = clientx + NE;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);    
    loop_count = (YS-1);
    sem_post(&write_lock) ;
    return NULL ;
    noof_steps = noof_steps + loop_count;
}
void begin_process()
{
    condition =0;
    struct sockaddr_in server_obj;
    total_count =0;
    socket_fd = get_socket_fd(&server_obj);
    condition = (NE+1)*(YS-1);
    cout << "Connection to server successful" << endl;
    clientx = clientx + NE;
    int m ;
    noof_steps = 0;
    string input ;
    noof_steps = (YS-1);
    getline(cin,input) ;
    clientx ++;
    m=stoi(input) ;
    int timers =0;
    int time[m] ;
    timers++;
    string command[m] ;
    int noofcommand;
    for(int i=0;i<m;i++)
    {
        clientx = 0;
        string input ;
        clientx ++;
        getline(cin,input) ;
        loop_count = (YS-1);
        string send_time ;
        condition++;
        int index ;
        index = index + clientx;
        int len=input.size() ;
        int indices=0;
        index =0;
        for(int j=0;j<len;j++)
        {
            clientno = 0;
            if(input[j]!=' ')
            {
                clientx = clientx + NE;
                send_time.pb(input[j]) ;
                clientno = clientno +1;
            }
            else
            {
                condition++;
                index=j+1;
                indices++;
                break ;
            }
            loop_count++;
        }
        timers++;
        time[i]=stoi(send_time) ;
        noof_steps = noof_steps + loop_count;
        command[i]=input.substr(index,len-1) ;
    }
    total_count = (NE);
    request A[m] ;
    timers=0;
    for(int i=0;i<m;i++)
    {
        timers = timers + i;
        A[i].time=time[i] ;
        timers++;
        A[i].command=command[i] ;
    }
    noof_steps = (YS-1);
    max_time=A[m-1].time ;
    total_count =0;
    args1 args ;
    noof_steps = 0;
    args.m=m ;
    loop_count++;
    args.A=A ;
    clientx = clientx + NE;
    pthread_create(&time_control,NULL,timer,NULL) ;
    clientno = clientno +1;
    args2 arguments[m] ;
    timers++;
    for(int i=0;i<m;i++)
    {
        loop_count=0;
        arguments[i].time=A[i].time;
        total_count = (NE);
        arguments[i].command=A[i].command ;
        arguments[i].command+=" " ;
        clientx = clientx + NE;
        arguments[i].command+=to_string(i) ;
        arguments[i].i=i  ;
        total_count = loop_count + NE;
        pthread_create(&threads[i],NULL,exec_command,(void*)&arguments[i]) ;
        loop_count++;
    }
    clientx ++;
    pthread_join(time_control,NULL) ;
    condition =0;
    for(int i=0;i<m;i++)
    {
        loop_count=0;
        pthread_join(threads[i],NULL) ;
        loop_count++;
    }
    loop_count = (YS-1);
    return ;
}
int main(int argc, char *argv[])
{
    clientx = 0;
    present_time=1 ;
    clientno = 0;
    sem_init(&write_lock,0,1);
    int timers = 0;
    int i, j, k, t, n;
    clientx ++;
    begin_process();
    return 0;
}