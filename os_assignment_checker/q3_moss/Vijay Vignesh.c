
//###########FILE CHANGE ./main_folder/Vijay Vignesh_305826_assignsubmission_file_/2019113001_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <queue>
#include <map>
#include <pthread.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

// queue to maintain requests
queue<int> request_queue;

// maintain worker thread pool
pthread_t* workers;

// mutex and cond for request_queue
pthread_mutex_t request_queue_mutex;
pthread_cond_t request_queue_cond;

// global dictionary used as database for server
map<int, string> mangodb;

// mutex lock for global dictionary
pthread_mutex_t mangodb_mutex;


const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

// utility function to tokenize_input string given delimiter
vector<string> tokenizeInput(string str, string delim) {
    vector<string> res = {};
    size_t pos = 0;
    while ((pos = str.find(delim)) != string::npos) {
        string token = str.substr(0, pos);
        if (token.length() > 0) res.push_back(token);
        str.erase(0, pos + delim.length());
    }
    if (str.length() > 0) res.push_back(str);
    return res;
}

// functions to handle commands ---
string handleInsert(int key, string value) {
    pthread_mutex_lock(&mangodb_mutex);
    if (mangodb.find(key) != mangodb.end()){pthread_mutex_unlock(&mangodb_mutex); return "Key already exists!";}
    else {mangodb.insert({key, value});pthread_mutex_unlock(&mangodb_mutex); return "Insertion successful";}
}
string handleDelete(int key) {
    pthread_mutex_lock(&mangodb_mutex);
    if (mangodb.find(key) == mangodb.end()) {pthread_mutex_unlock(&mangodb_mutex); return "No such key exists";}
    else {mangodb.erase(key);pthread_mutex_unlock(&mangodb_mutex);return "Deletion successful";}
}
string handleFetch(int key) {
    pthread_mutex_lock(&mangodb_mutex);
    auto itr = mangodb.find(key);
    if (itr == mangodb.end()) {pthread_mutex_unlock(&mangodb_mutex);return "Key does not exist";}
    else {pthread_mutex_unlock(&mangodb_mutex);return itr->second;}
}
string handleUpdate(int key, string value) {
    pthread_mutex_lock(&mangodb_mutex);
    auto itr = mangodb.find(key);
    if (itr == mangodb.end()) {pthread_mutex_unlock(&mangodb_mutex);return "Key does not exist";}
    else {itr->second = value;pthread_mutex_unlock(&mangodb_mutex);return value;}
}
string handleConcat(int key1, int key2) {
    pthread_mutex_lock(&mangodb_mutex);
    auto itr1 = mangodb.find(key1); auto itr2 = mangodb.find(key2);
    if ((itr1 == mangodb.end()) || (itr2 == mangodb.end())) {pthread_mutex_unlock(&mangodb_mutex);return "Concat failed as at least one of the keys does not exist";}
    else {string temp = itr2->second;itr2->second += itr1->second;itr1->second += temp;pthread_mutex_unlock(&mangodb_mutex);return itr2->second;}
}
// --- end of handling functions.

string handleQuery(string command) {
    
    vector<string> args = tokenizeInput(command, " ");
    string msg = "Invalid arguments!";
    
    if (args.size() == 2)
    {
        if     (args[0] == "delete")  msg = handleDelete(stoi(args[1]));
        else if(args[0] == "fetch")   msg = handleFetch(stoi(args[1]));
    }
    else if(args.size() == 3)
    {
        if     (args[0] == "insert")  msg = handleInsert(stoi(args[1]), args[2]);
        else if(args[0] == "update")  msg = handleUpdate(stoi(args[1]), args[2]);
        else if (args[0] == "concat") msg = handleConcat(stoi(args[1]), stoi(args[2]));
    }
    
    return msg;
}

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = to_string(pthread_self()) + ":" + handleQuery(cmd);

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        
        // delay b4 response
        sleep(3);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void* workerRoutine(void* args)
{
    while(1)
    {
        pthread_mutex_lock(&request_queue_mutex);
        while(request_queue.empty())
            pthread_cond_wait(&request_queue_cond, &request_queue_mutex);
        int client_socket_fd = request_queue.front();
        request_queue.pop();
        pthread_mutex_unlock(&request_queue_mutex);

        handle_connection(client_socket_fd);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int workers_size = 10;
    if(argc > 1) workers_size = atoi(argv[1]);
    workers = (pthread_t*)calloc(workers_size, sizeof(pthread_t));

    // create worker threads
    for (int i = 0; i < workers_size; i++) 
        if (pthread_create(&workers[i], NULL, &workerRoutine, NULL)) 
            perror("Error creating worker thread");

    // init the mutex and cond
    pthread_mutex_init(&request_queue_mutex, NULL);
    pthread_mutex_init(&mangodb_mutex, NULL);
    pthread_cond_init(&request_queue_cond, NULL);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT"<< endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        pthread_mutex_lock(&request_queue_mutex);
        request_queue.push(client_socket_fd);
        pthread_mutex_unlock(&request_queue_mutex);
        pthread_cond_signal(&request_queue_cond);
    }

    // destroy mutexes and conds
    pthread_mutex_destroy(&request_queue_mutex);
    pthread_mutex_destroy(&mangodb_mutex);
    pthread_cond_destroy(&request_queue_cond);

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Vijay Vignesh_305826_assignsubmission_file_/2019113001_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

// request structure ---
struct Request {
    int idx;
    int delay_time;
    string command;
};
// --- end of request structure

// client routine ---
void* clientRoutine(void* args) {
    struct Request *request = (struct Request*)args;

    // delay time
    sleep(request->delay_time);

    struct sockaddr_in server_addr;
    int socket_fd = get_socket_fd(&server_addr);
    int bytes_sent = send_string_on_socket(socket_fd, request->command);
    if (bytes_sent == -1) cerr << "Error writing to client, socket may be closed.\n";

    // read server response
    int bytes_received = 0; string output_str;
    tie(output_str, bytes_received) = read_string_from_socket(socket_fd, buff_sz);
    if (bytes_received <= 0) cerr << "Error reading server message\n";

    // print info
    cout << request->idx << ":" << output_str << "\n";

    // close connection
    close(socket_fd);

    return NULL;
}
// }}}

int main(int argc, char* argv[]) {

    int request_count;
    cin >> request_count;
    cin.ignore();
    struct Request request_list[request_count];
    for (int i = 0; i < request_count; i++) {
        request_list[i].idx = i;
        cin >> request_list[i].delay_time; 
        getline(cin, request_list[i].command);
    }

    // initialize threads
    pthread_t client_threads[request_count];
    for (int i = 0; i < request_count; i++) {
        pthread_t* thread = &client_threads[i];
        struct Request* request = &request_list[i];
        if (pthread_create(thread, NULL, &clientRoutine, request)) perror("Error creating client threads");
    }

    // join threads
    for (int i = 0; i < request_count; i++) if (pthread_join(client_threads[i], NULL)) perror("Error joining client threads");

    return 0;
}