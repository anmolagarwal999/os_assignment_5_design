
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/client_defs.cpp ####################//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <bits/stdc++.h>

#include "client_defs.h"

int print_q(std::vector<Request> v, int num_request){
    for (int i = 0; i < num_request; i++){
        Request r = v.at(i);

        std::cout << "Request " << r.id << ":\n";
        std::cout << "delay: " << r.delay << " type: " << r.type << "\n";
        std::cout << r.key1 << " " << r.key2 << " " << r.value << "\n\n";
    }

    return 0;
}

int parse_requests(std::vector<Request> *v, int num_requests){
    for (int i = 0; i < num_requests; i++){
        Request r;

        char command[20];
        command[0] = '\0';
        r.id = i;
        r.key1 = 0;
        r.key2 = 0;
        r.value = "";

        scanf("%d %s", &r.delay, command);

        if (!strcmp(command,"insert"))
            r.type = '0';
        else if(!strcmp(command,"delete"))
            r.type = '1';
        else if(!strcmp(command,"update"))
            r.type = '2';
        else if(!strcmp(command,"concat"))
            r.type = '3';
        else if(!strcmp(command,"fetch"))
            r.type = '4';
        else{
            return 1;
        }

        switch(r.type){
            case '0':         // add
            case '2':         // update
                std::cin >> r.key1 >> r.value;
                break;
            case '1':         // del
            case '4':         // fetch
                std::cin >> r.key1;
                break;
            case '3':         // concat
                std::cin >> r.key1 >> r.key2;
                break;
            case '5':         // exit simulation
                break;
            default:
                std::cout << "Invalid input";
                return -1;
                break;
        }

        (*v).push_back(r);
    }

    return 0;
}

int connect_to_dict(int portno){
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buf[256];
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server = gethostbyname("localhost");

    // setup serv_addr
    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char*) server->h_addr, (char*) &serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        std::cout << "error in connecting\n";
        return -1;
    }

    // std::cout << "successfully connected\n";

    return sockfd;
}

std::string my_itoa(int x){
    char buf[100];
    sprintf(buf, "%d", x);
    std::string s = buf;
    return s;
}

int send_request(Request r, int sockfd){
    std::string msg = "";
    msg += r.type;

    switch(r.type){
        case '0':         // add
        case '2':         // update
            msg += " ";
            msg += my_itoa(r.key1);
            msg += " ";
            msg += r.value;
            break;
        case '1':         // del
        case '4':         // fetch
            msg += " ";
            msg += my_itoa(r.key1);
            break;
        case '3':         // concat
            msg += " ";
            msg += my_itoa(r.key1);
            msg += " ";
            msg += my_itoa(r.key2);
            break;
        case '5':         // exit simulation
            break;
    }

    // left over for as server.cpp uses getline
    msg += '\n';

    int n = dprintf(sockfd, "%s", msg.c_str());
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    char buf[256];
    bzero(buf,256);
    n = read(sockfd,buf,255);
    if (n < 0) {
        std::cout << "error in reading\n";
        return 1;
    }
    printf("%d:%s\n", r.id, buf);

    return 0;
}

int send_exit_request(int portno){
    char msg[4] = "5\n";

    int sockfd = connect_to_dict(portno);    
    int n = dprintf(sockfd, "%s", msg);
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    sleep(3);

    sockfd = connect_to_dict(portno);    
    n = dprintf(sockfd, "%s", msg);
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/dictionary.cpp ####################//

#include <iostream>
#include <bits/stdc++.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

#include "dictionary.h"


// returns 0 on success, 1 on failure
int operation_insert(Entry *dictionary, unsigned int key, std::string value){
    if (key > 100){
        std::cerr << "Invalid key\n";
        return 1;
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == false){
        dictionary[key].in_use = true;
        dictionary[key].value = value;

        pthread_mutex_unlock(&dictionary[key].mutex);
        return 0;        
    }
    else{
        pthread_mutex_unlock(&dictionary[key].mutex);
        return 1;
    }
}


int operation_delete(Entry *dictionary, unsigned int key){
    if (key > 100){
        std::cerr << "Invalid key\n";
        return 1;
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        dictionary[key].in_use = false;
        dictionary[key].value = "";

        pthread_mutex_unlock(&dictionary[key].mutex);
        return 0;        
    }
    else{
        pthread_mutex_unlock(&dictionary[key].mutex);
        return 1;
    }
}


std::pair<int, std::string> operation_update(Entry *dictionary ,unsigned int key, std::string value){
    std::pair<int, std::string> p;
    
    if (key > 100){
        std::cerr << "Invalid key\n";
        p.first = 1;
        p.second = "";
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        dictionary[key].value = value;

        p.first = 0;
        p.second = value;
    }
    else{
        p.first = 1;
        p.second = "";
    }

    pthread_mutex_unlock(&dictionary[key].mutex);
    return p;
}


std::pair<int, std::string> operation_concat(Entry *dictionary , unsigned int key1, unsigned int key2){
    std::pair<int, std::string> p;
    
    if (key1 > 100 || key2 >100){
        std::cerr << "Invalid key(s)\n";
        p.first = 1;
        p.second = "";
    }

    int l_key = (key1 > key2) ? key2 : key1;
    int g_key = (key1 > key2) ? key1 : key2;
    
    pthread_mutex_lock(&dictionary[l_key].mutex);
    pthread_mutex_lock(&dictionary[g_key].mutex);
    if (dictionary[l_key].in_use == true && dictionary[g_key].in_use == true){
        std::string a = dictionary[l_key].value;

        a += dictionary[g_key].value;
        dictionary[g_key].value += dictionary[l_key].value;
        dictionary[l_key].value = a;

        p.first = 0;
        p.second = dictionary[key2].value;
    }
    else{
        p.first = 1;
        p.second = "";
    }

    pthread_mutex_unlock(&dictionary[g_key].mutex);
    pthread_mutex_unlock(&dictionary[l_key].mutex);
    return p;
}

std::pair<int, std::string> operation_fetch(Entry *dictionary, unsigned int key){
    std::pair<int, std::string> p;
    
    if (key > 100){
        std::cerr << "Invalid key\n";
        p.first = 1;
        p.second = "";
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        p.first = 0;
        p.second = dictionary[key].value;
    }
    else{
        p.first = 1;
        p.second = "";
    }
    pthread_mutex_unlock(&dictionary[key].mutex);
    return p;   
}
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/test.cpp ####################//

#include <iostream>
#include <bits/stdc++.h>
#include <pthread.h>
#include <unistd.h>

    int count = 0;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void* thread(void* p_count){
    int *p = (int*) p_count; 
    for(int i = 0; i < 5; i++){
        pthread_mutex_lock(&m);
        std::cout << *p << " updating count to " << ++(*&count) << '\n';
        pthread_mutex_unlock(&m);
    }
    return NULL;
}

int main(void){

    pthread_t tds[5];

    pthread_mutex_init(&m, NULL);

    for (int i = 0; i < 5; i ++){
        std::pair<int, int*> p;
        // p.first = i;
        // p.second = &count;
        pthread_create(&tds[i], NULL, thread, (void*) &i);
        // sleep(1);
    }

    for(int i = 0; i < 5; i++)
        pthread_join(tds[i], NULL);

    std::cout << "final count is " << count <<'\n';
    
}


//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/server.cpp ####################//

#include <iostream>
#include <bits/stdc++.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "dictionary.h"

Entry dict[DICT_SIZE];
std::queue<FILE*> socketFP_queue;
pthread_mutex_t mutex_queue;

bool all_processed = false;
pthread_mutex_t mutex_all_processed;
Worker *worker_status;
sem_t job_available;

bool input_finished = false;
pthread_mutex_t mutex_input_finished;

void* worker_thread(void* vp_id);

int main(int argc, char** argv){
    // argv[1] holds port to use
    if (argc != 3){
        std::cout << "Usage: ./server <n> <port number>";
        return 1;
    }
    // init all
    for (int i = 0; i < DICT_SIZE; i++){
        dict[i].key = i;
        dict[i].in_use = false;
        dict[i].value = "";
        pthread_mutex_init(&dict[i].mutex, NULL);
    }
    
    pthread_mutex_init(&mutex_input_finished, NULL);
    pthread_mutex_init(&mutex_all_processed, NULL);
    pthread_mutex_init(&mutex_queue, NULL);
    sem_init(&job_available, 0, 0);

    int port_no = atoi(argv[2]);
    int num_workers = atoi(argv[1]);

    worker_status = (Worker*) malloc(sizeof(Worker) * num_workers); 
    for (int i = 0; i < num_workers; i++){
        worker_status[i].id = i;
        pthread_mutex_init(&worker_status[i].mutex, NULL);
        worker_status->is_busy = false;
        pthread_create(&(worker_status[i].tid), NULL, worker_thread, &worker_status[i]);
    }

    // set up ports
    int acceptfd, commfd, clilen, n;
    char buf[256];

    struct sockaddr_in serv_addr, cli_addr;

    // create a socket, store fd in acceptfd
    acceptfd = socket(AF_INET, SOCK_STREAM, 0);
    if (acceptfd < 0){
        perror("scoket error: ");
        std::cout << "Error in socket\n";
        return 1;
    }

    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port_no);
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    // serv_addr.sin_zero is just zero padding

    // bind fd to socket
    if (bind(acceptfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0){
        perror("bind error: ");
        return 1;
    }
    else
        std::cout << "server bound\n";

    // listen to that boundfile no.
    listen(acceptfd, num_workers);

    // keep listening for clients
    while(1){
        pthread_mutex_lock(&mutex_input_finished);
        if (input_finished){
            pthread_mutex_unlock(&mutex_input_finished);
            break;
        }
        pthread_mutex_unlock(&mutex_input_finished);

        clilen = sizeof(cli_addr);
        commfd = accept(acceptfd, (struct sockaddr*) &cli_addr, (socklen_t*) &clilen);
        if (commfd < 0){
            std::cout << "Error in accept\n";
            return 1;
        }
        FILE* commFP = fdopen(commfd, "r+");

        sem_post(&job_available);
        socketFP_queue.push(commFP);
    }
        
    // clear dict
    for (int i = 0; i < DICT_SIZE; i++)
        pthread_mutex_destroy(&dict[i].mutex);


    for(int i = 0 ; i < num_workers; i++){
        pthread_mutex_destroy(&worker_status[i].mutex);
    }
        free(worker_status);

    return 0;
}

void* worker_thread(void* vp_id){
    // FILE* commFP = (FILE*) vp_fd;
    Worker* w = (Worker*) vp_id;

    while(1){
        sem_wait(&job_available);
        pthread_mutex_lock(&mutex_queue);
        FILE* commFP = socketFP_queue.front();
        socketFP_queue.pop();
        pthread_mutex_unlock(&mutex_queue);

        pthread_mutex_lock(&w->mutex);
        w->is_busy = true;
        pthread_mutex_unlock(&w->mutex);

        // fprintf(commFP ,"thread created\n");
        // fflush(commFP);

        // std::cout << "worker thread " << w->id << " processing command\n";

        // init buffer
        char* stor = (char*) malloc(256);
        bzero(stor, 256);
        size_t n = 256;

        // read msg
        int char_read = getline(&stor, &n, commFP);
        stor[char_read - 1] = '\0';

        // std::cout << "Message recieved: " << stor << "\n";
        // transmit ack and exit routine
        if (char_read < 0){
            std::cout << "read error\n";
            fclose(commFP);
            return NULL;
        }

        // n = fprintf(commFP, "Message recieved: \"%s\"!", stor);
        // if (n < 0){
        //     std::cout << "write error\n";
        //     fclose(commFP);
        //     return NULL;
        // }

        // modify dict
        sleep(2);

        int command;
        int key1 = 0, key2 = 0;
        char value[256];
        bzero((void*) value, 256);

        int ret = 0;
        std::pair<int, std::string> p;

        // send tid
        fprintf(commFP, "%d:", w->id);

        command = stor[0] - '0';
        char* command_info = &(stor[2]);
        switch (command){
            case 0:
                sscanf(command_info, "%d %s", &key1, value);
                ret = operation_insert(dict, key1, value);
                if(!ret){
                    fprintf(commFP, "Insertion successful!");
                }
                else{
                    fprintf(commFP, "Key already exists!");
                }

                
                break;

            case 1:
                sscanf(command_info, "%d", &key1);
                ret = operation_delete(dict, key1);
                if(!ret){
                    fprintf(commFP, "Deletion successful");
                }
                else{
                    fprintf(commFP, "Key doesn't exist");
                }

                
                break;

            case 2:
                sscanf(command_info, "%d %s", &key1, value);
                p = operation_update(dict, key1, value);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "Key doesn't exist");
                }

                
                break;

            case 3:
                sscanf(command_info, "%d %d", &key1, &key2);
                p = operation_concat(dict, key1, key2);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "At least one of the keys doesn't exist");
                }

                
                break;

            case 4:
                sscanf(command_info, "%d", &key1);
                p = operation_fetch(dict, key1);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "The key doesn't exist");
                }
                
                
                break;

            case 5:
            default:
                pthread_mutex_lock(&mutex_input_finished);
                input_finished = true;
                pthread_mutex_unlock(&mutex_input_finished);
                fclose(commFP);
                return NULL;
                break;
        }

        fflush(commFP);
        fclose(commFP);
    }
}

/*
void* worker_thread(void* args){
    while(1){
        int command;
        int key1, key2;
        std::string value;

        std::pair<int, std::string> p;

        std::cin >> command;
        switch (command){
            case 0:
                std::cin >> key1 >> value;
                std::cout << "Insert returned: " << operation_insert(dict, key1, value) << "\n";
                break;
            case 1:
                std::cin >> key1;
                std::cout << "Delete returned: " << operation_delete(dict, key1) << "\n";
                break;
            case 2:
                std::cin >> key1 >> value;
                p = operation_update(dict, key1, value);
                std::cout << "Update returned: " << p.first << " and " << p.second << "\n";
                break;
            case 3:
                std::cin >> key1 >> key2;
                p = operation_concat(dict, key1, key2);
                std::cout << "concat returned: " << p.first << " and " << p.second << "\n";
                break;
            case 4:
                std::cin >> key1;
                p = operation_fetch(dict, key1);
                std::cout << "fetch returned: " << p.first << " and " << p.second << "\n";
                break;
            case 5:
            default:
                return NULL;
                break;
        } 

        fflush(stdin);
    }
}
*/
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/client_defs.h ####################//

#ifndef __ADJDSKJFHLKSJDFHDS
#define __ADJDSKJFHLKSJDFHDS

#include <string>
#include <bits/stdc++.h>
#include <vector>

struct s_request{
    int id;
    int delay;
    char type;
    int key1;
    int key2;
    std::string value;
};
typedef s_request Request;

int print_q(std::vector<Request> v, int num_request);

int parse_requests(std::vector<Request> *v, int num_requests);
int connect_to_dict(int portno);                                // connects to the server, returns the socket fd
int send_request(Request r, int sockfd);
int send_exit_request(int portno);

void* request_thread(void* vp_r);

#endif

/*
11
1 insert 21 hello
2 insert 21 hello
2 insert 22 yes
2 insert 23 no
3 concat 21 22
3 concat 21 23
4 delete 23
5 delete 24
6 concat 21 24
7 update 21 final
8 concat 21 22


2
1 insert 1 hello
2 insert 1 bye

*/
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/dictionary.h ####################//

#ifndef __ASSN_DICT_HEEEEEEEEEEADER
#define __ASSN_DICT_HEEEEEEEEEEADER

#include <pthread.h>
#include <bits/stdc++.h>
#include <stdlib.h>

#define DICT_SIZE 101
#define LISTENPORTNO 8005

struct s_dict_entry{
    int key;
    bool in_use = false;
    std::string value;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
};
typedef struct s_dict_entry Entry;

struct s_worker_status{
    int id;
    pthread_t tid;
    bool is_busy;
    pthread_mutex_t mutex;
};
typedef struct s_worker_status Worker;

int                         operation_insert(Entry *dictionary, unsigned int key, std::string value);
int                         operation_delete(Entry *dictionary, unsigned int key);
std::pair<int, std::string> operation_update(Entry *dictionary, unsigned int key, std::string value);
std::pair<int, std::string> operation_concat(Entry *dictionary, unsigned int key1, unsigned int key2);
std::pair<int, std::string> operation_fetch(Entry *dictionary, unsigned int key);

#endif
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q3/client.cpp ####################//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <vector>
#include "client_defs.h"

int portno;

int main(int argc, char** argv){
    if (argc != 2){
        std::cout << "Usage: ./client <portno>\n";
        return 1;
    }

    portno = atoi(argv[1]);

    std::vector<Request> v;
    int num_requests;

    std::cin >> num_requests;

    if(parse_requests(&v, num_requests)){
        std::cout << "Error in reading input\n";
        return 1;
    }

    // print_q(v, num_requests);

    pthread_t request_tids[num_requests];
    for (int i = 0; i < num_requests; i++){
        pthread_create(&request_tids[i], NULL, request_thread, (void*) &(v.at(i)));
    }    

    for (int i = 0; i < num_requests; i++){
        pthread_join(request_tids[i], NULL);
    }

    std::cout << "exiting\n";
    if(send_exit_request(portno)){
        std::cout << "error in exiting\n";
        return 1;
    }

    return 0;
}

void* request_thread(void* vp_r){
    Request r = *(Request*) vp_r;

    sleep(r.delay);
    int sockfd = connect_to_dict(portno);
    if (sockfd < 0){
        std::cout << "Connection error in request thread " << r.id << "\n";
        return NULL;
    }

    int err = send_request(r, sockfd);
    if (err < 0){
        std::cout << "Error while communicating with server\n";
        return NULL;
    }

    return NULL;
}