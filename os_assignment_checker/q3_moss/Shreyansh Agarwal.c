
//###########FILE CHANGE ./main_folder/Shreyansh Agarwal_305947_assignsubmission_file_/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <pthread.h>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 1
#define PORT_ARG 8002

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
queue<int> clientQueue;
pthread_mutex_t clientQueue_mutex, dict_mutex[101];
pthread_cond_t clientQueue_cond;

vector<pair<int, string>> dict;

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    // debug("readin input in server");
    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

string processInput(string input)
{
    vector<string> tokens;
    stringstream ss(input);
    string temp;
    while (ss >> temp)
        tokens.push_back(temp);
    if (tokens[0] == "insert")
    {
        if (tokens.size() != 3)
        {
            return "Invalid use of insert";
        }
        int key = strtol(tokens[1].c_str(), NULL, 10);
        if (key == EINVAL)
        {
            return "Invalid Key";
        }
        pthread_mutex_lock(&dict_mutex[key]);
        if (dict[key].first == -1)
        {
            dict[key].second = tokens[2];
            dict[key].first = 1;
        }
        else
        {
            pthread_mutex_unlock(&dict_mutex[key]);
            return "Key already present";
        }
        pthread_mutex_unlock(&dict_mutex[key]);
        return "Insertion Successful.";
    }
    else if (tokens[0] == "fetch")
    {
        if (tokens.size() != 2)
        {
            return "Invalid use of fetch";
        }
        int key = strtol(tokens[1].c_str(), NULL, 10);
        if (key == EINVAL)
        {
            return "Invalid Key";
        }
        pthread_mutex_lock(&dict_mutex[key]);
        if (dict[key].first == -1)
        {
            pthread_mutex_unlock(&dict_mutex[key]);
            return "Key not present";
        }
        else
        {
            string ans = dict[key].second;
            pthread_mutex_unlock(&dict_mutex[key]);
            return ans;
        }
    }
    else if (tokens[0] == "delete")
    {
        if (tokens.size() != 2)
        {
            return "Invalid use of delete";
        }
        int key = strtol(tokens[1].c_str(), NULL, 10);
        if (key == EINVAL)
        {
            return "Invalid Key";
        }
        pthread_mutex_lock(&dict_mutex[key]);
        if (dict[key].first == -1)
        {
            pthread_mutex_unlock(&dict_mutex[key]);
            return "Key not present";
        }
        else
        {
            dict[key].first = -1;
            pthread_mutex_unlock(&dict_mutex[key]);
            return "Deletion successful";
        }
    }
    else if (tokens[0] == "update")
    {
        if (tokens.size() != 3)
        {
            return "Invalid use of print";
        }
        int key = strtoll(tokens[1].c_str(), NULL, 10);
        if (key == EINVAL)
        {
            return "Invalid Key";
        }
        pthread_mutex_lock(&dict_mutex[key]);
        if (dict[key].first == -1)
        {
            pthread_mutex_unlock(&dict_mutex[key]);
            return "Key not present";
        }
        else
        {
            dict[key].second = tokens[2];
            pthread_mutex_unlock(&dict_mutex[key]);
            return tokens[2];
        }
    }
    else if (tokens[0] == "concat")
    {
        if (tokens.size() != 3)
        {
            return "Invalid use of print";
        }
        int key1 = strtoll(tokens[1].c_str(), NULL, 10);
        int key2 = strtoll(tokens[2].c_str(), NULL, 10);
        if (key1 == EINVAL || key2 == EINVAL)
        {
            return "Invalid key";
        }
        pthread_mutex_lock(&dict_mutex[key1]);
        if (pthread_mutex_trylock(&dict_mutex[key2]) == EBUSY)
        {
            pthread_mutex_unlock(&dict_mutex[key1]);
        }
        else
        {
            if (dict[key1].first == -1 || dict[key2].first == -1)
            {
                pthread_mutex_unlock(&dict_mutex[key1]);
                pthread_mutex_unlock(&dict_mutex[key2]);
                return "Concat failed as at least one of the keys does not exist";
            }
            else
            {
                string temp1 = dict[key1].second;
                string temp2 = dict[key2].second;
                dict[key1].second = temp1 + temp2;
                dict[key2].second = temp2 + temp1;
                string ans = dict[key2].second;
                pthread_mutex_unlock(&dict_mutex[key1]);
                pthread_mutex_unlock(&dict_mutex[key2]);
                return ans;
            }
        }
    }
    return "Invalid Input";
}

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        if (cmd == "exit")
        {
            // cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        else
        {
            string response = processInput(cmd);
            response = to_string(pthread_self()) + ": " + response;
            sleep(2);
            send_string_on_socket(client_socket_fd, response);
        }
        // pthread_t p = pthread_self();
        // cout << "Thread id: " << p << " Client sent : " << cmd << endl;
        // string msg_to_send_back = "Ack: " + cmd;

        // int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *serverFunc(void *args)
{
    int i, j, k, t, n;
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(wel_socket_fd, 1);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        // handle_connection(client_socket_fd);
        pthread_mutex_lock(&clientQueue_mutex);
        clientQueue.push(client_socket_fd);
        pthread_cond_signal(&clientQueue_cond);
        pthread_mutex_unlock(&clientQueue_mutex);
    }

    close(wel_socket_fd);
}

void *threadFunc(void *args)
{
    while (true)
    {
        pthread_mutex_lock(&clientQueue_mutex);
        while (clientQueue.empty())
        {
            pthread_cond_wait(&clientQueue_cond, &clientQueue_mutex);
        }
        int client_socket_fd = clientQueue.front();
        clientQueue.pop();
        pthread_mutex_unlock(&clientQueue_mutex);
        handle_connection(client_socket_fd);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Error: please specify the number of worker threads";
        exit(EXIT_FAILURE);
    }
    int num_threads = atoi(argv[1]);
    dict.resize(101);
    for (int i = 0; i < 101; i++)
    {
        dict[i].first = -1;
        dict[i].second = "";
    }
    pthread_mutex_init(&clientQueue_mutex, NULL);
    for (int i = 0; i < 101; i++)
    {
        pthread_mutex_init(&dict_mutex[i], NULL);
    }
    pthread_cond_init(&clientQueue_cond, NULL);
    pthread_t threads[num_threads];
    for (int i = 0; i < num_threads; i++)
    {
        pthread_create(&threads[i], NULL, threadFunc, NULL);
    }
    serverFunc(NULL);
    return 0;
}
//###########FILE CHANGE ./main_folder/Shreyansh Agarwal_305947_assignsubmission_file_/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8002
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    pair<int, string> input = *(pair<int, string> *)args;
    sleep(input.first);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    // cout << "Connection to server successful" << endl;

    string to_send = input.second;
    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << output_msg << endl;
    send_string_on_socket(socket_fd, "exit");
    return NULL;
}

int main(int argc, char *argv[])
{
    int NUM_THREADS = 2;
    cin >> NUM_THREADS;
    vector<pair<int, string>> input;
    int sleep_time;
    string temp_inp;
    for (int i = 0; i < NUM_THREADS; i++)
    {
        cin >> sleep_time;
        getline(cin, temp_inp);
        input.push_back({sleep_time, temp_inp});
    }
    pthread_t thread[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&thread[i], NULL, begin_process, (void *)&input[i]);
    }
    for (int i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(thread[i], NULL);
    }
    // begin_process();
    return 0;
}
/*
11
1 insert 1 hello
2 insert 1 hello
2 insert 2 yes
2 insert 3 no
3 concat 1 2
3 concat 1 3
4 delete 3
5 delete 4
6 concat 1 4
7 update 1 final
8 concat 1 2
*/