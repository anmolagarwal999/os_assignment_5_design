
//###########FILE CHANGE ./main_folder/Shikhar Saxena_305903_assignsubmission_file_/2021121010/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include <tuple>
#include <sstream>
#include <queue>
#include <unordered_map>
#include <vector>
using namespace std;

typedef long long LL;

#define MAX_CLIENTS 5
#define pb push_back
#define PORT_ARG 8000
#define MAX_KEYS 101

const LL buff_sz = 1048576;

queue<int> request;                  // Queue of client_socket_fd which make user requests
sem_t request_semaphore;             // Semaphore (posts with each incoming client request)
sem_t request_lock;                  // Binary Semaphore (locks modifications on request queue)

unordered_map<int, string> dict;     // To store dictionary pairs
sem_t lock[MAX_KEYS];                // Lock for each possible key (0 - 100)


pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

string insert(vector<string>& tokens) {
    
    if(tokens.size() != 3) {
        return "Usage: insert <key> <value>\n";
    }
    
    int key = stoi(tokens.at(1));
    string value = tokens.at(2);

    sem_wait(&lock[key]);

    // Check if already exists
    if(dict.count(key) > 0) {
        sem_post(&lock[key]);
        return "Key already exists\n";
    }
    
    else {
        dict.insert({key, value});
        sem_post(&lock[key]);
        return "Insertion successful\n";
    }
}

string del(vector<string>& tokens) {
    
    if(tokens.size() != 2) {
        return "Usage: delete <key>\n";
    }

    int key = stoi(tokens.at(1));
    
    sem_wait(&lock[key]);

    // Check if exists
    if(dict.count(key) > 0) {
        dict.erase(key);
        sem_post(&lock[key]);
        return "Deletion successful\n";
    }
    
    else {
        sem_post(&lock[key]);
        return "No such key exists\n";
    }
}

string update(vector<string>& tokens) {
    
    if(tokens.size() != 3) {
        return "Usage: update <key> <value>\n";
    }

    int key = stoi(tokens.at(1));
    string value = tokens.at(2);
    
    sem_wait(&lock[key]);

    auto it = dict.find(key);

    if(it == dict.end()) /* key does not exist */
    {
        sem_post(&lock[key]);
        return "Key does not exist\n";
    }

    else {
        it->second = value;
        sem_post(&lock[key]);
        return value + "\n";
    }
}

string concat(vector<string>& tokens) {
    
    if(tokens.size() != 3) {
        return "Usage: concat <key1> <key2>\n";
    }

    int key1 = stoi(tokens.at(1));
    int key2 = stoi(tokens.at(2));

    sem_wait(&lock[key1]);
    sem_wait(&lock[key2]);

    auto it1 = dict.find(key1);
    auto it2 = dict.find(key2);

    if(it1 == dict.end() || it2 == dict.end()) 
    {
        /* atleast one key does not exist */
        sem_post(&lock[key1]);
        sem_post(&lock[key2]);
        return "Concat failed as at least one of the keys does not exist\n";
    }

    else {
        string value1 = it1->second;
        string value2 = it2->second;
        it1->second = value1 + value2;
        it2->second = value2 + value1;
        sem_post(&lock[key1]);
        sem_post(&lock[key2]);
        return value2 + value1 + "\n";
    } 
}

string fetch(vector<string>& tokens) {
    
    if(tokens.size() != 2) {
        return "Usage: fetch <key>\n";
    }

    int key = stoi(tokens.at(1));

    sem_wait(&lock[key]);

    auto it = dict.find(key);

    if(it == dict.end()) /* key does not exist */
    {
        sem_post(&lock[key]);
        return "Key does not exist\n";
    }

    else {
        string value = it->second;
        sem_post(&lock[key]);
        return value + "\n";
    }
}

/* Function Names and Pointers */
string function_name[5] = {
    "insert",
    "delete",
    "update",
    "concat",
    "fetch",
};

string (*functions[5])(vector<string>&) = {
    insert,
    del,
    update,
    concat,
    fetch,
};

// Function executes the command input by given client
// And writes back the apt output
void execute(vector<string>& tokens, int client_socket_fd) {

    string output_msg = to_string(pthread_self()) + ":"; 

    int flag = 1;

    for(int i = 0; i < 5; i++) {

        if(tokens.at(0) == function_name[i]) {

            flag = 0;
            output_msg += (*functions[i])(tokens);
            break;
        }
    }

    if(flag)
        output_msg += "Invalid Command\n";

    sleep(2); /* sleep for 2 seconds before writing back output */

    int sent_to_client = send_string_on_socket(client_socket_fd, output_msg);
    
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
    }

    /* client_socket_fd closed in handle_connection function */
}

void handle_connection(int client_socket_fd)
{

    int received_num;

    /* read message from client */
    int ret_val = 1;

    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;
    
    if (ret_val <= 0)
    {
        printf("Server could not read msg sent from client\n");
        close(client_socket_fd);
        return;
    }
    
    /* Parse cmd */
    vector<string> tokens;
    
    istringstream ss(cmd);

    for(string word; ss >> word; )
        tokens.push_back(word);

    /* execute command and send output back to client */
    execute(tokens, client_socket_fd);

    close(client_socket_fd);   
}

void* worker_start(void* args) {

    do {
        int client_socket_fd;
        sem_wait(&request_semaphore);

        /* Pop client_socket_fd */
        sem_wait(&request_lock);
        client_socket_fd = request.front();
        request.pop();
        sem_post(&request_lock);

        /* Handle connection */
        handle_connection(client_socket_fd);

    } while(1);

    return NULL;
}

int main(int argc, char *argv[])
{

    if(argc != 2) {
        cerr << "Usage: ./server <worker_threads_number>\n" << flush;
        exit(1);
    }

    int no_of_worker_threads = stoi(argv[1]);

    if(no_of_worker_threads <= 0) {
        cerr << "Invalid Number of Worker threads\n" << flush;
        exit(1);
    }

    sem_init(&request_semaphore, 0, 0);
    sem_init(&request_lock, 0, 1);

    int wel_socket_fd, client_socket_fd;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(PORT_ARG);


    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    /* Init Dictionary Key Locks */
    for(int i = 0; i < MAX_KEYS; i++)
        sem_init(&lock[i], 0, 1);

    /* Create Worker threads */
    vector<pthread_t> v;

    for (int i = 0; i < no_of_worker_threads; ++i)
    {
        pthread_t p;
        v.pb(p);
        pthread_create(&v.at(i), NULL, worker_start, NULL);
    }

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */

        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        
        /* add client_socket_fd to request queue */
        sem_wait(&request_lock);
        request.push(client_socket_fd);
        sem_post(&request_semaphore);
        sem_post(&request_lock);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Shikhar Saxena_305903_assignsubmission_file_/2021121010/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <iostream>
#include <vector>
#include <tuple>
using namespace std;

typedef long long LL;

#define pb push_back
#define SERVER_PORT 8000

const LL buff_sz = 1048576;

struct user_request {
    int id; // 0 indexed
    int time;
    string command;
    pthread_t user_req_thread_id;
};

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(1);
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());

    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket\n";
        exit(1);
    }

    return bytes_sent;
}

int get_socket_fd() {

    struct sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (socket_fd < 0) {
        perror("Error in socket creation for CLIENT");
        exit(1);
    }

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(SERVER_PORT); //convert to big-endian order

    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(1);
    }

    return socket_fd;
}


void* begin_process(void* args) {

    user_request* u = (user_request*) args;
    
    sleep(u->time); /* Sleep for time before connecting */

    int socket_fd = get_socket_fd();
    
    send_string_on_socket(socket_fd, u->command);
    
    int num_bytes_read;
    string output_msg;
    
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);

    output_msg = to_string(u->id) + ":" + output_msg;

    cout << output_msg;

    close(socket_fd);
    return NULL;
}

int main(int argc, char *argv[]) {

    // Take Input
    int m;
    cin >> m;

    vector<user_request> v;

    for(int i = 0; i < m; i++) {
        user_request u;
        u.id = i;
        cin >> u.time;
        getline(cin, u.command);

        v.pb(u);
    }

    // Create User Request Threads
    for(user_request& i : v) {
        pthread_create(&i.user_req_thread_id, NULL, begin_process, &i);
    }

    // Wait for threads to complete
    for(user_request& i : v) {
        pthread_join(i.user_req_thread_id, NULL);
    }

    return 0;
}