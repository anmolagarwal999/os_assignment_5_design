
//###########FILE CHANGE ./main_folder/Sneha Raghava Raju_305784_assignsubmission_file_/2020101125_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <queue> 
#include <sstream>

#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;

/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////

#define MAX_STR_IP 400
#define ll long long int
const ll buff_sz = 1048576;

#define MAX_CLIENTS 100
#define PORT_ARG 8001


queue<int> Queue;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//lock to print statements on the terminal
pthread_mutex_t terminal_lock = PTHREAD_MUTEX_INITIALIZER;

// threads wait till it can do work
pthread_cond_t condition_variable = PTHREAD_COND_INITIALIZER;

//creating an array of structs to store the key value pairs (i.e the dictionary)
struct node
{
    int key = 0;
    string value;
    pthread_mutex_t dict_lock;  //so that the nodes are not written to at the same time               
};

vector<node> dictionary(102);

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    //printf("Message sent: %s\n", s.c_str());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

string read_string_from_socket(int fd) 
{
    std::string output;
    output.resize(buff_sz);

    int bytes_received = read(fd, &output[0], buff_sz - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Could not read data from socket. \n";
        //return NULL;
    }

    output[bytes_received] = '\0';
    output.resize(bytes_received);
    // debug(output);

    pthread_mutex_lock(&terminal_lock);
    printf("client socket: %d  Command: %s\n\n", fd, output.c_str());
    pthread_mutex_unlock(&terminal_lock);

    return output;
}

void handle_connection(int client_socket_fd, int indx)
{
    //int client_socket_fd = *((int*)ptr_client_socket_fd);

    //we can now free this, since we saved the value
    //free(ptr_client_socket_fd);

    //####################################################

    string cmd_str = read_string_from_socket(client_socket_fd);
    string output_str = to_string(indx) + ":";

    istringstream parsed_cmd(cmd_str);
    string cmd;

    //storing the first arg ('insert', 'concat', etc) into cmd
    parsed_cmd >> cmd;

    if (cmd == "insert")
    {
        //storing the key value pair given by the user into variables
        int key;
        parsed_cmd >> key;

        string value;
        parsed_cmd >> value;

        pthread_mutex_lock(&dictionary[key].dict_lock);
        if (dictionary[key].key == 0)
        {
            dictionary[key].value = value;
            dictionary[key].key = key;
            output_str += "Insertion successful";
        }
        else if(dictionary[key].key != 0)
        {
            output_str += "Key already exists";
        }
        pthread_mutex_unlock(&dictionary[key].dict_lock);
    }

    else if (cmd == "delete")
    {
        //storing the key value pair given by the user into variables
        int key;
        parsed_cmd >> key;

        pthread_mutex_lock(&dictionary[key].dict_lock);
        if(dictionary[key].key != 0)
        {
            dictionary[key].key = 0;
            output_str += "Deletion successful";
        }
        else
        {
            output_str += "No such key exists";
        }
        pthread_mutex_unlock(&dictionary[key].dict_lock);
    }

    else if (cmd == "update")
    {
        //storing the key value pair given by the user into variables
        int key;
        parsed_cmd >> key;

        string value;
        parsed_cmd >> value;

        pthread_mutex_lock(&dictionary[key].dict_lock);
        if(dictionary[key].key == key)
        {
            //dictionary[key].key = key;
            dictionary[key].value = value;
            output_str += value;
        }
        else
        {
            output_str += "Key does not exist";
        }
        pthread_mutex_unlock(&dictionary[key].dict_lock);
    }
    else if (cmd == "concat")
    {
        //storing the key value pair given by the user into variables
        int key_1, key_2;
        parsed_cmd >> key_1;
        parsed_cmd >> key_2;

        pthread_mutex_lock(&dictionary[key_1].dict_lock);
        pthread_mutex_lock(&dictionary[key_2].dict_lock);
        if( (dictionary[key_1].key != 0) && (dictionary[key_2].key != 0) )
        {
            string str_1 = dictionary[key_1].value;
            string str_2 = dictionary[key_2].value;
            
            dictionary[key_1].value = str_1 + str_2;
            dictionary[key_2].value = str_2 + str_1;

            output_str += dictionary[key_2].value;
        }
        else
        {
            output_str += "Concat failed as at least one of the keys does not exist";
        }
        pthread_mutex_unlock(&dictionary[key_1].dict_lock);
        pthread_mutex_unlock(&dictionary[key_2].dict_lock);
    }
    else if (cmd == "fetch")
    {
        //storing the key value pair given by the user into variables
        int key;
        parsed_cmd >> key;

        pthread_mutex_lock(&dictionary[key].dict_lock);
        if(dictionary[key].key != 0)
        {
            output_str += dictionary[key].value;
        }
        else
        {
            output_str += "Key does not exist";
        }
        pthread_mutex_unlock(&dictionary[key].dict_lock);
    }
    else if (cmd == "exit")
    {
        cout << "Exit pressed by client" << endl;
        //goto close_client_socket_ceremony;
        close(client_socket_fd);
        printf(BRED "Disconnected from client" ANSI_RESET "\n");
    }

    sleep(2);

    int sent_to_client = send_string_on_socket(client_socket_fd, output_str);

    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
        //goto close_client_socket_ceremony;
        close(client_socket_fd);
        printf(BRED "Disconnected from client" ANSI_RESET "\n");
    }

// close_client_socket_ceremony:
//     close(client_socket_fd);
//     printf(BRED "Disconnected from client" ANSI_RESET "\n");
//     //return NULL;
}

void *thread_func(void *arg) 
{
    int id = *(int *)arg;
    while (1)
    {
        pthread_mutex_lock(&mutex);

        while (Queue.empty())
        { 
            pthread_cond_wait(&condition_variable, &mutex);
        }

        int client_socket_fd = Queue.front();
        Queue.pop();
        pthread_mutex_unlock(&mutex);
        handle_connection(client_socket_fd, id);
    }
    return NULL;
}


int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        printf(BRED "Error: Must provide number of worker threads\n" ANSI_RESET "\n");
        exit(-1);
    }

    int THREAD_POOL_SIZE = stoi(argv[1]);

    pthread_t thread_pool[THREAD_POOL_SIZE];
    
    int num_of_nodes_in_dict = 100;

    for(int i = 0; i< num_of_nodes_in_dict; i++)
    {
        pthread_mutex_init(&dictionary[i].dict_lock, NULL);
        dictionary[i].value = "";
        dictionary[i].key = 0;
    }

    //creating the thread pool
    for(int i = 0; i< THREAD_POOL_SIZE; i++)
    {
        int *indx = (int*)malloc(sizeof(int)); 
        *indx = i;
        pthread_create(&thread_pool[i], NULL, thread_func, (void *)indx);
    }

    int i, j, k, t, n;
    
    //wel_socket - where the server listens for requests
    //client_socket- used to communicate with the client
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;

    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    
    //INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
   

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    printf(BBLU "Server has started listening on the LISTEN PORT\n" ANSI_RESET "\n");
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        //accept is a blocking call
        printf(BYEL "Waiting for a new client to request for a connection\n" ANSI_RESET);
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        //handle_connection(client_socket_fd);

        //int *pclient = malloc(sizeof(int));
        //int *pclient = (int *)malloc(sizeof(int));
        //*pclient = client_socket_fd;            //storing the value of client_socket_fd 
        
        //mutex lock to prevent race condition (i.e different threads trying to push at the same time)
        pthread_mutex_lock(&mutex);     //acquire lock
        Queue.push(client_socket_fd);     //adding to queue
        pthread_cond_signal(&condition_variable);
        pthread_mutex_unlock(&mutex);   //release lock

        // pthread_t t;
        // //pthread_create(thread, NULL, thread function, argument)
        // //for thread function we pass handle_connection because we want each thread to do this
        // //for argument we pass in client_socket_fd as a pointer (because it requires it as a pointer)
        // pthread_create(&t, NULL, handle_connection, pclient);

    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Sneha Raghava Raju_305784_assignsubmission_file_/2020101125_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstdio>
#include <sstream>

#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;

pthread_mutex_t terminal_lock = PTHREAD_MUTEX_INITIALIZER;

// client request struct
struct request
{

    int time;           //time in sec at which to execute the request
    string command;     //input command (eg: 'insert')
    pthread_t thread;
    int fd;             //file descriptor
    int index;          //request index
    pthread_mutex_t client_lock;

};
//typedef struct c_request c_request;
vector <request> requests;

string read_string_from_socket(int fd)
{
   std::string output;
    output.resize(buff_sz);

    int bytes_received = read(fd, &output[0], buff_sz - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = '\0';
    output.resize(bytes_received);

    return output;
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

void *get_socket_fd(void *ind)
{
    struct sockaddr_in server_obj;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }

    int index = *(int*) ind;
    requests[index].fd = socket_fd;

    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order
    
    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //sleep till 'time' when the request is to be executed
    int sleep_time = requests[index].time;
    sleep(sleep_time);

    pthread_mutex_lock(&requests[index].client_lock);
    if(send_string_on_socket(requests[index].fd, requests[index].command)<0) 
    {
        fprintf(stderr, BRED "Error: Could not send message" ANSI_RESET "\n"); 
        return NULL;
    }
    string str = read_string_from_socket(requests[index].fd);
    pthread_mutex_unlock(&requests[index].client_lock);    

    pthread_mutex_lock(&terminal_lock);
    printf("%d:%s\n", index, str.c_str());
    pthread_mutex_unlock(&terminal_lock);

    // //part;
    // // printf(BGRN "Connected to server\n" ANSI_RESET);
    // // part;
    // return socket_fd;
    return NULL;
}

////////////////////////////////////////////////////////

// void begin_process()
// {
//     struct sockaddr_in server_obj;
//     int socket_fd = get_socket_fd(&server_obj);


//     cout << "Connection to server successful" << endl;
    
//     while (true)
//     {
//         string to_send;
//         cout << "Enter msg: ";
//         getline(cin, to_send);
//         send_string_on_socket(socket_fd, to_send);
//         int num_bytes_read;
//         string output_msg;
//         //tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
//         cout << "Received: " << output_msg << endl;
//         cout << "====" << endl;
//     }
//     // part;
// }

int main(int argc, char *argv[])
{
    int num_of_requests = 0;

    string num_of_requests_str;
    getline(cin, num_of_requests_str);

    num_of_requests = stoi(num_of_requests_str);
    
    for(int i = 0; i < num_of_requests; i++)
    {
        string input_cmd;
        getline(cin, input_cmd);

        struct request req;

        pthread_mutex_init(&req.client_lock, NULL);

        req.index = i;

        istringstream parsed_cmd(input_cmd);

        string time;
        parsed_cmd >> time;

        req.time = stoi(time);

        string cmd = input_cmd.substr(input_cmd.find(' ') + 1, input_cmd.length());
        req.command = cmd;

        requests.push_back(req);
        //requests[i] = req;
    }

    int x = 0;

    while(x < num_of_requests)
    {
        int *indx = (int*)malloc(sizeof(int)); 
        *indx = x;
        pthread_create(&requests[x].thread, NULL, get_socket_fd, (void *)indx);
        //free(indx);
        x++;
    }

    x = 0;
    while(x < num_of_requests)
    {
        pthread_join(requests[x].thread, NULL);
        x++;
    }

    //int i, j, k, t, n;
    //begin_process();
    
    return 0;
}