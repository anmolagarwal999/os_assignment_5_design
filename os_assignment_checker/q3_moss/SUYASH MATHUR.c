
//###########FILE CHANGE ./main_folder/SUYASH MATHUR_305883_assignsubmission_file_/2019114006_assignment_5/q3/server.cpp ####################//

#include "headers.h"
using namespace std;

#define BACKLOG 2048

pthread_mutex_t pool_mutex, print_lock;
pthread_cond_t pool_cond;

queue<int*> connections;
string dict[100000];
pthread_mutex_t dict_lock[100000];

string dict_fetch(int key)
{
    pthread_mutex_lock(&dict_lock[key]);
    string ret_str;
    if(dict[key]=="")
    {
        ret_str="Key does not exist";
    }
    else
    {
        ret_str=dict[key];
    }
    pthread_mutex_unlock(&dict_lock[key]);
    return ret_str;
}

string dict_concat(int key1, int key2)
{
    pthread_mutex_lock(&dict_lock[key1]);
    pthread_mutex_lock(&dict_lock[key2]);
    string ret_str;
    if(dict[key1]=="" || dict[key2]=="")
    {
        ret_str = "Concat failed as at least one of the keys does not exist";
    }
    else
    {
        string t1=dict[key1];
        string t2=dict[key2];
        dict[key1]=t1+t2;
        dict[key2]=t2+t1;
        ret_str=t2+t1;
    }
    pthread_mutex_unlock(&dict_lock[key1]);
    pthread_mutex_unlock(&dict_lock[key2]);
    return ret_str;
}

string dict_delete(int key)
{
    pthread_mutex_lock(&dict_lock[key]);
    string ret_str;
    if(dict[key]=="")
    {
        ret_str="No such key exists";
    }
    else
    {
        dict[key]="";
        ret_str="Deletion successful";
    }
    pthread_mutex_unlock(&dict_lock[key]);
    return ret_str;
}

string dict_update(int key, string val)
{
    pthread_mutex_lock(&dict_lock[key]);
    string ret_str;
    if(dict[key]=="")
    {
        ret_str="Key does not exist";
    }
    else
    {
        dict[key]=val;
        ret_str=dict[key];
    }
    pthread_mutex_unlock(&dict_lock[key]);
    return ret_str;
}

string dict_insert(int key, string val)
{
    pthread_mutex_lock(&dict_lock[key]);
    string ret_str;
    if(dict[key]!="")
    {
        ret_str="Key already exists";
    }
    else
    {
        dict[key]=val;
        ret_str="Insertion successful";
    }
    pthread_mutex_unlock(&dict_lock[key]);
    return ret_str;
}


void* connection_handler(void* arg)
{
    // cout<<"AAAAAAAAAAAAAAAAAAAAAA";
    char buffer[MAX_BUF_SIZE+1],  sendline[MAX_BUF_SIZE+1];
    char recvline[MAX_BUF_SIZE+1];
    size_t n;
    string response;
    memset(recvline, 0, MAX_BUF_SIZE);
    int connection_fd = *(int*)arg;

    while((n=read(connection_fd, recvline, MAX_BUF_SIZE-1)) >0)
    {
        //MESSAGE
        printf("%s", recvline);
        if(recvline[n-1] == '\n')
        {
            break;
        }
        memset(recvline, 0, MAX_BUF_SIZE);
    }
    // cout<<"BBBBBBBBBBBBBBBBBBBBBBBBB";

    if(n<0)
    {
        printf("ERROR IN READING.\n");
        return NULL;
        // exit(-1);
    }

    string command, value;
    int key1, key2;
    stringstream tokenizer(recvline);
    
    tokenizer>>command;
    tokenizer>>key1;

    if(command=="insert")
    tokenizer>>value;
    else if(command=="update")
    tokenizer>>value;
    else if(command=="concat")
    tokenizer>>key2;

    if(command=="fetch")
    response = dict_fetch(key1);
    
    if(command=="insert")
    response=dict_insert(key1, value);
    
    if(command=="update")
    response=dict_update(key1, value);
    
    if(command=="delete")
    response=dict_delete(key1);
    
    if(command=="concat")
    response=dict_concat(key1, key2);

    //Send a response
    snprintf(sendline, MAX_BUF_SIZE+1, "%s", response.c_str());

    sleep(2);


    write(connection_fd, sendline, strlen(sendline));
    close(connection_fd);

    free(arg);
    return NULL;
}

void* thread_function(void* arg)
{
    while(true)
    {
        pthread_mutex_lock(&pool_mutex);
        while(connections.size() == 0)
        {
            pthread_cond_wait(&pool_cond, &pool_mutex);
        }
        int* conn = connections.front();
        // cout<<"SECCCCC";
        connections.pop();
        pthread_mutex_unlock(&pool_mutex);
        connection_handler(conn);
    }
}

int main(int argc, char** argv)
{
    if(argc!=2)
    {
        cout<<"ERROR: Invalid number of arguments."<<endl;
        exit(1);
    }
    pthread_mutex_init(&pool_mutex, NULL);
    pthread_cond_init(&pool_cond, NULL);
    pthread_mutex_init(&print_lock, NULL);

    for(int i=0;i<DICTIONARY_SIZE;i++)
    {
        pthread_mutex_init(&dict_lock[i], NULL);
        dict[i]="";
    }

    int pool_thread_count;
    pool_thread_count=atoi(argv[1]);
    pthread_t thread_pool[pool_thread_count];


    for(int i=0;i<pool_thread_count;i++)
    {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    int listenfd, connection_fd;
    struct sockaddr_in servaddr;
    char client_address[MAX_BUF_SIZE+1], readable_client_address[MAX_BUF_SIZE+1];

    //INITIALIZING SOCKET
    listenfd=socket(AF_INET, SOCK_STREAM, 0);
    if(listenfd<0)
    {
        perror("ERROR IN SOCKET");
        exit(-1);
    }

    //INITIALIZING ADDRESS
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_port = htons(PORT);
    servaddr.sin_family=AF_INET;
    servaddr.sin_addr.s_addr=htonl(INADDR_ANY);

    if(bind(listenfd, (SA*)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("Bind error");
        exit(-1);
    }

    if((listen(listenfd, BACKLOG)) <0)
    {
        perror("Listening error");
        exit(-1);
    }

    while(true)
    {
        struct sockaddr_in addr;
        socklen_t addr_length;

        printf("WAITING FOR A CONNECTION ON PORT %d\n", PORT);
        fflush(stdout);

        connection_fd = accept(listenfd, (SA*)&addr, &addr_length);

        inet_ntop(AF_INET, &addr, client_address, MAX_BUF_SIZE);
        cout<<client_address<<endl;

        
        pthread_t thrd;

        pthread_mutex_lock(&pool_mutex);
        int *pclient = (int*)malloc(sizeof(int));
        *pclient = connection_fd;
        connections.push(pclient);
        pthread_cond_signal(&pool_cond);
        pthread_mutex_unlock(&pool_mutex);
        // pthread_create(&thrd, NULL, connection_handler, (void*)pclient);
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/SUYASH MATHUR_305883_assignsubmission_file_/2019114006_assignment_5/q3/client.cpp ####################//

#include "headers.h"
using namespace std;

pthread_t client_threads[10000];
pthread_mutex_t print_lock;
struct Request
{
    int key1;
    int key2;
    string command;
    string value;
    int time_of_request;
    int id;
};

const char SERVER_ADDRESS[] = "127.0.0.1";
void* client_thread(void* arg)
{
    struct Request* req = (struct Request*)(arg);
    pthread_t thread_id = pthread_self();
    int req_no = req->id;
    sleep(req->time_of_request);

    int listenfd, connection_fd;
    char client_address[MAX_BUF_SIZE+1], readable_client_address[MAX_BUF_SIZE+1], send_data[MAX_BUF_SIZE+1], receive_data[MAX_BUF_SIZE+1];
    struct sockaddr_in servaddr;

    //INITIALIZING SOCKET
    listenfd=socket(AF_INET, SOCK_STREAM, 0);
    if(listenfd<0)
    {
        perror("ERROR IN SOCKET");
        exit(-1);
    }

    //INITIALIZING ADDRESS
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family=AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr=htonl(INADDR_ANY);

    if(inet_pton(AF_INET, SERVER_ADDRESS, &servaddr.sin_addr)<=0)
    {
        perror("Error in connecting");
        return NULL;
    }

    int connect_res=connect(listenfd, (SA*)&servaddr, sizeof(servaddr));
    if(connect_res<0)
    {
        perror("Error in connecting");
        return NULL;
    }

    if(req->command=="insert" || req->command=="update")
    sprintf(send_data, "%s %d %s\n", req->command.c_str(), req->key1, req->value.c_str());
    else if(req->command=="delete" || req->command=="fetch")
    sprintf(send_data, "%s %d\n", req->command.c_str(), req->key1);
    else if(req->command=="concat")
    sprintf(send_data, "%s %d %d\n", req->command.c_str(), req->key1, req->key2);

    int num_bytes=strlen(send_data);
    int write_result=write(listenfd, send_data, num_bytes);
    if(write_result != num_bytes)
    {
        printf("Error sending data.");
        return NULL;
    }

    memset(receive_data, 0, MAX_BUF_SIZE+1);

    pthread_mutex_lock(&print_lock);
    cout<<req_no<<":"<<thread_id<<":";
    while((num_bytes = read(listenfd, receive_data, MAX_BUF_SIZE))>0)
    {
        cout<<receive_data;
        memset(receive_data, 0, MAX_BUF_SIZE+1);
    }
    cout<<endl;
    pthread_mutex_unlock(&print_lock);

    if(num_bytes<0)
    {
        perror("Error reading bytes.");
        return NULL;
    }
    
    return NULL;
}

int main(int argc, char** argv)
{
    int socket_fd, connection_fd, n, requests_count;
    struct sockaddr_in servaddr;
    char client_address[MAX_BUF_SIZE+1], sendline[MAX_BUF_SIZE+1], recvline[MAX_BUF_SIZE+1];
    pthread_mutex_init(&print_lock, NULL);
    cin>>requests_count;
    


    for(int i=0;i<requests_count;i++)
    {
        struct Request* req = new Request();
        req->id=i;
        
        cin>>req->time_of_request >> req->command >> req->key1;
        if(req->command == "insert")
        {
            cin>>req->value;
        }
        else if(req->command=="update")
        cin>>req->value;
        else if(req->command=="concat")
        cin>>req->key2;
        pthread_create(&client_threads[i], NULL, client_thread, (void*)req);
    }

    for(auto &thread:client_threads)
    {
        pthread_join(thread, NULL);
    }
}
//###########FILE CHANGE ./main_folder/SUYASH MATHUR_305883_assignsubmission_file_/2019114006_assignment_5/q3/headers.h ####################//

#ifndef _HEADERS_H_
#define _HEADERS_H_

#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#define PORT 12000
#define SA struct sockaddr
#define MAX_BUF_SIZE 10000
#include <bits/stdc++.h>
#include <pthread.h>
#define DICTIONARY_SIZE 10000

#endif
