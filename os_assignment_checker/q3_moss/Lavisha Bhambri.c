
//###########FILE CHANGE ./main_folder/Lavisha Bhambri_305790_assignsubmission_file_/2020101088_assignment_5/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <vector>
#include <queue>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////


#define THREAD_POOL_SIZE 100
pthread_t thread_pool[THREAD_POOL_SIZE];
queue<int>q;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_var = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

// Vector that stores the pair of key and value
vector<pair<string, string>> myVector;

string insertString(string key, string val) {
    string myMsg;
    pthread_mutex_lock(&mutex2);

    for (int i = 0; i < myVector.size(); i++) {
        if (myVector[i].first == key) {
            myMsg = "Key already exists";
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    

    myMsg = "Insertion successful";
    myVector.push_back(make_pair(key, val));
    pthread_mutex_unlock(&mutex2);

    return myMsg;
}

string deleteString(string key) {
    string myMsg;
    int position = -1;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < myVector.size(); i++) {
        if (myVector[i].first == key) {
            position = i;
            myVector.erase(myVector.begin() + position);
            myMsg = "Deletion successful";
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    myMsg = "No such key exists";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}

string updateString(string key, string val) {
    string myMsg;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < myVector.size(); i++) {
        if (myVector[i].first == key) {
            myMsg = val;
            myVector[i].second = val;
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    myMsg = "Key does not exist";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}

string concatString(string key1, string key2) {
    string myMsg;
    int position1 = -1, position2 = -1;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < myVector.size(); i++) {
        if (myVector[i].first == key1)
            position1 = i;
        if (myVector[i].first == key2)
            position2 = i;
    }

    if (position1 == -1 || position2 == -1) {
        myMsg = "Concat failed as at least one of the keys does not exist";
        pthread_mutex_unlock(&mutex2);
        return myMsg;
    }

    
    string temp = myVector[position1].second;
    myVector[position1].second += myVector[position2].second;
    myVector[position2].second += temp;

    myMsg = myVector[position2].second;
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}

string fetchString(string key) {
    string myMsg;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < myVector.size(); i++) {
        if (myVector[i].first == key) {
            myMsg = myVector[i].second;
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }

    myMsg = "Key does not exist";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}


string input_handler(string input) {
    vector<string>tokenisedStrings; // stores tokenised strings
    cout <<"INPUT : " << input << endl;
    // Tokeinising the string on basis of space
    string word = "";
    for (auto x : input) 
    {
        if (x == ' ')
        {
            tokenisedStrings.push_back(word);
            word = "";
        }
        else {
            word = word + x;
        }
    }
    tokenisedStrings.push_back(word);
    
    if (tokenisedStrings[1] == "insert")
        return insertString(tokenisedStrings[2], tokenisedStrings[3]);
    
    else if (tokenisedStrings[1] == "delete")
        return deleteString(tokenisedStrings[2]);
    
    else if (tokenisedStrings[1] == "update")
        return updateString(tokenisedStrings[2], tokenisedStrings[3]);

    else if (tokenisedStrings[1] == "concat")
        return concatString(tokenisedStrings[2], tokenisedStrings[3]);
    
    return fetchString(tokenisedStrings[2]);
}



pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}


///////////////////////////////

void* handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    // free(client_socket_fd_ptr);
    //####################################################
    
    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    // while (true)
    // {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            close(client_socket_fd);
            return NULL;
            // goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit\n")
        {
            cout << "Exit pressed by client" << endl;
            close(client_socket_fd);
            return NULL;
            // goto close_client_socket_ceremony;
        }

        cout << "****************** cmd ********************" << endl;
        string msg_to_send_back = input_handler(cmd);  // Stores the output string 

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            close(client_socket_fd);
            return NULL;
            // goto close_client_socket_ceremony;
        }
    // }

// close_client_socket_ceremony:
//     close(client_socket_fd);
//     printf(BRED "Disconnected from client" ANSI_RESET "\n");
//     return NULL;
}



void* thread_function(void* ptr) {
    while (true) {

        // Called mutex lock on queue
        pthread_mutex_lock(&mutex);
        // pthread_cond_wait(&condition_var, &mutex);
        int pclient = q.front();
        
        // If queue is empty then wait 
        if (q.empty()) {
            pthread_cond_wait(&condition_var, &mutex);
            pclient = q.front(); 
            q.pop();
        }else { // Else pop the client_socket_id
            pclient = q.front(); 
            q.pop();
        }
        pthread_mutex_unlock(&mutex);
        
        handle_connection(pclient);
    }
    return NULL;
}


int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    // Create n worker threads
    for (int i = 0; i < atoi(argv[1]); i++) {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        
        pthread_mutex_lock(&mutex);
        q.push(client_socket_fd);
        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&condition_var); // To give signal when the queue is not empty

    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Lavisha Bhambri_305790_assignsubmission_file_/2020101088_assignment_5/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

// int user_counter = 0;
pthread_mutex_t scan_mutex, print_mutex, condition_mutex;




void begin_process()
{   

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);


    cout << "Connection to server successful" << endl;
    
    while (true)
    {
        string to_send = "", individual_string;
        cout << "Enter msg: ";
        int num; 
        // cin >> num;
        
        getline(cin, individual_string);
        num = stoi(individual_string);
        for (int i = 0; i < num; i++) {
            getline(cin, individual_string);
            to_send += individual_string;
            to_send += "\n";
        }
        
        send_string_on_socket(socket_fd, to_send);
        int num_bytes_read;
        string output_msg;
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        cout << output_msg;
        cout << "====" << endl;
    }
    
}

typedef struct thread_details
{
    string s; // Stores request given to the thread
    int idx; // Stores index of thread
} td;

int max_time_delay = 0;

void *my_begin_process(void *inp) {
    struct sockaddr_in server_obj;
    int thread_idx = ((struct thread_details *)inp)->idx;
    string individual_string = ((struct thread_details *)inp)->s;

    int time_delay = atoi((individual_string.substr(0, individual_string.find(" "))).c_str());
    sleep(time_delay);

    pthread_mutex_lock(&condition_mutex);
    int socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, individual_string);
    pthread_mutex_unlock(&condition_mutex);

    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);

    pthread_mutex_lock(&print_mutex);
    printf("%d:%d:%s", thread_idx,gettid(),output_msg.c_str());
    pthread_mutex_unlock(&print_mutex);
    cout << endl;

    return NULL;
}

void create_threads() {
    pthread_mutex_init(&print_mutex, NULL);
    pthread_mutex_init(&scan_mutex, NULL);
    pthread_mutex_init(&condition_mutex, NULL);

    int m; 
        
    string individual_string;
    getline(cin, individual_string);
    m = stoi(individual_string);
    pthread_t thread_ids[m + 1];
    vector<string>v(m + 1);
    
    for (int i = 0; i < m; i++) {
        getline(cin, individual_string);
        v[i] = individual_string;
        max_time_delay = max(max_time_delay, atoi((individual_string.substr(0, individual_string.find(" "))).c_str()));
    }

    v[m] = to_string(max_time_delay + 2) + " insert " + to_string(2) + " " + to_string(4);

    for (int i = 0; i < m + 1; i++) {
        pthread_t curr_thread_id;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->s = v[i];
        thread_input->idx = i;
        pthread_create(&curr_thread_id, NULL, my_begin_process, (void *)(thread_input));
        thread_ids[i] = curr_thread_id;
        
        if (i == m)
            break;
    }

    for (int i = 0; i < m; i++) {
        pthread_join(thread_ids[i], NULL);
    }

}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    create_threads();
    return 0;
}