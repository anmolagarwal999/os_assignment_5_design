
//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q3/src/common.h ####################//

#ifndef __Q3_COMMON_H
#define __Q3_COMMON_H

#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdarg.h>     /* for variadic function */
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <pthread.h>
#include <queue>

#define SERVER_PORT 6969
#define MAXLINE 4096
#define DICTIONARY_SIZE 101
#define SA struct sockaddr

// Ascii codes for 256-bit colors
#define COLOR_BLACK   "\033[0;30m"
#define COLOR_RED     "\033[0;31m"
#define COLOR_GREEN   "\033[0;32m"
#define COLOR_YELLOW  "\033[0;33m"
#define COLOR_BLUE    "\033[0;34m"
#define COLOR_MAGENTA "\033[0;35m"
#define COLOR_CYAN    "\033[0;36m"
#define COLOR_WHITE   "\033[0;37m"
#define COLOR_RESET   "\033[0m"

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q3/src/utils.h ####################//

#ifndef __Q3_UTILS_H
#define __Q3_UTILS_H

void err_n_die(const char *fmt, ...);

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q3/src/utils.cpp ####################//

#include "common.h"
#include "utils.h"

void err_n_die(const char *fmt, ...)
{
    int errno_save;
    va_list ap;

    errno_save = errno;

    fprintf(stderr, COLOR_RED);
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    fflush(stderr);

    if (errno_save != 0)
    {
        fprintf(stderr, "(errno = %d) : %s\n", errno_save, strerror(errno_save));
        fprintf(stderr, "\n");
        fprintf(stderr, COLOR_RESET);
        fflush(stderr);
    }

    va_end(ap);
    exit(1);
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q3/src/client/client.cpp ####################//

#include "../common.h"
#include "../utils.h"

char SERVER_ADDRESS[] = "127.0.0.1";

pthread_mutex_t print_mutex;

typedef struct Request
{
    int req_num;
    int time_delay;
    std::string command;
    int key1;
    int key2;
    std::string value;
} Request;

Request* create_new_request()
{
    Request* request = new Request();
    request->req_num = 0;
    request->time_delay = 0;
    request->key1 = 0;
    request->key2 = 0;
    return request;
}

void *client_thread(void *data)
{
    Request *request = (Request*)(data);
    sleep(request->time_delay);

    pthread_t thread_id = pthread_self();

    int sockfd, bytes_read, send_bytes;
    struct sockaddr_in servaddr;
    char sendline[MAXLINE], recvline[MAXLINE];

    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        err_n_die("Error while creating the socket");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(SERVER_PORT);

    if (inet_pton(AF_INET, SERVER_ADDRESS, &servaddr.sin_addr) <= 0)
        err_n_die("inet_pton error for %s\n", SERVER_ADDRESS);

    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        err_n_die("Error while connecting to the server");

    if (request->command == "concat")
        sprintf(sendline, "%s %d %d\n",\
                request->command.c_str(),\
                request->key1,\
                request->key2\
               );

    else if (request->command == "delete" || request->command == "fetch")
        sprintf(sendline, "%s %d\n",\
                request->command.c_str(),\
                request->key1\
               );

    else
        sprintf(sendline, "%s %d %s\n",\
                request->command.c_str(),\
                request->key1,\
                request->value.c_str()\
               );

    send_bytes = strlen(sendline);

    if (write(sockfd, sendline, send_bytes) != send_bytes)
        err_n_die("Error while sending data to the server");

    memset(recvline, 0, MAXLINE);

    pthread_mutex_lock(&print_mutex);
    std::cout \
        << COLOR_YELLOW << request->req_num \
        << COLOR_WHITE << ":" \
        << COLOR_BLUE << thread_id \
        << COLOR_WHITE << ":" << COLOR_GREEN;
    while ( (bytes_read = read(sockfd, recvline, MAXLINE-1)) > 0)
    {
        std::cout << recvline;
        memset(recvline, 0, MAXLINE);
    }
    std::cout << COLOR_RESET << std::endl;
    pthread_mutex_unlock(&print_mutex);

    if (bytes_read < 0)
        err_n_die("Error while reading data from the server");

    delete request;
    return NULL;
}

int main(int argc, char **argv)
{
    int sockfd, bytes_read;
    int send_bytes;
    struct sockaddr_in servaddr;
    char sendline[MAXLINE], recvline[MAXLINE];

    int num_requests = 0;
    std::cin >> num_requests;

    pthread_t client_threads[num_requests];

    pthread_mutex_init(&print_mutex, NULL);

    for (int req_num=0; req_num<num_requests; req_num++)
    {
        Request *request = create_new_request();
        request->req_num = req_num;

        std::cin >> request->time_delay;
        std::cin >> request->command;
        std::cin >> request->key1;

        if (request->command == "concat")
        {
            std::cin >> request->key2;
        }
        else if (request->command != "delete" && request->command != "fetch")
        {
            std::cin >> request->value;
        }

        pthread_create(&client_threads[req_num], NULL, client_thread, (void*)request);
    }

    for (int req_num=0; req_num<num_requests; req_num++)
    {
        pthread_join(client_threads[req_num], NULL);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q3/src/server/server.cpp ####################//

#include "../common.h"
#include "../utils.h"

#define BACKLOG 1024

void* handle_connection(void *arg);
void* thread_function(void*);

pthread_mutex_t pool_lock;
pthread_cond_t pool_ready;

typedef struct {
    int *fd;
} Connection;

std::queue<Connection*> connections;
std::string dictionary[DICTIONARY_SIZE];
pthread_mutex_t dictionary_lock[DICTIONARY_SIZE];

std::string dict_insert(int key, std::string value);
std::string dict_delete(int key);
std::string dict_update(int key, std::string value);
std::string dict_concat(int key1, int key2);
std::string dict_fetch(int key);

int main(int argc, char **argv)
{
    int num_pool_threads = 0;
    int                     listenfd, connfd, n;
    int                     sendbytes;
    struct sockaddr_in      servaddr;

    if (argc != 2)
        err_n_die("usage: %s <number of workers in thread pool>", argv[0]);

    num_pool_threads = atoi(argv[1]);

    if (num_pool_threads == 0)
        err_n_die("error: number of threads in thread pool must be greater than 0");

    pthread_t thread_pool[num_pool_threads];

    pthread_mutex_init(&pool_lock, NULL);
    pthread_cond_init(&pool_ready, NULL);

    for (int i = 0; i < DICTIONARY_SIZE; i++) {
        dictionary[i] = "";
        pthread_mutex_init(&dictionary_lock[i], NULL);
    }

    for (int thread_num=0; thread_num<num_pool_threads; thread_num++)
    {
        pthread_create(&thread_pool[thread_num], NULL, thread_function, NULL);
    }

    if ( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        err_n_die("Error while creating the socket!");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(SERVER_PORT);

    if ( (bind(listenfd, (SA *) &servaddr, sizeof(servaddr))) < 0)
        err_n_die("Error while binding the socket!");

    if (listen(listenfd, BACKLOG) < 0)
        err_n_die("Error while listening to the socket!");

    std::cout << COLOR_GREEN;
    std::cout << "Server is running on port " << SERVER_PORT;
    std::cout << " with a pool of " << num_pool_threads << " threads" << std::endl;
    std::cout << COLOR_RESET;

    for ( ; ; )
    {
        struct sockaddr_in addr;
        socklen_t addr_len;
        char client_addr[MAXLINE+1];

        printf(COLOR_YELLOW);
        printf("Waiting for connections on port %d...\n", SERVER_PORT);
        printf(COLOR_RESET);
        fflush(stdout);

        connfd = accept(listenfd, (SA *) &addr, &addr_len);

        // pthread_t conn_thread;
        pthread_mutex_lock(&pool_lock);
        int *client_pntr = (int *)malloc(sizeof(int));
        *client_pntr = connfd;
        Connection *conn = new Connection();
        conn->fd = client_pntr;
        connections.push(conn);
        pthread_cond_signal(&pool_ready);
        pthread_mutex_unlock(&pool_lock);
        // pthread_create(&conn_thread, NULL, handle_connection, (void*)client_pntr);

    }

    return 0;
}

void* thread_function(void* arg)
{
    while (true)
    {
        pthread_mutex_lock(&pool_lock);
        while (connections.size() == 0)
        {
            pthread_cond_wait(&pool_ready, &pool_lock);
        }
        Connection *conn = connections.front();
        connections.pop();
        pthread_mutex_unlock(&pool_lock);
        handle_connection(conn->fd);
    }

    return NULL;
}

void* handle_connection(void* arg)
{
    int connfd = *((int*)arg);
    free(arg);
    size_t bytes_read;
    char recvline[MAXLINE+1];
    char sendline[MAXLINE+1];

    memset(recvline, 0, MAXLINE);
    while (( bytes_read = read(connfd, recvline, MAXLINE-1) ) > 0)
    {
        printf("%sReceived new message: %s%s", COLOR_CYAN, recvline, COLOR_RESET);
        if (recvline[bytes_read-1] == '\n')
        {
            break;
        }
        memset(recvline, 0, MAXLINE);
    }

    if (bytes_read < 0)
        err_n_die("Error while reading from the socket!");

    std::stringstream tokenizer(recvline);
    std::string command;
    int key1, key2;
    std::string value;

    tokenizer >> command;
    tokenizer >> key1;

    if (command == "concat")
    {
        tokenizer >> key2;
    }
    else if (command != "delete" && command != "fetch")
    {
        tokenizer >> value;
    }

    std::string response;

    if (command == "insert")
        response = dict_insert(key1, value);
    else if (command == "delete")
        response = dict_delete(key1);
    else if (command == "update")
        response = dict_update(key1, value);
    else if (command == "concat")
        response = dict_concat(key1, key2);
    else if (command == "fetch")
        response = dict_fetch(key1);


    snprintf(sendline, MAXLINE, "%s", response.c_str());

    sleep(2);

    write(connfd, sendline, strlen(sendline));
    close(connfd);
    return NULL;
}

std::string dict_insert(int key, std::string value)
{
    pthread_mutex_lock(&dictionary_lock[key]);
    if (dictionary[key] != "")
    {
        pthread_mutex_unlock(&dictionary_lock[key]);
        return "Key already exists";
    }
    dictionary[key] = value;
    pthread_mutex_unlock(&dictionary_lock[key]);
    return "Insertion successful";
}

std::string dict_delete(int key)
{
    pthread_mutex_lock(&dictionary_lock[key]);
    if (dictionary[key] == "")
    {
        pthread_mutex_unlock(&dictionary_lock[key]);
        return "No such key exists";
    }
    dictionary[key] = "";
    pthread_mutex_unlock(&dictionary_lock[key]);
    return "Deletion successful";
}

std::string dict_update(int key, std::string value)
{
    pthread_mutex_lock(&dictionary_lock[key]);
    if (dictionary[key] == "")
    {
        pthread_mutex_unlock(&dictionary_lock[key]);
        return "Key does not exist";
    }
    dictionary[key] = value;
    std::string ret = dictionary[key];
    pthread_mutex_unlock(&dictionary_lock[key]);
    return ret;
}

std::string dict_concat(int key1, int key2)
{
    pthread_mutex_lock(&dictionary_lock[key1]);
    pthread_mutex_lock(&dictionary_lock[key2]);
    if (dictionary[key1] == "" || dictionary[key2] == "")
    {
        pthread_mutex_unlock(&dictionary_lock[key1]);
        pthread_mutex_unlock(&dictionary_lock[key2]);
        return "Concat failed as at least one of the keys does not exist";
    }
    std::string val1 = dictionary[key1], val2 = dictionary[key2];
    dictionary[key1] = val1 + val2;
    dictionary[key2] = val2 + val1;
    std::string ret = dictionary[key2];
    pthread_mutex_unlock(&dictionary_lock[key1]);
    pthread_mutex_unlock(&dictionary_lock[key2]);
    return ret;
}

std::string dict_fetch(int key)
{
    pthread_mutex_lock(&dictionary_lock[key]);
    if (dictionary[key] == "")
    {
        pthread_mutex_unlock(&dictionary_lock[key]);
        return "Key does not exist";
    }
    std::string ret = dictionary[key];
    pthread_mutex_unlock(&dictionary_lock[key]);
    return ret;
}
