
//###########FILE CHANGE ./main_folder/Sai Pranav Chunduru_305855_assignsubmission_file_/2020101033/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
/////////////////////////////
// #include <iostream>
#include <bits/stdc++.h>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;
int n;
pthread_mutex_t mylock[100] = {PTHREAD_MUTEX_INITIALIZER};

////////////////////////////////////

const LL buff_sz = 1048576;

map <int,string> dictionary;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////
int insert(string k, string value)
{
    int key = stoi(k);
    for (auto it = dictionary.begin(); it != dictionary.end(); it++)
    {
        if (it->first == key)
        {
            return 0;
        }
        
    }
    pthread_mutex_lock(&mylock[key]);
    dictionary.insert({key,value});
    pthread_mutex_unlock(&mylock[key]);
    return 1;
}

int dele(string k)
{
    int key = stoi(k);
    pthread_mutex_lock(&mylock[key]);
    int r = dictionary.erase(key);
    pthread_mutex_unlock(&mylock[key]);
    return(r);
}

int update(string k, string value)
{
    int key = stoi(k);
    pthread_mutex_lock(&mylock[key]);
    if (dictionary.erase(key))
    {
        dictionary.insert({key,value});
        return 1;
    }
    else
    {
        return 0;
    }
    pthread_mutex_unlock(&mylock[key]);
}

string concat(string a, string b)
{
    int x = stoi(a),y = stoi(b);
    for (auto it = dictionary.begin(); it != dictionary.end(); it++)
    {
        if (it->first == x)
        {
            for (auto jt = dictionary.begin(); jt != dictionary.end(); jt++)
            {
                pthread_mutex_lock(&mylock[x]);
                pthread_mutex_lock(&mylock[y]);
                if (jt->first == y)
                {
                    string con = it->second;
                    it->second = con + jt->second;
                    jt->second = jt->second + con;
                    return jt->second;
                }
                pthread_mutex_unlock(&mylock[x]);
                pthread_mutex_unlock(&mylock[y]);                
            }
            
        }
        
    }
    return "Concat failed as at least one of the keys does not exist";
}

string fetch(string k)
{
    int key = stoi(k);
    for (auto it = dictionary.begin(); it != dictionary.end(); it++)
    {
        if (it->first == key)
        {
            return it->second;
        }
        
    }
    return "Key does not exist";
}

void* foo(void* cmd)
{
        
        string *com = (string *) cmd;
        vector <string> tokens;
        stringstream check1(*com);
        string intermediate;
        string *s = (string *)malloc(sizeof(string));
        while(getline(check1, intermediate, ' '))
        {
            tokens.push_back(intermediate);
        }   
        if (tokens[1] == "insert")
        {
            int ret = insert(tokens[2],tokens[3]);
            if (ret)
            {
                *s = "Insertion successful";
            }
            else
            {
                *s = "Key already exists";
            }
            
        }
        else if (tokens[1] == "delete")
        {
            int ret = dele(tokens[2]);
            if (ret)
            {
                *s = "Deletion successful";
            }
            else
            {
                *s = "No such key exists";
            }
        }
        else if (tokens[1] == "update")
        {
            int ret = update(tokens[2],tokens[3]);
            if (ret)
            {
                *s = tokens[3];
            }
            else
            {
                *s = "No such key exists";
            }
        }
        else if (tokens[1] == "concat")
        {
            *s = concat(tokens[2],tokens[3]);
        }
        else
        {
            *s = fetch(tokens[2]);
        }
        return s;
}

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;
    // printf("Read something 2\n");
    /* read message from client */
    int ret_val = 1;
    queue <string> que;
    while (true)
    {
        string cmd,*s;
        // printf("Read something 3\n");
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        que.push(cmd);
        pthread_t threads[n];
        for (int i = 0; i < n && !que.empty(); i++)
        {
            pthread_create(&threads[i],NULL,foo,&que.front());
            que.pop();
        }
        
        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        for (int i = 0; i < n; i++)
        {
            pthread_join(threads[i],(void **)&s);
            sleep(2);
            int sent_to_client = send_string_on_socket(client_socket_fd, *s);
            if (sent_to_client == -1)
            {
                perror("Error while writing to client. Seems socket has been closed");
                goto close_client_socket_ceremony;
            }
        }
        // debug(sent_to_client);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

int main(int argc, char *argv[])
{
    int i, j, k, t;
    n = atoi(argv[argc-1]);
    debug(n);    
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        handle_connection(client_socket_fd);
    }
    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Sai Pranav Chunduru_305855_assignsubmission_file_/2020101033/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
/////////////////////////////
#include <pthread.h>
// #include <iostream>
#include <bits/stdc++.h>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
int m,socket_fd;
pthread_mutex_t mylock = PTHREAD_MUTEX_INITIALIZER;
// vector<string> comms;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////
void *inout(void *s)
{
    pthread_mutex_lock(&mylock);
    string to_send = *(string *) s;
    // cout << "sending: " << *(string *)s << " " << to_send << endl;
    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << output_msg << endl;
    pthread_mutex_unlock(&mylock);
    return NULL;
}
void begin_process()
{
    struct sockaddr_in server_obj;
    socket_fd = get_socket_fd(&server_obj);
    cout << "Connection to server successful" << endl;
    pthread_t threads[m];
    cin >> m;
    for (int i = 0; i < m; i++)
    {
        string to_send;
        getline(cin, to_send);
        // cout << "1:" << to_send << endl;  
        pthread_create(&threads[i],NULL,inout,(void *)&to_send);
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(threads[i],NULL);
    }
    
}

int main(int argc, char *argv[])
{

    int i, j, k, t;
    begin_process();
    return 0;
}