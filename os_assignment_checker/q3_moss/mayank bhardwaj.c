
//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q3/._client.cpp ####################//

///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

#include <signal.h>
#include <stdarg.h>
#include <errno.h>
///////////////////////////////////////////////////

#include <sys/time.h>
#include <sys/ioctl.h>
///////////////////////////////////////////////////

#include <netdb.h>
#include <stdio.h>
#include <iostream>
///////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
///////////////////////////////////////////////////

#include <tuple>
#include <map>
#include <iterator>
#include <string>
#include <vector>
#include <queue>  
 ///////////////////////////////////////////////////


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
///////////////////////////////////////////////////

#include <netinet/in.h>
#include <arpa/inet.h>
///////////////////////////////////////////////////

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
///////////////////////////////////////////////////



#include <assert.h>
#include <fcntl.h>

using namespace std;
///////////////////////////////////////////////////



// Primary Colours
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define BLU "\e[0;34m"

  // Ternary colours
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
  // Secondary Colours
#define BLK "\e[0;30m"
#define YEL "\e[0;33m"

// Regular color text

#define ANSI_RESET "\x1b[0m"

///////////////////////////////////////////////////

typedef struct data {
    bool busy;
    string value;
} data;
 ///////////////////////////////////////////////////

typedef struct th_args {
    
} th_args;
///////////////////////////////////////////////////

typedef long long LL;

///////////////////////////////////////////////////
#define MAX_CLIENTS 77
#define PORT_ARG 8001
///////////////////////////////////////////////////
#define part cout << "-----------------------------------" << endl;
#define debug(x) cout << #x << " : " << x << endl

#define pb push_back
///////////////////////////////////////////////////


const LL B_S = 1048576;// 1 MB = 1048576 bytes( This number symbolizes the "Battle of Life")                             // for receiving the string from client
const int initial_message_length = 256;

pthread_mutex_t socketlock = PTHREAD_MUTEX_INITIALIZER; // lock while sending string to socket 
pthread_mutex_t lock[101];                              // lock for  dictionary's key
pthread_mutex_t quelock = PTHREAD_MUTEX_INITIALIZER;    // queue lock
///////////////////////////////////////////////////

pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;      // conditional variable for multiple threads
///////////////////////////////////////////////////
queue<int *> q;                                         //  client fds' addresses queue
map<int, data> mydict;                                  //  common dictionary of server

///////////////////////////////////////////////////


pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } 
    else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    }
            else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    }  
    else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&conditionalvariable, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}

///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  */

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a NEW CLIENT's request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New CLIENT connected to port %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd; //pcli = pclient

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&conditionalvariable);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        pthread_mutex_destroy(&lock[i]);
    }

    return 0;
}







/*
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, B_S);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&_, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}
    //////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, ess;
  ect  /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    // IP address can be anything (INADDR_ANY) 
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests 
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_address_object);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&ess, &cectlilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(ess.sinect_port), inet_ntoa(ess.sinect_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd;

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&_);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        destroy_pthread_mutex(&lock[i]);
    }

    return 0;
}
*/

//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q3/._server.cpp ####################//

///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

#include <signal.h>
#include <stdarg.h>
#include <errno.h>
///////////////////////////////////////////////////

#include <sys/time.h>
#include <sys/ioctl.h>
///////////////////////////////////////////////////

#include <netdb.h>
#include <stdio.h>
#include <iostream>
///////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
///////////////////////////////////////////////////

#include <tuple>
#include <map>
#include <iterator>
#include <string>
#include <vector>
#include <queue>  
 ///////////////////////////////////////////////////


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
///////////////////////////////////////////////////

#include <netinet/in.h>
#include <arpa/inet.h>
///////////////////////////////////////////////////

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
///////////////////////////////////////////////////



#include <assert.h>
#include <fcntl.h>

using namespace std;
///////////////////////////////////////////////////



// Primary Colours
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define BLU "\e[0;34m"

  // Ternary colours
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
  // Secondary Colours
#define BLK "\e[0;30m"
#define YEL "\e[0;33m"

// Regular color text

#define ANSI_RESET "\x1b[0m"

///////////////////////////////////////////////////

typedef struct data {
    bool busy;
    string value;
} data;
 ///////////////////////////////////////////////////

typedef struct th_args {
    
} th_args;
///////////////////////////////////////////////////

typedef long long LL;

///////////////////////////////////////////////////
#define MAX_CLIENTS 77
#define PORT_ARG 8001
///////////////////////////////////////////////////
#define part cout << "-----------------------------------" << endl;
#define debug(x) cout << #x << " : " << x << endl

#define pb push_back
///////////////////////////////////////////////////


const LL B_S = 1048576;// 1 MB = 1048576 bytes( This number symbolizes the "Battle of Life")                             // for receiving the string from client
const int initial_message_length = 256;

pthread_mutex_t socketlock = PTHREAD_MUTEX_INITIALIZER; // lock while sending string to socket 
pthread_mutex_t lock[101];                              // lock for  dictionary's key
pthread_mutex_t quelock = PTHREAD_MUTEX_INITIALIZER;    // queue lock
///////////////////////////////////////////////////

pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;      // conditional variable for multiple threads
///////////////////////////////////////////////////
queue<int *> q;                                         //  client fds' addresses queue
map<int, data> mydict;                                  //  common dictionary of server

///////////////////////////////////////////////////


pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } 
    else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    }
            else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    }  
    else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&conditionalvariable, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}

///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  */

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a NEW CLIENT's request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New CLIENT connected to port %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd; //pcli = pclient

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&conditionalvariable);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        pthread_mutex_destroy(&lock[i]);
    }

    return 0;
}







/*
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, B_S);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&_, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}
    //////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, ess;
  ect  /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    // IP address can be anything (INADDR_ANY) 
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests 
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_address_object);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&ess, &cectlilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(ess.sinect_port), inet_ntoa(ess.sinect_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd;

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&_);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        destroy_pthread_mutex(&lock[i]);
    }

    return 0;
}
*/

//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <vector>
#include <queue>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BGRN "\e[1;32m"
#define ANSI_RESET "\x1b[0m"

typedef long long int LL;

#define pb push_back

///////////////////////////////
#define MAX_CLIENTS 105
#define PORT_ARG 8002

const int initial_msg_len = 512;

////////////////////////////////////

const LL buff_sz = 1048578;
///////////////////////////////////////////////////

#define THREAD_POOL_SIZE 105
pthread_t thread_pool[THREAD_POOL_SIZE];
queue<int> q;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_var = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

// Vector that stores the pair of key and value
vector<pair<string, string>> vector_of_pairs;

string Insert_S(string key, string val)
{
    bool is_present = false;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++)
    {
        if (vector_of_pairs[i].first == key)
        {
            is_present = true;
            break;
        }
    }
    if (is_present)
    {
        pthread_mutex_unlock(&mutex2);
        return "Key already exists";
    }
    else
    {
        vector_of_pairs.push_back(make_pair(key, val));
        pthread_mutex_unlock(&mutex2);
        return "Insertion successful";
    }
}

string Delete_S(string key)
{
    // string myMsg;
    bool is_present = false;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++)
    {
        if (vector_of_pairs[i].first == key)
        {
            vector_of_pairs.erase(vector_of_pairs.begin() + i);
            is_present = true;
            break;
        }
    }
    if (is_present)
    {
        pthread_mutex_unlock(&mutex2);
        return "Deletion successful";
    }
    else
    {
        pthread_mutex_unlock(&mutex2);
        return "No such key exists";
    }
}

string Update_S(string key, string val)
{
    bool is_present = false;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++)
    {
        if (vector_of_pairs[i].first == key)
        {
            is_present = true;
            vector_of_pairs[i].second = val;
            break;
        }
    }
    if (is_present)
    {
        pthread_mutex_unlock(&mutex2);
        return val;
    }
    else
    {
        pthread_mutex_unlock(&mutex2);
        return "Key does not exist";
    }
}

string Concate_S(string key1, string key2)
{
    bool is_present1 = false;
    bool is_present2 = false;
    int position1 = -1, position2 = -1;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++)
    {
        if (vector_of_pairs[i].first == key1)
        {
            is_present1 = true;
            position1 = i;
        }
        if (vector_of_pairs[i].first == key2)
        {
            is_present2 = true;
            position2 = i;
        }
    }

    if (!is_present1 || !is_present2)
    {
        pthread_mutex_unlock(&mutex2);
        return "Concat failed as at least one of the keys does not exist";
    }
    string temp = vector_of_pairs[position1].second;
    vector_of_pairs[position1].second += vector_of_pairs[position2].second;
    vector_of_pairs[position2].second += temp;

    pthread_mutex_unlock(&mutex2);
    return vector_of_pairs[position2].second;
}

string Get_String(string key)
{
    bool is_present = false;
    string pos;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++)
    {
        if (vector_of_pairs[i].first == key)
        {
            is_present = true;
            pos = vector_of_pairs[i].second;
            break;
        }
    }
    pthread_mutex_unlock(&mutex2);
    if (is_present)
    {
        return pos;
    }
    else
        return "Key does not exist";
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    // free(client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    // while (true)
    // {
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;
    // printf("Read something\n");
    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        close(client_socket_fd);
        return NULL;
        // goto close_client_socket_ceremony;
    }
    cout << "Client sent : " << cmd << endl;
    if (cmd == "exit\n")
    {
        cout << "Exit pressed by client" << endl;
        close(client_socket_fd);
        return NULL;
        // goto close_client_socket_ceremony;
    }

    // cout << "****************** cmd ********************" << endl;
    vector<string> tokenisedStrings; // stores tokenised strings
    string msg_to_send_back ;
    // cout << "INPUT : " << input << endl;
    // Tokeinising the string on basis of space
    string word = "";
    for (auto x : cmd)
    {
        if (x == ' ')
        {
            tokenisedStrings.push_back(word);
            word = "";
        }
        else
        {
            word = word + x;
        }
    }
    tokenisedStrings.push_back(word);

    if (tokenisedStrings[1] == "insert")
     msg_to_send_back = Insert_S(tokenisedStrings[2], tokenisedStrings[3]);

    else if (tokenisedStrings[1] == "delete")
     msg_to_send_back = Delete_S(tokenisedStrings[2]);

    else if (tokenisedStrings[1] == "update")
     msg_to_send_back = Update_S(tokenisedStrings[2], tokenisedStrings[3]);

    else if (tokenisedStrings[1] == "concat")
    msg_to_send_back = Concate_S(tokenisedStrings[2], tokenisedStrings[3]);

    else
    msg_to_send_back = Get_String(tokenisedStrings[2]);
    

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
        close(client_socket_fd);
        return NULL;
        // goto close_client_socket_ceremony;
    }
    // }

    
}

void *thread_function(void *ptr)
{
    while (true)
    {

        // Called mutex lock on queue
        pthread_mutex_lock(&mutex);
        pthread_cond_wait(&condition_var, &mutex);
        int pclient = q.front();

        if (q.empty())
        {    
            // If queue is empty then wait
            pthread_cond_wait(&condition_var, &mutex);
        }
        q.pop();

        pthread_mutex_unlock(&mutex);
        handle_connection(pclient);
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    
    auto mayank=argv[1];
    // Create n worker threads
    for (int i = 0; i < atoi(mayank); i++)
    {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&mutex);
        q.push(client_socket_fd);
        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&condition_var); // To give signal when the queue is not empty
    }

    close(wel_socket_fd);
    return 0;
}

//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////


typedef long long int LL;
const LL MOD = 1000000007;
#define pb push_back

///////////////////////////////
#define SERVER_PORT 8002
////////////////////////////////////

const LL buff_sz = 1048578;
///////////////////////////////////////////////////

pthread_mutex_t mutex_s, mutex_p, mutex_c;

typedef struct thread_details
{
    string s; // Stores request given to the thread
    int thread_index; // Stores index of thread
} td;

int m_delay = 0;


pair<string, int> Read_S_From_Socket(int file_descriptor, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_intake = read(file_descriptor, &output[0], bytes - 1);
    if (bytes_intake <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    output[bytes_intake] = 0;
    output.resize(bytes_intake);

    return {output, bytes_intake};
}

int send_string_on_socket(int file_descriptor, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(file_descriptor, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int Get_Socket_Fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_file_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_file_descriptor < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_file_descriptor, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    // printf(BGRN "Connected to server\n" ANSI_RESET);
    return socket_file_descriptor;
}
////////////////////////////////////////////////////////

void start() {
    int m;     
    string input;
    getline(cin, input);
    m = stoi(input);

    vector<string>myVec(m + 1);
    pthread_t thread_ids[m + 1];
    
    for (int i = 0; i < m; i++) {
        getline(cin, input);
        myVec[i] = input;
        int t = atoi((input.substr(0, input.find(" "))).c_str());
        if (m_delay > t)
            m_delay = t;
    }

    myVec[m] = to_string(m_delay + 1) + " fetch " + to_string(2) + " " + to_string(4);

    for (int i = 0; i <= m; i++) {
        pthread_t curr_thread_id;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->s = myVec[i];
        thread_input->thread_index = i;
        pthread_create(&curr_thread_id, NULL, begin_process, (void *)(thread_input));
        thread_ids[i] = curr_thread_id;
        if (i == m)
            break;
    }

   for (int i = 0; i < m; i++) 
        pthread_join(thread_ids[i], NULL);
}

void *begin_process(void *inp) {
    struct sockaddr_in server_obj;
    string input = ((struct thread_details *)inp)->s;
    sleep(atoi((input.substr(0, input.find(" "))).c_str()));

    pthread_mutex_lock(&mutex_c);
    int socket_file_descriptor = Get_Socket_Fd(&server_obj);
    send_string_on_socket(socket_file_descriptor, input);
    pthread_mutex_unlock(&mutex_c);

    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = Read_S_From_Socket(socket_file_descriptor, buff_sz);

    pthread_mutex_lock(&mutex_p);
    cout << ((struct thread_details *)inp)->thread_index << ":" << gettid() << ":" << output_msg.c_str() << endl;
    pthread_mutex_unlock(&mutex_p);
    return NULL;
}


int main(int argc, char *argv[])
{
    pthread_mutex_init(&mutex_p, NULL);
    pthread_mutex_init(&mutex_s, NULL);
    pthread_mutex_init(&mutex_c, NULL);

    start();
    return 0;
}