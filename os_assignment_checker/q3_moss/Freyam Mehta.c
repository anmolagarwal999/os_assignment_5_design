
//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q3/server.cpp ####################//

#include <arpa/inet.h>
#include <assert.h>
#include <bits/stdc++.h>
#include <fcntl.h>
#include <iostream>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <tuple>
#include <unistd.h>

using namespace std;

#define BUFFER_SIZE 1048576
#define DICT_SIZE 100
#define MX_CLIENTS 100
#define PORT 8001

struct Dictionary {
    string str;            // command
    int ID;                // ID
    bool active;           // is_active
    pthread_mutex_t mutex; // lock
};

vector<Dictionary> dict(DICT_SIZE);
queue<int *> q;

pthread_mutex_t MAP_LOCK = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t QUEUE_LOCK = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t COND_INIT = PTHREAD_COND_INITIALIZER;

int SERVER_LOG(int fd, const string &s);
void *THREAD_WORKER(void *arg);
void CLIENT_HANDLER(int client_fd);
void COMMAND_HANDLER(string s, int client_socket_fd);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cout << "Invalid Arguments" << endl;
        return 0;
    }

    int N_WORKER_THREADS = stoi(argv[1]); // Number of worker threads

    pthread_t WORKER_THREADS[N_WORKER_THREADS]; // Array of worker threads

    for (int i = 0; i < DICT_SIZE; ++i) {
        dict[i].active = 0, dict[i].ID = i;       // Initialize dictionary
        pthread_mutex_init(&dict[i].mutex, NULL); // Initialize mutex
    }

    for (int i = 0; i < N_WORKER_THREADS; ++i)
        pthread_create(&WORKER_THREADS[i], NULL, THREAD_WORKER, NULL); // Create worker threads

    sockaddr_in SERVER_ADDR, CLIENT_ADDR;

    int SERV_SOCK_FD = socket(AF_INET, SOCK_STREAM, 0); // Create socket
    if (SERV_SOCK_FD < 0) {
        cerr << "Error in creating socket" << endl;
        exit(1);
    }

    bzero((char *)&SERVER_ADDR, sizeof(SERVER_ADDR)); // Initialize server address

    SERVER_ADDR.sin_family = AF_INET;
    SERVER_ADDR.sin_addr.s_addr = INADDR_ANY;
    SERVER_ADDR.sin_port = htons(PORT);

    if (bind(SERV_SOCK_FD, (sockaddr *)&SERVER_ADDR, sizeof(SERVER_ADDR)) < 0) { // Bind socket to server address
        cerr << "Error in binding" << endl;
        exit(1);
    }

    listen(SERV_SOCK_FD, MX_CLIENTS); // Listen for clients

    cout << "Server is listening on port " << PORT << endl;
    socklen_t clilen = sizeof(CLIENT_ADDR); // Get the size of the client address

    for (;;) {
        printf("Waiting for connection\n");
        int client_socket_fd = accept(SERV_SOCK_FD, (sockaddr *)&CLIENT_ADDR, &clilen);

        if (client_socket_fd < 0) {
            cerr << "Error in accepting connection" << endl;
            exit(1);
        } else {
            printf("Connection accepted from port number %d and IP %s \n", ntohs(CLIENT_ADDR.sin_port), inet_ntoa(CLIENT_ADDR.sin_addr));
        }

        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;

        pthread_mutex_lock(&QUEUE_LOCK);   // Lock the queue
        q.push(pclient);                   // Add the client socket to the queue
        pthread_mutex_unlock(&QUEUE_LOCK); // Unlock the queue
        pthread_cond_signal(&COND_INIT);   // Signal the condition variable
    }

    close(SERV_SOCK_FD); // Close the socket
}

int SERVER_LOG(int fd, const string &s) {
    pthread_mutex_lock(&MAP_LOCK);
    pthread_mutex_unlock(&MAP_LOCK);

    int bytes = write(fd, s.c_str(), s.length()); // Write to log file
    if (bytes < 0) {
        cerr << "Failed To communicate with the server" << endl;
        return -1;
    }

    return bytes;
}

void COMMAND_HANDLER(string s, int client_socket_fd) {
    char *input = (char *)malloc((s.length() + 1) * sizeof(char)); // Allocate memory for input
    strcpy(input, s.c_str());                                      // Copy string to char array
    input[s.length()] = '\0';

    int argc = 0;

    string TEMP_STRING;
    char *string = strtok_r(input, " ", &input); // Tokenize input
    char *arguments[s.length()];

    while (string != NULL) {
        arguments[argc] = (char *)malloc((strlen(string) + 1) * sizeof(char)); // Allocate memory for arguments
        strcpy(arguments[argc], string);
        string = strtok_r(input, " ", &input);
        argc++;
    }

    if (argc == 0)
        return;

    arguments[argc] = NULL;

    if (strcmp(arguments[0], "insert") == 0) {
        if (argc != 3) {
            cout << "Invalid number of arguments" << endl;
            SERVER_LOG(client_socket_fd, "Invalid number of arguments");
            return;
        }

        int key = atoi(arguments[1]);

        pthread_mutex_lock(&dict[key].mutex);

        if (dict[key].active == 0) {
            dict[key].active = 1;         // Set active to 1
            dict[key].str = arguments[2]; // Set string

            cout << "Insertion successful" << endl;
            SERVER_LOG(client_socket_fd, "Insertion successful");
        } else {
            cout << "Key already exists" << endl;
            SERVER_LOG(client_socket_fd, "Key already exists");
        }

        pthread_mutex_unlock(&dict[key].mutex);
    }

    if (strcmp(arguments[0], "concat") == 0) {
        if (argc != 3) {
            cout << "Invalid number of arguments" << endl;
            SERVER_LOG(client_socket_fd, "Invalid number of arguments");
            return;
        }

        int key1 = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key1].mutex);

        int key2 = atoi(arguments[2]);
        pthread_mutex_lock(&dict[key2].mutex);

        if (dict[key1].active == 1 && dict[key2].active == 1) {
            TEMP_STRING = dict[key1].str;
            dict[key1].str = dict[key1].str + dict[key2].str;
            dict[key2].str += TEMP_STRING;

            cout << "Concatenation successful" << endl;
            SERVER_LOG(client_socket_fd, dict[key2].str);
        } else {
            cout << "Concat failed as at least one of the keys does not exist" << endl;
            SERVER_LOG(client_socket_fd, "Concat failed as at least one of the keys does not exist");
        }

        pthread_mutex_unlock(&dict[key1].mutex);
        pthread_mutex_unlock(&dict[key2].mutex);
    } else if (strcmp(arguments[0], "fetch") == 0) {
        if (argc != 2) {
            cout << "Invalid number of arguments" << endl;
            SERVER_LOG(client_socket_fd, "Invalid number of arguments");
            return;
        }
        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);

        if (dict[key].active == 1) {
            cout << "Fetched " << dict[key].str << " at " << key << endl;
            SERVER_LOG(client_socket_fd, dict[key].str);
        } else {
            cout << "Key " << key << " is not active" << endl;
            SERVER_LOG(client_socket_fd, "No such key exists");
        }

        pthread_mutex_unlock(&dict[key].mutex);
    } else if (strcmp(arguments[0], "update") == 0) {
        if (argc != 3) {
            cout << "Invalid number of arguments" << endl;
            SERVER_LOG(client_socket_fd, "Invalid number of arguments");
            return;
        }

        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);

        if (dict[key].active == 1) {
            dict[key].str = arguments[2];
            cout << "Updated " << dict[key].str << " at " << key << endl;
            SERVER_LOG(client_socket_fd, dict[key].str);
        } else {
            cout << "No such key exists" << endl;
            SERVER_LOG(client_socket_fd, "No such key exists");
        }

        pthread_mutex_unlock(&dict[key].mutex);
    } else if (strcmp(arguments[0], "delete") == 0) {
        if (argc != 2) {
            cout << "Invalid number of arguments" << endl;
            SERVER_LOG(client_socket_fd, "Invalid number of arguments");
            return;
        }

        int key = atoi(arguments[1]);
        pthread_mutex_lock(&dict[key].mutex);

        if (dict[key].active == 1) {
            dict[key].active = 0; // Set active to 0
            cout << "Deletion successful" << endl;
            SERVER_LOG(client_socket_fd, "Deletion successful");
        } else {
            cout << "No such key exists" << endl;
            SERVER_LOG(client_socket_fd, "No such key exists");
        }

        pthread_mutex_unlock(&dict[key].mutex);
    }
}

void CLIENT_HANDLER(int client_fd) {
    string buffer(BUFFER_SIZE, '\0');
    pthread_mutex_lock(&MAP_LOCK);

    pthread_mutex_unlock(&MAP_LOCK);

    int byte_read = read(client_fd, &buffer[0], BUFFER_SIZE - 1); // Read from client

    buffer[byte_read] = '\0';
    buffer.resize(byte_read); // Resize buffer to actual size
    if (byte_read <= 0) {
        cout << "Client disconnected" << endl;
        SERVER_LOG(client_fd, "Client disconnected\n");
        close(client_fd);
        return;
    }
    COMMAND_HANDLER(buffer, client_fd);
}

void *THREAD_WORKER(void *arg) {
    for (;;) {
        pthread_mutex_lock(&QUEUE_LOCK); // Lock the queue

        while (q.empty())
            pthread_cond_wait(&COND_INIT, &QUEUE_LOCK); // Wait for signal

        int *CLIENT_SOCKER_FD = q.front(); // Get the first element
        q.pop();

        pthread_mutex_unlock(&QUEUE_LOCK); // Unlock the queue

        string buffer(BUFFER_SIZE, '\0');

        pthread_mutex_lock(&MAP_LOCK);
        pthread_mutex_unlock(&MAP_LOCK);

        int byte_read = read(*CLIENT_SOCKER_FD, &buffer[0], BUFFER_SIZE - 1); // Read from client

        buffer[byte_read] = '\0';
        buffer.resize(byte_read);

        if (byte_read <= 0) {
            cerr << "Failed To communicate with the server" << endl; // If read fails
            return NULL;
        }

        COMMAND_HANDLER(buffer, *CLIENT_SOCKER_FD);
    }
}

//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q3/client.cpp ####################//

#include <arpa/inet.h>
#include <assert.h>
#include <bits/stdc++.h>
#include <fcntl.h>
#include <iostream>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <tuple>
#include <unistd.h>

using namespace std;

#define PORT 8001           // port number
#define BUFFER_SIZE 1048576 // 1MB

typedef struct REQUEST {
    int i;
    pthread_t tid;
    int client_fd;
    int req_time;
    string command;
    pthread_mutex_t mutex;
} REQUEST;

vector<REQUEST> clients_requests;                     // vector of requests
pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER; // mutex for terminal

void HANDLE_CLIENTS(int N_CLIENTS); // function to handle clients
void INIT_FD();
void *CLIENT_WORKER(void *(arg)); // function to handle clients

int CLIENT_FD;

int main() {
    int N_CLIENTS;
    cin >> N_CLIENTS;

    string buf;
    getline(cin, buf);

    HANDLE_CLIENTS(N_CLIENTS); // handle clients
    return 0;
}

void HANDLE_CLIENTS(int N_CLIENTS) {
    for (int i = 0; i < N_CLIENTS; ++i) {
        string str;
        getline(cin, str);

        string TIME_REQUIRED = "";
        auto itr = str.begin();

        while (itr != str.end()) {
            if (*itr == ' ')
                break;

            TIME_REQUIRED += *itr;
            itr++;
        }

        itr++;
        string command(itr, str.end());

        REQUEST tmp;
        tmp.req_time = stoi(TIME_REQUIRED); // time required
        tmp.command = command;              // command
        clients_requests.emplace_back(tmp); // add to vector
    }

    for (int i = 0; i < N_CLIENTS; ++i) {
        clients_requests[i].mutex = PTHREAD_MUTEX_INITIALIZER; // initialize mutex
        clients_requests[i].i = i;
    }

    for (int i = 0; i < N_CLIENTS; ++i)
        pthread_create(&clients_requests[i].tid, NULL, CLIENT_WORKER, &clients_requests[i].i); // create threads

    for (int i = 0; i < N_CLIENTS; ++i)
        pthread_join(clients_requests[i].tid, NULL); // join threads

    exit(0);
}

void INIT_FD() {
    sockaddr_in server_obj;
    int SOCKET_FD = socket(AF_INET, SOCK_STREAM, 0); // create socket

    if (SOCKET_FD < 0) {
        perror("socket");
        exit(1);
    }

    memset(&server_obj, 0, sizeof(server_obj)); // clear server_obj
    server_obj.sin_family = AF_INET;            // set family
    server_obj.sin_port = htons(PORT);          // set port

    if (connect(SOCKET_FD, (sockaddr *)&server_obj, sizeof(server_obj)) < 0) { // connect to server
        perror("connect");
        exit(1);
    }

    CLIENT_FD = SOCKET_FD; // set client_fd
    return;
}

void *CLIENT_WORKER(void *(arg)) {
    int index = *(int *)arg;

    // clients_requests[index].client_fd = CLIENT_FD;
    INIT_FD();
    clients_requests[index].client_fd = CLIENT_FD; // set client_fd

    string command = clients_requests[index].command;
    int req_time = clients_requests[index].req_time;
    int client_fd = clients_requests[index].client_fd;

    sleep(req_time); // sleep for required time

    pthread_mutex_lock(&clients_requests[index].mutex); // lock mutex

    int x = write(client_fd, command.c_str(), command.length()); // write command to client
    if (x < 0) {
        cerr << "Error writing to socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_unlock(&clients_requests[index].mutex); // unlock mutex
    pthread_mutex_lock(&clients_requests[index].mutex);   // lock mutex

    string result(BUFFER_SIZE, '\0');
    int byte_read = read(client_fd, &result[0], BUFFER_SIZE - 1);
    result[byte_read] = '\0';
    result.resize(byte_read);

    if (byte_read <= 0) {
        cerr << "Error reading from socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_lock(&terminal); // lock terminal

    cout << clients_requests[index].i << ":" << gettid() << ":" << result << endl; // log

    pthread_mutex_unlock(&terminal); // unlock terminal

    pthread_mutex_unlock(&clients_requests[index].mutex); // unlock mutex

    return NULL;
}
