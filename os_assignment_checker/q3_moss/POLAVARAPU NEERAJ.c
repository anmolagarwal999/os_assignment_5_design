
//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q3/._server.cpp ####################//

#include "header.h"

void* allocate(void* args) {
    struct course *myCourse = (struct course*) args;
    for(int i=0;i<myCourse->num_labs;i++) {
        struct lab *myLab = &Labs[myCourse->labids[i]];
            for(int j=0;j<myLab->num_std;j++) {
                pthread_mutex_lock(&myLab->ta_lock[j]);
                if (myLab->turns[j] > 0 && myLab->occupied[j] == 0) {
                    myLab->turns[j] -= 1;
                    myLab->occupied[j] = 1;
                    myCourse->ta_id = j;
                    myCourse->ta_lab = myCourse->labids[i];
                    printf("%sTA %d from lab %s has been allocated to course %s for his %d TA ship%s\n", BLUE, j, myLab->name, myCourse->name, (myLab->repeat - myLab->turns[j]), BLUE);
                }
                pthread_mutex_unlock(&myLab->ta_lock[j]);
                if(myCourse->ta_id != -1) {
                    break;
                }
            }
        if(myCourse->ta_id != -1) {
            break;
        }
    }
    int slots = rand()%(myCourse->max_slots - 1 + 1) + 1;
    pthread_mutex_lock(&myCourse->c_lock);
    myCourse->t_index = 0;
    int student_interest = 0;
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        if (myStudent->current == myCourse->index) {
            student_interest++;
        }
    }
    myCourse->slots = (student_interest >= slots) ? slots: student_interest;
    pthread_mutex_unlock(&myCourse->c_lock);
    printf("%sCourse %s has been allocated %d seats%s\n", RED, myCourse->name, myCourse->slots, RED);
    
    pthread_mutex_lock(&myCourse->c_lock);
    while(myCourse->t_index < myCourse->slots) {
        pthread_cond_wait(&myCourse->c_wait, &myCourse->c_lock);
    }
    pthread_mutex_unlock(&myCourse->c_lock);

    // take the tutorial
    printf("%sTutorial has started for Course %s with %d seats filled out of %d%s\n", RED, myCourse->name, myCourse->t_index, myCourse->slots, RED);
    sleep(5);
    struct lab *myLab = &Labs[myCourse->ta_lab];
    printf("%sTA %d from lab %s has completed the tutorial for course %s%s\n", BLUE, myCourse->ta_id, myLab->name, myCourse->name, BLUE);
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        pthread_mutex_lock(&myStudent->s_lock);
        if (myStudent->current == myCourse->index) {
            myStudent->inCourse = 0;
            pthread_cond_signal(&myStudent->wait_course);
        }
        pthread_mutex_unlock(&myStudent->s_lock);
    }
//    pthread_mutex_unlock(&myCourse->c_lock);
}

void* cMain(void* args) {
    pthread_t threads[clen];
    int response = 0;
    for(int i=0;i<clen;i++) {
        response = pthread_create(&threads[i], NULL, allocate, (void *)&Courses[i]);
        if(response < 0) {
            printf("Error Number: %d\n", errno);
            perror("cmain");
        }
    }

    for(int i=0;i<clen;i++) {
        pthread_join(threads[i], NULL);
    }
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr

char dictionary[1000][10000];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

char *editor(char *command, int a, int b, char *value)
{
    char *message;
    message = (char *)malloc(sizeof(char) * 1000);
    if (strcmp(command, "insert") == 0)
    {
        if (strcmp(dictionary[a], "") != 0)
            strcpy(message, "Key already exists");
        else
        {
            strcpy(dictionary[a], value);
            strcpy(message, "Insertion successful");
            return message;
        }
        return message;
    }
    if (strcmp(command, "delete") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "No such key exists");
        else
        {
            strcpy(dictionary[a], "");
            strcpy(message, "Deletion successful");
            return message;
        }
        return message;
    }
    if (strcmp(command, "update") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "Key does not exist");
        else
        {
            strcpy(dictionary[a], value);
            strcpy(message, value);
            return message;
        }
        return message;
    }
    if (strcmp(command, "concat") == 0)
    {
        if (strcmp(dictionary[a], "") == 0 || strcmp(dictionary[b], "") == 0)
            strcpy(message, "Concat failed as at least one of the keys does not exist");
        else
        {
            char temp[1000];
            strcpy(temp, dictionary[a]);
            strcat(dictionary[a], dictionary[b]);
            strcat(dictionary[b], temp);
            strcpy(message, dictionary[b]);
            return message;
        }
        return message;
    }
    if (strcmp(command, "fetch") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "Key does not exist");
        else
        {
            strcpy(message, dictionary[a]);
            return message;
        }
        return message;
    }
    return message;
}

void func(int sockfd, int id)
{
    char buff[MAX];
    bzero(buff, MAX);
    read(sockfd, buff, sizeof(buff));
    char command[1000];
    int a, b;
    char value[1000];

    char *token = strtok(buff, " ");
    strcpy(command, token);
    int y = strcmp(command, "insert");
    if (strcmp(command, "insert") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        strcpy(value, token);
        b = -20;
    }
    if (strcmp(command, "update") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        strcpy(value, token);
        b = -20;
    }
    if (strcmp(command, "delete") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        b = -20;
    }
    if (strcmp(command, "concat") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        b = atoi(token);
    }
    if (strcmp(command, "fetch") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        b = -20;
    }

    pthread_mutex_lock(&mutex);
    char message[1000];
    strcpy(message, editor(command, a, b, value));
    bzero(buff, MAX);
    sprintf(buff, "%d:%s", id, message);
    write(sockfd, buff, sizeof(buff));
    pthread_mutex_unlock(&mutex);
}

int sockfd, connfd;
socklen_t len;
struct sockaddr_in servaddr, cli;

void connecttoserver()
{
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    //defining
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
    //defining done
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    if ((listen(sockfd, 100)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
}

void *serverthread(void *arg)
{
    int id = (long int)arg;
    while (1)
    {
        len = sizeof(cli);
        connfd = accept(sockfd, (SA *)&cli, &len); //accept()
        func(connfd, id);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&mutex, NULL);
    if (argc != 2)
    {
        printf("NOT THE CORRECT ARGUMENTS\n");
        exit(0);
    }
    connecttoserver();
    int n = atoi(argv[1]);
    pthread_t servers[n];
    for (long int i = 0; i < n; i++)
    {
        pthread_create(&servers[i], NULL, serverthread, (void *)i);
    }
    for (int i = 0; i < n; i++)
    {
        pthread_join(servers[i], NULL);
    }
    close(sockfd);
    pthread_mutex_destroy(&mutex);
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr

int clienttime[10000];
char clientcommand[10000][10000];
int m;

void func(int sockfd, int id)
{
	char buff[MAX];
	bzero(buff, sizeof(buff));
	strcpy(buff, clientcommand[id]);
	write(sockfd, buff, sizeof(buff));
	bzero(buff, sizeof(buff));
	read(sockfd, buff, MAX);
	printf("%d:%s\n", id, buff);
}

int connecttoserver(int id)
{
	int sockfd;
	struct sockaddr_in servaddr, cli;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		printf("socket creation failed...\n");
		exit(0);
	}

	//defining
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);
	//defining done
	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
	{
		printf("connection with the server failed...\n");
		exit(0);
	}

	return sockfd;
}

void *clientthread(void *arg)
{
	int id = (long int)arg;
	sleep(clienttime[id]);
	int sockfd = connecttoserver(id);
	func(sockfd, id);
	close(sockfd);
	return NULL;
}

void scaninput()
{
	scanf("%d", &m);
	for (int i = 0; i < m; i++)
	{
		scanf("%d ", &clienttime[i]);
		scanf("%[^\n]", clientcommand[i]);
	}
	// m = 11;
	// clienttime[0] = 1;
	// strcpy(clientcommand[0], "insert 1 hello");
	// clienttime[1] = 2;
	// strcpy(clientcommand[1], "insert 1 hello");
	// clienttime[2] = 2;
	// strcpy(clientcommand[2], "insert 2 yes");
	// clienttime[3] = 2;
	// strcpy(clientcommand[3], "insert 3 no");
	// clienttime[4] = 3;
	// strcpy(clientcommand[4], "concat 1 2");
	// clienttime[5] = 3;
	// strcpy(clientcommand[5], "concat 1 3");
	// clienttime[6] = 4;
	// strcpy(clientcommand[6], "delete 3");
	// clienttime[7] = 5;
	// strcpy(clientcommand[7], "delete 4");
	// clienttime[8] = 6;
	// strcpy(clientcommand[8], "concat 1 4");
	// clienttime[9] = 7;
	// strcpy(clientcommand[9], "update 1 final");
	// clienttime[10] = 8;
	// strcpy(clientcommand[10], "concat 1 2");
}

int main()
{
	scaninput();
	pthread_t clients[m];
	for (long int i = 0; i < m; i++)
	{
		pthread_create(&clients[i], NULL, clientthread, (void *)i);
	}
	for (int i = 0; i < m; i++)
	{
		pthread_join(clients[i], NULL);
	}
}
