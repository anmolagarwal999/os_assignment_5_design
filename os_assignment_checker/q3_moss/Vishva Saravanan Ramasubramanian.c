
//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/dictdb.cpp ####################//

#include "dictdb.h"

#include <pthread.h>

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "colors.h"
#include "utils.h"
using namespace std;

// global dictionary
map<int, string> DICTDB;

// mutex lock for global dictionary
pthread_mutex_t dictdb_mutex;

// execute command on dictionary
string execute_query(string command) {
    string response = "Invalid command!";
    vector<string> args = split(command, " ");
    if (args.size()) {
        if (args[0] == "insert") {
            if (args.size() < 3) {
                response = args[0] + ": insufficient number of arguments";
            } else {
                response = q_insert(stoi(args[1]), args[2]);
            }
        } else if (args[0] == "delete") {
            if (args.size() < 2) {
                response = args[0] + ": insufficient number of arguments";
            } else {
                response = q_delete(stoi(args[1]));
            }
        } else if (args[0] == "update") {
            if (args.size() < 3) {
                response = args[0] + ": insufficient number of arguments";
            } else {
                response = q_update(stoi(args[1]), args[2]);
            }
        } else if (args[0] == "concat") {
            if (args.size() < 3) {
                response = args[0] + ": insufficient number of arguments";
            } else {
                response = q_concat(stoi(args[1]), stoi(args[2]));
            }
        } else if (args[0] == "fetch") {
            if (args.size() < 2) {
                response = args[0] + ": insufficient number of arguments";
            } else {
                response = q_fetch(stoi(args[1]));
            }
        } else {
            response = args[0] + ": command not found";
        }
    }

    return response;
}

// insert key-value pair into dictionary
string q_insert(int key, string value) {
    string response = "";

    pthread_mutex_lock(&dictdb_mutex);
    // check if key already exists
    if (DICTDB.find(key) != DICTDB.end()) {
        response = "Key already exists";
    } else {
        DICTDB.insert({key, value});
        response = "Insertion successful";
    }
    pthread_mutex_unlock(&dictdb_mutex);

    return response;
}

// delete key from dictionary
string q_delete(int key) {
    string response = "";

    pthread_mutex_lock(&dictdb_mutex);
    // check if key exists
    if (DICTDB.find(key) == DICTDB.end()) {
        response = "No such key exists";
    } else {
        DICTDB.erase(key);
        response = "Deletion successful";
    }
    pthread_mutex_unlock(&dictdb_mutex);

    return response;
}

// update value of given key in dictionary
string q_update(int key, string value) {
    string response = "";

    pthread_mutex_lock(&dictdb_mutex);
    // check if key exists
    auto itr = DICTDB.find(key);
    if (itr == DICTDB.end()) {
        response = "Key does not exist";
    } else {
        itr->second = value;
        response = value;
    }
    pthread_mutex_unlock(&dictdb_mutex);

    return response;
}

// fetch concatenation of values at specified keys in the dictionary
string q_concat(int key1, int key2) {
    string response = "";

    pthread_mutex_lock(&dictdb_mutex);
    // check if keys exist
    auto itr1 = DICTDB.find(key1);
    auto itr2 = DICTDB.find(key2);
    if ((itr1 == DICTDB.end()) || (itr2 == DICTDB.end())) {
        response = "Concat failed as at least one of the keys does not exist";
    } else {
        string temp = itr2->second;
        itr2->second += itr1->second;
        itr1->second += temp;
        response = itr2->second;
    }
    pthread_mutex_unlock(&dictdb_mutex);

    return response;
}

// fetch value of given key from dictionary
string q_fetch(int key) {
    string response = "";

    pthread_mutex_lock(&dictdb_mutex);
    // check if key exists
    auto itr = DICTDB.find(key);
    if (itr == DICTDB.end()) {
        response = "Key does not exist";
    } else {
        response = itr->second;
    }
    pthread_mutex_unlock(&dictdb_mutex);

    return response;
}

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/server.cpp ####################//

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdarg>
#include <cstdlib>
#include <iostream>
#include <queue>
#include <string>
#include <tuple>
#include <utility>

#include "colors.h"
#include "dictdb.h"
#include "utils.h"
using namespace std;

// server config {{{
#define MAX_CLIENTS 4
#define PORT 8001
#define BUFFER_SIZE 1048576
#define WORKER_POOL_SIZE 10
#define WORKER_DELAY 2
// }}}

// queue requests in the form <socket_fd, command>
queue<pair<int, string>> request_queue;

// mutex lock and condition variable for request queue
pthread_mutex_t request_queue_mutex;
pthread_cond_t request_queue_cond;

// maintain worker thread pool
pthread_t* worker_pool;

// add incoming requests to the queue {{{
void submit_request(int client_socket_fd) {
    string request;
    int bytes_received = 0;
    tie(request, bytes_received) = read_socket(client_socket_fd, BUFFER_SIZE);

    // queue the request
    pthread_mutex_lock(&request_queue_mutex);
    request_queue.push(make_pair(client_socket_fd, request));
    pthread_mutex_unlock(&request_queue_mutex);
    pthread_cond_broadcast(&request_queue_cond);
}
// }}}

// worker routine {{{
void* worker_routine(void* args) {
    while (1) {
        // look for pending requests in the queue
        int socket_fd = -1;
        string command = "";

        pthread_mutex_lock(&request_queue_mutex);
        while (request_queue.empty()) {
            pthread_cond_wait(&request_queue_cond, &request_queue_mutex);
        }
        tie(socket_fd, command) = request_queue.front();
        request_queue.pop();
        pthread_mutex_unlock(&request_queue_mutex);

        // process command
        string response = to_string(pthread_self()) + ":" + execute_query(command);

        // add a slight delay before responding
        sleep(WORKER_DELAY);

        // send response to client
        int bytes_sent = write_socket(socket_fd, response);
        if (bytes_sent == -1) {
            cerr << "Error writing to client, socket may be closed.\n";
        }

        // close connection
        close(socket_fd);
        cout << ANSI_RED << "Client disconnected." << ANSI_RESET << "\n";
    }
    return NULL;
}
// }}}

int main(int argc, char* argv[]) {
    int worker_pool_size = WORKER_POOL_SIZE;

    // determine number of workers in pool from argument
    if (argc > 1) worker_pool_size = atoi(argv[1]);

    // allocate memory for worker thread pool
    worker_pool = (pthread_t*)calloc(worker_pool_size, sizeof(pthread_t));

    // initialize worker threads
    for (int i = 0; i < worker_pool_size; i++) {
        if (pthread_create(&worker_pool[i], NULL, &worker_routine, NULL)) {
            perror("Error creating worker thread");
        }
    }

    // initialize request queue mutex
    pthread_mutex_init(&request_queue_mutex, NULL);

    // initialize request queue condition
    pthread_cond_init(&request_queue_cond, NULL);

    // initialize socket server
    int welcoming_socket_fd, client_socket_fd;
    socklen_t clilen;

    struct sockaddr_in server_addr, client_addr;

    // create welcoming socket
    welcoming_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (welcoming_socket_fd < 0) {
        cerr << "Error creating welcoming socket\n";
        exit(-1);
    }

    // set server address config
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    // bind socket to address
    if (bind(welcoming_socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
        cerr << "Error binding welcome socket\n";
        exit(-1);
    }

    // listen for incoming requests
    if (listen(welcoming_socket_fd, MAX_CLIENTS)) {
        cerr << "Error listening on welcoming socket\n";
        exit(-1);
    }
    cout << "Server with " << worker_pool_size << " workers listening on port " << PORT << ".\n";

    clilen = sizeof(client_addr);
    while (true) {
        // accept a new request and create a client socket
        client_socket_fd = accept(welcoming_socket_fd, (struct sockaddr*)&client_addr, &clilen);
        if (client_socket_fd < 0) {
            cerr << "Error accepting request\n";
            exit(-1);
        }
        cout << ANSI_GREEN << "Client " << inet_ntoa(client_addr.sin_addr) << ":"
             << ntohs(client_addr.sin_port) << " connected." << ANSI_RESET << "\n";

        submit_request(client_socket_fd);
    }

    // destroy mutexes
    pthread_mutex_destroy(&request_queue_mutex);

    // destroy condition variables
    pthread_cond_destroy(&request_queue_cond);

    // close welcoming socket fd
    close(welcoming_socket_fd);

    return 0;
}

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/client.cpp ####################//

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdarg>
#include <cstring>
#include <iostream>
#include <tuple>

#include "colors.h"
#include "utils.h"
using namespace std;

// client config {{{
#define SERVER_PORT 8001
#define BUFFER_SIZE 1048576
// }}}

// request class {{{
class Request {
   public:
    int idx;
    int delay;
    string command;
    string response;

    Request(int p_idx, int p_delay, string p_command) {
        idx = p_idx;
        delay = p_delay;
        command = p_command;
        response = "";
    }
};
// }}}

// client routine {{{
void* client_routine(void* args) {
    Request request = *(Request*)args;

    // sleep away the delay time
    sleep(request.delay);

    struct sockaddr_in server_addr;

    // set server address config
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);

    int socket_fd = connect_to_server(&server_addr);

    int bytes_sent = write_socket(socket_fd, request.command);
    if (bytes_sent == -1) {
        cerr << "Error writing to client, socket may be closed.\n";
    }

    // read server response
    int bytes_received = 0;
    tie(request.response, bytes_received) = read_socket(socket_fd, BUFFER_SIZE);
    if (bytes_received <= 0) {
        cerr << "Error reading server message\n";
    }

    // print transaction info
    cout << request.idx << ":" << request.response << "\n";

    // close connection
    close(socket_fd);

    return NULL;
}
// }}}

int main(int argc, char* argv[]) {
    int request_count;
    cin >> request_count;

    // initialize request list
    cin.ignore();
    vector<Request> request_list;
    for (int i = 0; i < request_count; i++) {
        int delay;
        cin >> delay;
        string command;
        getline(cin, command);

        Request request = Request(i, delay, trim(command));
        request_list.push_back(request);
    }

    // initialize threads
    pthread_t user_threads[request_count];
    for (int i = 0; i < request_count; i++) {
        pthread_t* thread = &user_threads[i];
        Request* request = &request_list[i];
        if (pthread_create(thread, NULL, &client_routine, request)) {
            perror("Error creating client thread");
        }
    }

    // join threads
    for (int i = 0; i < request_count; i++) {
        if (pthread_join(user_threads[i], NULL)) {
            perror("Error joining client thread");
        }
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/utils.cpp ####################//

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdarg>
#include <iostream>
#include <tuple>
#include <vector>

#include "colors.h"
using namespace std;

// utility function to read string from socket
pair<string, int> read_socket(const int& fd, int bytes) {
    string message;
    message.resize(bytes);

    int bytes_received = read(fd, &message[0], bytes - 1);
    if (bytes_received <= 0) {
        cerr << "Failed to read data from socket.\n";
    }

    message[bytes_received] = 0;
    message.resize(bytes_received);

    return {message, bytes_received};
}
// }}}

// utility function to send string to socket
int write_socket(int fd, const string& s) {
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0) {
        cerr << "Failed to send data via socket.\n";
    }

    return bytes_sent;
}

// utility function to connect to server and return file descriptor
int connect_to_server(struct sockaddr_in* ptr) {
    struct sockaddr_in server_addr = *ptr;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        cerr << "Error creating client socket.\n";
        exit(-1);
    }

    // establish connection
    if (connect(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
        cerr << "Error connecting to server.\n";
        exit(-1);
    }

    /* cout << ANSI_GREEN << "Successfully connected to server." << ANSI_RESET << "\n"; */

    return socket_fd;
}

// utility function to trim whitespace from a string
string trim(const string& line) {
    const char* WhiteSpace = " \t\v\r\n";
    size_t start = line.find_first_not_of(WhiteSpace);
    size_t end = line.find_last_not_of(WhiteSpace);
    return start == end ? string() : line.substr(start, end - start + 1);
}

// utility function to split string given delimiter
vector<string> split(string str, string delim) {
    vector<string> res = {};
    size_t pos = 0;

    while ((pos = str.find(delim)) != string::npos) {
        string token = str.substr(0, pos);
        if (token.length() > 0) res.push_back(token);
        str.erase(0, pos + delim.length());
    }

    if (str.length() > 0) res.push_back(str);
    return res;
}

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/include/utils.h ####################//

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <vector>

// utility function to read string from socket
std::pair<std::string, int> read_socket(const int& fd, int bytes);

// utility function to send string to socket
int write_socket(int fd, const std::string& s);

// utility function to connect to server and return file descriptor
int connect_to_server(struct sockaddr_in* ptr);

// utility function to trim whitespace from a string
std::string trim(const std::string& line);

// utility function to split string given delimiter
std::vector<std::string> split(std::string str, std::string delim);

#endif

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/include/colors.h ####################//

#ifndef COLORS_H_
#define COLORS_H_

#define ANSI_RED "\x1B[31m"
#define ANSI_GREEN "\x1B[32m"
#define ANSI_YELLOW "\x1B[33m"
#define ANSI_BLUE "\x1B[34m"
#define ANSI_PURPLE "\x1B[35m"
#define ANSI_CYAN "\x1B[36m"
#define ANSI_WHITE "\x1B[37m"
#define ANSI_RESET "\x1B[0m"

#endif

//###########FILE CHANGE ./main_folder/Vishva Saravanan Ramasubramanian_305789_assignsubmission_file_/2019113019/q3/include/dictdb.h ####################//

#ifndef DICTDB_H_
#define DICTDB_H_
#include <pthread.h>

#include <map>
#include <string>

// global dictionary
extern std::map<int, std::string> DICTDB;

// mutex lock for global dictionary
extern pthread_mutex_t dictdb_mutex;

// execute command on dictionary
std::string execute_query(std::string command);

// insert key-value pair into dictionary
std::string q_insert(int key, std::string value);

// delete key from dictionary
std::string q_delete(int key);

// update value of given key in dictionary
std::string q_update(int key, std::string value);

// fetch concatenation of values at specified keys in the dictionary
std::string q_concat(int key1, int key2);

// fetch value of given key from dictionary
std::string q_fetch(int key);

#endif
