
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q3/clientUtils.h ####################//


#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream> 
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple> 
using namespace std;
typedef long long ll;

struct commandList_Item{
    ll send_time;
    string command;
};

vector <commandList_Item> commandList;

struct thread_details
{
    int id;
};

pthread_mutex_t clientLock = PTHREAD_MUTEX_INITIALIZER;


const ll MOD = 1000000007;


#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

const ll buff_sz = 1048576;
ll m; //totalnumberofuserrequeststhroughoutthesimulation

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q3/server.cpp ####################//

#include "serverUtils.h"
#include "clientUtils.h"

//------------------------------------------------------------------------------------------------------------------------

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);
 
    int bytes_received = read(fd, &output[0], bytes - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

//------------------------------------------------------------------------------------------------------------------------

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    //printf("sending\n");
    //cout<<s;
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }
    return bytes_sent;
}

void adder(string cmd, int client_socket_fd,pthread_t pthreadID)
{
    vector<string> token_commands;
    stringstream check1(cmd);
    string intermediate, ack;
    string st = boost::lexical_cast<string>(pthreadID);

    while (getline(check1, intermediate, ' '))
    {
        token_commands.push_back(intermediate);
    }

    ll index = stoi(token_commands[1]), flag = 0, index2;
    //cout << token_commands[0] << "\n";

    if (token_commands[0] == "insert")
    {
        //printf("in insert:\n");
        if (keys[index] == "0")
        {
            //printf("Insertion Successful\n");
            pthread_mutex_lock(&mutex_lock[index]);
            keys[index] = token_commands[2];
            pthread_mutex_unlock(&mutex_lock[index]);
            ack = st + ":Insertion Successful";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
            flag = 1;
        } else 
        {
            //printf("Key already exists\n");
            ack = st + ":Key already exists";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
    }

    if (token_commands[0] == "delete")
    {
        if (keys[index] != "0")
        {
            pthread_mutex_lock(&mutex_lock[index]);
            keys[index] = "0";
            pthread_mutex_unlock(&mutex_lock[index]);
            ack = st + ":Deletion Successful";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
        else
        {
            keys[index] = "0";
            ack = st + ":No such key exists";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
    }

    if (token_commands[0] == "update")
    {
        if (keys[index] != "0")
        {
            pthread_mutex_lock(&mutex_lock[index]);
            keys[index] = token_commands[2];
            string p = st + ":"+keys[index];
            cout<<p<<"\n";
            pthread_mutex_unlock(&mutex_lock[index]);
            send_string_on_socket(client_socket_fd, p);
        }
        else
        {
            //printf("Key doesn't exist\n");
            ack = st + ":Key doesn't exist";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
    }

    if (token_commands[0] == "concat")
    {
        index2 = stoi(token_commands[2]);
        if (keys[index] != "0" && keys[index2] != "0")
        {
            string s1 = keys[index];
            string s2 = keys[index2];
            string s1s2 = s1 + s2;
            string s2s1 = s2 + s1;
            pthread_mutex_lock(&mutex_lock[index]);
            keys[index] = s1s2;
            pthread_mutex_unlock(&mutex_lock[index]);

            pthread_mutex_lock(&mutex_lock[index2]);
            keys[index2] = s2s1;
            pthread_mutex_unlock(&mutex_lock[index2]);
            s2s1 = st+":"+s2s1;
            cout<<s2s1<<"\n";
            send_string_on_socket(client_socket_fd, s2s1);
        }
        else
        {
            //printf("Concat failed as at least one of the keys does not exist\n");
            ack = st + ":Concat failed as at least one of the keys does not exist";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
    }

    if (token_commands[0] == "fetch")
    {
        if (keys[index] != "0")
        {
            pthread_mutex_lock(&mutex_lock[index]);
            string p = st + ":"+keys[index];
            cout<<p<<"\n";
            send_string_on_socket(client_socket_fd, p);
            pthread_mutex_unlock(&mutex_lock[index]);
        }
        else
        {
            //printf("Key doesn't exist\n");
            ack = st+ ":Key doesn't exist";
            cout<<ack<<"\n";
            send_string_on_socket(client_socket_fd, ack);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

void handle_connection(int client_socket_fd,pthread_t pthreadID)
{
    int received_num, sent_num;
    int ret_val = 1;
    //printf("handle:%d\n", client_socket_fd);
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;

    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        //goto close_client_socket_ceremony;
    }

    adder(cmd, client_socket_fd,pthreadID);

close_client_socket_ceremony:
    close(client_socket_fd);
    //printf(RED "Disconnected from client" RESET "\n");
    return;
}

//------------------------------------------------------------------------------------------------------------------------

void *serverThread_routine(void *arg)
{
    while (1)
    {
        int *P_clientSocketfd;

        pthread_mutex_lock(&clientList_Lock);
        while (clientListQueue.empty())
        {
            pthread_cond_wait(&c, &clientList_Lock);
        }
        pthread_mutex_unlock(&clientList_Lock);

        if (!clientListQueue.empty())
        {
            P_clientSocketfd = clientListQueue.front();
            clientListQueue.pop();
            int clientSocketfd = *P_clientSocketfd;
            pthread_t pthreadID= pthread_self();
            handle_connection(clientSocketfd,pthreadID);
        }
    }
    return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    counter = 0;
    for (int i = 0; i < length; i++)
    {
        keys[i] = "0";
    }

    if (argc < 2)
    {
        cout << "Wrong Number of Arguments Given\n";
        exit(0);
    }

    ll noOfWorkerThreads = stoi(argv[1]);
    pthread_t serverThread[noOfWorkerThreads];

    for (int i = 0; i < noOfWorkerThreads; i++)
    {
        pthread_create(&serverThread[i], NULL, serverThread_routine, NULL);
    }

    //----------------------------------------------------------------------

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;

    //----------------------------------------------------------------------

    //creates a new socket
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //----------------------------------------------------------------------

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj)); // sets everything to 0

    port_number = SERVER_PORT;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /* bind socket to this port number on this machine */
    if (::bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    //----------------------------------------------------------------------

    /* listen for incoming connection requests */
    listen(wel_socket_fd, noOfWorkerThreads);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        //printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        //printf(GREEN "New client connected from port number %d and IP %s \n" RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        int *P_clientSocketfd = (int *)malloc(sizeof(int));
        *P_clientSocketfd = client_socket_fd;

        pthread_mutex_lock(&clientList_Lock);
        clientListQueue.push(P_clientSocketfd);
        pthread_cond_signal(&c);
        pthread_mutex_unlock(&clientList_Lock);
        counter++;
        //printf("counter:%d m:%lld\n", counter,m);
    }

    //----------------------------------------------------------------------
    //cout << "bye\n";
    for (int i = 0; i < noOfWorkerThreads; i++)
    {
        pthread_join(serverThread[i], NULL);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q3/client.cpp ####################//

// g++ -std=c++11 client.cpp
#include "clientUtils.h"
#include "serverUtils.h"

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);
 
    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{  
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }
    //printf("Info sent\n");

    return bytes_sent;
}

int get_socket_fd()
{
    struct sockaddr_in server_obj;

    // socket() creates an endpoint for communication and returns a file descriptor that refers to that endpoint.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    

    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    /* connect to server */
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    //printf("Socket connected\n");
    return socket_fd;
}

void *client_Thread_Routine(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    sleep(commandList[id].send_time);

    int client_socket_fd = get_socket_fd();

    pthread_mutex_lock(&clientLock);
    send_string_on_socket(client_socket_fd, commandList[id].command);
    pthread_mutex_unlock(&clientLock);

    int num_bytes_read;
    string output_msg;

    pthread_mutex_lock(&clientLock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(client_socket_fd, buff_sz);
    pthread_mutex_unlock(&clientLock);

    cout << id<<":" << output_msg << endl;
    return NULL;
}

void input()
{
    string mString, input, string_send_time = "";
    getline(cin, mString);
    m = stoi(mString);

    ll ll_send_time, i, j;
    for (i = 0; i < m; i++)
    {
        getline(cin, input);
        //cout << input << "\n";
        for (j = 0; input[j] != ' '; j++)
        {
            ;
        }
        string string_send_time = input.substr(0, j);
        string command = input.substr(j + 1, input.length());
        ll_send_time = stoi(string_send_time);
        commandList_Item item;
        item.send_time = ll_send_time;
        item.command = command;
        commandList.push_back(item);
    }
}

int main(int argc, char *argv[])
{
    input();
    printf("\n");

    // creating client threads
    pthread_t ClientThread[100];
    for (int i = 0; i < m; i++)
    {
        pthread_t curr_tid;
        struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
        thread_input->id = i;
        pthread_create(&curr_tid, NULL, client_Thread_Routine, (void *)(thread_input));
        ClientThread[i] = curr_tid;
    }

    // joining client threads
    for (int l = 0; l < m; l++)
    {
        pthread_join(ClientThread[l], NULL);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q3/serverUtils.h ####################//

// g++ -std=c++11 server.cpp -lpthread
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <fcntl.h>
#include <iostream> 
#include <list>
#include <queue>
#include <assert.h>
#include <tuple>
#include <string.h>
#include <iostream>
#include <vector>
#include <string> 
#include <boost/lexical_cast.hpp>
#include <sstream>
using namespace std;

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef long long ll;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

#define MAX_CLIENTS 4
#define SERVER_PORT 8008

const int initial_msg_len = 256;

int counter;
 
ll length =1000;
string keys[1000];


queue<int*> clientListQueue;

pthread_mutex_t mutex_lock[1000];
pthread_mutex_t clientList_Lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;
    