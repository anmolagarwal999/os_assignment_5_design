
//###########FILE CHANGE ./main_folder/rishabh jain_305876_assignsubmission_file_/assignment5/q3/server.cpp ####################//

#include <signal.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <tuple>
#include <map>
#include <iterator>
#include <string>
#include <vector>
#include <queue>   
#include <assert.h>
using namespace std;
#define bgreen "\e[1;32m"
#define byellow "\e[1;33m"
#define bblue "\e[1;34m"
#define cyan "\e[0;36m"
#define ANSI_RESET "\x1b[0m"


typedef long long LL;
typedef struct th_args {
    
} th_args;

typedef struct data {
    bool busy;
    string value;
} data;

///////////////////////////////////////////////////

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;
#define MAX_CLIENTS 11
#define PORT_ARG 8001

///////////////////////////////////////////////////

const int initial_msg_len = 256;
const LL buff_sz = 1048576;                             // for receiving the string from client

pthread_mutex_t socketlock = PTHREAD_MUTEX_INITIALIZER; // lock for sending string on socket
pthread_mutex_t lock[101];                              // lock for each key of dictionary
pthread_mutex_t quelock = PTHREAD_MUTEX_INITIALIZER;    // lock for queue
pthread_cond_t condvar = PTHREAD_COND_INITIALIZER;      // conditional variable for multiple threads

map<int, data> mydict;                                  // server's common dictionary 
queue<int *> q;                                         // queue of addresses of client file descriptors

///////////////////////////////////////////////////

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform requicyan operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(bblue "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&condvar, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}

///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
   
    // create socket
    //the server must have a special socket to welcome some initial contact from client process
    //get welcome socket
    //get ip,port
     wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

  
    //binds socket to the port number
    /*  bind() assigns address specified by addr to the socket refercyan to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr. */

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
   
   
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurcyan in SERVER");
            exit(-1);
        }

        printf(bgreen "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        int* pclient = (int*) malloc(sizeof(int)); 
        *pclient = client_socket_fd;

        pthread_mutex_lock(&quelock);
            q.push(pclient);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&condvar);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        pthread_mutex_destroy(&lock[i]);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/rishabh jain_305876_assignsubmission_file_/assignment5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <string>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;

//colour coding
#define black "\e[1;30m"
#define red "\e[1;31m"
#define green "\e[1;32m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "--------" << endl;
#define debug(x) cout << #x << " : " << x << endl

#define SERVER_PORT 8001
typedef struct packet {
    int time;
    string command;
} packet;

const LL buff_sz = 1039999;
int socket_fd;
vector<packet> requests;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debugging
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}



int send_string_on_socket(int fd, const string &s)
{
  
    int bytes_sent = write(fd, s.c_str(), s.length());
  
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
  
        exit(-1);
    }

    return bytes_sent;
}
int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
// converts ip-address in dots and numbers

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(green "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}

////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    int *thread_idx_addr = (int *)args;
    int thread_idx = *thread_idx_addr;

    // Delay sending request
    sleep(requests[thread_idx].time);
    string cmd = requests[thread_idx].command;

    pthread_mutex_lock(&lock);
    send_string_on_socket(socket_fd, cmd);
    pthread_mutex_unlock(&lock);

    int num_bytes_read;
    string output_msg;

    pthread_mutex_lock(&lock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << thread_idx << ":" << gettid() << ":" << output_msg << endl;
    pthread_mutex_unlock(&lock);

    return NULL;
}

////////////////////////////////////////////////////////

int main(int argc, char *argv[]) 
{
    int m, i;
    string m_str;
    getline(cin, m_str);
    m = stoi(m_str);            // Number of clients/requests

    // Get input line by line as a single string and parse to separate the send-time
    for (i = 0; i < m; i++) {
        string input, time_str = "";
        getline(cin, input);
        auto it = input.begin();
        while (*it != ' ') {
            time_str += *it;
            it++;
        }
        it++;
        packet data;
        data.time = stoi(time_str);
        string cmd(it, input.end());
        data.command = cmd;
        requests.push_back(data);
    }

    struct sockaddr_in server_obj;
    socket_fd = get_socket_fd(&server_obj);
    // cout << "Connection to server successful" << endl;

    // Multithreading
    pthread_t clients[m];
    int thread_idx[m];

    for (i = 0; i < m; i++) {
        thread_idx[i] = i;
        pthread_create(&clients[i], NULL, begin_process, (void *)&thread_idx[i]);
    }

    for (i = 0; i < m; i++) {
        pthread_join(clients[i], NULL);
    }

    return 0;
}