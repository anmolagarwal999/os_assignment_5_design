
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/client/main.cpp ####################//

#include <iostream>
#include <string>
#include <stdlib.h>
#include <pthread.h>

#include "client.h"

using namespace std;
extern pthread_mutex_t out_lock;
void sim(void)
{
    int N;
    cin >> N;
    pthread_t threads[N];
    for (int i = 0; i < N; i++)
    {
        request* r = (request*) malloc(sizeof(request));
        int time;
        string s;
        cin >> r->time;
        getline(cin, s);
        s = s.substr(1);
        r->msg = s;
        r->index = i;
        pthread_create(&threads[i], NULL, begin_process, r);
    }
    for (int i = 0; i < N; i++)
    {
        pthread_join(threads[i],NULL);
    }
    return;
}

int main()
{
    pthread_mutex_init(&out_lock, NULL);
    sim();
    return 0;
}
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/client/client.h ####################//

#if !defined(CLIENT_H)
#define CLIENT_H

#include <string>

using namespace std;

typedef struct request
{
    int index;
    int time;
    string msg;
} request;

void* begin_process(void *req);

#endif // CLIENT_H

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/client/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

#include "client.h"

using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"



typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

#define SERVER_PORT 8080

pthread_mutex_t out_lock;
const LL buff_sz = 1048576;
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    return socket_fd;
}

void* begin_process(void *req)
{
    request* r = (request*)req;
    sleep(r->time);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    send_string_on_socket(socket_fd, r->msg);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&out_lock);
    cout << r->index << ":" << output_msg << endl;
    pthread_mutex_unlock(&out_lock);
    close(socket_fd);
    return NULL;
}

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/server/main.cpp ####################//

#include <iostream>
#include <stdlib.h>

using namespace std;

#include "server.h"
#include "handler.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Usage: ./srvr <num_threads>" << endl;
        return 1;
    }
    int num_threads = atoi(argv[1]);
    init_handlers(num_threads);
    start_server(num_threads);
}
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/server/server.h ####################//

#if !defined(SERVER_H)
#define SERVER_H

#include <queue>
using namespace std;

#define PORT 8080
extern pthread_mutex_t requests_lock;
extern pthread_cond_t requests_cond;
extern queue<int> requests;

void start_server(int num_req);

#endif // SERVER_H

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/server/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <queue>

#include "server.h"

using namespace std;

pthread_mutex_t requests_lock;
pthread_cond_t requests_cond;
queue<int> requests;

void start_server(int num_req)
{
    int wel_sock, client_sock;
    struct sockaddr_in server_addr, client_addr;

    wel_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_sock < 0)
    {
        perror("ERROR creating welcome socket: ");
        exit(1);
    }

    bzero((char *)&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    if (bind(wel_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("ERROR on binding: ");
        exit(1);
    }

    listen(wel_sock, 1024);
    cout << "Server is listening on port: " << PORT << endl;
    socklen_t client_len = sizeof(client_addr);

    while (1)
    {
        client_sock = accept(wel_sock, (struct sockaddr *)&client_addr, &client_len);
        if (client_sock < 0)
        {
            perror("ERROR on accept: ");
            exit(1);
        }
        cout << "Client connected on port " << ntohs(client_addr.sin_port) << " and IP " << inet_ntoa(client_addr.sin_addr) << endl;
        pthread_mutex_lock(&requests_lock);
        requests.push(client_sock);
        pthread_cond_signal(&requests_cond);
        pthread_mutex_unlock(&requests_lock);
    }
    close(wel_sock);

    cout << "Server closed" << endl;
}

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/server/handler.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

//////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <queue>
#include <string>
#include <unordered_map>

using namespace std;

#include "server.h"
#include "handler.h"

val M[101];

void init_dict(void)
{
    for (int i = 0; i <= 100; i++)
    {
        pthread_mutex_init(&M[i].lock, NULL);
        M[i].s = "";
    }
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

string process_cmd(string cmd)
{
    string op = cmd.substr(0, cmd.find(" "));
    string ret = "Invalid operation";
    if (op == "insert")
    {
        string tmp = cmd.substr(cmd.find(" ") + 1);
        int key = stoi(tmp.substr(0, tmp.find(" ")));
        string value = tmp.substr(tmp.find(" ") + 1);
        pthread_mutex_lock(&M[key].lock);
        if (M[key].s.empty())
        {
            M[key].s = value;
            ret = "Insertion successful";
        }
        else
        {
            ret = "Key already exists";
        }
        pthread_mutex_unlock(&M[key].lock);
    }
    else if (op == "delete")
    {
        string tmp = cmd.substr(cmd.find(" ") + 1);
        int key = stoi(tmp);

        pthread_mutex_lock(&M[key].lock);
        if (M[key].s.size())
        {
            M[key].s = "";
            ret = "Deletion successful";
        }
        else
        {
            ret = "No such key exists";
        }
        pthread_mutex_unlock(&M[key].lock);
    }
    else if (op == "update")
    {
        string tmp = cmd.substr(cmd.find(" ") + 1);
        int key = stoi(tmp.substr(0, tmp.find(" ")));
        string value = tmp.substr(tmp.find(" ") + 1);

        pthread_mutex_lock(&M[key].lock);
        if (M[key].s.size())
        {
            M[key].s = value;
            ret = M[key].s;
        }
        else
        {
            ret = "No such key exists";
        }
        pthread_mutex_unlock(&M[key].lock);
    }
    else if (op == "fetch")
    {
        string tmp = cmd.substr(cmd.find(" ") + 1);
        int key = stoi(tmp);
        cout << "key: " << key << endl;

        pthread_mutex_lock(&M[key].lock);
        if (M[key].s.size())
        {
            ret = M[key].s;
        }
        else
        {
            ret = "Key does not exist";
        }
        pthread_mutex_unlock(&M[key].lock);
    }
    else if (op == "concat")
    {
        string tmp = cmd.substr(cmd.find(" ") + 1);
        int key1 = stoi(tmp.substr(0, tmp.find(" ")));
        int key2 = stoi(tmp.substr(tmp.find(" ") + 1));

        while (true)
        {
            int k1_lock = pthread_mutex_trylock(&M[key1].lock);
            int k2_lock = pthread_mutex_trylock(&M[key2].lock);
            if (k1_lock == 0 && k2_lock == 0)
            {
                break;
            }
            else
            {
                pthread_mutex_unlock(&M[key1].lock);
                pthread_mutex_unlock(&M[key2].lock);
            }
        }
        if (M[key1].s.size() && M[key2].s.size())
        {
            string t = M[key1].s;
            M[key1].s += M[key2].s;
            M[key2].s += t;
            ret = M[key2].s;
        }
        else
        {
            ret = "Concat failed as at least one of the keys does not exist";
        }
        pthread_mutex_unlock(&M[key1].lock);
        pthread_mutex_unlock(&M[key2].lock);
    }
    return to_string(pthread_self()) + ":" + ret;
}

void *handler(void *arg)
{
    for (;;)
    {
        pthread_mutex_lock(&requests_lock);
        while (requests.empty())
        {
            pthread_cond_wait(&requests_cond, &requests_lock);
        }
        int client_sock = requests.front();
        requests.pop();
        pthread_mutex_unlock(&requests_lock);
        int received_num, sent_num;

        int ret_val = 1;

        string cmd;
        string msg_to_send_back;
        int sent_to_client;
        tie(cmd, received_num) = read_string_from_socket(client_sock, 1048576);
        ret_val = received_num;
        if (ret_val <= 0)
        {
            perror("Error reading from socket: ");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        msg_to_send_back = process_cmd(cmd);

        sleep(2);
        sent_to_client = send_string_on_socket(client_sock, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    close_client_socket_ceremony:;
        close(client_sock);
        cout << "Thread " << pthread_self() << " finished handling request" << endl;
    }
    return NULL;
}

void init_handlers(int num_threads)
{
    pthread_t threads[num_threads];
    for (int i = 0; i < num_threads; i++)
    {
        pthread_create(&threads[i], NULL, handler, NULL);
    }
}
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q3/server/handler.h ####################//

#if !defined(HANDLER_H)
#define HANDLER_H

#include <pthread.h>
#include <string>

using namespace std;

void init_handlers(int num_threads);

typedef struct val
{
    string s;
    pthread_mutex_t lock;
} val;

#endif // HANDLER_H
