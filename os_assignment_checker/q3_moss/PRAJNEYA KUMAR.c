
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q3/server.cpp ####################//

// C program for the Server Side
 
// inet_addr
#include <arpa/inet.h>
 
// For threading, link with lpthread
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <cstdlib>

#include <bits/stdc++.h>
#include <map>
using namespace std;
 
// Semaphore variables
sem_t x[128];
pthread_t tid;
pthread_t insertthreads[100];
pthread_t deletethreads[100];
pthread_t updatethreads[100];
pthread_t concatthreads[100];
pthread_t fetchthreads[100];

//dictionary
map<int, string> m;
// queue
priority_queue<int> pq;


typedef struct Command{
    int time;
    int command_type;
    // 0 insert
    // 1 delete
    // 2 update
    // 3 contact
    // 4 fetch
    int key1;
    int key2;
    char val[100];
} Command;

typedef struct threadArg{
	Command * command;
	int socket;
} threadArg;

typedef struct ErrorCode{
	int error;
	char value[200];
	pid_t thread_id;
} ErrorCode;

unsigned char * serialize_int(unsigned char *buffer, int value)
{
  buffer[0] = value >> 24;
  buffer[1] = value >> 16;
  buffer[2] = value >> 8;
  buffer[3] = value;
  return buffer + 4;
}

unsigned char * serialize_string(unsigned char *buffer, int size, char * value)
{
  int i = 0;
  for(i = 0; i < size; i++){
  	buffer[i] = value[i];
  }
  return buffer + i;
}

unsigned char * serialize_command(unsigned char *buffer, struct ErrorCode *value)
{
  buffer = serialize_int(buffer, value->error);
  buffer = serialize_int(buffer, value->thread_id);
  buffer = serialize_int(buffer, strlen(value->value));
  buffer = serialize_string(buffer, strlen(value->value), value->value);
  return buffer;
}

int sendErrorCommand(int socket, struct ErrorCode *error)
{
  unsigned char buffer[512], *ptr;

  ptr = serialize_command(buffer, error);

  return send(socket, buffer, ptr - buffer, 0);
}


// Insert Function
void* insertRunner(void* a)
{

	threadArg * allargs = (threadArg *) a;
	Command * c = (Command *) allargs->command;
	int network_socket = allargs->socket;

    // Lock the semaphore
    sem_wait(&x[c->key1]);

    ErrorCode * e = (ErrorCode *) malloc(sizeof(ErrorCode));
    strcpy(e->value, "");
    e->thread_id = gettid();

    // cout << "ATTEMPT " << c->command_type << " " << c->key1 << " " << c->val << endl;

    if (m.find(c->key1) == m.end()){
    	e->error = 0;
    	m[c->key1] = c->val;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
    else{
    	e->error = 1;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
 
    // Unlock the semaphore
    sem_post(&x[c->key1]);
    // cout << "POOPING OUT " << pq.top() << endl;
    pq.pop();

    pthread_exit(NULL);
}
 
// Delete Function
void* deleteRunner(void* a)
{
    threadArg * allargs = (threadArg *) a;
	Command * c = (Command *) allargs->command;
	int network_socket = allargs->socket;
    // Lock the semaphore
    sem_wait(&x[c->key1]);

    ErrorCode * e = (ErrorCode *) malloc(sizeof(ErrorCode));
    strcpy(e->value, "");
    e->thread_id = gettid();

    if (m.find(c->key1) == m.end()){
    	e->error = 2;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
    else{
    	e->error = 3;
    	m.erase(c->key1);
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
 
    // Unlock the semaphore
    sem_post(&x[c->key1]);
    // cout << "POOPING OUT " << pq.top() << endl;
    pq.pop();

    pthread_exit(NULL);
}

// Update Function
void* updateRunner(void* a)
{
	threadArg * allargs = (threadArg *) a;
	Command * c = (Command *) allargs->command;
	int network_socket = allargs->socket;
    // Lock the semaphore
    sem_wait(&x[c->key1]);

    ErrorCode * e = (ErrorCode *) malloc(sizeof(ErrorCode));
    strcpy(e->value, "");
    e->thread_id = gettid();

    if (m.find(c->key1) == m.end()){
    	e->error = 4;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
    else{
    	e->error = 5;
    	m[c->key1] = c->val;
    	strcpy(e->value, c->val);
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
 
    // Unlock the semaphore
    sem_post(&x[c->key1]);
    // cout << "POOPING OUT " << pq.top() << endl;
    pq.pop();

    pthread_exit(NULL);
}

// Concat Function
void* concatRunner(void* a)
{
	threadArg * allargs = (threadArg *) a;
	Command * c = (Command *) allargs->command;
	int network_socket = allargs->socket;

    // Lock the semaphore
    sem_wait(&x[c->key1]);
    sem_wait(&x[c->key2]);

    ErrorCode * e = (ErrorCode *) malloc(sizeof(ErrorCode));
    strcpy(e->value, "");
    e->thread_id = gettid();

    if (m.find(c->key1) == m.end() || m.find(c->key2) == m.end()){
    	e->error = 6;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
    else{
    	e->error = 7;

    	string fstring = "";

    	for(int i = 0; i < m[c->key1].length(); i++){
    		fstring += m[c->key1][i];
    	}

    	string lstring = "";

    	for(int i = 0; i < m[c->key2].length(); i++){
    		lstring += m[c->key2][i];
    	}

    	m[c->key1] = fstring+lstring;
    	m[c->key2] = lstring+fstring;

    	strcpy(e->value, m[c->key2].c_str());
    	sleep(2);

    	sendErrorCommand(network_socket, e);
    }
 
    // Unlock the semaphore
    sem_post(&x[c->key1]);
    sem_post(&x[c->key2]);
    // cout << "POOPING OUT " << pq.top() << endl;
    pq.pop();

    pthread_exit(NULL);
}

// Fetch Function
void* fetchRunner(void* a)
{
	threadArg * allargs = (threadArg *) a;
	Command * c = (Command *) allargs->command;
	int network_socket = allargs->socket;
    // Lock the semaphore
    sem_wait(&x[c->key1]);

    ErrorCode * e = (ErrorCode *) malloc(sizeof(ErrorCode));
    strcpy(e->value, "");
    e->thread_id = gettid();

    if (m.find(c->key1) == m.end()){
    	e->error = 8;
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
    else{
    	e->error = 9;
    	strcpy(e->value, m[c->key1].c_str());
    	sleep(2);
    	sendErrorCommand(network_socket, e);
    }
 
    // Unlock the semaphore
    sem_post(&x[c->key1]);
    // cout << "POOPING OUT " << pq.top() << endl;
    pq.pop();

    pthread_exit(NULL);
}

void runCommand(Command * c, threadArg * ta, int i){
    if (c->command_type == 0) {
        // Create insert thread
        if (pthread_create(&insertthreads[i], NULL,
                           insertRunner, ta)
            != 0)

            // Error in creating thread
            printf("Failed to create thread\n");
    }
    else if (c->command_type == 1) {
        // Create delete thread
        if (pthread_create(&deletethreads[i], NULL,
                           deleteRunner, ta)
            != 0)

            // Error in creating thread
            printf("Failed to create thread\n");
    }
    else if (c->command_type == 2) {
        // Create update thread
        if (pthread_create(&updatethreads[i], NULL,
                           updateRunner, ta)
            != 0)

            // Error in creating thread
            printf("Failed to create thread\n");
    }
    else if (c->command_type == 3) {
        // Create concat thread
        if (pthread_create(&concatthreads[i], NULL,
                           concatRunner, ta)
            != 0)

            // Error in creating thread
            printf("Failed to create thread\n");
    }
    else if (c->command_type == 4) {
        // Create fetch thread
        if (pthread_create(&fetchthreads[i], NULL,
                           fetchRunner, ta)
            != 0)

            // Error in creating thread
            printf("Failed to create thread\n");
    }
    else{
        cout << "INVALID COMMAND" << endl;
    }
}
 
// Driver Code
int main(int argc, char *argv[])
{

    if(argc!=2){
        cout << "USAGE: ./server n" << endl;
        return 0;
    }

    int n = atoi(argv[1]);

    // Initialize variables
    int serverSocket, newSocket;
    struct sockaddr_in serverAddr;
    struct sockaddr_storage serverStorage;
 
    socklen_t addr_size;

    for(int z = 0; z<128; z++)
    	sem_init(&x[z], 0, 1);
 
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(8989);
 
    // Bind the socket to the
    // address and port number.
    bind(serverSocket,
         (struct sockaddr*)&serverAddr,
         sizeof(serverAddr));
 
    // Listen on the socket,
    // with n max connection
    // requests queued
    if (listen(serverSocket, n) == 0)
        printf("Listening\n");
    else
        printf("Error\n");

    // Array for thread
    pthread_t tid[60];
 
    int i = 0;
 
    while (1) {
        addr_size = sizeof(serverStorage);
 
        // Extract the first
        // connection in the queue
        newSocket = accept(serverSocket,
                           (struct sockaddr*)&serverStorage,
                           &addr_size);

        unsigned char buffer[512];

        int recVal = recv(newSocket, buffer, sizeof(buffer), 0);

        // cout << "RECEIVED " << recVal << " BYTES" << endl;

        Command * c = (Command *) malloc(sizeof(Command));

        c->time = (buffer[3]<<0) | (buffer[2]<<8) | (buffer[1]<<16) | (buffer[0]<<24);
        c->command_type = (buffer[7]<<0) | (buffer[6]<<8) | (buffer[5]<<16) | (buffer[4]<<24);

        int len_key1, len_key2, len_val;
        
        c->key1 = (buffer[11]<<0) | (buffer[10]<<8) | (buffer[9]<<16) | (buffer[8]<<24);
        c->key2 = (buffer[15]<<0) | (buffer[14]<<8) | (buffer[13]<<16) | (buffer[12]<<24);

        len_val = (buffer[19]<<0) | (buffer[18]<<8) | (buffer[17]<<16) | (buffer[16]<<24);

        for(int z=20; z<20+len_val; z++){
        	c->val[z-20] = buffer[z];
        }

        // cout << c->time << " - " << c->command_type << " - "  << c->key1 << " - "  << c->key2 << " - "  << c->val << endl;

        threadArg * ta = (threadArg *) malloc(sizeof(threadArg));
        ta->command = c;
        ta->socket = newSocket;

        // cout << "THREAD" << ta->command->time << " " << ta->command->command_type << " "  << ta->command->key1 << " "  << ta->command->key2 << " "  << ta->command->val << endl;

        if(pq.empty() || pq.top()>=c->time){
            // cout << "PUSHING " << c->time << endl;
            pq.push(c->time);
            runCommand(c, ta, i);
        }
        else{
            int thread_count = i;
            int z = 0;

            while (z<thread_count){
                // Suspend execution of
                // the calling thread
                // until the target
                // thread terminates
                pthread_join(insertthreads[z++],
                             NULL);
                pthread_join(deletethreads[z++],
                             NULL);
                pthread_join(updatethreads[z++],
                             NULL);
                pthread_join(concatthreads[z++],
                             NULL);
                pthread_join(fetchthreads[z++],
                             NULL);
            }
            // cout << "WAITING FOR REST THREADS TO FINISH " << c->time << endl;
            pq.push(c->time);
            runCommand(c, ta, i);
        }
        i++;
 
        if (i >= n) {
            // Update i
            i = 0;
 
            while (i < n) {
                // Suspend execution of
                // the calling thread
                // until the target
                // thread terminates
                pthread_join(insertthreads[i++],
                             NULL);
                pthread_join(deletethreads[i++],
                             NULL);
                pthread_join(updatethreads[i++],
                             NULL);
                pthread_join(concatthreads[i++],
                             NULL);
                pthread_join(fetchthreads[i++],
                             NULL);
            }
 
            cout << "MAX WORKER THREAD LIMIT REACHED" << endl;
            return 0;
        }
    }
 
    return 0;
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q3/client.cpp ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
 
// inet_addr
#include <arpa/inet.h>
#include <unistd.h>
 
// For threading, link with lpthread
#include <pthread.h>
#include <semaphore.h>

#include <iostream>
using namespace std;

typedef struct Command{
	int uid;
    int time;
    int command_type;
    // 0 insert
    // 1 delete
    // 2 update
    // 3 contact
    // 4 fetch
    int key1;
    int key2;
    char val[100];
} Command;

Command ** commands;

pthread_t * command_t;

unsigned char * serialize_int(unsigned char *buffer, int value)
{
  /* Write big-endian int value into buffer; assumes 32-bit int and 8-bit char. */
  buffer[0] = value >> 24;
  buffer[1] = value >> 16;
  buffer[2] = value >> 8;
  buffer[3] = value;
  return buffer + 4;
}

unsigned char * serialize_string(unsigned char *buffer, int size, char *value)
{
  int i = 0;
  for(i = 0; i < size; i++){
  	buffer[i] = value[i];
  }
  return buffer + i;
}

unsigned char * serialize_command(unsigned char *buffer, struct Command *value)
{
  buffer = serialize_int(buffer, value->time);
  buffer = serialize_int(buffer, value->command_type);
  buffer = serialize_int(buffer, value->key1);
  buffer = serialize_int(buffer, value->key2);
  buffer = serialize_int(buffer, strlen(value->val));
  buffer = serialize_string(buffer, strlen(value->val), value->val);
  return buffer;
}

int send_Command(int socket, struct Command *command)
{
  unsigned char buffer[512], *ptr;

  ptr = serialize_command(buffer, command);

  return send(socket, buffer, ptr - buffer, 0);
}

 
// Function to send data to
// server socket.
void* commandRunner(void* a)
{
 
    Command * c = (Command *)a;

    int network_socket;
 
    // Create a stream socket
    network_socket = socket(AF_INET,
                            SOCK_STREAM, 0);
 
    // Initialise port number and address
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(8989);
 	
 	// sleep for time allocated
 	sleep(c->time);
    // Initiate a socket connection
    int connection_status = connect(network_socket,
                                    (struct sockaddr*)&server_address,
                                    sizeof(server_address));
 
    // Check for connection error
    if (connection_status < 0) {
        puts("Error\n");
        return 0;
    }
 
    // printf("%d Connection established\n", c->uid);
    // Send data to the socket
    int sendVal = send_Command(network_socket, c);

    // cout << "SENT " << sendVal << " BYTES" << endl;

    unsigned char buffer[512];
    int recVal = recv(network_socket, buffer, sizeof(buffer), 0);

    int errcode = (buffer[3]<<0) | (buffer[2]<<8) | (buffer[1]<<16) | (buffer[0]<<24);
    int thread_id = (buffer[7]<<0) | (buffer[6]<<8) | (buffer[5]<<16) | (buffer[4]<<24);
    int len_val = (buffer[11]<<0) | (buffer[10]<<8) | (buffer[9]<<16) | (buffer[8]<<24);

    int z = 0;
    char ret_value[200];

    for(z = 12; z<12+len_val; z++){
    	ret_value[z-12] = buffer[z];
    }

    // cout << "RECEIVED " << recVal << " BYTES" << endl;

    if(errcode==0){
    	cout << c->uid << ":" << thread_id << ":Insertion Successful" << endl;
    }
    else if(errcode==1){
    	cout << c->uid << ":" << thread_id << ":Key already exists" << endl;
    }
    else if(errcode==2){
    	cout << c->uid << ":" << thread_id << ":No such key exists" << endl;
    }
    else if(errcode==3){
    	cout << c->uid << ":" << thread_id << ":Deletion Successful" << endl;
    }
    else if(errcode==4){
    	cout << c->uid << ":" << thread_id << ":Key does not exist" << endl;
    }
    else if(errcode==5){
    	printf("%d:%d:%s\n", c->uid, thread_id, ret_value);
    }
    else if(errcode==6){
    	cout << c->uid << ":" << thread_id << ":Concat failed as at least one of the keys does not exist" << endl;
    }
    else if(errcode==7){
    	printf("%d:%d:%s\n", c->uid, thread_id, ret_value);
    }
    else if(errcode==8){
    	cout << c->uid << ":" << thread_id << ":Key does not exist" << endl;
    }
    else if(errcode==9){
    	printf("%d:%d:%s\n", c->uid, thread_id, ret_value);
    }
 
    // Close the connection
    close(network_socket);

    pthread_exit(NULL);
 
    return 0;
}
 
// Driver Code
int main()
{

	int m;
    scanf("%d", &m);

    commands = (Command **) malloc(sizeof(Command *) * m);

    for(int i = 0; i < m; i++){
    	Command * c = (Command *) malloc(sizeof(Command));
    	scanf(" %d", &c->time);

    	c->uid = i;

    	char command[50];
    	scanf("%s", command);

    	if(!strcmp(command, "insert")){
    		c->command_type = 0;
    		scanf("%d %s", &c->key1, c->val);

    		c->key2 = 0;
    	}
    	else if(!strcmp(command, "delete")){
    		c->command_type = 1;
    		scanf("%d", &c->key1);

    		c->key2 = 0;
    		strcpy(c->val, "NULL");
    	}
    	else if(!strcmp(command, "update")){
    		c->command_type = 2;
    		scanf("%d %s", &c->key1, c->val);

    		c->key2 = 0;
    	}
    	else if(!strcmp(command, "concat")){
    		c->command_type = 3;
    		scanf("%d %d", &c->key1, &c->key2);

    		strcpy(c->val, "NULL");
    	}
    	else if(!strcmp(command, "fetch")){
    		c->command_type = 4;
    		scanf("%d", &c->key1);

    		c->key2 = 0;
    		strcpy(c->val, "NULL");
    	}
    	commands[i] = c;
    }

    command_t = (pthread_t *) malloc(sizeof(pthread_t) * m);
    for(int i = 0; i < m; i++){
    	pthread_create(&command_t[i], 0, commandRunner, commands[i]);
    }

    int i = 0;
	// sleep(5);
	while(i<m){
		pthread_join(command_t[i++],NULL);
		// printf("%d:\n",i);
	}
}