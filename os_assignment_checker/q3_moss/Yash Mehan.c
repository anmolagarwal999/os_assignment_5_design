
//###########FILE CHANGE ./main_folder/Yash Mehan_305808_assignsubmission_file_/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <map>
#include <string>
#include <vector>
#include <queue>        
using namespace std;
/////////////////////////////

typedef long long LL;

#define MAX_CLIENTS 4
#define PORT_ARG 8001
int threadpool_size;
const int initial_msg_len = 256;
const LL buff_sz = 1048576;
/////////////////////////////////////////
struct info 
{
    // bool init;
    string value;
};
vector <info> mydict(102);
queue <int*> q;
pthread_mutex_t lock[101];
pthread_mutex_t q_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_var = PTHREAD_COND_INITIALIZER;
////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }
    return bytes_sent;
}

void parse_and_do(string arg, int client_socket_fd)
{
    string msg;
    vector <string> tokenized;
    int i = 0;
    tokenized.push_back("");
    for(int n = 0; n< arg.size(); n++)
    {
        if(arg[n] != ' ')
            tokenized[i]+=arg[n];
        else
        {
            tokenized.push_back("");
            i++;
        }
    }

    int key = stoi(tokenized[1]);
    
    if(tokenized[0] == "insert")
    {
        if(mydict[key].value == ""){
            
        pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokenized[2];
        pthread_mutex_unlock(&lock[key]);
            //sleep(2);
            msg = "Insertion successful";
            send_string_on_socket(client_socket_fd, msg);
        }    
        else{
            //sleep(2);
            msg = "Key already exists";
            send_string_on_socket(client_socket_fd, msg);
        }    
    }
    else if (tokenized[0] == "delete")
    {
        if(mydict[key].value != ""){
        pthread_mutex_lock(&lock[key]);  
            mydict[key].value = ""; 
        pthread_mutex_unlock(&lock[key]);
            //sleep(2);
        msg = "Deletion successful";
        send_string_on_socket(client_socket_fd, msg);
        }
        else{
            //sleep(2);
            msg = "No such key exists";
            send_string_on_socket(client_socket_fd, msg);
        }
    }
    else if (tokenized[0] == "update")
    {
        if(mydict[key].value != ""){
        pthread_mutex_lock(&lock[key]);
            mydict[key].value = tokenized[2];
        pthread_mutex_unlock(&lock[key]);
                        //sleep(2);
            send_string_on_socket(client_socket_fd, mydict[key].value);
        }
        else{
                        //sleep(2);
            msg = "Key does not exist";
            send_string_on_socket(client_socket_fd, msg);
        }
    }
    else if (tokenized[0] == "concat")
    {
        int key2 = stoi(tokenized[2]);
        if(mydict[key].value != "" && mydict[key2].value != ""){
        string val1 = mydict[key].value;
        string val2 = mydict[key2].value;
        pthread_mutex_lock(&lock[max(key, key2)]);
        pthread_mutex_lock(&lock[min(key, key2)]);
            mydict[key].value = val1+val2;
            mydict[key2].value = val2+val1;
        pthread_mutex_unlock(&lock[max(key, key2)]);
        pthread_mutex_unlock(&lock[min(key, key2)]);
                        //sleep(2);
            send_string_on_socket(client_socket_fd, mydict[key2].value);
        }
        else{
                          //sleep(2);
            msg = "Concat failed as at least one of the keys does not exist";
            send_string_on_socket(client_socket_fd, msg);
        }
    }
    else if (tokenized[0] == "fetch")
    {
        if(mydict[key].value != ""){
                        //sleep(2);
            pthread_mutex_lock(&lock[key]);
                send_string_on_socket(client_socket_fd, mydict[key].value);   
            pthread_mutex_unlock(&lock[key]);
        }
        else{
            msg = "Key doesnt exist";
                        //sleep(2);
            send_string_on_socket(client_socket_fd, msg);
        }
    }    
}
//////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    string output;
    output.resize(bytes);
    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

void handle_connection(int client_socket_fd)
{

    int received_num, sent_num;
    int ret_val = 1;

    // while (true)
    // {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl; // what client sent is in `cmd` variable        
        parse_and_do(cmd, client_socket_fd);
    // }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf("Disconnected from client\n");
    // return NULL;
}

void* thread_func(void* arg)
{
    while(true)
    {
        int* pclient;
        pthread_mutex_lock(&q_lock);
        while(q.empty()){
            pthread_cond_wait(&condition_var, &q_lock);
        }
        pthread_mutex_unlock(&q_lock);
        if(!q.empty())
        {
            pthread_mutex_lock(&q_lock);
            pclient = q.front();
            q.pop();
            pthread_mutex_unlock(&q_lock);
            handle_connection(*pclient);
        }
    }
}


int main(int argc, char *argv[])
{
    if(argc < 2){cout<<"not enough arguments supplied\n"; exit(0);}
    threadpool_size = stoi(argv[1]);
    pthread_t threadpool[threadpool_size];

    for(auto it : mydict)
        it.value = "";

    //setup and initialse all the locks that will be used
    for(int i = 0; i<101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }
    //creating a threadpool_size number of threads, these will wait for work and then start working
    for(int i = 0; i< threadpool_size; i++){
        pthread_create(&threadpool[i], NULL, thread_func, NULL);
    }

    int i, j, k, t, n;
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        printf("New client connected from port number %d and IP %s \n" ,ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        //write code which "hands off" each new request to an idling thread
        //i.e. write a request-thread assigning interface
        int* pclient = (int*) malloc(sizeof(int)); //OYE IS MALLOC KO KAB FREE KARNA HAI
        *pclient = client_socket_fd;

        pthread_mutex_lock(&q_lock);
            q.push(pclient);
        pthread_mutex_unlock(&q_lock);

        pthread_cond_signal(&condition_var);
    }
    close(wel_socket_fd);
    for(int i = 0; i<101; i++){
        pthread_mutex_destroy(&lock[i]);
    }
    return 0;
}   
//###########FILE CHANGE ./main_folder/Yash Mehan_305808_assignsubmission_file_/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

struct list_item
{
    int sending_time;
    string command;
};

struct int_package
{int i;};

string m_str;
int m;
// int socket_fd;
vector <list_item> request_list;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

const long long int buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

int new_socket()
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    // cout << "new socket made with fd" <<socket_fd << endl;
    return socket_fd;
}

void* thread_func(void* arg)
{
    int_package* idx = (int_package*) arg;
    int whichindex = idx -> i;      
    // cout<<"thread "<<whichindex<<" made\n"; 
    // cout<<whichindex<<" "<<request_list[whichindex].command<<"\n";
    sleep(request_list[whichindex].sending_time);
    
    int socket_fd = new_socket();
    
    pthread_mutex_lock(&lock);
        send_string_on_socket(socket_fd, request_list[whichindex].command);
    pthread_mutex_unlock(&lock);
    
    int num_bytes_read;
    string output_msg;
    
    pthread_mutex_lock(&lock);
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        cout << whichindex << ":"<<gettid()<<":"<<output_msg << endl;
    pthread_mutex_unlock(&lock);
    return NULL;
}

void take_input()
{
    string inp;
        string send_time_str = "";
        int send_time;
        getline(cin, inp);
            auto it = inp.begin();
            while(*it != ' '){
                send_time_str += *it;
                it++;
            }
            it++;
        send_time = stoi(send_time_str);
        string to_send(it, inp.end()); 
        list_item l;
        l.sending_time = send_time;
        l.command = to_send;
        request_list.push_back(l);
}

int main(int argc, char *argv[])
{
    // freopen("input.txt", "r", stdin);
    
    int i, j, k, t, n;
    getline(cin, m_str);
    m = stoi(m_str); //now i have m

    pthread_t threadpool[m];
    int_package indices[m];
    
    for(int i = 0; i<m; i++)
        take_input();

    for(int j = 0; j< m; j++){
        indices[j].i = j;
        pthread_create(&threadpool[j], NULL, thread_func, (void*) &indices[j]);
    }

    for(int j = 0; j< m; j++){
        pthread_join(threadpool[j], NULL);
    }

    return 0;
}