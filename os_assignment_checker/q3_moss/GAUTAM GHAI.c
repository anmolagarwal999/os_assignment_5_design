
//###########FILE CHANGE ./main_folder/GAUTAM GHAI_305794_assignsubmission_file_/2020101020_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <bits/stdc++.h>
#include <semaphore.h>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
queue<int *> q;
pthread_mutex_t lockk = PTHREAD_MUTEX_INITIALIZER;
//pthread_mutex_t printing = PTHREAD_MUTEX_INITIALIZER;
//pthread_mutex_t cond;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;
int poolsize;
////////////////////////////////////

const LL buff_sz = 1048576;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *pclient_socket_fd,void*ind)
{
    int client_socket_fd = *((int *)pclient_socket_fd);
    int index=*((int*)ind);
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    //while (true)

    string cmd;
    //pthread_mutex_lock(&printing);
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    //pthread_mutex_unlock(&printing);
    ret_val = received_num;
    // debug(ret_val);
    // printf("Read something\n");
    string msg_to_send_back=cmd;
    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        //goto close_client_socket_ceremony;
    }
    else
    {
        /*vector<string> args;
        LL num_args = parseinput(cmd, args);

        if (args[0] == "insert" && num_args == 3)
        {
            LL r = insert(args);
            if (r == 1)
            {
                msg_to_send_back = "Key already exists";
            }
            else if (r == 2)
            {
                code 
                msg_to_send_back = "Invalid Key";
            }
            else if (r == 0)
            {
                /* code 
                msg_to_send_back = "Insertion Successful";
            }
        }
        else if (args[0] == "delete" && num_args == 2)
        {
            LL r = deleted(args);
            if (r == 1)
            {
                msg_to_send_back = "No such Key exists";
            }
            else if (r == 2)
            {
                /* code 
                msg_to_send_back = "Invalid Key";
            }
            else if (r == 0)
            {
                /* code 
                msg_to_send_back = "Deletion Successful";
            }
        }
        else if (args[0] == "update" && num_args == 3)
        {
            LL r = update(args);
            if (r == 1)
            {
                msg_to_send_back = "Key does not exist";
            }
            else if (r == 2)
            {
                /* code 
                msg_to_send_back = "Invalid Key";
            }
            else if (r == 0)
            {
                /* code 
                msg_to_send_back = args[2];
            }
        }
        else if (args[0] == "concat" && num_args == 3)
        {
            LL r = concat(args);
            if (r == 1)
            {
                msg_to_send_back = "Concat failed as atleast one of the keys does not exist";
            }
            else if (r == 2)
            {
                /* code 
                msg_to_send_back = "Invalid Key";
            }
            else if (r == 0)
            {
                /* code 
                int key = stoi(args[2]);
                msg_to_send_back = map_array[key];
            }
        }
        else if (args[0] == "fetch" && num_args == 2)
        {
            LL r = fetch(args);
            if (r == 1)
            {
                msg_to_send_back = "Key does not exist";
            }
            else if (r == 2)
            {
                /* code 
                msg_to_send_back = "Invalid Key";
            }
            else if (r == 0)
            {
                /* code 
                int key = stoi(args[1]);
                msg_to_send_back = map_array[key];
            }
        }
        else
        {
            /* code 
            msg_to_send_back = "Command is invalid";
        }*/
        cout << "Received "<<msg_to_send_back << endl; 
        msg_to_send_back = to_string(index) + ":" + msg_to_send_back;
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            //goto close_client_socket_ceremony;
        }
    }

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

    // debug(sent_to_client);

    //close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}
void *func(void *arg)
{
    int*index=(int*)arg;
    while (1)
    {
        /* code */
        int *pclient;
        pthread_mutex_lock(&lockk);
        //pthread_cond_wait(&cv,&lockk);
        pclient = q.front();

        q.pop();
        if (pclient == NULL)
        {
            pthread_cond_wait(&cv, &lockk);
            pclient = q.front();
            q.pop();
        }
        pthread_mutex_unlock(&lockk);
        if (pclient != NULL)
        {
            handle_connection(pclient,index);
        }
    }
    return NULL;
}
int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    //cout << argc << endl;
    if (argc != 2)
    {
        printf("Error: enter 2 arguments\n");
        exit(0);
    }
    /*pthread_mutex_init(&lockk, NULL);
    pthread_mutex_init(&printing, NULL);
    pthread_mutex_init(&cond, NULL);*/

    poolsize = atoi(argv[1]);
    pthread_t thread_pool[poolsize];
    for (int i = 0; i < poolsize; i++)
    {
        pthread_create(&thread_pool[i], NULL, func, &(i));
    }
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        //handle_connection(client_socket_fd);
        int *pclient = (int *)malloc(sizeof(int));
        *pclient = client_socket_fd;
        pthread_mutex_lock(&lockk);

        q.push(pclient);
        pthread_cond_signal(&cv);
        pthread_mutex_unlock(&lockk);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/GAUTAM GHAI_305794_assignsubmission_file_/2020101020_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pthread_mutex_t lock=PTHREAD_MUTEX_INITIALIZER;
struct command
{
    int time;
    int id;
    string operation;
} typedef command;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *st)
{
    //string s = *(string *)(st);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    command *ops = (command *)st;
    cout << "Connection to server successful" << endl;
    //int i=0;
    //while (i<n)

    string to_send=ops->operation.substr(1,ops->operation.length()-1);
    int x=ops->time;
    sleep(x);
    pthread_mutex_lock(&lock);
   // cout << "Enter msg: ";
    //getline(cin, to_send);
    send_string_on_socket(socket_fd, to_send);
    pthread_mutex_unlock(&lock);
    int num_bytes_read;
    string output_msg;
    
    pthread_mutex_lock(&lock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    //strcpy(str,output_msg);
    //char str[]=
    printf("%d : ",ops->id);
    cout << output_msg << endl;
    //cout << "Received: " << output_msg << endl;
    printf("===\n");
    //cout << "====" << endl;
    pthread_mutex_unlock(&lock);
    //i++;

    return NULL;
    // part;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    scanf("%d", &n);
    //char A[n][100];
    command list[n];
    pthread_mutex_init(&lock,NULL);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &list[i].time);
        getline(cin, list[i].operation);
        list[i].id = i;
        //list[i].operation=
        //getline(cin,A[i]);
        // scanf("%s", A[i]);
        ///cin >> A[i];
        //cout << A[i]<<endl;
    }
    pthread_t th_clients[n];
    for (int i = 0; i < n; i++)
    {
        //pthread_t t;
        pthread_create(&th_clients[i], NULL, begin_process, &list[i]);
        //pthread_join(t,NULL);
        // begin_process(n);
    }
    for (int i = 0; i < n; i++)
    {
        pthread_join(th_clients[i], NULL);
    }
    exit(0);
}