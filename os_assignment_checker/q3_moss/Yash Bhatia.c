
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q3/test.cpp ####################//

#include <bits/stdc++.h>
using namespace std;

vector<string> split(string str, string token){
    vector<string>result;
    while(str.size()){
        int index = str.find(token);
        if(index!=string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

int main()
{
    int m; cin >> m;
    vector<vector<string>>str;
    for(int i=0; i<m; i++)
    {
        string s;cin >> s;
        getline(cin,s);
        vector<string>temp = split(s, " ");
        str.push_back(temp);
    }
    for(int i=0; i<m; i++)
    {
        cout << str[i].size() << "\n";
    }
} 
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream> 
#include <semaphore.h>
#include <cstring>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <vector>
#include <tuple>
#include <queue>
#include <map>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
void __print(int x) {cerr << x;}
void __print(long x) {cerr << x;}
void __print(long long x) {cerr << x;}
void __print(unsigned x) {cerr << x;}
void __print(unsigned long x) {cerr << x;}
void __print(unsigned long long x) {cerr << x;}
void __print(float x) {cerr << x;}
void __print(double x) {cerr << x;}
void __print(long double x) {cerr << x;}
void __print(char x) {cerr << '\'' << x << '\'';}
void __print(const char *x) {cerr << '\"' << x << '\"';}
void __print(const string &x) {cerr << '\"' << x << '\"';}
void __print(bool x) {cerr << (x ? "true" : "false");}
 
template<typename T, typename V>
void __print(const pair<T, V> &x) {cerr << '{'; __print(x.first); cerr << ','; __print(x.second); cerr << '}';}
template<typename T>
void __print(const T &x) {int f = 0; cerr << '{'; for (auto &i: x) cerr << (f++ ? "," : ""), __print(i); cerr << "}";}
void _print() {cerr << "]\n";}
template <typename T, typename... V>
void _print(T t, V... v) {__print(t); if (sizeof...(v)) cerr << ", "; _print(v...);}
 
void AilaJaadu()
{
    freopen("error1.txt", "w", stderr);
    #define debug(x...) cerr << "[" << #x << "] = ["; _print(x);
}
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
int wel_socket_fd, client_socket_fd, port_number;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        //cerr << "Failed to read data from socket. \n";
        return {"-1",1};
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}


int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

vector<string> split(string str, char del) 
{
    vector<string> res;
    stringstream ss(str);
    string temp;
    while (getline(ss, temp, del)) 
    {
        res.push_back(temp);
    }
    return res;
}

int onetime;
sem_t semaphore;
map<int,string>dict;
pthread_mutex_t mutex[105];

void * handler(void *input) {
    string inp = string((char*)input);
    string res;
    sem_wait(&semaphore);
        vector<string>splitted = split(inp, ' ');
        res = res + splitted[splitted.size() - 1] + ":" + to_string(pthread_self())+":";
        string op = splitted[1];
        if(op == "insert")
        {
            int key = stoi(splitted[2]);
            string value = splitted[3];
            pthread_mutex_lock(&mutex[key]);
            if(dict.find(key) != dict.end())
            {
                res += "Key already exists";
            }
            else
            {
                dict[key] = value;
                res += "Insertion successful";
            }
            pthread_mutex_unlock(&mutex[key]);

        }

        if(op == "delete")
        {
            int key = stoi(splitted[2]);
            pthread_mutex_lock(&mutex[key]);
            if(dict.find(key) == dict.end())
            {
                res += "No such key exists";
            }
            else
            {
                dict.erase(key);
                res += "Deletion successful";
            }
            pthread_mutex_unlock(&mutex[key]);
        }

        if(op == "update")
        {
            int key = stoi(splitted[2]);
            string value = splitted[3];
            pthread_mutex_lock(&mutex[key]);
            if(dict.find(key) == dict.end())
            {
                res += "Key does not exist";
            }
            else
            {
                dict[key] = value;
                res += dict[key];
            }
            pthread_mutex_unlock(&mutex[key]);
        }

        if(op == "concat")
        {
            int key1 = stoi(splitted[2]);
            int key2 = stoi(splitted[3]);
            pthread_mutex_lock(&mutex[key1]);
            pthread_mutex_lock(&mutex[key2]);
            if(dict.find(key1) == dict.end() || dict.find(key2) == dict.end())
            {
                res += "Concat failed as at least one of the keys does not exist";
            }
            else
            {
                string s1 = dict[key1];
                string s2 = dict[key2];
                
                dict[key1] += s2;
                dict[key2] += s1;
                res += dict[key2];
            }
            pthread_mutex_unlock(&mutex[key2]);
            pthread_mutex_unlock(&mutex[key1]);
        }

        if(op == "fetch")
        {
            int key = stoi(splitted[2]);
            pthread_mutex_lock(&mutex[key]);
            if(dict.find(key) == dict.end())
            {
                res += "Key does not exist";
            }
            else
            {
                res += dict[key];
            }
            pthread_mutex_unlock(&mutex[key]);
        }
    cout << res << endl;
    sem_post(&semaphore);
    return NULL;
}


void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;
    for(int i=0;i<105;i++)
    {
        pthread_mutex_init(&mutex[i], NULL);
    }

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        pthread_t my_thread;
        //cout << cmd << "\n";
        //cerr << &cmd << "\n";
        char *s =(char *) malloc(sizeof(char) * 1024);
        strcpy(s, cmd.c_str());
        pthread_create(&my_thread, NULL, handler, s);
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        // string msg_to_send_back = "Ack: " + cmd;

        // ////////////////////////////////////////
        // // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        // int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // // debug(sent_to_client);
        // if (sent_to_client == -1)
        // {
        //     perror("Error while writing to client. Seems socket has been closed");
        //     goto close_client_socket_ceremony;
        // }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}


int main(int argc, char *argv[])
{
    AilaJaadu();
    int i, j, k, t, n;
    onetime = stoi(argv[1]);
    socklen_t clilen;
    sem_init(&semaphore, 0, onetime);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream> 
#include <ios>    
#include <limits>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back

void __print(int x) {cerr << x;}
void __print(long x) {cerr << x;}
void __print(long long x) {cerr << x;}
void __print(unsigned x) {cerr << x;}
void __print(unsigned long x) {cerr << x;}
void __print(unsigned long long x) {cerr << x;}
void __print(float x) {cerr << x;}
void __print(double x) {cerr << x;}
void __print(long double x) {cerr << x;}
void __print(char x) {cerr << '\'' << x << '\'';}
void __print(const char *x) {cerr << '\"' << x << '\"';}
void __print(const string &x) {cerr << '\"' << x << '\"';}
void __print(bool x) {cerr << (x ? "true" : "false");}
 
template<typename T, typename V>
void __print(const pair<T, V> &x) {cerr << '{'; __print(x.first); cerr << ','; __print(x.second); cerr << '}';}
template<typename T>
void __print(const T &x) {int f = 0; cerr << '{'; for (auto &i: x) cerr << (f++ ? "," : ""), __print(i); cerr << "}";}
void _print() {cerr << "]\n";}
template <typename T, typename... V>
void _print(T t, V... v) {__print(t); if (sizeof...(v)) cerr << ", "; _print(v...);}
 
void AilaJaadu()
{
    freopen("error.txt", "w", stderr);
    #define debug(x...) cerr << "[" << #x << "] = ["; _print(x);
}

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    debug(fd,bytes, output[0]);
    int bytes_received = read(fd, &output[0], bytes - 1);
    debug("Hi Debug");
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // // return "
        // exit(-1);
        return {"lol", -1};
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);
    debug(output,bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void begin_process()
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);


    cout << "Connection to server successful" << endl;
    
    while (true)
    {
        string to_send;
        cout << "Enter msg: ";
        getline(cin, to_send);
        send_string_on_socket(socket_fd, to_send);
        int num_bytes_read;
        string output_msg;
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        cout << "Received: " << output_msg << endl;
        cout << "====" << endl;
    }
    // part;
}

pthread_mutex_t mutex;

struct sockaddr_in server_obj;
int socket_fd;

void send_process(string p)
{
    pthread_mutex_lock(&mutex);    
    string to_send = p;
    send_string_on_socket(socket_fd, to_send);
    //int num_bytes_read;
    //string output_msg;
    //tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_unlock(&mutex);
}

void * thread_handler(void *arg)
{
    string s  = *(string*)arg;
    stringstream ss(s);
    string token;
    getline(ss, token, ' ');
    debug(token);
    int time = stoi(token);
    sleep(time);
    send_process(s);  
    return NULL;
}
int m;
void *read_mess(void *arg)
{
    int num_mess = m;
    while(num_mess)
    {
        string output_msg;
        int num_bytes_read;
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        debug(output_msg, num_bytes_read);
        debug(num_bytes_read);
        if(num_bytes_read != 0)
        {
            num_mess--;
            cout << output_msg << endl;
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    AilaJaadu();
    int i, j, k, t, n;
    cin >> m; 
    pthread_t socket_read;
    //pthread_create(&socket_read, NULL, &read_mess, NULL);
    pthread_mutex_init(&mutex, NULL);
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    vector<pthread_t> my_threads(m);
    vector<string>str;
    socket_fd = get_socket_fd(&server_obj);
    for(int i=0; i<m; i++)
    {
        string s;
        getline(cin,s);
        s = s + " " + to_string(i);
        str.push_back(s);
    }

    for(int i=0; i<m; i++)
    {
        pthread_create(&my_threads[i], NULL, &thread_handler, &str[i]);
    }
    
    for(int i=0; i<m; i++)
    {
        pthread_join(my_threads[i], NULL);
    }
    pthread_mutex_destroy(&mutex);
    //pthread_join(socket_read, NULL);
    return 0;
}