
//###########FILE CHANGE ./main_folder/Keshav Bajaj_305797_assignsubmission_file_/2019115010/q3/server.cpp ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define PORT 1337
#define BUFFER_SIZE 256
#define MAX_DICT_SIZE 101
#define MAX_STRING_SIZE 256
#define BACKLOG 5
#define MAX_RESPONSE_SIZE 256
#define MAX_REQUESTS 256
#define MAX_ARGS 256

using namespace std;

// char dict[MAX_DICT_SIZE][MAX_STRING_SIZE];
queue<pair<int, int *>> q;
vector<string> dict(MAX_DICT_SIZE, "");

pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t my_condition = PTHREAD_COND_INITIALIZER;
pthread_mutex_t my_dict_mutex[MAX_DICT_SIZE];

string take_input(char *args[MAX_ARGS])
{
    int key;
    string value;
    if (!(args[0] && args[1] && args[2]))
        return "Invalid number of argument\n";

    if (!(0 <= atoi(args[1]) && atoi(args[1]) <= 100))
        return "Invalid key\n";

    if (dict[atoi(args[1])].length())
        return "Key already exists\n";

    value = args[2];
    pthread_mutex_lock(&my_dict_mutex[atoi(args[1])]);
    dict[atoi(args[1])] = value;
    pthread_mutex_unlock(&my_dict_mutex[atoi(args[1])]);

    return "Insertion Successful\n";
}

string handle_fetch(char *args[MAX_ARGS])
{
    int key;
    if (!(args[0] && args[1]))
        return "Invalid number of argument\n";

    if (!(0 <= atoi(args[1]) && atoi(args[1]) <= 100))
        return "Invalid key\n";

    key = atoi(args[1]);
    if (!dict[atoi(args[1])].length())
        return "Key does not exist\n";
    else
        return dict[atoi(args[1])] + "\n";
}

string handle_delete(char *args[MAX_ARGS])
{
    int key;
    if (!(args[0] && args[1]))
        return "Invalid number of argument\n";

    key = atoi(args[1]);
    if (!(0 <= atoi(args[1]) && atoi(args[1]) <= 100))
        return "Invalid key\n";

    if (!dict[atoi(args[1])].length())
        return "No such key exists\n";

    pthread_mutex_lock(&my_dict_mutex[atoi(args[1])]);
    dict[atoi(args[1])] = "";
    pthread_mutex_unlock(&my_dict_mutex[atoi(args[1])]);
    return "Deletion successful\n";
}

string handle_update(char *args[MAX_ARGS])
{
    int key;
    string value;
    if (!(args[0] && args[1] && args[2]))
        return "Invalid number of arguments\n";

    if (!(0 <= key && key <= 100))
        return "Invalid key\n";

    key = atoi(args[1]);
    if (!dict[key].length())
        return "Key does not exist\n";

    value = args[2];
    pthread_mutex_lock(&my_dict_mutex[atoi(args[1])]);
    dict[atoi(args[1])] = value;
    pthread_mutex_unlock(&my_dict_mutex[atoi(args[1])]);

    return value + "\n";
}

string handle_concat(char *args[MAX_ARGS])
{
    int key1, key2;
    string temp1, temp2, concat1, concat2;
    if (!(args[0] && args[1] && args[2]))
        return "Invalid number of argument\n";

    key1 = atoi(args[1]);
    if (!(0 <= key1 && key1 <= 100))
        return "Invalid key1\n";
    key2 = atoi(args[2]);
    if (!(0 <= key2 && key2 <= 100))
        return "Invalid key2\n";

    temp1 = dict[key1];
    temp2 = dict[key2];

    if (!(temp1.length() and temp2.length()))
        return "Concat failed as at least one of the keys does not exist\n";

    concat1 = temp1 + temp2;
    concat2 = temp2 + temp1;
    pthread_mutex_lock(&my_dict_mutex[key1]);
    pthread_mutex_lock(&my_dict_mutex[key2]);
    dict[key1] = concat1;
    dict[key2] = concat2;
    pthread_mutex_unlock(&my_dict_mutex[key2]);
    pthread_mutex_unlock(&my_dict_mutex[key1]);

    return dict[key2] + "\n";
}

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(1);
    }
    return;
}

string handle_commands(char *user_input)
{

    char **args = (char **)malloc(MAX_ARGS * sizeof(char *));

    args[0] = strtok(user_input, " \t");
    int i = 0;
    string response = "\n";
    while (args[i])
        args[++i] = strtok(NULL, " \t");

    if (strncmp(args[0], "insert", 7) == 0)
        response = take_input(args);
    else if (strncmp(args[0], "fetch", 6) == 0)
        response = handle_fetch(args);
    else if (strncmp(args[0], "delete", 6) == 0)
        response = handle_delete(args);
    else if (strncmp(args[0], "update", 6) == 0)
        response = handle_update(args);
    else if (strncmp(args[0], "concat", 6) == 0)
        response = handle_concat(args);
    return response;
}

void *handle_connection(void *ptr_client_socket, int request_index)
{
    // steup buffer
    int bytes;
    char user_input[BUFFER_SIZE];
    string response;
    bzero(user_input, BUFFER_SIZE);

    pid_t tid = gettid();
    int client_socket = *((int *)ptr_client_socket);

    // read data from client
    int check_code = read(client_socket, user_input, BUFFER_SIZE - 1);
    check(check_code, "Error: Cant read from sock\n");
    user_input[BUFFER_SIZE - 1] = '\0'; // null terminating it, just in case

    // test();
    response = handle_commands(user_input);
    char *c_response = strdup(response.c_str());

    // write ack to client
    check_code = write(client_socket, c_response, strlen(c_response));
    check(check_code, "Error: Can't Listen to client\n");

    cout << request_index << ":" << tid << ":" << c_response << flush;
    tid = gettid();
    // close connection
    close(client_socket);

    return NULL;
}

void *handle_thread(void *arg)
{
    int *client_socket = NULL;
    while (true)
    {
        // PROTECC THE QUEUE OPERATIONS
        pthread_mutex_lock(&my_mutex);
        if (q.empty())
        {
            pthread_cond_wait(&my_condition, &my_mutex);
        }
        pair<int, int *> data;
        if (!q.empty())
        {
            data = q.front(); //get the first element
            q.pop();          // YEET
        }
        pthread_mutex_unlock(&my_mutex);

        if (data.second)
            handle_connection(data.second, data.first);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int num_clients;
    if (argc != 2)
    {
        printf("Error: Please enter one param for number of client threads \n");
        exit(1);
    }
    num_clients = atoi(argv[1]);
    int i;
    for (i = 0; i < num_clients; i++)
    {
        pthread_t thread;
        pthread_create(&thread, NULL, handle_thread, NULL);
    }

    // setup data structures and shit
    struct sockaddr_in server_addr, client_addr;
    int sockfd;
    int client_socket;
    socklen_t client_len = sizeof(client_addr);

    // setup socket
    int check_code = (sockfd = socket(AF_INET, SOCK_STREAM, 0));
    check(check_code, "Error: cant open sock\n");
    // setup reuse address option for socket
    int enable = 1;
    check_code = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
    check(check_code, "Error: cant reuse address\n");
    // server addr struct setup
    bzero((char *)&server_addr, sizeof(server_addr)); // for resetting server_addr
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    // bind the socket fd with server address
    check_code = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    check(check_code, "Error in binding :0\n");
    check_code = listen(sockfd, BACKLOG);
    check(listen(sockfd, BACKLOG), "cant listen :/");
    i = 0;
    cout << "Server has started listening on the LISTEN PORT" << endl;
    while (true)
    {
        // accept connection request from client
        int *client_on_heap = (int *)malloc(sizeof(int));
        check_code = (client_socket = accept(sockfd, (struct sockaddr *)&client_addr, &client_len));
        check(check_code, "can't accept\n");
        *client_on_heap = client_socket;

        pthread_mutex_lock(&my_mutex);
        q.push(make_pair(i, client_on_heap));
        i++;
        pthread_cond_signal(&my_condition);
        pthread_mutex_unlock(&my_mutex);
    }

    // close server socket
    close(sockfd);

    return 0;
}

//###########FILE CHANGE ./main_folder/Keshav Bajaj_305797_assignsubmission_file_/2019115010/q3/client.cpp ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define PORT 1337
#define BUFFER_SIZE 256
#define MAX_REQUESTS 256
using namespace std;

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s", message);
        exit(1);
    }
    return;
}

void *handle_connect(void *input_ptr)
{
    int sockfd;
    string input = *((string *)input_ptr);
    char rbuff[BUFFER_SIZE];

    struct sockaddr_in server_addr;

    int check_code = (sockfd = socket(AF_INET, SOCK_STREAM, 0));
    check(check_code, "cant create sock\n");

    bzero((char *)&server_addr, sizeof(server_addr));

    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    check_code = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    check(check_code, "cant connect to server\n");
    send(sockfd, input.c_str(), strlen(input.c_str()), 0);

    check_code = recv(sockfd, rbuff, BUFFER_SIZE, 0) - 1;
    check(check_code, "can't recevie\n");
    fputs(rbuff, stdout);
    bzero((char *)&rbuff, sizeof(rbuff));

    close(sockfd);

    return NULL;
}

int main()
{
    string temp;
    int m;
    getline(cin, temp);
    m = stoi(temp);
    int last_time = 0, i = 0;
    vector<vector<string>> inputs(MAX_REQUESTS);
    for (i = 0; i < m; i++)
    {
        string temp;
        string time;
        string remaining;
        getline(cin, temp);
        time = temp.substr(0, temp.find(' '));
        remaining = temp.substr(temp.find(' ') + 1, temp.length() - 1);

        int time_int = stoi(time);
        inputs[time_int].push_back(remaining);
        last_time = max(last_time, stoi(time));
    }

    for (i = 0; i <= last_time; i++)
    {
        vector<pthread_t> thread_pool;
        thread_pool.resize(inputs[i].size());

        for (int j = 0; j < inputs[i].size(); j++)
            pthread_create(&thread_pool[j], NULL, handle_connect, &inputs[i][j]);
        sleep(1);
    }

    return 0;
}
