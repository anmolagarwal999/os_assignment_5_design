
//###########FILE CHANGE ./main_folder/Rudransh Singh_305844_assignsubmission_file_/q3/server.cpp ####################//

#include <bits/stdc++.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "client.hpp"
#include "server.hpp"
#define PORT 8080
#define MAX_REQUEST_LENGTH 1024
//make max and min macros
#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)
using namespace std;
queue <int> connector_fd_queue; // queue to store the fd of the connecting sockets
value dictionary[101];

pthread_mutex_t queue_lock; // lock for the queue
pthread_cond_t queue_cond; // condition variable for the queue
//size of the dictionary is 101 (i.e keys range from 0 to 99)
vector <string> parse(string str) // parses the string and stores it in a vector
{
    vector <string> result;
    string word = "";
    for (auto x : str) 
    {
        if (x == ' ')
        {
            result.push_back(word);
            word = "";
        }
        else {
            word = word + x;
        }
    }
    result.push_back(word);
    return result;
}
string convertToString(char* a, int size)
{
    int i;
    string s = "";
    for (i = 0; i < size; i++) {
        s = s + a[i];
    }
    return s;
}

void init_dict()
{
    for (int i = 0; i < 101; i++)
    {
        value v;
        v.s = "";
        pthread_mutex_init(&v.valuelock, NULL);
        dictionary[i] = v;
    }
}

//insert key-value pair in the dictionary along with buffer, check whether the key already exists or not
void insert_dict(int key, string value, string &buffer)
{
    pthread_mutex_lock(&dictionary[key].valuelock);
    if (dictionary[key].s == "")
    {
        dictionary[key].s = value;
        buffer=buffer+"Insertion successful\n";
    }
    else
    {
        buffer=buffer+"Key already exists\n";
    }
    pthread_mutex_unlock(&dictionary[key].valuelock);
}

//delete key-value pair in the dictionary along with buffer, check whether the key is empty of not
void delete_dict(int key, string &buffer)
{
    pthread_mutex_lock(&dictionary[key].valuelock);
    if (dictionary[key].s != "")
    {
        dictionary[key].s = "";
        buffer=buffer+"Deletion successful\n";
    }
    else
    {
        buffer=buffer+"No such key exists\n";
    }
    pthread_mutex_unlock(&dictionary[key].valuelock);
}

//update key-value pair in the dictionary along with buffer, check whether the key already exists or not
void update_dict(int key, string value, string &buffer)
{
    pthread_mutex_lock(&dictionary[key].valuelock);
    if (dictionary[key].s != "")
    {
        dictionary[key].s = value;
        buffer=buffer+value;
        buffer=buffer+"\n";
    }
    else
    {
        buffer=buffer+"Key does not exist\n";
    }
    pthread_mutex_unlock(&dictionary[key].valuelock);
}
//: Let values corresponding to the keys before execution of this command be {key1:
//value_1, key_2:value_2}. Then, the corresponding values after this command's execution should be {key1:
//value_1+value_2, key_2: value_2+value_1}
void concat_dict(int key1, int key2, string &buffer)
{
    int maxkey = max(key1, key2);
    int minkey = min(key1, key2);
    pthread_mutex_lock(&dictionary[maxkey].valuelock);
    pthread_mutex_lock(&dictionary[minkey].valuelock);
    if (dictionary[key1].s != "" && dictionary[key2].s != "")
    {
        //first make two temporary strings and store concatenated values in them
        string temp1 = dictionary[key1].s;
        string temp2 = dictionary[key2].s;
        dictionary[key1].s = temp1 + temp2;
        dictionary[key2].s = temp2 + temp1;
        buffer=buffer+dictionary[key2].s;
        buffer=buffer+"\n";
    }
    else
    {
        buffer=buffer+"Concat failed as at least one of the keys does not exist\n";
    }
    pthread_mutex_unlock(&dictionary[minkey].valuelock);
    pthread_mutex_unlock(&dictionary[maxkey].valuelock);
}
//fetch function, fetches value at key, shows "Key does not exist" otherwise
void fetch_dict(int key, string &buffer)
{
    pthread_mutex_lock(&dictionary[key].valuelock);
    if (dictionary[key].s != "")
    {
        buffer=buffer+dictionary[key].s;
        buffer=buffer+"\n";
    }
    else
    {
        buffer=buffer+"Key does not exist\n";
    }
    pthread_mutex_unlock(&dictionary[key].valuelock);
}
// The actual server function
void processoperation(int sockfd)
{
    char buffer[MAX_REQUEST_LENGTH];
    bzero(buffer, MAX_REQUEST_LENGTH);
    //read from client
    int read_size = read(sockfd, buffer, MAX_REQUEST_LENGTH);
    if (read_size == 0)
    {
        //client disconnected
        return;
    }
    vector <string> request_from_client;
    string request = convertToString(buffer, read_size);
    request_from_client = parse(request);
    string buffer_to_client = to_string(pthread_self());
    buffer_to_client=buffer_to_client+":";
    if (request_from_client[0] == "insert")
    {
        int key = stoi(request_from_client[1]);
        string value = request_from_client[2];
        insert_dict(key, value, buffer_to_client);
    }
    else if (request_from_client[0] == "delete")
    {
        int key = stoi(request_from_client[1]);
        delete_dict(key, buffer_to_client);
    }
    else if (request_from_client[0] == "update")
    {
        int key = stoi(request_from_client[1]);
        string value = request_from_client[2];
        update_dict(key, value, buffer_to_client);
    }
    else if (request_from_client[0] == "concat")
    {
        int key1 = stoi(request_from_client[1]);
        int key2 = stoi(request_from_client[2]);
        concat_dict(key1, key2, buffer_to_client);
    }
    else if (request_from_client[0] == "fetch")
    {
        int key = stoi(request_from_client[1]);
        fetch_dict(key, buffer_to_client);
    }
    else
    {
        buffer_to_client = "Invalid request\n";
    }
//write to socket
write(sockfd, buffer_to_client.c_str(), buffer_to_client.length());
sleep(2);

}
void searchforclient()
{
    int sockfd,connecterfd;
    sockaddr_in servaddr;
    //create socket and verify it's connection
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    //set servaddr attributes
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        exit(EXIT_FAILURE);
    }
    //bind socket to server
    if (bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    //listen for client
    if (listen(sockfd, 10) < 0)
    {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }
    sockaddr_in cliaddr;
    socklen_t clilen = sizeof(cliaddr);
    //accept client
    while(1)
    {
        if ((connecterfd = accept(sockfd, (struct sockaddr *)&cliaddr, &clilen)) < 0)
        {
            perror("accept failed");
            exit(EXIT_FAILURE);
        }
        //add the fd to the queue
        pthread_mutex_lock(&queue_lock);
        connector_fd_queue.push(connecterfd);
        pthread_cond_signal(&queue_cond);
        pthread_mutex_unlock(&queue_lock);
    }
}

void* worker_thread(void* arg)
{
    int connecterfd;
    while(1)
    {
        pthread_mutex_lock(&queue_lock);
        while(connector_fd_queue.empty())
        {
            pthread_cond_wait(&queue_cond, &queue_lock);
        }
        int sockfd = connector_fd_queue.front();
        connector_fd_queue.pop();
        pthread_mutex_unlock(&queue_lock);
        processoperation(sockfd);
        close(sockfd);
    }
    pthread_exit(NULL);
}

int main(int arg, char* argv[])
{
    // number of worker threads equal to argv[1]
    int num_threads = atoi(argv[1]);
    //create worker threads
    pthread_t worker_threads[num_threads];
    //error checking
    if (num_threads < 1)
    {
        cout << "Invalid number of threads" << endl;
        return 0;
    }
    for (int i = 0; i < num_threads; i++)
    {
        pthread_create(&worker_threads[i], NULL, worker_thread, NULL);
    }
    searchforclient();
    //wait for joining
    for (int i = 0; i < num_threads; i++)
    {
        pthread_join(worker_threads[i], NULL);
    }
    return 0;
}

//###########FILE CHANGE ./main_folder/Rudransh Singh_305844_assignsubmission_file_/q3/client.cpp ####################//

//making client side for socket programming
#include <bits/stdc++.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include "client.hpp"
using namespace std;
#define MAX_REQUEST_LENGTH 1024
#define PORT 8080


user_request* input_request;
vector <string> parse(string str) // parses the string and stores it in a vector
{
    vector <string> result;
    string word = "";
    for (auto x : str) 
    {
        if (x == ' ')
        {
            result.push_back(word);
            word = "";
        }
        else {
            word = word + x;
        }
    }
    result.push_back(word);
    return result;
}

void* client_thread_sim(void* arg)
{
    int* index= (int*)arg;
    sleep(input_request[*index].request_time);
    int sockfd;
    sockaddr_in servaddr;
    //create socket and verify it's connection
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    //set servaddr attributes
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        exit(EXIT_FAILURE);
    }
    //connect client to server
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("connection with server failed");
        exit(EXIT_FAILURE);
    }
    //send request to server
    string request = input_request[*index].request_string;
    char buffer[MAX_REQUEST_LENGTH];
    // write data to the server
    strcpy(buffer, request.c_str());
    int read_size = write(sockfd, buffer, strlen(buffer));
    if (read_size == -1)
    {
        perror("write failed");
        exit(EXIT_FAILURE);
    }
    bzero(buffer, MAX_REQUEST_LENGTH);
    //read from server
    read_size = read(sockfd, buffer, MAX_REQUEST_LENGTH);
    if (read_size == 0)
    {
        //client disconnected
        perror("read failed");
        exit(EXIT_FAILURE);
    }
    //print response from server
    cout << (*index) <<":"<< buffer;
    close(sockfd);
    pthread_exit(NULL);
}
int main()
{
    //first take input of number of user requests
    int num_user_requests;
    cin>>num_user_requests;
    // make num_user_requests threads
    pthread_t client_threads[num_user_requests];
    // make an array of user_request structs
    input_request=(user_request*)malloc(sizeof(user_request)*num_user_requests);
    for(int i=0; i < num_user_requests ;i++)
    {
       // first take input of the time of request
       cin>> input_request[i].request_time;
       // set the request_index to i
       input_request[i].request_index = i;
       // take input of the actual command
       string s;
       getline(cin,s);
       s=s.substr(1);
       input_request[i].request_string=s;
       input_request[i].request_command = parse(s);
       int* a=(int *) malloc(sizeof(int));
       *a=i;
       pthread_create(&client_threads[i],NULL,client_thread_sim,a);
    }
    for(int i=0; i < num_user_requests ;i++)
    {
        pthread_join(client_threads[i],NULL);
    }
}
