
//###########FILE CHANGE ./main_folder/Kushagra Kharbanda_305971_assignsubmission_file_/2020101002_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 10
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

unordered_map<int, string> dict;
queue <int> clients_fds;

// locks
pthread_mutex_t mutex_keys[101];
pthread_mutex_t mutex_queue = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;
pthread_mutex_t concat_lock = PTHREAD_MUTEX_INITIALIZER;

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }
    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################
    int received_num, sent_num;
    pthread_t thread_id = pthread_self();
    string self_id = to_string(thread_id);
    /* read message from client */
    int ret_val = 1;
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;
    // cout << "cmd is " << cmd << endl;
    string command[3];
    istringstream ss(cmd);
    int send_to_client ;
    string word; // for storing each word
    int q = 0;
    // Traverse through all words
    // while loop till we get 
    // strings to store in string word
    while (ss >> word) 
    {
        // print the read word
        command[q] = word;
        q++;
    }
    string to_send = self_id + ":";
    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        goto close_client_socket_ceremony;
    }

    if(command[0] == "insert")
    {
        int key = stoi(command[1]);
        string value = command[2];
        string output;
        pthread_mutex_lock(&mutex_keys[key]);
        if(dict.count(key) != 0)
        {
            output = "Key already exists";
        }
        else
        {
            dict[key] = value;
            output = "Insertion successful";
        }
        to_send += output;
        pthread_mutex_unlock(&mutex_keys[key]);
    }

    else if(command[0] == "delete")
    {
        int key = stoi(command[1]);
        string output;
        pthread_mutex_lock(&mutex_keys[key]);
        if(dict.count(key) == 0)
        {
            output = "No such key exists";
        }
        else
        {
            dict.erase(key);
            output = "Deletion successful";
        }
        to_send += output;
        pthread_mutex_unlock(&mutex_keys[key]);
    }
    if(command[0] == "update")
    {
        int key = stoi(command[1]);
        string value = command[2];
        string output;
        pthread_mutex_lock(&mutex_keys[key]);
        if(dict.count(key) == 0)
        {
            output = "Key does not exist";
        }
        else
        {
            dict[key] = value;
            output = value;
        }
        to_send += output;
        pthread_mutex_unlock(&mutex_keys[key]);
    }
    if(command[0] == "concat")
    {
        int key1 = stoi(command[1]);
        int key2 = stoi(command[2]);
        string output;
        pthread_mutex_lock(&concat_lock);
        pthread_mutex_lock(&mutex_keys[key1]);
        pthread_mutex_lock(&mutex_keys[key2]);
        pthread_mutex_unlock(&concat_lock);
        if(dict.count(key1) == 0 || dict.count(key2) == 0)
        {
            output = "Concat failed as at least one of the keys does not exist";
        }
        else
        {
            string str1 = dict[key1];
            string str2 = dict[key2];
            output = dict[key2] + dict[key1];
            dict[key1] = str1 + str2;
            dict[key2] = str2 + str1;
        }
        to_send += output;
        pthread_mutex_unlock(&mutex_keys[key1]);
        pthread_mutex_unlock(&mutex_keys[key2]);
    }
    if(command[0] == "fetch")
    {
        int key = stoi(command[1]);
        string output;
        pthread_mutex_lock(&mutex_keys[key]);
        if(dict.count(key) == 0)
        {
            output = "Key does not exist";
        }
        else
        {
            output = dict[key];
        }
        to_send += output;
        pthread_mutex_unlock(&mutex_keys[key]);
    }
    sleep(2); // artificial sleep
    send_to_client = send_string_on_socket(client_socket_fd, to_send);
    if (send_to_client == -1)   
    {
        perror("Error while writing to client. Seems socket has been closed");
        goto close_client_socket_ceremony;
    }

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.



close_client_socket_ceremony:
    close(client_socket_fd);
    //printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return;
}
void *worker_start(void *arg)
{
    while (1)
    {
        pthread_mutex_lock(&mutex_queue);
        while (clients_fds.empty())
        {
            pthread_cond_wait(&cond_var, &mutex_queue);
        }
        int client_socket_fd = clients_fds.front();
        clients_fds.pop();
        pthread_mutex_unlock(&mutex_queue);

        handle_connection(client_socket_fd);
    }
}

int main(int argc, char *argv[])
{
    // Initialising locks
    int max_workers = atoi(argv[1]);
    for(int i = 0; i < 101; i++)
    {
        pthread_mutex_init(&mutex_keys[i], NULL);
    }
    pthread_t worker_threads[max_workers];
    for(int i = 0; i < max_workers; i++)
    {
        pthread_create(worker_threads + i, NULL, worker_start, NULL);
    }
    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        //printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        //printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        pthread_mutex_lock(&mutex_queue);
        clients_fds.push(client_socket_fd);
        pthread_mutex_unlock(&mutex_queue);
        // while(!clients_fds.empty())
        // {
        //     pthread_cond_signal(&cond_var);
        // }
        pthread_cond_broadcast(&cond_var);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Kushagra Kharbanda_305971_assignsubmission_file_/2020101002_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

int indexes[1000];
vector<int> request_time;
vector<string> commands;
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void* begin_thread(void* arg)
{
    int idx = *(int *)arg;
    sleep(request_time[idx]);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, commands[idx]);
    // printf("command sent by %d\n", idx);
    string result;
    int num_bytes;
    tie(result, num_bytes) = read_string_from_socket(socket_fd, buff_sz);
    string final_output = to_string(idx) + ":" + result;
    cout << final_output << endl;
    return NULL;
}

int main(int argc, char *argv[])
{
    for(int i =0; i<1000; i++)
    {
        indexes[i] = i;
    }
    int m; cin >> m;
    request_time.resize(m); commands.resize(m);
    for(int i = 0; i< m; i++)
    {
        cin >> request_time[i];
        getline(cin, commands[i]);
    }
    // printf("input done\n");
    pthread_t clients[m];
    for(int i = 0; i < m; i++)
    {
        pthread_create(&clients[i], NULL, begin_thread, &indexes[i]);
    }
    // printf("threads created\n");
    // Joining threads
    for(int i = 0; i < m; i++)
    {
        pthread_join(clients[i], NULL);
    }
    return 0;
}