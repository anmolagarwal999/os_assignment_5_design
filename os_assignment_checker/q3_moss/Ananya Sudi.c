
//###########FILE CHANGE ./main_folder/Ananya Sudi_305909_assignsubmission_file_/2020101075/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include<queue>
#include<sstream>
#include<map>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////
map<int,string>list;
string dict_func(string command){
    string to_send_back;
    vector<string>temp_list;
    stringstream myStream(command); 
    string tempString;
    while (getline(myStream, tempString, ' ')) 
    { 
        temp_list.push_back(tempString);
 
    } 
    if(temp_list[0]=="insert"){
        if(temp_list.size()<3){
            to_send_back="number of args are not correct for insert";
        }
        else{
            int key=atoi(temp_list[1].c_str());
            string val=temp_list[2];
            int count=list.count(key);
            if(count>0){
                to_send_back="Key already exists";
            }
            else{
                list.insert({key,val});
                to_send_back="Insertion successful";
            }
        }
    }
    else if(temp_list[0]=="concat"){
        int k1=atoi(temp_list[1].c_str());
        int k2=atoi(temp_list[2].c_str());
        if(list.count(k1)>0 && list.count(k2)>0){
            auto it1=list.find(k1);
            auto it2=list.find(k2);

            string val1=it1->second+it2->second;
            string val2=it2->second+it1->second;
            it1->second=val1;
            it2->second=val2;
            to_send_back=it2->second;
        }
        else{
            to_send_back="Concat failed as at least one of the keys does not exist";
        }
    }
    else if(temp_list[0]=="delete"){
        int k=atoi(temp_list[1].c_str());
        if(list.count(k)>0){
            list.erase(k);
            to_send_back="Deletion successful";
        }
        else{
            to_send_back="key doesn't exist";
        }
    }
    else if(temp_list[0]=="update"){
        int k=atoi(temp_list[1].c_str());
        if(list.count(k)>0){
            auto it=list.find(k);
            it->second=temp_list[2];
            to_send_back=it->second;
        }
        else{
            to_send_back="Key does not exist";
        }    
    }
    else if(temp_list[0]=="fetch"){
        int k=atoi(temp_list[1].c_str());
        if(list.count(k)>0){
            auto it=list.find(k);
            to_send_back=it->second;
        }
        else{
            to_send_back="Key doesn't exist";
        }
    }
    else{
        to_send_back="invalid operation";
    }
    
    return to_send_back; 
}
pthread_mutex_t dict_lock=PTHREAD_MUTEX_INITIALIZER;

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################
    cout<<client_socket_fd<<"\n";
    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        pthread_mutex_lock(&dict_lock);

        string msg_to_send_back=dict_func(cmd);

        pthread_mutex_unlock(&dict_lock);

        // cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        // string msg_to_send_back = "Ack: " + cmd;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        break;
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}
queue<int>q;
pthread_cond_t cv= PTHREAD_COND_INITIALIZER;
pthread_mutex_t cnt_lock=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cnt_lock2=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cnt_lock3=PTHREAD_MUTEX_INITIALIZER;



void* worker_func(void* arg){
    pthread_mutex_lock(&cnt_lock);
    while(true){
    if(!q.empty()){
        pthread_mutex_lock(&cnt_lock2);
        int client_soc_fd=q.front();
        q.pop();
        handle_connection(client_soc_fd);
        pthread_mutex_unlock(&cnt_lock2);
    }
    else{
        pthread_cond_wait(&cv,&cnt_lock);
    }
    }
    pthread_mutex_unlock(&cnt_lock);
    return NULL;
}


int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    int num_workers=atoi(argv[1]);
    pthread_t wor_th[num_workers];
    for(int i=0;i<num_workers;i++){
        pthread_create(&wor_th[i],NULL,worker_func,NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        cout<<(struct sockaddr *)&serv_addr_obj<<"\n";
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        pthread_mutex_lock(&cnt_lock3);
        q.push(client_socket_fd);
        pthread_mutex_unlock(&cnt_lock3);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        pthread_cond_signal(&cv);
        // handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Ananya Sudi_305909_assignsubmission_file_/2020101075/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

// typedef struct details{
//     char* inp;
// }details;
// details det[100];


pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

// void *begin_process(void* input_string)
pthread_mutex_t cnt_lock=PTHREAD_MUTEX_INITIALIZER;

void *begin_process(void* input_string)
{
    int timer;
    string input=*((string*)input_string);
    timer=atoi(&input[0]);
    sleep(timer);
    string to_send=input.substr(2,input.size());
    // cout<<to_send<<"\n";

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    // cout << "Connection to server successful" << endl;
    pthread_mutex_lock(&cnt_lock);
    
    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout<<pthread_self()<<" "<<output_msg<<"\n";
    // cout << "Received: " << output_msg << endl;
    // cout << "====" << endl;
    // part;
    pthread_mutex_unlock(&cnt_lock);

    return NULL;
}
// char* inp[100];
vector<string>inp;
int main(int argc, char *argv[])
{
    int m; //num of user threads
    cin>>m;
    getchar();
    vector<pthread_t>th;
    inp.resize(m+1);
    th.resize(m+1);
    for(int i=0;i<m;i++){
        getline(cin,inp[i]);
    }
    for(int i=0;i<m;i++){
        int r=pthread_create(&th[i],NULL,begin_process,&inp[i]);
        if(r!=0){
            cout<<"err in creating th"<<"\n";
        }
    }
    for(int i=0;i<m;i++){
        int rc=pthread_join(th[i],NULL);
    }
    int i, j, k, t, n;
    // begin_process();
    return 0;
}

// void* reqhandler(void* args)
// {
//     int reqno=((int)args);
//     request curreq=reqarray[reqno];
//     int sleeptime=curreq->time;
//     if(sleeptime>1)
//     {
//         sleep(sleeptime-1);
//     }
//     sadin server;
//     socket_fd[reqno]=get_socket_fd(&server);
//     // cout<<"Connection to server successful" << endl;
//     // cout<<"Thread for request: "<<reqno<<"running"<<endl;
//     string to_send=getstring(curreq);
//     // cout<<"Sending to server: "<<to_send<<endl;
//     // cout<<"gonna try sending req\n";
//     sendtosock(socket_fd[reqno],to_send);
//     // cout<<"req sent\n";
//     int readbytes;
//     string outputstr;
//     tie(outputstr,readbytes)=readfromsocket(socket_fd[reqno],buff_sz);
//     pthread_mutex_lock(&writelock);
//     cout<<reqno<<":"<<pthread_self()<<":"<<outputstr<<endl;
//     pthread_mutex_unlock(&writelock);
//     return NULL;
// }