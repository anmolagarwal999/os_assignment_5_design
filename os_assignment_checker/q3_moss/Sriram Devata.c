
//###########FILE CHANGE ./main_folder/Sriram Devata_305951_assignsubmission_file_/2019113007/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
#include <semaphore.h>
#include <queue>
#include <sstream>
#include <thread>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;

sem_t client_reqs;
pthread_t *worker_threads;

pthread_mutex_t client_sockets_lock;
queue<int> client_sockets;

string full_dictionary[101];
pthread_mutex_t full_dictionary_lock[101];

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd, int worker_id)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    // printf("%d handling %d\n", worker_id, client_socket_fd);

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            break;
        }
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            break;
        }

        // vector words will have all the space-separated words in the string that the client sent
        std::vector<std::string> words;
        const char delim = ' ';
        // construct a stream from the string 
        std::stringstream ss(cmd); 
        std::string s; 
        while (std::getline(ss, s, delim)) { 
            words.push_back(s); 
        }

        string msg_to_send_back;

        if(words[0] == "insert"){
            int index = stoi(words[1]);
            pthread_mutex_lock(&full_dictionary_lock[index]);
            if(full_dictionary[index].empty()){
                full_dictionary[index] = words[2];
                msg_to_send_back = "Insertion successful";
            }
            else{
                msg_to_send_back = "Key already exists";
            }
            pthread_mutex_unlock(&full_dictionary_lock[index]);
        }
        else if(words[0] == "delete"){
            int index = stoi(words[1]);
            pthread_mutex_lock(&full_dictionary_lock[index]);
            if(!full_dictionary[index].empty()){
                full_dictionary[index].clear();
                msg_to_send_back = "Deletion successful";
            }
            else{
                msg_to_send_back = "No such key exists";
            }
            pthread_mutex_unlock(&full_dictionary_lock[index]);
        }
        else if(words[0] == "update"){
            int index = stoi(words[1]);
            pthread_mutex_lock(&full_dictionary_lock[index]);
            if(full_dictionary[index].empty()){
                msg_to_send_back = "Key does not exist";
            }
            else{
                full_dictionary[index] = words[2];
                msg_to_send_back = full_dictionary[index];
            }
            pthread_mutex_unlock(&full_dictionary_lock[index]);
        }
        else if(words[0] == "concat"){
            int index1 = stoi(words[1]);
            int index2 = stoi(words[2]);

            pthread_mutex_lock(&full_dictionary_lock[index1]);
            string string1 = full_dictionary[index1];
            pthread_mutex_unlock(&full_dictionary_lock[index1]);
            pthread_mutex_lock(&full_dictionary_lock[index2]);
            string string2 = full_dictionary[index2];
            pthread_mutex_unlock(&full_dictionary_lock[index2]);

            if(string1.empty() || string2.empty()) {
                msg_to_send_back = "Concat failed as at least one of the keys does not exist";
            }
            else{
                pthread_mutex_lock(&full_dictionary_lock[index1]);
                full_dictionary[index1] = string1+string2;
                pthread_mutex_unlock(&full_dictionary_lock[index1]);
                pthread_mutex_lock(&full_dictionary_lock[index2]);
                full_dictionary[index2] = string2+string1;
                pthread_mutex_unlock(&full_dictionary_lock[index2]);
                msg_to_send_back = string2+string1;
            }
        }
        else if(words[0] == "fetch"){
            int index = stoi(words[1]);
            pthread_mutex_lock(&full_dictionary_lock[index]);
            if(full_dictionary[index].empty()){
                msg_to_send_back = "Key does not exist";
            }
            else{
                msg_to_send_back = full_dictionary[index];
            }
            pthread_mutex_unlock(&full_dictionary_lock[index]);
        }
        else {
            msg_to_send_back = "Invalid Command";
        }

        // artificial sleep as mentioned in the requirements
        sleep(2);

        auto myid = std::this_thread::get_id();
        stringstream st;
        st << myid;
        msg_to_send_back = ":"+st.str()+":"+msg_to_send_back;

        cout << msg_to_send_back << endl;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            break;
        }
    }

    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *worker_function(void *args) {

    int worker_id = pthread_self();

    while(1){
        int client_socket_fd = -1;

        // wait until there's something in the client_sockets queue
        sem_wait(&client_reqs);

        pthread_mutex_lock(&client_sockets_lock);
        if(!client_sockets.empty()){
            client_socket_fd = client_sockets.front();
            client_sockets.pop();
        }
        pthread_mutex_unlock(&client_sockets_lock);

        if(client_socket_fd != -1){
            //printf("Worker %d started handling %d\n", worker_id, client_socket_fd);
            handle_connection(client_socket_fd, worker_id);
        }
        else{
            printf("Why is this -1 for %d\n", worker_id);
        }
    }

    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    int num_threads = atoi(argv[1]);
    printf("Running server with %d worker threads\n", num_threads);

    for(int i = 0; i < 101; i++){
        full_dictionary[i] = "";
        pthread_mutex_init(&full_dictionary_lock[i], NULL);
    }

    // there are currently no client requests
    sem_init(&client_reqs, 0, 0);
    // lock when popping from the queue
    pthread_mutex_init(&client_sockets_lock, NULL);

    worker_threads = (pthread_t *) malloc(sizeof(pthread_t) * num_threads);
    for(int i = 0; i < num_threads; i++){
        pthread_create(&worker_threads[i], NULL, worker_function, NULL);
    }

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        pthread_mutex_lock(&client_sockets_lock);
        client_sockets.push(client_socket_fd);
        pthread_mutex_unlock(&client_sockets_lock);
        sem_post(&client_reqs);
    }

    close(wel_socket_fd);

    free(worker_threads);

    return 0;
}
//###########FILE CHANGE ./main_folder/Sriram Devata_305951_assignsubmission_file_/2019113007/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

typedef struct user_request {
    int index;
    string given_string;
} ureq;

pthread_t *client_threads;
ureq *ureqs;

void *begin_process(void *args) {

    ureq p_ureq = *(ureq *)args;

    // sleep for the time given by the first element
    int sleep_time = stoi(p_ureq.given_string.substr(0, p_ureq.given_string.find(' ')));
    sleep(sleep_time);

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    // cout << "Socket id for " << p_ureq.index << " is " << socket_fd << endl;

    // everything after the first space is the actual query to send to the server
    string to_send = p_ureq.given_string.substr(p_ureq.given_string.find(' ')+1);

    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << p_ureq.index << output_msg << endl;

    close(socket_fd);

    return NULL;
}

int main(int argc, char *argv[])
{

    string input;
    int m;
    getline(cin, input);
    m = stoi(input);

    client_threads = (pthread_t *) malloc(sizeof(pthread_t) * m);
    ureqs = (ureq *) malloc(sizeof(ureq) * m);

    for(int i = 0; i < m; i++){
        getline(cin, input);
        // given_string has the input string
        ureqs[i].given_string = input;
        // index stores the 0-indexed query number
        ureqs[i].index = i;
        pthread_create(&client_threads[i], NULL, begin_process, &ureqs[i]);
    }

    for(int i = 0; i < m; i++){
        pthread_join(client_threads[i], NULL);
    }

    return 0;
}