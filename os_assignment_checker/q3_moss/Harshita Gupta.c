
//###########FILE CHANGE ./main_folder/Harshita Gupta_305977_assignsubmission_file_/2020101078_assignment_5/q3/server.h ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include <queue>
using namespace std;

#define MAX 50

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;

const LL buff_sz = 1048576;

typedef struct worker
{
    int id;
    pthread_t worker_thread_id;
    //pthread_mutex_t w_mutex;

} worker;

worker *worker_list;
pthread_cond_t service_client;
//sem_t service_client_sem;

pthread_mutex_t queue_lock;
queue<int> client_q;

int n;

typedef struct dict_entry
{
    int key;
    char value[MAX];
    pthread_mutex_t dict_entry_mutex;
    int is_present;
} dict_entry;

dict_entry dict[MAX];

//###########FILE CHANGE ./main_folder/Harshita Gupta_305977_assignsubmission_file_/2020101078_assignment_5/q3/my_client.cpp ####################//

#include "client.h"

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //descriptor that refers to that endpoint.  The file descriptor
    //returned by a successful call will be the lowest-numbered file
    //descriptor not currently open for the process.

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        return -1;
    }

    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        return -1;
    }
    return socket_fd;
}

void *client_thread(void *arg)
{
    int cid = *(int *)arg;
    // cout << "Thread started for" << req_list[cid].client_thread_id << endl;
    struct sockaddr_in server_obj;
    req_list[cid].client_socket_fd = get_socket_fd(&server_obj); //create a socket and connect it to the server and return the fd

    sleep(req_list[cid].t);
    //write your request to the socket - to be sent to the server
    //accquire the lock so that only the client can access the socket for now

    pthread_mutex_lock(&req_list[cid].client_mutex);
    int rc = write(req_list[cid].client_socket_fd, req_list[cid].command, strlen(req_list[cid].command));
    if (rc < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        pthread_mutex_unlock(&req_list[cid].client_mutex);
        return NULL;
    }
    pthread_mutex_unlock(&req_list[cid].client_mutex);

    //read the server's response from the socket
    // cout << "Sent command to the server successfully " << req_list[cid].client_thread_id << endl;

    pthread_mutex_lock(&req_list[cid].client_mutex);
    char buf[MAX];
    rc = read(req_list[cid].client_socket_fd, buf, MAX); //read returns the number of bytes read;
    if (rc < 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        pthread_mutex_unlock(&req_list[cid].client_mutex);
        return NULL;
    }
    buf[rc] = '\0';

    pthread_mutex_unlock(&req_list[cid].client_mutex);
    // print the result on the terminal - one client at a time

    pthread_mutex_lock(&output);
    cout << req_list[cid].id << " : " << buf << endl;
    pthread_mutex_unlock(&output);
    return NULL;
}

int main(int argc, char *argv[])
{
    int m; //total number of user requests made throughout the simulation
    scanf("%d", &m);

    req_list = (client_req *)malloc(m * sizeof(client_req));

    int rc = pthread_mutex_init(&output, NULL); //initialise the lock
    assert(rc == 0);

    for (int i = 0; i < m; i++)
    {
        //get input
        req_list[i].id = i;
        scanf("%d ", &req_list[i].t);
        cin.getline(req_list[i].command, CMAX);
        rc = pthread_mutex_init(&req_list[i].client_mutex, NULL); //initialise the lock
        assert(rc == 0);
    }

    for (int i = 0; i < m; i++)
    {
        pthread_create(&req_list[i].client_thread_id, NULL, client_thread, (void *)&req_list[i].id);
    }

    for (int i = 0; i < m; i++)
    {
        pthread_join(req_list[i].client_thread_id, NULL);
    }

    //begin_process();
    return 1; //successful simulation
}
//###########FILE CHANGE ./main_folder/Harshita Gupta_305977_assignsubmission_file_/2020101078_assignment_5/q3/client.h ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

using namespace std;

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

#define SERVER_PORT 8001
const LL buff_sz = 1048576;

#define MAX 50
#define CMAX 100

typedef struct client_req
{
    int id;
    int t; //time in sec after which the request to connect to the server is made
    char command[CMAX];
    pthread_t client_thread_id;
    pthread_mutex_t client_mutex;
    // a mutex accquired whenever we are accessing/modifying
    //data members of this stucture that can be changed by different threads simultaneously
    int client_socket_fd;
    int req_type;
    //0 - insert
    //1 - delete
    //2 - update
    //3 - concat
    //4 - fetch
    //-1 - invalid

} client_req;

client_req *req_list;
pthread_mutex_t output; //lock accquired by the client thread who wishes to print on the terminal
//###########FILE CHANGE ./main_folder/Harshita Gupta_305977_assignsubmission_file_/2020101078_assignment_5/q3/my_server.cpp ####################//

#include "server.h"

int send_string_on_socket(int fd, const string &s, int worker_thread_id)
{
    // debug(s.length());
    string str = to_string(worker_thread_id);
    str = str + " : ";
    str = str + s;

    int bytes_sent = write(fd, str.c_str(), str.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void printQueue(queue<int> q)
{
    //printing content of queue
    while (!q.empty())
    {
        cout << " " << q.front();
        q.pop();
    }
    cout << endl;
}

int get_arguments(char command[], char arguments[4][MAX])
{
    int itr = 0;
    int i = 0;

    while (command[i] != '\0')
    {
        int j = 0;

        while (command[i] != '\0' && command[i] != ' ')
        {
            arguments[itr][j++] = command[i++];
        }

        arguments[itr][j] = '\0';

        while (command[i] == ' ')
        {
            i++;
        }

        if (command[i] == '\0')
            break;

        itr++;
    }

    // for (int i = 0; i <= itr; i++)
    //     cout << "-" << arguments[i] << "-" << endl;

    itr++;
    return itr;
}

void handle_command(char *command, int client_socket_fd)
{
    int wid = gettid();
    char arguments[4][MAX];
    int num_args = get_arguments(command, arguments);

    if (strcmp(arguments[0], "insert") == 0)
    {
        if (num_args != 3)
        {
            cout << "Invalid Command" << endl;
            return;
        }

        int key = atoi(arguments[1]);
        char value[MAX];
        strcpy(value, arguments[2]);
        pthread_mutex_lock(&dict[key].dict_entry_mutex);

        if (dict[key].is_present == 1)
        {
            cout << "Key already exists" << endl;
            send_string_on_socket(client_socket_fd, "Key already exists", wid);
        }
        else
        {
            //insert this new key
            dict[key].is_present = 1;
            dict[key].key = key;
            strcpy(dict[key].value, value);
            cout << "Insertion successful" << endl;
            send_string_on_socket(client_socket_fd, "Insertion successful", wid);
        }
        pthread_mutex_unlock(&dict[key].dict_entry_mutex);
    }
    else if (strcmp(arguments[0], "delete") == 0)
    {
        if (num_args != 2)
        {
            cout << "Invalid Command" << endl;
            return;
        }

        int key = atoi(arguments[1]);

        pthread_mutex_lock(&dict[key].dict_entry_mutex);

        if (dict[key].is_present == 0)
        {
            cout << "No such key exists" << endl;
            send_string_on_socket(client_socket_fd, "No such key exists", wid);
        }
        else
        {
            dict[key].is_present = 0;
            strcpy(dict[key].value, "\0");
            cout << "Deletion successful" << endl;
            send_string_on_socket(client_socket_fd, "Deletion successful", wid);
        }

        pthread_mutex_unlock(&dict[key].dict_entry_mutex);
    }
    else if (strcmp(arguments[0], "update") == 0)
    {
        if (num_args != 3)
        {
            cout << "Invalid Command" << endl;
            return;
        }

        int key = atoi(arguments[1]);
        char value[MAX];
        strcpy(value, arguments[2]);

        pthread_mutex_lock(&dict[key].dict_entry_mutex);
        if (dict[key].is_present == 0)
        {
            cout << "Updation successful" << endl;
            send_string_on_socket(client_socket_fd, "Updation successful", wid);
        }
        else
        {
            dict[key].key = key;
            strcpy(dict[key].value, value);
            cout << "No such key exists" << endl;
            send_string_on_socket(client_socket_fd, "No such key exists", wid);
        }
        pthread_mutex_unlock(&dict[key].dict_entry_mutex);
    }
    else if (strcmp(arguments[0], "concat") == 0)
    {
        if (num_args != 3)
        {
            cout << "Invalid Command" << endl;
            return;
        }

        int key1 = atoi(arguments[1]);
        int key2 = atoi(arguments[2]);

        pthread_mutex_lock(&dict[key1].dict_entry_mutex);
        pthread_mutex_lock(&dict[key2].dict_entry_mutex);

        if (dict[key2].is_present == 0 || dict[key1].is_present == 0)
        {
            cout << "Concat failed as at least one of the keys does not exist" << endl;
            send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist", wid);
        }
        else
        {
            char temp[MAX];
            strcpy(temp, "");
            strcat(temp, dict[key1].value);
            strcat(dict[key1].value, dict[key2].value);
            strcat(dict[key2].value, temp);

            cout << "Concatenation successful" << endl;
            send_string_on_socket(client_socket_fd, dict[key2].value, wid);
        }

        pthread_mutex_unlock(&dict[key2].dict_entry_mutex);
        pthread_mutex_unlock(&dict[key1].dict_entry_mutex);
    }
    else if (strcmp(arguments[0], "fetch") == 0)
    {
        if (num_args != 2)
        {
            cout << "Invalid Command" << endl;
            return;
        }

        int key = atoi(arguments[1]);

        pthread_mutex_lock(&dict[key].dict_entry_mutex);
        if (dict[key].is_present == 0)
        {
            cout << "Key does not exist" << endl;
            send_string_on_socket(client_socket_fd, "Key does not exist", wid);
        }
        else
        {
            cout << "Fetch successful" << endl;
            send_string_on_socket(client_socket_fd, dict[key].value, wid);
        }
        pthread_mutex_unlock(&dict[key].dict_entry_mutex);
    }
    else
    {
        cout << "Invalid Command" << endl;
        return;
    }
}

void handle_connection(int client_socket_fd)
{
    char command[MAX];
    int cmd_size = MAX;
    int bytes_read = read(client_socket_fd, &command[0], cmd_size - 1);

    if (bytes_read <= 0)
    {
        cerr << "Failed to read data from socket. \n";
        printf("Server could not read msg sent from client\n");
        goto close_client_socket_ceremony;
    }
    command[bytes_read] = '\0';

    handle_command(command, client_socket_fd);

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

void *worker_thread(void *arg)
{
    int wid = *(int *)arg;
    while (1)
    {
        pthread_mutex_lock(&queue_lock);

        //sem_wait(&service_client_sem);

        while (client_q.empty())
        {
            pthread_cond_wait(&service_client, &queue_lock);
        }

        int client_sock_fd = client_q.front();

        client_q.pop();

        // cout << "Worker thread " << worker_list[wid].worker_thread_id << "assigned for " << client_sock_fd;
        pthread_mutex_unlock(&queue_lock);

        handle_connection(client_sock_fd);
    }
}

int create_server_socket()
{
    // the server requires 2 sockets
    // one to listen to the client requests - wel_socket_fd
    // one to actually communicate with the clients - client_socket_fd
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0); // initialisation
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        return -1;
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj)); //initialise the structures - no garbage values
    port_number = PORT_ARG;                               // port to which the server listen to for incoming client requests
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */
    // bind the socket with the structure

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        return -1;
    }

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");

        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        // system calledits the structure client_addr_obj

        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            return -1;
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&queue_lock);
        client_q.push(client_socket_fd);
        pthread_mutex_unlock(&queue_lock);

        pthread_cond_signal(&service_client);
        //sem_post(&service_client_sem);

        //printQueue(client_q);
    }
    close(wel_socket_fd);
    return 1;
}

int main(int argc, char *argv[])
{
    n = atoi(argv[1]);

    worker_list = (worker *)malloc(n * sizeof(worker));

    pthread_mutex_init(&queue_lock, NULL);

    pthread_cond_init(&service_client, NULL);
    //sem_init(&service_client_sem, 0, 0); //initialise to 0 as 0 client requests

    for (int i = 0; i < MAX; i++)
    {
        dict[i].key = i;
        pthread_mutex_init(&dict[i].dict_entry_mutex, NULL);
    }

    for (int i = 0; i < n; i++)
    {
        pthread_create(&worker_list[i].worker_thread_id, NULL, worker_thread, (void *)&worker_list[i].id);
    }

    int rc = create_server_socket();

    return 1;
}