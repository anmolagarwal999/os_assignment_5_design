
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/server.h ####################//

#ifndef __SERVER__H__
#define __SERVER__H__

#include "que.h"
#include <stdio.h>
#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <semaphore.h>

#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;

#define MAX_CLIENTS 500
#define PORT_ARG 8001
#define  MAX_LENGTH 512
#define MAX_THREAD_POOL_SIZE 256

pthread_t thread_pool[MAX_THREAD_POOL_SIZE]; 

typedef struct ret
{
    char str[MAX_LENGTH];
}output;

typedef struct wrap
{
    int id;
    long long time;
    char command[10];
    int key;
    int key2;
    char val[MAX_LENGTH];
}input;

string dict[102];
pthread_mutex_t dict_lock[102];
pthread_mutex_t workerlock[256];
pthread_mutex_t mainlock;
pthread_cond_t que_cond;
pthread_mutex_t que_lock;

#endif  //!__SERVER__H__

//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/client.h ####################//

#ifndef __CLIENT__H__
#define __CLIENT__H__

#include <stdio.h>
#include <time.h>
#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <chrono>
using namespace std;
using namespace std::chrono;


#define debug(x) cout << #x << " : " << x << endl;

#define SERVER_PORT 8001
#define  MAX_LENGTH 512

clock_t before;
typedef struct wrap
{
    int id;
    long long time;
    char command[10];
    int key;
    int key2;
    char val[MAX_LENGTH];
}input;

typedef struct ret
{
    char str[MAX_LENGTH];
}output;

pthread_mutex_t userlock[256];
pthread_mutex_t mainlock;

#endif  //!__CLIENT__H__
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/que.h ####################//

#ifndef __QUE__H__
#define __QUE__H__

struct node {
    struct node* next;
    int* client_socket;
};

typedef struct node node_t;

void push(int* client_socket);
int* pop();

#endif  //!__QUE__H__
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/server.cpp ####################//

#include "server.h"

void* handle_connection(void* args)
{
    pthread_mutex_lock(&mainlock);
    int socket_fd = *((int*)args);
    input in;
    recv(socket_fd,&in,sizeof(in),0);
    output out;
    pthread_mutex_lock(dict_lock+in.key);
    string str;
    
    str = to_string(in.id)+":"+to_string(pthread_self())+":";
    if (strcmp(in.command,"insert")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {    
            dict[in.key] = "";
            int j = 0;
            while(in.val[j]!='\0')
            {
                dict[in.key] += in.val[j];
                j++;
            }
            str += "Insertion successful";

        }
        else
        {
            str += "Key already exists";
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"delete")==0)
    {

        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "No such key exists";
        }
        else
        {
            dict[in.key] = "-1";
            str += "Deletion successful";
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"update")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "Key does not exist";
        }
        else
        {
            dict[in.key] = "";
            int j = 0;
            while(in.val[j]!='\0')
            {
                dict[in.key] += in.val[j];
                j++;
            }
            str += in.val;
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"concat")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0 || dict[in.key2]=="-1")
        {
            str += "Concat failed as at least one of the keys does not exist";
        }
        else
        {
            string temp = dict[in.key];
            dict[in.key] += dict[in.key2];
            dict[in.key2] += temp;   
            str += dict[in.key2];
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"fetch")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "Key does not exist";
        }
        else
        {
            str += dict[in.key];
        }        
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else
    {
        str = "Invalid command";
    }
    pthread_mutex_unlock(dict_lock+in.key);
    pthread_mutex_unlock(&mainlock);
    // sleep(2);
    return NULL;
}

void* thread_function(void* args)
{
    while (1)
    {
        int *client;
        pthread_mutex_lock(&que_lock);
        if ((client=pop())==NULL)
        {
            pthread_cond_wait(&que_cond,&que_lock);
            client = pop();
        }
        pthread_mutex_unlock(&que_lock);
        if (client!=NULL)
        {
            handle_connection(client);
        }
    }
    return NULL;
}

int main(int argc, char* argv[])
{
    pthread_mutex_init(&mainlock,NULL);
    que_cond = PTHREAD_COND_INITIALIZER;
    if (argc!=2)
    {
        fprintf(stderr,"Invalid number of arguments\n");
        exit(1);
    }

    pthread_mutex_init(&que_lock,NULL);
    for (int i = 0; i < 101; i++)
    {
        dict[i] = "-1";
        pthread_mutex_init(dict_lock+i,NULL);
    }
    

    int num_workers = atoi(argv[1]);
    for(int i=0;i<num_workers;i++)
    {
        pthread_mutex_init(workerlock+i,NULL);
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    int wel_socket_fd, port_number;
    socklen_t clilen;
    
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); 

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(wel_socket_fd, MAX_CLIENTS);
    std::cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while(1)
    {
        int client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR on accept");
            exit(-1);
        }
        pthread_mutex_lock(&que_lock);
        push(&client_socket_fd);
            
        pthread_cond_signal(&que_cond);
        pthread_mutex_unlock(&que_lock);
    }
    
}
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/que.c ####################//

#include "que.h"
#include <stdlib.h>

node_t* head = NULL;
node_t* tail = NULL;

void push(int* client_socket)
{
    node_t* new_node = (node_t*)malloc(sizeof(node_t));
    new_node->client_socket = client_socket;
    new_node->next = NULL;
    if (tail == NULL)
    {
        head = new_node;
        tail = new_node;
    }
    else
    {
        tail->next = new_node;
        tail = new_node;
    }
}

int* pop()
{
    if (head == NULL)
    {
        return NULL;
    }
    else
    {
        int* result = head->client_socket;
        node_t* temp = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
        }
        free(temp);
        return result;
    }
}
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q3/client.cpp ####################//

#include "client.h"

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
    // pthread_mutex_lock(&mainlock);
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    // pthread_mutex_unlock(&mainlock);
    return socket_fd;
}

void* handleuser(void* args)
{
    input *in = (input*)args;
    struct sockaddr_in server_obj;
    // pthread_mutex_lock(userlock+in->id);
    pthread_mutex_lock(&mainlock); 
    int socket_fd = get_socket_fd(&server_obj);
    pthread_mutex_unlock(&mainlock);

    // printf("%s\n",in->command);
    pthread_mutex_lock(&mainlock); 
    send(socket_fd,in,sizeof(input),0);
    // pthread_mutex_unlock(&mainlock);

    output out;
    // pthread_mutex_lock(&mainlock); 
    recv(socket_fd,&out,sizeof(output),0);
    pthread_mutex_unlock(&mainlock);

    printf("%s\n",out.str);
    // pthread_mutex_unlock(&mainlock);
    // pthread_mutex_unlock(userlock+in->id);
    return NULL;
}

int main(int argc, char *argv[])
{    
    auto start = high_resolution_clock::now();
    pthread_mutex_init(&mainlock,NULL);
    int m;
    scanf("%d",&m);
    pthread_t users[m+2];
    for (int i = 0; i < m; i++)
    {
        pthread_mutex_init(userlock+i,NULL);
    }
    input args[m];
    for (int i = 0; i < m; i++)
    {
        args[i].id = i;
        scanf("%lld%s",&args[i].time,args[i].command);
        // printf("%d %s\n",args[i].time,args[i].command);
        if(strcmp(args[i].command,"insert")==0)
        {
            scanf("%d%s",&args[i].key,args[i].val);
        }
        else if(strcmp(args[i].command,"delete")==0)
        {
            scanf("%d",&args[i].key);
        }
        else if(strcmp(args[i].command,"update")==0)
        {
            scanf("%d%s",&args[i].key,args[i].val);
        }
        else if(strcmp(args[i].command,"concat")==0)
        {
            scanf("%d%d",&args[i].key,&args[i].key2);
        }
        else if(strcmp(args[i].command,"fetch")==0)
        {
            scanf("%d",&args[i].key);
        }
        else
        {
            cout << "Invalid command" << endl;
        }
        // if (i> 0 and args[i].time==args[i].time)
        // {
        //     pthread_create(users+i,NULL,handleuser,(void*)&args[i]);
        //     continue;
        // }
        
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<seconds>(stop - start);
        // if (duration.count() < args[i].time)
        // {
        //     usleep(args[i].time - duration.count());
        // }        
        // printf("%lld %ld\n",args[i].time,duration.count());
        if (args[i].time-duration.count()>0)
        {
            sleep(args[i].time-duration.count());
        }
        else
        {
            usleep(50);
        }
        
        pthread_create(users+i,NULL,handleuser,args+i);
        // sleep(1);
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(users[i],NULL);
    }
    
    return 0;
}