
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Server/server_functions.cpp ####################//

#include "server.h"

// Function to receive command from client
std::pair<std::string, int> read_from_socket(int sockfd)
{
    std::string command;
    command.resize(MAX_BUFFER_SIZE);

    int bytes_received = read(sockfd, &command[0], MAX_BUFFER_SIZE - 1);
    if (bytes_received <= 0)
        std::cerr << "Error reading from socket"
                  << "\n";

    command[bytes_received] = '\0';
    command.resize(bytes_received);
    return std::make_pair(command, bytes_received);
}

// Function to return a response to the client
int send_to_socket(int sockfd, std::string response)
{
    int bytes_sent = write(sockfd, response.c_str(), response.size());
    if (bytes_sent < 0)
        std::cerr << "Error writing to socket"
                  << "\n";

    return bytes_sent;
}

// Function to parse the command from the client
std::vector<std::string> command_parser(std::string command)
{
    std::vector<std::string> tokens;
    std::string delimiter = " ";
    size_t pos = 0, prev = 0;
    std::string token;

    // Parsing the string to get the command and the arguments
    while ((pos = command.find(delimiter, pos)) != std::string::npos)
    {
        token = command.substr(prev, pos - prev);
        tokens.push_back(token);
        prev = ++pos;
    }

    tokens.push_back(command.substr(prev, pos));
    return tokens;
}

// Function to execute the parsed command
std::string execute_command(std::vector<std::string> tokens)
{
    std::string response;

    // Insert key-value pair
    if (tokens[0] == "insert")
    {
        // Checking if the key exists
        if (!key_exists[std::stoi(tokens[1])])
        {
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[1])]);
            dictionary[std::stoi(tokens[1])] = tokens[2];
            key_exists[std::stoi(tokens[1])] = true;
            response = std::to_string(pthread_self()) + ":Insertion successful";
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[1])]);
        }
        else
            response = std::to_string(pthread_self()) + ":Key already exists";
    }
    // Delete key-value pair
    else if (tokens[0] == "delete")
    {
        // Checking if the key exists
        if (key_exists[std::stoi(tokens[1])])
        {
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[1])]);
            dictionary[std::stoi(tokens[1])] = "";
            key_exists[std::stoi(tokens[1])] = false;
            response = std::to_string(pthread_self()) + ":Deletion successful";
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[1])]);
        }
        else
            response = std::to_string(pthread_self()) + ":No such key exists";
    }
    // Fetch value corresponding to key
    else if (tokens[0] == "fetch")
    {
        // Checking if the key exists
        if (key_exists[std::stoi(tokens[1])])
        {
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[1])]);
            response = std::to_string(pthread_self()) + ":" + dictionary[std::stoi(tokens[1])] + "\n";
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[1])]);
        }
        else
            response = std::to_string(pthread_self()) + ":Key does not exist";
    }
    // Concatenate key-value pairs
    else if (tokens[0] == "concat")
    {
        if (key_exists[std::stoi(tokens[1])] && key_exists[std::stoi(tokens[2])])
        {
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[1])]);
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[2])]);
            std::string temp_1 = dictionary[std::stoi(tokens[1])];
            std::string temp_2 = dictionary[std::stoi(tokens[2])];
            dictionary[std::stoi(tokens[1])] = temp_1 + temp_2;
            dictionary[std::stoi(tokens[2])] = temp_2 + temp_1;
            response = std::to_string(pthread_self()) + ":" + dictionary[std::stoi(tokens[2])];
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[2])]);
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[1])]);
        }
        else
            response = std::to_string(pthread_self()) + ":Concat failed as at least one of the keys does not exist";
    }
    // Update key-value pair
    else if (tokens[0] == "update")
    {
        // Checking if the key exists
        if (key_exists[std::stoi(tokens[1])])
        {
            pthread_mutex_lock(&key_mutexes[std::stoi(tokens[1])]);
            dictionary[std::stoi(tokens[1])] = tokens[2];
            response = std::to_string(pthread_self()) + ":" + dictionary[std::stoi(tokens[1])];
            pthread_mutex_unlock(&key_mutexes[std::stoi(tokens[1])]);
        }
        else
            response = std::to_string(pthread_self()) + ":Key does not exist";
    }
    else
        response = std::to_string(pthread_self()) + ":Invalid command";

    return response;
}

// The thread that reads the command from the client socket and parses it
void *handle_client(void *arg)
{
    // One iteration of the loop services one client
    while (true)
    {
        // Getting the latest client socket ID from the queue and popping it
        sem_wait(&queue_sem);
        pthread_mutex_lock(&queue_mutex);
        int client_socket_fd = client_fd_queue.front();
        client_fd_queue.pop();
        pthread_mutex_unlock(&queue_mutex);

        int received_bytes = 0, sent_bytes = 0;
        std::string command;
        std::tie(command, received_bytes) = read_from_socket(client_socket_fd);

        if (received_bytes <= 0)
        {
            std::cout << "Server could not read message from the client.\n";
            break;
        }

        // Parse the command
        std::vector<std::string> tokens = command_parser(command);
        std::string response = execute_command(tokens);

        sleep(2);

        sent_bytes = send_to_socket(client_socket_fd, response);
        if (sent_bytes <= 0)
        {
            std::cout << "Server could not send message to the client (socket may have been closed).\n";
            break;
        }
    }

    pthread_exit(NULL);
}

// Function to create a pool of worker threads to serve client requests
void create_server_threads(int n)
{
    for (int i = 0; i < n; i++)
    {
        pthread_t thread_id;
        pthread_create(&thread_id, NULL, handle_client, NULL);
        client_threads[i] = thread_id;
    }
}

// The initialisation function
void init_server(int n)
{
    thread_num = n;
    client_threads = new pthread_t[n];
    sem_init(&queue_sem, 0, 0);
    pthread_mutex_init(&queue_mutex, NULL);
}

// The cleanup function
void cleanup_server()
{
    for (int i = 0; i < 101; i++)
        pthread_mutex_destroy(&key_mutexes[i]);
    sem_destroy(&queue_sem);
    pthread_mutex_destroy(&queue_mutex);
    for (int i = 0; i < thread_num; i++)
        pthread_join(client_threads[i], NULL);
    delete[] client_threads;
}
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Server/server.h ####################//

#ifndef __SERVER_H__
#define __SERVER_H__

// Header files
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <vector>
#include <queue>
#include <map>
#include <pthread.h>
#include <semaphore.h>

// Colours
#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define RESET "\x1B[0m"

// Constants
#define MAX_BUFFER_SIZE 1024

// Global entities
extern int thread_num;
extern pthread_t *client_threads;
extern std::queue<int> client_fd_queue;
extern sem_t queue_sem;
extern pthread_mutex_t queue_mutex;

// The dictionary that stores data
extern std::vector<std::string> dictionary;
extern std::vector<pthread_mutex_t> key_mutexes;
extern std::vector<bool> key_exists;

// Function declarations
std::pair<std::string, int> read_from_socket(int sockfd);
int send_to_socket(int sockfd, std::string response);
std::vector<std::string> command_parser(std::string command);
std::string execute_command(std::vector<std::string> tokens);
void *handle_client(void *arg);
void create_server_threads(int n);
void init_server(int n);

#endif
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Server/server.cpp ####################//

#include "server.h"

// Global entities
int *client_server_fds = NULL;
int thread_num;
pthread_t *client_threads = NULL;
std::queue<int> client_fd_queue;
sem_t queue_sem;
pthread_mutex_t queue_mutex;

// The dictionary that stores data
std::vector<std::string> dictionary(101, "");
std::vector<pthread_mutex_t> key_mutexes(101, PTHREAD_MUTEX_INITIALIZER);
std::vector<bool> key_exists(101, false);

int main(int argc, char *argv[])
{
    // Two arguments must be specified
    if (argc != 2)
    {
        std::cout << "Incorrect number of arguments specified. Please try again.\n";
        return 1;
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t client_length;
    struct sockaddr_in server_address_obj, client_address_obj;

    // Intialise the server entities
    init_server(atoi(argv[1]));
    create_server_threads(atoi(argv[1]));

    // Creating server socket for arbitrary clients to make initial contact
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        printf("Error creating welcoming socket\n");
        return 1;
    }

    bzero((char *)&server_address_obj, sizeof(server_address_obj));

    port_number = 8001;
    // Specifying IPv4
    server_address_obj.sin_family = AF_INET;
    // Binding the port to all available interfaces
    server_address_obj.sin_addr.s_addr = INADDR_ANY;
    // Translates host byte order to network byte order
    server_address_obj.sin_port = htons(port_number);

    // Binding the socket to the port
    if (bind(wel_socket_fd, (struct sockaddr *)&server_address_obj, sizeof(server_address_obj)) < 0)
    {
        printf("Error binding socket\n");
        return 1;
    }

    // Listening for incoming connections
    listen(wel_socket_fd, atoi(argv[1]));
    client_length = sizeof(client_address_obj);

    // Running a loop to keep accepting connections
    while (true)
    {
        // Accepting incoming connections
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_address_obj, &client_length);
        if (client_socket_fd < 0)
        {
            printf("Error accepting connection.\n");
            return 1;
        }

        // Storing the client socket ID in the queue
        pthread_mutex_lock(&queue_mutex);
        client_fd_queue.push(client_socket_fd);
        pthread_mutex_unlock(&queue_mutex);
        sem_post(&queue_sem);
    }

    // Closing the socket, performing cleanup and exiting the program
    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Client/client.h ####################//

#ifndef __CLIENT_H__
#define __CLIENT_H__

// Header files
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <pthread.h>

// Colours
#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define RESET "\x1B[0m"

// Constants
#define MAX_BUFFER_SIZE 1024

// Structure definition
struct client_data
{
    pthread_t tid;
    int index;
    std::string command;
    int time;
};

// Global print lock
extern pthread_mutex_t print_lock;

// Function declarations
std::pair<std::string, int> read_string_from_socket(int socket_fd);
void send_string_on_socket(std::string &string, int socket_fd);
int get_socket_fd(struct sockaddr_in *address);
void begin_process(std::vector<std::pair<int, std::string>> &commands, int n);

#endif
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Client/client_functions.cpp ####################//

#include "client.h"

// Function to read data from server socket
std::pair<std::string, int> read_string_from_socket(int socket_fd)
{
    char buffer[MAX_BUFFER_SIZE];
    int bytes_read = read(socket_fd, buffer, MAX_BUFFER_SIZE - 1);
    if (bytes_read <= 0)
    {
        pthread_mutex_lock(&print_lock);
        std::cout << "Error reading from server\n";
        pthread_mutex_unlock(&print_lock);
        exit(EXIT_FAILURE);
    }
    std::string data(buffer);

    return std::make_pair(data, bytes_read);
}

// Function to write data to server socket
void send_string_on_socket(std::string &string, int socket_fd)
{
    int bytes_sent = write(socket_fd, string.c_str(), string.length());
    if (bytes_sent < 0)
    {
        pthread_mutex_lock(&print_lock);
        std::cerr << "Error writing to socket" << std::endl;
        pthread_mutex_unlock(&print_lock);
        exit(EXIT_FAILURE);
    }
}

// Function to create a socket, connect to the server and return the file descriptor
int get_socket_fd(struct sockaddr_in *address)
{
    struct sockaddr_in server_address_obj = *address;

    // Creating socket file descriptor
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        pthread_mutex_lock(&print_lock);
        std::cerr << "Error creating socket" << std::endl;
        pthread_mutex_unlock(&print_lock);
        exit(EXIT_FAILURE);
    }

    // Initial setup of the socket
    int port_number = 8001;
    memset(&server_address_obj, 0, sizeof(server_address_obj));
    server_address_obj.sin_family = AF_INET;
    server_address_obj.sin_port = htons(port_number);

    // Connecting to the server
    if (connect(socket_fd, (struct sockaddr *)&server_address_obj, sizeof(server_address_obj)) < 0)
    {
        pthread_mutex_lock(&print_lock);
        perror("Error in connecting to server");
        pthread_mutex_unlock(&print_lock);
        exit(EXIT_FAILURE);
    }

    return socket_fd;
}

// The function run by the client thread
void *client_thread(void *arg)
{
    // Extracting the client data from the argument
    client_data client_data_obj = *(client_data *)arg;
    int index = client_data_obj.index;
    std::string command = client_data_obj.command;
    int time = client_data_obj.time;

    // Connecting the user thread to server
    struct sockaddr_in server_address_obj;
    int server_socket_fd = get_socket_fd(&server_address_obj);

    // Sleeping for the specified time
    sleep(time);

    // Sending data to server
    send_string_on_socket(command, server_socket_fd);

    int num_bytes_read;
    std::string response;

    // Read response from server
    std::tie(response, num_bytes_read) = read_string_from_socket(server_socket_fd);
    pthread_mutex_lock(&print_lock);
    std::cout << index << ":" << response << "\n";
    pthread_mutex_unlock(&print_lock);

    pthread_exit(NULL);
}

// Function to create the thread for each command
void begin_process(std::vector<std::pair<int, std::string>> &commands, int n)
{
    client_data client_data_objs[n];

    // Creating the threads
    for (int i = 0; i < n; i++)
    {
        client_data_objs[i].index = i;
        client_data_objs[i].command = commands[i].second;
        client_data_objs[i].time = commands[i].first;

        pthread_create(&client_data_objs[i].tid, NULL, client_thread, (void *)&client_data_objs[i]);
    }

    // Joining the threads
    for (int i = 0; i < n; i++)
        pthread_join(client_data_objs[i].tid, NULL);

    pthread_mutex_destroy(&print_lock);
}
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q3/Client/client.cpp ####################//

#include "client.h"

pthread_mutex_t print_lock;

int main(int argc, char *argv[])
{
    pthread_mutex_init(&print_lock, NULL);
    
    int n;
    std::cin >> n;

    std::vector<std::pair<int, std::string>> commands;

    // Storing commands entered by the user
    for (int i = 0; i < n; i++)
    {
        std::string command_string;
        int time;
        std::cin >> time;
        std::getline(std::cin >> std::ws, command_string);
        commands.push_back({time, command_string});
    }

    begin_process(commands, n);
    return 0;
}