
//###########FILE CHANGE ./main_folder/Karthik Viswanathan_305865_assignsubmission_file_/2019113015_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <string>
#include <set>
#include <tuple>
#include <sstream>
#include <map>
#include <queue>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 10
#define PORT_ARG 8005

const int initial_msg_len = 256;
string dict[1000];

struct listen {
    int wel_socket_fd;
    struct sockaddr_in client_addr_obj;
};
typedef struct pool {
    int id;
    int cur_cmd_id;
} pool;
struct listen listener;
pthread_t *p_tid;
struct pool thread_pool[100];
queue<int> request;
queue<int> rid;
pthread_mutex_t global_pool_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t request_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t request_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t map_lock[100];
int cmd_id = 0;
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    //debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

string insert_to_map(int arg1, string arg2)
{
    string out;
    if (dict[arg1].empty())
    {
        out = "Insertion successful";
        dict[arg1] = arg2;
        return out;
    }
    out = "Key already exists";
    return out;
}
string update_map(int arg1, string arg2)
{
    string out = arg2;
    if (dict[arg1].empty())
    {
        out = "Key does not exist";
    }
    dict[arg1] = arg2;
    out = arg2;
    return out;
}
string concat_to_map(int arg1, int arg2)
{
    string out;
    if (dict[arg1].empty() || dict[arg2].empty())
    {
        out = "Concat failed as at least one of the keys does not exist";
    }
    cout << dict[arg1] << " " << dict[arg2] << endl;
    string temp = dict[arg1];
    dict[arg1] += dict[arg2];
    dict[arg2] += temp;
    out = dict[arg2];
    return out;
}
string delete_from_map(int arg1)
{
    string out;
    if (!dict[arg1].empty())
    {
        dict[arg1].clear();
        out = "Deletion successful";
        return out;
    }
    else
    {
        out = "No such key exists";
        return out;
    }
}
string fetch_from_map(int arg1)
{
    string out;
    if (!dict[arg1].empty())
    {
        out = dict[arg1];
        return out;
    }
    out = "Key does not exist";
    return out;
}
///////////////////////////////
string tokenize(string message);
void handle_connection(int client_socket_fd, int cid)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        //cout<<"HEYYYY :"<<cmd<<endl;
        //cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string out = tokenize(cmd);
        uint64_t threadId = 0;
        pthread_t ptid = pthread_self();
        memcpy(&threadId, &ptid, std::min(sizeof(threadId), sizeof(ptid)));
        string msg_to_send_back = to_string(cid) + ":" + to_string(threadId) + ":" + out;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        close(client_socket_fd);
        return;
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}
string tokenize(string message)
{
    stringstream ss(message);
    string s;
    string args[1000];
    int i = 0;
    char type;
    cout<<message<<endl;
    string out;
    while (getline(ss, s, ' '))
    {
        int client_number = 0;
        if (i == 1)
        {
            string in = "insert";
            string co = "concat";
            string up = "update";
            string de = "delete";
            string fe = "fetch";
            //cout<<s<<endl;
            if (s == in)
            {
                type = 'i';
            }
            else if (s == co)
            {
                type = 'c';
            }
            else if (s == up)
            {
                type = 'u';
            }
            else if (s == de)
            {
                type = 'd';
            }
            else if (s == fe)
            {
                type = 'f';
            }
            else
            {
                type = 'x';
                continue;
            }
            //cout<<type<<s<<endl;
        }
        if (i >= 2)
        {
            //cout<<type<<" HEYYY  "<<endl;
            if (type == 'i' || type == 'u')
            {
                //cout<<s<<endl;
                int arg1 = stoi(s);
                pthread_mutex_lock(&map_lock[arg1]);
                getline(ss, s, ' ');
                string arg2 = s;
                if (type == 'i')
                {
                    out = insert_to_map(arg1, arg2);
                }
                if (type == 'u')
                {
                    out = update_map(arg1, arg2);
                }
                pthread_mutex_unlock(&map_lock[arg1]);
            }
            if (type == 'c')
            {
                int arg1 = stoi(s);
                getline(ss, s, ' ');
                int arg2 = stoi(s);
                pthread_mutex_lock(&map_lock[arg1]);
                pthread_mutex_lock(&map_lock[arg2]);
                out = concat_to_map(arg1, arg2);
                pthread_mutex_unlock(&map_lock[arg2]);
                pthread_mutex_unlock(&map_lock[arg1]);
            }
            if (type == 'd' || type == 'f')
            {
                int arg1 = stoi(s);
                if (type == 'd')
                {
                    out = delete_from_map(arg1);
                }
                if (type == 'f')
                {
                    out = fetch_from_map(arg1);
                }
            }
            if (type == 'x')
            {
                out = "other command entered";
                return out;
            }
        }
        i++;
    }
    return out;
}
void* simulate_pool(void *arg)
{
    while (1)
    {
        int flag = 0;
        int fd;
        int cid;
        pthread_mutex_lock(&global_pool_lock);
        while(request.empty())
        {
            pthread_cond_wait(&request_cond, &global_pool_lock);
            flag = 1;
        }
        fd = request.front();
        request.pop();
        cid = rid.front();
        rid.pop();
        //cout<<cid<<endl;
        pthread_mutex_unlock(&global_pool_lock);
        handle_connection(fd, cid);
    }
}
int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    int max_thread_pool = atoi(argv[1]);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    int tid = 1;
    listener.client_addr_obj = client_addr_obj;
    listener.wel_socket_fd = wel_socket_fd;
    p_tid = (pthread_t *) malloc(sizeof(pthread_t) * max_thread_pool);
    for (int i = 0; i < 100; i++)
    {
        pthread_mutex_init(&map_lock[i], NULL);
    }
    for (int i = 0; i < max_thread_pool; i++)
    {
        pthread_create(&p_tid[i], NULL, &simulate_pool, (void *)i);
    }
    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        //printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        pthread_mutex_lock(&global_pool_lock);
        //printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        request.push(client_socket_fd);
        rid.push(cmd_id);
        cmd_id++;
        //cout<<"accepted"<<endl;
        pthread_cond_signal(&request_cond);
        pthread_mutex_unlock(&global_pool_lock);
    }
    close(wel_socket_fd);
    for (int i = 0; i < max_thread_pool; i++) 
    {
        pthread_join(p_tid[i], NULL);
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/Karthik Viswanathan_305865_assignsubmission_file_/2019113015_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream>
#include <bits/stdc++.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

pthread_mutex_t send_lock = PTHREAD_MUTEX_INITIALIZER;

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl


typedef struct query
{
    int id;
    int time;
    string query;
    int socket_fd;
} query;
pthread_t *q_tid;
///////////////////////////////
#define SERVER_PORT 8005
////////////////////////////////////

const LL buff_sz = 1048576;
struct query q_argv[1000];
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void* begin_process(void *arg)
{
    int id = (uintptr_t) arg;
    sleep(q_argv[id].time);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    string msg = q_argv[id].query;
    //cout<<"sent "<<msg<<endl;
    send_string_on_socket(socket_fd, msg);
    string output_msg;
    int num_bytes_read;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&send_lock);
    cout <<output_msg << endl;
    pthread_mutex_unlock(&send_lock);
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    int queries;
    scanf("%d \n", &queries);
    for (int i = 0; i < queries; i++)
    {
        q_argv[i].id = i;
        cin >> q_argv[i].time;
        getline(cin, q_argv[i].query);
    }
    pthread_t *q_tid = (pthread_t *) malloc(sizeof(pthread_t) * queries);
    for (int i = 0; i < queries; i++)
    {
        if (pthread_create(&q_tid[i], NULL, &begin_process, (void *)i))
        {
            perror("Error initializing students");
            return -1;
        }
    }
    for (int i = 0; i < queries; i++) 
    {
        pthread_join(q_tid[i], NULL);
    }
    return 0;
}