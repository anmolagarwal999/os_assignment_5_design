
//###########FILE CHANGE ./main_folder/Adhiraj Deshmukh_305930_assignsubmission_file_/2021121012_assignment_5/q3/server_prog.cpp ####################//

#include <stdio.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <map>
#include <unordered_map>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"    

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;
////////////////////////////////////

#define CMAX 30

pthread_t th_workers[CMAX];

sem_t mutex_queue;
sem_t sem_clients;
sem_t mutex_dict[101];

queue<int*> q_clients;
string dictionary[101];
int arr[MAX_CLIENTS];

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);

    // cout << "Output: " << output << endl;

    // debug(bytes_received);

    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    // debug(output);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

int parse(char* command_line, char** args, char delimit[]) {
    int pos = 0;
    if(command_line != NULL) {
        args[pos] = command_line;

        char* temp = strtok(command_line, delimit);
        while(temp != NULL) {
            args[pos++] = temp;
            temp = strtok(NULL, delimit);
        }
    }

    args[pos] = NULL;
    
    return pos;
}

void *handle_connection(void *args) {
    int client_socket_fd = *((int *) args);
    free(args);

    char delimit[] = " \t\r\n\v\f";
    char* input_line;
    char** arr;

    int received_num, sent_num, n2;
    string cmd, msg_to_send_back;

    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);

    // cout << "CMD: " << cmd << endl;

    int ret_val = received_num;
    if (ret_val <= 0) {
        printf("Server could not read msg sent from client\n");
        goto close_client_socket_ceremony;
    }

    if (cmd == "exit") {
        cout << "Exit pressed by client" << endl;
        goto close_client_socket_ceremony;
    }

    input_line = (char *) calloc(256, sizeof(char));
    for(int i=0; i<cmd.length(); i++) {
        input_line[i] = cmd[i];
    }
    input_line[cmd.length()] = '\0';

    // printf("input_line: %s\n", input_line);

    arr = (char**) calloc(256, sizeof(char*));
    for(int i=0; i<256; i++) {
        arr[i] = (char *) calloc(256, sizeof(char));
    }

    
    n2 = parse(input_line, arr, delimit);

    // String that we can send back.
    msg_to_send_back = "Error in message !!";

    if(strcmp(arr[0], "insert") == 0) {

        if(n2 != 3) {
            cout << gettid() << ":'insert' takes 2 arguments" << endl;
            goto close_client_socket_ceremony;
        }

        int key = atoi(arr[1]) % 101;
        string value(arr[2]);

        sem_wait(&mutex_dict[key]);
        if(dictionary[key].length()) {
            msg_to_send_back = to_string((int)gettid()) + ":" + "Key already exists";
        }
        else {
            dictionary[key] = value;
            msg_to_send_back = to_string((int)gettid()) + ":" + "Insertion successful";
        }
        sem_post(&mutex_dict[key]);
    }
    else if(strcmp(arr[0], "delete") == 0) {

        if(n2 != 2) {
            cout << gettid() << ":'delete' takes 1 argument" << endl;
            goto close_client_socket_ceremony;
        }

        int key = atoi(arr[1]) % 101;

        sem_wait(&mutex_dict[key]);
        if(dictionary[key].length()) {
            dictionary[key].clear();
            msg_to_send_back = to_string((int)gettid()) + ":" + "Deletion successful";
        }
        else {
            msg_to_send_back = to_string((int)gettid()) + ": " + "No such key exists";
        }
        sem_post(&mutex_dict[key]);
    }
    else if(strcmp(arr[0], "update") == 0) {

        if(n2 != 3) {
            cout << gettid() << ":'update' takes 2 arguments" << endl;
            goto close_client_socket_ceremony;
        }

        int key = atoi(arr[1]);
        string value(arr[2]);

        sem_wait(&mutex_dict[key]);
        if(dictionary[key].length()) {
            dictionary[key] = value;
            msg_to_send_back = to_string((int)gettid()) + ":" + dictionary[key];
        }
        else {
            msg_to_send_back = to_string((int)gettid()) + ":" + "Key does not exist";
        }
        sem_post(&mutex_dict[key]);
    }
    else if(strcmp(arr[0], "concat") == 0) {

        if(n2 != 3) {
            cout << gettid() << ":'concat' takes 2 arguments" << endl;
            goto close_client_socket_ceremony;
        }

        int key1 = atoi(arr[1]), key2 = atoi(arr[2]);

        sem_wait(&mutex_dict[key1]);
        sem_wait(&mutex_dict[key2]);
        if(!dictionary[key1].length() || !dictionary[key2].length()) {
            msg_to_send_back = to_string((int)gettid()) + ":" + "Concat failed as at least one of the keys does not exist";
        }
        else {
            string value1 = dictionary[key1], value2 = dictionary[key2];

            dictionary[key1] += value2;
            dictionary[key2] += value1;

            msg_to_send_back = to_string((int)gettid()) + ":" + dictionary[key2];
        }
        sem_post(&mutex_dict[key2]);
        sem_post(&mutex_dict[key1]);
    }
    else if(strcmp(arr[0], "fetch") == 0) {

        if(n2 != 2) {
            cout << gettid() << ":'fetch' takes 1 arguments" << endl;
            goto close_client_socket_ceremony;
        }

        int key = atoi(arr[1]);

        sem_wait(&mutex_dict[key]);
        if(dictionary[key].length()) {
            msg_to_send_back = to_string((int)gettid()) + ":" + dictionary[key];
        }
        else {
            msg_to_send_back = to_string((int)gettid()) + ":" + "Key does not exist";
        }
        sem_post(&mutex_dict[key]);
    }
    else {
        printf("%d:'%s' command doesn't exist!\n", (int)gettid(), arr[0]);
        goto close_client_socket_ceremony;
    }

    // Sleeping before sending signal beck
    sleep(2);

    // cout << "MSG TO SEND : " << msg_to_send_back << endl;

    if (send_string_on_socket(client_socket_fd, msg_to_send_back) == -1) {
        perror("Error while writing to client. Seems socket has been closed");
        goto close_client_socket_ceremony;
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}

void *init_worker(void *arg) {
    while(true) {
        
        sem_wait(&sem_clients);
        sem_wait(&mutex_queue);

        if(!q_clients.empty()) {
            int *pclient = q_clients.front();
            q_clients.pop();

            sem_post(&mutex_queue);

            handle_connection((void *) pclient);
        }
        else {
            sem_post(&mutex_queue);
        }
    }

    return NULL;
}

int main(int argc, char *argv[])
{

    // int i, j, k, t, n;

    if(argc != 2) {
        cerr << "Only 1 integer input required" << endl;
        return 0;
    }

    int n = atoi(argv[1]);
    assert(n >= 0 && n <= CMAX);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    sem_init(&mutex_queue, 0, 1);
    for(int i=0; i<101; i++) {
        sem_init(&mutex_dict[i], 0, 1);
    }
    sem_init(&sem_clients, 0, 0);

    // Creating worker threads
    for(int i=0; i<n; i++) {
        pthread_create(&th_workers[i], NULL, init_worker, NULL);
    }

    /* listen for incoming connection requests */

    // listen(wel_socket_fd, MAX_CLIENTS);
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        int *pclient = (int *) malloc(sizeof(int));
        *pclient = client_socket_fd;

        sem_wait(&mutex_queue);
        q_clients.push(pclient);
        sem_post(&sem_clients);
        sem_post(&mutex_queue);
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Adhiraj Deshmukh_305930_assignsubmission_file_/2021121012_assignment_5/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <map>
#include <unordered_map>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////
#define MAX_CLIENTS 100

const LL buff_sz = 1048576;

typedef struct message {
    int id;
    int time;
    string str;
} messsage;

pthread_t th_clients[MAX_CLIENTS];
message arr_clients[MAX_CLIENTS];

///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{

    // cout << "We are sending " << s << endl;

    int bytes_sent = write(fd, s.c_str(), s.length());

    // debug(bytes_sent);                                                                                                                                                                                          
    // debug(s); 
                                                                                                                                                                                                      
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "                                                                                                                                                                                                
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    int id = ((message *) args)->id;
    int time = ((message *) args)->time;
    string str = ((message *) args)->str;

    sleep(time);

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    // cout << "Connection to server successful" << endl;

    send_string_on_socket(socket_fd, str);

    int num_bytes_read;
    string output_msg;

    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);

    // Recieved message
    cout << id << ":" << output_msg << endl;

    // part;

    return NULL;
}

int main(int argc, char *argv[])
{

    // int i, j, k, t, n;
    size_t sz = 256, input_chars = 0;

    int m;
    cin>>m;
    // cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 

    for(int i=0; i<m; i++) {
        arr_clients[i].id = i;

        cin>>arr_clients[i].time;
        cin.ignore();

        getline(cin, arr_clients[i].str);
    }

    for(int i=0; i<m; i++) {
        pthread_create(&th_clients[i], NULL, begin_process, (void *) &arr_clients[i]);
    }

    for(int i=0; i<m; i++) {
        pthread_join(th_clients[i], NULL);
    }

    return 0;
}