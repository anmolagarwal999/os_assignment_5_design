
//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/2019113005_assignment_5/q3/server.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
#include <semaphore.h>
#include <queue>
#include <sstream>
#include <thread>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 101
#define PORT_ARG 8001
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

const int initial_msg_len = 256;

pthread_mutex_t all_client_locks[MAX_CLIENTS];
pthread_mutex_t client_sockets_lock;
pthread_t *client_threads;
string server_dict[MAX_CLIENTS];
sem_t client_sem;
queue<int> client_sockets;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = "Ack: " + cmd;

        string s;
        stringstream ss(cmd);
        vector<string> words; // space-separated words sent by client
        char delim = ' ';
        while (getline(ss, s, delim)) // stream from the string
            words.pb(s);

        int i1 = stoi(words[1]);
        pthread_mutex_lock(&all_client_locks[i1]);
        if (words[0] == "concat")
        {
            string string1 = server_dict[i1];
            int i2 = stoi(words[2]);
            pthread_mutex_lock(&all_client_locks[i2]);
            string string2 = server_dict[i2];

            if (!string1.empty() && !string2.empty())
            {
                server_dict[i2] = string2 + string1;
                server_dict[i1] = string1 + string2;
                msg_to_send_back = string2 + string1;
            }
            else
                msg_to_send_back = "Concat failed as at least one of the keys does not exist";
            pthread_mutex_unlock(&all_client_locks[i2]);
        }
        else if (words[0] == "fetch")
        {
            if (!server_dict[i1].empty())
                msg_to_send_back = server_dict[i1];
            else
                msg_to_send_back = "Key does not exist";
        }
        else if (words[0] == "update")
        {
            if (!server_dict[i1].empty())
            {
                server_dict[i1] = words[2];
                msg_to_send_back = server_dict[i1];
            }
            else
                msg_to_send_back = "Key does not exist";
        }
        else if (words[0] == "delete")
        {
            if (server_dict[i1].empty())
                msg_to_send_back = "No such key exists";
            else
            {
                server_dict[i1].clear();
                msg_to_send_back = "Deletion successful";
            }
        }
        else if (words[0] == "insert")
        {
            if (!server_dict[i1].empty())
                msg_to_send_back = "Key already exists";
            else
            {
                msg_to_send_back = "Insertion successful";
                server_dict[i1] = words[2];
            }
        }
        pthread_mutex_unlock(&all_client_locks[i1]);

        sleep(2);

        stringstream st;
        auto myid = std::this_thread::get_id();
        st << myid;
        msg_to_send_back = " : " + st.str() + " : " + msg_to_send_back;
        cout << msg_to_send_back << endl;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            break;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *worker_function(void *args)
{
    int client_socket_fd = -1;
    for (;;)
    {
        sem_wait(&client_sem); // wait for client_sockets queue elements

        pthread_mutex_lock(&client_sockets_lock);
        if (client_sockets.empty() == false)
        {
            client_socket_fd = client_sockets.front();
            client_sockets.pop();
        }
        pthread_mutex_unlock(&client_sockets_lock);

        if (client_socket_fd == -1)
            continue;

        handle_connection(client_socket_fd);
        client_socket_fd = -1;
    }

    return NULL;
}

int main(int argc, char *argv[])
{
    int num_threads = atoi(argv[1]);
    client_threads = (pthread_t *)malloc(sizeof(pthread_t) * num_threads);
    for (int i = 0; i < MAX_CLIENTS; i++)
    {
        server_dict[i] = "";
        pthread_mutex_init(&all_client_locks[i], NULL);
    }

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    pthread_mutex_init(&client_sockets_lock, NULL); // lock before operating on the queue
    sem_init(&client_sem, 0, 0);                    // init to 0 client requests
    for (int i = 0; i < num_threads; i++)
        pthread_create(&client_threads[i], NULL, worker_function, NULL);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    // CHECK WHY THE CASTING IS REQUIRED
    if (::bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client.
        */
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&client_sockets_lock);
        client_sockets.push(client_socket_fd);
        pthread_mutex_unlock(&client_sockets_lock);
        sem_post(&client_sem);
    }

    free(client_threads);
    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/2019113005_assignment_5/q3/client.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
typedef struct arguments
{
    string command;
    int idx;
} args;

args *user_args;
pthread_t *threads;
///////////////////////////////////////////////////

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    // https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    // part;
    //  printf(BGRN "Connected to server\n" ANSI_RESET);
    //  part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *passed_args)
{
    args user_args = *(args *)passed_args;
    string time_cmd = user_args.command.substr(0, user_args.command.find(' '));
    string to_send = user_args.command.substr(user_args.command.find(' ') + 1);
    int idx = user_args.idx;
    sleep(stoi(time_cmd));

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    send_string_on_socket(socket_fd, to_send);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << idx << output_msg << endl;
    close(socket_fd);

    return NULL;
}

int main(int argc, char *argv[])
{
    string input;
    getline(cin, input);
    int m = stoi(input);

    user_args = (args *)malloc(sizeof(args) * m);
    threads = (pthread_t *)malloc(sizeof(pthread_t) * m);

    for (int i = 0; i < m; i++)
    {
        user_args[i].idx = i;
        getline(cin, input);
        user_args[i].command = input;
        pthread_create(&threads[i], NULL, begin_process, &user_args[i]);
    }

    for (int i = 0; i < m; i++)
        pthread_join(threads[i], NULL);

    return 0;
}
//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/__MACOSX/2019113005_assignment_5/q3/._client.cpp ####################//

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#define NUM_OF_SEMAPHORES 3
#define PERSON_NAME_SIZE 15
#define LAB_NAME_SIZE 15
#define PTHREAD_COND_SIZE sizeof(pthread_cond_t)
#define PTHREAD_MUTEX_SIZE sizeof(pthread_mutex_t)
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct person
{
    int id;
    int reach_time;
    int group_no;
    int no_of_sems;
    int no_of_goals;
    int patience;
    char name[PERSON_NAME_SIZE];
    char sem_types[NUM_OF_SEMAPHORES];
    char fan_type;
} person;
person *people;

typedef struct arg_struct
{
    person arg_person;
    char zone;
} args;
args *team_args;

typedef struct team
{
    int id;
    int no_of_chances;
    int *prev_chance;
    float *goal_prob;
    char team_type;
} team;
team *teams;

pthread_t *people_thread, *teams_thread;
sem_t h_zone, a_zone, n_zone;
int h_capacity, a_capacity, n_capacity, spectating_time, num_people = 0;

// keeps track of goals of every team
int goals[2];
pthread_cond_t goals_changed[2];
pthread_mutex_t goals_lock[2];

// keeps track of the zones a person is in
char *person_in_zone;
pthread_mutex_t *person_zone_lock;
pthread_cond_t *person_moved;

int char_cmp(char *input_char, char check_char[])
{
    char temp[2];
    temp[0] = *(char *)input_char;
    temp[1] = '\0';
    return strcmp(temp, check_char);
}

void *search_in_N(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&n_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'N'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone N\n" RESET, cur_person.name);
        }
        else
            sem_post(&n_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_A(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&a_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'A'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone A\n" RESET, cur_person.name);
        }
        // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
        else
            sem_post(&a_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_H(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&h_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT) // patience runs out
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'H'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone H\n" RESET, cur_person.name);
        }
        else
            sem_post(&h_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *simulate_person_in_game(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int type_of_fan = -1;
    if (char_cmp(&cur_person.fan_type, "A") == 0) // A fan => follow goals of the Home team
        type_of_fan = 0;
    else if (char_cmp(&cur_person.fan_type, "H") == 0) // H fan => follow goals of the Away team
        type_of_fan = 1;
    if (type_of_fan == -1) // neutral => doesn't get enraged
        return NULL;

    pthread_mutex_lock(&goals_lock[type_of_fan]);
    while (goals[type_of_fan] < cur_person.no_of_goals) // wait till opposing team goals doesn't enrage
    {
        if (goals[type_of_fan] >= 0)
            pthread_cond_wait(&goals_changed[type_of_fan], &goals_lock[type_of_fan]);
        else
            break;
    }
    if (goals[type_of_fan] >= cur_person.no_of_goals)
        printf(RED "%s is leaving due to bad performance of his team\n" RESET, cur_person.name);
    pthread_mutex_unlock(&goals_lock[type_of_fan]);
    pthread_cond_broadcast(&person_moved[cur_person.id]);

    return NULL;
}

void *sear(void *input_person)
{
    person cur_person = *(person *)input_person;
    int id = cur_person.id;

    pthread_t threads[NUM_OF_SEMAPHORES]; // n, a, h
    if (char_cmp(&cur_person.fan_type, "N") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "A") == 0)
    {
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "H") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }

    return NULL;
}

void *person_motion(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;
    int id = cur_person.id, rt = cur_person.reach_time;

    sleep(rt); // enters at reach_time
    printf(BLU "%s has reached the stadium\n" RESET, cur_person.name);

    // simulate seat allocation
    pthread_t thread;
    pthread_create(&thread, NULL, sear, &arguments);
    pthread_join(thread, NULL);

    // did not find any seat
    int isFound_seat = 0;
    pthread_mutex_lock(&person_zone_lock[id]);
    // D = Zoneless, G = Left
    if (char_cmp(&person_in_zone[id], "D") != 0 && char_cmp(&person_in_zone[id], "G") != 0)
        isFound_seat = 1;
    pthread_mutex_unlock(&person_zone_lock[id]);

    if (isFound_seat)
    {
        struct timespec ts; // spectating time
        clock_gettime(CLOCK_REALTIME, &ts);

        // found a seat and watching the game
        pthread_create(&thread, NULL, simulate_person_in_game, &arguments);

        pthread_mutex_lock(&person_zone_lock[id]);
        ts.tv_sec += spectating_time;
        // signal conditional variable when person wants to leave
        if (pthread_cond_timedwait(&person_moved[id], &person_zone_lock[id], &ts) == ETIMEDOUT)
            printf(RED "%s watched the match for %d seconds and is leaving\n" RESET, cur_person.name, spectating_time);
        pthread_mutex_unlock(&person_zone_lock[id]);

        // person left, increment semaphore
        pthread_mutex_lock(&person_zone_lock[id]);
        if (char_cmp(&person_in_zone[id], "H") == 0)
            sem_post(&h_zone);
        else if (char_cmp(&person_in_zone[id], "A") == 0)
            sem_post(&a_zone);
        else if (char_cmp(&person_in_zone[id], "N") == 0)
            sem_post(&n_zone);
        person_in_zone[id] = 'G';
        pthread_mutex_unlock(&person_zone_lock[id]);

        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }
    else
    {
        printf(BLU "%s did not find a seat in any of the zones\n" RESET, cur_person.name);
        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }

    return NULL;
}

void *simulate_team(void *input_team)
{
    team cur_team = *(team *)input_team;

    int i;
    for (i = 0; i < cur_team.no_of_chances; i++)
    {
        // probability of team scoring
        float x = cur_team.goal_prob[i] - ((float)rand() / (float)(RAND_MAX / 1.0));
        int id = cur_team.id;
        sleep(cur_team.prev_chance[i]); // sleep till next chance to score goal

        if (x >= 0) // gloal scored
        {
            pthread_mutex_lock(&goals_lock[id]);
            goals[id]++;
            printf(CYN "Team %c has scored thier goal %d\n" RESET, cur_team.team_type, goals[id]);
            pthread_mutex_unlock(&goals_lock[id]);
        }
        else // gloal missed
            printf(CYN "Team %c missed their chance to score their goal %d\n" RESET, cur_team.team_type, goals[id] + 1);
        pthread_cond_broadcast(&goals_changed[id]); // broadcast after every chance
    }
    return NULL;
}

void allocate_memory(int isDatastruct)
{
    if (isDatastruct == 1) // init people, teams, args
    {
        people = malloc(sizeof(person) * 1);
        team_args = malloc(sizeof(args) * 1);
        teams = malloc(sizeof(team) * 2);
        int i;
        for (i = 0; i < 2; i++)
        {
            teams[i].goal_prob = malloc(sizeof(float) * 1);
            teams[i].prev_chance = malloc(sizeof(int) * 1);
        }
    }
    else if (isDatastruct == 0) // init rest
    {
        person_moved = malloc(sizeof(pthread_cond_t) * num_people);
        person_in_zone = malloc(sizeof(char) * num_people);
        person_zone_lock = malloc(sizeof(pthread_mutex_t) * num_people);
        people_thread = malloc(sizeof(pthread_t) * num_people);
        teams_thread = malloc(sizeof(pthread_t) * 2);
    }
}

void take_input()
{
    int i, ii;
    int num_groups, num_in_group;

    scanf("%d %d %d", &h_capacity, &a_capacity, &n_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    for (i = 1; i < num_groups + 1; i++)
    {
        scanf("%d", &num_in_group);

        for (ii = 0; ii < num_in_group; ii++)
        {
            char name[PERSON_NAME_SIZE], support_teams;
            int reach_time, patience, no_of_goals;
            people = realloc(people, sizeof(person) * (num_people + 1));
            team_args = realloc(team_args, sizeof(args) * (num_people + 1));

            scanf("%s %c %d %d %d", name, &support_teams, &reach_time, &patience, &no_of_goals);
            strcpy(people[num_people].name, name);
            people[num_people].fan_type = support_teams;
            people[num_people].reach_time = reach_time;
            people[num_people].patience = patience;
            people[num_people].no_of_goals = no_of_goals;
            people[num_people].id = num_people;
            people[num_people].group_no = i;
            team_args[num_people].arg_person = people[num_people];

            num_people++;
        }
    }

    int goal_scoring_chances;
    scanf("%d\n", &goal_scoring_chances);

    for (i = 0; i < goal_scoring_chances; i++)
    {
        int prev_time[2] = {0, 0}, inp_time, j = -1;
        char inp_team;
        scanf("%c", &inp_team);
        if (char_cmp(&inp_team, "H") == 0)
            j = 0;
        else if (char_cmp(&inp_team, "A") == 0)
            j = 1;

        if (j == -1)
        {
            printf("Incorrect Team Name Entered\n");
            exit(1);
        }

        teams[j].goal_prob = realloc(teams[j].goal_prob, sizeof(float) * (teams[j].no_of_chances + 1));
        teams[j].prev_chance = realloc(teams[j].prev_chance, sizeof(int) * (teams[j].no_of_chances + 1));

        int chances = teams[j].no_of_chances;
        teams[j].no_of_chances++;

        float goal_prob;
        scanf("%d %f\n", &inp_time, &goal_prob);
        teams[j].goal_prob[chances] = goal_prob;

        // time from the previous chance to goal
        teams[j].prev_chance[chances] = inp_time - prev_time[j];
        prev_time[j] = inp_time;
    }
}

int main(int argc, char *argv[])
{
    allocate_memory(1);
    teams[0].team_type = 'H';
    teams[1].team_type = 'A';
    int i;
    for (i = 0; i < 2; i++)
    {
        teams[i].id = i;
        teams[i].no_of_chances = 0;
    }
    take_input();
    allocate_memory(0);

    pthread_mutex_init(&goals_lock[0], NULL);
    pthread_mutex_init(&goals_lock[1], NULL);
    pthread_cond_init(&goals_changed[0], NULL);
    pthread_cond_init(&goals_changed[1], NULL);
    goals[0] = 0;
    goals[1] = 0;

    // initialize the semaphores corresponding to the number of seats in each zone
    sem_init(&h_zone, 0, h_capacity);
    sem_init(&a_zone, 0, a_capacity);
    sem_init(&n_zone, 0, n_capacity);

    for (i = 0; i < num_people; i++)
    {
        person_in_zone[i] = 'E';
        pthread_mutex_init(&person_zone_lock[i], NULL);
        pthread_cond_init(&person_moved[i], NULL);
    }

    // create threads
    pthread_create(&teams_thread[0], NULL, simulate_team, &teams[0]);
    pthread_create(&teams_thread[1], NULL, simulate_team, &teams[1]);
    for (i = 0; i < num_people; i++)
        pthread_create(&people_thread[i], NULL, person_motion, &team_args[i]);

    // wait for the threads to finish
    pthread_join(teams_thread[0], NULL);
    pthread_join(teams_thread[1], NULL);
    for (i = 0; i < num_people; i++)
        pthread_join(people_thread[i], NULL);

    goals[0] = -1;
    pthread_cond_broadcast(&goals_changed[0]);
    pthread_cond_destroy(&goals_changed[0]);
    pthread_mutex_destroy(&goals_lock[0]);
    pthread_cancel(teams_thread[0]);
    goals[1] = -1;
    pthread_cond_broadcast(&goals_changed[1]);
    pthread_cond_destroy(&goals_changed[1]);
    pthread_mutex_destroy(&goals_lock[1]);
    pthread_cancel(teams_thread[1]);

    sem_destroy(&n_zone);
    sem_destroy(&a_zone);
    sem_destroy(&h_zone);

    return 0;
}

//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/__MACOSX/2019113005_assignment_5/q3/._server.cpp ####################//

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#define NUM_OF_SEMAPHORES 3
#define PERSON_NAME_SIZE 15
#define LAB_NAME_SIZE 15
#define PTHREAD_COND_SIZE sizeof(pthread_cond_t)
#define PTHREAD_MUTEX_SIZE sizeof(pthread_mutex_t)
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct person
{
    int id;
    int reach_time;
    int group_no;
    int no_of_sems;
    int no_of_goals;
    int patience;
    char name[PERSON_NAME_SIZE];
    char sem_types[NUM_OF_SEMAPHORES];
    char fan_type;
} person;
person *people;

typedef struct arg_struct
{
    person arg_person;
    char zone;
} args;
args *team_args;

typedef struct team
{
    int id;
    int no_of_chances;
    int *prev_chance;
    float *goal_prob;
    char team_type;
} team;
team *teams;

pthread_t *people_thread, *teams_thread;
sem_t h_zone, a_zone, n_zone;
int h_capacity, a_capacity, n_capacity, spectating_time, num_people = 0;

// keeps track of goals of every team
int goals[2];
pthread_cond_t goals_changed[2];
pthread_mutex_t goals_lock[2];

// keeps track of the zones a person is in
char *person_in_zone;
pthread_mutex_t *person_zone_lock;
pthread_cond_t *person_moved;

int char_cmp(char *input_char, char check_char[])
{
    char temp[2];
    temp[0] = *(char *)input_char;
    temp[1] = '\0';
    return strcmp(temp, check_char);
}

void *search_in_N(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&n_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'N'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone N\n" RESET, cur_person.name);
        }
        else
            sem_post(&n_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_A(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&a_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'A'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone A\n" RESET, cur_person.name);
        }
        // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
        else
            sem_post(&a_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_H(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&h_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT) // patience runs out
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'H'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone H\n" RESET, cur_person.name);
        }
        else
            sem_post(&h_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *simulate_person_in_game(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int type_of_fan = -1;
    if (char_cmp(&cur_person.fan_type, "A") == 0) // A fan => follow goals of the Home team
        type_of_fan = 0;
    else if (char_cmp(&cur_person.fan_type, "H") == 0) // H fan => follow goals of the Away team
        type_of_fan = 1;
    if (type_of_fan == -1) // neutral => doesn't get enraged
        return NULL;

    pthread_mutex_lock(&goals_lock[type_of_fan]);
    while (goals[type_of_fan] < cur_person.no_of_goals) // wait till opposing team goals doesn't enrage
    {
        if (goals[type_of_fan] >= 0)
            pthread_cond_wait(&goals_changed[type_of_fan], &goals_lock[type_of_fan]);
        else
            break;
    }
    if (goals[type_of_fan] >= cur_person.no_of_goals)
        printf(RED "%s is leaving due to bad performance of his team\n" RESET, cur_person.name);
    pthread_mutex_unlock(&goals_lock[type_of_fan]);
    pthread_cond_broadcast(&person_moved[cur_person.id]);

    return NULL;
}

void *sear(void *input_person)
{
    person cur_person = *(person *)input_person;
    int id = cur_person.id;

    pthread_t threads[NUM_OF_SEMAPHORES]; // n, a, h
    if (char_cmp(&cur_person.fan_type, "N") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "A") == 0)
    {
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "H") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }

    return NULL;
}

void *person_motion(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;
    int id = cur_person.id, rt = cur_person.reach_time;

    sleep(rt); // enters at reach_time
    printf(BLU "%s has reached the stadium\n" RESET, cur_person.name);

    // simulate seat allocation
    pthread_t thread;
    pthread_create(&thread, NULL, sear, &arguments);
    pthread_join(thread, NULL);

    // did not find any seat
    int isFound_seat = 0;
    pthread_mutex_lock(&person_zone_lock[id]);
    // D = Zoneless, G = Left
    if (char_cmp(&person_in_zone[id], "D") != 0 && char_cmp(&person_in_zone[id], "G") != 0)
        isFound_seat = 1;
    pthread_mutex_unlock(&person_zone_lock[id]);

    if (isFound_seat)
    {
        struct timespec ts; // spectating time
        clock_gettime(CLOCK_REALTIME, &ts);

        // found a seat and watching the game
        pthread_create(&thread, NULL, simulate_person_in_game, &arguments);

        pthread_mutex_lock(&person_zone_lock[id]);
        ts.tv_sec += spectating_time;
        // signal conditional variable when person wants to leave
        if (pthread_cond_timedwait(&person_moved[id], &person_zone_lock[id], &ts) == ETIMEDOUT)
            printf(RED "%s watched the match for %d seconds and is leaving\n" RESET, cur_person.name, spectating_time);
        pthread_mutex_unlock(&person_zone_lock[id]);

        // person left, increment semaphore
        pthread_mutex_lock(&person_zone_lock[id]);
        if (char_cmp(&person_in_zone[id], "H") == 0)
            sem_post(&h_zone);
        else if (char_cmp(&person_in_zone[id], "A") == 0)
            sem_post(&a_zone);
        else if (char_cmp(&person_in_zone[id], "N") == 0)
            sem_post(&n_zone);
        person_in_zone[id] = 'G';
        pthread_mutex_unlock(&person_zone_lock[id]);

        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }
    else
    {
        printf(BLU "%s did not find a seat in any of the zones\n" RESET, cur_person.name);
        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }

    return NULL;
}

void *simulate_team(void *input_team)
{
    team cur_team = *(team *)input_team;

    int i;
    for (i = 0; i < cur_team.no_of_chances; i++)
    {
        // probability of team scoring
        float x = cur_team.goal_prob[i] - ((float)rand() / (float)(RAND_MAX / 1.0));
        int id = cur_team.id;
        sleep(cur_team.prev_chance[i]); // sleep till next chance to score goal

        if (x >= 0) // gloal scored
        {
            pthread_mutex_lock(&goals_lock[id]);
            goals[id]++;
            printf(CYN "Team %c has scored thier goal %d\n" RESET, cur_team.team_type, goals[id]);
            pthread_mutex_unlock(&goals_lock[id]);
        }
        else // gloal missed
            printf(CYN "Team %c missed their chance to score their goal %d\n" RESET, cur_team.team_type, goals[id] + 1);
        pthread_cond_broadcast(&goals_changed[id]); // broadcast after every chance
    }
    return NULL;
}

void allocate_memory(int isDatastruct)
{
    if (isDatastruct == 1) // init people, teams, args
    {
        people = malloc(sizeof(person) * 1);
        team_args = malloc(sizeof(args) * 1);
        teams = malloc(sizeof(team) * 2);
        int i;
        for (i = 0; i < 2; i++)
        {
            teams[i].goal_prob = malloc(sizeof(float) * 1);
            teams[i].prev_chance = malloc(sizeof(int) * 1);
        }
    }
    else if (isDatastruct == 0) // init rest
    {
        person_moved = malloc(sizeof(pthread_cond_t) * num_people);
        person_in_zone = malloc(sizeof(char) * num_people);
        person_zone_lock = malloc(sizeof(pthread_mutex_t) * num_people);
        people_thread = malloc(sizeof(pthread_t) * num_people);
        teams_thread = malloc(sizeof(pthread_t) * 2);
    }
}

void take_input()
{
    int i, ii;
    int num_groups, num_in_group;

    scanf("%d %d %d", &h_capacity, &a_capacity, &n_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    for (i = 1; i < num_groups + 1; i++)
    {
        scanf("%d", &num_in_group);

        for (ii = 0; ii < num_in_group; ii++)
        {
            char name[PERSON_NAME_SIZE], support_teams;
            int reach_time, patience, no_of_goals;
            people = realloc(people, sizeof(person) * (num_people + 1));
            team_args = realloc(team_args, sizeof(args) * (num_people + 1));

            scanf("%s %c %d %d %d", name, &support_teams, &reach_time, &patience, &no_of_goals);
            strcpy(people[num_people].name, name);
            people[num_people].fan_type = support_teams;
            people[num_people].reach_time = reach_time;
            people[num_people].patience = patience;
            people[num_people].no_of_goals = no_of_goals;
            people[num_people].id = num_people;
            people[num_people].group_no = i;
            team_args[num_people].arg_person = people[num_people];

            num_people++;
        }
    }

    int goal_scoring_chances;
    scanf("%d\n", &goal_scoring_chances);

    for (i = 0; i < goal_scoring_chances; i++)
    {
        int prev_time[2] = {0, 0}, inp_time, j = -1;
        char inp_team;
        scanf("%c", &inp_team);
        if (char_cmp(&inp_team, "H") == 0)
            j = 0;
        else if (char_cmp(&inp_team, "A") == 0)
            j = 1;

        if (j == -1)
        {
            printf("Incorrect Team Name Entered\n");
            exit(1);
        }

        teams[j].goal_prob = realloc(teams[j].goal_prob, sizeof(float) * (teams[j].no_of_chances + 1));
        teams[j].prev_chance = realloc(teams[j].prev_chance, sizeof(int) * (teams[j].no_of_chances + 1));

        int chances = teams[j].no_of_chances;
        teams[j].no_of_chances++;

        float goal_prob;
        scanf("%d %f\n", &inp_time, &goal_prob);
        teams[j].goal_prob[chances] = goal_prob;

        // time from the previous chance to goal
        teams[j].prev_chance[chances] = inp_time - prev_time[j];
        prev_time[j] = inp_time;
    }
}

int main(int argc, char *argv[])
{
    allocate_memory(1);
    teams[0].team_type = 'H';
    teams[1].team_type = 'A';
    int i;
    for (i = 0; i < 2; i++)
    {
        teams[i].id = i;
        teams[i].no_of_chances = 0;
    }
    take_input();
    allocate_memory(0);

    pthread_mutex_init(&goals_lock[0], NULL);
    pthread_mutex_init(&goals_lock[1], NULL);
    pthread_cond_init(&goals_changed[0], NULL);
    pthread_cond_init(&goals_changed[1], NULL);
    goals[0] = 0;
    goals[1] = 0;

    // initialize the semaphores corresponding to the number of seats in each zone
    sem_init(&h_zone, 0, h_capacity);
    sem_init(&a_zone, 0, a_capacity);
    sem_init(&n_zone, 0, n_capacity);

    for (i = 0; i < num_people; i++)
    {
        person_in_zone[i] = 'E';
        pthread_mutex_init(&person_zone_lock[i], NULL);
        pthread_cond_init(&person_moved[i], NULL);
    }

    // create threads
    pthread_create(&teams_thread[0], NULL, simulate_team, &teams[0]);
    pthread_create(&teams_thread[1], NULL, simulate_team, &teams[1]);
    for (i = 0; i < num_people; i++)
        pthread_create(&people_thread[i], NULL, person_motion, &team_args[i]);

    // wait for the threads to finish
    pthread_join(teams_thread[0], NULL);
    pthread_join(teams_thread[1], NULL);
    for (i = 0; i < num_people; i++)
        pthread_join(people_thread[i], NULL);

    goals[0] = -1;
    pthread_cond_broadcast(&goals_changed[0]);
    pthread_cond_destroy(&goals_changed[0]);
    pthread_mutex_destroy(&goals_lock[0]);
    pthread_cancel(teams_thread[0]);
    goals[1] = -1;
    pthread_cond_broadcast(&goals_changed[1]);
    pthread_cond_destroy(&goals_changed[1]);
    pthread_mutex_destroy(&goals_lock[1]);
    pthread_cancel(teams_thread[1]);

    sem_destroy(&n_zone);
    sem_destroy(&a_zone);
    sem_destroy(&h_zone);

    return 0;
}
