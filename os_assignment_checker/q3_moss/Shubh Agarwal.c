
//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q3/server.cpp ####################//

#include <cstdio>
#include <iostream>
#include <sstream>
#include <netdb.h>
#include <vector>
#include <string>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;
constexpr unsigned int port = 8080;

typedef sockaddr SockAddress;

map<int, string> dictionary;

queue<int> Socket;
vector<pthread_mutex_t> mutexLock(100, PTHREAD_MUTEX_INITIALIZER);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void insert(string value, int key, string& str)
{
    pthread_mutex_lock(&mutexLock[key]);
    if (!dictionary.count(key))
	{
		dictionary[key] = value;
        str += "Insertion Successful";
	}
    else
        str += "Key already exists";
    pthread_mutex_unlock(&mutexLock[key]);
}

void Delete (int key, string& str)
{
    pthread_mutex_lock(&mutexLock[key]);
    if (!dictionary.count(key))
        str += "No such key exists";
    else
	{
        dictionary.erase(key);
        str += "Deletion Successful";
	}
    pthread_mutex_unlock(&mutexLock[key]);
}

void update(string value, int key, string& str)
{
    pthread_mutex_lock(&mutexLock[key]);
	if (dictionary.count(key))
	{
		dictionary[key] = value;
        str += value;
	}
	else
        str += "No such key exists";
    pthread_mutex_unlock(&mutexLock[key]);
}

void concat(int key1, int key2, string& str)
{
    pthread_mutex_lock(&mutexLock[key1]);
    pthread_mutex_lock(&mutexLock[key2]);
    if (!dictionary.count(key1) || !dictionary.count(key2))
        str += "Concat failed as a least one of the keys does not exist";
    else
    {
        string value1 = dictionary[key1];
        string value2 = dictionary[key2];
        dictionary[key1] += value2;
        dictionary[key2] += value1;
        str += value2 + value1;
    }
    pthread_mutex_unlock(&mutexLock[key1]);
    pthread_mutex_unlock(&mutexLock[key2]);
}

// make a function fetch that adds the value of the key if present to the string str
void fetch(int key, string& str)
{
    pthread_mutex_lock(&mutexLock[key]);
    if (!dictionary.count(key))
        str += "Key does not exist";
    else
        str += dictionary[key];
    pthread_mutex_unlock(&mutexLock[key]);
}

// use multiple threads to handle clients
void server(int sockfd)
{
	// read the message from client and copy it in buffer
    char buffer[1024] = {0};
	read(sockfd, buffer, 1024);

    string str = "";
    vector<string> tokens;

    // convert buff to a string
    stringstream temp(buffer);
    string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' '))
        tokens.push_back(intermediate);

    str += to_string(pthread_self()) + ":";
    int nParts = tokens.size();

    if (tokens.size() > 1)
        if (tokens[1] == "insert")
            if (nParts == 4)
                insert(tokens[3], stoi(tokens[2]), str);
            else
                str += "Incorrect number of arguments";
        else if (tokens[1] == "delete")
            if (nParts == 3)
                Delete(stoi(tokens[2]), str);
            else
                str += "Incorrect number of arguments";
        else if (tokens[1] == "concat")
            if (nParts == 4)
                concat(stoi(tokens[2]), stoi(tokens[3]), str);
            else
                str += "Incorrect number of arguments";
        else if (tokens[1] == "update")
            if (nParts == 4)
                update(tokens[3], stoi(tokens[2]), str);
            else
                str += "Incorrect number of arguments";
        else if (tokens[1] == "fetch")
            if (nParts == 3)
                fetch(stoi(tokens[2]), str);
            else
                str += "Incorrect number of arguments";
        else
            str += "Incorrect command";
    else
        str += "Incorrect number of arguments";

    write(sockfd, str.c_str(), str.length());
}

void *worker_thread(void *arg)
{
	while (1)
	{
		pthread_mutex_lock(&lock);
		while (!Socket.size())
			pthread_cond_wait(&cond, &lock);

        int sockfd = Socket.front();
		Socket.pop();

		pthread_mutex_unlock(&lock);
		server(sockfd);
		close(sockfd);
	}
	return NULL;
}

void client_search()
{
	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		exit(0);

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);

	if ((bind(sockfd, (SockAddress *)&servaddr, sizeof(servaddr))))
		exit(0);

	if ((listen(sockfd, 5)))
		exit(0);

	socklen_t len = sizeof(cli);

	while (true)
	{
		connfd = accept(sockfd, (SockAddress *)&cli, &len);
		if (connfd < 0)
			exit(0);

		pthread_mutex_lock(&lock);
        Socket.push(connfd);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
	}
}

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		int i, nWorkerThreads = atoi(argv[1]);
        vector<pthread_t> worker_threads(nWorkerThreads);

		for (i = 0; i < nWorkerThreads; i++)
			pthread_create(&worker_threads[i], NULL, worker_thread, NULL);

		client_search();
		for (i = 0; i < nWorkerThreads; i++)
			pthread_join(worker_threads[i], NULL);
	}
	else
        cout << "Missing number of threads as argument";
	return 0;
}

//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q3/client.cpp ####################//

#include <cstdio>
#include <netdb.h>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

using namespace std;

constexpr int MAXBUFFERSIZE = 1024;
constexpr int PORT = 8080;

class Data
{
public:
    Data(int client_id, string command)
        : client_id(client_id), command(command) {}

    Data() = default;

	int client_id;
	string command;
};

void func(int sockfd, Data& data)
{
	write(sockfd, data.command.c_str(), data.command.length());
	char buff[MAXBUFFERSIZE] = {0};
	read(sockfd, buff, MAXBUFFERSIZE);

    cout << data.client_id << ": " << buff << endl;
}

void* client(void* args)
{
    // Tokenize string and store in vector
    vector<string> tokens;

    Data data = *(Data*)args;

    stringstream temp(data.command);
    string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' '))
        tokens.push_back(intermediate);

    // Printing the token vector
	if(tokens.size() < 2)
	{
        cout << "Command doesn't exist\n";
		return NULL;
	}

    // Convert string to integer
    sleep(stoi(tokens[0]));

	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	// socket create and varification
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		exit(0);

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	// connect the client socket to server socket
	if (connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr)) != 0)
		exit(0);

	func(sockfd, data);
	close(sockfd);

    return NULL;
}

int main()
{
    // Input n clients
	int nClients;
    cin >> nClients;
    getchar();

    // Create a thread for each client
    vector<pthread_t> client_threads(nClients);
    vector<Data> datas(nClients);

	for(int i = 0, j = 0; i < nClients; i++, j = 0)
    {
        getline(cin, datas[i].command);
        datas[i].client_id = i;
        pthread_create(&client_threads[i], NULL, client, &datas[i]);
    }

    // wait for all threads to finish
	for(int i = 0; i < nClients; i++)
        pthread_join(client_threads[i], NULL);

	return 0;
}
