
//###########FILE CHANGE ./main_folder/SHREYANSH VERMA_305938_assignsubmission_file_/2019113012/q3/server_prog.cpp ####################//

#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 20
#define PORT_ARG 8002

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;

pair < string,int> map_val[105]; 

queue <int> qu;
int spawn_check_var = 0;
pthread_mutex_t spawn_check = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t qu_handler = PTHREAD_MUTEX_INITIALIZER;
pthread_t worker_threads[105];
pthread_mutex_t key_handler[105];
sem_t worker_thread_sem;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // cout<<"Output = "<<output<<'\n';
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

bool check_num(string &str)
{
    bool flag = 0;
    for(int i=0;i<str.size();i++)
    {
        if(str[i]<'0'||str[i]>'9')
        {
            flag = 1;
        }
    }
    return flag;
}

void* handle_connection(void *)
{
   
    while(1)
    {
        vector <string> vec;
        sem_wait(&worker_thread_sem); // Worker threads waiting on a mutex
        int received_num, sent_num;
        int ret_val = 1;
        pthread_mutex_lock(&qu_handler);
        if(qu.empty())
        {
            pthread_mutex_unlock(&qu_handler);
            continue;
        }
        int client_socket_fd = qu.front(); // Taking out file descriptor out of the queue.
        qu.pop();
        pthread_mutex_unlock(&qu_handler);
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz); //Retrieving the message sent from the client.
        ret_val = received_num;

        if (ret_val <= 0)
        {
            printf("Server could not read msg sent from client\n");
            close(client_socket_fd);
            return NULL;
        }
        string tempr = "";
        for(int i=0;i<cmd.length();i++) //Parsing the received message
        {
            if(cmd[i]==' ')
            {
                if(tempr.size())
                vec.pb(tempr);
                tempr="";
                continue;
            }
            tempr+=cmd[i];
        }
        if(tempr.size())
        {
            vec.pb(tempr);
        }
        // for(int i=0;i<vec.size();i++)
        // {
        //     cout<<"Check\n";
        //     cout<<"vec = "<<vec[i]<<'\n';
        // }
        string msg_to_send_back="";
        uint a = pthread_self();
        string thread_name = to_string(a); //Obtaining the thread ID
        thread_name+=":";
        msg_to_send_back+=thread_name;
        if(vec[0]=="insert") //Checking the given operations and also error handling
        {
            int flag_correct=0;
            if(vec.size()!=3)
            {
                flag_correct = 1;
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[1]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct)
            {
                 msg_to_send_back+="Incorrect parameters";
            }
            else
            {
                int num1 = atoi(vec[1].c_str());
                pthread_mutex_lock(&key_handler[num1]);
                if(map_val[num1].second==0)
                {
                    msg_to_send_back+="Insertion Sucessful";
                    map_val[num1].first = vec[2];
                    map_val[num1].second = 1;
                }
                else
                {
                    msg_to_send_back+="Key already exists";
                }
                pthread_mutex_unlock(&key_handler[num1]);
            }
        }
        else if (vec[0]=="delete")
        {
            int flag_correct = 0;
            if(vec.size()!=2)
            {
                flag_correct = 1;
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[1]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct)
            {
                 msg_to_send_back+="Incorrect parameters";
            }
            else
            {
                int num1 = atoi(vec[1].c_str());
                pthread_mutex_lock(&key_handler[num1]);
                if(map_val[num1].second==1)
                {
                    msg_to_send_back+="Deletion successful";
                    map_val[num1].first = "";
                    map_val[num1].second = 0;
                }
                else
                {
                    msg_to_send_back+="No such key exists";
                }
                pthread_mutex_unlock(&key_handler[num1]);
            }
        }
        else if(vec[0]=="update")
        {
            int flag_correct=0;
            if(vec.size()!=3)
            {
                flag_correct = 1;
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[1]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct)
            {
                 msg_to_send_back+="Incorrect parameters";
            }
            else
            {
                int num1 = atoi(vec[1].c_str());
                pthread_mutex_lock(&key_handler[num1]);
                if(map_val[num1].second==1)
                {
                    msg_to_send_back+=vec[2];
                    map_val[num1].first = vec[2];
                }
                else
                {
                    msg_to_send_back+="Key does not exist";
                }
                pthread_mutex_unlock(&key_handler[num1]);
            }
        }
        else if(vec[0]=="concat")
        {
            int flag_correct=0;
            if(vec.size()!=3)
            {
                flag_correct = 1;
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[1]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[2]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct)
            {
                 msg_to_send_back+="Incorrect parameters";
            }
            else
            {
                int num1 = atoi(vec[1].c_str());
                int num2 = atoi(vec[2].c_str());
                int key1,key2,smol,larg;
                bool flag = 0;
                smol = min(num1,num2);
                larg = max(num1,num2);
                pthread_mutex_lock(&key_handler[smol]);
                pthread_mutex_lock(&key_handler[larg]);
                if(map_val[num1].second==0)
                {
                    flag=1;
                }
                if(map_val[num2].second==0)
                {
                    flag=1;
                }
                if(flag)
                {
                    msg_to_send_back+="Concat failed as at least one of the keys does not exist";
                }
                else
                {
                    string str1,str2;
                    str1 = map_val[num1].first;
                    str2 = map_val[num2].first;
                    map_val[num1].first = str1+str2;
                    map_val[num2].first = str2+str1;
                    msg_to_send_back+=map_val[num2].first;
                }
                pthread_mutex_unlock(&key_handler[larg]);
                pthread_mutex_unlock(&key_handler[smol]);
            }
            
        }
        else if(vec[0]=="fetch")
        {
            int flag_correct=0;
            if(vec.size()!=2)
            {
                flag_correct = 1;
            }
            if(flag_correct!=1)
            {
                if(check_num(vec[1]))
                {
                    flag_correct = 1;
                }
            }
            if(flag_correct)
            {
                 msg_to_send_back+="Incorrect parameters";
            }
            else
            {
                int num1 = atoi(vec[1].c_str());
                pthread_mutex_lock(&key_handler[num1]);
                if(map_val[num1].second==1)
                {
                    msg_to_send_back+=map_val[num1].first;
                }
                else
                {
                    msg_to_send_back+="Key does not exist";
                }
                pthread_mutex_unlock(&key_handler[num1]);
            }
        }
        else
        {
            msg_to_send_back+="Invalid request";
        }
        cout << "Client sent : " << cmd << endl;
        // msg_to_send_back+='\n';
        // if(cmd==)


        
        vec.clear();
        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back); // Sending the result back to the client.
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
        }
        close(client_socket_fd);


    }
    return NULL;

}

int main(int argc, char *argv[])
{

    int i, j, k, t, n,m;
    m = atoi(argv[1]);
    cout<<"M = "<<m<<'\n';
    cout<<"Wait till worker thread initializes\n";
    // cin>>m;
    sem_init(&worker_thread_sem, 0, 0);
    for(int i=0;i<=100;i++)
    {
        key_handler[i] = PTHREAD_MUTEX_INITIALIZER; //Initializing mutex to handle map
        map_val[i] = {"",0};
    }
    for(int i=0;i<m;i++)
    {
        pthread_create(&worker_threads[i],NULL,handle_connection,NULL);
    }
    sleep(5); // Wait while worker threads are being initialized
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number);
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    while (1)
    {

        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        pthread_mutex_lock(&qu_handler);
            if (client_socket_fd < 0)
            {
                perror("ERROR while accept() system call occurred in SERVER");
                exit(-1);
            }
            printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        qu.push(client_socket_fd); // Given file descriptors are put into the queue.
        sem_post(&worker_thread_sem); // Worker threads waiting on the semaphore are signalled.
        pthread_mutex_unlock(&qu_handler);
        // handle_connection();
    }

    close(wel_socket_fd);
    return 0;
}
//###########FILE CHANGE ./main_folder/SHREYANSH VERMA_305938_assignsubmission_file_/2019113012/q3/client_sim.cpp ####################//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8002
////////////////////////////////////

const LL buff_sz = 1048576;

// pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;

vector < pair <int,string> > vec;
pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;
///////////////////////////////////////////////////

string str_err_1 = "Insertion Sucessful!";
string str_err_2 = "Key already exists!";
string str_err_3 = "No such key exists!";
string str_err_4 = "Deletion Sucessful!";
string str_err_5 = "Concat failed as at least one of the keys does not exist!";


pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    return socket_fd;
}

void * begin_process(void * ptr)
{
    long long int indx = (long long int)ptr;
    sleep(vec[indx].first);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    // cout<<"indx = "<<indx<<'\n';
    // cout << "Connection to server successful" << endl;
    string to_send;
    // cout << "Enter msg: "<<vec[indx].second;
    // temp = vec[indx].second;
    string temp = vec[indx].second;
    // cout<<"Temp = "<<temp<<'\n';
    send_string_on_socket(socket_fd, temp);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&print_lock);
    cout<<indx<<":"<< output_msg << '\n';
    pthread_mutex_unlock(&print_lock);
    fflush(stdout);
    // cout << "====" << endl;
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n,m;
    pthread_t client_thread[1000];
    char str[500];
    string val2;
    int val1;
    cin>>m;
    for(int i=0;i<m;i++)
    {
        cin>>val1;
        cin.getline(str,100);
        vec.pb({val1,str});


    }
    // cout<<"reach here\n";
    for(int i=0;i<m;i++)
    {
        // cout<<"Inside]n";
        pthread_create(&client_thread[i],NULL,begin_process,(void *)(long long int)i);
    }
    for(int i=0;i<m;i++)
    {
        pthread_join(client_thread[i],NULL);
    }
    // begin_process();
    return 0;
}