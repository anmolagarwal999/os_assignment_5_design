
//###########FILE CHANGE ./main_folder/Swetha Vipparla_305815_assignsubmission_file_/Assignment 5/q3/server.cpp ####################//

#include <cstdio>
#include <iostream>
#include <sstream>
#include <netdb.h>
#include <vector>
#include <string>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>
#include <semaphore.h>

using namespace std;

#define SERVER_PORT 8001
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

map<int, string> Dict;
queue<int> Socket;
vector<pthread_mutex_t> mutexLock(100, lock);

#define create(a, b) pthread_create(&a, NULL, b, NULL)
#define join(a) pthread_join(a, NULL)
#define pb push_back
#define mLock(key) &mutexLock[key]

void insert(string value, int key, string& str)
{
    pthread_mutex_lock(mLock(key));

    if (!Dict.count(key))
	{
		Dict[key] = value;
        str += "Insertion Successful";
	}
    else
        str += "Key already exists";

    pthread_mutex_unlock(mLock(key));
}

int error(int key, string& str)
{
    if (!Dict.count(key))
    {
        str += "No such key exists";
        return 1;
    }
    return 0;
}

void update(string value, int key, string& str)
{
    pthread_mutex_lock(mLock(key));
	if (!error(key, str))
	{
		Dict[key] = value;
        str += value;
	}
    pthread_mutex_unlock(mLock(key));
}

void del(int key, string& str)
{
    pthread_mutex_lock(mLock(key));

    if (!error(key, str))
    {
        Dict.erase(key);
        str += "Deletion Successful";
	}

    pthread_mutex_unlock(mLock(key));
}

void concat(int key1, int key2, string& str)
{
    pthread_mutex_lock(mLock(key1));
    pthread_mutex_lock(mLock(key2));

    if (Dict.count(key1) == 0 || Dict.count(key2) == 0)
        str += "Concat failed as a least one of the keys does not exist";
    else
    {
        string val1 = Dict[key1];
        string val2 = Dict[key2];
        Dict[key1] += val2;
        Dict[key2] += val1;
        str += val2 + val1;
    }

    pthread_mutex_unlock(mLock(key1));
    pthread_mutex_unlock(mLock(key2));
}

// if the key is present, then add it to the string
void fetch(int key, string& str)
{
    pthread_mutex_lock(mLock(key));

    if (!error(key, str))
        str += Dict[key];

    pthread_mutex_unlock(mLock(key));
}

#define prnt(str) str+="Too many arguments"

void *worker_thread(void *arg)
{
	for(;;)
	{
		pthread_mutex_lock(&lock);
		while (Socket.size() == 0)
			pthread_cond_wait(&cond, &lock); // wait if the queue is empty

        int sockfd = Socket.front();
		Socket.pop();

		pthread_mutex_unlock(&lock);
		
        // store the message read from the client in a buffer and multiple threads are used to handle the clients
        char buffer[1024] = {0};
        read(sockfd, buffer, 1024);

        string str = "";
        vector<string> tokens;

        // convert the buffer to a string
        stringstream temp(buffer);
        string intr;

        // Tokenizing the string by space
        while(getline(temp, intr, ' '))
            tokens.pb(intr);

        str += to_string(pthread_self()) + ":"; // add the thread id to the string
        int numParts ;

        if ((numParts = tokens.size()) < 1)
            prnt(str);
        else
        {
            int t2 = stoi(tokens[2]);

            if (tokens[1] == "insert")
            {
                if (numParts == 4)
                    insert(tokens[3], t2, str);
                else
                    prnt(str);
            }
            else if (tokens[1] == "concat")
            {
                if (numParts == 4)
                    concat(t2, stoi(tokens[3]), str);
                else
                    prnt(str);
            }
            else if (tokens[1] == "delete")
            {
                if (numParts == 3)
                    del(t2, str);
                else
                    prnt(str);
            }
            else if (tokens[1] == "fetch")
            {
                if (numParts == 3)
                    fetch(t2, str);
                else
                    prnt(str);
            }
            else if (tokens[1] == "update")
            {
                if (numParts == 4)
                    update(tokens[3], t2, str);
                else
                    prnt(str);
            }
            else
                str += "Incorrect command";
        }   

        write(sockfd, str.c_str(), str.length());

		close(sockfd);
	}
	return NULL;
}

void searchClient()
{
	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) // create socket
		exit(0);

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERVER_PORT);

    bool cond1 = bind(sockfd, (sockaddr *)&servaddr, sizeof(servaddr));
    bool cond2 = listen(sockfd, 5);

	if (cond1 || cond2)
		exit(0);

	socklen_t len = sizeof(cli);

	for(;;)
	{
		connfd = accept(sockfd, (sockaddr *)&cli, &len); 

		if (connfd > 0)
        {
            pthread_mutex_lock(&lock);
            Socket.push(connfd);
            pthread_mutex_unlock(&lock);
            pthread_cond_signal(&cond);
        }
		else
            exit(0);
	}
}

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		int i = -1, numThreads = atoi(argv[1]);
        vector<pthread_t> workThreads(numThreads);

        while(++i < numThreads)
			create(workThreads[i], worker_thread);

		searchClient();

        i = -1;

        // wait for all the threads to terminate
		while (++i < numThreads)
			join(workThreads[i]);
	}
	else
        cout << "Missing number of threads as argument";
	return 0;
}








//###########FILE CHANGE ./main_folder/Swetha Vipparla_305815_assignsubmission_file_/Assignment 5/q3/client.cpp ####################//

#include <cstdio>
#include <netdb.h>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include <semaphore.h>

using namespace std;

#define SERVER_PORT 8001
#define ip "127.0.0.1"
#define create(x, y) pthread_create(&x, NULL, client, &y)
#define join(x) pthread_join(x, NULL)
#define pb push_back

class ClientReq
{
public:
    ClientReq(int clientID, string command) : clientID(clientID), command(command)
    {
    }

    ClientReq() = default;

    int clientID;
    string command;
};

void *client(void *args)
{
    vector<string> tokens;

    ClientReq data = *(ClientReq *)args;

    stringstream temp(data.command);
    string buf;

    // Tokenizing the string using space as delimiter and store it in a vector
    while (getline(temp, buf, ' '))
        tokens.pb(buf);

    if (tokens.size() > 2)
    {
        sleep(stoi(tokens[0]));

        int sockfd;
        sockaddr_in servaddr, cli;

        // create and verify socket
        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
            exit(0);

        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = inet_addr(ip);
        servaddr.sin_port = htons(SERVER_PORT);

        // connect the client socket and server socket
        if (connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr)))
            exit(0);

        write(sockfd, data.command.c_str(), data.command.length());

        char buff[1024] = {0};
        read(sockfd, buff, 1024);

        cout << data.clientID << ": " << buff << endl;

        close(sockfd);
    }

    else
        cout << "Command doesn't exist\n";

    return NULL;
}

int main()
{
    int numClients, i = -1;

    cin >> numClients;
    getchar(); // remove the newline character

    // thread for each client
    vector<pthread_t> clientThreads(numClients);

    // array to store all requests
    vector<ClientReq> datas(numClients);

    while (++i < numClients)
    {
        getline(cin, datas[i].command);
        datas[i].clientID = i;
        create(clientThreads[i], datas[i]);
    }

    i = -1;

    // wait for all threads to finish
    while (++i < numClients)
        join(clientThreads[i]);

    return 0;
}