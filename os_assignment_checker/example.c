

void *spectator_empty_e_stage(void *ptr2)
{
    int id = *((int *)ptr2);

    //get details of the spectator
    struct spectator *ptr = perf_ptr[id];

    bool found_stage = false;

    //find patience time of the spectator
    struct timespec *st = perf_ptr[id]->st_leave;

    //wait for WAITING TIME of the spectator (THIS CAN BE REPLACED by sleep(x) as well)
    sem_timedwait(&rogue_sem, perf_ptr[id]->st_arrival);


    ///////////////////////////////////////////
    //change status of spectator from "UNARRIVED" to "arrived and waiting for a SEAT"
    //a mutex is needed to make sure that just one thread does this
    pthread_mutex_lock(&ptr->mutex);
    if (ptr->curr_stat == Unarrived)
    {
        ptr->curr_stat = Waiting;
        printf(BYEL "spectator %s has arrived TO THE STADIUM %c\n" ANSI_RESET, perf_ptr[id]->name, perf_ptr[id]->instrument_id);
    }
    pthread_mutex_unlock(&ptr->mutex);

    ///////////////////////////////////////////

    // printf("Entered searching for elctric stage\n");
    int ret2;

block2:
    found_stage = false;
    //WAIT on the ZONE semaphore with TIMED WAIT worth "patience time"
    ret2 = sem_timedwait(&sem_empty_e, st);

    if (ret2 == 0)
    {

        //A SEAT IS AVAILABLE FOR THE SPECTATOR.
        //#################################3
        //BUT WE CANNOT BE SURE THAT THE SPECTATOR WILL TAKE THE SEAT 
        //as it is possible that one of his other THREADS has ALSO FOUND HIM A SEAT
        //#######################################


        //unnecessary  but still -> preliminary check-> acquire mutex to make sure that status does not change while reading
        pthread_mutex_lock(&ptr->mutex);

        //preliminary check
        //make sure that SPECTATOR has still not got a seat ie one of his other threads has not been fruitful yet
        if (ptr->curr_stat != Waiting)
        {
            //SOME OTHER THREAD was able to secure a seat for the spectator
            //No need of this thread any longer
            //Neceassry to unlock as other redundant thread may need it

            //increment semapore for false alarm
            // printf(ANSI_RED "FALSE ALARM . I AM PRIVILEGED status is %d\n" ANSI_RESET, ptr->curr_stat);
            sem_post(&sem_empty_e);
            pthread_mutex_unlock(&ptr->mutex);
            return NULL;
        }

        //assign a seat and change status
        //needs to be done while LOCK is HELD
        found_stage=true;
        pthread_mutex_unlock(&ptr->mutex);

        
        if (!found_stage)
        {
            printf("Extremely BAD SHIT HAS HAPPENED -> Red John\n");
            //exit(0);

            //goto block2->redundant but keep for future debugging
            goto block2;
        }
        else
        {
            // printf("I have been able to find a stage\n");
            give_leader_performance(id);
        }
    }
    else
    {
        //SEMAPHORE was relinquished SINCE TIMER HAD EXPIRED
        //display that he left as he was impatient
        pthread_mutex_lock(&ptr->mutex);
        //still check if still waiting, change to leftover and display message
        if (ptr->curr_stat == Waiting)
        {
            ptr->curr_stat = Left_show;
            printf(BCYN "spectator %s became impatient and left the show\n" ANSI_RESET, perf_ptr[id]->name);
        }
        //if not, then either some other thread got a stage for spectator or the other thread already left
        //nothing to be done, just leave
        pthread_mutex_unlock(&ptr->mutex);
    }

    return NULL;
}