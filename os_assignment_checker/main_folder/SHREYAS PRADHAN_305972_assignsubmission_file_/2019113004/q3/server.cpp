#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
#include <semaphore.h>
#include <vector>
#include <queue>
#include <sstream>
#include <thread>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;

sem_t thread_sem;
pthread_mutex_t client_lock;

vector<pthread_t> workers;
queue<int> clients;
vector<string> dictionary(110);
vector<pthread_mutex_t> dictionary_lock(110);

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    // printf("%d handling %d\n", worker_id, client_socket_fd);

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }

        vector<string> words;
        string word = "";

        int str_sz = cmd.size();
        for (int i = 0; i < str_sz; i++)
        {
            if (cmd[i] == ' ')
            {
                words.push_back(word);
                word = "";
            }
            else
            {
                word = word + cmd[i];
            }
        }
        words.push_back(word);

        string msg_to_send_back;

        string operation = words[0];

        if (operation == "insert")
        {
            int key = stoi(words[1]);
            string value = words[2];

            pthread_mutex_lock(&dictionary_lock[key]);

            int dict_size = dictionary[key].size();
            if (dict_size > 0)
            {
                msg_to_send_back = "Key already exists";
            }
            else
            {
                msg_to_send_back = "Insertion successful";
                dictionary[key] = value;
            }

            pthread_mutex_unlock(&dictionary_lock[key]);
        }

        if (operation == "delete")
        {
            int key = stoi(words[1]);

            pthread_mutex_lock(&dictionary_lock[key]);

            int dict_size = dictionary[key].size();
            if (dict_size == 0)
            {
                msg_to_send_back = "No such key exists";
            }
            else
            {
                msg_to_send_back = "Deletion successful";
                dictionary[key] = "";
            }

            pthread_mutex_unlock(&dictionary_lock[key]);
        }

        if (operation == "update")
        {
            int key = stoi(words[1]);
            string value = words[2];

            pthread_mutex_lock(&dictionary_lock[key]);

            int dict_size = dictionary[key].size();
            if (dict_size == 0)
            {
                msg_to_send_back = "Key does not exist";
            }
            else
            {
                dictionary[key] = value;
                msg_to_send_back = dictionary[key];
            }

            pthread_mutex_unlock(&dictionary_lock[key]);
        }

        if (operation == "fetch")
        {
            int key = stoi(words[1]);

            pthread_mutex_lock(&dictionary_lock[key]);

            int dict_size = dictionary[key].size();
            if (dict_size == 0)
            {
                msg_to_send_back = "Key does not exist";
            }
            else
            {
                msg_to_send_back = dictionary[key];
            }

            pthread_mutex_unlock(&dictionary_lock[key]);
        }

        if (operation == "concat")
        {
            int key_1 = stoi(words[1]);
            int key_2 = stoi(words[2]);

            string value_1 = dictionary[key_1];
            string value_2 = dictionary[key_2];

            int sz_1 = value_1.size();
            int sz_2 = value_2.size();

            if (sz_1 * sz_2 == 0)
            {
                msg_to_send_back = "Concat failed as at least one of the keys does not exist";
            }
            else
            {
                pthread_mutex_lock(&dictionary_lock[key_1]);
                dictionary[key_1] = value_1 + value_2;
                pthread_mutex_unlock(&dictionary_lock[key_1]);

                pthread_mutex_lock(&dictionary_lock[key_2]);
                dictionary[key_2] = value_2 + value_1;
                pthread_mutex_unlock(&dictionary_lock[key_2]);

                msg_to_send_back = dictionary[key_2];
            }
        }

        sleep(2);

        msg_to_send_back = ":" + to_string(pthread_self()) + ":" + msg_to_send_back;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *worker(void *args)
{

    while (1)
    {
        sem_wait(&thread_sem);

        pthread_mutex_lock(&client_lock);

        int client_socket_fd = -1;

        int clients_sz = clients.size();
        if (clients_sz > 0)
        {
            client_socket_fd = clients.front();
            clients.pop();
        }
        pthread_mutex_unlock(&client_lock);

        handle_connection(client_socket_fd);
    }

    return NULL;
}

void init(int n_threads)
{

    // locks
    for (int i = 0; i < 110; i++)
    {
        dictionary[i] = "";
        pthread_mutex_init(&dictionary_lock[i], NULL);
    }
    pthread_mutex_init(&client_lock, NULL);

    sem_init(&thread_sem, 0, 0);

    // setting number of threads
    workers.resize(n_threads);
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    int n_threads = atoi(argv[1]);
    init(n_threads);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    for (int i = 0; i < n_threads; i++)
    {
        pthread_create(&workers[i], NULL, worker, NULL);
    }

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        pthread_mutex_lock(&client_lock);
        clients.push(client_socket_fd);
        pthread_mutex_unlock(&client_lock);

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        sem_post(&thread_sem);
    }

    close(wel_socket_fd);

    return 0;
}