#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>

#define TUT_TIME 5

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define DEBUG 0

typedef struct course
{
    int id;
    char name[25];
    int course_max_slots;
    float interest;
    int n_labs;
    int *labs;
    int ta;
    int withdrawn;

    pthread_mutex_t course_lock;

} course;

typedef struct lab
{
    int id;
    char name[20];
    int n_tas;
    int max_times;

    // TA section - each array is of size n_tas
    int *taught_courses;
    int *is_free;

    pthread_mutex_t *ta_lock;
    pthread_cond_t *ta_cond; // so courses can wait for a TA.

} lab;

typedef struct student
{
    int id;
    float reg_time;
    int pref[3];
    int cur_pref;
    float calibre;
    int is_waiting;

    pthread_mutex_t student_lock;
    pthread_cond_t student_cond; // so students can wait for a course

} student;

int n_students;
student *students;
pthread_t *student_threads;
int students_finished;
pthread_mutex_t students_lock;

int n_courses;
course *courses;
pthread_t *course_threads;

int n_labs;
lab *ta_labs;

// aux variables
pthread_mutex_t print_lock;

void init();
void *course_thread(void *args);
void *student_thread(void *args);
