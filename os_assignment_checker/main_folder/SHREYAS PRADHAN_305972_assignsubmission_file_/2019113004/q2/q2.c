#include "q2_header.h"

int main()
{
    // take input and initialise stuff
    init();

    if (DEBUG)
    {
        for (int i = 0; i < total_spectator_threads; i++)
        {
            printf("%d %c\n", i, ind_to_char_team(spectator_thread_inputs[i].zone));
        }
    }

    if (DEBUG)
    {
        for (int i = 0; i < n_goal_events; i++)
        {
            printf("%d %d\n", i, goal_events[i].id);
        }
    }

    if (DEBUG)
    {
        for (int i = 0; i < n_spectators; i++)
        {
            printf("%d %d\n", i, spectators[i].id);
        }
    }

    // start goal threads
    for (int i = 0; i < n_goal_events; i++)
    {
        pthread_create(&goal_threads[i], NULL, goal_thread, &goal_events[i]);
    }

    // spectator threads
    for (int i = 0; i < total_spectator_threads; i++)
    {
        pthread_create(&spectator_threads[i], NULL, spectator_thread, &spectator_thread_inputs[i]);
    }

    // waiting for threads to finish
    for (int i = 0; i < n_goal_events; i++)
    {
        pthread_join(goal_threads[i], NULL);
    }
    for (int i = 0; i < total_spectator_threads; i++)
    {
        pthread_join(spectator_threads[i], NULL);
    }

    return 0;
}

void *spectator_thread(void *args)
{
    spectator_thread_input spec_inp = *(spectator_thread_input *)args;

    spectator cur_spec = spec_inp.spec;
    int cur_zone = spec_inp.zone;

    sem_t *s;
    if (cur_zone == HOME)
    {
        s = &sem_h;
    }
    else if (cur_zone == AWAY)
    {
        s = &sem_h;
    }
    else
    {
        s = &sem_n;
    }

    // sleep till reach time
    sleep(cur_spec.reach_time);

    if (!DEBUG)
    {
        pthread_mutex_lock(&print_lock);
        printf(BLUE "Person %s has reached the stadium\n" RESET, spectators[cur_spec.id].name);
        pthread_mutex_unlock(&print_lock);
    }

    // start waiting for seat
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        perror("clock_gettime");
        exit(EXIT_FAILURE);
    }
    ts.tv_sec += cur_spec.patience;

    int wait_res;
    while ((wait_res = sem_timedwait(s, &ts)) == -1 && errno == EINTR)
        continue;

    if (wait_res == -1)
    {
        // lost patience so exit

        pthread_mutex_lock(&spectators[cur_spec.id].lock);
        spectators[cur_spec.id].left++;
        int req;
        if (spectators[cur_spec.id].type == HOME)
            req = 2;
        if (spectators[cur_spec.id].type == AWAY)
            req = 1;
        if (spectators[cur_spec.id].type == NEUTRAL)
            req = 3;

        if (!DEBUG && spectators[cur_spec.id].left == req)
        {
            pthread_mutex_lock(&print_lock);
            printf(YELLOW "Person %s couldn’t get a seat\n" RESET, spectators[cur_spec.id].name);
            pthread_mutex_unlock(&print_lock);
        }

        pthread_mutex_unlock(&spectators[cur_spec.id].lock);

        return NULL;
    }

    // got a seat
    pthread_mutex_lock(&spectators[cur_spec.id].lock);
    if (!spectators[cur_spec.id].got_seat)
    {
        spectators[cur_spec.id].got_seat = 1;
    }
    else
    {
        // got a seat in another zone
        pthread_mutex_unlock(&spectators[cur_spec.id].lock);
        sem_post(s);
        return NULL;
    }
    pthread_mutex_unlock(&spectators[cur_spec.id].lock);

    // spectator has had a seat in this zone

    if (!DEBUG)
    {
        pthread_mutex_lock(&print_lock);
        printf(CYAN "Person %s has got a seat in zone %c\n" RESET, spectators[cur_spec.id].name, ind_to_char_team(cur_zone));
        pthread_mutex_unlock(&print_lock);
    }

    // wait for goals or max wait time
    struct timespec to;
    int cond_wait_res;
    if (cur_zone == HOME || cur_zone == AWAY)
    {
        if (clock_gettime(CLOCK_REALTIME, &to) == -1)
        {
            perror("clock_gettime");
            exit(EXIT_FAILURE);
        }
        to.tv_sec += max_spec_time;
    }

    int is_time_up = 0;
    // wait for goals
    if (cur_zone == HOME)
    {
        pthread_mutex_lock(&goal_lock);
        while (goals_a <= cur_spec.goal_limit)
        {
            cond_wait_res = pthread_cond_timedwait(&r_goals_a[cur_spec.goal_limit], &goal_lock, &to);
            if (cond_wait_res == ETIMEDOUT)
            {
                // reached max time limit
                is_time_up = 1;
                break;
            }
        }
        pthread_mutex_unlock(&goal_lock);
    }
    else if (cur_zone == AWAY)
    {
        pthread_mutex_lock(&goal_lock);
        while (goals_h <= cur_spec.goal_limit)
        {
            cond_wait_res = pthread_cond_timedwait(&r_goals_h[cur_spec.goal_limit], &goal_lock, &to);
            if (cond_wait_res == ETIMEDOUT)
            {
                // reached max time limit
                is_time_up = 1;
                break;
            }
        }
        pthread_mutex_unlock(&goal_lock);
    }
    else
    {
        // neutral fans simply wait for X time
        sleep(max_spec_time);
        is_time_up = 1;
    }

    // either goal limit has been reached or max spectation limit is reached
    // either way, spectator will now exit

    if (is_time_up)
    {
        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            printf(RED "Person %s watched the match for %d seconds and is leaving\n" RESET, spectators[cur_spec.id].name, max_spec_time);
            pthread_mutex_unlock(&print_lock);
        }
    }
    else
    {
        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            printf(GREEN "Person %s is leaving due to the bad defensive performance of his team\n" RESET, spectators[cur_spec.id].name);
            pthread_mutex_unlock(&print_lock);
        }
    }

    sem_post(s);
    return NULL;
}

void *goal_thread(void *args)
{
    goal_event cur_goal = *(goal_event *)args;

    sleep(cur_goal.goal_time);

    float outcome = (float)rand() / (float)(RAND_MAX);

    if (outcome <= cur_goal.p)
    {
        pthread_mutex_lock(&goal_lock);
        // goal success
        if (cur_goal.team == HOME)
        {
            goals_h++;
            pthread_cond_broadcast(&r_goals_h[goals_h]);
        }
        else
        {
            goals_a++;
            pthread_cond_broadcast(&r_goals_a[goals_a]);
        }
        pthread_mutex_unlock(&goal_lock);

        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            printf(GREEN "Team %c have scored their %dth goal\n" RESET, ind_to_char_team(cur_goal.team), (cur_goal.team == HOME ? goals_h : goals_a));
            pthread_mutex_unlock(&print_lock);
        }
    }
    else
    {
        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            printf(RED "Team %c missed the chance to score their %dth goal\n" RESET, ind_to_char_team(cur_goal.team), (cur_goal.team == HOME ? goals_h : goals_a));
            pthread_mutex_unlock(&print_lock);
        }
    }

    return NULL;
}

void init()
{
    // input

    scanf("%d %d %d %d %d", &cap_h, &cap_a, &cap_n, &max_spec_time, &n_groups);

    // groups
    spectators = malloc(sizeof(spectator) * 1);
    n_spectators = 0;
    total_spectator_threads = 0;

    for (int i = 0; i < n_groups; i++)
    {
        int group_size;
        scanf("%d", &group_size);
        n_spectators += group_size;

        spectators = realloc(spectators, sizeof(spectator) * n_spectators);
        for (int j = n_spectators - group_size; j < n_spectators; j++)
        {
            char type;
            scanf("%s %c %d %d %d", spectators[j].name, &type, &spectators[j].reach_time, &spectators[j].patience, &spectators[j].goal_limit);

            spectators[j].id = j;
            spectators[j].got_seat = 0;
            spectators[j].left = 0;

            pthread_mutex_init(&spectators[j].lock, NULL);

            if (type == 'H')
            {
                spectators[j].type = HOME;
                total_spectator_threads += 2;
            }
            else if (type == 'A')
            {
                spectators[j].type = AWAY;
                total_spectator_threads += 1;
            }
            else
            {
                spectators[j].type = NEUTRAL;
                total_spectator_threads += 3;
            }
        }
    }

    // the inputs to spectator threads

    spectator_thread_inputs = malloc(sizeof(spectator_thread_input) * total_spectator_threads);
    int threads_counter = 0;
    for (int i = 0; i < n_spectators; i++)
    {
        if (spectators[i].type == HOME || spectators[i].type == NEUTRAL)
        {
            // zone home
            spectator_thread_inputs[threads_counter].spec = spectators[i];
            spectator_thread_inputs[threads_counter].zone = HOME;
            threads_counter++;

            // zone neutral
            spectator_thread_inputs[threads_counter].spec = spectators[i];
            spectator_thread_inputs[threads_counter].zone = NEUTRAL;
            threads_counter++;
        }
        if (spectators[i].type == AWAY || spectators[i].type == NEUTRAL)
        {
            // zone away
            spectator_thread_inputs[threads_counter].spec = spectators[i];
            spectator_thread_inputs[threads_counter].zone = AWAY;
            threads_counter++;
        }
    }

    // goals
    scanf("%d", &n_goal_events);
    goal_events = malloc(sizeof(goal_event) * n_goal_events);

    for (int i = 0; i < n_goal_events; i++)
    {
        char team;
        scanf("%c %d %f", &team, &goal_events[i].goal_time, &goal_events[i].p);

        goal_events[i].id = i;
        goal_events[i].team = (team == 'H' ? HOME : AWAY);
    }

    goals_h = 0;
    goals_a = 0;

    pthread_mutex_init(&goal_lock, NULL);

    // alocating threads
    spectator_threads = malloc(sizeof(pthread_t) * total_spectator_threads);
    goal_threads = malloc(sizeof(pthread_t) * n_goal_events);

    // semaphores
    sem_init(&sem_h, 0, cap_h);
    sem_init(&sem_a, 0, cap_a);
    sem_init(&sem_n, 0, cap_n);

    // condition variables
    r_goals_h = malloc(sizeof(pthread_cond_t) * (n_goal_events + 1));
    r_goals_a = malloc(sizeof(pthread_cond_t) * (n_goal_events + 1));

    for (int i = 0; i < n_goal_events + 1; i++)
    {
        pthread_cond_init(&r_goals_h[i], NULL);
        pthread_cond_init(&r_goals_a[i], NULL);
    }

    // aux
    pthread_mutex_init(&print_lock, NULL);
}

char ind_to_char_team(int ind)
{
    if (ind == HOME)
    {
        return 'H';
    }
    if (ind == AWAY)
    {
        return 'A';
    }
    if (ind == NEUTRAL)
    {
        return 'N';
    }
}