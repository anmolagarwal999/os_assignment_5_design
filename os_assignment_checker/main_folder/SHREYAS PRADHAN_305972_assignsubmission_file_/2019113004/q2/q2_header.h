#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

#define HOME 0
#define AWAY 1
#define NEUTRAL 2

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define DEBUG 0

typedef struct spectator
{
    int id;
    char name[30];
    int type;
    int reach_time;
    int patience;
    int goal_limit;
    int got_seat;
    int left;

    pthread_mutex_t lock;

} spectator;

typedef struct goal_event
{
    int id;

    int team;
    int goal_time;
    float p;

} goal_event;

typedef struct spectator_thread_input
{
    spectator spec;
    int zone;

} spectator_thread_input;

// global variables

int cap_h, cap_a, cap_n;
int max_spec_time;
int n_groups, n_spectators, n_goal_events;
int total_spectator_threads;
int goals_h, goals_a;

spectator *spectators;
goal_event *goal_events;
spectator_thread_input *spectator_thread_inputs;

pthread_t *spectator_threads;
pthread_t *goal_threads;

sem_t sem_h, sem_a, sem_n;
pthread_cond_t *r_goals_h;
pthread_cond_t *r_goals_a;
pthread_mutex_t goal_lock;

// aux variables
pthread_mutex_t print_lock;

// functions

void init();
void *goal_thread(void *args);
void *spectator_thread(void *args);
char ind_to_char_team(int ind);