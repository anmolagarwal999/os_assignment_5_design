#include <iostream>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#define SERVER_PORT 8001
#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define ql queue<ll>
#define vs vector<string>
#define NUM_DICT 100
#define MAXIMUM_CLIENTS 100
ll buffersize = 1e6 + 7;

pthread_mutex_t q_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t q_hasEntries_mutex = PTHREAD_COND_INITIALIZER;
ql socket_queue;
typedef struct node
{
    bool key_pair_exists;
    pthread_mutex_t lock;
    string value;
    int key;

} node;

vector<node> nodes;
extern int errno;
string connection_handler(int);
void to_server(int fd, const string &s);
void request_handler(int id, int client_socket_fd);
int find_key_pair(int key)
{
    for (int i = 0; i < nodes.size(); i++)
    {
        if (nodes[i].key == key)
            return i;
    }
    return -1;
}
int find_empty_node()
{
    for (int i = 0; i < nodes.size(); i++)
    {
        if (nodes[i].key_pair_exists == false)
            return i;
    }
    return -1;
}
void *thread_func(void *(arg))
{
    int id = gettid();
    int client_socket_fd;
    while (true)
    {
        pthread_mutex_lock(&q_lock);
        while (socket_queue.empty())
            pthread_cond_wait(&q_hasEntries_mutex, &q_lock);
        client_socket_fd = socket_queue.front();
        socket_queue.pop();
        pthread_mutex_unlock(&q_lock);
        request_handler(id, client_socket_fd);
    }
    return NULL;
}
void request_handler(int id, int client_socket_fd)
{

    string input_str = connection_handler(client_socket_fd);
    string command, value, key_str;
    int key;
    string output = to_string(id);
    output += ":";
    istringstream ss(input_str);
    ss >> command;
    tuple<int, string> new_node;
    int command_args = 0;
    while (ss)
    {
        if (command_args == 0)
            ss >> key_str;
        else
            ss >> value;
        command_args++;
    }
    if (command == "insert")
    {
        key = stoi(key_str);
        int index = find_key_pair(key);

        if (index == -1)
        {
            node new_node;
            pthread_mutex_init(&new_node.lock, NULL);
            new_node.value = "";
            new_node.key = -1;
            new_node.key_pair_exists = 0;
            nodes.push_back(new_node);
            index = find_empty_node();
            if (index == -1)
                index = nodes.size() - 1;
            pthread_mutex_lock(&nodes[index].lock);
            nodes[index].value = value;
            nodes[index].key_pair_exists = true;
            nodes[index].key = key;
            output += "Insertion successful";
            pthread_mutex_unlock(&nodes[index].lock);
        }
        else
        {
            output += "Key already exists";
        }
    }

    else if (command == "delete")
    {
        key = stoi(key_str);
        int index = find_key_pair(key);
        if (index != -1)
        {
            pthread_mutex_lock(&nodes[index].lock);
            nodes[index].key_pair_exists = false;
            output += "Deletion successful";
            pthread_mutex_unlock(&nodes[index].lock);
        }
        else
        {
            output += "No such key exists";
        }
    }

    else if (command == "update")
    {
        key = stoi(key_str);
        int index = find_key_pair(key);

        if (index != -1)
        {
            pthread_mutex_lock(&nodes[index].lock);
            nodes[index].value = value;
            nodes[index].key_pair_exists = true;
            nodes[index].key = key;
            output += nodes[index].value;
            pthread_mutex_unlock(&nodes[index].lock);
        }
        else
        {
            output += "Key does not exist";
        }
    }
    else if (command == "concat")
    {
        int key1 = stoi(key_str), key2 = stoi(value);
        int index1 = find_key_pair(key1), index2 = find_key_pair(key2);
        if (index1 != -1 && index2 != -1)
        {
            string concatenated_string = "";
            pthread_mutex_lock(&nodes[index1].lock);
            pthread_mutex_lock(&nodes[index2].lock);
            concatenated_string = nodes[index1].value + nodes[index2].value;
            nodes[index1].value = concatenated_string;
            nodes[index2].value = concatenated_string;
            output += nodes[index2].value;
            pthread_mutex_unlock(&nodes[index2].lock);
            pthread_mutex_unlock(&nodes[index1].lock);
        }
        else
        {
            output += "Concat failed as at least one of the keys does not exist";
        }
    }
    else if (command == "fetch")
    {
        key = stoi(key_str);
        int index = find_key_pair(key);

        if (index != -1)
        {
            pthread_mutex_lock(&nodes[index].lock);
            output += nodes[index].value;
            pthread_mutex_unlock(&nodes[index].lock);
        }
        else
        {
            output += "Key does not exist";
        }
    }
    to_server(client_socket_fd, output);
}

int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        printf("Not enough arguments. Aborting!\n");
        exit(-1);
    }

    int thread_num = atoi(argv[1]);
    pthread_t worker_threads[thread_num];

    for (int i = thread_num; i > 0; i--)
        pthread_create(&worker_threads[i], NULL, thread_func, NULL);
    
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));

    int socket_fd_intial = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd_intial < 0)
    {
        printf("Error:%s\n", strerror(errno));
        exit(-1);
    }

    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(SERVER_PORT);

    if (bind(socket_fd_intial, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        printf("Error:%s\n", strerror(errno));
        exit(-1);
    }
    listen(socket_fd_intial, MAXIMUM_CLIENTS);
    clilen = sizeof(client_addr_obj);
    while (1)
    {
        int client_socket_fd = accept(socket_fd_intial, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            printf("Error:%s\n", strerror(errno));
            exit(-1);
        }
        pthread_mutex_lock(&q_lock);
        socket_queue.push(client_socket_fd);
        pthread_mutex_unlock(&q_lock);
        pthread_cond_signal(&q_hasEntries_mutex);
    }

    close(socket_fd_intial);

    return 0;
}
void to_server(int fd, const string &s)
{

    int sent_bytes = write(fd, s.c_str(), s.length());
    if (sent_bytes < 0)
    {
        printf("Error:%s\n", strerror(errno));
    }
}
string connection_handler(int client_socket_fd)
{

    string buffer;
    int fd;
    buffer.resize(buffersize);
    fd = read(client_socket_fd, &buffer[0], buffersize - 1);
    if (fd <= 0)
    {
        printf("Error:%s", strerror(errno));
        return "\0";
    }
    // buffer[fd] = '\0';
    buffer.resize(fd);
    buffer = buffer.c_str();
    return buffer;
}