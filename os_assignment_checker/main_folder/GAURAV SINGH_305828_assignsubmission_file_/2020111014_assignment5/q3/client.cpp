#include <bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#define SERVER_PORT 8001
#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define ql queue<ll>
#define vl vector<ll>
#define vs vector<string>
#define NUM_DICT 100
#define MAXIMUM_CLIENTS 100
ll buffersize = 1e6 + 7;
pthread_mutex_t terminal_lock = PTHREAD_MUTEX_INITIALIZER;
vector<pthread_mutex_t> c_lock;
vs client_command;
vector<pthread_t> thread_pool;
vl c_time;
extern int errno;
//functions
int send_on_socket(int fd, const string &s);
string read_from_socket(int client_socket_fd);
int get_socket_fd();
void *thread_func(void *(arg));


int main(int argc, char *argv[])
{

    string input_lines_str;
    int *index = new int;
    int line_num = 0;
    getline(cin, input_lines_str);
    line_num = stoi(input_lines_str);
    int ip_i = 0;

    for (int i = 0; i < line_num; i++)
    {

        string string_input;
        getline(cin, string_input);

        pthread_mutex_t temp_lock;
        pthread_t temp_thread;
        string temp_client_command;
        int temp_c_time;
        int space_divider = string_input.find(' ');
        string temp_string_time = string_input.substr(0, space_divider);
        temp_c_time = stoi(temp_string_time);
        temp_client_command = string_input.substr(space_divider + 1, string_input.length());

        pthread_mutex_init(&temp_lock, NULL);
        c_lock.push_back(temp_lock);
        client_command.push_back(temp_client_command);
        c_time.push_back(temp_c_time);
        thread_pool.push_back(temp_thread);
    }

    for (int i = 0; i < line_num; i++)
    {
        index = new int;
        *index = i;
        pthread_create(&thread_pool[i], NULL, thread_func, (void *)index);
    }

    for (int i = 0; i < line_num; i++)
    {
        pthread_join(thread_pool[i], NULL);
    }
    return 0;
}
int send_on_socket(int fd, const string &s)
{
    int sent_bytes = write(fd, s.c_str(), s.length());
    if (sent_bytes < 0)
    {
        printf("Error:%s\n", strerror(errno));
        return 0;
    }
    return sent_bytes;
}

string read_from_socket(int client_socket_fd)
{
    string buffer;
    int fd;
    buffer.resize(buffersize);
    fd = read(client_socket_fd, &buffer[0], buffersize - 1);
    if (fd <= 0)
    {
        printf("Error:%s", strerror(errno));
        return "\0";
    }

    buffer.resize(fd);
    buffer = buffer.c_str();
    return buffer;
}

int get_socket_fd()
{
    int socket_v = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_v < 0)
    {
        printf("Error:%s\n", strerror(errno));
        exit(-1);
    }
    return socket_v;
}

void *thread_func(void *(arg))
{
    int index = *(int *)arg;
    int socket_fd = get_socket_fd();

    struct sockaddr_in server_obj;
    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(SERVER_PORT);

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        printf("Error:%s\n", strerror(errno));
        exit(-1);
    }
    sleep(c_time[index]);
    pthread_mutex_lock(&c_lock[index]);
    if (send_on_socket(socket_fd, client_command[index]) < 0)
    {
        printf("Error:%s\n", strerror(errno));
        return NULL;
    }
    pthread_mutex_unlock(&c_lock[index]);
    pthread_mutex_lock(&c_lock[index]);
    string response = read_from_socket(socket_fd);
    pthread_mutex_unlock(&c_lock[index]);
    pthread_mutex_lock(&terminal_lock);
    printf("%d:", index);
    if (response.c_str())
    {
        printf("%s\n", response.c_str());
    }
    pthread_mutex_unlock(&terminal_lock);
    return NULL;
}

