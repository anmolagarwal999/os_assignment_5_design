#ifndef DEFINITIONS
#define DEFINITIONS

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

#define MAX_NO 1000

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

struct Course {
    char name[20];
    float interest;
    int maxslots;
    int numlabs;
    int stucount;
    int* lablist;
    int removeflag; //0 if not removed 1 if removed
};

struct Lab {
    char name[20];
    int TAnum;
    int limit;
    int over;
};

struct Student {
    float calibre;
    int prefid[3];
    char status; // w= waiting, c=in class, f=finished course 
    int time;
    int currentcourse;
    int qflag;
    int dontcheckprob;
    // int pref2id;
    // int pref3id;

};

struct TA {
    int lab_id;
    int ta_id;
    int alloted; //0 if no course has been alloted, 1 if course has been alloted
    int courseno;
    int timesalloted;
    // int lockid;
};

typedef struct Course* course;
typedef struct Lab* lab;
typedef struct Student* stud;
typedef struct TA* ta;

int num_courses;
int num_students;
int num_labs;
int num_tas;

void searchTA(int* lablist, int numlabs, int coursenum);
void tutorial(ta takingTA);
void applycourse(int coursenum, int studindex);
void* coursehandler(void* args);
void* studenthandler(void* args);
void freestudents(int removedcourse);

#endif