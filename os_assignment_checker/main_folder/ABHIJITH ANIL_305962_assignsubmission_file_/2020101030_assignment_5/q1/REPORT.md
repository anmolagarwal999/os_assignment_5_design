# Question 1
Making an unconventional course registration system for the college, where a student can take trial classes of a course and can withdraw and opt for a different course if he/she does not like the course.
- To compile and get the executable, use the makefile as `make`
- To run the simulation, use `./a.out`

# Entities at play:
## Course:
- Each course is simulated by a course thread, handled by the coursehandler() function
- If the course has been alotted at least 1 student (checked by waiting for a semaphore), the course searches through the list of labs associated with the course to find a TA who is free to take a tutorial, using the searchTA() function
- The bonus part has been implemented, and the TA which has the the least number of alotted times from a lab is given the alottment, and this TA begins the tutorial.
- In case no TA is eligible to take a tutorial, the course has exhausted all its TAs and is thus removed from the course offering portal.
- When a course is removed from the portal, all the students who were waiting for a tutorial for the course is freed.

## Labs:
- Lab is simulated using a struct, and not a separate thread. When the Lab has exhausted all its TAs, it is flagged as exhausted, and is not considered in selectTA() function.

## TAs:
- Each TA has a lock, which is acquired and released after checking whether the TA has been alotted a course or not, in the selectTA() function
- TAs also are simulated through an array of structures, and not via threads

## Tutorial
- In a tutorial, a random number is decided depending upon the current number of students in the course and the maximum allowed slot limit of the course
- The tutorial is 3 seconds long
- after the tutorial, the semaphore is decremented to account for the number of students who attended tutorial.
- After the tutorial, students who attended the tutorial are signalled so that they can go about deciding whether or not to take the course 

## Student:
- Students are simulated by student threads, handled by the studenthandler() function.
- When a student has entered his preferences, the preferences are checked from 1(highest pref) to 3(lowest pref)
- Students sends a request to apply for a course using the applycourse() function
- When the student is alotted a seat in the course (acquires the course lock) the semaphore of that particular course is posted , and the student waits for the tutorial to end by waiting on a conditional variable and releasing the course lock.
- After the tutorial, the probability of the student taking the course is calculated by a random value rprob= (rand()%100)/100, and checked with the input probability (iprob, from calibre and interest quotient of course) of the student
    - if rprob < iprob, the student likes the subject, and  hence, takes the course and exits the simulation
    - Else, the student withdraws from the course, and applies for the course of his next preference.
- If a student hasnt finalised a course after finishing all 3 preferences, he doesnt get a course of his preference, and thus, exits the simulation. 

The simulation ends when all of the students exits the simulation.