## client side changes
--> in the client program, i've creadted 'm' user threads corresponding to m input strings.
--> the function handler of threads is begin_process() , here i'll be initialising socket and connection is made to server
--> then, message is sent to server from appropriate index
-->in the same function, declared variable to recieve message from server
-->then, while recieving msg and printing it , i've used mutex_lock so as to ensure that no 2 threads are printing at same time.

## server side 
--> created 'n' worker threads.
--> function handler for these threads: 'worker_th()' here, i will check whether there are any clients whose connections are to be handled or not. we'll do it using queue. if queue is empty, the thread goes to cond_wait else, it takes one of the client socket_id from queue and handles connection.
-->in main(), there's an infinite loop where server will constantly be checking for client connections, if it gets a connection req, it accepts then. here, i'll push the socket_fd to queue and send signal to thread waiting on a conditional_variable so that the thread wakes up and implements the handle_connection for that client.
-->during pushing to queue, protection is ensured using locks
-->in the handle_connection(),we'll be retrieving the input using socket_fd. 
-->we would send the input string which is retrievd to dict_func() where tokensing of input string takes place and according to input (i.e, "insert"/"delete"/"update"/"fetch"),appropriate if statement gets executed and it returns appropriate return string.
--> this func returns string to connection_handler(), from here it sents back to client.
--> server again starts listening to ports