#include "header.h"

void DataStoring(char* Name, char zone, int rT, int P, int pG){
    strcpy(spectators[specIndex].name,Name);
    spectators[specIndex].Zone=zone;
    spectators[specIndex].Group = groupIndex;
    spectators[specIndex].reachingTime=rT;
    spectators[specIndex].patienceTime=P;
    spectators[specIndex].patienceGoals=pG;
    spectators[specIndex].left = 0;
    spectators[specIndex].got=0;
    specIndex++;
}

void printData(){
    printf("Zone capacity: H=%d A=%d N=%d\n",H,A,N);
    printf("Spectating time: %d\n",X);
    printf("Number of Groups: %d\n",G);
    for(int i=1;i<=G;i++){
        printf("Number of people in Group-%d: %d\n",i,groupCapacity[i]);
    }
    printf("Number of people reach stadium: %d\n",specIndex);
    for(int i=0;i<specIndex;i++){
        printf("Name:%s\tZone:%c\tGroup:%d\tTimetoReach:%d\tPatienceValue:%d\tPatienceGoals:%d\n",spectators[i].name,spectators[i].Zone,spectators[i].Group,spectators[i].reachingTime,spectators[i].patienceTime,spectators[i].patienceGoals);
    }
    printf("Number of Chances for Goals: %d\n",numGoals);
    for(int i=0;i<numGoals;i++){
        printf("Team:%c\tTime Elapsed:%d\tconversionProbability:%.2f\n",Goals[i].Team,Goals[i].timeElapsed,Goals[i].conversionProb);
    }
    printf("__________________________________________________\n\n");
}

int main(){
    groupCount=0;
    tick=0;
    scanf("%d %d %d",&H,&A,&N);
    scanf("%d",&X);
    scanf("%d",&G);
    sem_init(&synchro,0,2);
    if (pthread_mutex_init(&groupLock, NULL) != 0) {
        printf("\n mutex init has failed\n");
        return 1;
    }
    groupCapacity = (int*)malloc((G+1)*sizeof(int));
    groupWaiting = (int*)calloc(G+1,sizeof(int));
    spectators = (Person*)malloc(sizeof(Person)*500);
    groupIndex=1;
    specIndex=0;
    homeGoal=0;
    awayGoal=0;
    seatAvailable[0]=H;
    seatAvailable[1]=A;
    seatAvailable[2]=N;
    char* Name;
    char zone;
    int rT; //reachingtime
    int P; //patiencetime
    int pG; //patienceGoals
    for(groupIndex=1;groupIndex<=G;groupIndex++){
        scanf("%d",&groupCapacity[groupIndex]);
        for(int j=0;j<groupCapacity[groupIndex];j++){
            scanf("%s %c %d %d %d",Name,&zone,&rT,&P,&pG);
            DataStoring(Name,zone,rT,P,pG);
        }
    }
    scanf("%d\n",&numGoals);
    Goals = (Goal*)malloc(sizeof(Goal)*numGoals);
    for(int j=0;j<numGoals;j++){
        scanf("%c %d %f\n",&Goals[j].Team,&Goals[j].timeElapsed,&Goals[j].conversionProb);
    }
    remainIndex=specIndex;
    printData();

    while(groupCount!=G){
        pthread_create(&tid0,NULL,game,NULL);
        pthread_create(&tid1,NULL,ticket,NULL);
        pthread_create(&tid2,NULL,group,NULL);
        pthread_join(tid0,NULL);
        pthread_join(tid1,NULL);
        pthread_join(tid2,NULL);
        tick++;
    }
    pthread_exit(NULL);
    pthread_mutex_destroy(&groupLock);
    sem_destroy(&synchro);
    return 0;
}

