#include "header.h"

void* ticket(){
    sem_wait(&synchro);
    for(int i=0;i<specIndex;i++){
        if(spectators[i].Zone == 'H' && spectators[i].patienceGoals<=homeGoal && tick > spectators[i].reachingTime && spectators[i].left==0){
            printf(ANSI_COLOR_RED "%s is leaving due to bad performance of his team\n",spectators[i].name);
            spectators[i].left=1;
            seatAvailable[0]++;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if(spectators[i].Zone == 'A' && spectators[i].patienceGoals<=awayGoal && tick > spectators[i].reachingTime && spectators[i].left==0){
            printf(ANSI_COLOR_RED "%s is leaving due to bad performance of his team\n",spectators[i].name);
            spectators[i].left=1;
            seatAvailable[1]++;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if(spectators[i].reachingTime == tick && spectators[i].left==0){
            printf(ANSI_COLOR_MAGENTA "%s has reached the stadium\n",spectators[i].name);                
            if(seatAvailable[0]>0 && spectators[i].Zone == 'H'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[0]--;
                spectators[i].got=1;
            }
            if(seatAvailable[1]>0 && spectators[i].Zone == 'A'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[1]--;
                spectators[i].got=1;
            }
            if(seatAvailable[2]>0 && spectators[i].Zone == 'N'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[2]--;
                spectators[i].got=1;
            }
        }
        
        if(spectators[i].reachingTime < tick && spectators[i].patienceTime == tick  && spectators[i].got==0 && spectators[i].left==0){
            printf(ANSI_COLOR_BLUE "%s has not got the seat\n",spectators[i].name);
            spectators[i].left=1;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if((tick-spectators[i].reachingTime) == X && spectators[i].left==0){
            printf(ANSI_COLOR_BLACK "%s has watched the game for %d seconds and now leaving\n",spectators[i].name,X);
            if(spectators[i].Zone == 'H')seatAvailable[0]++;
            if(spectators[i].Zone == 'A')seatAvailable[1]++;
            if(spectators[i].Zone == 'N')seatAvailable[2]++;
            spectators[i].left=1;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
    }
    sem_post(&synchro);
}

