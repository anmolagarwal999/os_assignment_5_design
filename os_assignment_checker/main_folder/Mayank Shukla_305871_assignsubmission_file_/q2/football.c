#include "header.h"

void* game(){
    srand(time(0));
    sem_wait(&synchro);
    for(int i=0;i<numGoals;i++){
        float prob = (rand()%100 + 1)/100.0;
        if(Goals[i].timeElapsed == tick){
            if(Goals[i].conversionProb >= prob){
                if(Goals[i].Team == 'H'){
                    homeGoal++;
                    printf(ANSI_COLOR_RESET "Team H has scored their %d Goal\n",homeGoal);
                }else{
                    awayGoal++;
                    printf(ANSI_COLOR_RESET "Team A has scored their %d Goal\n",awayGoal);
                }
            }
            else{
                if(Goals[i].Team == 'H'){
                    printf(ANSI_COLOR_RESET "Team H has missed their chance for %d Goal\n",homeGoal+1);
                }else{
                    printf(ANSI_COLOR_RESET "Team A has missed their chance for %d Goal\n",awayGoal+1);
                }
            }
        }
    }
    sem_post(&synchro);
}