#ifndef __CLASSICO
#define __CLASSICO

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_BLACK   "\e[1;90m"

#define WHTHB "\e[0;107m"

sem_t semaphore;
sem_t synchro;

typedef struct{
    char name[50];
    char Zone;
    int Group;
    int reachingTime;
    int patienceTime;
    int patienceGoals;
    int left;
    int got;
}Person;

Person* spectators;

typedef struct{
    char Team;
    int timeElapsed;
    float conversionProb;
}Goal;

Goal* Goals; 

int H,A,N; //capacity of zones
int X; //spectating time
int G; //Number of groups
int* groupCapacity; //Number of people in each group
int* groupWaiting;
int seatAvailable[3];
int groupIndex,specIndex; //zoneIndex: which zone current person is, watchIndex:total number of people in stadium
// waitIndex: total number of people waiting for ticket, groupIndex: keep track of groups, specIndex: number of people arriving at stadium
int numGoals; //number of chances for goal
int remainIndex;
void DataStoring(char* Name, char zone, int rT, int P, int pG);
void printData();
void* ticket();
void* game();
void* group();

int tick;
int homeGoal,awayGoal;
int groupCount;
pthread_mutex_t groupLock;

pthread_t tid0;
pthread_t tid1;
pthread_t tid2;
#endif