# Q2: The Clasico Experience  

* It contain six files including header file.
__1.header.h:__  Contains prototypes of function, library declaration, variable declaration.  

typedef struct{
    char name[50];
    char Zone;
    int Group;
    int reachingTime;
    int patienceTime;
    int patienceGoals;
    int left;
    int got;
}Person; 

This structure is used to store the details of person.

typedef struct{
    char Team;
    int timeElapsed;
    float conversionProb;
}Goal;

This structure is used to store the details of Goal.

__2.main.c:__ Main file contains **main()** function in which input reading, and thread declaration is done.  

__3.ticket.c:__ This file contain __void* ticket()__ function which is function passed to a thread with *tid0*. In this thread, seat allocation, person leaving the stadium because of either of bad performance of team, or finish of spectating time, or on not getting the seat due to late arrival and no seat vacant within his/her patience time, functionalities are implemented.  
In this i am also checking that from which Zone person belongs. If seat is vacant in that group then only it will be allocated.

__4.football.c:__ This file contain __void* game()__ function which is passed to a thread with thread id *tid1*. In this thread, rand() function is used to calculate the probability of conversion of chances to goal. We are storing the goals (if chances are converted to goal) into variable *homeGoal* and *awayGoal*. We are using these variables to check whether it number of goals reached to Goals for which fan will leave the stadium due to bad performance.

__5.groupWait.c:__ This file contain __void* group()__ function which is passed to a thread with thread id *tid2*. In this thread i am checking whether all the members of a Group reached to exit Gate. If all the members will reach there then that will leave the stadium for dinner.  
To check that, i am using a Global Array *groupWaiting*  which is updated when person leaves the stadium due to reasons explained above (ticket.c). When groupWaiting of ith group will match the groupCapacity of ith group then ith group will leave for dinner.

Since we are using **groupWaiting** in two threads hence i am using **mutex lock** to avoid deadlock or wrong reading of numbers.

__6.makefile:__ Used to run multi file program.  

These threads run in parallel, to give person buying ticket, leaving stadium, Team converting the chances to Goals, and Group leaving for dinner.
To achieve this parallelism i am using the posix library <pthread.h>