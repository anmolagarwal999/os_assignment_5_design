#include "header.h"

void* group(){
    for(int i=1;i<=G;i++){
        if(groupWaiting[i] == groupCapacity[i]){
            printf(ANSI_COLOR_YELLOW "Group %d left for dinner\n",i);
            pthread_mutex_lock(&groupLock);
            groupWaiting[i]=-1;
            groupCount++;
            pthread_mutex_unlock(&groupLock);
        }
    }
}