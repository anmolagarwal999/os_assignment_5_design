#ifndef __PORTAL
#define __PORTAL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct{
    float calibre;
    int fillTime;
    int preference[3];
}Student;

Student* students;

typedef struct{
    char name[50];
    int TA;
    int limit; //number of times a member of lab can TA in a course
    int chanceLeft[100];
}Lab;

Lab* Labs;

typedef struct{
    char name[50];
    float interest;
    int slots; //number of slots which can be allocated by a TA
    int totalLabs;
    int LabIDs[100];
}Course;

Course* courses;

int num_students;
int num_labs;
int num_courses;

void* student();

#endif