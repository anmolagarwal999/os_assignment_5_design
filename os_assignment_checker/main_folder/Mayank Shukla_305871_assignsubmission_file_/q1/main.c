#include "header.h"

void printData(){
    for(int i=0;i<num_courses;i++){
        printf("%s %.2f %d %d",courses[i].name,courses[i].interest,courses[i].slots,courses[i].totalLabs);
        for(int j=0;j<courses[0].totalLabs;j++){
            printf(" %d",courses[0].LabIDs[j]);
        }
        printf("\n");
    }
    for(int i=0;i<num_students;i++){
        printf("%.2f %d %d %d %d\n",students[i].calibre,students[i].preference[0],students[i].preference[1],students[i].preference[2],students[i].fillTime);
    }
    for(int i=0;i<num_labs;i++){
        printf("%s %d %d\n",Labs[i].name,Labs[i].TA,Labs[i].limit);
        for(int j=0;j<Labs[i].TA;j++){
            Labs[i].chanceLeft[j]=Labs[i].limit;
        }
    }
}

int main(){
    scanf("%d %d %d",&num_students,&num_labs,&num_courses);
    courses = (Course*)malloc(num_courses*sizeof(Course));
    for(int i=0;i<num_courses;i++){
        scanf("%s %f %d %d",courses[i].name,&courses[i].interest,&courses[i].slots,&courses[i].totalLabs);
        for(int j=0;j<courses[i].totalLabs;j++){
            scanf("%d",&courses[i].LabIDs[j]);
        }
    }
    students = (Student*)malloc(num_students*sizeof(Student));
    for(int i=0;i<num_students;i++){
        scanf("%f %d %d %d %d",&students[i].calibre,&students[i].preference[0],&students[i].preference[1],&students[i].preference[2],&students[i].fillTime);
    }
    Labs = (Lab*)malloc(num_labs*sizeof(Lab));
    for(int i=0;i<num_labs;i++){
        scanf("%s %d %d",Labs[i].name,&Labs[i].TA,&Labs[i].limit);
    }
    printData();
    pthread_t tid;
    pthread_create(&tid,NULL,student,NULL);
    pthread_exit(NULL);
}