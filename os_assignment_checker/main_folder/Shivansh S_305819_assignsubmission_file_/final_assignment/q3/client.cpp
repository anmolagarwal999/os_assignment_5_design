#include "header.h"
using namespace std;


vector <pair< pair<int, int>, string>> v;

void errorReport(int response, string message) {
    if(response < 0) {
        std::cout << message << std::endl;
        perror("ERROR: ");
    }
}

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    errorReport(bytes_received, "Failed to read data from socket\n");
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    errorReport(bytes_sent,  "Failed to send data to socket\n");
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    errorReport(socket_fd, "Error in socket creation\n");
    int port_num = PORT;

    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);
    int response = connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj));
    errorReport(response, "Error in connection\n");
    return socket_fd;
}

void *begin_process(void *args)
{
    pair< pair<int, int>, string> input = *(pair< pair<int, int>, string> *)args;
    int socket_fd, num_bytes_read;
    string response;

    struct sockaddr_in server_obj;
    sleep(input.first.second);
    socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, input.second);
    tie(response, num_bytes_read) = read_string_from_socket(socket_fd, BUFSIZE);
    cout << input.first.first << response << endl;
    close(socket_fd);

    return NULL;
}

int main(int argc, char *argv[])
{

    int m;
    string temp;
    getline(cin, temp);
    m = stoi(temp);
    pthread_t threads[m];
    /* vector<pthread_t> threads(m); */

    for (int i = 0; i < m; i++)
    {
        getline(cin, temp);
        int time = stoi(temp.substr(0, temp.find(" ")));
        string command = temp.substr(temp.find(" ") + 1);
        v.push_back({{i, time}, command});
        pthread_create(&threads[i], NULL, begin_process, &v[i]);
    }

    for (int i = 0; i < m; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}
