## Question 3

#### Shivansh S, 2019114003

### Execution

for this you need to compile both the `server` and `client` files by typing
```bash
g++ -pthreads -o server server.c
g++ -pthreads -o clients clients.c
```

Once they are compiled the input formats is as mentioned in the pdf

### Logic:

Honestly, the code is heavily derived from the existing boilerplate code. The client logic has high similarities, except that we want to run the begin_process concurrently in multiple threads. We see that one request is given to the server and then the thread exits.

### Server:

The server takes `N` as input, which is the total number of threads. It is important to note that we are using a queue, where we need to remove peoople from the queue. Hence, before client arrives to the queue, we need to use a semaphore which wakes up the background threads and . 
