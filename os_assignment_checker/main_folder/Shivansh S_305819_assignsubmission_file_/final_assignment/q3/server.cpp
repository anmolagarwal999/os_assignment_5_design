#include "header.h"
using namespace std;
#define MAX_CLIENTS 100

const int initial_msg_len = 256;

sem_t thread_sem;
pthread_mutex_t client_lock;

vector<pthread_t> requests;
queue<int> clients;
vector<string> dict(110);
vector<pthread_mutex_t> d_lock(110);

void errorReport(int response, string message) {
    if(response < 0) {
        std::cout << message << std::endl;
        perror("ERROR: ");
    }
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    errorReport(bytes_received, "Failed to read data from socket\n");
    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    errorReport(bytes_sent, "Failed to write data to socket\n");
    return bytes_sent;
}

int getInput(string tokens[], char* command) {
    int index = 0;
    size_t len = MAXLEN;
    char* save_pointer;
    char* args = strtok_r(command, " \t\n", &save_pointer);
    while(args!=NULL) {
        tokens[index] = args;
        index++;
        args = strtok_r(NULL, " \t\n", &save_pointer);
    }
    tokens[index] = '\0';
    return index;
}

string update(string tokens[], int token_len) {
    if (token_len != 3) {
        return "Invalid number of arguments\n";
    }
    int key = stoi(tokens[1]);
    string change_to = tokens[2];
    if (0<= key && key<=100) {
        if (dict[key].length() > 0) {
            pthread_mutex_lock(&d_lock[key]);
            dict[key] = change_to;
            pthread_mutex_unlock(&d_lock[key]);
        } else {
            return "Key does not exist\n";
        }
    } else {
        return "Invalid Key\n";
    }
    return change_to + "\n";
}

string del(string tokens[], int token_len) {
    if (token_len != 2) {
        return "Invalid number of arguments\n";
    }
    int key = stoi(tokens[1]);
    if ( 0<= key && key<=100 )  {
        if(dict[key].length() > 0) {
            pthread_mutex_lock(&d_lock[key]);
            dict[key] = "";
            pthread_mutex_unlock(&d_lock[key]);
        } else {
            return "No such key exists\n";
        }
    } else {
        return "Invalid key\n";
    }
    return "Deleting Successful\n";
}

string concat(string tokens[], int token_len) {
    if (token_len != 3) {
        return "Invalid number of arguments\n";
    }
    int key1 = stoi(tokens[1]);
    int key2 = stoi(tokens[2]);

    if(0<=key1 && key1<=100 && 0<=key2 && key2<=100) {
        if(dict[key1].length() > 0 && dict[key2].length() > 0) {
            string value1 = dict[key1];
            string value2 = dict[key2];
            pthread_mutex_lock(&d_lock[key1]);
            dict[key1] = value1 + value2;
            pthread_mutex_unlock(&d_lock[key1]);
            pthread_mutex_lock(&d_lock[key1]);
            dict[key2] = value2 + value1;
            pthread_mutex_unlock(&d_lock[key2]);
        } else {
            return "Concat failed as at least one of the keys does not exist\n";
        }
    } else {
        return "Invalid Key arguments sent\n";
    }
    return dict[key2] + "\n";

}

string insert(string tokens[], int token_len) {
    if (token_len != 3) {
        return "Invalid number of arguments\n";
    }
    int key = stoi(tokens[1]);
    string value = tokens[2];

    if (0<= key && key <= 100) {
        if(dict[key].length() > 0) {
            return "Key exists\n";
        }
        pthread_mutex_lock(&d_lock[key]);
        dict[key] = value;
        pthread_mutex_unlock(&d_lock[key]);
    } else {
        return "Invalid key passed\n";
    }
    return "Insertion Successful\n";
}

string fetch(string tokens[], int token_len) {
    if (token_len != 2) {
        return "Invalid Number of Arguments\n";
    }

    int key = stoi(tokens[1]);
    if (0<= key && key<=100) {
        if (dict[key].length() > 0) {
            return dict[key] + "\n";
        } else {
            return "Key does not exist\n";
        }
    } else {
        return "Invalid Key passed\n";
    }
}

string handle_commands(char* command) {
    string tokens[MAXLEN];
    string delims = " \t\n";
    int token_len = getInput(tokens, command);
    int index = 0;
    string response = "";
    if(tokens[0] == "insert") {
        response = insert(tokens, token_len);
    } else if (tokens[0] == "concat") {
        response = concat(tokens, token_len);
    } else if (tokens[0] == "delete") {
        response = del(tokens, token_len);
    } else if (tokens[0] == "update") {
        response = update(tokens, token_len);
    } else if (tokens[0] == "fetch") {
        response = fetch(tokens, token_len);
    }
    return response;
}


void handle_connection(int client_socket_fd)
{
    string cmd;
    int received_num, sent_num;

    while (true)
    {
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, BUFSIZE);
        if (received_num <= 0){
            printf("Server unable to read message from client\n");
            goto close_client_socket_ceremony;
        } else if (cmd == "exit"){
            cout << "Exit started" << endl;
            goto close_client_socket_ceremony;
        }

        char* command = new char[cmd.length() + 1];
        strcpy(command, cmd.c_str());
        string response = handle_commands(command);
        sleep(2);
        response = ":" + to_string(pthread_self()) + ":" + response;
        int sent_to_client = send_string_on_socket(client_socket_fd, response);
        if (sent_to_client == -1){
            perror("Socket seems to be closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf("Disconnected from client\n");
}

void *worker(void *args)
{
    while (true)
    {
        sem_wait(&thread_sem);
        pthread_mutex_lock(&client_lock);
        int client_socket_fd = (clients.size() > 0)? clients.front() : -1;
        if (clients.size() > 0)
            clients.pop();
        pthread_mutex_unlock(&client_lock);
        handle_connection(client_socket_fd);
    }

    return NULL;
}


int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    pthread_mutex_init(&client_lock, NULL);
    sem_init(&thread_sem, 0, 0);

    int n_threads = atoi(argv[1]);
    for(int i=0;i<110;i++) {
        pthread_mutex_init(&d_lock[i], NULL);
        dict[i] = "";
    }

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    requests.resize(n_threads);
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    errorReport(wel_socket_fd, "Error creating welcome socket\n");

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port
    int response = bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj));
    errorReport(response, "Binding error in main, server\n");
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    for (int i = 0; i < n_threads; i++){
        pthread_create(&requests[i], NULL, worker, NULL);
    }

    while (1)
    {
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        errorReport(client_socket_fd, "Error while accept\n");
        pthread_mutex_lock(&client_lock);
        clients.push(client_socket_fd);
        pthread_mutex_unlock(&client_lock);
        printf("New client port number %d\n", ntohs(client_addr_obj.sin_port));

        sem_post(&thread_sem);
    }

    close(wel_socket_fd);

    return 0;
}
