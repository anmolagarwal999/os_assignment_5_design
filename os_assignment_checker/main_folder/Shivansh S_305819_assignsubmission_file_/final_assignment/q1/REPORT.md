## Question1

#### Shivansh S, 2019114003

### Running the Code:

Its simple, just run `make` inside `q1/` and boom, you are ready to go! Now you just need to exectue the files and give input 

### Logic

So there are 2 main threads at work, Courses and Students. Courses allocate the ta among itself, there are cases where it has to wait but in my implementation that is not required. Simple put, Student waits for Course to get allocated, Course waits for Students to come and fill the slots, and once the Student is allocated a seet Student waits till tutorial is over. In such a case, we see how the interaction between the threads are formalised. 

### File Structures

`main.c` contains driver code which just helps accepts inputs, initialise arrays of the structs and creates initial threads for student and courses. 
`input.c` is a really helpful file which helps us not only take input, but parse it easily, and define each stru ture on those bases.
`students.c` is a file related to functioning and threads of students in our simulation.
`courses.c` similar to students, courses also have their own function handling all their threads, waiting and all to ge tthem ready for maximum productivity

#### Input Format:

```
<num_students> <num_labs> <num_courses>
<name of course 1> <interest quotient for course 1> <maximum number of slots which can be allocated by a TA> <number of labs
from which course accepts TA> [list of lab IDs]
.
.
.
<name of the last course> <interest quotient for the last course> <maximum number of slots which can be allocated by a TA>
<number of labs from which course accepts TA> [list of lab IDs]
<calibre quotient of first student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
.
.
<calibre quotient of last student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
<name of first lab> <number of students/TA mentors in first lab> <number of times a member of the lab can TA in a course>
.
.
.
<name of last lab> <number of students.TA Mentors in the last lab> <number of times a member of the lab can TA in a course>
```
