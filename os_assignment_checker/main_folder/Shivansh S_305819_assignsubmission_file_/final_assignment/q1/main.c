#include "header.h"

struct course *Courses = NULL;
struct student *Students = NULL;
struct lab *Labs = NULL;
int clen = 0;
int slen = 0;
int llen = 0;

int main () {
    char* delims = " \t\n";
    char* firstLine[MAXLEN];
    int firstLineLen = getInput(firstLine, delims);

    int num_students = atoi(firstLine[0]);
    int num_labs = atoi(firstLine[1]);
    int num_courses = atoi(firstLine[2]);
    
    Courses = (struct course*) malloc(sizeof(struct course)*num_courses);
    char* courseLine[MAXLEN];
    while (clen < num_courses) {
        int courseLineLen = getInput(courseLine, delims);
        addCourse(courseLine);
    }

    Students = (struct student*) malloc(sizeof(struct student)*num_students);
    char* studentLine[MAXLEN];
    while(slen < num_students) {
        int studentLineLem = getInput(studentLine, delims);
        addStudent(studentLine);
    }

    Labs = (struct lab*) malloc(sizeof(struct lab)*num_labs);
    char* labLine[MAXLEN];
    while(llen < num_labs) {
        int labLineLen = getInput(labLine, delims);
        addLab(labLine);
    }

    pthread_t s_thread;
    pthread_create(&s_thread, NULL, sMain, NULL);
    pthread_t c_thread;
    pthread_create(&c_thread, NULL, cMain, NULL);

    pthread_join(s_thread, NULL);
    pthread_join(c_thread, NULL);
    free(Courses);
    free(Students);
    free(Labs);

    return 0;
}