#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>

#define MAXLEN 2048
#define BLUE "\033[0;34m"
#define CYAN "\033[0;36m"
#define RED "\033[31m"
#define WHITE "\033[0;37m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"

struct student {
    int index;
    int inCourse;
    double calibre;
    int current;
    int current_id;
    int p1;
    int p2;
    int p3;
    pthread_mutex_t s_lock;
    pthread_cond_t wait_course;
    int fill_time;
};
struct course {
    int index;
    char* name;
    double interest;
    int max_slots;
    int num_labs;
    pthread_mutex_t c_lock;
    pthread_cond_t c_wait;
    int ta_id;
    int ta_lab;
    int labids[MAXLEN];
    int t_index;
    int slots;
};
struct lab {
    char* name;
    int num_std;
    int repeat;
    int isFree;
    pthread_mutex_t ta_lock[MAXLEN];
    int turns[MAXLEN];
    int occupied[MAXLEN];
};

extern int errno;
extern struct course *Courses;
extern int clen;
extern struct student *Students;
extern int slen;
extern struct lab *Labs;
extern int llen;

int main(void);
int getInput(char* tokens[], char* delims);
void addCourse(char* tokens[]);
void addStudent(char* tokens[]);
void addLab(char* tokens[]);
void* sMain(void* args);
void* cMain(void* args);