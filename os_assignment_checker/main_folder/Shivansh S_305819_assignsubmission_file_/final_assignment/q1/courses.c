#include "header.h"

void* allocate(void* args) {
    struct course *myCourse = (struct course*) args;
    for(int i=0;i<myCourse->num_labs;i++) {
        struct lab *myLab = &Labs[myCourse->labids[i]];
            for(int j=0;j<myLab->num_std;j++) {
                pthread_mutex_lock(&myLab->ta_lock[j]);
                if (myLab->turns[j] > 0 && myLab->occupied[j] == 0) {
                    myLab->turns[j] -= 1;
                    myLab->occupied[j] = 1;
                    myCourse->ta_id = j;
                    myCourse->ta_lab = myCourse->labids[i];
                    printf("%sTA %d from lab %s has been allocated to course %s for his %d TA ship%s\n", BLUE, j, myLab->name, myCourse->name, (myLab->repeat - myLab->turns[j]), BLUE);
                }
                pthread_mutex_unlock(&myLab->ta_lock[j]);
                if(myCourse->ta_id != -1) {
                    break;
                }
            }
        if(myCourse->ta_id != -1) {
            break;
        }
    }
    int slots = rand()%(myCourse->max_slots - 1 + 1) + 1;
    pthread_mutex_lock(&myCourse->c_lock);
    myCourse->t_index = 0;
    int student_interest = 0;
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        if (myStudent->current == myCourse->index) {
            student_interest++;
        }
    }
    myCourse->slots = (student_interest >= slots) ? slots: student_interest;
    pthread_mutex_unlock(&myCourse->c_lock);
    printf("%sCourse %s has been allocated %d seats%s\n", RED, myCourse->name, myCourse->slots, RED);
    
    pthread_mutex_lock(&myCourse->c_lock);
    while(myCourse->t_index < myCourse->slots) {
        pthread_cond_wait(&myCourse->c_wait, &myCourse->c_lock);
    }
    pthread_mutex_unlock(&myCourse->c_lock);

    // take the tutorial
    printf("%sTutorial has started for Course %s with %d seats filled out of %d%s\n", RED, myCourse->name, myCourse->t_index, myCourse->slots, RED);
    sleep(5);
    struct lab *myLab = &Labs[myCourse->ta_lab];
    printf("%sTA %d from lab %s has completed the tutorial for course %s%s\n", BLUE, myCourse->ta_id, myLab->name, myCourse->name, BLUE);
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        pthread_mutex_lock(&myStudent->s_lock);
        if (myStudent->current == myCourse->index) {
            myStudent->inCourse = 0;
            pthread_cond_signal(&myStudent->wait_course);
        }
        pthread_mutex_unlock(&myStudent->s_lock);
    }
//    pthread_mutex_unlock(&myCourse->c_lock);
}

void* cMain(void* args) {
    pthread_t threads[clen];
    int response = 0;
    for(int i=0;i<clen;i++) {
        response = pthread_create(&threads[i], NULL, allocate, (void *)&Courses[i]);
        if(response < 0) {
            printf("Error Number: %d\n", errno);
            perror("cmain");
        }
    }

    for(int i=0;i<clen;i++) {
        pthread_join(threads[i], NULL);
    }
}
