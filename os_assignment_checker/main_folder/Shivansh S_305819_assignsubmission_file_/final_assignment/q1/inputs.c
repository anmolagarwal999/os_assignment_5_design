#include "header.h"

int getInput(char* tokens[], char* delims) {
    int index = 0;
    size_t len = MAXLEN;
    char* line = (char *) malloc(sizeof(char)*len);
    if(!fgets(line, len, stdin)) {
        printf("Error Number: %d\n", errno);
        perror("getInput, utils.c");
    } else {
        char* save_pointer;
        char* args = strtok_r(line, delims, &save_pointer);
        while(args!=NULL) {
            tokens[index] = args;
            index++;
            args = strtok_r(NULL, delims, &save_pointer);
        }
        tokens[index] = '\0';
    }
    return index;

}

void addCourse(char* tokens[]) {
   Courses[clen].name = tokens[0];
   Courses[clen].interest = atof(tokens[1]);
   Courses[clen].max_slots = atoi(tokens[2]);
   Courses[clen].num_labs = atoi(tokens[3]);
   Courses[clen].index = clen;
   int cindex = 0;
   int carg = 4;
   while(cindex < Courses[clen].num_labs) {
       Courses[clen].labids[cindex] = atoi(tokens[carg]);
       carg++;
       cindex++;
   }
   Courses[clen].labids[cindex] = '\0';
   Courses[clen].ta_id = -1;
   Courses[clen].ta_lab = -1;
   pthread_mutex_init(&Courses[clen].c_lock, NULL);
   pthread_cond_init(&Courses[clen].c_wait, NULL);
   Courses[clen].t_index = 0;
   Courses[clen].slots = 0;
   clen++;
}

void addStudent(char* tokens[]) {
    Students[slen].index = slen;
    Students[slen].calibre =  atof(tokens[0]);
    Students[slen].p1 = atoi(tokens[1]);
    Students[slen].p2 = atoi(tokens[2]);
    Students[slen].p3 = atoi(tokens[3]);
    Students[slen].fill_time = atoi(tokens[4]);
    Students[slen].inCourse = 0;
    Students[slen].current = Students[slen].p1;
    Students[slen].current_id = 1;
    pthread_mutex_init(&Students[slen].s_lock, NULL);
    pthread_cond_init(&Students[slen].wait_course, NULL);
    slen++;
}

void addLab(char* tokens[]) {
    Labs[llen].name = tokens[0];
    Labs[llen].num_std = atoi(tokens[1]);
    Labs[llen].repeat = atoi(tokens[2]);
    Labs[llen].isFree = (Labs[llen].repeat == 0)? 0: Labs[llen].num_std;
    for(int i=0;i<Labs[llen].num_std;i++) {
        Labs[llen].turns[i] = Labs[llen].repeat;
        Labs[llen].occupied[i] = 0;
        pthread_mutex_init(&Labs[llen].ta_lock[i], NULL);
    }
    llen++;
} 