#include "header.h"

void slotme(void* args) {
    struct student *myStudent = (struct student*) args; 
    struct course *myCourse = &Courses[myStudent->p1];
    pthread_mutex_lock(&myCourse->c_lock);

    // Waiting for allocation
    while(myCourse->t_index >= myCourse->slots) {
        pthread_cond_wait(&myStudent->wait_course, &myCourse->c_lock);
    }
    myStudent->inCourse = 1;
    myCourse->t_index += 1;
    if(myCourse->t_index <= myCourse->slots) {
        pthread_cond_signal(&myCourse->c_wait);
    }
    printf("%sStudent %d has been allocated a seat in course %s%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
    pthread_mutex_unlock(&myCourse->c_lock);

    // Wait for course to end
    pthread_mutex_lock(&myStudent->s_lock);
    while(myStudent->inCourse == 1) {
        pthread_cond_wait(&myStudent->wait_course, &myStudent->s_lock);
    }
    double prob = myCourse->interest * myStudent->calibre;
    double rand_num = rand()/((double) RAND_MAX);
    if (rand_num < prob) {
        printf("%sStudent %d has withdrawn from course %s%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
        if(myStudent->current_id == 1) {
            myStudent->current = myStudent->p2;
            myStudent->current_id = 2;
        } else if (myStudent->current_id == 2) {
            myStudent->current = myStudent->p3;
            myStudent->current_id = 3;
        }
    }
    else {
        printf("%sStudent %d has selected the course %s permanently%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
    }
    pthread_mutex_unlock(&myStudent->s_lock);
}

void* enroll(void* args) {
    struct student *myStudent = (struct student*) args; 
    sleep(myStudent->fill_time);
    printf("%sStudent %d has filled in preferences for course registeration%s\n",GREEN, myStudent->index, GREEN);
    slotme((void *)myStudent);
}

void* sMain(void* args) {
    pthread_t threads[slen];
    int response = 0;
    for(int i=0;i<slen;i++) {
        response = pthread_create(&threads[i], NULL, enroll, (void *)&Students[i]);
        if(response < 0) {
            printf("Error Number: %d\n", errno);
            perror("sMain");
        }
    }

    for(int i=0;i<slen;i++) {
        pthread_join(threads[i], NULL);
    }
    
}