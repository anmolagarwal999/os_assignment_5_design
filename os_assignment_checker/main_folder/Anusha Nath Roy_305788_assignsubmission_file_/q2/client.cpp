// ------------- headers --------------------

#include  <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>

#include <semaphore.h>

#define SERVER_PORT 8001
using namespace std;

// ---------------- colors -------------------------
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


//----------------- utilities -------------------------
#define MAX_STR_IP 400
#define ll long long int
const ll buff_sz = 1048576;

pthread_mutex_t print_terminal = PTHREAD_MUTEX_INITIALIZER;

// client request struct
struct c_request{

    pthread_mutex_t c_lock;         // lock
    string client_command;          // the client command
    string client_ip_command;       // the terminal p command
    pthread_t client_thread;        // the client thread
    int sno;                        // serial number
    int client_fd;                  // client file descriptor
    bool in_thread;                 // bool in thread
    int c_time;                     // required time

};
typedef struct c_request c_request;
vector <c_request> c_requests;

// a function to calculate the e-th power of a number n
ll power(ll n, ll e)
{
    ll answer = 1;
    for (ll i = 0; i < e; i++)
    {
        answer *= n;
    }
    return answer;
}

int stoiu(string stringv){
    
    int n;
    int parts = 0;
    for (int i = 0; i < stringv.length(); i++)
    {
        n = stringv[stringv.length() - 1 - i] - '0';
        parts += n * power(10, i);
    }
    return parts;
}

int send_socket(int fd, const string &s)
{
    //printf("send <%d> : <%s>\n", fd, s.c_str());
    int sent_bytes = write(fd, s.c_str(), s.length());
    return sent_bytes;
}

string read_socket(int fd)
{
   string b;
   b.resize(buff_sz);
   int read_bytes = read(fd,&b[0],buff_sz-1);

   if(read_bytes < 0){
       cerr << ANSI_COLOR_RED "cannot read!";
       return NULL;
   }

   b[read_bytes] = '\0';
   b.resize(read_bytes);
   return b;
}


int get_socket_fd()
{
    int socket_v = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_v < 0)
    {
        fprintf(stderr, ANSI_COLOR_RED "Socket could not be created!");
        exit(-1);
    }
    
    return socket_v;
}

void *client_thread_handler(void *(arg)){
    int index = *(int*) arg;

    // connection
    int temp_socket_fd = get_socket_fd();
    c_requests[index].client_fd = temp_socket_fd;

    struct sockaddr_in server_obj;
    int server_object_size = sizeof(server_obj);
    memset(&server_obj, 0, server_object_size); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(SERVER_PORT); // convert to big-endian order
    
    if (connect(temp_socket_fd, (struct sockaddr *)&server_obj, server_object_size) < 0)
    {
        fprintf(stderr, ANSI_COLOR_RED "Socket could not be connected!");
        exit(-1);
    }

    //sleeping required time
    sleep(c_requests[index].c_time);

    pthread_mutex_lock(&c_requests[index].c_lock);
    if(send_socket(c_requests[index].client_fd,c_requests[index].client_command)<0) {fprintf(stderr, ANSI_COLOR_RED "could not send message to server!"); return NULL;}
    pthread_mutex_unlock(&c_requests[index].c_lock);    

    pthread_mutex_lock(&c_requests[index].c_lock);
    string response = read_socket(c_requests[index].client_fd);
    pthread_mutex_unlock(&c_requests[index].c_lock);    

    pthread_mutex_lock(&print_terminal);
    printf("%d:", index);
    if(response.c_str()){
        printf("%s\n", response.c_str());
    }
    pthread_mutex_unlock(&print_terminal);
    return NULL;
}

int main(int argc, char *argv[])
{

    string input_lines_str;
    int input_lines = 0;
    getline(cin,input_lines_str);
    input_lines = stoiu(input_lines_str);
    int client_num = input_lines;
    int ip_i = 0;
    

    // take inputs
    while(input_lines--){

        string string_input;
        getline(cin, string_input);
        //cout<< "****** check1:" << string_input << endl;

        c_request temp_client_ip;

        temp_client_ip.client_ip_command = string_input;
        temp_client_ip.sno = ip_i;
        ip_i ++;
        
        int space_divider = string_input.find(' ');
        string temp_string_time = string_input.substr(0, space_divider);
        temp_client_ip.c_time = stoiu(temp_string_time);
        temp_client_ip.client_command = string_input.substr(space_divider + 1, string_input.length());

        //cout<< "****** check2:" << temp_client_ip.client_command << endl;
        pthread_mutex_init(&temp_client_ip.c_lock, NULL);
        // temp_client_ip.c_lock = PTHREAD_MUTEX_INITIALIZER;
        temp_client_ip.in_thread = 0;

        c_requests.push_back(temp_client_ip);
    }

    // create threads
    for(int i=0; i < client_num; i++){
        int *passed_index = new int;
        *passed_index = i;
        pthread_create(&c_requests[i].client_thread, NULL, client_thread_handler, (void *)passed_index);
    }

    for(int i=0; i< client_num; i++){
        pthread_join(c_requests[i].client_thread, NULL);
    }

    return 0;
}