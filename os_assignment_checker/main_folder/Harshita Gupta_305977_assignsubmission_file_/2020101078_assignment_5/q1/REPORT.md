## An alternate course allocation portal

**Context** - Sanchirat and Pratrey, students of the college, have been asked to use their skills to design an unconventional course registration system for the college, where a student can take trial classes of a course and can withdraw and opt for a different course if he/she does not like the course. Different labs in the college have been asked to provide students who can act as course TA mentors temporarily and take the trial tutorials.

### How to run the program
1. Run the command `gcc -pthread portal.c` 
2. Run `./a.out`
3. Input the description of the courses, labs, TAs and students. 

### Variables

1. `struct ta` - describes a TA.
   ```c
    int lab_id;
    int ta_id;       // [0, LabList[lab_id],num_TAs]
    int num_taships; // [0, LabList[lab_id].max_taships]
    int is_busy; //is the ta is taking a tutorial for a course
    pthread_mutex_t ta_mutex;
   ````
2. `struct lab` - describes a lab
    ```c
    int id;
    char name[MAX];
    int num_TAs;
    int max_taships;
    int total_tut; //total number of tutorials that have taken place by the TAs of this lab
    ta *TAs;
    pthread_mutex_t lab_mutex;
    ````
3. `struct course` - describes a course
    ```c
    int id;
    char name[MAX];
    float interest; //[0, 1]
    int max_slots;
    int num_labs;
    int *lab_id;
    int is_removed;
    pthread_t thread_id;
    pthread_cond_t tut_happening; //after getting slots student threads wait in tut-happening until the course thread finds a TA
    //and signals all the waiting student threads (use broadcast)
    int curr_ta_id;  //id of the TA currently assigned to the course
    int curr_lab_id; //lab Id of the TA currently assigned
    int curr_total_slots; //total number slots decided using rand
    int curr_slots_allocated;
    pthread_mutex_t course_lock;
    ````
4. `struct student` - describes a student
    ```cpp
    int id;
    float calibre;
    int c_id[3];
    int registration_time;
    int curr_cid; //the id of the course the student is currently waiting on to get slots
    int is_allocated_curr_slot;
    int is_attending_curr_tut;
    int selected_course; //1 if the student decided on a course
    pthread_t thread_id;          //thread
    pthread_mutex_t student_lock; //lock

    pthread_cond_t get_slot; //after getting slots student threads wait in tut-happening until the course thread finds a TA and signals all the waiting student threads (use broadcast)

    ````

### Routines
1. In the `main thread`, we take the input, create a thread for each `course` and each `student` and only when each of the student thread join the main thread back, the simulation ends.

2. In `get_input`, we make sure to `initialize` all the `locks` and `condition variables`.

2. In the `student thread`, we first initialize the `curr_cid`, `is_allocated_curr_slot` and `is_attending_curr_tut` variables. Next the thread waits on the `get_slot` condition variable (after acquiring the `student_lock`). When the student is allocated a seat in a course's tutorial or when the course is removed, `get_slot` is signalled. The thread rechecks the condition before coming out of the while loop. If the course is removed, the student changes his course preference (if the current preference was the first or second one). Otherwise, the thread now waits on the `tut_happening` CV. When the tutorial for the course starts, `tut_happening` is signalled. After the student comes out of the while loop we use rand to decide if the student selects the course. If the students does, we set `is_allocated` to `1` and the thread ends. Otherwise the the student changes his course preference. The same steps are taken for each of the preferences until the student selects a course or all the preferences are exhausted. 

3. In the `course_thread`, while the simulation is running, if there is no TA allocated to the course for now, `if (CourseList[cid].curr_ta_id == -1)`, we call the `find_ta` function which allocates a TA from the eligible labs for the course. The `find_ta` routine returns -1 if all the TAs from all the eligible labs have exhausted their maximum number of TAships, 1 if it finds and allocates a TA to the course, 0 if a TA can be allocated to the course (all TAs from all eligible labs haven't exhausted their maximum TAships yet) but none of them are available at the moment. If the routine returns -1, we call `remove_course` and the course_thread ends. Otherwise, when the course is allocated a TA, we use rand to find the number of slots for the tutorial and call `allocate_slots`. After returning from allocate_slots we call `tutorial` and this continues until the simulation ends.

4. In the `find_ta` routine,

5. In the `allocate_slots` routine, we traverse the student list and check for the students who have their current preference as the `CourseList[cid]` and who haven't been allocated a seat yet. For these students we set `is_allocated` as `1` and signal the student thread, waiting on `get_slot`. 

```c
    int aslots = 0;
    int i = 0;
    while (aslots < CourseList[cid].curr_total_slots && i < num_students)
    {
        //printf(RED "HERE %d %d %d\n", StudentList[i].id, i, aslots);
        if (StudentList[i].curr_cid == cid && StudentList[i].is_allocated_curr_slot == 0)
        {
            //student is waiting on this course and has not been allocated any slot yet
            //printf(MAG "HERE %d\n", StudentList[i].id);
            aslots++;

            //pthread_mutex_lock(&StudentList[i].student_lock);
            StudentList[i].is_allocated_curr_slot = 1;
            //pthread_mutex_unlock(&StudentList[i].student_lock);

            int rc = pthread_cond_signal(&StudentList[i].get_slot);
            assert(rc == 0);
        }

        i++;
    }
    pthread_mutex_lock(&CourseList[cid].course_lock);
    CourseList[cid].curr_slots_allocated = aslots;
    pthread_mutex_unlock(&CourseList[cid].course_lock);

````


6. In the `tutorial` routine, we traverse through the student list and for the students with their current preference as the same course, `is_allocated = 1` and `attending_curr_tut = 0`, we set `attending_curr_tut = 1`, use `pthread_cond_broadcast(&CourseList[cid].tut_happening)` to signal all the `student` threads waiting on `tut_happening`. Change the relevant variables.

```c
    pthread_mutex_lock(&CourseList[cid].course_lock);
    CourseList[cid].curr_slots_allocated = -1;
    CourseList[cid].curr_total_slots = -1;
    CourseList[cid].curr_ta_id = -1;
    CourseList[cid].curr_lab_id = -1;
    pthread_mutex_unlock(&CourseList[cid].course_lock);

    pthread_mutex_lock(&LabList[lab_id].TAs[ta_id].ta_mutex);
    LabList[lab_id].TAs[ta_id].is_busy = 0;
    pthread_mutex_unlock(&LabList[lab_id].TAs[ta_id].ta_mutex);
````

7. In the `remove_course` routine, we set `CourseList[cid].is_removed = 1`, since this is a common data structure accessed by multiple student threads and modified by the course thread, we acquire the `course_lock`. Then we signal all the student threads waiting on the `get_slot` CV.

#### Threads
* A thread for each `student`
* A thread for each `course`

#### Locks
* pthread_mutex_t ta_mutex;
* pthread_mutex_t lab_mutex;
* pthread_mutex_t course_lock;
* pthread_mutex_t student_lock;

#### Condition Variables 
* pthread_cond_t tut_happening;
* pthread_cond_t get_slot;