#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int num_students;
int num_labs;
int num_courses;
int time_elapsed;
int students_left;
pthread_mutex_t students_left_mutex;

struct TA
{
    pthread_mutex_t TA_lock;
    int number_of_tut_taken;
    int available;
};

struct lab
{
    int lab_id;
    char lab_name[100];
    int maximum_number_of_times;
    pthread_mutex_t lab_lock;
    int total_number_of_members;
    struct TA *members;
    int all_TAs_done;
};

struct course
{
    bool valid;
    bool tut_active;
    pthread_t course_thread;
    int course_id;
    char course_name[100];
    float interest_quotient;
    int course_max_slots;
    int number_of_accepted_labs;
    int accepted_labs[100];
    pthread_cond_t cond_course;
    pthread_cond_t tut_condition;
    pthread_mutex_t course_lock;
    pthread_mutex_t tut_lock;
    int waiting_for_tut;
    int waiting_for_alloc;
};

struct student
{
    bool valid;
    pthread_t student_thread;
    pthread_mutex_t student_lock;
    int student_id;
    float student_calibre;
    int current_preference;
    int preference_list[3];
    int join_time;
};

struct course *course_list;
struct lab *lab_list;
struct student *student_list;

void *course_routine(void *args)
{
    int id = *(int *)args;
    int not_available = 0;
    //printf(GREEN "Course %s has started" RESET "\n", course_list[id].course_name);
    while (students_left)
    {
        //loop through all labs
        for (int i = 0; i < num_labs; i++)
        {
            //loop through the labs this course requires
            for (int j = 0; j < course_list[id].number_of_accepted_labs; j++)
            {
                //if you find a matching lab
                if (course_list[id].accepted_labs[j] == lab_list[i].lab_id)
                {
                    //check how many member of that lab are available
                    if (lab_list[i].all_TAs_done == 0)
                    {
                        printf(MAGENTA "Lab %s doesn’t have any TA’s eligible and is removed from lab offerings" RESET "\n", lab_list[i].lab_name);

                        //if no members are available,it means
                        not_available++;
                        if (not_available >= course_list[id].number_of_accepted_labs)
                        {
                            course_list[id].valid = false;
                            /* pthread_mutex_lock(&course_list[id].course_lock);
                            //signal all the students waiting for the course to be allocated
                            pthread_cond_broadcast(&course_list[id].cond_course);
                            pthread_mutex_unlock(&course_list[id].course_lock); */
                            printf(GREEN "Course %s doesn’t have any TA’s eligible and is removed from course offerings" RESET "\n", course_list[id].course_name);
                            for (int k = 0; k < num_students; k++)
                            {
                                if (student_list[k].preference_list[student_list[k].current_preference] == id && student_list[k].valid)
                                {
                                    pthread_mutex_lock(&student_list[k].student_lock);
                                    student_list[k].current_preference++;
                                    while (course_list[student_list[k].preference_list[student_list[k].current_preference]].valid == false && student_list[k].current_preference < 3)
                                    {
                                        student_list[k].current_preference++;
                                    }
                                    if (student_list[k].current_preference >= 3)
                                    {
                                        student_list[k].valid = false;
                                        printf(CYAN "Student %d couldnt get any of his preferred courses\n" RESET, student_list[k].student_id);
                                        continue;
                                    }
                                    printf(RED "Student %d has withdrawn from course %s\n" RESET, student_list[k].student_id, course_list[id].course_name);
                                    printf(RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" RESET "\n", k, course_list[student_list[k].preference_list[student_list[k].current_preference - 1]].course_name, student_list[k].current_preference - 1, course_list[student_list[k].preference_list[student_list[k].current_preference]].course_name, student_list[k].current_preference);
                                    pthread_mutex_unlock(&student_list[k].student_lock);
                                }
                            }
                            pthread_exit(NULL);
                        }
                    }

                    for (int k = 0; k < lab_list[i].total_number_of_members; k++)
                    {
                        //if TA is available
                        if (lab_list[i].members[k].number_of_tut_taken < lab_list[i].maximum_number_of_times && lab_list[i].members[k].available == 1)
                        {
                            //lock the TA
                            pthread_mutex_lock(&lab_list[i].members[k].TA_lock);

                            //print message
                            printf(MAGENTA "TA %d from lab %s has been allocated to course %s for his %d TA ship" RESET "\n", k, lab_list[i].lab_name, course_list[id].course_name, lab_list[i].members[k].number_of_tut_taken);

                            //calculate number of seats
                            int d = rand() % course_list[id].course_max_slots + 1;

                            //print message
                            printf(GREEN "Course %s has been allocated %d seats" RESET "\n", course_list[id].course_name, d);

                            pthread_mutex_lock(&course_list[id].course_lock);
                            //conducting tut
                            course_list[id].waiting_for_alloc = 0;
                            //signal all the students waiting for the tutorial to take place
                            pthread_mutex_unlock(&course_list[id].course_lock);
                            // now signal D times the course condition
                            for (int temp = 0; temp < d; temp++)
                            {
                                pthread_cond_signal(&course_list[id].cond_course);
                            }
                            sleep(2);
                            while (course_list[id].waiting_for_alloc == 0)
                                ;
                            //modfy TA variable
                            printf(YELLOW "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, course_list[id].course_name, course_list[id].waiting_for_alloc, d);
                            course_list[id].tut_active = true;
                            lab_list[i].members[k].available = 0;
                            //conduct tutorial
                            sleep(3);
                            //modify TA parameters
                            lab_list[i].members[k].available = 1;
                            lab_list[i].members[k].number_of_tut_taken++;
                            //unlock TA
                            pthread_mutex_unlock(&lab_list[i].members[k].TA_lock);
                            printf(YELLOW "TA %d from lab %s has completed the tutorial and left the course %s" RESET "\n", k, lab_list[i].lab_name, course_list[id].course_name);

                            course_list[id].tut_active = false;
                            pthread_mutex_unlock(&course_list[id].course_lock);

                            /*  pthread_mutex_lock(&course_list[id].tut_lock);

                            pthread_cond_broadcast(&course_list[id].tut_condition);
                            pthread_mutex_unlock(&course_list[id].tut_lock); */
                        }

                        //if the TA has completed max sessions
                        else if (lab_list[i].members[k].number_of_tut_taken >= lab_list[i].maximum_number_of_times && lab_list[i].members[k].available == 1)
                        {
                            //lock the TA
                            pthread_mutex_lock(&lab_list[i].members[k].TA_lock);
                            //modfy TA variables
                            lab_list[i].members[k].available = 0;
                            //unlock TA
                            pthread_mutex_unlock(&lab_list[i].members[k].TA_lock);

                            //lock the lab
                            pthread_mutex_lock(&lab_list[i].lab_lock);
                            //modfy lab variables
                            lab_list[i].all_TAs_done--;
                            //unlock lab
                            pthread_mutex_unlock(&lab_list[i].lab_lock);
                        }
                    }
                }
            }
        }
    }
}

void *student_routine(void *args)
{

    int id = *(int *)args;
    sleep(student_list[id].join_time);
    printf(RED "Student %d has filled in preferences for course registration" RESET "\n", id);

#define course_number student_list[id].preference_list[student_list[id].current_preference]
    //pthread_cond_wait(&condLock[*(int *)args],NULL);

L:
    if (course_list[course_number].valid == false)
    {
        if (student_list[id].current_preference < 3)
        {
            student_list[id].current_preference++;
        }

        if (student_list[id].current_preference >= 3)
        {
            student_list[id].valid = false;
            printf(CYAN "Student %d couldnt get any of his preferred courses\n" RESET, student_list[id].student_id);
            pthread_mutex_lock(&students_left_mutex);
            students_left--;
            pthread_mutex_unlock(&students_left_mutex);
            pthread_exit(NULL);
        }

        printf(RED "Student %d has withdrawn from course %s\n" RESET, student_list[id].student_id, course_list[course_number - 2].course_name);
        printf(RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" RESET "\n", student_list[id].student_id, course_list[course_number - 1].course_name, student_list[id].current_preference - 1, course_list[course_number].course_name, student_list[id].current_preference);
        goto L;
    }

    pthread_mutex_lock(&course_list[course_number].course_lock);
    course_list[course_number].waiting_for_alloc++;
    pthread_cond_wait(&course_list[course_number].cond_course, &course_list[course_number].course_lock);

    if (course_list[course_number].valid == false)
    {
        if (student_list[id].current_preference < 3)
        {
            student_list[id].current_preference++;
        }

        if (student_list[id].current_preference >= 3)
        {
            student_list[id].valid = false;
            printf(CYAN "Student %d couldnt get any of his preferred courses\n" RESET, student_list[id].student_id);
            pthread_mutex_lock(&students_left_mutex);
            students_left--;
            pthread_mutex_unlock(&students_left_mutex);
            pthread_exit(NULL);
        }

        printf(RED "Student %d has withdrawn from course %s\n" RESET, student_list[id].student_id, course_list[course_number - 2].course_name);
        printf(RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" RESET "\n", student_list[id].student_id, course_list[course_number - 1].course_name, student_list[id].current_preference - 1, course_list[course_number].course_name, student_list[id].current_preference);
        goto L;
    }

    //if the course is not withdrawn, then student is allocated to the course
    printf(RED "Student %d has been allocated a seat in course %s\n" RESET, id, course_list[course_number].course_name);
    pthread_mutex_unlock(&course_list[course_number].course_lock);

    //now student is allocated to the course, he waits for the tutorial to take place
    /* pthread_mutex_lock(&course_list[course_number].tut_lock);
    //increment the number of students waiting for the tutorial
    course_list[course_number].waiting_for_tut++;
    //wait for the tutorial to take place
    pthread_cond_wait(&course_list[course_number].tut_condition, &course_list[course_number].tut_lock);
    pthread_mutex_unlock(&course_list[course_number].tut_lock); */

    int val = rand() % 100;

    if (course_list[course_number].interest_quotient * student_list[id].student_calibre * 100 > val)
    {
        student_list[id].valid = false;
        printf(CYAN "Student %d has selected course %s permanently\n" RESET, student_list[id].student_id, course_list[course_number].course_name);
        pthread_mutex_lock(&students_left_mutex);
        students_left--;
        pthread_mutex_unlock(&students_left_mutex);
        pthread_exit(NULL);
    }

    else
    {

        if (student_list[id].current_preference < 3)
        {
            student_list[id].current_preference++;
        }

        if (student_list[id].current_preference >= 3)
        {
            student_list[id].valid = false;
            printf(CYAN "Student %d couldnt get any of his preferred courses\n" RESET, student_list[id].student_id);
            pthread_mutex_lock(&students_left_mutex);
            students_left--;
            pthread_mutex_unlock(&students_left_mutex);
            pthread_exit(NULL);
        }

        printf(RED "Student %d has withdrawn from course %s\n" RESET, student_list[id].student_id, course_list[course_number - 2].course_name);
        printf(RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" RESET "\n", student_list[id].student_id, course_list[course_number - 1].course_name, student_list[id].current_preference - 1, course_list[course_number].course_name, student_list[id].current_preference);
        goto L;
    }

    student_list[id].valid = false;
    return NULL;
}

//////////////////////////////////////////////////////////////////

int main(int argc, char const *argv[])
{
    pthread_mutex_init(&students_left_mutex, NULL);
    //input
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    students_left = num_students;
    course_list = (struct course *)malloc(sizeof(struct course) * num_courses);
    student_list = (struct student *)malloc(sizeof(struct student) * num_students);
    lab_list = (struct lab *)malloc(sizeof(struct lab) * num_labs);

    for (int i = 0; i < num_courses; i++)
    {
        if (scanf("%s %f %d %d", course_list[i].course_name, &course_list[i].interest_quotient, &course_list[i].course_max_slots, &course_list[i].number_of_accepted_labs))
        {
            course_list[i].valid = true;
            course_list[i].course_id = i;
            course_list[i].waiting_for_tut = 0;
            course_list[i].waiting_for_alloc = 0;
            course_list[i].tut_active = false;
            for (int j = 0; j < course_list[i].number_of_accepted_labs; j++)
            {
                scanf("%d", &course_list[i].accepted_labs[j]);
            }
            pthread_cond_init(&course_list[i].cond_course, NULL);
            pthread_cond_init(&course_list[i].tut_condition, NULL);
            pthread_mutex_init(&course_list[i].course_lock, NULL);
            pthread_mutex_init(&course_list[i].tut_lock, NULL);
        }
    }

    for (int i = 0; i < num_students; i++)
    {
        if (scanf("%f %d %d %d %d", &student_list[i].student_calibre, &student_list[i].preference_list[0], &student_list[i].preference_list[1], &student_list[i].preference_list[2], &student_list[i].join_time))
        {
            student_list[i].valid = true;
            student_list[i].student_id = i;
            student_list[i].current_preference = 0;
            pthread_mutex_init(&student_list[i].student_lock, NULL);
        }
    }

    for (int i = 0; i < num_labs; i++)
    {
        if (scanf("%s %d %d", lab_list[i].lab_name, &lab_list[i].total_number_of_members, &lab_list[i].maximum_number_of_times))
        {
            lab_list[i].lab_id = i;
            lab_list[i].all_TAs_done = lab_list[i].total_number_of_members;
            lab_list[i].members = (struct TA *)malloc(sizeof(struct TA) * lab_list[i].total_number_of_members);
            for (int j = 0; j < lab_list[i].total_number_of_members; j++)
            {
                pthread_mutex_init(&lab_list[i].members[j].TA_lock, NULL);
                lab_list[i].members[j].available = true;
                lab_list[i].members[j].number_of_tut_taken = 0;
            }
            pthread_mutex_init(&lab_list[i].lab_lock, NULL);
        }
    }

    int i = 0;
    //starting course_threads
    while (i < num_courses)
    {
        int *arg = malloc(sizeof(int));
        *arg = i;
        if (pthread_create(&course_list[i].course_thread, NULL, &course_routine, arg) != 0)
            perror("Failed to create thread");
        i++;
    }

    i = 0;
    //starting studnet_threads
    while (i < num_students)
    {
        int *arg = malloc(sizeof(int));
        *arg = i;
        if (pthread_create(&student_list[i].student_thread, NULL, &student_routine, arg) != 0)
            perror("Failed to create thread");
        i++;
    }

    //main loop
    time_elapsed = 0;
    int check;
    while (students_left)
    {
        time_elapsed++;
        sleep(1);
    }

    pthread_mutex_destroy(&students_left_mutex);

    for (int j = 0; j < num_students; j++)
    {
        if (pthread_join(student_list[j].student_thread, NULL) != 0)
            perror("Failed to join sthread");
    }

    for (int j = 0; j < num_courses; j++)
    {
        if (pthread_cancel(course_list[j].course_thread) != 0)
            perror("Failed to cancel cthread");
    }

    exit(EXIT_SUCCESS);
    return 0;
}