Each student and course is a thread
The course threads terminate when no TAs/ no students are available
The student threads terminate when none of the the courses they prefer are no longer available

The routine function for the courses' threads loops while students still exist

The students' threads wait for the preferred course to hold a tutorial

In more detail, under the courses routine function we
1. loop through all labs the labs this course requires
2. check how many members of the lab are available once we find the matching lab
3. If no members are available we get rid of the lab by flagginng it and sending a message
4. Try another lab, if we find a TA, we first lock the TA
5. after locking the TA we calculate a random number of seats and signal those many students
6. after ensureing the students responded we conduct the tutorial
7. if no TAs are found we get rid of the course by flagging it and sending a message
8. we modfy the students' preferences accordingly

In more detail, under the students routine function we
1. we check if our current preferred course is valid i.e. it has not been flagged
2. if it  not valid we go the next preference
3. if non of our preferences exist we send a message and exit
4. if our current preference is valid we try to lock onto the course mutex and increment a vraible to show that we are ready
5. we wait for the tut to get over
6. we then perform the steps as psecified in teh docuemnts to accept/ reject the course 

