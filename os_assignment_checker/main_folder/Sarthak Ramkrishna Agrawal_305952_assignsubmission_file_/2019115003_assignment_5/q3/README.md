# Q3

## Run

- server

```bash
g++ -pthread server.cpp -o server
./server
```

- client

```bash
g++ -pthread client.cpp -o client
./client
```

## Approach

#### Server

- Pool of worker threads, handled by a queue

`main()`

- Modified boilerplate mostly
- Init an array of threads, size taken as argument
- Every time a request is identified (the while loop), we add the socket id (file descriptor) to a queue, and signal the threads that they may access the queue now, since it is not empty anymore.
- Then we spawn all the worker processes

`thread_function()`

- Function that handles threads. Allocates each to one request.
- Wait till the queue is non-empty.
- Once true, we take the first request and dequeue it.
- Then the request is handled.

`handle_connection()`

- First read from socket, and tokenise the request wrt spaces
- Handle error cases of too many or less words.
- Obtain the key number, check for correct range.
- Make sure the command is acceptable.
- Lock a mutex corresponding to the key (index "key" in the array of mutexes)
- Handle the correct command, and the errors specified in the doc, with appropriate messages
- Send response to client and close socket

#### Client

- Use a struct for maintaining relevant information of the request:
  - Time
  - Command
  - Index
- Take input and spawn threads.
- Obtain the timestamp for each request and have the respective thread sleep for that time.
- Send the command and wait for request
- Lock the STDIO and print reponse, then unlock it. This prevents two threads overlapping output.

## Assumptions

- stio errors not handled, as not specified
- segmentation faults for lack of memory if too many threads are spawned are not handled, as not specified
