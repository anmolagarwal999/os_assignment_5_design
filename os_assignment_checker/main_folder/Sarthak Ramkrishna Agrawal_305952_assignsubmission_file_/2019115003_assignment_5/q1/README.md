# Q1

### Data structures

- Maintain a 2-d array for TAs, with TAs for each lab as a separate row
- Struct for student
  - ID
  - calibre
  - preferences (string array of size 3)
  - course_allocation: tells whether student is allocated, and if yes then to which course
  - tut_allocation: tells whether student is attending a tut, and if yes then to which tut
- Struct for Courses
  - ID
  - Name
  - Interest
  - Maximum seats
  - List of labs from which we can select TAs
  - Boolean for if it exists
  - Bool for if a tut is running
  - Conditional variable for if tut has ended

### Student simulation

- Iterate over preferences
  - try to join a tut of current preference course.
    - If a tut of the course is ongoing, wait for it to end
    - If successful, sleep till courses signals tut is over.
      - find the probability of joining and generate a random number. If number is greater than probability of joining, then you dont like the course.
      - If current preference is 2 (highest), exit simulation (mutex lock needed here) and not choose any course
    - Else continue to next preference

### Courses

- Remove courses that dont exist at the start (in main)
- Exit if no students left
- if none have the course as prefernce, wait
- Iterate over all TAs and check if they are free and eligible for TAship (have not already been TA over the lab limit)
- If found, allocate TA to course (use mutex lock here)
  - Use a semaphore to allocate students to the tutorial for this course.
  - Iterate over students, ensure their preference is this course, they have not already picked a course, and the course slots (random number between 1 and max) are not full
  - Now we start the tut, and then sleep for 3s
  - Once tut is finished - signal the participating students the same. - Free the TA and increase the number of times they have TAed
- If TA not found, check if any TA is still eligible for TAship
  - If yes, continue waiting
  - Else exit since no TA available, signal that this course is dead
