## Running instructions:

1. To run the server code , compile the server with g++ server.cpp -pthread and execute it with ./a.out n, where n is the number of worker threads.

2. To run the client code , compile it with g++ client.cpp -pthread and execute it with ./a.out.

## Client side code:

1. On the client side , we first input the m queires and store them in an array of structures.The structure input has a string field for the query and another string field for the request id. We then create a pool of m threads to handle the m queries. Each thread runs the function "thread_function".
2. Role of thread_function: In this function, the threads communicate the query to the server. Initially, the threads are made to sleep for time t( time after which the query should be sent), and then the query is sent on the socket to the server.
3. After the query is processed on the server , output is returned to the client side using the function read_string_from_socket. This ouput is then printed on the client side.

## Networking details :

In my code , the server and client are both connected to port 8002. There are two helper functions read_string_from_socket and send_string_on_socket that are extremely useful in communicating between the server and the client.

## Server Side Code :

1. Initially , the thread of worker pools is created based on the number speciied in the input to the function.
2. After this, we create a queue of all the queries sent to the server side from the client side. Everytime a new query is sent , this query is pushed into the queue.

3. The threads execute the function thread_function. In this function, the threads are made to sleep using a conditional variable condt while the queue is empty. As soon as a query enters the queue , a thread is woken up and starts implementing this query. The implementation of each query is done in the handle connections function.

4. To handle the data structures part of the question, I used a map data structure to simulate a dictionary. The map server_dict maintains all the keys and values from the different queries.

5. In the handle connections function , we first get the id of the thread that is handling this query using pthread_self(). Then, the query is tokenised and compared against the different specified commands insert, update ,delete, concat, and fetch. At this point , the respective map element is locked and the the command is carried out. For example, if index 13 has to be updated, the thread first acquires the lock corresponding to index 13 from an array of 105 locks, one for each of the possible index values. Then the update occurs, which ensures that the operations are thread safe.
   After completing the command (update,insert,etc), the thread releases the lock.

6. The output of the different commands is recorded and then sent back to the client side using the sent_string_on_socket function, where the final output is displayed.
