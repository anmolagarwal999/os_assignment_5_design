#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <typeinfo>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <map>
#include <string>
#include <queue>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;
pthread_mutex_t socket_lock = PTHREAD_MUTEX_INITIALIZER;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8002

const int initial_msg_len = 256;
pthread_cond_t condt = PTHREAD_COND_INITIALIZER;

////////////////////////////////////

const LL buff_sz = 1048576;

map<int, string> server_dict;
queue<int> socket_info;
pthread_mutex_t dict_lock[105];
pthread_mutex_t output_lock;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const char *s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s, strlen(s));
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        // cout << "Client sent : " << cmd << endl;
        // split the string at the spaces
        int start = 0;
        int count = 0;
        string arr[5];
        long long int thread_id;
        thread_id = pthread_self();
        int request_id;
        for (int i = 0; cmd[i] != '\0'; i++)
        {
            if (cmd[i] == ' ')
            {
                arr[count] = cmd.substr(start, i - start);
                count++;
                start = i + 1;
            }
        }
        arr[count] = cmd.substr(start);
        count++;
        // tokenised
        int last = count - 1;
        string out = "";
        out = ":" + to_string(thread_id) + ":";
        if (arr[1] == "insert")
        {
            int index = stoi(arr[2]);
            pthread_mutex_lock(&dict_lock[index]);
            if (server_dict.find(index) == server_dict.end())
            {
                server_dict.insert(std::pair<int, string>(index, arr[3]));
                out += "Insertion Successful";
            }
            else
            {
                out += "Key already exists";
            }
            pthread_mutex_unlock(&dict_lock[index]);
        }
        else if (arr[1] == "delete")
        {
            int index = stoi(arr[2]);
            pthread_mutex_lock(&dict_lock[index]);
            if (server_dict.find(index) == server_dict.end())
            {
                out += "No such key exists";
            }
            else
            {
                map<int, string>::iterator iter = server_dict.find(index);
                server_dict.erase(iter);
                out += "Deletion Successful";
            }
            pthread_mutex_unlock(&dict_lock[index]);
        }
        else if (arr[1] == "update")
        {
            int index = stoi(arr[2]);
            pthread_mutex_lock(&dict_lock[index]);
            if (server_dict.find(index) == server_dict.end())
            {
                out += "Key does not exist";
            }
            else
            {
                map<int, string>::iterator iter = server_dict.find(index);
                (*iter).second = arr[3];
                out += arr[3];
            }
            pthread_mutex_unlock(&dict_lock[index]);
        }
        else if (arr[1] == "concat")
        {
            int index1 = stoi(arr[2]);
            int index2 = stoi(arr[3]);
            pthread_mutex_lock(&dict_lock[index1]);
            pthread_mutex_lock(&dict_lock[index2]);
            if ((server_dict.find(index1) == server_dict.end()) or (server_dict.find(index2) == server_dict.end()))
            {
                out += "Concat failed as at least one of the keys does not exist";
            }
            else
            {
                string s1 = server_dict[index1];
                string s2 = server_dict[index2];
                map<int, string>::iterator iter = server_dict.find(index1);
                map<int, string>::iterator iter1 = server_dict.find(index2);
                (*iter).second = s1 + s2;
                (*iter1).second = s2 + s1;
                out += (*iter1).second;
            }
            pthread_mutex_unlock(&dict_lock[index2]);
            pthread_mutex_unlock(&dict_lock[index1]);
        }
        else if (arr[1] == "fetch")
        {
            int index = stoi(arr[2]);
            pthread_mutex_lock(&dict_lock[index]);
            if (server_dict.find(index) == server_dict.end())
            {
                out += "No such key exists";
            }
            else
            {
                map<int, string>::iterator iter = server_dict.find(index);
                out += (*iter).second;
            }
            pthread_mutex_unlock(&dict_lock[index]);
        }
        else
        {
            out += "Command not found";
        }
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        // // cout << output << endl;
        // string msg_to_send_back = output;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        // cout << typeid(output).name() << endl;
        // output = string(output).c_str();

        int sent_to_client = send_string_on_socket(client_socket_fd, out.c_str());
        sleep(2);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        close(client_socket_fd);
        return;
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *thread_function(void *arg)
{

    int client_socket;
    while (true)
    {
        pthread_mutex_lock(&socket_lock);
        while (socket_info.empty())
        {
            pthread_cond_wait(&condt, &socket_lock);
        }
        client_socket = socket_info.front();
        socket_info.pop();
        pthread_mutex_unlock(&socket_lock);
        handle_connection(client_socket);
    }
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    pthread_mutex_t push_lock = PTHREAD_MUTEX_INITIALIZER;

    for (int k = 0; k < 105; k++)
    {
        pthread_mutex_init(&dict_lock[k], NULL);
    }
    pthread_mutex_init(&output_lock, NULL);
    int number_of_threads = atoi(argv[1]);
    cout << number_of_threads << endl;

    pthread_t thread_pool[number_of_threads];

    for (int thread_create = 0; thread_create < number_of_threads; thread_create++)
    {
        pthread_create(&thread_pool[thread_create], NULL, thread_function, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    // create the thread pool
    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call

        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&socket_lock);
        socket_info.push(client_socket_fd);
        pthread_cond_signal(&condt);
        pthread_mutex_unlock(&socket_lock);
        // handle_connection(client_socket_fd);
    }
    for (int j = 0; j < number_of_threads; j++)
    {
        pthread_join(thread_pool[j], NULL);
    }

    close(wel_socket_fd);
    return 0;
}