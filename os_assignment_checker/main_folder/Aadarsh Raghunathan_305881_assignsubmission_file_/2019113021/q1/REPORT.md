## Structures Used :

### struct students:

This structure maintains information of the student id along with information about the calibre and course preferences of the each student. It also has information about which course the student has currently selected. The variables done and picked tell us whether the student has either selected a course permanently or have been allocated a seat in a tutorial. The struct also contains a lock for each student , so that it can be woken up using conditional variables.

### struct courses :

This structure maintains all information about the courses. This information includes the course id , name, interest , max slots and the list of labs from which the course can select TA's. Apart from this , this struct also contains two variables running and exist , which represent whether a tutorial is running and whether the course still exists respectively. Finally , the struct contains a conditional variable general_tut_end that signals all students whenever a particular tutorial end.

### struct labs:

The labs struct maintains information about the number of potential TA's available in that particular lab and also the maximum number of times that each mentor can become a TA.

### struct ta:

This struct stores information about each ta including whether the ta's are busy and the total number of times they have become ta's.

### Arrays :

Apart from the 4 strucutures listed above , I used 4 arrays s_arr[N],l_arr[N],c_arr[N] , and t_arr[N][m]. These 4 arrays maintain the list of all students , labs , courses , and ta's respectively.

## Logic:

I have divided the code into two broad functions , student_simulation and course_simulation. Each student is simulated using the function student_simulation and each course is simulated using the function course_simulation. Independent threads are used to simulate each student and each course.

### Student Logic:

1. Iterate through the preferences for the student.

2. If a tutorial is going on in the current preference of the student , then wait for the tutorial to finish.

3. After this , the students are set to sleep on a conditional variable waiting for the tutorial to end. In case the course gets deleted while students are waiting , then a signal is sent to wake up and release all the students.

4. After the tutorial ends , students either select the course with probability calibre\*interest , or move onto their next preference.

### Course Logic:

1. First check if the tutorial still exists , that is , there are still ta's who can take tutorials for the course.

2. Iterate through all ta's and select a ta who is suitable for the course.

3. Allow students to enter the tutorial , as soon as the tutorial is over , use the conditional variable to wake up all students.

4. Repeat steps 1-3 in a while loop until the course can no longer exist , i.e., there are no longer any ta's available.
