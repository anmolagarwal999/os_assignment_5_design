# Assignment 5
### Q3
Q3 requires us to maintain a dictionary of sorts in the server side which contain a string and a key from which certain operations like 
insertion , deletion, updation and concatenation and fetching can happen from the client side.

The server and client programs can be compiled using the makefile and 
Server
```./server <number of threads>```
Client
```./client```

Both the Server and client communicate using TCP-IP sockets and both have thread pools for better and parallel communication.
The server has a thread pool of given size and a queue kind of structure is used to enqueue and dequeue work.
the queue is used to handle each new connection to a thread and dequeue when done.
The dictionary as well as the queue are between mutex locks so as to be thread-safe.
The client side too has a thread-pool equal to the number of requests and sends requests to the server at the required time.
There is no busy waiting and a conditional lock has also been implemented when dequeueing.
