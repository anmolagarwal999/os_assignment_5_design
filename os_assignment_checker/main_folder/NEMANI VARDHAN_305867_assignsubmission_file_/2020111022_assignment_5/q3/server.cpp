#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <queue>
#include <thread>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 6969
LL thread_pool_size;
const int initial_msg_len = 256;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
////////////////////////////////////
//pair<int, string> wordmap[100];
struct arrnode{
    string message;
    int temp;
};
struct arrnode wordmap[101];
const LL buff_sz = 1048576;
///////////////////////////////////////////////////
// queue stuff
void cleaner()
{
    for (int i = 0; i < 100; i++)
    {
        wordmap[i].message = "";
        wordmap[i].temp = 0;
    }
}
struct node
{
    struct node *next;
    int *client_socket;
};
typedef struct node node;

node *head = NULL;
node *tail = NULL;
void enqueue(int *client_sock)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->client_socket = client_sock;
    temp->next = NULL;
    if (tail == NULL)
    {
        head = temp;
    }
    else
    {
        tail->next = temp;
    }
    tail = temp;
}

int *dequeue()
{
    if (head == NULL)
    {
        return NULL;
    }
    else
    {
        node *temp = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
        }
        return temp->client_socket;
    }
}
vector<string> simple_tokenizer(string s)
{
    vector<string> tokens;
    char str[s.length() + 1];
    strcpy(str, s.c_str());
    char *token = strtok(str, " ");

    // Keep printing tokens while one of the
    // delimiters present in str[].
    while (token != NULL)
    {
        tokens.pb(token);
        // printf("Regards %s\n", token);
        token = strtok(NULL, " ");
    }
    return tokens;
}
//////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *p_client_socket_fd)
{
    string threadid = to_string(pthread_self());
    // cout<<threadid<<endl;
    int client_socket_fd = *(int *)p_client_socket_fd;
    free(p_client_socket_fd);
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;
    int sent;
    //while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        // string msg_to_send_back = "Ack: " + cmd;
        
        // vector works
        vector<string> words = simple_tokenizer(cmd);
        string msg_to_send_back;
        // for(int i=0;i<words.size();i++)
        // {
        //     cout<<"from vector"<<words[i]<<endl;
        // }
        if (words[1] == "insert")
        {
            msg_to_send_back = words[4]+" : "+threadid;
            cout << "#debug1"
                 << " " << wordmap[atoi(words[2].c_str())].temp << " " << wordmap[atoi(words[2].c_str())].message << "!debug1 " << endl;
            if (wordmap[atoi(words[2].c_str())].temp == 0)
            {
                pthread_mutex_lock(&mutex);
                wordmap[atoi(words[2].c_str())].temp = atoi(words[0].c_str());
                wordmap[atoi(words[2].c_str())].message = words[3];
                pthread_mutex_unlock(&mutex);
                msg_to_send_back += ": Insertion Successful";
            }
            else
            {
                msg_to_send_back += ": Key already exists";
            }
            //msg_to_send_back+=" index is "+words[4];
            cout << "#debug2"
                 << " " << wordmap[atoi(words[2].c_str())].temp << " " << wordmap[atoi(words[2].c_str())].message << "!debug2" << endl;
            // int sent = send_string_on_socket(client_socket_fd, msg_to_send_back);
        }
        if (words[1] == "delete")
        {
            msg_to_send_back = words[3]+" : "+threadid;
            if (wordmap[atoi(words[2].c_str())].temp == 0 && wordmap[atoi(words[2].c_str())].message == "")
            {
                msg_to_send_back += ": No such key exists";
            }
            else
            {
                pthread_mutex_lock(&mutex);
                wordmap[atoi(words[2].c_str())].temp = 0;
                wordmap[atoi(words[2].c_str())].message = "";
                pthread_mutex_unlock(&mutex);
                msg_to_send_back += ": Deletion successful";
            }
            //msg_to_send_back+=" index is "+words[3];
            // int sent = send_string_on_socket(client_socket_fd, msg_to_send_back);
        }
        if (words[1] == "update")
        {
            msg_to_send_back = words[4]+" : "+threadid;
            if (wordmap[atoi(words[2].c_str())].temp == 0 && wordmap[atoi(words[2].c_str())].message == "")
            {
                msg_to_send_back += ": Key does not exist";
            }
            else
            {
                pthread_mutex_lock(&mutex);
                wordmap[atoi(words[2].c_str())].message = words[3];
                pthread_mutex_unlock(&mutex);
                msg_to_send_back += ": " + wordmap[atoi(words[2].c_str())].message;
            }
            //msg_to_send_back+=" index is "+words[4];
            // int sent = send_string_on_socket(client_socket_fd, msg_to_send_back);
        }
        if (words[1] == "concat")
        {
            msg_to_send_back = words[4]+" : "+threadid;
            if (wordmap[atoi(words[2].c_str())].temp == 0 || wordmap[atoi(words[3].c_str())].temp == 0)
            {
                msg_to_send_back += ": Concat failed as at least one of the keys does not exist";
            }
            else
            {
                string temp1 = wordmap[atoi(words[2].c_str())].message;
                string temp2 = wordmap[atoi(words[3].c_str())].message;
                pthread_mutex_lock(&mutex);
                wordmap[atoi(words[2].c_str())].message = temp1 + temp2;
                wordmap[atoi(words[3].c_str())].message = temp2 + temp1;
                pthread_mutex_unlock(&mutex);
                // printf("1st word: %s\n", wordmap[atoi(words[2].c_str())].message.c_str());
                // printf("2nd word: %s\n", wordmap[atoi(words[3].c_str())].message.c_str());
                msg_to_send_back += ": " + wordmap[atoi(words[3].c_str())].message;
            }
            //msg_to_send_back+=" index is "+words[4];
            // int sent = send_string_on_socket(client_socket_fd, msg_to_send_back);
        }
        if (words[1] == "fetch")
        {
            msg_to_send_back = words[3]+" : "+threadid;
            if (wordmap[atoi(words[2].c_str())].temp == 0 && wordmap[atoi(words[2].c_str())].message == "")
            {
                msg_to_send_back += ": Key does not exist";
            }
            else
            {
                msg_to_send_back += ": " + wordmap[atoi(words[2].c_str())].message;
            }
            //msg_to_send_back+=" index is "+words[3];
            // int sent = send_string_on_socket(client_socket_fd, msg_to_send_back);
        }
        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}
void *thread_function(void *temp)
{
    while (1)
    {
        pthread_mutex_lock(&mutex);
        int *client;
        if((client=dequeue())==NULL)
        {
            pthread_cond_wait(&cond,&mutex);
            client=dequeue();
        }
        pthread_mutex_unlock(&mutex);
        if (client != NULL)
        {
            handle_connection(client);
        }
    }
}
int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    cleaner();
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    thread_pool_size = atoi(argv[1]);
    cout << "thread pool size" << thread_pool_size << endl;
    pthread_t thread_pool[thread_pool_size];
    for (int i = 0; i < thread_pool_size; i++)
    {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    // CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, thread_pool_size);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client.
        */
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        // handle_connection(client_socket_fd);
        pthread_t t;
        int *pclient = new int(client_socket_fd);
        // client_queue.push(pclient);
        // pthread_create(&t, NULL, handle_connection, pclient);
        pthread_mutex_lock(&mutex);
        enqueue(pclient);
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
    }

    close(wel_socket_fd);
    return 0;
}