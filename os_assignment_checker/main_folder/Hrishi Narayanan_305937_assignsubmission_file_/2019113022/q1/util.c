// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern availability_type availability;

void get_input()
{
    scanf("%d %d %d", &input.students, &input.labs, &input.courses);

    set.students = malloc(sizeof(student_type) * input.students);
    set.labs = malloc(sizeof(lab_type) * input.labs);
    set.courses = malloc(sizeof(course_type) * input.courses);

    for (int i = 0; i < input.courses; i++)
    {
        set.courses[i].id = i;
        scanf("%s %f %d %d", set.courses[i].name, &set.courses[i].interest, &set.courses[i].course_max_slots, &set.courses[i].num_labs);

        set.courses[i].labs = malloc(sizeof(int) * set.courses[i].num_labs);

        for (int j = 0; j < set.courses[i].num_labs; j++)
            scanf("%d", &set.courses[i].labs[j]);
    }

    for (int i = 0; i < input.students; i++)
    {
        set.students[i].id = i;
        scanf("%f %d %d %d %d", &set.students[i].calibre, &set.students[i].preferences[0], &set.students[i].preferences[1], &set.students[i].preferences[2], &set.students[i].fill_time);
    }

    for (int i = 0; i < input.labs; i++)
    {
        set.labs[i].id = i;
        scanf("%s %d %d", set.labs[i].name, &set.labs[i].num_tas, &set.labs[i].max_courses);
    }
}

void initialise_all()
{
    locks.lab_tas = malloc(sizeof(pthread_mutex_t *) * input.labs);
    locks.course_spots = malloc(sizeof(pthread_mutex_t) * input.courses);
    locks.student_status = malloc(sizeof(pthread_mutex_t) * input.students);

    availability.lab_tas = malloc(sizeof(pthread_cond_t *) * input.labs);
    availability.course_spots = malloc(sizeof(pthread_cond_t) * input.courses);
    availability.student_status = malloc(sizeof(pthread_cond_t) * input.students);

    status.lab_tas = malloc(sizeof(int **) * input.labs);
    status.course_spots = malloc(sizeof(int) * input.courses);
    status.student_status = malloc(sizeof(int) * input.students);

    for (int i = 0; i < input.labs; i++)
    {
        locks.lab_tas[i] = malloc(sizeof(pthread_mutex_t) * set.labs[i].num_tas);
        availability.lab_tas[i] = malloc(sizeof(pthread_cond_t) * set.labs[i].num_tas);
        status.lab_tas[i] = malloc(sizeof(int *) * set.labs[i].num_tas);

        for (int j = 0; j < set.labs[i].num_tas; j++)
        {
            status.lab_tas[i][j] = malloc(sizeof(int) * 2);
            status.lab_tas[i][j][0] = set.labs[i].max_courses;
            status.lab_tas[i][j][1] = 1;
            pthread_mutex_init(&locks.lab_tas[i][j], NULL);
            pthread_cond_init(&availability.lab_tas[i][j], NULL);
        }
    }

    for (int i = 0; i < input.courses; i++)
    {
        status.course_spots[i] = 0;
        pthread_mutex_init(&locks.course_spots[i], NULL);
        pthread_cond_init(&availability.course_spots[i], NULL);
    }

    for (int i = 0; i < input.students; ++i)
    {
        status.student_status[i] = -1 * (set.students[i].preferences[0] + 1);
        pthread_mutex_init(&locks.student_status[i], NULL);
        pthread_cond_init(&availability.student_status[i], NULL);
    }
}

