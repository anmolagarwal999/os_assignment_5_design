# Question 1 : An alternate course allocation portal

## Compiling and running the code

- To compile the code, use the command:
  ```sh
  make
  ```
- Alternatively, you can compile the code as:
  ```sh
  gcc -o q1 main.c sims.c util.c -lpthread
  ```
- The compiled executable `q1` can be run with:
  ```sh
  ./q1
  ```
- The given testcases are present in `test.txt` and can be run with the code as:
  ```sh
  ./q1 < test.txt
  ```

## Structuring of the code

- Data structures used in the code are defined in `defs.h`
- The main logic of the code is contained in `main.c`
- The `util.c` contains the utility functions, which are taking the input and initialisation of mutex
- The threads are run as simulation of either courses or students within the `sims.c`

## The Data Structures: `defs.h`

- `student_type` struct contains all the details pertaining to a single student
  ```c
    typedef struct student_type
    {
        int id;
        int fill_time;
        float calibre;
        int preferences[MAX_PREF];
        int current_course_preference;
        int current_preference_number;
    } student_type;
  ```
- `lab_type` struct contains all the details pertaining to a single lab
  ```c
    typedef struct lab_type
    {
        int id;
        char name[MAX_NAME];
        int num_tas;
        int max_courses;
    } lab_type;
  ```
- `course_type` struct contains all the details pertaining to a single course
  ```c
    typedef struct thread_types
    {
        pthread_t *students;
        pthread_t *courses;
    } threads_type;
  ```
- The status corresponding to assignment of TAs to labs(`lab_tas`), assignment of course spots (`course_spots`) and students assignment to a course (`student_status`) are stored in the struct `status_type`
  ```c
    typedef struct status_type
    {
        int ***lab_tas;
        int *course_spots;
        int *student_status;
    } status_type;
  ```
- The locks corresponding to assignment of TAs to labs(`lab_tas`), assignment of course spots (`course_spots`) and students assignment to a course (`student_status`) are stored in struct `locks_type`
  ```c
  typedef struct locks_type
  {
      pthread_mutex_t **lab_tas;
      pthread_mutex_t *course_spots;
      pthread_mutex_t *student_status;
  } locks_type;
  ```
- The availability corresponding to assignment of TAs to labs(`lab_tas`), assignment of course spots (`course_spots`) and students assignment to a course (`student_status`) are stored in struct `availability_type`
  ```c
  typedef struct availability_type
  {
      pthread_cond_t **lab_tas;
      pthread_cond_t *course_spots;
      pthread_cond_t *student_status;
  } availability_type;
  ```

## Main logic: `main.c`

- `get_input()` and `initialise_all()` are used to get input and intialise mutex respectively
- After initialisation, threads are created. Then the program waits for all the threads to finish:

  ```c
      threads.courses = malloc(sizeof(pthread_t) * input.courses);
      threads.students = malloc(sizeof(pthread_t) * input.students);

      for (int i =0; i<input.students; i++)
          pthread_create(&threads.students[i], NULL, student_simulator, &set.students[i]);

      for (int i =0; i<input.students; i++)
          pthread_join(threads.students[i], NULL);
  ```

- Note that the program only waits for the student threads to finish and not the courses.

## Utility functions: `util.c`

- The `get_input()` function is use to take and store the inputs recieved.
- The `initialise_all()` initialises all threads and corresponding mutex.

## Simulation of courses and students: `sims.c`

- Each student is simulated by the `student_simulator()` function:

  - Each student sleeps for the `fill_time` value provided as input
  - The student loops over all three preferences and status is updated accordingly
  - If there is a spot in the prefered course, then the course is assigned to student
  - Else, the students waits for the spot to open up. If there is no spot available in any of the prefered courses, then the student thread ends.
  - During tutorial, status of the student is updated to show that the student is in tutuorial. This is updated once again when the TA releases the student
  - The course is withdrawn or finalise based on random probability value
  - In case the course is removed or the student withdraws, it is updated accordingly

- Each course is simulated by the `course_simulator` function:
  - The course iterates over each lab and chooses the first TA that is applicable to the course.
  - In case the selected TA is currently busy, the course will wait for the TA to become available
  - The course is removed if no TA is assigned to it
  - This opens up slots for the course. The course is broadcasted to have empty spots if there are students waiting and there are empty spots, or untill atleast one seat is filled
  - Once the broadcasting is stopped start the tutorial with the students in the course. The TA is made to do tutorials corresponding the the lab for the course, and this is updated accordingly
  - The student threads are release after the tutorial is done
