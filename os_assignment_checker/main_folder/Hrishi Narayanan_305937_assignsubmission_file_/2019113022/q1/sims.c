// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern availability_type availability;

void *student_simulator(void *args)
{
    student_type student = *(student_type *)args;

    student.current_preference_number = -1;
    student.current_course_preference = -1;

    sleep(student.fill_time);

    printf("\033[31;1mStudent %d has filled in preferences for course registration\033[0;0m\n", student.id);

    for (int i = 0; i < MAX_PREF; i++)
    {
        pthread_mutex_lock(&locks.course_spots[student.preferences[i]]);
        
        if (status.course_spots[student.preferences[i]] != 1 && student.current_course_preference == -1)
        {
            student.current_preference_number = i;
            student.current_course_preference = status.course_spots[student.preferences[i]];
        }
        
        pthread_mutex_unlock(&locks.course_spots[student.preferences[i]]);
    }

    while (student.current_preference_number > -1)
    {
        int withdrawn = 0;

        printf("\033[32;1mStudent %d is looking for course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

        pthread_mutex_lock(&locks.student_status[student.id]);
        status.student_status[student.id] = -1 * (student.current_course_preference + 1);
        pthread_mutex_unlock(&locks.student_status[student.id]);

        pthread_mutex_lock(&locks.course_spots[student.current_course_preference]);

        while (status.course_spots[student.current_course_preference] == 0)
            pthread_cond_wait(&availability.course_spots[student.current_course_preference], &locks.course_spots[student.current_course_preference]);

        if (status.course_spots[student.current_course_preference] > 0)
            status.course_spots[student.current_course_preference]--;
        else
            withdrawn = 1;

        pthread_mutex_unlock(&locks.course_spots[student.current_course_preference]);

        if (withdrawn == 0)
        {
            printf("\033[33;1mStudent %d has been allocated a seat in course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

            pthread_mutex_lock(&locks.student_status[student.id]);
            status.student_status[student.id] = student.current_course_preference + 1;
            pthread_mutex_unlock(&locks.student_status[student.id]);

            pthread_mutex_lock(&locks.student_status[student.id]);

            while (status.student_status[student.id] != 0)
                pthread_cond_wait(&availability.student_status[student.id], &locks.student_status[student.id]);

            pthread_mutex_unlock(&locks.student_status[student.id]);

            float x = (float)rand() / (float)(RAND_MAX / 1.0);
            if (x < set.courses[student.current_course_preference].interest * student.calibre)
            {
                printf("\033[34;1mStudent %d has selected course %s permanently\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

                pthread_mutex_lock(&locks.student_status[student.id]);
                status.student_status[student.id] = 0;
                pthread_mutex_unlock(&locks.student_status[student.id]);

                return 0;
            }
            else
            {
                printf("\033[35;1mStudent %d has withdrawn from course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);
                withdrawn = 1;
            }
        }

        if (withdrawn == 1)
        {
            int previous_course_preference = student.current_course_preference;

            for (int i = student.current_preference_number + 1; i < MAX_PREF; i++)
            {

                pthread_mutex_lock(&locks.course_spots[student.preferences[i]]);

                if (status.course_spots[student.preferences[i]] != -1)
                {
                    student.current_preference_number = i;
                    student.current_course_preference = status.course_spots[student.preferences[i]];
                }
    
                pthread_mutex_unlock(&locks.course_spots[student.preferences[i]]);
            }
            if (previous_course_preference == student.current_course_preference)
                student.current_preference_number = -1;
            else
                printf("\033[36;1mStudent %d has changed current preference from %s to %s\033[0;0m\n", student.id, set.courses[previous_course_preference].name, set.courses[student.current_course_preference].name);
        }
    }

    if (student.current_preference_number == -1)
        printf("\033[33;1mStudent %d did not get any of their preferred courses\033[0;0m\n", student.id);

    return 0;
}

void *course_simulator(void *args)
{
    course_type course = *(course_type *)args;

    while (1)
    {
        course.chosen_ta_lab_id = -1;
        course.chosen_ta_id = -1;

        for (int i = 0; i < course.num_labs; i++)
        {
            int lab_id = course.labs[i];

            for (int j = 0; j < set.labs[lab_id].num_tas; j++)
            {

                pthread_mutex_lock(&locks.lab_tas[lab_id][j]);

                while (status.lab_tas[lab_id][j][0] > 0 && status.lab_tas[lab_id][j][1] <= 0)
                {
                    pthread_cond_wait(&availability.lab_tas[lab_id][j], &locks.lab_tas[lab_id][j]);
                }

                if (status.lab_tas[lab_id][j][0] > 0 && status.lab_tas[lab_id][j][1] > 0)
                {
                    status.lab_tas[lab_id][j][0]--;
                    status.lab_tas[lab_id][j][1] = 0;

                    course.chosen_ta_lab_id = lab_id;
                    course.chosen_ta_id = j;
                }

                pthread_mutex_unlock(&locks.lab_tas[lab_id][j]);

                if (course.chosen_ta_id != -1)
                    break;
            }
            if (course.chosen_ta_id != -1)
                break;
        }

        if (course.chosen_ta_id == -1)
        {
            pthread_mutex_lock(&locks.course_spots[course.id]);
            
            status.course_spots[course.id] = -1;
            pthread_cond_broadcast(&availability.course_spots[course.id]);
            
            pthread_mutex_unlock(&locks.course_spots[course.id]);
            
            printf("\033[31;1mCourse %s does not have any TA's eligible and is removed from course offerings\033[0;0m\n", course.name);
            break;
        }

        pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);
        printf("\033[32;1mTA %d from lab %s has been allocated to course %s for their TA ship number %d\033[0;0m\n", course.chosen_ta_id, set.labs[course.chosen_ta_lab_id].name, course.name, set.labs[course.chosen_ta_lab_id].max_courses - status.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id][0]);
        pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        int left_spots_in_lab = 0;
        for (int i = 0; i < set.labs[course.chosen_ta_lab_id].num_tas; i++)
        {
            pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][i]);
            left_spots_in_lab += status.lab_tas[course.chosen_ta_lab_id][i][0];
            pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][i]);
        }
        if (left_spots_in_lab == 0)
            printf("\033[34;1mLab %s no longer has mentors available for TAship\033[0;0m\n", set.labs[course.chosen_ta_lab_id].name);

        int tut_slots = (rand() % course.course_max_slots) + 1;

        pthread_mutex_lock(&locks.course_spots[course.id]);
        status.course_spots[course.id] = tut_slots;
        pthread_mutex_unlock(&locks.course_spots[course.id]);

        printf("\033[36;1mCourse %s has been allocated %d seats\033[0;0m\n", course.name, tut_slots);

        int waiting_students = tut_slots;
        int filled_seats = 0;

        while ((filled_seats < 1) || (waiting_students != 0 && filled_seats < tut_slots))
        {
            pthread_cond_broadcast(&availability.course_spots[course.id]);

            waiting_students = 0;
            filled_seats = 0;

            for (int i = 0; i < input.students; i++)
            {
                pthread_mutex_lock(&locks.student_status[i]);

                if (status.student_status[i] == -1 * (course.id + 1))
                    waiting_students += 1;
                if (status.student_status[i] == (course.id + 1))
                    filled_seats += 1;
                
                pthread_mutex_unlock(&locks.student_status[i]);
            }

            if (waiting_students == 0)
                break;
        }

        filled_seats = 0;
        for (int i = 0; i < input.students; i++)
        {
            pthread_mutex_lock(&locks.student_status[i]);

            if (status.student_status[i] == (course.id + 1))
                filled_seats += 1;
            
            pthread_mutex_unlock(&locks.student_status[i]);
        }

        printf("\033[32;1mTA %d has started tutorial for Course %s with %d seats filled out of %d\033[0;0m\n", course.chosen_ta_id, course.name, filled_seats, tut_slots);

        pthread_mutex_lock(&locks.course_spots[course.id]);
        status.course_spots[course.id] = 0;
        pthread_mutex_unlock(&locks.course_spots[course.id]);

        sleep(TUT_TIME);

        for (int i = 0; i < input.students; i++)
        {
            pthread_mutex_lock(&locks.student_status[i]);

            if (status.student_status[i] == (course.id + 1))
            {
                status.student_status[i] = 0;
                pthread_cond_broadcast(&availability.student_status[i]);
            }

            pthread_mutex_unlock(&locks.student_status[i]);
        }

        printf("\033[35;1mTA %d from lab %s has completed the tutorial and left the course %s\033[0;0m\n", course.chosen_ta_id, set.labs[course.chosen_ta_lab_id].name, course.name);

        pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        status.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id][1] = 1; // the ta is now available to ta some other course
        pthread_cond_broadcast(&availability.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);
    
    }

    return 0;
}