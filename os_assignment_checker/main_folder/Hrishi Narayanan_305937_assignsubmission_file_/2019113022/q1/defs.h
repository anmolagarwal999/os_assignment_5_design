#ifndef DEFS_H
#define DEFS_H

#define MAX_NAME 20
#define MAX_PREF 3
#define TUT_TIME 5

typedef struct input_type
{
    int students;
    int labs;
    int courses;
} input_type;

typedef struct student_type
{
    int id;
    int fill_time;
    float calibre;
    int preferences[MAX_PREF];
    int current_course_preference;
    int current_preference_number;
} student_type;

typedef struct lab_type
{
    int id;
    char name[MAX_NAME];
    int num_tas;
    int max_courses;
} lab_type;

typedef struct course_type
{
    int id;
    char name[MAX_NAME];
    float interest;
    int course_max_slots;
    int num_labs;
    int *labs;
    int chosen_ta_lab_id;
    int chosen_ta_id;
} course_type;

typedef struct set_type
{
    course_type *courses;
    student_type *students;
    lab_type *labs;
} set_type;

typedef struct threads_type
{
    pthread_t *students;
    pthread_t *courses;
} threads_type;

typedef struct status_type
{
    int ***lab_tas;
    int *course_spots;
    int *student_status;
} status_type;

typedef struct locks_type
{
    pthread_mutex_t **lab_tas;
    pthread_mutex_t *course_spots;
    pthread_mutex_t *student_status;
} locks_type;

typedef struct availability_type
{
    pthread_cond_t **lab_tas;
    pthread_cond_t *course_spots;
    pthread_cond_t *student_status;
} availability_type;

#endif