// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "util.h"
#include "sims.h"

input_type input;
set_type set;

threads_type threads;

status_type status;
locks_type locks;
availability_type availability;

signed main()
{
    threads.courses = NULL;
    threads.students = NULL;

    get_input();
    initialise_all();

    threads.courses = malloc(sizeof(pthread_t) * input.courses);
    threads.students = malloc(sizeof(pthread_t) * input.students);

    for (int i =0; i<input.students; i++)
        pthread_create(&threads.students[i], NULL, student_simulator, &set.students[i]);

    for (int i =0; i<input.students; i++)
        pthread_join(threads.students[i], NULL);

    exit(0);
}
