#ifndef SIMS_H
#define SIMS_H

void *student_simulator(void *args);
void *course_simulator(void *args);

#endif