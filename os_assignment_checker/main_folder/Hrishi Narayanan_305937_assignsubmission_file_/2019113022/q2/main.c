#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "util.h"
#include "sims.h"

input_type input;
set_type set;

threads_type threads;
sem_type sem;

status_type status;
locks_type locks;
condition_type condition;

signed main()
{
    get_input();
    initialise_all();

    sem_init(&sem.h_zone, 0, input.h_capacity);
    sem_init(&sem.a_zone, 0, input.a_capacity);
    sem_init(&sem.n_zone, 0, input.n_capacity);

    threads.people = malloc(sizeof(pthread_t) * input.people);
    threads.teams = malloc(sizeof(pthread_t) * NUM_TEAMS);

    for (int i = 0; i < input.people; i++)
        pthread_create(&threads.people[i], NULL, entry_exit_simulator, &set.people[i]);

    for (int i = 0; i < NUM_TEAMS; i++)
        pthread_create(&threads.teams[i], NULL, team_simulator, &set.teams[i]);

    for (int i = 0; i < input.people; i++)
        pthread_join(threads.people[i], NULL);

    for (int i = 0; i < NUM_TEAMS; i++)
        pthread_join(threads.teams[i], NULL);

    for (int i = 0; i < NUM_TEAMS; i++)
    {
        status.goals[i] = -1;
        pthread_cond_broadcast(&condition.goals[i]);
        pthread_mutex_destroy(&locks.goals[i]);
        pthread_cond_destroy(&condition.goals[i]);
        pthread_cancel(threads.teams[i]);
    }

    sem_destroy(&sem.h_zone);
    sem_destroy(&sem.a_zone);
    sem_destroy(&sem.n_zone);

    exit(0);
}