# Question 2 : The Clasico Experience

## Compiling and running the code

- To compile the code, use the command:
  ```sh
  make
  ```
- Alternatively, you can compile the code as:
  ```sh
  gcc -o q2 main.c sims.c util.c find.c -lpthread
  ```
- The compiled executable `q2` can be run with:
  ```sh
  ./q2
  ```
- The given testcases are present in `test.txt` and can be run with the code as:
  ```sh
  ./q2 < test.txt
  ```

## Structuring of the code

- Data structures used in the code are defined in `defs.h`
- The main logic of the code is contained in `main.c`
- The `util.c` contains the utility functions, which are taking the input and initialisation of mutex
- The threads are run as simulation of each of the teams and each action by the people within the `sims.c`
- The action of finding seats is handled in `find.c`

## The Data Structures: `defs.h`

- `team_type` struct contains all the details pertaining to a single team
  ```c
    typedef struct team_type
    {
        char type;
        int id;
        int chances;
        int *previous_chance_time;
        float *goal_probability;
    } team_type;
  ```
- `person_type` struct contains all the details pertaining to a single person
  ```c
    typedef struct person_type
    {
        char name[MAX_NAME];
        char type;
        int id;
        int group;
        int reach;
        int patience;
        int goals;
    } person_type;
  ```
- The status corresponding to number of goals scored(`goals`) and the zone assigned to each person (`person_zone`) are stored in the struct `status_type`
  ```c
    typedef struct status_type
    {
        int goals[NUM_TEAMS];
        char *person_zone;
    } status_type;
  ```
- The locks corresponding to number of goals scored(`goals`) and the zone assigned to each person (`person_zone`) are stored in the struct `locks_type`
  ```c
    typedef struct locks_type
    {
        pthread_mutex_t goals[NUM_TEAMS];
        pthread_mutex_t *person_zone;
    } locks_type;
  ```
- The conditions corresponding to number of goals scored(`goals`) and the zone assigned to each person (`person_zone`) are stored in the struct `condition_type`
  ```c
    typedef struct condition_type
    {
        pthread_cond_t goals[NUM_TEAMS];
        pthread_cond_t *person_zone;
    } condition_type;
  ```

## Main logic: `main.c`

- `get_input()` and `initialise_all()` are used to get input and intialise mutex respectively
- After initialisation of threads, the semphores are initialised. \*
- The threads are then created. Then the program waits for all the threads to finish, and then destroys the semaphores

## Utility functions: `util.c`

- The `get_input()` function is use to take and store the inputs recieved.
- The `initialise_all()` initialises all threads and corresponding mutex.

## Simulation of teams and actions by people: `sims.c`

- Each team is simulated by the `team_simulator()` function:

  - The function sleeps till the time of next goal scoring chance, after which there is an attempt to goal
  - If the team scores, then the `goals` corresponding to the team are updated
  - The goal values are broadcasted after each goal scoring chance, irrespective of whether or not goal is actually scored

- Entry and exit are simulated by the `entry_exit_simulator` function:

  - The function sleeps till the person's `reach` time
  - Two threads are initialised by the function:
    - The first thread simulates the person entering the stadium and looking for seating
    - The second thread simulates the person watching the game and goes on till the person exits the stadium
  - If the person does not find a seat, then the person exits the stadium
  - The person is allowed to stay in the stadium, after getting a seating till the `spectation_time` expires. After this, the person is forced to exit the stadium
  - When a person leaves, the semaphore corresponding to the zone where the person was seated is incremented, so that the seating can be reused by others.

- Reaction of the person during the game is simulated by the `game_reaction_simulator` function:

  - For a non-neutral fan, keep waiting till the game time is over or the goals of the opposing team is more than the threshold
  - If the threshold is crossed, the fan exits the stadium
  - If that does not happen the thread continues till the game finishes

- Seating of the person is simulated by the `seating_simulator` function:
  - Based on the person's allegiance to the teams, the person is eligible to sit in specific zones
  - Each zone is represented by a distinct semaphore
  - The function creates one thread each for the zones where the seats must be found. Then it waits for the threads to finish

## Finding seating for people: `find.c`

- The functions `find_zone_h`, `find_zone_n` and `find_zone_a` are used to find seats in zones H, N and A respectively
- Each zone is essentially a counting semaphore, which helps handle conditions like total number of seats etc
- The functions waits for the semaphore using `sem_timedwait`
- If the semaphore signals before the person finds a seating, then the person is allocated a seat in that particular zone
- In case the semaphore times out before the person finds a seating, then the person exits as no seats are available
- In case the seating is provided within another zone, then the information is updated and is handled accordingly.
