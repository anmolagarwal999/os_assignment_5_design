#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;
extern sem_type sem;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void *find_zone_h(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.h_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'H';

        printf("\033[32;1m%s (%c) got a seat in zone H\033[0;0m\n", person.name, person.type);
    }
    else
        sem_post(&sem.h_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}

void *find_zone_n(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.n_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' == 0 || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'N';

        printf("\033[32;1m%s (%c) got a seat in zone N\033[32;1m\n", person.name, person.type);
    }

    else
        sem_post(&sem.n_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}

void *find_zone_a(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.a_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'A';

        printf("\033[32;1m%s (%c) got a seat in zone A\033[0;0m\n", person.name, person.type);
    }

    else
        sem_post(&sem.a_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}
