#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void get_input()
{
    scanf("%d %d %d %d", &input.h_capacity, &input.a_capacity, &input.n_capacity, &input.spectation_time);

    set.people = malloc(sizeof(person_type) * 1);

    scanf("%d", &input.groups);

    input.people = 0;
    for (int i = 1; i <= input.groups; i++)
    {
        int in_group;
        scanf("%d", &in_group);

        for (int j = 0; j < in_group; j++)
        {
            set.people = realloc(set.people, sizeof(person_type) * (input.people + 1));

            scanf("%s %c %d %d %d", set.people[input.people].name, &set.people[input.people].type, &set.people[input.people].reach, &set.people[input.people].patience, &set.people[input.people].goals);

            set.people[input.people].group = i;
            set.people[input.people].id = input.people;

            input.people++;
        }
    }

    set.teams = malloc(sizeof(team_type) * NUM_TEAMS);

    set.teams[0].type = 'H';
    set.teams[1].type = 'A';
    for (int i = 0; i < NUM_TEAMS; i++)
    {
        set.teams[i].id = i;
        set.teams[i].chances = 0;
        set.teams[i].previous_chance_time = malloc(sizeof(int) * 1);
        set.teams[i].goal_probability = malloc(sizeof(float) * 1);
    }

    int chances, time;
    char team;

    scanf("%d\n", &chances);

    int prev_a_time = 0, prev_h_time = 0;

    for (int i = 0; i < chances; i++)
    {
        scanf("%c", &team);

        if (team == 'H')
        {
            set.teams[0].previous_chance_time = realloc(set.teams[0].previous_chance_time, sizeof(int) * (set.teams[0].chances + 1));
            set.teams[0].goal_probability = realloc(set.teams[0].goal_probability, sizeof(float) * (set.teams[0].chances + 1));

            scanf("%d %f\n", &time, &set.teams[0].goal_probability[set.teams[0].chances]);

            set.teams[0].previous_chance_time[set.teams[0].chances] = time - prev_h_time;
            prev_h_time = time;

            set.teams[0].chances++;
        }

        else if (team == 'A')
        {
            set.teams[1].previous_chance_time = realloc(set.teams[1].previous_chance_time, sizeof(int) * (set.teams[1].chances + 1));
            set.teams[1].goal_probability = realloc(set.teams[1].goal_probability, sizeof(float) * (set.teams[1].chances + 1));

            scanf("%d %f\n", &time, &set.teams[1].goal_probability[set.teams[1].chances]);

            set.teams[1].previous_chance_time[set.teams[1].chances] = time - prev_a_time;
            prev_a_time = time;

            set.teams[1].chances++;
        }
    }
}

void initialise_all()
{
    for(int i = 0; i < NUM_TEAMS; i++){
        pthread_mutex_init(&locks.goals[i], NULL);
        pthread_cond_init(&condition.goals[i], NULL);
        status.goals[i] = 0;
    }

    condition.person_zone = malloc(sizeof(pthread_cond_t) * input.people);
    status.person_zone = malloc(sizeof(char) * input.people);
    locks.person_zone = malloc(sizeof(pthread_mutex_t) * input.people);
    
    for (int i = 0; i < input.people; i++){
        pthread_cond_init(&condition.person_zone[i], NULL);
        pthread_mutex_init(&locks.person_zone[i], NULL);
        status.person_zone[i] = 'E';
    }
}