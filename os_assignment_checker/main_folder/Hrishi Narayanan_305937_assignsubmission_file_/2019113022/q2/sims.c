#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "find.h"

extern input_type input;
extern set_type set;

extern threads_type threads;
extern sem_type sem;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void *team_simulator(void *args)
{
    team_type team = *(team_type *)args;

    for (int i = 0; i < team.chances; i++)
    {
        sleep(team.previous_chance_time[i]);

        float x = (float)rand() / (float)(RAND_MAX / 1.0);

        if (x >= team.goal_probability[i])
            printf("\033[36;1mTeam %c missed their chance to score a goal\033[0;0m\n", team.type);
        else
        {
            pthread_mutex_lock(&locks.goals[team.id]);
            status.goals[team.id] += 1;
            printf("\033[36;1mTeam %c has scored goal number %d\033[0;0m\n", team.type, status.goals[team.id]);
            pthread_mutex_unlock(&locks.goals[team.id]);
        }

        pthread_cond_broadcast(&condition.goals[team.id]);
    }

    return 0;
}

void *game_reaction_simulator(void *args)
{
    person_type person = *(person_type *)args;

    if (person.type == 'H')
    {
        pthread_mutex_lock(&locks.goals[1]);

        while (status.goals[1] < person.goals && status.goals[1] >= 0)
            pthread_cond_wait(&condition.goals[1], &locks.goals[1]);

        if (status.goals[1] >= person.goals)
            printf("\033[31;1m%s got enraged and went to the exit\033[0;0m\n", person.name);

        pthread_mutex_unlock(&locks.goals[1]);

        pthread_cond_broadcast(&condition.person_zone[person.id]);
    }

    else if (person.type == 'A')
    {
        pthread_mutex_lock(&locks.goals[0]);

        while (status.goals[0] < person.goals && status.goals[0] >= 0)
            pthread_cond_wait(&condition.goals[0], &locks.goals[0]);

        if (status.goals[0] >= person.goals)
            printf("\033[31;1m%s got enraged and went to the exit\033[0;0m\n", person.name);

        pthread_mutex_unlock(&locks.goals[0]);

        pthread_cond_broadcast(&condition.person_zone[person.id]);
    }

    return 0;
}

void *seating_simulator(void *args)
{
    person_type person = *(person_type *)args;

    if (person.type == 'H')
    {
        pthread_t h_thread;
        pthread_t n_thread;

        pthread_create(&h_thread, NULL, find_zone_h, &person);
        pthread_create(&n_thread, NULL, find_zone_n, &person);

        pthread_join(h_thread, NULL);
        pthread_join(n_thread, NULL);
    }
    else if (person.type == 'A')
    {
        pthread_t a_thread;

        pthread_create(&a_thread, NULL, find_zone_a, &person);

        pthread_join(a_thread, NULL);
    }
    else if (person.type == 'N')
    {
        pthread_t h_thread;
        pthread_t a_thread;
        pthread_t n_thread;

        pthread_create(&h_thread, NULL, find_zone_h, &person);
        pthread_create(&a_thread, NULL, find_zone_a, &person);
        pthread_create(&n_thread, NULL, find_zone_n, &person);

        pthread_join(h_thread, NULL);
        pthread_join(a_thread, NULL);
        pthread_join(n_thread, NULL);
    }

    return 0;
}

void *entry_exit_simulator(void *args)
{
    person_type person = *(person_type *)args;
    sleep(person.reach);

    printf("\033[31;1m%s has reached the stadium\033[0;0m\n", person.name);

    pthread_t thread;
    pthread_create(&thread, NULL, seating_simulator, &person);
    pthread_join(thread, NULL);

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'G' || status.person_zone[person.id] == 'D')
    {
        printf("\033[34;1m%s did not find a seat in any of the zones\033[0;0m\n", person.name);
        pthread_mutex_unlock(&locks.person_zone[person.id]);

        printf("\033[35;1m%s left the stadium\033[0;0m\n", person.name);
        return 0;
    }

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    pthread_create(&thread, NULL, game_reaction_simulator, &person);

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += input.spectation_time;

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (pthread_cond_timedwait(&condition.person_zone[person.id], &locks.person_zone[person.id], &ts) == ETIMEDOUT)
        printf("\033[33;1m%s watched the match for %d seconds and is leaving\033[0;0m\n", person.name, input.spectation_time);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'H')
        sem_post(&sem.h_zone);

    if (status.person_zone[person.id] == 'A')
        sem_post(&sem.a_zone);

    if (status.person_zone[person.id] == 'N')
        sem_post(&sem.n_zone);

    status.person_zone[person.id] = 'G';

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    printf("\033[35;1m%s left the stadium\033[0;0m\n", person.name);

    return 0;
}