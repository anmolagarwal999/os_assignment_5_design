#ifndef DEFS_H
#define DEFS_H

#define _GNU_SOURCE

#define MAX_NAME 20
#define NUM_TEAMS 2

typedef struct input_type
{
    int h_capacity;
    int a_capacity;
    int n_capacity;
    int spectation_time;
    int groups;
    int people;
} input_type;

typedef struct sem_type
{
    sem_t h_zone;
    sem_t a_zone;
    sem_t n_zone;
} sem_type;

typedef struct person_type
{
    char name[MAX_NAME];
    char type;
    int id;
    int group;
    int reach;
    int patience;
    int goals;
} person_type;

typedef struct team_type
{
    char type;
    int id;
    int chances;
    int *previous_chance_time;
    float *goal_probability;
} team_type;

typedef struct set_type
{
    person_type *people;
    team_type *teams;
} set_type;

typedef struct threads_type
{
    pthread_t *people;
    pthread_t *teams;
} threads_type;

typedef struct status_type
{
    int goals[NUM_TEAMS];
    char *person_zone;
} status_type;

typedef struct locks_type
{
    pthread_mutex_t goals[NUM_TEAMS];
    pthread_mutex_t *person_zone;
} locks_type;

typedef struct condition_type
{
    pthread_cond_t goals[NUM_TEAMS];
    pthread_cond_t *person_zone;
} condition_type;

#endif