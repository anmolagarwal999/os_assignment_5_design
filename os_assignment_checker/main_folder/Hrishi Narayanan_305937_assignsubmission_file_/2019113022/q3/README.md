# Question 3 : Multithreaded client and server

## Compiling and running the code

- To compile the code, use the command:
  ```sh
  make
  ```
- Alternatively, you can compile the code as:
  ```sh
  g++ -o server server_side.cpp -lpthread
  g++ -o client client_side.cpp -lpthread
  ```
- The compiled executables `server` and `client` can be run with:
  ```sh
  ./server thread_value
  ./client
  ```
  Here, `thread_value` is the number of worker threads to be spawned by the server
- The given testcases are present in `test.txt` and can be run with the code as:
  ```sh
  ./server thread_value
  ./client < test.txt
  ```

## Structuring of the code

- Server side logic is managed by the program `server_side.cpp`
- Client side simulation is done by the `client_side.cpp` program

## The Data Structures

- `request_type` struct contains all the details pertaining to a single request send from the client side

  ```c
    typedef struct request_type
    {
        int id;
        int time;
        string message;
    } request_type;
  ```

- The status corresponding to client sockets(`client_sockets`) and the dictionary managed by the server (`dictionary`) are stored in the struct `status_type`
  ```c
    typedef struct status_type
    {
        queue <int> client_sockets;
        string dictionary[MAX_CLIENTS+1];
    } status_type;
  ```
- The locks corresponding to client sockets(`client_sockets`) and the dictionary managed by the server (`dictionary`) are stored in the struct `locks_type`
  ```c
    typedef struct locks_type
    {
        pthread_mutex_t client_socket;
        pthread_mutex_t dictionary[MAX_CLIENTS+1];
    } locks_type;
  ```

## Functioning of the client side: `server_side.cpp`

- The server gets the `thread_value` as input and spawns worker threads accordingly
- The server allocates a socket filedescriptor whenever a client tries to make a connection with the server. This socket is then pushed into the queue
- The semaphore corresponding to all the requests (`requests`) is posted every time a socket filedescriptor is pushed
- The worker threads wait on this semaphore and handle the request every time a socket filedescriptor is popped

## Functioning of the client side: `client_side.cpp`

- The input recieved is broken down into the sleep time and command to be send
- The thread sleeps for the time corresponding to the sleep time recieved as input and then posts it to the server
- The thread recieves signal from the server. It formats the message and prints the output required
