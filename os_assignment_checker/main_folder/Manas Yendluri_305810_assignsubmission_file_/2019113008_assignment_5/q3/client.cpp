#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
struct commandStruct {
    int id;
    int delayTime;
    string command;
};
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////




// client routine ---
void* clientExecute(void* args) {
    struct commandStruct *request = (struct commandStruct*)args;

    // delay time
    sleep(request->delayTime);

    struct sockaddr_in serverAddress;
    //get socket address

    int socketFd = get_socket_fd(&serverAddress);

    //send the command from client using fd
    if (send_string_on_socket(socketFd, request->command)==-1)
    {
        cerr << "couldnt write to the server socket. socket issue ig .\n";
    }

    // read server response
    pair<string, int> outputPair;
    outputPair = read_string_from_socket(socketFd, buff_sz);
    if (outputPair.second <= 0) 
    {
        cerr << "couldnt read server msg\n";
    }
    cout << request->id << ":" << outputPair.first << "\n";

    // close connection
    close(socketFd);

    return NULL;
}
// }}}

int main(int argc, char* argv[]) {

    int commandCount;
    cin >> commandCount;
    struct commandStruct commandList[commandCount];
    // take input
    for (int i = 0; i < commandCount; i++) {
        commandList[i].id = i;
        cin >> commandList[i].delayTime; 
        getline(cin, commandList[i].command);
    }

    // initialize threads
    pthread_t clientThreads[commandCount];
    for (int i = 0; i < commandCount; i++) {
        struct commandStruct* command = &commandList[i];
        pthread_t* thread = &clientThreads[i];        
        if (pthread_create(thread, NULL, &clientExecute, command)) 
        {
            cerr<<"Error creating client threads\n";
        }
    }

    // join threads
    for (int i = 0; i < commandCount; i++) 
    {
        if (pthread_join(clientThreads[i], NULL))
        {
            cerr<<"Error joining client threads\n";
        }
    }
    return 0;
}