#include "headers.h"
using namespace std;

#define server_port 8001
#define buffer_max_sz 1048576

typedef struct request request;

struct request
{
    int i;
    pthread_t tid;
    int client_fd;
    int req_time;
    string command;
    pthread_mutex_t mutex;
};

vector<request> clients;
pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER;

int get_client_socket_fd()
{
    struct sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = server_port;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    return socket_fd;
}

void *client_thread(void *(arg))
{
    int idx = *(int *)arg;
    clients[idx].client_fd = get_client_socket_fd();
    int client_fd = clients[idx].client_fd;
    int fd_copy = client_fd;
    int fd_client = clients[idx].client_fd;
    int client_copy = fd_client;
    int req_time = clients[idx].req_time;
    int required_time = req_time;
    string command = clients[idx].command;
    string command_copy = command;
    sleep(req_time);
    required_time = req_time;
    pthread_mutex_lock(&clients[idx].mutex);
    int x = write(client_fd, command.c_str(), command.length());
    if (x <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }
    pthread_mutex_unlock(&clients[idx].mutex);
    pthread_mutex_lock(&clients[idx].mutex);

    string buffer;
    buffer.resize(buffer_max_sz);
    int byte_read = read(client_fd, &buffer[0], buffer_max_sz - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);
    if (byte_read <= 0)
    {
        cerr << "Failed To communicate with the server" << endl;
        pthread_mutex_unlock(&clients[idx].mutex);
        return NULL;
    }
    pthread_mutex_lock(&terminal);
    cout << clients[idx].i << " : " << gettid() << " : " << buffer << endl;
    pthread_mutex_unlock(&terminal);

    pthread_mutex_unlock(&clients[idx].mutex);

    return NULL;
}

int main()
{
    int num_clients;
    cin >> num_clients;
    string x;
    getline(cin, x);
    for (int i = 0; i < num_clients; i++)
    {
        string str;
        string time_req = "";
        getline(cin, str);
        auto itr = str.begin();
        while (*itr != ' ')
        {
            time_req += *itr;
            itr++;
        }
        itr = itr+1;
        request tmp;
        request temp2;
        tmp.req_time = stoi(time_req);
        temp2.req_time = tmp.req_time;
        string command(itr, str.end());
        temp2.command = command;
        tmp.command = command;
        clients.push_back(tmp);
    }
    // get the connection sockets
    int i = 0;
    while(i<num_clients)
    {
         clients[i].i = i;
        clients[i].mutex = PTHREAD_MUTEX_INITIALIZER;
        i++;
    }
    i = 0;
    while(i<num_clients)
    {
        int *idx = new int;
        *idx = i;
        pthread_create(&clients[i].tid, NULL, client_thread, (void *)idx);
        i++;
    }
   
   i = 0;

    while(i<num_clients)
    {
        pthread_join(clients[i].tid, NULL);
        i++;
    }
    return 0;
}