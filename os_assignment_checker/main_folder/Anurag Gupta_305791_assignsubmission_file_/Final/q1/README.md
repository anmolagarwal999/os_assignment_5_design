# Question 1

## Running the code

Run the code with:

```bash
gcc -pthread -w main.c
```

## Working of the code
### int main()

- The input is stored in `struct student`, `struct lab`, and `struct course` instances
- Create the threads for simulating students and for simulating courses
- Wait for the student and course threads to finish

### StudentSimulator (student thread)

- Sleep for `fill_time` seconds of a student
- Fill the preferences of students in the given order.
- Choose the available course in the order of preference.
- Loop until the student goes through all 3 preferences
- Finalize a course for the student.
- If the student waves up because the course has spots open, take up a spot
- Wait until the TA releases the student
- Choose to finalize or withdraw the course based on a random probability
- If the student withdrew willingly or if the course was removed, update the course preference

### courseSimulator (course thread)

- Go through each of the labs and choose the first TA that can also TA the course
- If the TA is currently busy, wait for the TA to become available
- Allocate a TA to the course following all the necessary conditions.
- If no TA has been found at any point, then withdraw the course
- Allocate seats to the course.
- Until there is at least 1 seat filled, or if there are students waiting and there are empty spots, keep broadcasting that this course has empty spots
- If noone is waiting for this course, stop broadcasting and start the tutorial with noone
- After the tutorial is done, remove all the students that took the tutorial.
