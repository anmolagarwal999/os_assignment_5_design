#include<stdio.h>
#include <stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include <string.h>
#include<time.h>

void *courseSimulator(void *arg);
void position_giver(int pos);
void course_size_allocation(int id);
void course_TA_allocation(intid);
void course_start(int id);
void tut_over(int id);
int course_Removed(int id);
void student_CourseSelection(int id);
void student_courseApplication(int id);
void student_PreferenceFilling(int id);
int student_Removed(int id);
void *StudentSimulator(void *arg);


time_t startTime;
int studentsOver;


pthread_t courseThreadArr[10000];
pthread_t studentThreadArr[10000];
pthread_mutex_t course_TA_allocation_lock;
pthread_mutex_t student_CourseApplication_lock;
pthread_mutex_t mutex_lock[100];
pthread_cond_t c[100];

pthread_mutex_t mutex_lock2[100];
pthread_cond_t c2[100];

int turns[100];
struct Student
{
    float calibre;
    int courseIDs[3];
    float time;
    int isLearning;
    int prefPos;
    int existing;
};

struct Course
{
    char CourseName[50];
    float interestQ;
    int course_max_slots;
    int p;
    int listp[100];

    int D;
    int currentStudentSize;

    int isOn;
    int isExisting;

    char courseTA[100];
    int labID_Allocated;
    int TA_Allocated;
};

struct Lab
{
    char labName[50];
    int noOfTAs;
    int courseDone[100];
    int busy[100];
    int max;
};

int num_labs;
int num_courses;
int num_student;
int  i;
int  j;
char position[50];
struct Course CourseList[100];
struct Lab LabList[100];
struct Student StudentList[100];

struct thread_details
{
    int id;
};
//////////////END/////////////////