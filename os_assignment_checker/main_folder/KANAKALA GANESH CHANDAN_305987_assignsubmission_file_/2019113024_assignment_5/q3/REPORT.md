# Report on Q3

> K Ganesh Chandan
> 2019113024

# Logic Explanation

The template from the tutorial file can be modifed to complete this question. The 2 files provided to us were `server.cpp` and `client.cpp`, which can be used as boilerplate code.

- First we choose a port to make the communication b/w the client and the server happen.

# Client

- The Strucure `request` given can be used to hold all the inputs request given.
- We will have something called `client routine` which will act as the thread.
- `clinet routine`:- Dependingon the input hte thread waits as specified. Connects to the server. Writes the commands which are supposed bo executed by this thread to hte file descriptor of ther server. Waits for the response from the server, and upon getitn a response form the server, it prints the response and closes teh connection with ther server and finished exectuting.
- The commands stored in the list of `request`'s , for each process a `client routine` thread is executed

# Server

- decide the no of workers and initializes a `worker pool`.
- It will have a `request queue` a queue which will conatain all the pending requests from the client side.
- A routine `worker routine` is used.
- `worker routine` : - This thread keeps waiting conditionally till it finds something in `request queue`. Then the socket file descriptor is taken, teh request is read and the request is executed in `execute requeset`.
- `execute request`:- tokenises the input and returns an approproate resoonse. This is done by taking the approproate action like isnert, delete, update concat etc, by acting on a global buffer which is locked by mutex by each thread.
- So basically, After initializing the threads in the server and waiting fninised, the request is pushed to the `request queeu`.
- This request will be parsed by to the fucntion whic handles the execution of the quesry using the `execute request`, which returns a response and teh response is sent back to client.
