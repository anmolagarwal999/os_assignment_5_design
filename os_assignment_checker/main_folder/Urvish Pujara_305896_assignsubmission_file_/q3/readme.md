## q3

## Multithreaded client and server
to run the program;
```g++ q3/server.cpp```
```./a.out``` in terminal 1.
to run the client program:
```g++ q3/client.cpp```
```./a.out``` in terminal 2.
this consists of 2 files client.cpp and server.cpp

client.cpp:

This file includes connection with server, taking 'n' as input from user (n = number of commands you want to run).
Further, it takes n inputs from user which are in the format : ```<int sleep_time> <string command>```.
The format of those n commands is as specified in the question pdf.
Further, for each command, a thread is created which handles the specified sleep for each command and then sends the command to socket for server to read it .
The format of sent command is a string a = ```<int command_id> <string command>```
After the server reads and processes the command it sends an appropriate message to client regarding what was the result of command 
The format of msg is : ```<int command_id>:<long int worker_thread_id>:<string command>\n```
Then all the threads are joined.

server.cpp:

This is the file that does the actual work.

It stores a dictionary in the form of array of strings.
As mentioned in the question pdf "We constrain the ‘keys’ to be non-negative integers (<=100) and ‘values’ to be non-empty strings containing only alphabets and numbers." the array is of length 101 with keys ranging from 0 to 100 (both inclusive).

As soon as the program runs;
- All the values of keys i.e. 101 strings are given value = "no_value".
- Hence please note that a valued key cant have value == "no_value".
- That is the default value set for my data structure which means currently there is no key k.
- All the values of flags are set 0.
- If flag[i] == 0 , it means the key i is totally free right now and you can deal with it without any hesitation.
- If flag[i] == 1 , it means the key i is being worked upon right now and might get updated so beware and wait till the ongoing work is finished.

Now as soon as the cmd is read from the socket it is sent via thread to function process_command, it does basic utility things like splitting command into words.
After that the type of command is checked (insert/delete etc.) and accordingly the required arguements are worked upon to obtin correct data type instead of word.
As soon as the arguements are set, respective functions are called that does the main thing (insertion/updation etc.) in command.
They are put in conditional variables which checks if the flags are 0 and then works.
if the flag is 1 it waits conditionally until flag is again set to 0.

and the thread_id of worker thread is stores in msg_to_send_back by pthread_self() fnction along with the output.
This message is sent back to socket after all the procedure of a command is complete.
This msg gets read and displayed by client.


-- Note: ```bytes_received : 0
Failed to read data from socket. 
Segmentation fault (core dumped)```
this is not at all an error. It is a kind of termination message as it ends when it doesnt recieve further input from client after execution of all commands.

-- Note: The message displayed in client is also displayed in server along with command string and bytes read in command string.

-- Note: The limit for max. command is set 30 for now, it can be changed as per the requirements.
