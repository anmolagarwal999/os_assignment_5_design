#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream>
// included by me
#include <pthread.h>
#include <semaphore.h>
#include <sstream>
// included by me
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001
const int initial_msg_len = 256;


//// my variables ////

string values_arr[101];
int flag_arr[101];
int no_of_worker_threads = 5;
int cmd_no=0;
sem_t curr_sem;
pthread_mutex_t copying_command_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cnt_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;
pthread_t t[100];
int client_socket_fd;
typedef struct thread_details
{
    int idx;
    string cmd;
} td;
td *thread_input[100];
//// my variables ////



////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    //cout << "Client sent : " << endl;
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }
    //cout << "Client sent : " << endl;
    output[bytes_received] = 0;
    //cout << "Client sent : " << endl;
    output.resize(bytes_received);
    //cout << output << endl;
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}
string insert(int key,string value)
{
    if(flag_arr[key]==0){
        pthread_mutex_lock(&cnt_lock);
        flag_arr[key]=1;
        long int curr_thread = pthread_self();
        string msg = to_string(curr_thread) + ":";
        if(values_arr[key]=="no_value"){
            values_arr[key]=value;
            msg += "Insertion successful";
        }
        else{
            msg += "Key already exists";
        }
        flag_arr[key]=0;
        
        pthread_mutex_unlock(&cnt_lock);
        pthread_cond_signal(&c);
        return msg;
    }
    else{

        pthread_mutex_lock(&cnt_lock);
        while(flag_arr[key]==1){
            pthread_cond_wait(&c,&cnt_lock);
        }
        pthread_mutex_unlock(&cnt_lock);
    }
    return "";
    
}

string delete_key(int key)
{
    if(flag_arr[key]==0){
        pthread_mutex_lock(&cnt_lock);
        flag_arr[key]=1;
        long int curr_thread = pthread_self();
        string msg = to_string(curr_thread) + ":";
        if(values_arr[key]!="no_value"){
            values_arr[key]="no_value";
            msg += "Deletion successful";
        }
        else{
            msg += "No such key exists";
        }
        flag_arr[key]=0;
        
        pthread_mutex_unlock(&cnt_lock);
        pthread_cond_signal(&c);
        return msg;
    }
    else{

        pthread_mutex_lock(&cnt_lock);
        while(flag_arr[key]==1){
            pthread_cond_wait(&c,&cnt_lock);
        }
        pthread_mutex_unlock(&cnt_lock);
    }
    return "";
    
}

string update(int key,string value)
{
    if(flag_arr[key]==0){
        pthread_mutex_lock(&cnt_lock);
        flag_arr[key]=1;
        long int curr_thread = pthread_self();
        string msg = to_string(curr_thread) + ":";
        if(values_arr[key]!="no_value"){
            values_arr[key]=value;
            msg += value;
        }
        else{
            msg += "Key does not exist";
        }
        flag_arr[key]=0;
        pthread_mutex_unlock(&cnt_lock);
        pthread_cond_signal(&c);
        return msg;
    }
    else{

        pthread_mutex_lock(&cnt_lock);
        while(flag_arr[key]==1){
            pthread_cond_wait(&c,&cnt_lock);
        }
        pthread_mutex_unlock(&cnt_lock);
    }
    return "";
}

string concat(int key1,int key2)
{
    if(flag_arr[key1]==0 && flag_arr[key2]==0){
        pthread_mutex_lock(&cnt_lock);
        flag_arr[key1]=1;
        flag_arr[key2]=1;

        long int curr_thread = pthread_self();
        string msg = to_string(curr_thread) + ":";
        if(values_arr[key1]!="no_value" && values_arr[key2]!="no_value"){
            string value1 = values_arr[key1];
            string value2 = values_arr[key2];
            values_arr[key1] = value1 + value2;
            values_arr[key2] = value2 + value1;
            msg += value2 + value1;
        }
        else{
            msg += "Concat failed as at least one of the keys does not exist";
        }
        flag_arr[key1]=0;
        flag_arr[key2]=0;
        pthread_mutex_unlock(&cnt_lock);
        pthread_cond_signal(&c);
        return msg;
    }
    else{

        pthread_mutex_lock(&cnt_lock);
        while(flag_arr[key1]==1 || flag_arr[key2]==1){
            pthread_cond_wait(&c,&cnt_lock);
        }
        pthread_mutex_unlock(&cnt_lock);
    }
    return "";
}

string fetch(int key)
{
    if(flag_arr[key]==0){
        pthread_mutex_lock(&cnt_lock);
        flag_arr[key]=1;
        long int curr_thread = pthread_self();
        string msg = to_string(curr_thread) + ":";
        if(values_arr[key]!="no_value"){
            msg += values_arr[key];
        }
        else{
            msg += "Key does not exist";
        }
        flag_arr[key]=0;
        pthread_mutex_unlock(&cnt_lock);
        pthread_cond_signal(&c);
        return msg;
    }
    else{

        pthread_mutex_lock(&cnt_lock);
        while(flag_arr[key]==1){
            pthread_cond_wait(&c,&cnt_lock);
        }
        pthread_mutex_unlock(&cnt_lock);
    }
    return "";
}

///////////////////////////////
void* process_command(void* arg){
    sem_wait(&curr_sem);
    string cmd = ((struct thread_details *)arg)->cmd;
    
    string words[5];
    int i=0;
    for(i=0;i<5;i++){
        words[i] = "";
    }
    i=0;
    for (auto x : cmd) 
    {
        if(i<5){
            if (x == ' ' || x == '\n')
            {
                i++;
            }
            else {
                words[i] = words[i] + x;
            }
        }
    }
    //std::cout << "words:" << std::endl;
    //for(i=0;i<5;i++){
    //    std::cout << words[i] << std::endl;;
    //}
    string msg_to_send_back = words[0] + ":";
    //std::cout << msg_to_send_back << std::endl;
    // cecking for command type
    if(words[1]=="insert"){
            int key;
            stringstream geek(words[2]);
            geek >> key;
            //printf("key:%d\n",key);
            string value_str = words[3];
            //std::cout << value_str << std::endl;
            msg_to_send_back += insert(key,value_str);
            std::cout << msg_to_send_back << std::endl;
    }

    if(words[1]=="delete"){
            int key;
            stringstream geek(words[2]);
            geek >> key;
            printf("key:%d\n",key);
            msg_to_send_back += delete_key(key);
            std::cout << msg_to_send_back << std::endl;
    }

        
    if(words[1]=="update"){
            int key;
            stringstream geek(words[2]);
            geek >> key;
            string value_str = words[3];
            msg_to_send_back += update(key,value_str);
            std::cout << msg_to_send_back << std::endl;
    }

    if(words[1]=="concat"){
            int key1;
            stringstream geek(words[2]);
            geek >> key1;
            int key2 = atoi(words[3].c_str());
            geek >> key2;
            msg_to_send_back += concat(key1,key2);
            std::cout << msg_to_send_back << std::endl;
    }
            
    if(words[1]=="fetch"){
            int key;
            stringstream geek(words[2]);
            geek >> key;
            msg_to_send_back += fetch(key);
            std::cout << msg_to_send_back << std::endl;
    }
    cout << endl;
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    // debug(sent_to_client);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
        close(client_socket_fd);
        printf(BRED "Disconnected from client" ANSI_RESET "\n");
        return NULL;
    }
    sem_post(&curr_sem);
    return NULL;
    
}
void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################
    for(int cod=0;cod<30;cod++){
        thread_input[cod] = (td *)(malloc(sizeof(td)));
    }
    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        received_num = 0;
        
        string cmd = "";
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        cout << "Client sent : " << cmd;
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        /*if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }*/

        //////////// ______________________ ////////////
        pthread_mutex_lock(&copying_command_lock);
        thread_input[cmd_no]->idx = cmd_no;
        thread_input[cmd_no]->cmd = cmd;
        pthread_create(&t[cmd_no], NULL, process_command, (void *)(thread_input[cmd_no]));
        cmd_no++;
        pthread_mutex_unlock(&copying_command_lock);
        //////////// ______________________ ////////////
        
        if (thread_input[cmd_no-1]->cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        /*
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }*/
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    
    // return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    sem_init(&curr_sem, 0, no_of_worker_threads);
    for (i = 0; i < 101; i++)
    {
        values_arr[i]="no_value";
        flag_arr[i]=0; //unused right now or not being dealt with right now
    }
    int wel_socket_fd,  port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        handle_connection(client_socket_fd);
    }
    sem_destroy(&curr_sem);
    close(wel_socket_fd);
    return 0;
}