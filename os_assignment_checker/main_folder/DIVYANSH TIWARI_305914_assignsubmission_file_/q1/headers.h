#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

typedef struct course
{
    char name[100];
    double interest_quotient;
    int max_seats_allowed;
    int num_labs;
    int lab_ids[100];
    pthread_mutex_t course_mutex;
    pthread_cond_t course_condition;

    int done;
    int curr_lab_id;
    int curr_ta_id;

} course;

typedef struct student
{
    // necessary
    double calibre_quotient;
    int pref_one;
    int pref_two;
    int pref_three;
    int time;

    // flags
    int status;
    int permanent; // stores the course index if finalized
    int status_change;
    // char curr_course_name[100];
    int curr_pref;
    int curr_pref_number;

    pthread_mutex_t student_mutex;
    pthread_cond_t student_condition;
} student;

typedef struct lab
{
    // necessary
    char name_lab[100];
    int num_tas;
    int max_tuts;

    int tas_working_status[100];
    int taships_completed[100];

} lab;

int num_students, num_labs, num_courses, curr_time = 0;
int num_students_waiting[100] = {0};

course *Courses;
student *Students;
lab *Labs;

pthread_mutex_t time_mutex;
pthread_cond_t time_condition;