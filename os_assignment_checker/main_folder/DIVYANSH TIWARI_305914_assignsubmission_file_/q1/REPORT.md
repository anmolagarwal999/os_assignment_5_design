
# This is the report for Question 1

# The executable can be obtained by performing "gcc -pthread q1.c"

# File contents

> - All the include files, data structures and declaration are written in headers.h file.
> - The implementation of the program is done in q1.c file.

# NOTE:

> - **The bonus part has been implemented.**
>
> - **There weren't any deadlocks observed while testing the code.**
>

# ASSUMPTIONS

<br>

> - There has been put up a hard limit of the number of characters in name to be 100 so as to reduce the use of malloc.
>
> - Likewise, the number of TAs in each lab can be a maximum of 100.
>
> - A course can have a maximum of 100 labs associated with it.
>
> - Sleep() and usleep() are added in the program to get intentional delays.
>
> - TAs are being alloted to a course in case there is atleast 1 student in need of a tutorial.

# WORKFLOW

> - The main function initialises the threads, mutexes, conditional variables and later awaits the termination of the threads.
>
> - The threads, as per their definition, execute parallely.
>
> - The main function calls init_input() function which is used to take the inputs from the user.
>
> - Since, the very beginning of the program needs us to get the students registered at the time as given in input, there is a time_thread which increments the time at intervals of 1 second and also checks for the termination of program via the print_end() function.
>
> - The print_end() function checks if there are any running courses or not, and if there are any students still not decided or not. It takes actions accordingly via logic and sends signals to the student thread accordingly to get the desired output.
>
> - The students_function() takes care of all the status changes, and course changes related to the student whenever summoned via a broadcast signal according to the logic.
>
> - Each student has got a unique thread representing him/her.
>
> - The student_function() also takes care of the cases when the course currently preferred has withdrawn.
>
> - The courses available to the students are represented in form of unique course_threads working with course_function().
>
> - The course_function() works in a way by extracting the students seeking the course at the moment followed by a search for a free TA in a lab associated with the course where TA hasn't exhausted his limit of tutorials.
>
> - **As an implementation of bonus** the TA chosen here is the one who has completed the least number of tutorials via traversing through all the TA data.
>
> - After the selection of TA, the tut begins in form of statements and logic.
>
> - Signals are broadcasted to each student thread to handle state change and print statements accordingly whenever needed.
>
> - After the completion of the tutorial, the course thread calculates the probability of the concerned students accepting the course or rejecting it and moving forward.
>
> - The student decided to accept or reject a course on the basis of a random number generated if less than the probability calculated, or more.
>
> - Appropriate signals are broadcasted to the students in accordance with the decision to make and reflect the necessary changes.
>
> - In case, no TA is found, course is declared as **withdrawn** and the signals are broadcasted to students seeking this course to make and reflect appropriate changes.
> 