#ifndef _LOL_
#define _LOL_

int num_students,num_labs,num_courses;

struct Course
{
    char name[20];
    double interest;
    int max;
    int num_labs;
    int *labs;
};
typedef struct Course Course;
struct Student
{
    double calibre;
    int pref1;
    int pref2;
    int pref3;
    int time;
};
typedef struct Student Student;
struct Lab
{
    char name[20];
    int num_TAs;
    int max_TAs;
};
typedef struct Lab Lab;

#endif