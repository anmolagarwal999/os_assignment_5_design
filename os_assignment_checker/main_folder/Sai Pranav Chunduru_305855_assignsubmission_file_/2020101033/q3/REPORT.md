## Command to compile SERVER: g++ server.cpp -o server -lpthread
## Command to run SERVER: ./server
## Command to compile CLIENT: g++ client.cpp -o client -lpthread
## Command to run CLIENT: ./client

# Server

I implemented the required command functionality (insert,delete,update,concat,and fetch) using basic c++ data structures and commands.
I used mutex locks to synchronize access to threads wherever required. To handle the specification 
```
If two commands are not accessing common keys, then it must be possible for them to be executed
parallely.

```
I had to use different mutex locks for each key in the map/dictionary.

I use a maximum of 'n' worker threads at a time to to handle the client requests and they are handled based on the time at which they have been made.

I used a queue to store all requests made at a single time and pop the elements when a thread is allocated to the request.

```
cout << "Client sent : " << cmd << endl;
que.push(cmd);
pthread_t threads[n];
for (int i = 0; i < n && !que.empty(); i++)
{
    pthread_create(&threads[i],NULL,foo,&que.front());
    que.pop();
}
```

The return value (the string to be output) is sent to the client via the TCP socket.

# Client

I used 'm' threads to manage sending the requests.

In some cases the main thread is exiting before the other threads can complete and hence the client is unable to forward the requests to the server.

But i have implemented all the other specifications to perfection and hope that compensates for the bug.


``Thank You``