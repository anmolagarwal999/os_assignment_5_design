#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

struct list_item
{
    int sending_time;
    string command;
};

struct int_package
{int i;};

string m_str;
int m;
// int socket_fd;
vector <list_item> request_list;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

const long long int buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

int new_socket()
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    // cout << "new socket made with fd" <<socket_fd << endl;
    return socket_fd;
}

void* thread_func(void* arg)
{
    int_package* idx = (int_package*) arg;
    int whichindex = idx -> i;      
    // cout<<"thread "<<whichindex<<" made\n"; 
    // cout<<whichindex<<" "<<request_list[whichindex].command<<"\n";
    sleep(request_list[whichindex].sending_time);
    
    int socket_fd = new_socket();
    
    pthread_mutex_lock(&lock);
        send_string_on_socket(socket_fd, request_list[whichindex].command);
    pthread_mutex_unlock(&lock);
    
    int num_bytes_read;
    string output_msg;
    
    pthread_mutex_lock(&lock);
        tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
        cout << whichindex << ":"<<gettid()<<":"<<output_msg << endl;
    pthread_mutex_unlock(&lock);
    return NULL;
}

void take_input()
{
    string inp;
        string send_time_str = "";
        int send_time;
        getline(cin, inp);
            auto it = inp.begin();
            while(*it != ' '){
                send_time_str += *it;
                it++;
            }
            it++;
        send_time = stoi(send_time_str);
        string to_send(it, inp.end()); 
        list_item l;
        l.sending_time = send_time;
        l.command = to_send;
        request_list.push_back(l);
}

int main(int argc, char *argv[])
{
    // freopen("input.txt", "r", stdin);
    
    int i, j, k, t, n;
    getline(cin, m_str);
    m = stoi(m_str); //now i have m

    pthread_t threadpool[m];
    int_package indices[m];
    
    for(int i = 0; i<m; i++)
        take_input();

    for(int j = 0; j< m; j++){
        indices[j].i = j;
        pthread_create(&threadpool[j], NULL, thread_func, (void*) &indices[j]);
    }

    for(int j = 0; j< m; j++){
        pthread_join(threadpool[j], NULL);
    }

    return 0;
}