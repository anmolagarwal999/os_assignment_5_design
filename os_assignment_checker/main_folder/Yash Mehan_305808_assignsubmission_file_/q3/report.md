# report
## running
- compile the server and client files by running `make`
- open two terminal windows, in one, run `./ser` and supply suitable number as a command line argument
- in the other, run `./cli`. You can either input the requests manually via stdin, or redirect from a file using `<`, or uncomment the `freopen` line in `main()` and change the name of the input file
- run `make clean` to remove both executables

## working
- each request is sent from a client via a thread
- after that, the client side thread is `join`ed (basically dies)
- there are a fixed number of threads in the server side threadpool
- the server has a queue, where the incoming requests line up, and the working threads pick up requests from that queue
- in case number of requests is less than number of threads i.e. some threads are free, they will not hog CPU cycles (i.e. no busy waiting)
- the server side also outputs logs on stdout
- the client side executable finishes after sending all the given requests but the server has to be killed manually by `^C`