#ifndef HEADERS_H /* This is an "include guard" */
#define HEADERS_H
#include <string>
#define RED "\x1b[31m"
#define BRED "\x1b[1;31m"
#define GREEN "\x1b[1;32m"
#define YELLOW "\x1b[1;33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define node_max
extern struct node* root; //main root
extern pthread_mutex_t treeLock; //tree lock

struct client_attr{
    long time;
   std::string  message;
   int id;
};
struct client
{
    int client_socket_fd;
};
struct worker
{
    int id;
};

//functions
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void pthreadJoin(pthread_t thread, void **retval);
void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *mutex);
void pthreadMutexLock(pthread_mutex_t *mutex);
void pthreadMutexUnlock(pthread_mutex_t *mutex);
void pthreadCondBroadcast(pthread_cond_t *cv);
void pthreadMutexDestroy(pthread_mutex_t *m);
void pthreadCondDestroy(pthread_cond_t *cv);
void pthreadMutexInit(pthread_mutex_t *m);
void pthreadCondSignal(pthread_cond_t *cv);
std::string fetch(int key);
bool update(int key, std::string value);
bool insert(int key, std::string value);
bool deleteKey(int key);
std::string concat(int key1, int key2);
#endif
