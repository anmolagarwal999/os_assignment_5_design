# How to run?
make 
for client :./client
for server :./server n

# why pool of workers?
pool of worker threads: so that we don't create unbounded number of threads which can take up lot up memorty and hence finally slowing down the system
this ensures we stick to limited number of threads

# map.cpp
dictionary using binary tree
contains function :
    std::string fetch(int key);
    bool update(int key, std::string value);
    bool insert(int key, std::string value);
    bool deleteKey(int key);
    std::string concat(int key1, int key2);
    and other helper function

- each function is implemented in such a way that it locks only 1 or 2 nodes at a time and not whole map so that two client can operate at the same time

# server_prog.cpp
It changes dictionary according to the client request .  
As soon as the server starts, it  spawns ‘n’ worker threads who then waits on cv for new request to arrive.  
then server begins to listen for client requests to connect.  
Whenever a new client’s connection request is accepted,it ges added to the queue at one of the n thread who is waiting on waitforqueue condition v is signaled and the worker thread dequeues and fulfill the clients request using functions of map.cpp  
and send back the output

# client_sim.cpp
m threads are created according to the input.each thread wait on cv till timeout ,timeout for each thread is qiven   
then it sends corresponding request to the server using a TCP socket,read the output send by server and displays and then exits.  