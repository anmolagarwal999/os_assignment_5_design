#include <iostream>
using namespace std;
#include "headers.h"
pthread_mutex_t treeLock = PTHREAD_MUTEX_INITIALIZER;

struct node
{
    int key;           //key
    string value;      //value
    pthread_mutex_t m; //mutex
    struct node *left, *right;
};
//======================
node *root; //main root
//======================

// Create a node
struct node *newNode(int key, string value)
{
    struct node *temp = new node();
    temp->key = key;
    temp->value = value;
    pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
    temp->left = temp->right = NULL;

    return temp;
}


//fetch a node
string fetch(int key)
{ //lock whole tree to see if empty
  
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    { //unlock before returning
        pthreadMutexUnlock(&treeLock);
        return "";
    }
    //confirmed not empty so unlock
    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose value is to fetched
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return "";
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            { //unlock before returning
                pthreadMutexUnlock(&current->m);
                return "";
            }
            else
            {
                next = current->right;
            }
        }
        else
        { //unlock before returning
            pthreadMutexUnlock(&current->m);
            return current->value;
        }
        //unlock current node since we confired it is not the one
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}
//update
bool update(int key, string value)
{ //lock whole tree to see if empty
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    { //unlock before returning
        pthreadMutexUnlock(&treeLock);
        return false;
    }
    //confirmed not empty so unlock
    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose left or right node needs to be modifed
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return false;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return false;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {
            current->value = value;
            //unlock before returning
            pthreadMutexUnlock(&current->m);

            return true;
        }
        //unlock current node since we confired it is not the one to be modifed
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}

// Insert a node
bool insert(int key, string value)
{
    // Create a new Node containing
    // the new key value pair
    node *newnode = newNode(key, value);
    //lock whole tree to see if we want to insert at root
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        root = newnode;
        pthreadMutexUnlock(&treeLock);
        return true;
    }
    //confirmed not want to insert at root so unlock
    pthreadMutexUnlock(&treeLock);

    node *current = root;
    node *next = NULL;
    //lock current node since it coulde be the node whose left or right node needs to be modifed
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {
                current->left = newnode;
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return true;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree
            if (current->right == NULL)
            {
                current->right = newnode;
                //unlock before returning
                pthreadMutexUnlock(&current->m);
                return true;
            }
            else
            {
                next = current->right;
            }
        }
        else
        { //unlock before returning
            pthreadMutexUnlock(&current->m);
            return false;
        }
        //unlock current node since we confired it is not the one to be modifed
        pthreadMutexUnlock(&current->m);
        current = next;
        //lock next current node
        pthreadMutexLock(&current->m);
    }
}

bool removeCurrentNode(node *current, node *parent)
{
    //no children
    if (current->left == NULL && current->right == NULL)
    {
        if (parent)
        {
            if (current == parent->left)
            {
                parent->left = NULL;
            }
            else
            {
                parent->right = NULL;
            }

            pthreadMutexUnlock(&parent->m);
        }
        else{
            root =NULL;
        }
        pthreadMutexUnlock(&current->m);
        delete current;

        return true;
    }

    // only has a single child
    if (current->left == NULL || current->right == NULL)
    {

        node *shift = (current->right == NULL ? current->left : current->right);
        if (!parent)
        {
            root = shift;
            pthreadMutexUnlock(&current->m);
            delete current;
            return true;
        }
        else
        {
            if (current == parent->left)
            {
                parent->left = shift;
            }
            else
            {
                parent->right = shift;
            }
            pthreadMutexUnlock(&current->m);
            delete current;
            pthreadMutexUnlock(&parent->m);
            return true;
        }
    }

    else
    {
        // Two children
        node *min_parent = current;
        node *min = current->right;
        pthreadMutexUnlock(&parent->m);

        //  lock on current, min_parent , and min
        while (true)
        {
            if (min->left == NULL)
            { // Minimum element
                current->key = min->key;
                current->value = min->value;
                node *shift = min->right;
                if (min_parent == current)
                {
                    min_parent->right = shift;
                    delete min;
                }
                else
                {
                    min_parent->left = shift;
                    delete min;
                    pthreadMutexUnlock(&min_parent->m);
                }

                pthreadMutexUnlock(&current->m);
                return true;
            }
            // traverse to find minimum in right sub tree
            pthreadMutexLock(&min->left->m);
            if (min_parent != current)
            {
                pthreadMutexUnlock(&min_parent->m);
            }
            min_parent = min;
            min = min->left;
        }
    }
}

bool deleteKey(int key)
{
    //lock whole tree to see if its empty
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        pthreadMutexUnlock(&treeLock);
        return false;
    }

    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    node *parent = NULL;
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {

                pthreadMutexUnlock(&current->m);
                if (parent)
                {
                    pthreadMutexUnlock(&parent->m);
                }
                return false;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                pthreadMutexUnlock(&current->m);
                if (parent)
                {
                    pthreadMutexUnlock(&parent->m);
                }
                return false;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {
            removeCurrentNode(current, parent);
            return true;
        }
        if (parent)
        {
            pthreadMutexUnlock(&parent->m);
        }
        parent = current;
        current = next;
        pthreadMutexLock(&current->m);
    }
}

//fetch a node
node *fetchToConcat(int key)
{
    pthreadMutexLock(&treeLock);
    if (root == NULL)
    {
        pthreadMutexUnlock(&treeLock);
        return NULL;
    }

    pthreadMutexUnlock(&treeLock);
    node *current = root;
    node *next = NULL;
    pthreadMutexLock(&current->m);
    while (true)
    {
        if (key < current->key)
        { // Search on the left of this subtree
            if (current->left == NULL)
            {

                pthreadMutexUnlock(&current->m);
                return NULL;
            }
            else
            {
                next = current->left;
            }
        }
        else if (key > current->key)
        { // Search on the right of this subtree

            if (current->right == NULL)
            {
                pthreadMutexUnlock(&current->m);
                return NULL;
            }
            else
            {
                next = current->right;
            }
        }
        else
        {

            pthreadMutexUnlock(&current->m);
            return current;
        }

        pthreadMutexUnlock(&current->m);
        current = next;
        //printf("okay");
        pthreadMutexLock(&current->m);
    }
}

string concat(int key1, int key2)
{
    int f = 0;
    if (key2 < key1)
    {
        f = 1;
        int t = key1;
        key1 = key2;
        key1 = t;
    }
    node *a = fetchToConcat(key1);
    node *b = fetchToConcat(key2);
    if (a && b)
    {
        pthreadMutexLock(&a->m);
        pthreadMutexLock(&b->m);
        string x = a->value;
        a->value += b->value;
        pthreadMutexUnlock(&a->m);
        b->value += x;
        pthreadMutexUnlock(&b->m);
        if (!f)
            return b->value;
        return a->value;
    }

    return "";
}
