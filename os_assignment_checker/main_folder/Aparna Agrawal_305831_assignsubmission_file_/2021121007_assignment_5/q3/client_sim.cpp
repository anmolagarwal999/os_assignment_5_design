#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include "headers.h"
using namespace std;
/////////////////////////////

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
struct timespec start; //time when simulation started
int num_client;
pthread_cond_t timec = PTHREAD_COND_INITIALIZER;
pthread_mutex_t timeLock = PTHREAD_MUTEX_INITIALIZER;
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *client_function(void *inp)
{
    struct timespec to;
    struct timespec now;
    long time = ((struct client_attr *)inp)->time;
    string message = ((struct client_attr *)inp)->message;
    int id = ((struct client_attr *)inp)->id;
    pthreadMutexLock(&timeLock); //FOR CONDITION

    to.tv_sec = start.tv_sec + time;
    pthread_cond_timedwait(&timec, &timeLock, &to);
    pthreadMutexUnlock(&timeLock);
    /* clock_gettime(CLOCK_REALTIME, &now);
    cout<< message;
    printf("%ld",now.tv_sec-start.tv_sec);
    cout << endl; */
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, message);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << id << output_msg << endl;
    pthread_exit(0);
}
int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    cin >> num_client;
    string x;
    pthread_t client_threads[num_client];
    clock_gettime(CLOCK_REALTIME, &start);

    for (i = 0; i < num_client; i++)
    {  
        struct client_attr *a = new client_attr();
        a->id = i;
        cin >> a->time;
        cin >> ws;
        
        getline(cin, a->message);

        pthreadCreate(&client_threads[i], NULL, client_function, (void *)(a));
    }
    for (i = 0; i < num_client; i++)
    {   
        pthreadJoin(client_threads[i],NULL);
    }
    return 0;
}



