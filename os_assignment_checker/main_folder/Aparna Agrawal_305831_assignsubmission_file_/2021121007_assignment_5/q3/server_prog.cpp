#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include "headers.h"
#include <queue>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define MAX_WORKERS 100
#define PORT_ARG 8001

const int initial_msg_len = 256;
int num_workers;
pthread_t worker_pool[MAX_WORKERS];
queue<struct client> client_Q;
pthread_mutex_t queueLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t waitforclient = PTHREAD_COND_INITIALIZER;
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

// modifies map accordingto the request made
string handle_map(string message)
{
    size_t pos = 0;
    int i = 0;
    std::string token;
    string arr[10];
    //----------extract the request-----------------
    while ((pos = message.find(" ")) != std::string::npos)
    {
        token = message.substr(0, pos);
        arr[i] = token;
        i++;
        message.erase(0, pos + 1);
    }
    if (message.length()!= 0)
    {
        arr[i] = message;
        i++;
    }

    //-----------------------------------------------
    //accordingly react
    if (arr[0].compare("insert") == 0)
    {
        if (i != 3)
        {
            cout << i;
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (insert(key, arr[2]))
        {
            return "Insertion successful";
        }

        return "Key already exists";
    }
    else if (arr[0].compare("delete") == 0)
    {
        if (i != 2)
        {
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (deleteKey(key))
        {
            return "Deletion successful";
        }
        else
        {
            return "No such key exists";
        }
    }
    else if (arr[0].compare("update") == 0)
    {
        if (i != 3)
        {
            return "invalid command";
        }
        int key = stoi(arr[1]);
        if (update(key, arr[2]))
        {
            return arr[2];
        }
        else
        {
            return "Key does not exist";
        }
    }
    else if (arr[0].compare("fetch") == 0)
    {
        if (i != 2)
        {
            cout << i;
            return "invalid command";
        }
        int key = stoi(arr[1]);
        string x = fetch(key);
        if (x == "")
        {
            return "Key does not exist";
        }

        return x;
    }
    else if (arr[0].compare("concat") == 0)
    {
        if (i != 3)
        {
            return "invalid command";
        }
        int key1 = stoi(arr[1]);
        int key2 = stoi(arr[2]);
        string x = concat(key1, key2);
        if (x == "")
        {
            return "Concat failed as at least one of the keys does not exist";
        }

        return x;
    }
    else
    {
        cout << arr[0];
        return "invalid command";
    }
}

void handle_connection(int client_socket_fd, int id)
{
    int received_num, sent_num;
    /* read message from client */
    int ret_val = 1;
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = received_num;

    //handle error
    if (ret_val <= 0)
    {
        // perror("Error read()");
        printf("Server could not read msg sent from client\n");
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //printout what client send
    cout << "Client sent : " << cmd << endl;
    //if client send an exit comd close the socket
    if (cmd == "exit")
    {
        cout << "Exit pressed by client" << endl;
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //manipulate map according to the request made
    string feedback = handle_map(cmd);
    string subpart = ":" + to_string(id);
    string msg_to_send_back = subpart + ":" + feedback;

    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read.
    //Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

    sleep(2); //asked to do in question
    //send back the verdict
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);

    //handle error
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
        close(client_socket_fd);
        printf(BRED "Disconnected from client" RESET "\n");
        return;
    }
    //close the socket
    close(client_socket_fd);
    printf(BRED "Disconnected from client" RESET "\n");
    return;
}

//thread function
void *workersJob(void *inp)
{
    int id = ((struct worker *)inp)->id;
    while (true)
    {
        //lock to modify shared queue
        pthreadMutexLock(&queueLock);
        while (client_Q.empty())
        { //wait till new client arrive and UNlock till then
            pthreadCondWait(&waitforclient, &queueLock);
        }
        struct client pclient = client_Q.front();
        client_Q.pop();
        //unlock since modification is done
        pthreadMutexUnlock(&queueLock);

        handle_connection(pclient.client_socket_fd, id);
    }
}

int main(int argc, char *argv[])
{

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    if (argc != 2)
    {
        return 0;
    }
    else
    {

        num_workers = atoi(argv[1]);
    }
    worker_pool[num_workers];

    //spawn these ‘n’ worker threads.
    for (int i = 0; i < num_workers; i++)
    {
        struct worker w;
        w.id = i;
        //cout << i;
        pthreadCreate(&worker_pool[i], NULL, workersJob, &w);
    }
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door of the server process. 
        When the server “hears” the knocking, it creates a new door—more precisely,
         a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(GREEN "New client connected from port number %d and IP %s \n" RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        struct client c;

        c.client_socket_fd = client_socket_fd;
        //lock the queue to modify saving it from race condition
        pthreadMutexLock(&queueLock);
        client_Q.push(c);
        //queue is modified successfully unlock the queuelock
        pthreadMutexUnlock(&queueLock);
        //signal the worker waiting for client
        pthreadCondSignal(&waitforclient);
    }

    close(wel_socket_fd);
    return 0;
}

