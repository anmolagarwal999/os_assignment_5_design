#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>
pthread_cond_t checkSeat = PTHREAD_COND_INITIALIZER;
pthread_cond_t waitForTA = PTHREAD_COND_INITIALIZER;
pthread_cond_t checkPref = PTHREAD_COND_INITIALIZER;
pthread_cond_t timec = PTHREAD_COND_INITIALIZER;
int studentsExited = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;                   //syncronises the acces of studentsExited
pthread_mutex_t studentLock= PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t preferenceLock = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t TALock = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t labLocks[lab_max];       // each lab has a lock,multiple courses can look for TA in different lab at the same time
pthread_mutex_t courseLocks[course_max]; //each course has alock, multiple students can update studentcurrent(student ids currrently

int main()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    if (num_students <= 0)
    {
        printf("invalid students\n");
        return 0;
    }
    else if (num_labs <= 0)
    {
        printf("invalid labs\n");
        return 0;
    }
    else if (num_courses <= 0)
    {
        printf("Invalid courses\n");
        return 0;
    }

    //Declaring the pthreads
    pthread_t students_thread[num_students];
    pthread_t courses_thread[num_courses];

    //printf("%d %d %d",num_students,num_labs,num_courses);

    //course inputs
    for (int i = 0; i < num_courses; i++)
    {
        courses_attr[i] = (struct course *)(malloc(sizeof(struct course)));
        scanf("%s %f %d %d", courses_attr[i]->name, &courses_attr[i]->interest, &courses_attr[i]->course_max_slot, &courses_attr[i]->lab_ids[0]);
        //printf("%d %s %f %d %d",i,courses[i].name,courses[i].interest,courses[i].course_max_slot,courses[i].lab_ids[0]);
        //printf("%d",courses[i].lab_ids[0]);
        for (int j = 1; j <= courses_attr[i]->lab_ids[0]; j++)
        {
            scanf("%d", &courses_attr[i]->lab_ids[j]);
        }
        courses_attr[i]->TA_mentor = -1;
        courses_attr[i]->active = 1;
        courses_attr[i]->NumStudentCurrentP = 0;
        pthreadMutexInit(&courseLocks[i]);
    }
    //student inputs
    for (int i = 0; i < num_students; i++)
    {
        students_attr[i] = (struct student *)(malloc(sizeof(struct student)));
        scanf("%f %d %d %d %d", &students_attr[i]->calibre, &students_attr[i]->preference[0], &students_attr[i]->preference[1], &students_attr[i]->preference[2], &students_attr[i]->t);
        students_attr[i]->current_pref = 0;
        students_attr[i]->available = 0;
    
    }
    //labs inputs and initialization
    for (int i = 0; i < num_labs; i++)
    {
        labs_attr[i] = (struct lab *)(malloc(sizeof(struct lab)));
        scanf("%s %d %d", labs_attr[i]->name, &labs_attr[i]->n_i, &labs_attr[i]->limit);
        for (int r = 0; r < labs_attr[i]->n_i; r++)
        {
            labs_attr[i]->mentors[r].tut_left = labs_attr[i]->limit;
            labs_attr[i]->mentors[r].status = 0;
        }
        labs_attr[i]->nextTA = 0;
       pthreadMutexInit(&labLocks[i]);
    }

    //start
    clock_gettime(CLOCK_REALTIME, &start);

    //create threads
    for (int i = 0; i < num_courses; i++)
    {
        pthread_t curr_tid;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->idx = i;
        pthreadCreate(&curr_tid, NULL, course_function, (void *)(thread_input));
        courses_thread[i] = curr_tid;
    }
    for (int i = 0; i < num_students; i++)
    {
        pthread_t curr_tid;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->idx = i;
        pthreadCreate(&curr_tid, NULL, student_function, (void *)(thread_input));
        students_thread[i] = curr_tid;
         
    }

    //pthreadJoin
    for (int i = 0; i < num_students; i++)
    {
        pthreadJoin(students_thread[i], NULL);
    }

    for (int i = 0; i < num_courses; i++)
    {
        pthread_cancel(courses_thread[i]);
    }

    //Destry

    pthreadCondDestroy(&checkSeat);
    pthreadCondDestroy(&waitForTA);
    pthreadCondDestroy(&checkPref);
    pthreadCondDestroy(&timec);
    pthreadMutexDestroy(&mutex);
     for (int i = 0; i < num_courses; i++)
    {
       pthreadMutexDestroy(&courseLocks[i]);
       free(courses_attr[i]);
    }

    for (int i = 0; i <  num_labs; i++)
    {
         pthreadMutexDestroy(&labLocks[i]);
         free(labs_attr[i]);
    }



}