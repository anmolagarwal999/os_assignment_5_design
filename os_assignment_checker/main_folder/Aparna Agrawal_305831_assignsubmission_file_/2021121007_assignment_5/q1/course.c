#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>

/* 
Event 7 : Seats have been allocated for the course
“Course c_i has been allocated j seats”
● Event 8 : TA has started the tutorial with k seats, where k <= number of seats allocated for the course (j from
the previous step)
“Tutorial has started for Course c_i with k seats filled out of j”
● Event 9 : TA has completed the tutorial and left the course
“TA t_j from lab l_k has completed the tutorial and left the course c_i”
● Event 10 : The course doesn’t have any eligible students for TA ship available and is removed from the course
offerings.
“Course c_i doesn’t have any TA’s eligible and is removed from course offerings”
 */

//function to allocate seats to students
int Allocate_seats(int id, int rand_seats) //called inside pthreadMutexLock(&courseLocks[id]);
{
    int count = 0;
    int student_id;
    while (courses_attr[id]->NumStudentCurrentP) //run till some student has current preference as this course
    {
        student_id = courses_attr[id]->studentcurrent[0];
       // pthreadMutexLock(&studentLock); //guards students_attr to change available

        students_attr[student_id]->available = 0;

        //pthreadMutexUnlock(&studentLock);

        for (int i = 1; i < courses_attr[id]->NumStudentCurrentP; i++)
            courses_attr[id]->studentcurrent[i - 1] = courses_attr[id]->studentcurrent[i];
        courses_attr[id]->NumStudentCurrentP--;
        count++;

        
        if (count == rand_seats) //if all tutorial seats are filled break
        {
            break;
        }
    }
    return count;
}

int AllocateNextTA(int id) //used inside pthreadMutexLock(&labLocks[id]);
{
    int flag = 0;

    for (int i = 0; i < labs_attr[id]->n_i; i++)
    {
        if (labs_attr[id]->mentors[i].status == 0 && labs_attr[id]->mentors[i].tut_left) //free TA with TAship not crossed limit
        {
            flag = 2;
            labs_attr[id]->nextTA = i;
            break;
        }
        if (labs_attr[id]->mentors[i].tut_left) //there is TA ship left but every one is busy rightnow
        {
            flag = 1;
        }
    }

    return flag;
}

int AllocateTA(int id, int *n, char *c, int *TAlab)
{ //printf("[id]");
    int flag = 0;
    for (int t = 1; t <= courses_attr[id]->lab_ids[0]; t++) //diffrent threads of courses checks each lab prefered by them for TA
    {
        int i = courses_attr[id]->lab_ids[t];
        pthreadMutexLock(&labLocks[i]);
        if (labs_attr[i]->nextTA == -1) //there is TA ship left but every one is busy rightnow
        {
            flag = AllocateNextTA(i); // updates nextTA
        }
        if (labs_attr[i]->nextTA >= 0)
        {
            flag = 2;
            courses_attr[id]->TA_mentor = labs_attr[i]->nextTA;                                  //Assign TA to course
            labs_attr[i]->mentors[labs_attr[i]->nextTA].status = 1;                              //change status of TA
            *n = labs_attr[i]->limit - labs_attr[i]->mentors[labs_attr[i]->nextTA].tut_left + 1; // set which TA ship to print by course funtion
            labs_attr[i]->mentors[labs_attr[i]->nextTA].tut_left -= 1;                           //reduce the TA ship left
            strcpy(c, labs_attr[i]->name);
            labs_attr[i]->nextTA = -1;
            *TAlab = i;
            AllocateNextTA(i); // updates nextTA
        }
        pthreadMutexUnlock(&labLocks[i]);
        if (flag == 2)
        {
            break;
        }
    }

    return flag;
}

void *course_function(void *inp)
{
    int TAlab = 0;
    int n = 0;
    char c[64];
    int TAstatus = 0;
    int id = ((struct thread_details *)inp)->idx;
    sleep(1); //students are filling
    while (1)
    {
        // -------------------wait on checkPref untill someone has this course as preferce or all students exited------------------------
        pthreadMutexLock(&preferenceLock);

        while (courses_attr[id]->NumStudentCurrentP == 0)
        {
            pthreadCondWait(&checkPref, &preferenceLock); //wait till student preference changes to this course or all student exited
            //printf("[%d]", studentsExited);
            //checks if all student exited
            if (studentsExited == num_students)
            {
                pthreadMutexUnlock(&preferenceLock);
                printf(RED "Course %s  is removed from course offering"RESET"\n", courses_attr[id]->name);
                pthread_exit(0);
            }
        }
        pthreadMutexUnlock(&preferenceLock);

        //--------------//Allocate TA//--------------------------//
        TAstatus = AllocateTA(id, &n, c, &TAlab);

        //wait for TA to be free
        pthreadMutexLock(&TALock);
        while (TAstatus == 1)
        {
            //printf("ohno%d\n",id);
            pthreadCondWait(&waitForTA, &TALock);
            TAstatus = AllocateTA(id, &n, c, &TAlab);
        }
        pthreadMutexUnlock(&TALock);
        
        if (TAstatus == 0) //All TAs from prefered labs has exhausted his TA ship
        {
            printf(RED "Course %s doesn’t have any TA’s eligible and is removed from course offering"RESET"\n", courses_attr[id]->name);
            courses_attr[id]->active = 0;
            pthreadCondBroadcast(&checkSeat);
            pthread_exit(0);
        }

        printf(BLUE "TA %d from lab %s has been allocated to course %s for his %dth TA ship" RESET"\n", courses_attr[id]->TA_mentor, c, courses_attr[id]->name, n);

        //--------------//TA is allocated or course is withdrawn//----------//

        int rand_seats = rand() % courses_attr[id]->course_max_slot + 1; //seats alotted for tutorial
        printf(MAGENTA "Course %s has been allocated %d seats"RESET"\n", courses_attr[id]->name, rand_seats);
        sleep(1);
        //------------Allocate seats to students----------------------------//
        pthreadMutexLock(&courseLocks[id]);
        int seats = Allocate_seats(id, rand_seats);
        pthreadMutexUnlock(&courseLocks[id]);
        pthreadCondBroadcast(&checkSeat); // updates if student got a seat
        sleep(1);
        printf(BLUE "Tutorial has started for Course %s with %d seats filled out of %d" RESET  "\n", courses_attr[id]->name, seats, rand_seats);

        //------------------tutorial-----------------------------------------//
        sleep(5);


        printf( BLUE "TA %d from lab %s has completed the tutorial and left the course %s" RESET "\n", courses_attr[id]->TA_mentor, c, courses_attr[id]->name);

         //change status of TA
        pthreadMutexLock(&labLocks[TAlab]);
        labs_attr[TAlab]->mentors[courses_attr[id]->TA_mentor].status = 0;
        pthreadMutexUnlock(&labLocks[TAlab]);

        //-------------------tutorial over-----------------------------------------//

        pthreadCondBroadcast(&waitForTA); //notify threads who are waiting for TA
    }
}
