
### some limits
#define Max  100
#define TA_max  100
#define lab_max 1000
#define student_max 1000
#define course_max 1000

### Entities attributes

    struct TA{
        int tut_left;      //initialised to limit of course and then decremented as TA is alocatted to course
        int status;        //set when TA is allocated to course and 0 when it gets free
    };
    struct lab{
        char name[64];      //name of lab
        int n_i;            //number of tAs
        int limit;          //limit number of times a member of the lab can be TA mentor in the semester
        int nextTA;         //next free TA of lab
    struct TA mentors[Max];
    };
    struct course{
    char name[64];
    float interest;        //general student interest in the course over the past year. This value is a number between 0 and 1.
    int lab_ids[Max];      // number of labs at 0th index
    int TA_mentor;         // TA allocated
    int course_max_slot;   //max slots given to course
    int NumStudentCurrentP;    // students currently interested in course
    int studentcurrent[student_max];   // student ids currrently interested in course
    int active;            // 0 if no more TA can be allocated 
    };
    struct student{
        
        int preference[3];  //input preferences
        int current_pref;   //current course preference
        int t;          //time when preference whas filled
        float calibre;  
        int available;  // set if student is available
    };


## globals

    struct timespec start;              //time when simulation started 
    struct lab *labs_attr[lab_max];
    struct course *courses_attr[course_max];
    struct student *students_attr[student_max];


    pthread_mutex_t preferenceLock;            //used with checkPref cv
    pthread_mutex_t TALock;                   //used with wait for TA cv
    pthread_mutex_t studentLock;              //used with checkseat cv
    
    pthread_mutex_t mutex;                     //syncronises the acces of tudentsExited
    pthread_mutex_t labLocks[lab_max];      // each lab has a lock,multiple courses can look for TA in different lab at the same time
    pthread_mutex_t courseLocks[course_max];    //each course has alock, multiple students can update studentcurrent(student ids currrently 
                                            //interested in course) in different courses athe same time


    pthread_cond_t timec;                       //used with  pthread_cond_timedwait for student to wait till they fill the preference
    extern pthread_cond_t checkSeat;     //used to wait for course to allocate student a seat in tutorial 
    extern pthread_cond_t waitForTA;    //used to wait for one of the TA from prefered labs to be free and has TA ship left
    extern pthread_cond_t checkPref;    //used to wait for student to have the course as priority

    extern int studentsExited;          //used to keep track of students who exited the simulation




## Logic
errorHandle.c file :contains erroer handling of pthread functions  
cousrse.c function of threads of courese  
student.c functions of threads of students  
main.c :input and execution  

### course
1.waits for at least one student to have current preference as this course (pthread_cond_t checkSeat and pthread_mutex_t preferenceLock is used )  
- student broadcast to each thread waiting on checkSeat  when it gets available and  increment the student count having the course as preferece in course_attr and append its id in studentcurrent of course_attr  
- student broadcast to each thread waiting on checkSeat when it exit to check if all student exited course thread must too exit

2.Allocates TA :
- diffrent threads of courses checks each lab prefered by them for TA
- function AllocateTA return 
        :1 if there is TA ship left but every TA is busy rightnow
        :2 if TA is assigned
        :0 if there is no TA left with >0 TAship
  AllocateNextTA is helper function to AllocateTA which beforehand finds the TA which is available 
- used lablocks[i] mutex to syncronize the the updation of tut_left of a TA status and nextTA of lab
 
- waits on waitForTA untill TAstatus is 1 ie there is TA with TAship left but all TAs are busy right now
- when TA left a course all threads waiting  on waitForTA are awaken and it again try to allocate TA 
- if TAstatus changes to 0 -> All TAs from prefered labs has exhausted his TA ship thus  course is withdrawn
-  if TAstatus changes to 2 -> tA is allocated

3. Allocate seats to students:
- Allocate_seats function is used  (called inside pthreadMutexLock(&courseLocks[id]);)
    it removes the 0th student from the list in course_attr ,decrements the NumStudentCurrentP  
    and under stdudentLocks changes the student_attr->available to 0 that is student is allocated seat and now is busy
    untill there is a student id in list or rand_seats(seat allocated to tut) has been exhausted.
- broad cast to student threads waiting on checkseats (notifies student threads to check if they got the seat in tut)
4.  sleeps while taking tuorial
5. TA lefts the course and fucntion updates TA status to 0 under labLocks  to show tha tA is available now
- broadcast to each  COURSE tHread waitng on waitForTA to make them check if the TA from there prefered lab has exitied
6.loop



### student
1.used  pthread_cond_timedwait to wait till the time when student thread finally fiils the preference  

2.After filling preference,(locked with courseLocks[preferedcourse])adds id to preffered course studentcurrent and wait on checkseat condition till the course 
allocate the seat or course is withdrawn  

3.if course is withdrawn change the preference tell the corresponding course or exit if no more preference   

4.sleeps some second swhile attending tut and making decision if want to look for other or select this course  

5.decision is taken by this   
    probability = preferd course interest * calibre * 100;  
        int decision = rand() % 100 <= probability;  

6. if selects peremently notify course waiting for students to check if all student have exited(broadcast to all threads waiting on checkpref) 

7.else withdrawn, change prefernce to next preferece untill a non-withdrawn course or no more preferences  

8.loop  

### main
 input 
 initialization
 after simulation
  free memory
  destroy cv,mutex

## how to run 
make
./a.out
