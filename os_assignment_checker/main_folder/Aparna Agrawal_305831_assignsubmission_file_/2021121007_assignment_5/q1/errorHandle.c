#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"

void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg)
{
    if (pthread_create(thread, attr, start_routine, arg) != 0)
    {
        perror("Thread was not created");
    }
}

void pthreadJoin(pthread_t thread, void **retval)
{
    if (pthread_join(thread, retval) != 0)
    {
        perror("Thread could not join");
    }
}

void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *m)
{
    if (pthread_cond_wait(cv, m) != 0)
    {
        perror("Cannot cond wait on cv");
    }
}

void pthreadMutexLock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_lock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadMutexUnlock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_unlock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadCondBroadcast(pthread_cond_t *cv)
{
    if (pthread_cond_broadcast(cv) != 0)
    {
        perror("Cannot broadcast");
    }
}

void pthreadMutexInit(pthread_mutex_t *m)
{
    if (pthread_mutex_init(m,0) != 0)
    {
        perror("Cannot initialize mutex");
    }
}


void pthreadMutexDestroy(pthread_mutex_t *m)
{
    fflush(stdout);
    int f = pthread_mutex_destroy(m);
    if ( f!= 0)
    {   printf("%d",f);
        perror("Cannot destroy mutex");
    }
}

void pthreadCondDestroy(pthread_cond_t *cv)
{
    fflush(stdout);
    if ( pthread_cond_destroy(cv)!= 0)
    {
        perror("Cannot destroy cv");
    }
}

