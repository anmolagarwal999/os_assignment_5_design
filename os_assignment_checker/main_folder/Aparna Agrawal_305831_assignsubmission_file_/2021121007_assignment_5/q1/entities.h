#ifndef ENTITIES_H /* This is an "include guard" */
#define ENTITIES_H
#define Max 100

#define TA_max 100
#define lab_max 1000
#define student_max 1000
#define course_max 1000

#define RED "\x1b[31m"
#define BRED "\x1b[1;31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int num_students, num_labs, num_courses;

typedef struct thread_details
{
    int idx;
} td;

struct TA
{
    int tut_left; //initialised to limit of course and then decremented as TA is alocatted to course
    int status;   //set when TA is allocated to course and 0 when it gets free
};
struct lab
{
    char name[64]; //name of lab
    int n_i;       //number of tAs
    int limit;     //limit number of times a member of the lab can be TA mentor in the semester
    int nextTA;    //next free TA of lab
    struct TA mentors[Max];
};
struct course
{
    char name[64];
    float interest;                  //general student interest in the course over the past year. This value is a number between 0 and 1.
    int lab_ids[Max];                // number of labs at 0th index
    int TA_mentor;                   // TA allocated
    int course_max_slot;             //max slots given to course
    int NumStudentCurrentP;          // students currently interested in course
    int studentcurrent[student_max]; // student ids currrently interested in course
    int active;                      // 0 if no more TA can be allocated
};
struct student
{

    int preference[3]; //input preferences
    int current_pref;  //current course preference
    int t;             //time when preference whas filled
    float calibre;
    int available; // set if student is available
};
struct timespec start; //time when simulation started
struct lab *labs_attr[lab_max];
struct course *courses_attr[course_max];
struct student *students_attr[student_max];




extern pthread_mutex_t preferenceLock;
extern pthread_mutex_t TALock;
extern pthread_mutex_t studentLock;
extern pthread_mutex_t mutex;                   //syncronises the acces of studentsExited
extern pthread_mutex_t labLocks[lab_max];       // each lab has a lock,multiple courses can look for TA in different lab at the same time
extern pthread_mutex_t courseLocks[course_max]; //each course has alock, multiple students can update studentcurrent(student ids currrently
                                         //interested in course) in different courses athe same time


extern pthread_cond_t timec;                    //used with  pthread_cond_timedwait for student to wait till they fill the preference
extern pthread_cond_t checkSeat; //used to wait for course to allocate student a seat in tutorial
extern pthread_cond_t waitForTA; //used to wait for one of the TA from prefered labs to be free and has TA ship left
extern pthread_cond_t checkPref; //used to wait for student to have the course as priority

extern int studentsExited; //used to keep track of students who exited the simulation

//functions
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void pthreadJoin(pthread_t thread, void **retval);
void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *mutex);
void pthreadMutexLock(pthread_mutex_t *mutex);
void pthreadMutexUnlock(pthread_mutex_t *mutex);
void pthreadCondBroadcast(pthread_cond_t *cv);
void pthreadMutexDestroy(pthread_mutex_t *m);
void pthreadCondDestroy(pthread_cond_t *cv);
void pthreadMutexInit(pthread_mutex_t *m);
void *student_function(void *inp);
void *course_function(void *inp);

#endif
