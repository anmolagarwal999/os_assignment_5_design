#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>

/* Event 1: Student filled in preferences for course registration
“Student i has filled in preferences for course registration”
● Event 2 : Student has been allocated a seat in a particular course
“Student i has been allocated a seat in course c_j”
● Event 3 : Student has withdrawn from a course
“Student i has withdrawn from course c_j”
● Event 4 : Student has changed their preference
“Student i has changed current preference from course_x (priority k) to course_y (priority k+1)”
● Event 5 : Student has selected a course permanently
“Student i has selected course c_j permanently”
● Event 6 : Student either didn’t get any of their preferred courses or has withdrawn from them and has exited
the simulation
“Student i couldn’t get any of his preferred courses” */

void *student_function(void *inp)
{
    char *courseAllotted;
    char *nextcourse;
    int coursepreferd;
    struct timespec to;
    int id = ((struct thread_details *)inp)->idx;

    //----------------wait to fill the preference---------------------

    pthreadMutexLock(&preferenceLock); //FOR CONDITION

    to.tv_sec = start.tv_sec + students_attr[id]->t; //time till student waitted to fill the preference
    pthread_cond_timedwait(&timec, &preferenceLock, &to);
    printf(GREEN "Student %d has filled in preferences for course registration"RESET "\n", id);
    pthreadMutexUnlock(&preferenceLock);

    //-------------STUDENT HAS FILLED THE PREFERENCE -------------------

    while (1)
    {
        //-------------tell prefered course this student is available and  waiting-----------------------------
        //make student available
        coursepreferd = students_attr[id]->preference[students_attr[id]->current_pref];
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //pthreadMutexLock(&studentLock);
        students_attr[id]->available = 1; //student is available now
        // pthreadMutexUnlock(&studentLock);
        //increment prefered course NumStudentCurrentP,add student id to course studentcurrent list
        pthreadMutexLock(&courseLocks[coursepreferd]);
        courses_attr[coursepreferd]->studentcurrent[courses_attr[coursepreferd]->NumStudentCurrentP] = id;
        courses_attr[coursepreferd]->NumStudentCurrentP++;
        pthreadMutexUnlock(&courseLocks[coursepreferd]);

        pthreadCondBroadcast(&checkPref); //broadcast to courses waiting on students with that couse as preference

        //---------------WAIT TILL STUDENT GETS ALLOTED TO A COURSE-------------
        pthreadMutexLock(&studentLock);
        while (students_attr[id]->available == 1)
        {


            courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;

            //-----------------------------------CHECK IF COURSE IS WITHDRAWN---------------------------------------------------
            while (students_attr[id]->current_pref <= 2 && !courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->active)
            {
                courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
                students_attr[id]->current_pref++;
                nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
                coursepreferd = students_attr[id]->preference[students_attr[id]->current_pref];
                //--------------------tell course-----------------------------------
                pthreadMutexLock(&courseLocks[coursepreferd]);
                courses_attr[coursepreferd]->studentcurrent[courses_attr[coursepreferd]->NumStudentCurrentP] = id;
                courses_attr[coursepreferd]->NumStudentCurrentP++;
                pthreadMutexUnlock(&courseLocks[coursepreferd]);

                printf( YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
                pthreadCondBroadcast(&checkPref); //broadcast to courses waiting on students with that couse as preference
            }
            if (students_attr[id]->current_pref > 2)
            {
                printf(BRED "Student %d couldn’t get any of his preferred courses"RESET"\n", id);
                pthreadMutexLock(&mutex);
                studentsExited++;
                pthreadMutexUnlock(&mutex);
                pthreadMutexUnlock(&studentLock);
                pthreadCondBroadcast(&checkPref);
                pthread_exit(0);
            }
             //-----------------------------WAIT FOR SEAT ---------------------------------------------------------------------------
            pthreadCondWait(&checkSeat, &studentLock);
            
            //printf("{%d %d}\n",students_attr[id]->available,id);
        }

        courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
        printf("Student %d has been allocated a seat in course %s\n", id, courseAllotted);

        pthreadMutexUnlock(&studentLock);
        //==================================================================================================

        //---------------STUDENT GETS ALLOTED TO A COURSE----------------------------

        //-----------student attending tutorial-------------------
        sleep(8);
        //-----------TUTORIAL OVER---------------------------------------

        //-------------CHECK IF STUDENT WIDRAWED OR CHOOSED COURSE PERMANENTLY-----------------------

        int probability = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->interest * students_attr[id]->calibre * 100;
        int decision = rand() % 100 <= probability;
        if (decision == 1)
        {
            printf(BRED "Student %d has selected course %s permanently" RESET "\n", id, courseAllotted);
            pthreadMutexLock(&mutex); //GUARDS studentsExited
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref); //NOTIFY course waiting for students to check if all student have exited
            pthread_exit(0);                  //EXITED
        }

        printf(YELLOW "Student %d has withdrawn from course %s"RESET"\n", id, courseAllotted);
        //--------------------------------------------------------------------------------------------

        //-------------Student withdrawn will go for next preference --------------------------------

        // change preference to next priority
        students_attr[id]->current_pref++;

        // there is no next preference
        if (students_attr[id]->current_pref > 2)
        {
            printf(BRED "Student %d couldn’t get any of his preferred courses" RESET "\n", id);
            pthreadMutexLock(&mutex);
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref);
            pthread_exit(0);
        }

        nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;

        //cehck if next preference couse is withdrawn if so change to next one
        while (students_attr[id]->current_pref <= 2 && !courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->active)
        {
            printf(YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
            courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
            students_attr[id]->current_pref++;
            nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
        }
        // there is no next active course preference so exit
        if (students_attr[id]->current_pref > 2)
        {
            printf(BRED "Student %d couldn’t get any of his preferred courses"RESET"\n", id);
            pthreadMutexLock(&mutex);
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref);
            pthread_exit(0);
        }
        // next active  course preference
        else
        {

            printf(GREEN"Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
        }
        //---------------------------------loop-----------------------------------------------
    }
}
