#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>
long H_goals = 0;
long A_goals = 0;

//Mutex
pthread_mutex_t personLocks[person_max];
pthread_mutex_t groupLocks[Max];
pthread_mutex_t seatlock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t A_goalLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t H_goalLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t goalLock = PTHREAD_MUTEX_INITIALIZER;

//cV
pthread_cond_t timec[person_max];
pthread_cond_t waitforGoal_A = PTHREAD_COND_INITIALIZER;
pthread_cond_t waitforGoal_H = PTHREAD_COND_INITIALIZER;
pthread_cond_t waitforseat = PTHREAD_COND_INITIALIZER;
pthread_cond_t waitforgoal = PTHREAD_COND_INITIALIZER;

/* Person has reached the stadium
“Person p_i has reached the stadium”
● Person has got a seat in a zone
“Person p_i has got a seat in zone z_j”
● Person couldn’t get a seat
“Person p_i couldn’t get a seat”
● Person watched the match for X time and is leaving
“Person p_i watched the match for X seconds and is leaving”
● Person is leaving due to the bad performance of his team
“Person p_i is leaving due to the bad defensive performance of his team”
● Person is waiting for his friends at the exit [Only if BONUS has been implemented]
“Person p_i is waiting for their friends at the exit” */

int seats(struct spectator *s) //called insinde seatlock mutex
{
    int seat = 0;
    int count = 0;
    char arr[3];


        if (capacity_H && (s->person_type == 'H'|| s->person_type == 'N'))
        {
            seat = 1;
            arr[count] = 'H';
            count++;
        }
        if (capacity_N && (s->person_type == 'H'|| s->person_type == 'N'))
        {
            seat = 1;
            arr[count] = 'N';
            count++;
        }
         if (capacity_A && (s->person_type == 'A'|| s->person_type == 'N'))
        {
            seat = 1;
            arr[count] = 'A';
            count++;
            
        }

    //------------seat allocate randomly-----------------------------
    if (seat == 0)
    {
        return 0;
    }
    int choice = rand() % count;
    if (arr[choice] == 'H')
    {
        capacity_H--;
        s->zone = 'H';
    }
    else if (arr[choice] == 'N')
    {
        capacity_N--;
        s->zone = 'N';
    }
    else if (arr[choice] == 'A')
    {
        capacity_A--;
        s->zone = 'A';
    }
    return seat;
}
void position_giver(long pos, char position[])
{
    if (pos == 1)
    {
        strcpy(position, "1st");
    }
    if (pos == 2)
    {
        strcpy(position, "2nd");
    }
    if (pos == 3)
    {
        strcpy(position, "3rd");
    }
    if (pos >= 4)
    {
        char str[100];
        sprintf(str, "%ld", pos);
        strcat(str, "th");
        strcpy(position, str);
    }
}
int watch_match(struct spectator *s, struct timespec got_seat)
{
    struct timespec now;
    struct timespec stop_watching;
    stop_watching.tv_sec = got_seat.tv_sec + Spectating_Time_X;

    //------------------------person type H-----------------------------
    //if team A had another goal this thread will be signaled to check or waits untill he watches the watch for the given amount of time
    if (s->person_type == 'H')
    {
        pthreadMutexLock(&A_goalLock);
        while (A_goals < s->goal_R)
        {
            pthread_cond_timedwait(&waitforGoal_A, &A_goalLock, &stop_watching);
            clock_gettime(CLOCK_REALTIME, &now);
            if (now.tv_sec >= stop_watching.tv_sec)
            {
                pthreadMutexUnlock(&A_goalLock);
                return 0;
            }
        }
        pthreadMutexUnlock(&A_goalLock);
    }
    //-------------------type A --------------------------------------
    //if team H had another goal this thread will be signaled to check or waits untill he watches the watch for the given amount of time
    else if (s->person_type == 'A')
    {
        pthreadMutexLock(&H_goalLock);
        while (H_goals < s->goal_R)
        {
            pthread_cond_timedwait(&waitforGoal_H, &H_goalLock, &stop_watching);
            clock_gettime(CLOCK_REALTIME, &now);
            if (now.tv_sec >= stop_watching.tv_sec)
            {
                pthreadMutexUnlock(&H_goalLock);
                return 0;
            }
        }
        pthreadMutexUnlock(&H_goalLock);
    }
    //-------------------type N-------------------------------------------
    //waits untill he watches the watch for the given amount of time
    //multiple cv with different mutexlocks allowed to stop watching multple threads at same time
    else if (s->person_type == 'N')
    {
        pthreadMutexLock(&personLocks[s->idx]);

        pthread_cond_timedwait(&timec[s->idx], &personLocks[s->idx], &stop_watching);

        pthreadMutexUnlock(&personLocks[s->idx]);
        return 0;
    }

    return 1;
}
void increase_seatcount(struct spectator *s)
{
    pthreadMutexLock(&seatlock);
    if (s->zone == 'H')
    {
        capacity_H++;
    }
    else if (s->zone == 'A')
    {
        capacity_A++;
    }
    else if (s->zone == 'N')
    {
        capacity_N++;
    }
    pthreadMutexUnlock(&seatlock);
    pthreadCondBroadcast(&waitforseat);
}
void *person_function(void *inp)
{
    struct spectator *s = ((struct spectator *)inp);
    struct timespec arrival;
    struct timespec patience;
    struct timespec got_seat;
    struct timespec now;
    int seat = 0;
    arrival.tv_sec = start.tv_sec + s->t;    //time till person donot arrive
    patience.tv_sec = arrival.tv_sec + s->p; //time till person waits for seat

    //----------------wait till person donot arrive (differrent person can buy tickets at the same time hence used different locks)---------------------
    //The effect of using more than one mutex for concurrent pthread_cond_wait() or pthread_cond_timedwait() operations on the same condition variable is undefined; that is, a condition variable becomes bound to a unique mutex when a thread waits on the condition variable, and this (dynamic) binding ends when the wait returns.
    //hence used different condition variables

    pthreadMutexLock(&personLocks[s->idx]); //FOR CONDITION
    pthread_cond_timedwait(&timec[s->idx], &personLocks[s->idx], &arrival);
    clock_gettime(CLOCK_REALTIME, &got_seat);
    printf(GREEN "%s has reached the stadium" RESET "\n", s->name);
    sleep(1); //takes 1 sec to buy ticket (used to confirm that differrent person can buy tickets at the same time)
    pthreadMutexUnlock(&personLocks[s->idx]);

    //-----------------get a seat -----------------------------------
    pthreadMutexLock(&seatlock); //FOR CONDITION(avoids race condition)
    seat = seats(s);
    while (seat == 0)
    {
        //check patiece is up or not
        clock_gettime(CLOCK_REALTIME, &now);
        if (now.tv_sec >= patience.tv_sec)
        {
            pthreadMutexUnlock(&seatlock);
            pthreadMutexLock(&groupLocks[s->group]);
            group[s->group]--;
            pthreadMutexUnlock(&groupLocks[s->group]);
            printf(CYAN "%s could not get a seat" RESET "\n", s->name);
            printf(MAGENTA "%s is waiting for his friends at the exit" RESET "\n", s->name);

            if (group[s->group] == 0)
            {
                printf(RED "Group %d has exited" RESET "\n", s->group + 1);
            }

            pthread_exit(0);
        }
        //waits on condition variable till patience time
        pthread_cond_timedwait(&waitforseat, &seatlock, &patience);
        seat = seats(s);
    }
    clock_gettime(CLOCK_REALTIME, &got_seat);
    printf(CYAN "%s has got a seat in zone %c" RESET "\n", s->name, s->zone);
    pthreadMutexUnlock(&seatlock);

    //----------------wait till person watches match-------------------------------------------
    int defended = 0;

    defended = watch_match(s, got_seat);

    if (defended)
    {
        printf(BRED "%s is leaving due to the bad defensive performance of his team" RESET "\n", s->name);
        increase_seatcount(s);
        sleep(1);
    }
    else
    {
        printf(BLUE "%s watched the match for %d seconds and is leaving" RESET "\n", s->name, Spectating_Time_X);
        increase_seatcount(s);
        sleep(1);
    }

    //person at the gate waiting for friend or the group exits
    pthreadMutexLock(&groupLocks[s->group]);
    group[s->group]--;
    pthreadMutexUnlock(&groupLocks[s->group]);

    printf(MAGENTA "%s is waiting for his friends at the exit" RESET "\n", s->name);
    if (group[s->group] == 0)
    {
        printf(RED "Group %d has exited for dinner" RESET "\n", s->group + 1);
    }

    pthread_exit(0);

} /* 
● Team converted the goal scoring chance to a goal
“Team t_i have scored their Yth goal”
● Team failed to convert the goal scoring chance to a goal
“Team t_i missed the chance to score their Yth goal”
 */
void *team_function(void *inp)
{
    struct chance *g = ((struct chance *)inp);
    struct timespec goal_time;
    goal_time.tv_sec = start.tv_sec + g->time;
    char which_goal[32];

    //You can assume that no 2 chances are created at the same instant i.e. the second token for each chance’s description would be distinct.
    //thus using single lock for all thread
    pthreadMutexLock(&goalLock); //FOR CONDITION
    pthread_cond_timedwait(&waitforgoal, &goalLock, &goal_time);
    pthreadMutexUnlock(&goalLock);
    if (rand() % 100 <= g->prob * 100)
    {
        if (g->team == 'A')
        {
            pthreadMutexLock(&A_goalLock);
            A_goals++;
            position_giver(A_goals, which_goal);
            printf("Team %c have scored their %s goal \n", g->team, which_goal);
            pthreadMutexUnlock(&A_goalLock);
            pthreadCondBroadcast(&waitforGoal_A);
        }
        else if (g->team == 'H')
        {
            pthreadMutexLock(&H_goalLock);
            H_goals++;
            position_giver(H_goals, which_goal);
            printf("Team %c have scored their %s goal\n", g->team, which_goal);
            pthreadMutexUnlock(&H_goalLock);
            pthreadCondBroadcast(&waitforGoal_H);
        }
    }
    else
    {
        if (g->team == 'A')
        {
            position_giver(A_goals + 1, which_goal);
            printf("Team %c missed the chance to score their %s goal \n", g->team, which_goal);
        }
        else if (g->team == 'H')
        {
            position_giver(H_goals + 1, which_goal);

            printf("Team %c missed the chance to score their %s goal\n", g->team, which_goal);
        }
    }
    pthread_exit(0);
}

int main()
{
    num_people = 0;

    //-------------------------inputs------------------------------------
    scanf("%d %d %d", &capacity_H, &capacity_A, &capacity_N);
    if (capacity_H <= 0 && capacity_A <= 0 && capacity_N <= 0)
    {
        printf("invalid capacity\n");
        return 0;
    }
    scanf("%d", &Spectating_Time_X);
    scanf("%d", &num_groups);

    for (int i = 0; i < num_groups; i++)
    {
        scanf("%d", &group[i]);
        pthreadMutexInit(&groupLocks[i]);
        for (int j = 0; j < group[i]; j++)
        {
            person[num_people] = (struct spectator *)(malloc(sizeof(struct spectator)));
            scanf("%s %c %d %d %d", person[num_people]->name, &person[num_people]->person_type, &person[num_people]->t, &person[num_people]->p, &person[num_people]->goal_R);
            person[num_people]->group = i;
            pthreadMutexInit(&personLocks[num_people]);
            pthread_cond_init(&timec[num_people], NULL);
            num_people++;
        }
    }

    int garbage;
    scanf("%d", &num_goals);

    for (int i = 0; i < num_goals; i++)
    {
        scanf("%d", &garbage);
        goals[i] = (struct chance *)(malloc(sizeof(struct chance)));
        scanf("%c %d %f", &goals[i]->team, &goals[i]->time, &goals[i]->prob);
    }

    //--------------------------------------------------------------------
    //start
    clock_gettime(CLOCK_REALTIME, &start);

    //Declaring the pthreads
    pthread_t person_thread[num_people];
    pthread_t goal_thread[num_goals];

    //create threads
    for (int i = 0; i < num_people; i++)
    {

        person[i]->idx = i;
        pthreadCreate(&person_thread[i], NULL, person_function, (void *)(person[i]));
    }

    for (int i = 0; i < num_goals; i++)
    {
        pthreadCreate(&goal_thread[i], NULL, team_function, (void *)(goals[i]));
    }

    //pthreadJoin
    for (int i = 0; i < num_people; i++)
    {
        pthreadJoin(person_thread[i], NULL);
        pthreadMutexDestroy(&personLocks[i]);
        pthreadCondDestroy(&timec[i]);
          free(person[i]);
    }
    for (int i = 0; i < num_goals; i++)
    {
        pthread_cancel(goal_thread[i]);
          free(goals[i]);

    }
    pthreadMutexDestroy(&seatlock); 
    pthreadMutexDestroy(&A_goalLock ); 
    pthreadMutexDestroy(&H_goalLock ); 
    pthreadMutexDestroy(&goalLock); 
    pthreadCondDestroy(&waitforGoal_A);
    pthreadCondDestroy(&waitforGoal_H);
    pthreadCondDestroy(&waitforseat);
    pthreadCondDestroy(&waitforgoal);

   
}
