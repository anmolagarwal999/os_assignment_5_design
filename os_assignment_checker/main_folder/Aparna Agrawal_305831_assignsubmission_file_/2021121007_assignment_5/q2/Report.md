## Globals
#define Max 100                 //for group 
#define person_max 1000         //person
#define goal_max 1000           //goals


int num_people;         //number of people comming to watch
int Spectating_Time_X;  //no one wants to watch the match for more than ‘X’ units (SPECTATING TIME) [X is an integer]. This value is fixed across all people.
int capacity_H;         // capacity of H zone
int capacity_N;         // capacity of N zone
int capacity_A;         // capacity of A zone
int num_groups;         //number of groups 
int num_goals;      //NUMBER OF GOAL SCORING CHANCES ‘G’
struct timespec start; //time when simulation started


int group[Max];
struct spectator *person[person_max];
struct chance *goals[goal_max];

## Entities
struct spectator{
    int idx;        
    char name[64];  // name of person
    int t;          //each person enters the stadium after a time ‘T’ 
    int p;          //PATIENCE VALUE P
    char person_type;//H/N/A
    int group;      // in which group the person is
    int goal_R;     //get_defensive_goal
    char zone;      //set after he gets the seat
};

struct chance{
char team;      //Team with the chance 
int time;       //Time in sec elapsed since the beginning of the match afterwhich the chance was created
 float prob;       //Probability that the chance actually results in a goa
};


## Mutex
wait till person donot arrive (differrent person can buy tickets at the same time,***hence used different locks so that each person can buy ticket without blocking other )---------------------
pthread_mutex_t personLocks[person_max];                    
pthread_mutex_t groupLocks[Max];    //each group has a lock so different person from different group can update at the same time
pthread_mutex_t seatlock = PTHREAD_MUTEX_INITIALIZER;   //used to syncronize seat allocation
pthread_mutex_t A_goalLock = PTHREAD_MUTEX_INITIALIZER; //used to syncronize goals of A,used with cv  waitforGoal_A for person to wait till he thinks his team is playing  defensive
pthread_mutex_t H_goalLock = PTHREAD_MUTEX_INITIALIZER; //used to syncronize goals of A,used with cv  waitforGoal_H for person to wait till he gets  he thinks his team is playing  defensive
pthread_mutex_t goalLock = PTHREAD_MUTEX_INITIALIZER; //used with cv to  wait for goal chance


## cV
(//The effect of using more than one mutex for concurrent pthread_cond_wait() or pthread_cond_timedwait() operations on the same condition variable is undefined; that is, a condition variable becomes bound to a unique mutex when a thread waits on the condition variable, and this (dynamic) binding ends when the wait returns.)
//hence used different condition variables with personLocks[person_max]
pthread_cond_t timec[person_max];
pthread_cond_t waitforGoal_A = PTHREAD_COND_INITIALIZER; //opposite team fan wait on this ,if opposite team is playing better  he thinks his team is playing  defensive
pthread_cond_t waitforGoal_H = PTHREAD_COND_INITIALIZER;    //same
pthread_cond_t waitforseat = PTHREAD_COND_INITIALIZER;   //wait for seat to get emty in the prefered zone of person
pthread_cond_t waitforgoal = PTHREAD_COND_INITIALIZER;    // wait till the chances of some goal arrives


## Logic
### person
wait till person donot arrive (differrent person can buy tickets at the same time hence used different locks)---------------------
    - The effect of using more than one mutex for concurrent pthread_cond_wait() or pthread_cond_timedwait() operations on the same condition variable is undefined; that is, a condition variable becomes bound to a unique mutex when a thread waits on the condition variable, and this (dynamic) binding ends when the wait returns.hence used different condition variables
    - waits till arrival.tv_sec = start.tv_sec + s->t;    //time till person donot arrive

wait for seat till patience is up
 - patience.tv_sec = arrival.tv_sec + s->p; //time till person waits for seat
 - pthread_cond_timedwait(&waitforseat, &seatlock, &patience); wait till he got the seat, whenever a seat is incremented that is someone left all the threats waiting on waitforseat is notified and the condition is rechecked,if the patience is up the person will leave

wait till person watches match
for person  of type A
 - pthread_cond_timedwait(&waitforGoal_A, &A_goalLock, &stop_watching);
   thread waits on waitforGoal_A till he finds out that his team is playing bad defensive or his watching time is up, every time team A goals all threads waiting on waitforGoal is notifed
 for person  of type H
 - pthread_cond_timedwait(&waitforGoal_H, &H_goalLock, &stop_watching);
  thread waits on waitforGoal_H till  he finds out that his team is playing bad defensive  or his watching time is up, every time team H goals all threads waiting on waitforGoal is notifed
 for person  of type N
 - pthread_cond_timedwait(&timec[s->idx], &personLocks[s->idx], &stop_watching);
  thread waits on waitforGoal_A till his watching time is up.

  each person of type N can leave the stand at the same time,person of type A or H or N can leave the stand at a time.beacause of same lock no two person of same type A or H can leave at the same time.

when the person exit
group[]
group count of respective group is decremented under the groupLock[] and the person waits for friend outside
if the group count reaches zero the group exits

### goals
each chance of goals wait till the time the chance of goal arrives
for that waitforGoal_A waitforGoal_H cv is used and  A_goalLock ,H_goalLock mutex
using rand() % 100 <= g->prob * 100 , decides if the respective team goals or not and increment accordingly.
if incremented notify  person thread wating on waitforgoals_X to check if there team is playaing good defensive or not  or not
and exit