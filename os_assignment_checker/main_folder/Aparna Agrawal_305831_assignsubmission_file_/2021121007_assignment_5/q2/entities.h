#ifndef ENTITIES_H /* This is an "include guard" */
#define ENTITIES_H
#define Max 100
#define person_max 1000
#define goal_max 1000

#define RED "\x1b[31m"
#define BRED "\x1b[1;31m"
#define GREEN "\x1b[1;32m"
#define YELLOW "\x1b[1;33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int num_people;         //number of people comming to watch
int Spectating_Time_X;  //no one wants to watch the match for more than ‘X’ units (SPECTATING TIME) [X is an integer]. This value is fixed across all people.
int capacity_H;         // capacity of H zone
int capacity_N;         // capacity of N zone
int capacity_A;         // capacity of A zone
int num_groups;         //number of groups 
int num_goals;      //NUMBER OF GOAL SCORING CHANCES ‘G’
struct timespec start; //time when simulation started

int group[Max];
struct spectator *person[person_max];
struct chance *goals[goal_max];

struct spectator{
    int idx;        
    char name[64];  // name of person
    int t;          //each person enters the stadium after a time ‘T’ 
    int p;          //PATIENCE VALUE P
    char person_type;//H/N/A
    int group;      // in which group the person is
    int goal_R;     //get_defensive_goal
    char zone;      //set after he gets the seat
};

struct chance{
char team;      //Team with the chance 
int time;       //Time in sec elapsed since the beginning of the match afterwhich the chance was created
 float prob;       //Probability that the chance actually results in a goa
};

//functions
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void pthreadJoin(pthread_t thread, void **retval);
void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *mutex);
void pthreadMutexLock(pthread_mutex_t *mutex);
void pthreadMutexUnlock(pthread_mutex_t *mutex);
void pthreadCondBroadcast(pthread_cond_t *cv);
void pthreadMutexDestroy(pthread_mutex_t *m);
void pthreadCondDestroy(pthread_cond_t *cv);
void pthreadMutexInit(pthread_mutex_t *m);

#endif
