#include<stdio.h>
#include<unistd.h>
#include<assert.h>
#include<pthread.h>
#include<stdlib.h>
#include<stdbool.h>
#include<semaphore.h>

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

int randNum(long int lower, long int upper) {
    return (int) (random() % (upper - lower + 1) + lower);
}

int min(int a, int b) {
    return (a < b) ? a : b;
}

void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr,
                   void *(*start_routine)(void *), void *arg) {
    if (pthread_create(thread, attr,
                       start_routine, arg) != 0) {
        perror("Thread was not created");
    }
}

void pthreadJoin(pthread_t thread, void **retval) {
    if (pthread_join(thread, retval) != 0) {
        perror("Thread could not join");
    }

}

void condWait(pthread_cond_t *cv, pthread_mutex_t *mutex) {
    if (pthread_cond_wait(cv, mutex) != 0) {
        perror("Cant cond wait on cv");
    }
}

void mutexLock(pthread_mutex_t *mutex) {
    fflush(stdout);
    if (pthread_mutex_lock(mutex) != 0) {
        perror("Cant lock mutex");
    }
}

void mutexUnlock(pthread_mutex_t *mutex) {
    fflush(stdout);
    if (pthread_mutex_unlock(mutex) != 0) {
        perror("Cant lock mutex");
    }
}

void condBroadcast(pthread_cond_t *cv) {
    if (pthread_cond_broadcast(cv) != 0) {
        perror("Cant broadcast");
    }
}

#define MAX_TA 100
#define MAX_STU 1000
#define MAX_LAB 30
#define MAX_COURSE 100

int num_students=0,num_labs=0,num_courses=0;


struct lab {
    char name[10];
    int num_ta;
    int ta[MAX_TA];
    int vac[MAX_TA];
    int limit;
    int flag;
};

struct course {
    int id;
    char name[10];
    float interest;
    int num_max_slots;
    int num_labs;
    int L[MAX_LAB];
    int exists;
};

struct student {
    int id;
    float calibre;
    int pref1;
    int pref2;
    int pref3;
    int time;
    int pref;
};

pthread_t p_stu[MAX_STU];
pthread_t p_courses[MAX_COURSE];
pthread_t p_labs[MAX_LAB];

struct student *stu;
struct course *courses;
struct lab *labss;


void *p_course(void *ptr)
{
    struct course *temp = ptr;

    int cnt=0;

    for(int i=0;i<num_students;i++)
    {
        if(stu[i].pref1 == temp->id && stu[i].pref == 1)
        {
           cnt++; 
        }

        if(stu[i].pref2 == temp->id && stu[i].pref == 2)
        {
           cnt++; 
        }

        if(stu[i].pref3 == temp->id && stu[i].pref == 3)
        {
           cnt++; 
        }
    }

    if(cnt)
    {
        printf("Course %s has been allocated %d seats\n",temp->name,cnt);
    }


    int t = cnt;

    /*while(1)
    {

    }*/
    //while(t)
    //{
        for(int i=0;i<temp->num_labs;i++)
        {
            struct lab *temp1 = &labss[temp->L[i]];
            if(temp1->flag)
            {
                
                int num;
           
                for(int j=0;j<temp1->num_ta;i++)
                {
                    if(temp1->vac[j])
                    {
                        if(temp1->ta[j] < temp1->limit)
                        {
                            temp1->vac[j] = 0;
                            temp1->ta[j]++;
                            num = j;
                            printf("TA %d from lab %s has been allocated to course %s for %d TA ship\n",j,temp1->name,temp->name,temp1->ta[j]);
                            break;
                        }
                    }
                //lab_check() function
                }

                int thr = rand()%temp->num_max_slots + 1;
                if(thr > cnt)
                {
                    thr = cnt;
                }

                printf("Tutorial has started for Course %s with %d seats filled out of %d\n",temp->name,thr,cnt);

                t = t - thr;

                if(!t)
                {
                    break;
                }

                printf("TA %d from lab %s has completed the tutorial and left the course %s\n",num,temp1->name,temp->name);
            }
        }
    //}
        if(!t)
        {
            courses[temp->id].exists = 0;
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n",temp->name);
        }

}

void *p_student(void *ptr)
{
    struct student *temp = (struct student*)ptr;
    sleep(temp->time);

    stu[temp->id].pref = 1;

    printf("Student %d has filled in preferences for course registration\n",temp->id);



    printf("Student %d has been allocated a seat in course registration %s\n",temp->id,courses[temp->pref1].name);

    //function

    float prob,temp1,thres;

    if(courses[temp->pref1].exists)
    {
        prob = (temp->calibre)*(courses[temp->pref1].interest);
        temp1 = rand()%5 + 3;
        thres = (1/temp1);

        if(prob >= thres)
        {
            printf("Student %d has selected the course %s permanently\n",temp->id,courses[temp->pref1].name);
            pthread_exit(0);
        }
    }

    stu[temp->id].pref1 = -1;

    printf("Student %d has withdrawn from the course %s\n",temp->id,courses[temp->pref1].name);

    stu[temp->id].pref = 2;    
    
    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n",temp->id,courses[temp->pref1].name,courses[temp->pref2].name);

    if(courses[temp->pref2].exists)
    {
        prob = (temp->calibre)*(courses[temp->pref2].interest);
        temp1 = rand()%5 + 3;
        thres = (1/temp1);

        if(prob >= thres)
        {
            printf("Student %d has selected the course %s permanently\n",temp->id,courses[temp->pref2].name);
            pthread_exit(0);
        }
    }

    stu[temp->id].pref2 = -1;

    printf("Student %d has withdrawn from the course %s\n",temp->id,courses[temp->pref2].name);  

    stu[temp->id].pref = 3;

    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n",temp->id,courses[temp->pref2].name,courses[temp->pref3].name); 

    if(courses[temp->pref3].exists)
    {
        prob = (temp->calibre)*(courses[temp->pref3].interest);
        temp1 = rand()%5 + 3;
        thres = (1/temp1);

        if(prob >= thres)
        {
            printf("Student %d has selected the course %s permanently\n",temp->id,courses[temp->pref3].name);
            pthread_exit(0);
        }
    }

    stu[temp->id].pref3 = -1;

    printf("Student %d has withdrawn from the course %s\n",temp->id,courses[temp->pref3].name);

    printf("Student %d couldn’t get any of his preferred courses\n",temp->id);


}



int main(void)
{
    srand(time(0));

    scanf("%d %d %d",&num_students,&num_labs,&num_courses);

    if(num_courses==0) 
    {
       printf("No Courses\n");
       return 0;
    } 

    if(num_labs==0) 
    {
       printf("No Labs\n");
       return 0;
    } 

    if(num_students==0) 
    {
       printf("No Students\n");
       return 0;
    }


    courses = (struct course*)malloc(sizeof(struct course)*num_courses);

    for(int i=0;i<num_courses;i++)
    {
        courses[i].id = i;
        courses[i].exists = 1;
        scanf("%s",courses[i].name);
        scanf("%f",&courses[i].interest);
        scanf("%d",&courses[i].num_max_slots);
        scanf("%d",&courses[i].num_labs);
        
        for(int j=0;j<courses[i].num_labs;j++)
        {
            scanf("%d",&courses[i].L[j]);
        }

        //pthreadCreate(&p_courses[i],NULL,&p_course,&courses[i]);
    }

    stu = (struct student*)malloc(sizeof(struct student)*num_students);

    for(int i=0;i<num_students;i++)
    {
        stu[i].id = i;
        scanf("%f",&stu[i].calibre);
        scanf("%d",&stu[i].pref1);
        scanf("%d",&stu[i].pref2);
        scanf("%d",&stu[i].pref3);
        scanf("%d",&stu[i].time);
    }


    labss = (struct lab*)malloc(sizeof(struct lab)*num_labs);

    for(int i=0;i<num_labs;i++)
    {
        labss[i].flag = 1;
        scanf("%s",labss[i].name);
        scanf("%d",&labss[i].num_ta);
        scanf("%d",&labss[i].limit);
        
        for(int j=0;j<labss[i].num_ta;j++)
        {
            labss[i].ta[j] = 0;
            labss[i].vac[j] = 1;
        }
        
        //pthreadCreate(&p_stu[i],NULL,&p_student,stu[i]);
    }      
    
    for(int i=0;i<num_students;i++)
    {
        pthreadCreate(&p_stu[i],NULL,&p_student,&stu[i]);
    }

    for(int i=0;i<num_courses;i++)
    {
        pthreadCreate(&p_courses[i],NULL,&p_course,&courses[i]);
    }    

    for(int i=0;i<num_students;i++)
    {
        pthread_join(p_stu[i],NULL);
    }
    
}