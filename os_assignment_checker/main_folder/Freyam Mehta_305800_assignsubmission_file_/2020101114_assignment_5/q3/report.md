# Multithreaded client and server

```
make server
make client
```

## Multithreaded Server

Multi-threaded server is a server that can handle multiple clients at the same time. I implemented it using pthreads.

I am using the following structure for the Server node.

```cpp
struct Dictionary {
    string str;            // command
    int ID;                // ID
    bool active;           // is_active
    pthread_mutex_t mutex; // lock
};
```

### `server.c` workflow

1. The number of worker threads must first be accepted.
2. The dictionary nodes are subsequently initialized and these worker threads are created. (`vector<Dictionary> dict(DICT_SIZE);`)
3. The server is then setup and started.
4. We await the client's answer before adding it to a queue. (`queue<int *> q;`)

`void *THREAD_WORKER()` is the main worker thread handler which keeps handling requests while the queue is not empty.

`void COMMAND_HANDLER(string s, int client_socket_fd)` is the handler for the client's command.

1. To return the command supplied to us the client, we utilize CLIENT_HANDLER().
2. The dictionary is then used to perform the required action.
3. Then, to send a message back to the client, we use `SERVER_LOG()`.

The END. We keep repeating these steps till the queue is empty.

### `client.c` workflow

1. The inputs were read.
2. Then, for each client request, we use `HANDLE_CLIENTS()` to handle the requests. We initiate a new thread for each request.

```cpp
for (int i = 0; i < N_CLIENTS; ++i) {
    clients_requests[i].mutex = PTHREAD_MUTEX_INITIALIZER; // initialize mutex
    clients_requests[i].i = i;
}

for (int i = 0; i < N_CLIENTS; ++i)
    pthread_create(&clients_requests[i].tid, NULL, CLIENT_WORKER, &clients_requests[i].i); // create threads

for (int i = 0; i < N_CLIENTS; ++i)
    pthread_join(clients_requests[i].tid, NULL); // join threads

```

3. To connect to a server, we must first create a connection and obtain the socket file desciptor. (`void INIT_FD()`)

```cpp
void INIT_FD() {
    sockaddr_in server_obj;
    int SOCKET_FD = socket(AF_INET, SOCK_STREAM, 0); // create socket

    if (SOCKET_FD < 0) {
        perror("socket");
        exit(1);
    }

    memset(&server_obj, 0, sizeof(server_obj)); // clear server_obj
    server_obj.sin_family = AF_INET;            // set family
    server_obj.sin_port = htons(PORT);          // set port

    if (connect(SOCKET_FD, (sockaddr *)&server_obj, sizeof(server_obj)) < 0) { // connect to server
        perror("connect");
        exit(1);
    }

    CLIENT_FD = SOCKET_FD; // set client_fd
    return;
}
```

4. sleep for sometime to simulate the delay of the server.

```cpp
sleep(req_time); // sleep for required time
```

5. We then use `write(client_fd, command.c_str(), command.length())` to send this command to the socket.
6. We then use `read(client_fd, &result[0], BUFFER_SIZE - 1)` to receive the response, which we then output to the terminal appropriately.

```cpp
pthread_mutex_lock(&clients_requests[index].mutex); // lock mutex

    int x = write(client_fd, command.c_str(), command.length()); // write command to client
    if (x < 0) {
        cerr << "Error writing to socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_unlock(&clients_requests[index].mutex); // unlock mutex
    pthread_mutex_lock(&clients_requests[index].mutex);   // lock mutex

    string result(BUFFER_SIZE, '\0');
    int byte_read = read(client_fd, &result[0], BUFFER_SIZE - 1);
    result[byte_read] = '\0';
    result.resize(byte_read);

    if (byte_read <= 0) {
        cerr << "Error reading from socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_lock(&terminal); // lock terminal

    cout << clients_requests[index].i << ":" << gettid() << ":" << result << endl; // log

    pthread_mutex_unlock(&terminal); // unlock terminal
```

The END. We close the client and keep the server running.
