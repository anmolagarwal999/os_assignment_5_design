#include <arpa/inet.h>
#include <assert.h>
#include <bits/stdc++.h>
#include <fcntl.h>
#include <iostream>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <tuple>
#include <unistd.h>

using namespace std;

#define PORT 8001           // port number
#define BUFFER_SIZE 1048576 // 1MB

typedef struct REQUEST {
    int i;
    pthread_t tid;
    int client_fd;
    int req_time;
    string command;
    pthread_mutex_t mutex;
} REQUEST;

vector<REQUEST> clients_requests;                     // vector of requests
pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER; // mutex for terminal

void HANDLE_CLIENTS(int N_CLIENTS); // function to handle clients
void INIT_FD();
void *CLIENT_WORKER(void *(arg)); // function to handle clients

int CLIENT_FD;

int main() {
    int N_CLIENTS;
    cin >> N_CLIENTS;

    string buf;
    getline(cin, buf);

    HANDLE_CLIENTS(N_CLIENTS); // handle clients
    return 0;
}

void HANDLE_CLIENTS(int N_CLIENTS) {
    for (int i = 0; i < N_CLIENTS; ++i) {
        string str;
        getline(cin, str);

        string TIME_REQUIRED = "";
        auto itr = str.begin();

        while (itr != str.end()) {
            if (*itr == ' ')
                break;

            TIME_REQUIRED += *itr;
            itr++;
        }

        itr++;
        string command(itr, str.end());

        REQUEST tmp;
        tmp.req_time = stoi(TIME_REQUIRED); // time required
        tmp.command = command;              // command
        clients_requests.emplace_back(tmp); // add to vector
    }

    for (int i = 0; i < N_CLIENTS; ++i) {
        clients_requests[i].mutex = PTHREAD_MUTEX_INITIALIZER; // initialize mutex
        clients_requests[i].i = i;
    }

    for (int i = 0; i < N_CLIENTS; ++i)
        pthread_create(&clients_requests[i].tid, NULL, CLIENT_WORKER, &clients_requests[i].i); // create threads

    for (int i = 0; i < N_CLIENTS; ++i)
        pthread_join(clients_requests[i].tid, NULL); // join threads

    exit(0);
}

void INIT_FD() {
    sockaddr_in server_obj;
    int SOCKET_FD = socket(AF_INET, SOCK_STREAM, 0); // create socket

    if (SOCKET_FD < 0) {
        perror("socket");
        exit(1);
    }

    memset(&server_obj, 0, sizeof(server_obj)); // clear server_obj
    server_obj.sin_family = AF_INET;            // set family
    server_obj.sin_port = htons(PORT);          // set port

    if (connect(SOCKET_FD, (sockaddr *)&server_obj, sizeof(server_obj)) < 0) { // connect to server
        perror("connect");
        exit(1);
    }

    CLIENT_FD = SOCKET_FD; // set client_fd
    return;
}

void *CLIENT_WORKER(void *(arg)) {
    int index = *(int *)arg;

    // clients_requests[index].client_fd = CLIENT_FD;
    INIT_FD();
    clients_requests[index].client_fd = CLIENT_FD; // set client_fd

    string command = clients_requests[index].command;
    int req_time = clients_requests[index].req_time;
    int client_fd = clients_requests[index].client_fd;

    sleep(req_time); // sleep for required time

    pthread_mutex_lock(&clients_requests[index].mutex); // lock mutex

    int x = write(client_fd, command.c_str(), command.length()); // write command to client
    if (x < 0) {
        cerr << "Error writing to socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_unlock(&clients_requests[index].mutex); // unlock mutex
    pthread_mutex_lock(&clients_requests[index].mutex);   // lock mutex

    string result(BUFFER_SIZE, '\0');
    int byte_read = read(client_fd, &result[0], BUFFER_SIZE - 1);
    result[byte_read] = '\0';
    result.resize(byte_read);

    if (byte_read <= 0) {
        cerr << "Error reading from socket" << endl;
        pthread_mutex_unlock(&clients_requests[index].mutex);
        return NULL;
    }

    pthread_mutex_lock(&terminal); // lock terminal

    cout << clients_requests[index].i << ":" << gettid() << ":" << result << endl; // log

    pthread_mutex_unlock(&terminal); // unlock terminal

    pthread_mutex_unlock(&clients_requests[index].mutex); // unlock mutex

    return NULL;
}
