#include "headers.h"

void *student_handler(void *arg) {
    student *stud = (student *)arg;

    clock_t diff;
    do {
        diff = clock() - time_0;
        diff /= 1000000;         // CLOCKS_PER_SEC = 1000000
    } while (diff < stud->time); // wait for the time to arrive (sleeeeeeeeeep)

    stud->registered = true;
    printf(RED "Student %d has filled in preferences for course registration\n" RESET, stud->ID);

    while (1) {
        pthread_mutex_lock(student_mutex + stud->ID); // lock student mutex

        bool flag_f = courses[stud->pref[stud->curr_pref]].is_removed; // check if the course is removed
        while (stud->attended == false) {                              // check if the student has attended the course
            if (flag_f == true)
                break;

            pthread_cond_wait(allocated + stud->ID, student_mutex + stud->ID); // wait for the course to be allocated
        }

        if (stud->curr_pref > 2) {
            stud->done = true;
            printf(CYAN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->ID); // if the student couldn’t get any of his preferred courses

            pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
            break;
        }

        while (flag_f) {
            // magic happens here
        }

        pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
    }

    return NULL;
}
