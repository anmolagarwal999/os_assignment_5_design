#ifndef __MAIN__H__
#define __MAIN__H__

#define BLUE "\x1B[34m"
#define CYAN "\x1B[36m"
#define GREEN "\x1B[32m"
#define RED "\x1B[31m"
#define RESET "\x1B[0m"
#define YELLOW "\x1B[33m"

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

typedef struct student {
    bool attended;        // true if student attended a lab
    bool done;            // true if student is done
    bool registered;      // true if registered for a course
    float calibre;        // student's calibre
    int course_allocated; // course allocated to student
    int curr_pref;        // current preference
    int ID;               // student ID
    int pref[3];          // student's preferences
    int time;             // time student attended lab
} student;

typedef struct course {
    bool is_removed; // true if course is is_removed
    char name[75];   // course name
    float interest;  // course interest
    int *LAB_IDS;    // labs for course
    int ID;          // course ID
    int max_slots;   // max slots for course
    int N_labs;      // total labs for course
} course;

typedef struct lab {
    bool flag;               // true if lab is full
    char name[75];           // lab name
    int *lab_TAs;            // lab TAs
    int CAP;                 // lab CAP
    int ID;                  // lab ID
    int max_limit;           // max limit for lab
    pthread_mutex_t *lk_TAs; // lock for lab TAs
} lab;

#define MX_COURSES 500 // max courses
#define MX_LABS 500    // max labs
#define MX_STUDS 500   // max students

// global variables

clock_t time_0;                     // time time_0 lab starts
pthread_cond_t allocated[MX_STUDS]; // condition for student to attend lab

course courses[MX_COURSES];               // course list
pthread_t courseT[MX_COURSES];            // course thread
pthread_mutex_t course_mutex[MX_COURSES]; // course mutex

lab labs[MX_LABS]; // lab list
// pthread_t labT[MX_LABS];            // lab thread
// pthread_mutex_t lab_mutex[MX_LABS]; // lab mutex

student students[MX_STUDS];              // student list
pthread_t studentT[MX_STUDS];            // student thread
pthread_mutex_t student_mutex[MX_STUDS]; // student mutex

void students_input(); // get students details
void courses_input();  // get courses details
void labs_input();     // get labs details

void *course_handler(void *arg);  // course handler
void *handle_labs(void *arg);     // lab handler
void *student_handler(void *arg); // student handler

#endif