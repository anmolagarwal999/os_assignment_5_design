# An alternate course allocation Portal

```
make
```

## Course Allocation Portal

An alternate course allocation portal for IIIT Hyderabad. This is a prototype of a course allocation portal.
I have created it using pthreads (C language) to take care of the concurrency issues.

We will be taking care of Students, Courses and Labs.

-   Courses: There are a variety of courses available.
-   Labs: Various labs across the campus (each consisting of a set of student mentors)
-   Students: Participants in the course registration.

I am using the following structure for the Student node.

```cpp
typedef struct student {
    bool attended;        // true if student attended a lab
    bool done;            // true if student is done
    bool registered;      // true if registered for a course
    float calibre;        // student's calibre
    int course_allocated; // course allocated to student
    int curr_pref;        // current preference
    int ID;               // student ID
    int pref[3];          // student's preferences
    int time;             // time student attended lab
} student;
```

I am using the following structure for the Course node.

```cpp
typedef struct course {
    bool is_removed; // true if course is is_removed
    char name[75];   // course name
    float interest;  // course interest
    int *LAB_IDS;    // labs for course
    int ID;          // course ID
    int max_slots;   // max slots for course
    int N_labs;      // total labs for course
} course;
```

I am using the following structure for the Lab node.

```cpp
typedef struct lab {
    bool flag;               // true if lab is full
    char name[75];           // lab name
    int *lab_TAs;            // lab TAs
    int CAP;                 // lab CAP
    int ID;                  // lab ID
    int max_limit;           // max limit for lab
    pthread_mutex_t *lk_TAs; // lock for lab TAs
} lab;
```

This is a partial implementation of the course allocation portal.
So, the functions only work partially. The inputs work and so do the preliminary MUTEXes and Semaphores.

# `main.c` workflow

```cpp
int main() {
    scanf("%d %d %d", &N_STUDENTS, &N_LABS, &N_COURSES);

    courses_input();
    students_input();
    labs_input();

    simulation();

    return 0;
}
```

It's only responsible for taking input and creating threads for each process.

Here, the global variables are used.

```cpp
#define MX_COURSES 500 // max courses
#define MX_LABS 500 // max labs
#define MX_STUDS 500 // max students
```

```cpp
void simulation() {
    srand(time(0));

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_create(&studentT[i], NULL, student_handler, students + i); // student thread

    for (int i = 0; i < N_COURSES; ++i)
        pthread_create(&courseT[i], NULL, course_handler, courses + i); // course thread

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_join(studentT[i], NULL); // wait for student thread to finish

    for (int i = 0; i < N_COURSES; ++i)
        pthread_join(courseT[i], NULL); // wait for course thread to finish

    for (int i = 0; i < N_STUDENTS; ++i) {
        pthread_mutex_destroy(student_mutex + i); // destroy student mutex
        pthread_cond_destroy(allocated + i);      // destroy allocated condition
    }

    for (int i = 0; i < N_COURSES; ++i)
        pthread_mutex_destroy(course_mutex + i); // destroy course mutex
}
```

The simulation function is responsible for creating threads for each process and waiting for them to finish before destroying the mutexes and condition variables.

## `student.c` workflow

```cpp
void *student_handler(void *arg) {
    student *stud = (student *)arg;

    clock_t diff;
    do {
        diff = clock() - time_0;
        diff /= 1000000;         // CLOCKS_PER_SEC = 1000000
    } while (diff < stud->time); // wait for the time to arrive (sleeeeeeeeeep)

    stud->registered = true;
    printf(RED "Student %d has filled in preferences for course registration\n" RESET, stud->ID);

    while (1) {
        pthread_mutex_lock(student_mutex + stud->ID); // lock student mutex

        bool flag_f = courses[stud->pref[stud->curr_pref]].is_removed; // check if the course is removed
        while (stud->attended == false) {                              // check if the student has attended the course
            if (flag_f == true)
                break;

            pthread_cond_wait(allocated + stud->ID, student_mutex + stud->ID); // wait for the course to be allocated
        }

        if (stud->curr_pref > 2) {
            stud->done = true;
            printf(CYAN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->ID); // if the student couldn’t get any of his preferred courses

            pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
            break;
        }

        while (flag_f) {
            // magic happens here
        }

        pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
    }

    return NULL;
}
```

The code works int eh following way.

1. The student thread is created.
2. Then, it waits for the time to arrive.
3. Then, it registers for the course.
4. Then, it waits for the course to be allocated.
5. Then, it checks if the course is removed.
6. Then, it checks if the student has attended the course.
7. Then, it checks if the student could get his preferred courses.

The assigning of the course is done in the following way.

--INCOMPLETE--

## `course.c` workflow

```cpp
void *course_handler(void *arg) {
    course *c = (course *)arg;
    while (1) {
        // magic happens here
    }
}
```

The code works in the following way.

1. The course thread is created.

--INCOMPLETE--
