#include "headers.h"

int N_STUDENTS; // number of students
int N_LABS;     // number of labs
int N_COURSES;  // number of courses

void courses_input(void) {
    int length = N_COURSES;
    for (int i = 0; i < length; ++i) {
        courses[i].ID = i;

        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].max_slots, &courses[i].N_labs);
        for (int j = 0; j < courses[i].N_labs; j++)
            scanf("%d", &courses[i].LAB_IDS[j]);

        courses[i].is_removed = false;
        courses[i].LAB_IDS = (int *)malloc(sizeof(int) * (courses[i].N_labs));

        pthread_mutex_init(course_mutex + i, NULL);
    }
}

void students_input() {
    int length = N_STUDENTS;
    for (int i = 0; i < length; ++i) {
        students[i].ID = i;
        scanf("%f", &students[i].calibre);

        students[i].attended = false;
        students[i].registered = false;

        scanf("%d %d %d", &students[i].pref[0], &students[i].pref[1], &students[i].pref[2]);

        students[i].course_allocated = -1;

        scanf("%d", &students[i].time);

        pthread_cond_init(allocated + i, NULL);
        pthread_mutex_init(student_mutex + i, NULL);
    }
}

void labs_input() {
    int length = N_LABS;
    for (int i = 0; i < length; ++i) {
        labs[i].ID = i;
        labs[i].lk_TAs = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * labs[i].CAP);
        labs[i].flag = true;
        labs[i].lab_TAs = (int *)malloc(sizeof(int) * labs[i].CAP);

        scanf("%s %d %d", labs[i].name, &labs[i].CAP, &labs[i].max_limit);
        for (int j = 0; j < labs[i].CAP; j++) {
            pthread_mutex_init(labs[i].lk_TAs + j, NULL);
            labs[i].lab_TAs[j] = 0;
        }
    }
}

void simulation() {
    srand(time(0));

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_create(&studentT[i], NULL, student_handler, students + i); // student thread

    for (int i = 0; i < N_COURSES; ++i)
        pthread_create(&courseT[i], NULL, course_handler, courses + i); // course thread

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_join(studentT[i], NULL); // wait for student thread to finish

    for (int i = 0; i < N_COURSES; ++i)
        pthread_join(courseT[i], NULL); // wait for course thread to finish

    for (int i = 0; i < N_STUDENTS; ++i) {
        pthread_mutex_destroy(student_mutex + i); // destroy student mutex
        pthread_cond_destroy(allocated + i);      // destroy allocated condition
    }

    for (int i = 0; i < N_COURSES; ++i)
        pthread_mutex_destroy(course_mutex + i); // destroy course mutex
}

int main() {
    scanf("%d %d %d", &N_STUDENTS, &N_LABS, &N_COURSES);

    courses_input();
    students_input();
    labs_input();

    simulation();

    return 0;
}
