## Client
### Main
* It reads input does the initialization.
* It maintains clock. The value of the clock is stored in ```int time;```
* A thread is created for each user request.
```
while(NotAllRequestsMade)
{
  Update time;
  Create threads, for every request whose time = current time.
}
```
* It then waits for all client thread to terminate. 
* It terminates itself.

### Threads Client
```
Astablish connection with server;
Send User Request in form of string;
Revice Response;
Print Response;
```
### Synchronization
* The Client threads are created to simulate independent user requests from different machines. 
* The client threads do not share data with other threads.
* So they do not have any synchronization criteria. 
* We must only ensure only one thread is printing at a time. This is done by ```pthread_mutex_t printLock```





---
<br>

## Server
### Main thread
* It spawns ```m``` worker threads.
* It starts listening to connection request form port ```9000```.
* It then does the following
```c
while(1)
{
 It then esablishes a conection with client threads (client machine);
 It schedules a worker to thread to handle / serve that connetcion;
}
```

### Worker thread
```c
while(1)
{
  wait for main thread to schedule;
  Process the request sent by client;
  Notify main that it has completed its job and waiting for next one.
}
```
  

### Synchronization
#### booking data used for scheduling requests.
```c
//Booking keeping

/*
  This is an array of lenght number of wokers.
  Each worker thread is given an Id. This id is the index. 
  Free[id] that tells whether the worker thread with id is working on a reqest or not.
*/
bool *Free;

/*
  It stores the number of Worker threads which are free now.
*/
int NoWorkerFree;
```
* The main thread conditionally waits for the condition ```NoWorkerFree > 0``` to occur. Once the condition happens. It schedules a worker thread to execute a user request, decrements *NoWorkerFree* by 1 and again waits on the same condition.
  * Conditional variable used is  ```Available```
  * The mutex lock for this is ```Available_mutex```
  * Each worker thread increment the on completion of a request increment the value of *NoWorkerFree* by one and signals the conditional variable *Available*.
* Each worker thread with Id ```x``` waits for the condition ```Free[x] == false``` to happen. Once the condition occurs the worker thread ```x``` process the request and sends response. Then the worker thread modifies  ```Free[x] = true``` and conditionally waits on the same condition again.
  * Conditional variable used is  ```Workers[x]```
  * The mutex lock for this is ```Workers_mut[x]```
  * The main thread selects a thread for scheduling when it selects thread ```x``` then it modifies value of ```Free[x]``` and signals the conditional variable ```Workers[x]```
* The mutex ```Workers_mut[x]``` is also used when we modify ```Free[x]``` to ensure only either worker ```x``` or main is reading/writing ```Free[x]```
#### The dictionary shared data
* The keys of the dictionary are integers. (0..100)
* We create a lock for each integer. Integer acts as index into the array of mutex_locks.
* Using mutex we make sure that no two threads are using same key to index.
* Two worker threads can still access dictionary parallely provided they have are to work with different keys.
---