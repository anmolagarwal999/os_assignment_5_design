#include <bits/stdc++.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>

#define PORT 9000
#define MAXBUF 1024
#define MAXLENSTRING 500
#define NO_CLIENTS 100

enum reqType {Insert, Del, update, concat, fetch};
typedef enum reqType reqType;

using namespace std;
struct request
{
	int key;
	string value;
	reqType type;
};
typedef request request;

pthread_mutex_t Keys[101];
pthread_cond_t *Workers;
pthread_mutex_t *Workers_mut;
pthread_cond_t Available;
pthread_mutex_t Available_mut;
int* ArgsSoc;
bool *Free;
int NoWorkerFree;
map <int, string> Dictonary;


void* WorkerFunction(void * Arg);
string convertToString(char* A, int size);
void ProcessRequest(request req, char* Output, int ThreadId);

int main(int argc, char const *argv[])
{
	int Num_workers = atoi(argv[1]);
	srand(time(NULL));

	//Intializing all locks 123.
	Workers = (pthread_cond_t*) malloc(Num_workers * sizeof(pthread_cond_t));
	Workers_mut = (pthread_mutex_t*) malloc(Num_workers * sizeof(pthread_mutex_t));

	pthread_mutex_init(&Available_mut, NULL);
	pthread_cond_init(&Available, NULL);

	for(int i = 0; i < 101; i++)
		pthread_mutex_init(&Keys[i], NULL);
	
	for(int i = 0; i < Num_workers; i++)
	{
		pthread_cond_init(&Workers[i], NULL);
		pthread_mutex_init(&Workers_mut[i], NULL);
	}

	//Initializing Socket for listening
	struct sockaddr_in bind_address;
	bind_address.sin_family = AF_INET;
	bind_address.sin_addr.s_addr = INADDR_ANY;
	bind_address.sin_port = htons( PORT );

  int error;
	int addrlen = sizeof(bind_address);

	int server_fd = socket(AF_INET, SOCK_STREAM, 0);
  assert(server_fd >= 0);
  
	error = bind(server_fd, (struct sockaddr *)&bind_address, sizeof(bind_address));
  assert(error == 0);

	error = listen(server_fd, NO_CLIENTS);
  assert(error == 0);

	//Initializing workers
	Free = (bool*) malloc(Num_workers * sizeof(bool));
	ArgsSoc = (int *) malloc(Num_workers * sizeof(int));
	pthread_t WorkerThreads[Num_workers];
	int args_i[Num_workers];

	NoWorkerFree = Num_workers;
	for(int i = 0; i < Num_workers; i++)
	{
		args_i[i] = i;
		Free[i] = true;
		pthread_create(&WorkerThreads[i], NULL, WorkerFunction, (void*)&args_i[i]);
	}

	int whichThread;
	while(1)
	{
		int new_socket = accept(server_fd, (struct sockaddr *)&bind_address, (socklen_t*)&addrlen);
	  assert(new_socket >= 0);

		pthread_mutex_lock(&Available_mut);
		while(NoWorkerFree <= 0)
			pthread_cond_wait(&Available, &Available_mut);
		pthread_mutex_unlock(&Available_mut);

		for(int i = 0; i < Num_workers; i++)
		{
			pthread_mutex_lock(&Workers_mut[i]);
			if(Free[i] == true)
				whichThread = i;
			pthread_mutex_unlock(&Workers_mut[i]);
		}

		pthread_mutex_lock(&Workers_mut[whichThread]);
		ArgsSoc[whichThread] = new_socket;
		Free[whichThread] = false;  //It is true before this.
		NoWorkerFree--;
		pthread_cond_signal(&Workers[whichThread]);
		pthread_mutex_unlock(&Workers_mut[whichThread]);
	}

	//exit by ctrl+c
  error = close(server_fd);
	assert(error == 0);
	return 0;
}


void* WorkerFunction(void * Arg)
{
	int Index = *(int *)Arg;
	char Readbuffer[MAXBUF] = {0}, WriteBuffer[MAXBUF] = {0}, Temp[MAXLENSTRING];
	int error, new_socket, valread;
	request req;

	while(1)
	{
		pthread_mutex_lock(&Workers_mut[Index]);
		while(Free[Index] == true)
			pthread_cond_wait(&Workers[Index], &Workers_mut[Index]);
		pthread_mutex_unlock(&Workers_mut[Index]);

		new_socket = ArgsSoc[Index];

		valread = read(new_socket, Readbuffer, 1024);
		sscanf(Readbuffer, "%d %d %s", &req.type, &req.key, Temp);
		req.value = convertToString(Temp, strlen(Temp));

		ProcessRequest(req, WriteBuffer, Index);
		float x = (rand() % 100)/(float)100;
		sleep(2 + x);
		send(new_socket , WriteBuffer, strlen(WriteBuffer), 0);

		error = close(new_socket);
		assert(error == 0);

		pthread_mutex_lock(&Available_mut);
		pthread_mutex_lock(&Workers_mut[Index]);
		Free[Index] = true;
		pthread_mutex_unlock(&Workers_mut[Index]);
		NoWorkerFree++;
		pthread_cond_signal(&Available);
		pthread_mutex_unlock(&Available_mut);

	}

	return NULL;
}

string convertToString(char* A, int size)
{
    string ans = "";
    for (int i = 0; i < size; i++)
			ans.push_back(A[i]);
			
    return ans;
}

void ProcessRequest(request req, char* Output, int ThreadId)
{
	int key2;
	char Temp[MAXLENSTRING];
	pthread_mutex_lock(&Keys[req.key]);

	if(req.type == Insert)
	{
		if(Dictonary.find(req.key) == Dictonary.end())
		{
			Dictonary[req.key] = req.value;
			sprintf(Output, "%d:Insertion successful", ThreadId);
		}
		else
			sprintf(Output, "%d:Key already exists", ThreadId);
	}

	if(req.type == Del)
	{
		if(Dictonary.find(req.key) != Dictonary.end())
		{
			Dictonary.erase(req.key);
			sprintf(Output, "%d:Deletion successful", ThreadId);
		}
		else
			sprintf(Output, "%d:No such key exists", ThreadId);
	}		

	if(req.type == update)
	{
		if(Dictonary.find(req.key) != Dictonary.end())
		{
			Dictonary[req.key] = req.value;

			for(int i = 0; i < req.value.length(); i++)
				Temp[i] = req.value[i];
			Temp[req.value.length()] = '\0';
			sprintf(Output, "%d:%s", ThreadId, Temp);
		}
		else
			sprintf(Output, "%d:Key does not exist", ThreadId);
	}

	if(req.type == concat)
	{
		stringstream geek(req.value);
		geek >> key2;
		pthread_mutex_lock(&Keys[key2]);
		if(Dictonary.find(req.key) != Dictonary.end() && Dictonary.find(key2) != Dictonary.end())
		{
			string ans = Dictonary[req.key] + Dictonary[key2];
			Dictonary[req.key] = ans;
			Dictonary[key2] = ans;

			for(int i = 0; i < ans.length(); i++)
				Temp[i] = ans[i];
			Temp[ans.length()] = '\0';
			sprintf(Output, "%d:%s", ThreadId, Temp);
		}
		else
			sprintf(Output, "%d:Concat failed as at least one of the keys does not exist", ThreadId);
			
		pthread_mutex_unlock(&Keys[key2]);
	}

	if(req.type == fetch)
	{
		if(Dictonary.find(req.key) != Dictonary.end())
		{
			string ret = Dictonary[req.key];
			
			for(int i = 0; i < ret.length(); i++)
				Temp[i] = ret[i];
			Temp[ret.length()] = '\0';
			sprintf(Output, "%d:%s", ThreadId, Temp);
		}
		else
			sprintf(Output, "%d:Key does not exist", ThreadId);

	}

	pthread_mutex_unlock(&Keys[req.key]);
}

//Send with id
//synconization part.