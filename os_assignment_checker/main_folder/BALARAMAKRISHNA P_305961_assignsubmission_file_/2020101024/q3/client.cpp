#include <bits/stdc++.h>
#include <assert.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#define PORT 9000
#define MAXBUF 1024
#define MAXLENSTRING 500

using namespace std;



enum reqType {Insert, Del, update, concat, fetch};
typedef enum reqType reqType;

struct request
{
	int key;
	char value[MAXLENSTRING];
	reqType type;
	int time;
};
typedef request request;
request* Requests;

pthread_mutex_t PrintLock;
int input();
void* Client(void* arg);


int main()
{
	int num_requests = input();

	pthread_t Threads[num_requests];
	int args[num_requests];
	for(int i = 0; i < num_requests; i++)
		args[i] = i;
	
	int time = 0, num_req_done = 0, N;
	while(num_req_done < num_requests)
	{
		sleep(1);
		time++;

		for(N = 0; Requests[num_req_done + N].time <= time && num_req_done + N < num_requests; N++);
		for(int i = 0; i < N; i++)
			pthread_create(&Threads[num_req_done + i], NULL, Client, (void *)&args[num_req_done + i]);
		num_req_done += N;
	}

	for(int i = 0; i < num_requests; i++)
		pthread_join(Threads[i], NULL);

	return 0;
}

int input()
{
	int Num_Requests;
	char Command[100];

	scanf("%d", &Num_Requests);
	
	Requests = (request*) malloc(Num_Requests * sizeof(request));
	assert(Requests != NULL);
	for(int i = 0; i < Num_Requests; i++)
	{
		scanf("%d %s %d", &Requests[i].time, Command, &Requests[i].key);

		if(strcmp(Command, "insert") == 0)
			Requests[i].type = Insert;
		if(strcmp(Command, "delete") == 0)
			Requests[i].type = Del;
		if(strcmp(Command, "update") == 0)
			Requests[i].type = update;
		if(strcmp(Command, "concat") == 0)
			Requests[i].type = concat;
		if(strcmp(Command, "fetch") == 0)
			Requests[i].type = fetch;

		if(Requests[i].type != fetch & Requests[i].type != Del)
			scanf("%s", &Requests[i].value);
		
	}

	pthread_mutex_init(&PrintLock, NULL);
	return Num_Requests;
}

void* Client(void* arg)
{
	int Index = *(int*)arg;
  int Error;
	char Readbuffer[MAXBUF] = {0}, WriteBuffer[MAXBUF] = {0};

	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	Error = inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
  assert(Error >= 0);

	int sock = socket(AF_INET, SOCK_STREAM, 0);
  assert(sock >= 0);
	
	Error = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
  assert(Error >= 0);



  //using socket
	sprintf(WriteBuffer, "%d %d %s", Requests[Index].type, Requests[Index].key, Requests[Index].value);
	send(sock , WriteBuffer, strlen(WriteBuffer), 0);

	Error = read(sock, Readbuffer, MAXBUF);
	assert(Error > 0);

	pthread_mutex_lock(&PrintLock);
	printf("%d:", Index);
	printf("%s\n", Readbuffer);
	pthread_mutex_unlock(&PrintLock);



  //Clossing the network connection.
  close(sock);
	return NULL;
}