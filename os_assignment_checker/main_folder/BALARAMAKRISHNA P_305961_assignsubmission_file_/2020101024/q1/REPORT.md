## LABS
### Data stored
```c
struct lab
{
 char Name[LabNameLenght];  //Name of the lab
 int N_TAs;                 //Number of members
 int Max_Tuts;              //number of times a member of the lab can be TA mentor

 pthread_mutex_t lock; 
 int* TimesTut;             //To know members who can take a tut
 bool* Available;           //To know members who are taking a tut
 bool Alive;                //Is there any member who can take tut
};
```


### Abstraction
```c
/*
  Returns -1 if lab is dead.
  Returns -2 if lab is alive but no TA is available.
  Returns an int >= 0. Index of alloted TA.
  It is a thread safe function.
  Which time'th TA is selected is stored in int variable pointed by time.
*/
int SelectTA(int Lab, int *time);
```
* Thread safety is ensured by ensuring only one thread is running *SelectTA* for a particular lab.   
Two Different threads can run *SelectTA* if they call this function with different lab(input). 
* A lab is alive if there exits a TA (int a) such that it can take one more tutorial (TimesTut[a] < Max_Tuts).  
Once a lab is dead it stays dead.
* A lab is alive but all TA that can take atleast one more tut are already taking a tut (busy, not available).

```c
/*
  Returns -1 if no TA can take tutorial. (withdrawn of course sinareo)
  Return int >= 0 which is the index of the TA form (*whichlab) alloted.
  The index of lab is stored in an int variable pointed by whichLab.
  Which time'th TA is selected is stored in int variable pointed by whichtime.
*/
int getTA(int *listOfLabs, int N, int* whichLab, int* whichtime);
```
This function runs only ***SelectTA*** internally and uses not global variables. This makes it thread safe. No need to worry about synchronization.



---
<br>


## Courses.
###  Data Stored
```c
struct course
{
  char Name[CourseNameLenght];      //For printing
  float Interest;                   //For deciding student selects or not.
  int max_slots;                    //For deciding num of slots.
  int N_Labs;                       //For selecting TA.
  int* AssortedLabs;                //For selecting Ta.

  pthread_rwlock_t lock;            //Synchonization of data inside course.
  bool Active;                      //Is the course still running or withdrawn.
};
```
### Abstraction
* All Data in courses is read only, whose value is given as input. Except for ***Active***. 
  * Read write block applies for this data. 
  * The value is changed only by the Tutorial thread corresponding to that course. 
  * This value is read by *Move_NextPref*.
* The use of each data in mentioned in "Data Stored".

---
<br>

## Students
### Data
```c
struct student
{
  float calibre;
  int Preference;                //Preferences[Pid] waiting for.
  int Preferences[3];
  int Pid;
  int time;

  bool Modifing;                //Synchronization tool
  int id;                       //The number based on order.
  bool alive;                   //Present in simuation or exited.
};

int active, exited;
pthread_rwlock_t rwlock, printLock;
pthread_mutex_t exitL; 
```
We sort the student array increasing with respect to time.
### Abstractions
```c
/*
  Returns a pointed to interger array in heap memory. 
  It stores the ideces of students who are alloted slots.
  It stores the lenght of array in int variable pointed by No.
  Lenght of array returned <= NSlots.
*/
int *getStudents(int CourseId, int NSlots, int *No);

/*
  Activates students simutes that student has fill form. He is avaliable to *getStudents*.
*/
void *studentActivation(void *Args);

/*
  This function is called when the student decides to leave his current preference.
  This function may cause the student at index "index" to either exit or wait for next (valid) perefrence.
  Valid preference is a course which has not yet withdrawn. 
*/
void Move_NextPref(int index);
```
Synchronization.
* ***getStudents***, ***studentActivation***, ***Move_NextPref*** Are the only functions which accesses the student array.
* ***studentActivation*** is a thread function. A thread exits which runs this function.
* ***getStudents*** is called by every Tutorial thread every-time before happening of a tutorial.
* ***Move_NextPref*** is called by every Tutorial thread on every allotted student who have decided not to take the course.
* ***studentActivation*** uses only students whose indices are greater than or equal to ***active***.
* ***Move_NextPref*** and ***getStudents*** uses only students whose indices are less than ***active***.
* ***Move_NextPref*** uses only those students whose modifying bool is true.
* ***getStudents*** uses only those students whose modifying value is false.
* ***Move_NextPref*** by usage is never called on two students from two different threads.
* ***getStudents***  called by a Tutorial thread does not look into students whose first preferences is not the same course.
  * **modifying** is used tell if a student first preference matches but it is not completely modified.

---
<br>




## Tutorial working
```c
/*
  It simulates the process of taking tutorial. 
  * getting TA.
  * deciding slots.
  * alloting students.
  * takking tutorial.
  * students making decision.
  * course withdrawl logic
  This function is run by a thread. (main function of a thread)
  We create a thread for every course.
*/
void *Tutorial(void *Args);
```
* Uses all the above the above abstractions defined. 

---
<br>

## Main function
* We call **Input** to initialize(take input) all the data.
* We sort the ***students*** array.
* We create a thread running ***Tutorial*** for every course.
* We busy wait till all students have exited the simulation.
* Terminate the program

---
---