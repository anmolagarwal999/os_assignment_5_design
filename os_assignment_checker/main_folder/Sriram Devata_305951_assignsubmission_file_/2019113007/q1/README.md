# Question 1

## Running the code

Run the code with:

```bash
gcc -g main.c -lpthread
./a.out < testcast.txt
```

## Working of the code

### Data structures

- `int ***lab_tas`: labwise information about each TA
- `pthread_mutex_t **lab_tas_lock`: mutex lock when reading/writing information about a TA
- `pthread_cond_t **lab_tas_available`: condition variable to signal that information about a TA has changed
- `int *courses_spots`: number of available spots in a course after a TA is allocated to the course
- `pthread_mutex_t *courses_spots_lock`: mutex lock when changing `courses_spots`
- `pthread_cond_t *courses_spots_available`: condition variable to signal when a course has been allocated some slots
- `int *student_status`: status of a student (waiting for a course or in a tutorial for a course)
- `pthread_mutex_t *student_status_lock`: mutex lock when changing `student_status`
- `pthread_cond_t *student_status_available`: condition variable to signal that the status of a student has changed

### int main()

- The input is stored in `struct student`, `struct lab`, and `struct course` instances
- Create the threads for simulating students and for simulating courses
- Wait for the student and course threads to finish

### simulate_student (student thread)

- Sleep for `fill_time` seconds of a student
- Choose the available course in the order of preference
- Loop until the student goes through all 3 preferences
- Update `student_status` to show that the student is looking for a course
- Wait for spots in the currently preferred course to open
- If the student waves up because the course has spots open, take up a spot
- Update `student_status` to show that the student is currently in a tutorial
- Wait until the TA releases the student
- Choose to finalize or withdraw the course based on a random probability
- If the student withdrew willingly or if the course was removed, update the course preference

### simulate_course (course thread)

- Go through each of the labs and choose the first TA that can also TA the course
- If the TA is currently busy, wait for the TA to become available
- If no TA has been found at any point, then withdraw the course
- Check if using up this TA made the total tutorials available in a lab 0
- Open D slots for the course
- Until there is at least 1 seat filled, or if there are students waiting and there are empty spots, keep broadcasting that this course has empty spots
- If noone is waiting for this course, stop broadcasting and start the tutorial with noone
- After the tutorial is done, wake up all the students in the tutorial
