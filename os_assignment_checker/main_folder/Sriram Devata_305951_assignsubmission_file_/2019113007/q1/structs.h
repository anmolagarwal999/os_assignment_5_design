typedef struct student {
    int id;
    int fill_time;
    float calibre;
    int preferences[3];
} student;

typedef struct lab {
    int id;
    char name[20];
    int num_tas;
    int max_courses;
} lab;

typedef struct course {
    int id;
    char name[20];
    float interest;
    int course_max_slots;
    int num_labs;
    int *labs;
} course;
