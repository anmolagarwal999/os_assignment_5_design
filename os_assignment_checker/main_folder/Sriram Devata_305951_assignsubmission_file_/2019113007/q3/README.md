# Question 3

## Running the code

Run the code with:

```bash
g++ server.cpp -o server -lpthread
./server N
```

```bash
g++ client.cpp -g -o client -lpthread
./client < testcase.txt
```

## Important data structures

- `sem_t client_reqs` - semaphore to store the client requests left to handle

- `pthread_mutex_t client_sockets_lock` - mutex lock when modifying `client_sockets`
- `queue<int> client_sockets` - queue to store the clients to be handled

- `string full_dictionary[101]` - the "dictionary" that the server is supposed to store
- `pthread_mutex_t full_dictionary_lock[101]` - mutex lock when modifying any value of the dictionary

## Working of the code

### Working of the server

- The server spawns `m` worker threads
- When any client tries to make a connection with the server, the server allocates a socket filedescriptor and pushes it into a queue
- When a socket filedescriptor is added to the queue, the semaphore `client_reqs` is posted
- Each worker thread waits on the semaphore `client_reqs`
- When the semaphore is posted, a worker pops a file descriptor from the queue and handles the requests

### Working of the client

- Each client thread gets its own query string to handle
- Each thread sleeps for the specific amount of time and then submits the query to the server
- The thread formats and print the relevant output
