# Question 2

## Running the code

Run the code with:

```bash
gcc -g main.c -lpthread
./a.out < testcast.txt
```

## Working of the code

### Data structures

- `sem_t h_zone, a_zone, n_zone`: semaphores corresponding to the available seats in zones H, A, and N respectively
- `int goals[2]`: number of goals that teams H and A have scored
- `pthread_mutex_t goals_lock[2]`: mutex lock when changing `goals`
- `pthread_cond_t goals_changed[2]`: conditional variable to signal that goals of a team have changed
- `char *person_in_zone`: zone that the person is currently in
- `pthread_mutex_t *person_zone_lock`: mutex lock when changing the zone of a person
- `pthread_cond_t *person_moved`: conditional variable to signal that a person has moved

### int main()

- Store the input into `struct person` and `struct team`
- For the goal scoring chances, store the time from the last scored goal, instead of time from the start of the match. This will allow us to just sleep the thread between the goals
- Create threads to simulate a person entering and exiting the stadium, and a thread to simulate the teams scoring goals
- Wait for these threads to finish

### simulate_person_enter_exit (Person going to the stadium and leaving)

- A person thread is broken into multiple threads. The first thread handles the person entering the stadium, watching a game (in another thread), and then exiting
- Sleep until the person's reach time
- Call the thread `simulate_person_seat` to search for seats in the respective zones
- If the person did not find any seat anywhere, leave the stadium
- Call the thread `simulate_person_in_game` for the person to be in the game
- Wait for the thread `simulate_person_in_game` to signal `person_moved` for `spectating_time`. If the thread doesn't signal by then, then leave the match due to spectating time being expired
- Increment the semaphore corresponding to where the person was in, since they just left the stadium

### simulate_person_seat (Person searching for a seat)

- Depending on the type of the fan, call different `search_in_{n,a,h}_zone` threads to search for seats in zones
- Wait for the created threads to finish

### search_in_{n,a,h}_zone (Person searching for a seat in specific zones)

- Wait for the semaphore corresponding to the zone with `sem_timedwait`
- If the person is still at the entrance after the semaphore timed out, update `person_in_zone` to show that the person didn't get any seat
- If the person is still at the entrance when the semaphore is signalled, then allocate a seat in that zone
- If some other thread gave the person a seat before this thread, then post to the present semaphore

### simulate_person_in_game (Person got a seat and is watching a game)

- If the fan is a supporter of H or A, then check for the goals of the opposing teams
- Wait until the opposing team's goals don't enrage them, and till the game is not done
- If the opposing team's goals enrage the person, exit
- The parent thread doesn't wait for this thread, the current thread exits when the game is done

### simulate_team (Teams scoring goals during their goal scoring chances)

- Sleep until the next goal scoring chance
- If the team scores a goal, increment the `goals` of the team
- After every goal scoring chance, broadcast to whatever thread is waiting on `goals_changed`
