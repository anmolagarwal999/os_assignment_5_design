typedef struct person {
    char name[20];
    char fan_type;
    int id;
    int group_no;
    int reach_time;
    int patience_time;
    int num_goals;
} person;

typedef struct team {
    char team_type;
    int id;
    int num_chances;
    int *time_from_previous_chance;
    float *probability_goal;
} team;
