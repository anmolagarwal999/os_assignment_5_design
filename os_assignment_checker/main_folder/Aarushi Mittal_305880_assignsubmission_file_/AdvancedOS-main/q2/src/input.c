#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"


void inputZones()
{
    Zone[HOME].Type = 'H';         // Home
    Zone[AWAY].Type = 'A';         // Away
    Zone[NEUT].Type = 'N';         // Neutral

    // Input zone capacities
    printf(BLUE "Home | Away | Neutral\n" CLEAR);
    scanf("%d %d %d", &Zone[0].Capacity, &Zone[1].Capacity, &Zone[2].Capacity);

    // Iterate through all the zones
    for(int i = 0; i < 3; i++)
    {
        // Intialize zones. Create locks for every seat in a zone
        Zone[i].NumSpectators = 0;
        Zone[i].Spectator = (tPerson*)malloc(Zone[i].Capacity * sizeof(tPerson));
        Zone[i].Seat = malloc(Zone[i].Capacity * sizeof(tSeat));
        Zone[i].SeatLocks = malloc(Zone[i].Capacity * sizeof(pthread_mutex_t));
        
        // Initialize the lock for each seat in a zone
        for(int j = 0; j < Zone[i].Capacity; j++)
        {
            pthread_mutex_init(&Zone[i].SeatLocks[j], NULL);
        }
    }
}

void inputGroups()
{
    Group = (tGroup*)malloc(num_groups * sizeof(tGroup));

    // Iterate through the groups
    for(int i = 0; i < num_groups; i++)
    {
        flushSTDIN();
        printf( RED "\n(Group %d)\n"   CLEAR, i+1);

        // Number of people in group i
        printf( RED "Number: "   CLEAR);
        scanf("%d", &Group[i].k);
        num_people += Group[i].k;

        // Allocate memory for all persons in a group
        Group[i].Person = (tPerson*)malloc(Group[i].k * sizeof(tPerson));

        // Initially, no one is waiting
        Group[i].Waiting = 0;
        
        // Input persons in the group
        inputPersons(i);
    }
}

void inputPersons(int i)
{
    // Iterate through all the persons in the group
    for(int j = 0; j < Group[i].k; j++)
    {
        // Prefix
        printf( RED "P%d: " CLEAR, j+1);

        // Input person variables
        scanf("%s %c %d %d %d", &Group[i].Person[j].Name,&Group[i].Person[j].SupportTeam,&Group[i].Person[j].ArrivalTime,&Group[i].Person[j].Patience,&Group[i].Person[j].EnrageNum);
        // Group[i].Person[j].status = REACHED;
    }
}

void inputGoals()
{
    Goal = (tGoal*)malloc(G * sizeof(tGoal));

    for(int i = 0; i < G; i++)
    {
        pthread_mutex_init(&Goal[i].GoalLock, NULL);
        flushSTDIN();

        // Prefix
        printf( GREEN "G%d: "   CLEAR, i+1);

        // Input goal variables
        scanf("%c %d %f", &Goal[i].Team,&Goal[i].GoalTime,&Goal[i].GoalProb);
    }   
    printf("\n");
}

void input()
{
    inputZones();

    printf( RED "Spectating Time: " CLEAR);
    scanf("%d", &X);

    // Groups, Persons
    printf( YELLOW "Number of groups: " CLEAR);
    scanf("%d", &num_groups);
    inputGroups();

    // Goals
    printf( GREEN "\nNumber of goal scoring chances: "   CLEAR);
    scanf("%d", &G);
    goal_thread = (pthread_t*)malloc(G * sizeof(pthread_t));
    inputGoals();
}

void printZone(int i)
{
    printf("\nType = %c\n", Zone[i].Type);
    printf("Capacity = %d\n", Zone[i].Capacity);
    for(int j = 0; j < Zone[i].NumSpectators; j++)
    {
        printf("%d: %s\n", j+1, Zone[i].Spectator[j].Name);
    }
    printf("\n");
}

void printGroup(int i)
{
    printf( RED "(Group %d)\nNumber: %d\n",i+1,Group[i].k);
    

    for(int j = 0; j < Group[i].k; j++)
    {
        printf(RED"P %d :",j+1);

        printf("%s\t%c %d %d %d\n", Group[i].Person[j].Name, Group[i].Person[j].SupportTeam ,Group[i].Person[j].ArrivalTime,Group[i].Person[j].Patience, Group[i].Person[j].EnrageNum);
    }
    printf("\n");
}

void printGoals()
{
    for(int i = 0; i < G; i++)
    {
        printf("G%d: %c %d %f\n", i+1, Goal[i].Team, Goal[i].GoalTime, Goal[i].GoalProb);
    }
}