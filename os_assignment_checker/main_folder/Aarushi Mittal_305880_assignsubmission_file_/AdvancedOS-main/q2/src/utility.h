#include "libraries.h"
//clears input buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
void coord(int i, int j)
{
    printf("(%d, %d) %s\n", i+1, j+1, Group[i].Person[j].Name);
}

int getZoneAsInt(char c)
{
	if(c =='H')
	return HOME;
	else if(c == 'A')
		return AWAY;
		else if(c == 'N')
		return NEUT;
    
}

char getZoneAsChar(int zone)
{
    if(zone == HOME)
    return 'H';
    else if(zone == AWAY)
    		return 'A';
    	else if(zone == NEUT)
    		return 'N';
}

int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}


