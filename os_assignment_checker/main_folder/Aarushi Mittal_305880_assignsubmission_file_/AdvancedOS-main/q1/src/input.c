#include "libraries.h"
#include "variables.h"
#include "utility.h"



void inputCourses()
{
    Course = (course*)malloc(numCourses * sizeof(course));
    for(int i = 0; i < numCourses; i++)
    {
        // Prefix
        printf( RED "C%d: "   CLEAR, i+1);

        // Set Course ID and status
        Course[i].cid = i;
        Course[i].Status = FREE;
        Course[i].SlotsFilled = 0;

        // Input course variables
        scanf("%s %f %d %d", &Course[i].Name,&Course[i].Interest,&Course[i].MaxSlots,&Course[i].NumLabs);

        // Input lab id's for this course
        Course[i].lid = (int*)malloc(Course[i].NumLabs * sizeof(int));
        for(int j = 0; j < Course[i].NumLabs; j++)
        {
            scanf("%d", &Course[i].lid[j]);
        }
    }
}

void inputStudents()
{
    Student = (student*)malloc(numStudents * sizeof(student));

    // Iterate through all the students
    for(int i = 0; i < numStudents; i++)
    {
        flushSTDIN();

        // Prefix
        printf( CYAN "S%d: "   CLEAR, i+1);

        // Set student ID
        Student[i].StudentID = i;
        Student[i].Current = 0;
        Student[i].Status = NOT_FILLED;

        // Input student variables
        scanf("%f %d %d %d %d", &Student[i].Calibre,&Student[i].Pref[0],&Student[i].Pref[1], &Student[i].Pref[2],&Student[i].FillTime);
    }
}

mentor* init_TA(int i)
{
    mentor *m =(mentor*)malloc((Lab[i].NumMentors + 1) * sizeof(mentor));
        for(int j = 0; j < Lab[i].NumMentors; j++)
        {
            m[j].ID = j;
            m[j].Num = 0;
            m[j].Status = FREE;
            pthread_mutex_init(&m[j].MentorLock, NULL);
        }

        return m;
}

void inputLabs()
{
    Lab = (lab*)malloc(numLabs * sizeof(lab));

    for(int i = 0; i < numLabs; i++)
    {
        flushSTDIN();

        // Prefix
        printf( GREEN "L%d: "   CLEAR, i+1);

        // Initialize all TA's
        Lab[i].Mentor = init_TA(i);

        // Input lab variables
        scanf("%s %d %f", &Lab[i].Name,&Lab[i].NumMentors,&Lab[i].Max);
    }   
    printf("\n");
}

void input()
{
    printf( BLUE "#Students | #Labs | #Courses\n"   CLEAR);
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);

    inputCourses();
    inputStudents();
    inputLabs();
}