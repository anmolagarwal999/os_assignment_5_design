#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputCourses();
    void inputStudents();
    mentor* init_TA(int i);
    void inputLabs();
    void printZone(int i);s
    void printGroup(int i);

// Main
void per_thrd();
void threadExit();

// Student
void* studentFunction(void* arg);
    void courseRegister(int i);

// Course
void* courseFunction(void* arg);
    void allocateMentor(int i);
    void numSlots(int i);
    void allocateSeats(int C);

// Lab
void* labFunction(void* arg);

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();
int randint(int L);

#endif
