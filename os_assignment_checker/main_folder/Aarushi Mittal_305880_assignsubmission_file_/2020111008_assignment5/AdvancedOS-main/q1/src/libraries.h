#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define CLEAR   "\x1b[0m"

// Course/Mentor status
#define FREE 0
#define OCCUPIED 1

// Student status
#define NOT_FILLED 0
#define FILLED 1

// Types time
typedef uint Time;

// Student 
struct stStudent
{
    
    int sid;
    float Calibre;
    int Current;
    int Pref[2];
    Time FillTime;
    int Status;
};
typedef struct stStudent student;


// Course 
struct stCourse
{
    int cid;
    char Name[10];
    float Interest;
    int MaxSlots;
    int Slots;
    int SlotsFilled;
    int NumLabs;
    int* lid;
    int Status;
};
typedef struct stCourse course;

// Mentor 
struct stMentor
{
    int ID;
    int Num;
    int Status;
    pthread_mutex_t MentorLock;
};
typedef struct stMentor mentor;

// Lab
struct stLab
{
    char Name[10];
    int NumMentors;
    tMentor* Mentor;
    int Max;
};
typedef struct stLab lab;


#endif
