#include "libraries.h"
//empties buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
//check if probability n is less than random number between 0 and 1
int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}

// Random number between 1 and L 
int randint(int L)
{
    srand(time(0));
    return (rand() % L) + 1;
}
