#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

#define SERVERPORT 8989
#define BUFSIZE 4096
#define SOCKETERROR -1
#define SERVER_BACKLOG 1

typedef struct sockaddr_in SA_IN;
typedef struct sockaddr SA;

void handle_connection(int client_socket)
{
    char buffer[BUFSIZE];
    size_t bytes_read;
    int msgsize = 0;
    char actualmsg[BUFSIZE];

    while ((bytes_read = read(client_socket, buffer+msgsize, sizeof(buffer)-msgsize-1)) > 0)
    {
        msgsize += bytes_read;
        if(msgsize >= BUFSIZE || buffer[msgsize-1] == '\n'){
            break;
        }
    }
    if(bytes_read < 0){
        cout << "Error reading from socket" << endl;
        exit(1);
    }
    buffer[msgsize-1] = '\0';
    cout << "Message received: " << buffer << endl;
    fflush(stdout);
    
    close(client_socket);
    cout << "Connection closed" << endl;
}

int main(int argc, char **argv) {
    int server_socket,client_socket,addr_size;
    SA_IN server_addr, client_addr;

    if((server_socket = socket(AF_INET,SOCK_STREAM,0))==-1){
        cout << "Failed to create socket" << endl;
        exit(1);
    }

    // initialise the address struct
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(SERVERPORT);

    if(bind(server_socket,(SA*)&server_addr, sizeof(server_addr))==-1){
        cout << "Failed to bind socket" << endl;
        exit(1);
    }
    if(listen(server_socket,SERVER_BACKLOG)==-1){
        cout << "Failed to listen on socket" << endl;
        exit(1);
    }
    
    while(1) {
        addr_size = sizeof(SA_IN);
        if((client_socket = accept(server_socket,(SA*)&client_addr,(socklen_t*)&addr_size))==-1)
        {
            cout << "Failed to accept connection" << endl;
            exit(1);
        }
        cout << "Connection accepted" << endl;
        handle_connection(client_socket);
    }

    return 0;
}
