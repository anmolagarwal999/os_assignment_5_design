#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <map>
#include <queue>
#include <vector>
#include <string>
#include <sstream>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8989

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

// map<int, string> dict;
vector<string> dict(101,"");
int MAX_WORKERS;
pthread_t *pool;
queue<int*> client_queue;

pthread_mutex_t client_queue_lock = PTHREAD_MUTEX_INITIALIZER;
vector<pthread_mutex_t> dict_locks(101, PTHREAD_MUTEX_INITIALIZER);
pthread_cond_t client_queue_cond = PTHREAD_COND_INITIALIZER;

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

void tokenize(string const &str,vector<string> &out,const char delim ) 
{ 
    // construct a stream from the string 
    stringstream ss(str); 
    string s; 
    while (getline(ss, s, delim)) { 
        out.push_back(s); 
    } 
} 
///////////////////////////////

void *handle_connection(void *client_socket_fd_ptr)
{
    int client_socket_fd = *((int *)client_socket_fd_ptr);
    free(client_socket_fd_ptr); 
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd, msg_to_send_back;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        else{
            
            // cout << "Client sent : " << cmd << endl;
        
            vector<string> cmd_split ;
            tokenize(cmd, cmd_split, ' ');

            if(cmd_split[0] == "insert"){
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[1])]);
                if (dict[stoi(cmd_split[1])] != "")
                {
                    msg_to_send_back = "Key already exists";
                }
                else
                {
                    dict[stoi(cmd_split[1])] = cmd_split[2];
                    msg_to_send_back = "Insertion successful";
                }
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[1])]);
            }
            else if(cmd_split[0] == "delete"){
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[1])]);
                if (dict[stoi(cmd_split[1])] == "")
                {
                    msg_to_send_back = "No such key exists";
                }
                else
                {
                    dict[stoi(cmd_split[1])]="";
                    msg_to_send_back = "Deletion successful";
                }
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[1])]);
            }
            else if(cmd_split[0] == "update"){
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[1])]);
                if (dict[stoi(cmd_split[1])] == "")
                {
                    msg_to_send_back = "Key does not exist";
                }
                else
                {
                    dict[stoi(cmd_split[1])] = cmd_split[2];
                    msg_to_send_back = cmd_split[2];
                }
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[1])]);
            }
            else if(cmd_split[0] == "concat"){
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[1])]);
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[2])]);
                if (dict[stoi(cmd_split[1])] == "" || dict[stoi(cmd_split[2])] == "")
                {
                    msg_to_send_back = "Concat failed as at least one of the keys does not exist";
                }
                else
                {
                    string a = dict[stoi(cmd_split[1])];
                    string b = dict[stoi(cmd_split[2])];
                    dict[stoi(cmd_split[1])] = a + b;
                    dict[stoi(cmd_split[2])] = b + a;
                    msg_to_send_back = dict[stoi(cmd_split[2])];
                }
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[2])]);
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[1])]);
            }
            else if(cmd_split[0] == "fetch"){
                pthread_mutex_lock(&dict_locks[stoi(cmd_split[1])]);
                if (dict[stoi(cmd_split[1])] == "")
                {
                    msg_to_send_back = "Key does not exist";
                }
                else
                {
                    msg_to_send_back = dict[stoi(cmd_split[1])];
                }
                pthread_mutex_unlock(&dict_locks[stoi(cmd_split[1])]);
            }
            else{
                msg_to_send_back = "Invalid command";
            }
            int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
            // debug(sent_to_client);
            if (sent_to_client == -1)
            {
                perror("Error while writing to client. Seems socket has been closed");
                goto close_client_socket_ceremony;
            }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}}

void *thread_function(void *arg)
{
    while (true)
    {
        pthread_mutex_lock(&client_queue_lock);
        
        if(client_queue.size()==0){
            pthread_cond_wait(&client_queue_cond, &client_queue_lock);
            pthread_mutex_unlock(&client_queue_lock);
        }
        else{
            int* client_socket_fd = client_queue.front();
            client_queue.pop();
            pthread_mutex_unlock(&client_queue_lock);
            handle_connection(client_socket_fd);
        }
        
    }
    
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;

    MAX_WORKERS = stoi(argv[1]);
    pool = (pthread_t *)malloc((MAX_WORKERS) * sizeof(pthread_t));
    for(i = 0; i < MAX_WORKERS; i++)
    {
        pthread_create(&pool[i], NULL, thread_function, NULL);
    }
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    // cout << "creation done\n" << endl;
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    // cout  << "bind done\n" << endl;
    listen(wel_socket_fd, MAX_CLIENTS);
    // cout << "listen done\n" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        
        // handle_connection(client_socket_fd);
        // pthread_t t;
        int *cptr = (int*)malloc(sizeof(int));
        *cptr = client_socket_fd;
        // pthread_create(&t, NULL, handle_connection, cptr);
        pthread_mutex_lock(&client_queue_lock);
        client_queue.push(cptr);
        
        pthread_mutex_unlock(&client_queue_lock);
        pthread_cond_signal(&client_queue_cond);
    }

    close(wel_socket_fd);
    return 0;
}
