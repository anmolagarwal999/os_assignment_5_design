# Q3: Multithreaded Client and Server

<b>Context: </b>Multiple clients making requests to a single server program.

## Logic

There are two pieces to the server programme: a primary thread (the handler) and a thread pool (the workers). The handler takes care of incoming requests and passes client socket IDs to the workers indirectly. The workers are in charge of reading, processing, and responding to the requests, as well as returning the response to the allocated client.

An shared queue is used to store the client socket IDs, which is used as follows:

For each request, the client programme generates a thread and a socket, then sends a connection request to the server.

The main server thread accepts new incoming connections and returns the client socket ID. The `accept()` system call does this and it gets blocked until one of the worker threads becomes free. The handler then pushes this new client socket ID to the main queue.

The worker thread then gets assigned a client thread request by popping a socket ID from the array.

To avoid busy waiting, condition variable have been used, due to which the client thread pauses execution until the handler pushes a new client socket ID to the queue, after which the worker thread reads the request and processes it.

The dictionary has been implemented by using the vector data structure of c++ STL. 

After the execution of the required operation, the worker thread sends the response back to the client socket, which prints it.

## Compilation

1. To compile both the server and client programs,
```bash
$ make
```
2. Run the server first,
```bash
$ ./server <number of threads>
```
3. Then run the client program and give the input
```bash
$ ./client
<input>
```
