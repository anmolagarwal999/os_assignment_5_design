#ifndef CLIENTS_H
#define CLIENTS_H

#include <string>

struct client_info {
    std::string command;
    int sleep_time;
    int socket_fd;
    int index;
};

typedef client_info clients;
#endif

/*
11
1 insert 1 hello
2 insert 1 hello
2 insert 2 yes
2 insert 3 no
3 concat 1 2
3 concat 1 3
4 delete 3
5 delete 4
6 concat 1 4
7 update 1 final
8 concat 1 2
*/