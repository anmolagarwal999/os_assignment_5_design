#include "courses.h"
#include "students.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
void *course_thread(void *arg)
{
    struct course *course = (struct course*)arg;
    // printf("4\n");
    while(1)
    {
        int flag=0;
        int flag2=0;
        int y=0;
        int x=0;
        for(;x<course->allowed_labs;x++){
            int lab_num = course->labs_list[x];
            for(;y<lab_info[lab_num].num_students;y++)
            {
                pthread_mutex_lock(&lab_info[lab_num].mutex[y]);
                if(lab_info[lab_num].ta_availability[y]==1 && lab_info[lab_num].labs_taken[y] < lab_info[x].max_taships){
                    
                    lab_info[lab_num].ta_availability[y]=0;
                    // lab_info[lab_num].labs_taken[y]++;
                    flag=1;
                    break;
                }
                if(lab_info[lab_num].labs_taken[y]<lab_info[lab_num].max_taships){
                    flag2=1;
                }
                pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
            }
            if(flag)
                break;
        }

        
        // printf("5\n");
        // if(!flag && !flag2){
        //     continue;
        // check tas
        // printf("3\n");
        // for(int x=0;x<course->allowed_labs;x++){
        //     int lab_num = course->labs_list[x];
        //     for(int y=0;y<lab_info[lab_num].num_students;y++)
        //     {
        //         pthread_mutex_lock(&lab_info[lab_num].mutex[y]);
        //         if(lab_info[lab_num].labs_taken[y]<lab_info[x].max_taships && lab_info[lab_num].ta_availability[y]==1){
        //             flag2=1;
        //             pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
        //             break;
        //         }
        //         pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
        //     }
        //     if(flag2)
        //         break;
        // }

        if(flag==0 && flag2==0){
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", course->name);
            break;
        }

// check students
        int flag1 = 0;
        for(int x=0;x<num_students;x++){
            if(student_info[x].cur_pref<3 && student_info[x].prefs[student_info[x].cur_pref] == course->id && student_info[x].status==0){
                flag1 = 1;
                break;
            }
        }

        if(flag1==0){
            
        }
        // chooseta
        
        x = course->labs_list[x];
        printf("TA %d from lab %s has been allocated to course %s for his %dth TA ship\n", y, lab_info[x].name, course->name, lab_info[x].labs_taken[y]);
        printf("Course %s has been allocated TA %d from lab %s\n", course->name, y, lab_info[x].name);

        // allot
        int can_choose[num_students];
        int total=0;
        for(int x=0;x<num_students;x++){
            if(student_info[x].cur_pref<3 && student_info[x].prefs[student_info[x].cur_pref] == course->id && student_info[x].status==0){
                can_choose[x]=1;
                total++;
            }
            else
                can_choose[x]=0;
        }

        int t = (rand()% course->max_slots)+1;
        printf("Course %s has been allocated %d seats\n" , course->name, t);
        int final = t;
        if(final>total)
            final = total;

        int done =0;
        for(int i=0;i<num_students;i++){
            if(can_choose[x]){
                done++;
                student_info[i].status=1;
                printf("Student %d has been allocated a seat in course %s\n", i, course->name);
            }
            if(done==final)
                break;
        }
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", course->name, final, t);
        sleep(2);

        //release

        for (int i = 0; i < num_students; i++)
            if (can_choose[i] && student_info[x].status==1)
            {
                student_info[i].status=2;
                pthread_cond_signal(&student_info[i].cond);
            }

        printf("TA %d from lab %s has completed the tutorial and left the course %s\n", y, lab_info[x].name, course->name);

        int flag3=0;
        for(int i=0;i<lab_info[x].num_students;i++){
            if(lab_info[x].labs_taken[i]<lab_info->max_taships){
                flag3=1;
                break;
            }
        }
        if(!flag3){
            printf("Lab %s no longer has students available for TA ship\n", lab_info[x].name);
        }

        lab_info[x].ta_availability[y]=1;
        pthread_mutex_unlock(&lab_info[x].mutex[y]);

        
    }

    for(int x=0;x<num_students;x++){
        if((student_info[x].status==0 || student_info[x].status==1) && student_info[x].cur_pref<3 && student_info[x].prefs[student_info->cur_pref]==course->id)
        {
            student_info[x].cur_pref++;
            student_info[x].status=2;
            pthread_cond_signal(&student_info[x].cond);
        }
    }
    pthread_exit(NULL);
}
