# Q1: Alternate Course Allocation Portal

The program has been divided into two logical parts : Students wait until they join the tutorial; and courses first call TAs which then call the interested students to join the tutorial.

The `main()` function creates threads for each Student and Course, and then joins them.

Also, I have assumed that a tutorial can take place without any students. 

### 1. Students

First, the student sleeps for some time. Then, we iterate through the preferences of the students' preferences and go over them one-by-one.

First, we check to see if the course hasn't already ended. If it has, we either skip the iteration if it is not the last preference or exit the loop if it is.

The student thread is put into a conditional wait until the course thread sends a conditional signal to the student thread.

if course is removed while waiting, preference shifts to the next one, if there is one. If not, we break out of the loop.

The probability of the student enrolling in the course is then calculated., and hence if the probability is greater than a random number, we exit the loop.

### 2. Courses
Since the course threads must run till all slots of TAs are complete, we make the course thread to run in an infinite loop. We then store all the details in a lab struct.

Then we check if there is any student interested in that course and if not, the course is terminated. If there are students interested, then we allot the TA first, then students are chosen from the list of waiting ones and made to wait for 2 seconds to simulate the tutorial.

Then the students are released and the conditional signal is sent.
The TA is also released and all values are accordingly changed.

## Compilation

1. To compile,
```bash
make
```
2. To run,
```bash
./a.out
```