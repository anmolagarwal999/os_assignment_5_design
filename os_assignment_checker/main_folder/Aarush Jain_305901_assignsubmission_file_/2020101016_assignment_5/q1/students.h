#include <pthread.h>
#ifndef STUDENTS_H
#define STUDENTS_H

#define MAX_STUDENTS       1000

struct student {
    double calibre;
    int sleep_time;
    int prefs[3];
    int id;
    int cur_pref;
    int status;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

struct student *student_info;
int num_students;

pthread_t student_threads[MAX_STUDENTS];

void *student_thread(void *arg);

#endif