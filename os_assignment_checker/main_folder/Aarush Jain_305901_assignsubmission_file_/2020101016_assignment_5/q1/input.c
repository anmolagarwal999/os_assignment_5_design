#include <stdio.h>
#include "input.h"
#include "courses.h"
#include "students.h"


void takeinput()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    // printf("%d %d %d\n",num_students,num_labs,num_courses);
    //Reading course info
    for(int x = 0; x<num_courses; x++){
        scanf("%s %lf %d %d", course_info[x].name, &course_info[x].interest , &course_info[x].max_slots, &course_info[x].allowed_labs);
        for(int y=0;y<course_info[x].allowed_labs;y++){
            scanf("%d", &course_info[x].labs_list[y]);
        }
        // printf("%s %lf %d %d\n", course_info[x].name, course_info[x].interest , course_info[x].max_slots, course_info[x].allowed_labs);
    }
    //-------------------------
    //Reading student info
    for(int x=0;x<num_students;x++){
        scanf("%lf %d %d %d %d", &student_info[x].calibre, &student_info[x].prefs[0], &student_info[x].prefs[1], &student_info[x].prefs[3], &student_info[x].sleep_time);
        // printf("%lf %d %d %d %d", student_info[x].calibre, student_info[x].prefs[0], student_info[x].prefs[1], student_info[x].prefs[3], student_info[x].sleep_time);
        // printf("%d %d\n", student_info[x].id,student_info[x].sleep_time);
    }
    //-------------------------
    //Reading lab info
    for(int x=0;x<num_labs;x++){
        scanf("%s %d %d", lab_info[x].name, &lab_info[x].num_students, &lab_info[x].max_taships);
    }
    //-------------------------
}