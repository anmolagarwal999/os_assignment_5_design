#include "students.h"
#include "courses.h"
#include <unistd.h>
#include <stdio.h>
void *student_thread(void *arg)
{
    struct student *student = (struct student *)arg;
    // printf("%d %d\n",student->id,student->sleep_time);
    sleep(student->sleep_time);

    printf("Student %d has filled in preferences for course registration\n",student->id);

    while(1)
    {
        int p = student->cur_pref;
        if(student->cur_pref >= 3){
            break;
        }
        if(course_removed[student->prefs[student->cur_pref]] == 0){
            pthread_mutex_lock(&student->mutex);
            student->status = 0;
            // printf("1\n");
            while(student->status!=2)
                pthread_cond_wait(&student->cond, &student->mutex);
            pthread_mutex_unlock(&student->mutex);
            // printf("2\n");
            if(p<student->cur_pref){
                if (student->cur_pref >= 3)
                {
                    printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                    break;
                }
                else
                {
                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
                    continue;
                }
            }

            pthread_mutex_lock(&student->mutex);
            double prob = student->calibre * course_info[student->prefs[student->cur_pref]].interest;
            if(prob > 0.75){
                printf("Student %d has selected the course %s permanently\n", student->id, course_info[student->prefs[student->cur_pref]].name);
                student->cur_pref = 3;
                pthread_mutex_unlock(&student->mutex);
                break;
            }
            else{
                printf("Student %d has withdrawn from course %s\n" , student->id, course_info[student->prefs[student->cur_pref]].name);
                student->cur_pref++;
                if (student->cur_pref >= 3)
                {
                    printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                }
                else
                {
                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
                }
                pthread_mutex_unlock(&student->mutex);
            }
        }
        else{
            student->cur_pref++;
            if(student->cur_pref >= 3){
                printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                break;
            }
            printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
            continue;
        }
    }
    pthread_exit(NULL);

}