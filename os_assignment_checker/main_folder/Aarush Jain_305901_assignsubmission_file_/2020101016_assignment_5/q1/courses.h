#include <pthread.h>
#ifndef COURSES_H
#define COURSES_H

#define MAX_LABS           1000
#define MAX_COURSES        1000
#define MAX_TAS            1000
struct course {
    char name[1000];
    double interest;
    int max_slots;
    int id;
    int allowed_labs;
    int labs_list[MAX_LABS];
};

struct lab {
    char name[1000];
    int num_students;
    int max_taships;
    int ta_availability[MAX_TAS];
    int labs_taken[MAX_TAS];
    pthread_mutex_t mutex[MAX_TAS];
};

struct course* course_info;
struct lab* lab_info;
int num_courses;
int num_labs;

int course_removed[MAX_COURSES];
char *course_names[MAX_COURSES];

pthread_t course_threads[MAX_COURSES];


void *course_thread(void *arg);

#endif