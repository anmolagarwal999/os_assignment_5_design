#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "input.h"
#include "students.h"
#include "courses.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


int main() {
//     struct course* course_info;
// struct lab* lab_info;
    student_info = (malloc(sizeof(struct student)*MAX_STUDENTS));
    course_info = malloc(sizeof(struct course)*(MAX_COURSES));
    lab_info = malloc(sizeof(struct lab)*MAX_LABS);
    takeinput();

    for(int x=0;x<num_students;x++){
        pthread_mutex_init(&student_info[x].mutex, NULL);
        pthread_cond_init(&student_info[x].cond, NULL);
        student_info[x].id = x;
        student_info[x].cur_pref = 0;
    }

    for(int x=0;x<num_courses;x++){
        course_removed[x]=0;
        course_names[x] = course_info[x].name;
        course_info[x].id = x;
    }

    for(int x=0;x<num_labs;x++){
        for(int y=0;y<lab_info[x].num_students;y++){
            pthread_mutex_init(&lab_info[x].mutex[y], NULL);
            lab_info[x].ta_availability[y]=1;
            lab_info[x].labs_taken[y]=0;
        }
    }

    for(int x=0;x<num_students;x++){
        pthread_create(&student_threads[x],NULL,student_thread,&student_info[x]);
    }

    for(int x=0;x<num_courses;x++){
        pthread_create(&course_threads[x],NULL,course_thread,&course_info[x]);
    }

    for(int x=0;x<num_courses;x++){
        pthread_join(course_threads[x],NULL);
    }
    
    for(int x=0;x<num_students;x++){
        pthread_join(student_threads[x],NULL);
    }

    return 0;
}
