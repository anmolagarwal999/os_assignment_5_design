README

Assignment 5
Concurrency, Parallelism and Networking
Operating Systems and Networks, Monsoon 2021
INTERNATIONAL INSTITUTE OF INFORMATION TECHNOLOGY, HYDERABAD

By - Manav Chaudhary(2021121003)

DESCRIPTION
-----------
The objective of this assignment is to introduce oneself to basics of "multi-threaded programming", "synchronizing multiple threads using locks, condition variables and semaphores" and "basic networking concepts".

FILES INCLUDED/SUBMITTED
------------------------
The submission 2021121003_assignment_5.tar.gz has the following files - 
A) q1
	A.1) q1.c
	A.2) REPORT.pdf
B) q2
	B.1) q2.c
	B.2) REPORT.pdf
C) q3(NOT IMPLEMENTED)
	C.1) q3_server.cpp
	C.2) q3_client.cpp
	C.3) REPORT.pdf
D) Assignment_5_v2.pdf
E) README.txt

USAGE(HOW TO COMPILE AND RUN)
-----------------------------
The three programs can essentially be compiled and run in the same manner. The following example illustrates that:

For compiling the programs with the ".c" extension, open the terminal. Make sure you are in the directory pertaining to the program you want to run, i.e. directory "q1" if you want to run q1.c, so on and so forth. You should also have the "gcc compiler" installed(look into build-essential if you are on Linux). Enter the respective commands for compiling q1.c/q2.c -  
						gcc -pthread q1.c -o q1.o
						gcc -pthread q2.c -o q2.o
This creates the output file "q1.o"/"q2.o". To run the "q1.c" program, enter the following command on the terminal:
						./q1.o
Similarly, to run "q2.c", enter the following command on the terminal:
						./q2.o
The program starts executing and asks for input from the user. The input format is mentioned in "Assignment_5_v2.pdf".

For compiling "q3_server.cpp" and "q3_client.cpp", open the terminal. Make sure you have the "gcc g++ compiler" installed(look into build-essential if you are on Linux) and that you are inside the "q3" directory. To compile these programs, enter the following commands: 
						g++ -std=c++11 -pthread q3_server.cpp -o server
						g++ -std=c++11 -pthread q3_client.cpp -o client
Now to run the program, open two terminals and on each of those type, one of these commands:
						./server
						./client
The server will start running and ask for user input for the number of worker threads in the thread pool.
The client will start running and require user input for the client requests. More information about input format is available on "Assignment_5_v2.pdf".

AUTHORS AND SUPPORT
-------------------
This project has been made by Manav Chaudhary. For further support, you can contact him on the following mail ID - manav.chaudhary@research.iiit.ac.in
