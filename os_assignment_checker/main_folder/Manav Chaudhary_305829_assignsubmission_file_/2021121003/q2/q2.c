//NAME - MANAV CHAUDHARY
//ROLL NO. - 2021121003
//Question 2: The Clasico Experience

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<pthread.h>
#include<time.h>
#include<unistd.h>
#include<sys/time.h>
#include<semaphore.h>
#include<stdbool.h>
#include<string.h>
//#include<signal.h>
//#include<setjmp.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef struct person_args
{
	pthread_t ptid;
	char name[256];			//name of the person(assumed to be without spaces)
	char fan_type[2];		//H(home Fan), N(neutral fan) or A(away fan)
	int r_time;				//time of reaching the stadium
	unsigned int p_val;		//patience value(in seconds)
	int goal_limit;			//When opponent team has scored >= goals, person leaves
	bool seat_get;			//whether person got a seat
	char seat_type[2];		//which seat person got
	bool exit;				//check whether person has exited stadium(using this instead of pthread_cancel)
} p_args;

typedef struct group_args
{
	int num_of_peeps;		//number of people in the group
	pthread_t *peeps;		//thread id of each person in the group
	int id;					//group id (1-based indexing)
	
} grp_args;

typedef struct goal_args
{
	char team[2];			//team with the chance to goal (A or H)
	int goal_time;			//Time in sec elapsed since the beginning of the match after which the chance was created
	double prob;			//probability that the chance actually results in a goal
} go_args;

//global variables
p_args *person_tid;
grp_args *grp_tid;
go_args *goal_tid;

int goals_t[2];

//WAS USING mutex locks
//let's try using semaphores instead of mutex locks
pthread_mutex_t team_t_goals[2];
sem_t mutex_zone[3];

int zone[3];
//zone[0] = zone H's current availability
//zone[1] = zone A's current availability
//zone[2] = zone N's current availability

//conditional variables
pthread_cond_t cond_team_a_goals = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_team_h_goals = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_zone[3];	//might not need these anymore

int spectating_time = 0;	//This value is fixed across all people

void *person_patience_sim(void *inp)	//for simulating exit gate reach if patience is exhausted
{
	sleep(((struct person_args*)inp)->p_val);
	if(((struct person_args*)inp)->seat_get==false)	//patience value exceeded
	{
		((struct person_args*)inp)->exit = true;
		printf("%s could not get a seat\n", ((struct person_args*)inp)->name);
//		pthread_cancel(((struct person_args*)inp)->ptid);
		printf("%s is waiting for their friends at the exit\n", ((struct person_args*)inp)->name);
	}
}

void *person_spectating_sim(void *inp)	//for simulating exit gate reach if spectating time is exhausted
{
	sleep(spectating_time);
	((struct person_args*)inp)->exit = true;
	//sleep timer? perhaps
	printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET, ((struct person_args*)inp)->name, spectating_time);
	
	if((((struct person_args*)inp)->fan_type[0])=='H')
	{
		//does the job of pthread_cancel without the problems
		pthread_mutex_lock(&team_t_goals[1]);
		pthread_cond_broadcast(&cond_team_a_goals);
		pthread_mutex_unlock(&team_t_goals[1]);
		
		if(((struct person_args*)inp)->seat_type[0]=='H')	//which seat is the person vacating?
			zone[0]++;
		else
			zone[2]++;
	}	//similarly
	else if((((struct person_args*)inp)->fan_type[0])=='A')
	{
		//does the job of pthread_cancel
		pthread_mutex_lock(&team_t_goals[0]);
		pthread_cond_broadcast(&cond_team_h_goals);
		pthread_mutex_unlock(&team_t_goals[0]);
		
		zone[1]++;
	}
	else
	{
		if(((struct person_args*)inp)->seat_type[0]=='H')	//which seat is the person vacating?
			zone[0]++;
		else if(((struct person_args*)inp)->seat_type[0]=='A')
			zone[1]++;
		else
			zone[2]++;
	}
	printf(GREEN "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
}

void *zone0_sim(void *inp)	//home zone(called by home fan as well as neutral fan)
{
	sem_wait(&mutex_zone[0]);
	if(((struct person_args*)inp)->seat_type[0]!='X')	//if some other seat has already been alloted
	{
		sem_post(&mutex_zone[0]);
		return NULL;
	}
	((struct person_args*)inp)->seat_type[0] = 'H';
	if(((struct person_args*)inp)->exit==false)
	{
		zone[0]--;
		((struct person_args*)inp)->seat_get = true;
		printf(MAGENTA "%s has got a seat in zone H\n" RESET, ((struct person_args*)inp)->name);
	}
	else if(((struct person_args*)inp)->exit==true)	//person couldn't get a seat(patience exceeded) and is moving towards exit
	{
		sem_post(&mutex_zone[0]);
		return NULL;
	}
	
	if(((struct person_args*)inp)->fan_type[0] = 'N')
	{
		sleep(spectating_time);
		
		printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET, ((struct person_args*)inp)->name, spectating_time);
		if((((struct person_args*)inp)->fan_type[0])=='N')
		{
			zone[0]++;
			sem_post(&mutex_zone[0]);
		}
		printf(GREEN "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
		return NULL;
	}
	else	//home fan
	{
		pthread_t spec_tid;
		pthread_create(&spec_tid, NULL, person_spectating_sim, inp);	//thread for counting spectating time
				
		//this is for checking the goal values of opponent team
		pthread_mutex_lock(&team_t_goals[1]);	//away team goal count
				
		while(goals_t[1]<((struct person_args*)inp)->goal_limit)
		{
			if(((struct person_args*)inp)->exit==true)
			{
				break;
			}
			pthread_cond_wait(&cond_team_a_goals, &team_t_goals[1]);
		}
				
		pthread_mutex_unlock(&team_t_goals[1]);
				
		if(((struct person_args*)inp)->exit==true)
		{
			sem_post(&mutex_zone[0]);
			return NULL;
		}
				
		if(goals_t[1] >= ((struct person_args*)inp)->goal_limit)
		{
			pthread_cancel(spec_tid);	//cancel the spectating time counting thread(assuming non-boundary cases with time)
			//only possible since spectator thread has NOT acquired any lock yet
			printf(MAGENTA "%s is leaving due to the bad defensive performance of his team\n" RESET, ((struct person_args*)inp)->name);
			zone[0]++;
			sem_post(&mutex_zone[0]);	//here?
			printf(MAGENTA "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
			return NULL;
		}
	}
}

void *zone1_sim(void *inp)	//away zone(only called by neutral thread)
{
	sem_wait(&mutex_zone[1]);
	if(((struct person_args*)inp)->seat_type[0]!='X')	//if some other seat has already been alloted
	{
		sem_post(&mutex_zone[1]);
		return NULL;
	}
	((struct person_args*)inp)->seat_type[0] = 'A';
	if(((struct person_args*)inp)->exit==false)
	{
		zone[1]--;
		((struct person_args*)inp)->seat_get = true;
		printf(MAGENTA "%s has got a seat in zone A\n" RESET, ((struct person_args*)inp)->name);
	}
	else if(((struct person_args*)inp)->exit==true)	//person couldn't get a seat(patience exceeded) and is moving towards exit
	{
		sem_post(&mutex_zone[1]);
		return NULL;
	}
	
	sleep(spectating_time);
		
	printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET, ((struct person_args*)inp)->name, spectating_time);
	if((((struct person_args*)inp)->fan_type[0])=='N')
	{
		zone[1]++;
		sem_post(&mutex_zone[1]);
	}
	printf(GREEN "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
	return NULL;
}

void *zone2_sim(void *inp)	//neutral zone(called by home thread as well as neutral thread in my implementation)
{
	sem_wait(&mutex_zone[2]);
	if(((struct person_args*)inp)->seat_type[0]!='X')	//if some other seat has already been alloted
	{
		sem_post(&mutex_zone[2]);
		return NULL;
	}
	((struct person_args*)inp)->seat_type[0] = 'N';
	if(((struct person_args*)inp)->exit==false)
	{
		zone[2]--;
		((struct person_args*)inp)->seat_get = true;
		printf(MAGENTA "%s has got a seat in zone N\n" RESET, ((struct person_args*)inp)->name);
	}
	else if(((struct person_args*)inp)->exit==true)	//person couldn't get a seat(patience exceeded) and is moving towards exit
	{
		sem_post(&mutex_zone[2]);
		return NULL;
	}
	
	if(((struct person_args*)inp)->fan_type[0]=='N')
	{
		//no need for a thread that counts spectating time, since neutral fans only leave their seat once the spectating time is over, so..
		sleep(spectating_time);
			
		printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET, ((struct person_args*)inp)->name, spectating_time);
		if((((struct person_args*)inp)->fan_type[0])=='N')
		{
			zone[2]++;
			sem_post(&mutex_zone[2]);
		}
		printf(GREEN "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
		return NULL;
	}
	else	//home fan has taken neutral zone
	{
		pthread_t spec_tid;
		pthread_create(&spec_tid, NULL, person_spectating_sim, inp);	//thread for counting spectating time
				
		//this is for checking the goal values of opponent team
		pthread_mutex_lock(&team_t_goals[1]);	//away team goal count
				
		while(goals_t[1]<((struct person_args*)inp)->goal_limit)
		{
			if(((struct person_args*)inp)->exit==true)
			{
				break;
			}
			pthread_cond_wait(&cond_team_a_goals, &team_t_goals[1]);
		}
				
		pthread_mutex_unlock(&team_t_goals[1]);
				
		if(((struct person_args*)inp)->exit==true)
		{
			sem_post(&mutex_zone[2]);
			return NULL;
		}
				
		if(goals_t[1] >= ((struct person_args*)inp)->goal_limit)
		{
			pthread_cancel(spec_tid);	//cancel the spectating time counting thread(assuming non-boundary cases with time)
			//only possible since spectator thread has NOT acquired any lock yet
			printf(MAGENTA "%s is leaving due to the bad defensive performance of his team\n" RESET, ((struct person_args*)inp)->name);
			zone[2]++;
			sem_post(&mutex_zone[2]);	//here?
			printf(MAGENTA "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
			return NULL;
		}
	}
}

void *person_sim(void *inp)
{
	pthread_t temp_tid, spec_tid;
	sleep(((struct person_args*)inp)->r_time);	//reaching time
	printf(CYAN "%s has reached the stadium\n" RESET, ((struct person_args*)inp)->name);

	pthread_create(&temp_tid, NULL, person_patience_sim, inp);	//another thread that will stop the person thread if person doesn't receive seat within patience time
    
	int fan_num = -1;
	if(((struct person_args*)inp)->fan_type[0]=='H')
	{
		fan_num = 0;	//zones H and N
	}
	else if(((struct person_args*)inp)->fan_type[0]=='A')
	{
		fan_num = 1;	//zone A
	}
	else
	{
		fan_num = 2;	//zone A, H, N
	}
	
	if(fan_num==1)	//zone A only
	{
		((struct person_args*)inp)->seat_type[0] = 'A';
		sem_wait(&mutex_zone[1]);
		
		if(((struct person_args*)inp)->exit==false)
		{
			zone[1]--;
			((struct person_args*)inp)->seat_get = true;
			printf(MAGENTA "%s has got a seat in zone A\n" RESET, ((struct person_args*)inp)->name);
		}
		else if(((struct person_args*)inp)->exit==true)	//person couldn't get a seat(patience exceeded) and is moving towards exit
		{
			sem_post(&mutex_zone[1]);
			return NULL;
		}
		
		pthread_create(&spec_tid, NULL, person_spectating_sim, inp);	//thread for counting spectating time
		
		//this is for checking the goal values of opponent team
		pthread_mutex_lock(&team_t_goals[0]);	//home team goal count
		
		while(goals_t[0]<((struct person_args*)inp)->goal_limit)	//while limit is not crossed
		{
			if(((struct person_args*)inp)->exit==true)
			{
				break;
			}
			pthread_cond_wait(&cond_team_h_goals, &team_t_goals[0]);	//wait(sleep) until a goal happen or spectating limit is reached
		}
		
		pthread_mutex_unlock(&team_t_goals[0]);
		
		if(((struct person_args*)inp)->exit==true)	//if spectating time is reached
		{
			sem_post(&mutex_zone[1]);
			return NULL;
		}
		
		if(goals_t[0] >= ((struct person_args*)inp)->goal_limit)	//if goal limit is reached before spectating time
		{
			pthread_cancel(spec_tid);	//cancel the spectating time counting thread(assuming non-boundary cases with time)
			//only possible since spectator thread has NOT acquired any lock yet
			printf(MAGENTA "%s is leaving due to the bad defensive performance of his team\n" RESET, ((struct person_args*)inp)->name);
			zone[1]++;
			sem_post(&mutex_zone[1]);	//here?
			printf(MAGENTA "%s is waiting for their friends at the exit\n" RESET, ((struct person_args*)inp)->name);
			return NULL;
		}
	}
	
	else if(fan_num==0)	//HOME FAN CAN USE zones H and N
	{
		pthread_t zonetid[2];
		
		pthread_create(&zonetid[0], NULL, zone0_sim, inp);
		pthread_create(&zonetid[1], NULL, zone2_sim, inp);
		
		pthread_join(zonetid[0], NULL);
		pthread_join(zonetid[1], NULL);

	}
	else	//NEUTRAL FAN CAN TAKE SEAT IN ANY OF THE THREE ZONES
	{
		pthread_t zonetid[3];
		
		pthread_create(&zonetid[0], NULL, zone0_sim, inp);
		pthread_create(&zonetid[1], NULL, zone1_sim, inp);
		pthread_create(&zonetid[2], NULL, zone2_sim, inp);
		
		pthread_join(zonetid[0], NULL);
		pthread_join(zonetid[1], NULL);
		pthread_join(zonetid[2], NULL);
		
	}
    return NULL;
}

void *grp_sim(void *inp)
{
	int i;
	//after all people of a group have exited the stadium, the group can leave for dinner
	for(i = 0; i<((struct group_args*)inp)->num_of_peeps; i++)
    {
    	pthread_join(((struct group_args*)inp)->peeps[i], NULL);
	}
	printf(YELLOW "Group %d is leaving for dinner\n" RESET, ((struct group_args*)inp)->id);
	return NULL;
}

void *goal_sim(void *inp)
{
	sleep(((struct goal_args*)inp)->goal_time);
	char team_iden[2];
	strcpy(team_iden, ((struct goal_args*)inp)->team);	//checking which team has goal scoring chance
	int team_idx = -1;
	if(team_iden[0]=='H')
		team_idx = 0;	//home team
	else
		team_idx = 1;	//away team
	double proba = ((struct goal_args*)inp)->prob;
	double temp = rand()%100 / 100.0;
	if(proba>temp)
	{
		pthread_mutex_lock(&team_t_goals[team_idx]);	//note: might not need this
		
		goals_t[team_idx]++;
		printf(BLUE "Team %s have scored their %dth goal\n" RESET, team_iden, goals_t[team_idx]);
		
		if(team_idx==1)
		{
			pthread_cond_broadcast(&cond_team_a_goals);
		}
		else
		{
			pthread_cond_broadcast(&cond_team_h_goals);
		}
		
		pthread_mutex_unlock(&team_t_goals[team_idx]);
	}
	else
	{
		printf(BLUE "Team %s missed the chance to score their %dth goal\n" RESET, team_iden, goals_t[team_idx] + 1);
	}	
	return NULL;
}

int main()
{
	goals_t[1] = 0;			//goals scored by away team
	goals_t[0] = 0;			//goals scored by home team
	srand(time(0));
	int i,j;
	
	//initialize the mutex locks and conditional variables
	for(i=0;i<2;i++)
	{
		pthread_mutex_init(&team_t_goals[i], NULL);
	}
	
	int num_people = 1;
	person_tid = (p_args*)malloc(num_people*sizeof(p_args));
	
	scanf("%d %d %d", &zone[0], &zone[1], &zone[2]);
	
	for(i=0;i<3;i++)	//initializing variables
	{
		sem_init(&mutex_zone[i], 0, zone[i]);	//each semaphore can allow upto x people, where x is number of total seats in zone
//		pthread_mutex_init(&mutex_zone[i], NULL);
		pthread_cond_init(&cond_zone[i], NULL);
	}
	
	scanf("%d", &spectating_time);
	
	int num_groups = 0;
	scanf("%d", &num_groups);
	grp_tid = (grp_args*)malloc(num_groups*sizeof(grp_args));
	
	pthread_t grp_bonus[num_groups];
	
	num_people = 0;
	int counter = 0;		//person counter
	
	int temp[num_groups];
	
	for(i = 0;i<num_groups;i++)
	{
		grp_tid[i].id = i+1;	//1-based indexing for groups			
		scanf("%d", &temp[i]);	//number of people in group i
		grp_tid[i].num_of_peeps = temp[i];
		grp_tid[i].peeps = (pthread_t*)malloc(temp[i]*sizeof(pthread_t));
		num_people += temp[i];
		person_tid = (p_args*)realloc(person_tid, num_people*sizeof(p_args));
		for(j = 0;j<temp[i];j++)
		{
			scanf("%s", person_tid[counter].name);
			scanf("%s", person_tid[counter].fan_type);
			person_tid[counter].seat_type[0] = 'X';		//no seat alloted yet
			scanf("%d", &person_tid[counter].r_time);
			scanf("%d", &person_tid[counter].p_val);
			scanf("%d", &person_tid[counter].goal_limit);
			person_tid[counter].seat_get = false;		//initially, no one has got a seat
			person_tid[counter].exit = false;			//no one has exited yet(well, no one has entered yet)
			counter++;
		}
	}
	
	int goal_chances = 0;
	scanf("%d", &goal_chances);
	goal_tid = (go_args*)malloc(goal_chances*sizeof(go_args));
	pthread_t goal_chances_tid[goal_chances];
	for(i = 0;i<goal_chances;i++)
	{
		scanf("%s", goal_tid[i].team);
		scanf("%d", &goal_tid[i].goal_time);
		scanf("%lf", &goal_tid[i].prob);
	}
	
	//CREATING THREADS LATER for preventing invalid access
	
	for(i = 0;i<goal_chances;i++)	//THE MATCH STARTS
	{
		pthread_create(&goal_chances_tid[i],NULL,goal_sim,&goal_tid[i]);
	}
	
	counter = 0;		//person counter
	for(i = 0;i<num_groups;i++)	//PEOPLE COME AND JOIN
	{
		for(j = 0;j<temp[i];j++)
		{
			pthread_create(&grp_tid[i].peeps[j],NULL,person_sim,&person_tid[counter]);	//when pthread creating, save people thread ids in grp_tid[i].peeps[j]
			person_tid[counter].ptid = grp_tid[i].peeps[j];	//for group exiting
			counter++;
		}
		pthread_create(&grp_bonus[i],NULL,grp_sim,&grp_tid[i]);		//pthread create for grp
	}
	
	//pthread join for groups only required
	for(i = 0; i<num_groups; i++)
    {
    	pthread_join(grp_bonus[i], NULL);
	}
	
	printf(RED "ALL GROUPS HAVE EXITED\n" RESET);
	printf(CYAN "SAYONARA\n" RESET);
	return 0;
}
