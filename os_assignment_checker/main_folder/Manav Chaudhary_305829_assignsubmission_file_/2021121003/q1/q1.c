//NAME - MANAV CHAUDHARY
//ROLL NO. - 2021121003
//Question 1: An alternate course allocation Portal

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<pthread.h>
#include<time.h>
#include<unistd.h>
#include<sys/time.h>
#include<semaphore.h>
#include<stdbool.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef struct course_args
{
	int cid;				//course id (0-based indexing)
	char name[256];			//course name MAX LIMIT 256 chars
	double iq;				//interest quotient
	int course_max_slots;	//maximum slots for tutorial
	int p;					//number of labs from which the course accepts TAs
	int* lab_ids;			//IDs of corresponding labs from which course accepts TA's
	bool course_available;	//is the course currently available?
	
} c_args;

typedef struct student_args
{
	int sid;				//student id (0-based indexing)
	double cq;				//calibre quotient
	int pref_id[3];			//course IDs of the students top 3 preferred courses (in decreasing order of preference)
	int time_sec;			//time (in seconds) after which student registers on the portal.
	
} s_args;

typedef struct lab_args
{
	int lid;				//lab id (0-based indexing)
	char name[256];			//name of the lab
	int tanum;				//number of TAs in the lab
	int limit;				//the maximum number of times a TA in lab is allowed to TA a course.
	int* talim;				//array to keep track of each TA's limits
	bool* ta_avail;			//boolean array to check if TA is available or not
	
} l_args;

//globally declared array of structs
l_args *lab_tid;
c_args *cou_tid;
s_args *stu_tid;

bool *tutorial_state;			//is the tutorial for a particular course currently active
int *course_available_seats;	//number of available seats in each course with course id i
int *current_preference;		//number of students who currently prefer ith course

pthread_mutex_t *lock_seats;		//array of locks, one lock for each course
pthread_mutex_t *lock_labs;			//each lab has its own lock so that TA count is provided mutual exclusion
pthread_mutex_t *lock_tutorials;	//tutorial locking for each course

pthread_cond_t *cond_lock_seats;	//conditional variables for avoiding busy waiting on seat allocation
pthread_cond_t *cond_lock_tuts;		//cond var so that students don't "busy wait" for tutorial to complete

///////////////////////////////////

void *course_sim(void *inp)	//simulating courses using threads
{
	int total_labs = ((struct course_args*)inp)->p;		//number of assorted labs available for particular course
	int i = 0;		//this variable keeps count of labs
	int j = 0;
	int ta_found = -1;				//initially, TA has not been alloted/found for the course
	int eligibility = total_labs;	//availability of TAs for course, i.e., should the course be removed?
	
	while(eligibility>0)
	{
		ta_found = -1;
		
		for(j = 0;j<lab_tid[i].tanum; j++)	//sequentially searching for TA
		{
			eligibility += lab_tid[((struct course_args*)inp)->lab_ids[i]].talim[j];	//number of eligible TA attempts from ith lab by jth TA
			
			pthread_mutex_lock(&lock_labs[i]);	//EACH LAB HAS ITS OWN LOCK
			
			//choose the first available TA from ith lab
			if(lab_tid[((struct course_args*)inp)->lab_ids[i]].ta_avail[j]==true && lab_tid[((struct course_args*)inp)->lab_ids[i]].talim[j]>0)	//allot jth TA from ith lab to Course if TA is available and has not used all of his TAship attempts
			{
				printf(YELLOW "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" RESET, j, lab_tid[((struct course_args*)inp)->lab_ids[i]].name, ((struct course_args*)inp)->name, (lab_tid[((struct course_args*)inp)->lab_ids[i]].limit - lab_tid[((struct course_args*)inp)->lab_ids[i]].talim[j] + 1));
				lab_tid[((struct course_args*)inp)->lab_ids[i]].ta_avail[j]==false;		//TA has been alloted to course, hence is unavailable
				lab_tid[((struct course_args*)inp)->lab_ids[i]].talim[j]--;				//TA has used one of his TAships
				ta_found = j;				//chosen the jth TA from ith lab
			}
			
			pthread_mutex_unlock(&lock_labs[i]);
			///////////////////////////////////////////////
			if(ta_found>=0)
			{
				break;
			}
		}
		
		if(ta_found>=0)
		{
			tutorial_state[((struct course_args*)inp)->cid] = true;	//tutorial for course is now active
			
			pthread_mutex_lock(&lock_seats[((struct course_args*)inp)->cid]);
			
			int max_lim_slots = ((struct course_args*)inp)->course_max_slots;
			int slots = 1 + rand() % max_lim_slots;	//Decide D as a random integer between 1 and �course_max_slots�
			
			printf(MAGENTA "Course %s has been allocated %d seats\n" RESET, ((struct course_args*)inp)->name , slots);	//printf uses an internal locking mechanism afaik
			course_available_seats[((struct course_args*)inp)->cid] = slots;
			
			pthread_cond_broadcast(&cond_lock_seats[((struct course_args*)inp)->cid]);	
			//Use pthread_cond_broadcast to unblock all threads that are blocked on a single condition variable
			//they now contend for the same mutex though
			
			pthread_mutex_unlock(&lock_seats[((struct course_args*)inp)->cid]);
			
			sleep(2);	//delay for students to register for course
			//EMPTY TUTORIALS ARE ALLOWED
			while(course_available_seats[((struct course_args*)inp)->cid]>0)
			{
				sleep(5);	//delay for student registration
				if(current_preference[((struct course_args*)inp)->cid]==0)	//if nobody wants the course anymore(all students who preferred have filled)
					break;
			}
			printf(YELLOW "Tutorial for course %s is beginning\n" RESET, ((struct course_args*)inp)->name);
			
			pthread_mutex_lock(&lock_tutorials[((struct course_args*)inp)->cid]);	//TUTORIAL LOCK
			
			pthread_mutex_lock(&lock_seats[((struct course_args*)inp)->cid]);	//no more allocation allowed for seats
			
			int seats_occupied = slots - course_available_seats[((struct course_args*)inp)->cid];
			course_available_seats[((struct course_args*)inp)->cid] = 0;	//for next tutorial(reset)
			
			pthread_mutex_unlock(&lock_seats[((struct course_args*)inp)->cid]);
			
			printf(YELLOW "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, ((struct course_args*)inp)->name, seats_occupied, slots);
			
			sleep(5);	//length of a tutorial: A small delay must be added when the TA is conducting a tutorial(5-6 seconds)
			
			printf(YELLOW "TA %d from lab %s has completed the tutorial for course %s\n" RESET, ta_found, lab_tid[((struct course_args*)inp)->lab_ids[i]].name,((struct course_args*)inp)->name);
			
			tutorial_state[((struct course_args*)inp)->cid] = false;	//tutorial for course is now inactive
			
			pthread_cond_broadcast(&cond_lock_tuts[((struct course_args*)inp)->cid]);	//tutorial is over so students can look to enroll/withdraw
			
			pthread_mutex_unlock(&lock_tutorials[((struct course_args*)inp)->cid]);
			
			////////////////lab lock
			pthread_mutex_lock(&lock_labs[i]);
			
			lab_tid[((struct course_args*)inp)->lab_ids[i]].ta_avail[j]==true;	//jth TA is now available
			ta_found = -1;	//reset the TA for course
			
			pthread_mutex_unlock(&lock_labs[i]);
			///////////////////////////////////
			
			
			i = -1;		//start checking from first lab again(sequential checking, i is incremented later, hence -1 here)
			eligibility = total_labs + 1;	//reset eligibility(this is decremented in the next line, hence +1)
		}
	
		eligibility--;
		
		if(eligibility==0)
		{
			pthread_mutex_lock(&lock_seats[((struct course_args*)inp)->cid]);	//some students may be sleeping(waiting) for tutorial allocation
			
			printf(MAGENTA "Course %s does not have any TA mentors eligible and is removed from course offerings\n" RESET, ((struct course_args*)inp)->name);
			((struct course_args*)inp)->course_available = false;	//course is NOT available anymore
			
			pthread_cond_broadcast(&cond_lock_seats[((struct course_args*)inp)->cid]);
			
			pthread_mutex_unlock(&lock_seats[((struct course_args*)inp)->cid]);
			break;
		}
		
		i++;

		if(i>=total_labs)
		{
			eligibility = total_labs;
			i = 0;
			//reset and start again(FAILSAFE)
		}	
	}
    return NULL;
}

void *student_sim(void *inp)
{
	sleep(((struct student_args*)inp)->time_sec);	//registration time
	
	int stu_num = ((struct student_args*)inp)->sid;	//student id
	printf(CYAN "Student %d has filled in preferences for course registration\n" RESET, stu_num);
	
	//PREFERENCE 1
	int pref1 = ((struct student_args*)inp)->pref_id[0];
	current_preference[pref1]++;
	
	while(cou_tid[pref1].course_available==true) //check if course is available
	{
		bool allocated = false;	//initially student has not been allocated the course
		
		/////////////////
		pthread_mutex_lock(&lock_seats[pref1]);
		
		while(course_available_seats[pref1]==0)	//wait(sleep) for slots to be made available for the course tutorial 
		{
			if(cou_tid[pref1].course_available==false)	//this means course is not available anymore, so student changes preference
				break;
			pthread_cond_wait(&cond_lock_seats[pref1], &lock_seats[pref1]);
		}
		
		//note to self: if course is removed from offerings, then there is no need to modify value for current_preference
			
		if(course_available_seats[pref1]>0)
		{
			course_available_seats[pref1]--;
			printf(CYAN "Student %d has been allocated a seat in course %s\n" RESET, stu_num, cou_tid[pref1].name);
			allocated = true;
			current_preference[pref1]--;
		}
		
		pthread_mutex_unlock(&lock_seats[pref1]);
		//////////////////
		
		if(allocated==true)
		{
			//wait for tutorial to start
			pthread_mutex_lock(&lock_tutorials[pref1]);
			
			while(tutorial_state[pref1]==true)
			{
				pthread_cond_wait(&cond_lock_tuts[pref1], &lock_tutorials[pref1]); //thread sleeps until tutorial completes
			}
				
			pthread_mutex_unlock(&lock_tutorials[pref1]);
			//tutorial over
			
			//accept or reject
			double prob = ((struct student_args*)inp)->cq * cou_tid[pref1].iq;
			double temp = rand()%100 / 100.0;
			if(prob>temp)
			{
				//ACCEPT HERE
				printf(CYAN "Student %d has selected the course %s permanently\n" RESET, stu_num, cou_tid[pref1].name);
				return NULL;	//exit thread
			}
			else
			{
				//REJECT HERE
				printf(CYAN "Student %d has withdrawn from course %s\n" RESET, stu_num, cou_tid[pref1].name);
				break;
			}
			
		}
		
		//otherwise if course is available, keep looking for a tutorial	
	}
	
	int pref2 = ((struct student_args*)inp)->pref_id[1];
	printf(CYAN "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" RESET, stu_num, cou_tid[pref1].name, cou_tid[pref2].name);
	
	//Similarly for other preferences
	//PREFERENCE 2
	
	current_preference[pref2]++;
	while(cou_tid[pref2].course_available==true) 
	{
		bool allocated = false;
		
		pthread_mutex_lock(&lock_seats[pref2]);
		
		while(course_available_seats[pref2]==0)
		{
			if(cou_tid[pref2].course_available==false)
				break;
			pthread_cond_wait(&cond_lock_seats[pref2], &lock_seats[pref2]);
		}
			
		if(course_available_seats[pref2]>0)
		{
			course_available_seats[pref2]--;
			printf(CYAN "Student %d has been allocated a seat in course %s\n" CYAN, stu_num, cou_tid[pref2].name);
			allocated = true;
			current_preference[pref2]--;
		}
		
		pthread_mutex_unlock(&lock_seats[pref2]);
		
		
		if(allocated==true)
		{
			pthread_mutex_lock(&lock_tutorials[pref2]);
			
			while(tutorial_state[pref2]==true)
			{
				pthread_cond_wait(&cond_lock_tuts[pref2], &lock_tutorials[pref2]); //thread sleeps until tutorial completes
			}
				
			pthread_mutex_unlock(&lock_tutorials[pref2]);
			
			//tutorial over
			
			//accept or reject
			double prob = ((struct student_args*)inp)->cq * cou_tid[pref2].iq;
			double temp = rand()%100 / 100.0;
			if(prob>temp)
			{
				//ACCEPT HERE
				printf(CYAN "Student %d has selected the course %s permanently\n" RESET, stu_num, cou_tid[pref2].name);
				return NULL;
			}
			else
			{
				//REJECT HERE
				printf(CYAN "Student %d has withdrawn from course %s\n" RESET, stu_num, cou_tid[pref2].name);
				break;
			}
			
		}
	}
	
	int pref3 = ((struct student_args*)inp)->pref_id[2];
	printf(CYAN "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" RESET, stu_num, cou_tid[pref2].name, cou_tid[pref3].name);
	
	//PREFERENCE 3
	
	current_preference[pref3]++;
	while(cou_tid[pref3].course_available==true)
	{
		bool allocated = false;
		
		pthread_mutex_lock(&lock_seats[pref3]);
		
		while(course_available_seats[pref3]==0)
		{
			if(cou_tid[pref3].course_available==false)
				break;
			pthread_cond_wait(&cond_lock_seats[pref3], &lock_seats[pref3]);
		}
			
		if(course_available_seats[pref3]>0)
		{
			course_available_seats[pref3]--;
			printf(CYAN "Student %d has been allocated a seat in course %s\n" RESET, stu_num, cou_tid[pref3].name);
			allocated = true;
			current_preference[pref3]--;
		}
		
		pthread_mutex_unlock(&lock_seats[pref3]);
		//////////////////
		
		if(allocated==true)
		{
			pthread_mutex_lock(&lock_tutorials[pref3]);
			
			while(tutorial_state[pref3]==true)
				pthread_cond_wait(&cond_lock_tuts[pref3], &lock_tutorials[pref3]); //thread sleeps until tutorial completes
			
			pthread_mutex_unlock(&lock_tutorials[pref3]);
			//tutorial over
			
			//accept or reject
			double prob = ((struct student_args*)inp)->cq * cou_tid[pref3].iq;
			double temp = rand()%100 / 100.0;
			if(prob>temp)
			{
				//ACCEPT HERE
				printf(CYAN "Student %d has selected the course %s permanently\n" RESET, stu_num, cou_tid[pref3].name);
				return NULL;
			}
			else
			{
				//REJECT HERE
				printf(CYAN "Student %d has withdrawn from course %s\n" RESET, stu_num, cou_tid[pref3].name);
				break;
			}
			
		}
	}
	
	printf(CYAN "Student %d could not get any of his preferred courses\n" RESET, stu_num);
	
    return NULL;
}

int main()
{
	srand(time(0));
	
	//The first 3 lines contain the number of students, number of labs and number of courses respectively
	int num_stu = 0;
	int num_labs = 0;
	int num_courses = 0;
	scanf("%d", &num_stu);
	scanf("%d", &num_labs);
	scanf("%d", &num_courses);
	
	//allocating memory to globally declared array of structures
	lab_tid = (l_args*)malloc(num_labs*sizeof(l_args));
	cou_tid = (c_args*)malloc(num_courses*sizeof(c_args));
	stu_tid = (s_args*)malloc(num_stu*sizeof(s_args));
	
	//additional variables
	course_available_seats = (int*)malloc(num_courses*sizeof(int));
	tutorial_state = (bool*)malloc(num_courses*sizeof(bool));
	current_preference = (int*)malloc(num_courses*sizeof(int));
	
	//allocating size to locks and conditional variables
	lock_seats = (pthread_mutex_t*)malloc(num_courses*sizeof(pthread_mutex_t));
	lock_tutorials = (pthread_mutex_t*)malloc(num_courses*sizeof(pthread_mutex_t));
	lock_labs = (pthread_mutex_t*)malloc(num_labs*sizeof(pthread_mutex_t));
	
	cond_lock_seats = (pthread_cond_t*)malloc(num_courses*sizeof(pthread_cond_t));
	cond_lock_tuts = (pthread_cond_t*)malloc(num_courses*sizeof(pthread_cond_t));
	
	int i = 0;
	
	for(i = 0;i<num_labs;i++)
	{
		pthread_mutex_init(&lock_labs[i], NULL);	//initializing
	}
	
	for(i=0;i<num_courses;i++)
	{
		course_available_seats[i] = 0;	//initially 0 seats are available for each course
		current_preference[i] = 0;		//number of students that currently prefer ith course initially 0
		tutorial_state[i] = false;		//no tutorial has started yet
		
		pthread_mutex_init(&lock_seats[i], NULL);
		pthread_mutex_init(&lock_tutorials[i], NULL);
		pthread_cond_init(&cond_lock_seats[i], NULL);
		pthread_cond_init(&cond_lock_tuts[i], NULL);
	}

	//for keeping track of THREADS ID's
	//pthread_t lab_tid[num_labs];	//no need to simulate labs using threads
	pthread_t c_tid[num_courses];	
	pthread_t s_tid[num_stu];
	
	for(i = 0;i<num_courses;i++)
	{
        cou_tid[i].cid = i;
        scanf("%s", cou_tid[i].name);	//name of the course: since input is space-separated, courses are assumed to have names without spaces
        scanf("%lf", &cou_tid[i].iq);	//the second token contains the �interest quotient� of the course.
        scanf("%d", &cou_tid[i].course_max_slots);	//The third token is �course_max_slots�. 
        scanf("%d", &cou_tid[i].p);	//The fourth token is the number of labs from which the course accepts TAs (let�s call this�p�).
        int n = cou_tid[i].p;
        cou_tid[i].lab_ids = (int*)malloc(n*sizeof(int));
        cou_tid[i].course_available = true;	//initially, all courses are available
        int j = 0;
        for(j = 0; j<n; j++)	//Then, there are �p� numbers which are the IDs of the corresponding labs (0 indexed).
        {
        	scanf("%d", &cou_tid[i].lab_ids[j]);
		}
	}
	
	for(i = 0;i<num_stu;i++)
	{
		stu_tid[i].sid = i;	//0-based indexing for students
        scanf("%lf", &stu_tid[i].cq);	//the first token contains �the calibre quotient of the student�,
        scanf("%d %d %d", &stu_tid[i].pref_id[0], &stu_tid[i].pref_id[1], &stu_tid[i].pref_id[2]);	//the next 3 tokens are the course IDs of the student�s preferred courses (in decreasing order of preference)
        scanf("%d", &stu_tid[i].time_sec);	//the last token is the time (in seconds) after which he registers on the portal. 
	}
	
	for(i = 0;i<num_labs;i++)			//labs are simulated WITHOUT using threads
	{
		lab_tid[i].lid = i;				//lab id is 0-indexed
        scanf("%s", lab_tid[i].name);	//labs are assumed to have names without spaces
        scanf("%d", &lab_tid[i].tanum);	//number of TAs in the lab
        scanf("%d", &lab_tid[i].limit);	//the maximum number of times a TA in the said lab is allowed to TA a course over the entire simulation
        int n = lab_tid[i].tanum;
        
        lab_tid[i].talim = (int*)malloc(n*sizeof(int));
        lab_tid[i].ta_avail = (bool*)malloc(n*sizeof(bool));
        int j = 0;
        for(j = 0; j<n; j++)
        {
        	lab_tid[i].talim[j] = lab_tid[i].limit;	//no member of lab has used any of their available TAships
        	lab_tid[i].ta_avail[j] = true;			//all TAs are initially available
		}
	}
	
	//Thread creation is done after data for the entities(including labs) has been put in, to avoid invalid access
	for(i = 0;i<num_courses;i++)
	{
		pthread_create(&c_tid[i],NULL,course_sim,&cou_tid[i]); //THREAD CREATION for courses
	}
	for(i = 0; i<num_stu; i++)
	{
		pthread_create(&s_tid[i],NULL,student_sim,&stu_tid[i]); //THREAD CREATION for students
	}


    for(i = 0; i<num_stu; i++)	//after all students have exited the simulation, you can exit it (so maybe remove courses?)
    {
    	pthread_join(s_tid[i], NULL);
	}
	
	printf(RED "\nALL STUDENTS HAVE EITHER FINALISED THEIR COURSE OR COULDN'T GET ANY OF THEIR CHOICE\n" RESET);
	
	//maybe this is not necessary, considering there are lock/unlock issues with pthread_cancel
//	for (i = 0; i<num_courses; i++)	//stop any lurking course threads
//    {
//        pthread_cancel(c_tid[i]);
//    }
	
	printf(CYAN "SAYONARA!!\n" RESET);
	
	return 0;
}

