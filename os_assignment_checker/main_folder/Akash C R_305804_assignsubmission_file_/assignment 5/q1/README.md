> ## README FILE FOR QUESTION 1

>## HOW TO RUN

- My q1 folder has many files and hence I have created a makefile.
 
- So  just type the following commands.

 1. $ make
 2. $ ./a.out < input

- A demo input file has also been provided.

>## <u> ASSUMPTIONS
- Incase of TA allocation, I am allocating TA to the course only if there is atleast one student who is interested in the course. And if no student is interested in the course then no TA is alloted.

- I am assuming all the names involved in the program are less than 100 character long.

- I am assuming none of names contain spaces since names are being read by scanf spaces will create problems

- I am assuming no course will accept TAs from more than 50 labs.

- Most of the integer variables are int and not long long int, so program expects small inputs.

- I am assuming all labs will have less than 100 TAs under it.

- I am assuming all the tutorials will be held for 2 seconds.

>## <u> LOGIC

- Since time_elapsed has to run continously a seperate thread has been created for it but no thread_join is used for it since it runs infinitely and we dont require it to finish its execution to close the program.

- Threads for Students and Courses have been created to run them parallely.

- Each Student is created as a thread so that process for all students go parallely.

- Since, registration of student is time based, simply using threads might lead to the possibility that one of the student thread started very lately and miss out the time at which it had to executed or get wrong time due to the race condition, so I have initiated all the threads before the time thread. I have used a conditional variable with the time_elapsed variable so as to restirct possible race conditions. So, all student threads will be up for the time to increment and will receive signal to check the condition on each increment of time using signal broadcast.

- I have created seperate threads for each and every student and also for each and every course.

>#### Working of Student Thread;

- There is a student struct, which keeps the status of student, and using the value of status different things are being carried out.
- The status of the students can be changed by Course threads, so in order to avoid unintended status changes there is a mutex lock in each student struct which protects the respective student's status and helps to carry out the desired thing as per the status.
- Whenever the student finishes the tute, he has a choice whether to choose the course or not.
- Inorder to determine whether the student takes the course or drop it, I have multiplied the probability by 10000 then generated a random number between 0 to 10000 and based on that random number student will decide which path to take.

>#### Working of Course Thread;

- First it will check how many students are interested in the course.
- If the course has atleast one student who is interested then course will search through the labs to get a TA for the course. Each lab has its own mutex lock preventing multiple courses to access the same lab at a time.
- After the TA is allocated tutorial will be held for some number of students (less than max_slot) and the students status is updated accordingly to take their decision.
- If no TA is found for the course then the course is withdrawn.

>#### Implementation of Bonus;

- Inside each lab struct, there is a variable called present_ta which stores the id of the ta to be selected next. And this variable is increased accordingly as any TA is chosen and it goes on till the number of tas in the lab and then back to 0. So, basically we are assigning TAs in order of their id thereby assuring that each TA gets equal priority while he/she is being chosen for the course TAship.