#include "courses.h"

void * execute_course(void * c_element)
{
	course * c_arr = (course *)c_element;
	int markem[num_students];	//to mark the id of students as 1 for those who took the course.
	for(int i = 0; i < num_students; i++)
		markem[i] = 0;

	
	while(1)
	{
		// checking how many students have got their course preference.
		for(int i = 0; i < num_students; i++)
		{
			//pthread_mutex_lock(&studentptr[i].student_status_lock);
			if(studentptr[i].current_pref == c_arr->id && markem[studentptr[i].id] == 0 )
			{
				if(studentptr[i].status == S_WFFC || studentptr[i].status == S_WFSC || studentptr[i].status == S_WFTC)
				{
					markem[studentptr[i].id] = 1;
					c_arr->present_students++;
				}
			}
			//pthread_mutex_unlock(&studentptr[i].student_status_lock);
		}
		int tafound = 0;
		int slots_alloted, slots_filled;
		if(c_arr->present_students > 0 && c_arr->status == C_WFTA)//allocate TA.
		{

			for(int i = 0; i < c_arr->n_labs; i++)
			{
				int present_lab = c_arr->lab_id[i];
				if(tafound)break;
				if(labptr[present_lab].status == L_OPEN)
				{
					pthread_mutex_lock(&labptr[present_lab].ta_lock);
					char * name = labptr[present_lab].name;

					if(labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship >= labptr[present_lab].limit)
					{
						labptr[present_lab].status = L_CLOSE;
						printf(TA_Color);
						printf("Lab %s no longer has students available for TA ship\n", name);
						printf(RESET);
					}
					else
					{
						tafound++;
						c_arr->ta_lab = present_lab;
						c_arr->ta_id = labptr[present_lab].present_ta;
						labptr[present_lab].tas[labptr[present_lab].present_ta].status = TA_WORKING;
						labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship++;
						printf(TA_Color);
						printf("TA %d from lab %s has been allocated to course %s for TAship %d\n", c_arr->ta_id, name, c_arr->name,labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship);
						printf(RESET);
						labptr[present_lab].present_ta = (labptr[present_lab].present_ta + 1) % (labptr[present_lab].members);
						c_arr->status = C_WFS;
						//pthread_mutex_unlock(&labptr[present_lab].ta_lock);

					}
					pthread_mutex_unlock(&labptr[present_lab].ta_lock);

				}
				else continue; //go search in the next lab.
			}

			if(tafound == 0)// withdraw the course;
			{
				printf(Course_Color);
				printf("Course %s does not have any TA's eligible and is removed from course offerings\n", c_arr->name);
				printf(RESET);
				c_arr->status = C_BYE;
			}

		}
		else if(c_arr->status == C_WFS )
		{
			slots_alloted = rand()%(c_arr->max_slots);
			slots_alloted++;
			slots_filled = 0;
			printf(Course_Color);
			printf("Course %s has been allocated %d seats\n", c_arr->name, slots_alloted);
			printf(RESET);
			//change student state to attending tute;

			for(int i = 0; i < num_students && slots_filled < slots_alloted ; i++)
			{
				if(markem[i] == 1)
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					slots_filled++;
					if(studentptr[i].status ==  S_WFFC)
						studentptr[i].status = S_ATFFC;
					else if(studentptr[i].status == S_WFSC)
						studentptr[i].status = S_ATFSC;
					else if(studentptr[i].status == S_WFTC)
						studentptr[i].status = S_ATFTC;

					markem[i] = 2;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&(studentptr[i].student_status_cond));
				}
			}
			c_arr->status = C_TUTE;
			c_arr->present_students -= slots_filled;

		}
		else if(c_arr->status == C_TUTE)
		{
			//change student state to finished tute.	;
			printf(Course_Color);
			printf("Tutorial has started for Course %s with %d seats filled out of %d\n", c_arr->name, slots_filled, slots_alloted);
			printf(RESET);
			sleep(2); //Assuming tute goes on for 2 seconds.
			//Now tute is completed.
			printf(Course_Color);
			printf("TA %d from lab %s has completed the tutorial and left the course %s\n", c_arr->ta_id, labptr[c_arr->ta_lab].name, c_arr->name );
			printf(RESET);
			tafound = 0;
			c_arr->status = C_WFTA;

			for(int i = 0; i < num_students; i++)
			{
				if(markem[i] == 2)// 2 means they are attending tute.
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					if(studentptr[i].status == S_ATFFC)
						studentptr[i].status = S_FTFC;
					else if(studentptr[i].status == S_ATFSC)
						studentptr[i].status = S_FTSC;
					else if(studentptr[i].status == S_ATFTC)
						studentptr[i].status = S_FTTC;
					markem[i] = 0;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&(studentptr[i].student_status_cond));
				}
			}

		}
		else if(c_arr->status == C_BYE)
		{
			for(int i = 0; i < num_students; i++)
			{
				if(markem[i] == 1)//1 means waiting
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					if(studentptr[i].status ==  S_WFFC)
						studentptr[i].status = S_WFSC;
					else if(studentptr[i].status == S_WFSC)
						studentptr[i].status = S_WFTC;
					else if(studentptr[i].status == S_WFTC)
						studentptr[i].status = S_BYE;

					markem[i] = 0;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&studentptr[i].student_status_cond);
				}
			}
			//break;//removed break bcoz after this course thread ends some students choose this course and ends up in deadlock;
			//so without break it will let the students go on to next course.
		}
		usleep(5000);

	}// while(1) loop ends here.

}
