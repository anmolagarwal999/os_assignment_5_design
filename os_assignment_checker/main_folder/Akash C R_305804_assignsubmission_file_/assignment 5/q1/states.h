#ifndef _STATES
#define _STATES

// STUDENT STATES ARE AS FOLLOWS

#define S_NR 	0		// Not registered yet
#define S_WFFC 	1  		//Waiting For First Course
#define S_ATFFC 2		//Attending Tute For First Course
#define S_WFSC 	3
#define S_ATFSC 4
#define S_WFTC 	5
#define S_ATFTC 6
#define S_SLCTD 7		//Selected
#define S_BYE 	8		//Did not like any course bechara....
#define S_FTFC 	9
#define S_FTSC 	10
#define S_FTTC 	11		// It means student finished attending that tute and yet to take decision.


// COURSE STATES ARE AS FOLLOWS

#define C_WFTA 	0 		//Waiting For TA
#define C_WFS 	1		//Waiting For Slots
#define	C_TUTE 	2		// Tutes going on
#define C_BYE	3 		// Course withdrawn 

//  LAB STATES ARE AS FOLLOWS

#define L_OPEN	0		//has tas to be selected
#define L_CLOSE	1		// No tas available


// STATES FOR TA

#define TA_FREE 	0		// it means he is not ta for any course at the moment
#define TA_WORKING 	1		// it means he is a ta for any course.

#endif