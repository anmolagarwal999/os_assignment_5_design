#include <stdio.h>
#include <unistd.h> 	//for the sleep function
#include <stdlib.h> 	// for the srand() and rand() function.
#include <time.h> 		// for the time(NULL) thing
#include <pthread.h> 	// this is for the threads.

#include "states.h"		// possible states of different entities
#include "colors.h"		// color codes for different colors
#include "datatypes.h"	// definition of different datatypes used

#include "student.h"	//all the functions related to student threads.
#include "courses.h"
//global variables.
extern int num_students, num_labs, num_courses;
extern int time_elapsed;

pthread_mutex_t timelock;
pthread_cond_t timecond;

//the following function is for time_thread;
void * time_tracker()
{
	time_elapsed = 0;
	sleep(1);
	while(1)
	{
		pthread_mutex_lock(&timelock);
		time_elapsed++;
		printf(RED "Time elapsed is -- %d seconds.............\n" RESET, time_elapsed);
		pthread_mutex_unlock(&timelock);
		pthread_cond_broadcast(&timecond);
		sleep(1);
	}
}

int main()
{

// input part starts here
//****************************************************************************************************
	srand(time(NULL));
	extern int num_students, num_labs, num_courses;
	extern course * courseptr;
	extern student * studentptr;
	extern lab * labptr;

	printf(RED);
	scanf( "%d %d %d" , &num_students, &num_labs, &num_courses); 			//taking input for the numbers
	printf(RESET);

	//declaring respective struct arrays;

	student arr_students[num_students];
	course arr_courses[num_courses];
	lab arr_labs[num_labs];

	courseptr = arr_courses;
	studentptr = arr_students;
	labptr = arr_labs;

	// input for the courses part

	printf(GREEN);
	for(int i = 0; i < num_courses; i++)
	{
		scanf( "%s" , arr_courses[i].name);
		scanf( "%lf %d %d" , &arr_courses[i].interest, &arr_courses[i].max_slots, &arr_courses[i].n_labs);
		arr_courses[i].id = i;
		arr_courses[i].present_students = 0;
		arr_courses[i].status = C_WFTA;

		for(int j = 0; j < arr_courses[i].n_labs ; j++)
			scanf( "%d" , &arr_courses[i].lab_id[j]);
	}
	printf(RESET);


	// input for the student part

	printf(BLUE);
	for(int i = 0; i < num_students; i++)
	{
		scanf( "%lf %d %d %d" , &arr_students[i].calibre, &arr_students[i].pref[0], &arr_students[i].pref[1], &arr_students[i].pref[2]);
		scanf( "%d" , &arr_students[i].time);

		arr_students[i].id = i;
		arr_students[i].status = S_NR;
	}
	printf(RESET);


	// input for the labs part

	printf(MAGENTA);
	for(int i = 0; i < num_labs; i++)
	{
		scanf( "%s" , arr_labs[i].name);
		scanf( "%d %d" , &arr_labs[i].members, &arr_labs[i].limit);

		arr_labs[i].status = L_OPEN;
		arr_labs[i].present_ta = 0;

		//allocating TA status for the particular lab.
		for(int j = 0; j < arr_labs[i].members ; j++)
		{
			arr_labs[i].tas[j].id = j;
			arr_labs[i].tas[j].status = TA_FREE;
			arr_labs[i].tas[j].no_taship = 0;
		}

	}
	printf(RESET);
//input part ends here
//************************************************************************************************
// processing part starts here.

	pthread_mutex_init(&timelock, NULL);
	pthread_cond_init(&timecond, NULL);

	for(int i = 0; i < num_labs; i++)
	{
		pthread_mutex_init(&arr_labs[i].ta_lock, NULL);
	}

	// We have to create thread of each student and course since they have to work in parallel.
	pthread_t student_threads[num_students];

	for(int i = 0; i < num_students; i++)
		if(pthread_create(&student_threads[i], NULL, &execute_student, (void *)&arr_students[i]) != 0)
			perror("Failed to create student thread");

	// We have to create thread of each courses.
	pthread_t course_threads[num_courses];

	for(int i = 0; i < num_courses; i++)
		if(pthread_create(&course_threads[i], NULL, &execute_course, (void *)&arr_courses[i]) != 0)
			perror("Failed to create courses thread");

	//Time thread runs infinitely so we wont wait for this thread execution to end.
	// and hence no pthread_join is used for this.
	pthread_t time_thread;
	printf(RED "Time elapsed is -- %d seconds.............\n" RESET, time_elapsed);
	if(pthread_create(&time_thread, NULL, &time_tracker, NULL) != 0)
		perror("Failed to create time thread");


	//Joining of threads here to ensure that they finished the execution.
	for(int i = 0; i < num_students; i++)
		if(pthread_join(student_threads[i], NULL) != 0 )
			perror("Failed to join student thread");

	pthread_mutex_destroy(&timelock);
	pthread_cond_destroy(&timecond);

	for(int i = 0; i < num_labs; i++)
	{
		pthread_mutex_destroy(&arr_labs[i].ta_lock);
	}

	return 0;
}
