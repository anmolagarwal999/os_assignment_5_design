#ifndef _DATATYPES
#define _DATATYPES
#include <pthread.h>

typedef struct course course;
typedef struct student student;
typedef struct lab lab;
typedef struct ta ta;

//These are used to point to the respective arrays.
course * courseptr;
student * studentptr;
lab * labptr;

//global variables.
int num_students, num_labs, num_courses;
int time_elapsed;

struct course
{	
	int id;					// id of the course.	( 0 based indexing )
	char name[100];			// name of the course
	double interest; 		//interest quotient lies between 0 and 1
	int max_slots; 			// course max_slots
	int n_labs;				// number of labs from which course accepts TAs.
	int lab_id[50];			// ids of the labs from which course accepts TAs.

	//status variable is used to store the status of the course
	int status;
	// course chooses a TA only if present_students are greater than zero.
	int present_students; // this is to know how many students are presently interested in the course.

	int ta_lab;
	int ta_id;
};


struct student
{
	int id;					// student id ( 0 based indexing )
	double calibre;			// calibre quotient of student
	int pref[3];			// Course if of student preference. Pref 1 is 1st priority
	int time;				// time at which he/she registers for the course.
	int current_pref;		// waiting for which course, if attending tute then -1.
	int status;				// status variable is to store the status of the student.
	pthread_mutex_t student_status_lock; // this is reqd as courses can change status of student structs.
	pthread_cond_t student_status_cond;
};

struct ta
{
	int id;				// id of the ta under each lab
	int no_taship;		// no of ta ships done.
	int status; 		//presently free or doing a ta work


};

struct lab
{
	int id;					// lab is ( 0 based indexing )
	char name[100];			// name of the lab
	int members; 			// no. of TAs in the lab
	int limit;				// no. of times a member of the lab can become a TA

	int status;
	ta tas[100];			// stores the details of the TAs under this lab.
	int present_ta;			
	// In order to execute the bonus point I am allocating TAs in the order of their ids, so present_ta keeps the id
	// of the TA who can be hired.
	pthread_mutex_t ta_lock; // to avoid multiple courses choosing same TA at the same moment.
	
};



#endif