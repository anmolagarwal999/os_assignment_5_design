#include "student.h"

extern course * courseptr;
extern student * studentptr;
extern lab * labptr;

extern pthread_mutex_t timelock;
extern pthread_cond_t timecond;

extern int time_elapsed;
extern int num_students;

//the following function is for the student_threads

void * execute_student(void * s_element)
{

//***************************************************************
	//course registration.
	pthread_mutex_lock(&timelock);
	student * s_arr = (student *)s_element;

	while(time_elapsed < s_arr->time)
	{
		pthread_cond_wait(&timecond, &timelock);
	}
	pthread_mutex_unlock(&timelock);
	//course registration done here
//********************************************************************
	pthread_mutex_init(&(s_arr->student_status_lock), NULL);
	pthread_cond_init(&(s_arr->student_status_cond), NULL);

	printf(Student_Color "Student %d has filled in preferences for course registration\n" RESET, s_arr->id);

	//pthread_mutex_lock(&(s_arr->student_status_lock));

	//pthread_mutex_unlock(&(s_arr->student_status_lock));
	s_arr->current_pref = s_arr->pref[0];
	s_arr->status = S_WFFC;
	int selected = -1;
	while(1)
	{
		pthread_mutex_lock(&(s_arr->student_status_lock));
		if(s_arr->status == S_WFFC)
		{
			char * name = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has his current preference as %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFFC)
		{
			char * name = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTFC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[0])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[0])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[0];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_WFSC;
				s_arr->current_pref = s_arr->pref[1];
			}
		}
		else if(s_arr->status == S_WFSC)
		{
			char * pname = &(courseptr + s_arr->pref[1])->name[0];
			char * bname = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has changed current preference from %s to %s\n", s_arr->id, bname, pname);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFSC)
		{
			char * name = &(courseptr + s_arr->pref[1])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTSC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[1])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[1])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[1];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_WFTC;
				s_arr->current_pref = s_arr->pref[2];
			}
		}
		else if(s_arr->status == S_WFTC)
		{
			char * pname = &(courseptr + s_arr->pref[2])->name[0];
			char * bname = &(courseptr + s_arr->pref[1])->name[0];
			printf(Student_Color);
			printf("Student %d has changed current preference from %s to %s\n", s_arr->id, bname, pname);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFTC)
		{
			char * name = &(courseptr + s_arr->pref[2])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTTC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[2])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[2])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[2];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_BYE;
				s_arr->current_pref = -1;
			}
		}
		else if(s_arr->status == S_SLCTD)
		{
			char * name = &(courseptr + selected)->name[0];
			printf(Student_Color);
			printf("Student %d has selected course %s permanently\n", s_arr->id, name);
			printf(RESET);
			//pthread_mutex_unlock(&(s_arr->student_status_lock));
			break;
		}
		else if(s_arr->status == S_BYE)
		{
			printf(Student_Color "Student %d couldn't get any of his preferred courses\n" RESET, s_arr->id);
			//pthread_mutex_unlock(&(s_arr->student_status_lock));
			break;
		}
		pthread_mutex_unlock(&(s_arr->student_status_lock));
		usleep(5000);
	}

	pthread_mutex_destroy(&(s_arr->student_status_lock));
	pthread_cond_destroy(&(s_arr->student_status_cond));

}
