># README FILE FOR QUESTION OF OSN ASSIGNMENT 5

>### HOW TO RUN....?
- I have created a makefile since I have more than one code files for this question
- Just type the following commands in the terminal 

1. $ make
2. $ ./a.out < input

- A demo input file has been provided with this.

>### ASSUMPTIONS

- I am assuming none of the names are more than 100 characters long.

- I have assumed chances are inputted in increasing order of times at which they were created.

- I am assuming number of people to participate in this simulation is less than 200.

- I am exiting the simulation as soon as all the groups of people have left the football match.

>### LOGIC

- Since people have to get their status updated parallely I have created threads of people.
- There is one thread for seat allocation.
- There is a thread for time which runs forever.

- First the people threads have initiated and they will all wait for the time_condition variable to tick.
- Each time the timer value has increased a broadcast signal is sent and the people thread will print the ground reaching message if the timer value is equal to their arrival time.
- In each iteration of timer variable, I am checking whether any chance of goals in that particular time and global goal variables for both the teams have been initialised to keep track the total number of goals of each team.
- Inside the time thread only I am traversing all the people structs and checking whether their patience limit has reached if they are still not assigned with a seat and changing the people status accordingly.
- mutex has been used to avoid the unnecessary change of people status variable.
- Inside the timer thread only match spectating time of each user is being tracked.
- Now once the timer thread complete its iteration , people thread gets to print the appropriate status message.
- In the seat allocation thread, I have created a global variable which keeps track of the total seats filled protected by a mutex and if its filled completely it stays in wait condition and this condition will be signaled whenever any people leave from their seats.
- As soon as the wait signal is triggered, we will traverse the people arrays to see who are all waiting and if waiting then allocating the proper zone seat if available and incrementing seats_filled variable accordingly.


>### BONUS IMPLEMENTATION.

- Whenever people exit from the seat they are assigned wait seat.
- For each gropu I have pthread_barrier with barrier limit set to number of people in that group
- If people status is wait they will be waiting near corresponding barrier guided by their group id.
- If all the threads arrive at barrier then group leaving msg is printed.
- To avoid multiple group exit msgs after the barrier I have used mutex_t after barricade which is try_locked.
- So only one will succesfully lock it and others will simply pass on since its try_lock and not lock.
- Whichever thread succeeds to lock will print the group exit msg on behalf of its group.

