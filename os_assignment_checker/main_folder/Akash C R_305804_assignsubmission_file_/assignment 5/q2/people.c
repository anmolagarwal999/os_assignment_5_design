#include "people.h"


extern pthread_mutex_t time_lock;
extern pthread_cond_t time_cond;

void * execute_people(void * people_element)
{	
	people * peeps = (people *)people_element;

	pthread_mutex_init(&peeps->s_lock, NULL);
	pthread_cond_init(&peeps->s_cond, NULL);

	pthread_mutex_lock(&time_lock);
	while(timer < peeps->reaching_time)
	{
		pthread_cond_wait(&time_cond, &time_lock);
	}
	//pthread_mutex_unlock(&time);

	//pthread_mutex_lock(&peeps->s_lock);
	peeps->status = P_RCHD;
	//pthread_mutex_unlock(&peeps->s_lock);

	printf(Reached);
	printf("Person %s has reached the stadium\n", peeps->name);
	printf(RESET);

	pthread_mutex_unlock(&time_lock);

	int x = 1;
	while(1)
	{
		pthread_mutex_lock(&peeps->s_lock);

		if(peeps->status == P_RCHD)
		{
			pthread_cond_wait(&peeps->s_cond, &peeps->s_lock);
		}
		else if(peeps->status == P_GS)
		{
			printf(GotSeat);
			if(x)//ye baar baar print ho rha tha isliye extra var
			printf("Person %s has got a seat in zone %c\n", peeps->name, peeps->seatzone);
			printf(RESET);
			x = 0;
			pthread_cond_wait(&peeps->s_cond, &peeps->s_lock);

		}
		else if(peeps->status == P_CGS)
		{
			printf(DidNotGetSeat);
			printf("Person %s could not get a seat\n", peeps->name);
			printf(RESET);	
			peeps->status = P_WAIT;
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);

		}
		else if(peeps->status == P_WH)
		{
			printf(WatchedHappy);
			printf("Person %s watched the match for %d seconds and is leaving\n", peeps->name, spect_time);
			printf(RESET);
			peeps->status = P_WAIT;
			pthread_mutex_lock(&lock_seat);
			filled_seats--;
			if(peeps->seatzone == 'H')
			{
				homezone.seats_filled--;
				peeps->seatzone = 0;
			}
			else if(peeps->seatzone == 'N')
			{
				neutralzone.seats_filled--;
				peeps->seatzone = 0;
			}
			else
			{
				awayzone.seats_filled--;
				peeps->seatzone = 0;
			}
			pthread_mutex_unlock(&lock_seat);
			pthread_cond_signal(&cond_seat);
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);

		}
		else if(peeps->status == P_WUH)
		{
			printf(WatchedSad);
			printf("Person %s is leaving due to the bad defensive performance of his team\n", peeps->name);
			printf(RESET);
			peeps->status = P_WAIT;
			pthread_mutex_lock(&lock_seat);
			filled_seats--;
			if(peeps->seatzone == 'H')
			{
				homezone.seats_filled--;
				peeps->seatzone = 0;
			}
			else if(peeps->seatzone == 'N')
			{
				neutralzone.seats_filled--;
				peeps->seatzone = 0;
			}
			else
			{
				awayzone.seats_filled--;
				peeps->seatzone = 0;
			}
			pthread_mutex_unlock(&lock_seat);
			pthread_cond_signal(&cond_seat);
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);


		}
		else if(peeps->status == P_WAIT)
		{			
			pthread_mutex_unlock(&peeps->s_lock);
			peeps->exit = 1;
			pthread_barrier_wait(&ptrtobarriers[peeps->group-1]);
			//group-1 bcoz its 1 based indexing;
			if(pthread_mutex_trylock(&ptrtogrpprint[peeps->group-1]) == 0)
			{
				printf(GroupLeave);
				printf("Group %d is leaving for dinner\n", peeps->group);
				printf(RESET);
			}
			break;
		}




		pthread_mutex_unlock(&peeps->s_lock);
		usleep(50000);
	}

	pthread_mutex_destroy(&peeps->s_lock);
	pthread_cond_destroy(&peeps->s_cond);

}


void * execute_seats()
{
	while(1)
	{	
		pthread_mutex_lock(&lock_seat);
		if(filled_seats == total_seats)
			pthread_cond_wait(&cond_seat, &lock_seat);

		int send = 0;
		for(int i = 0; i < total_people; i++)
		{
			if(arr_people[i].exit == 1)continue;
			send = 0;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_RCHD)
			{	
				send = 1;
				if(arr_people[i].team[0] == 'H')
				{
					if(homezone.seats_filled < homezone.total_seats)
					{
						homezone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'H';
						filled_seats++;
					}
					else if(neutralzone.seats_filled < neutralzone.total_seats)
					{
						neutralzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'N';
						filled_seats++;
					}
				}
				else if(arr_people[i].team[0] == 'A')
				{
					if(awayzone.seats_filled < awayzone.total_seats)
					{
						awayzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'A';
						filled_seats++;
					}
				}
				else
				{
					if(neutralzone.seats_filled < neutralzone.total_seats)
					{
						neutralzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'N';
						filled_seats++;
					}
					else if(homezone.seats_filled < homezone.total_seats)
					{
						homezone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'H';
						filled_seats++;
					}
					else if(awayzone.seats_filled < awayzone.total_seats)
					{
						awayzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'A';
						filled_seats++;
					}
				}
			}
			pthread_mutex_unlock(&arr_people[i].s_lock);
			if(send)
				pthread_cond_signal(&arr_people[i].s_cond);

			if(filled_seats == total_seats)break;
		}
		
		pthread_mutex_unlock(&lock_seat);
		usleep(50000);
	}
	
}

