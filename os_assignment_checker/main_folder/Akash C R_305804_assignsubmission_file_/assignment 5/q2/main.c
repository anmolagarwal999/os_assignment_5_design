#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "colors.h"
#include "datatypes.h"
#include "states.h"

#include "people.h"


extern int capacity_h, capacity_a, capacity_n;
extern int spect_time;
extern int num_groups;
extern int total_people;
extern int chances;

pthread_mutex_t time_lock;
pthread_cond_t time_cond;

pthread_t timethread;

//since game shd be checked at each second , I am coding it inside time tracker only.
void * time_tracker()
{	
	sleep(1);
	while(1)
	{
		pthread_mutex_lock(&time_lock);
		timer++;
		printf(RED);
		printf("Time elapsed is %d.............\n", timer);		
		printf(RESET);
//goal chance thing starts here.
		if(ptrgoal_chance[present_chance].time == timer)
		{
			int chance = (int)(ptrgoal_chance[present_chance].prob * 100);
			//printf("Chance is : %d\n", chance);
			if(rand()%100 < chance)
			{
				char team = ptrgoal_chance[present_chance].team[0];
				if(team == 'H')
				{
					homegoals++;
					printf(GoalScored);
					printf("Team " Home_Team " have scored goal number %d\n",homegoals);
					printf(RESET);
				}
				else 
				{
					awaygoals++;
					printf(GoalScored);
					printf("Team " Away_Team " have scored goal number %d\n",awaygoals);
					printf(RESET);
				}

				present_chance++;
			}
			else
			{	
				char team = ptrgoal_chance[present_chance].team[0];
				if(team == 'H')
				{					
					printf(GoalMissed);
					printf("Team " Home_Team " missed the chance to score goal number %d\n",homegoals+1);
					printf(RESET);
				}
				else 
				{					
					printf(GoalMissed);
					printf("Team " Away_Team " missed the chance to score goal number %d\n",awaygoals+1);
					printf(RESET);
				}
				present_chance++;
			}

			//checking if goals exceeded the tolerate limit;
			for(int i = 0; i < total_people; i++)
			{
				if(arr_people[i].exit == 1)continue;
				pthread_mutex_lock(&arr_people[i].s_lock);
				if(arr_people[i].status == P_GS)
				{
					if(arr_people[i].team[0] == 'H' && awaygoals >= arr_people[i].goals_limit)
					{
						arr_people[i].status = P_WUH;
					}
					else if(arr_people[i].team[0] == 'A' && homegoals >= arr_people[i].goals_limit)
					{
						arr_people[i].status = P_WUH;
					}
				}			
				pthread_mutex_unlock(&arr_people[i].s_lock);
				pthread_cond_signal(&arr_people[i].s_cond);
			}
		}
//patience checking thing starts here.
		int send = 0;
		for(int i = 0; i < total_people; i++)
		{	
			if(arr_people[i].exit == 1)continue;
			send = 0;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_RCHD)
			{	
				send = 1;
				int wait = timer - arr_people[i].reaching_time;
				if(wait > arr_people[i].patience_time)
				{
					arr_people[i].status = P_CGS;
				}
			}			
			pthread_mutex_unlock(&arr_people[i].s_lock);
			if(send)
			pthread_cond_signal(&arr_people[i].s_cond);
		}
//match watching time limit;
		for(int i = 0; i < total_people; i++)
		{	
			if(arr_people[i].exit == 1)continue;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_GS)
			{
				int watch = timer - arr_people[i].entry_time;
				if(watch >= spect_time)
				{
					arr_people[i].status = P_WH;
				}
			}
			pthread_mutex_unlock(&arr_people[i].s_lock);
			pthread_cond_signal(&arr_people[i].s_cond);
		}


		pthread_mutex_unlock(&time_lock);
		pthread_cond_broadcast(&time_cond);
		sleep(1);
	}
}


int main()
{	
// input part starts here
	scanf("%d %d %d", &homezone.total_seats, &awayzone.total_seats, &neutralzone.total_seats);
	scanf("%d %d", &spect_time, &num_groups);

	homezone.seats_filled = 0;
	neutralzone.seats_filled = 0;
	awayzone.seats_filled = 0;

	total_people = 0;
	int peepindex = 0;

	int groups[num_groups]; // just to store number of memb in each group
	int grpindex = 0;
	for(int i = 0; i < num_groups; i++)
	{
		int k;
		scanf("%d", &k);
		total_people+=k;

		groups[grpindex] = k;
		grpindex++;

		for(int j = 0; j < k; j++)
		{
			scanf("%s", arr_people[peepindex].name );
			scanf("%s %d %d", arr_people[peepindex].team, &arr_people[peepindex].reaching_time, &arr_people[peepindex].patience_time);
			scanf("%d", &arr_people[peepindex].goals_limit);
			arr_people[peepindex].id = peepindex;
			arr_people[peepindex].group = i+1;
			arr_people[peepindex].friends = k;			//considering himself as friend too.
			arr_people[peepindex].status = -1;
			arr_people[peepindex].exit = 0;
			peepindex++;
		}
	}

	
	scanf("%d", &chances);
	goal_chance arr_goal_chance[chances];
	ptrgoal_chance = arr_goal_chance;

	for(int i = 0; i < chances; i++)
	{
		scanf("%s",	arr_goal_chance[i].team);
		scanf("%d", &arr_goal_chance[i].time);
		scanf("%lf", &arr_goal_chance[i].prob);
	}
//////////////////////////////////input part ends here
///*************************************************
// processing part starts here.
// game ka thread;
// time ka thread;
// people ka thread;

	pthread_mutex_t grpprint[num_groups];
	ptrtogrpprint = grpprint;
	for(int i = 0; i < num_groups; i++)
	{
		pthread_mutex_init(&grpprint[i], NULL);
	}
	pthread_barrier_t arr_barriers[num_groups];
	ptrtobarriers = arr_barriers;
	for(int i = 0; i < num_groups; i++)
	{
		pthread_barrier_init(&arr_barriers[i], NULL, groups[i]);
	}

	for(int i = 0; i < num_groups; i++)
	{

	}
	srand(time(NULL));
	pthread_mutex_init(&lock_seat, NULL);
	pthread_cond_init(&cond_seat, NULL);
	filled_seats = 0;

	homegoals = 0;
	awaygoals = 0;
	present_chance = 0;
	//total_seats;
	total_seats = (homezone.total_seats + awayzone.total_seats + neutralzone.total_seats);
	
	timer = 0;
	printf(RED);
	printf("Time elapsed is %d.............\n", timer);
	printf(RESET);
	pthread_cond_init(&time_cond, NULL);
	pthread_mutex_init(&time_lock, NULL);
	

	pthread_t people_threads[total_people];
	

	for(int i = 0; i < total_people; i++)
		if(pthread_create(&people_threads[i], NULL, &execute_people, (void *)(&arr_people[i])) != 0)
			perror("Failed to create people thread");

	
	pthread_create(&timethread, NULL, &time_tracker, NULL);

	pthread_t seats;
	pthread_create(&seats, NULL, &execute_seats, NULL);




//iswale join ko comment kardo baad mein
//pthread_join(timethread, NULL);
	
	for(int i = 0; i < total_people; i++)
		if(pthread_join(people_threads[i], NULL) != 0)
			perror("Failed to join people thread");

	for(int i = 0; i < num_groups; i++)
	{
		pthread_barrier_destroy(&arr_barriers[i]);
	}

	pthread_cond_destroy(&time_cond);
	pthread_mutex_destroy(&time_lock);

	pthread_mutex_destroy(&lock_seat);
	pthread_cond_destroy(&cond_seat);

	return 0;
}