#ifndef _DATATYPES
#define _DATATYPES
#include <pthread.h>

#define Home_Team "FC Messilona" 
#define Away_Team "Benzdrid CF"

typedef struct zone zone;
typedef struct people people;
typedef struct goal_chance goal_chance;


struct zone
{
	int seats_filled;
	int total_seats;

};

struct people
{
	int id;
	int group;
	int friends;
	char name[100];
	char team[1];				//Can take either of the H, A, N values
	int reaching_time;
	int patience_time;
	int goals_limit;
	int entry_time;			//should be initialised if seats allocated
	int status;
	char seatzone;
	pthread_mutex_t s_lock;
	pthread_cond_t s_cond;

	int exit;				//he should exit the simulation.
};

struct goal_chance
{
	char team[1];
	int time;			// time is secs after which chance is there;
	double prob;
};

//global variables defined here:
int spect_time;
int num_groups;

int total_people;
int chances;
int total_seats;
int timer;

people arr_people[200];
goal_chance * ptrgoal_chance;
zone homezone;
zone awayzone;
zone neutralzone;

pthread_mutex_t lock_seat;
int filled_seats;
pthread_cond_t cond_seat;

int homegoals;
int awaygoals;
int present_chance;

pthread_barrier_t * ptrtobarriers;
pthread_mutex_t * ptrtogrpprint;
#endif