#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "colors.h"
#include "datatypes.h"
#include "states.h"

void * execute_people(void * people_element);

void * execute_seats();
