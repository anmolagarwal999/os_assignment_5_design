# COURSE ALLOCATION PORTAL
----

## Assumptions

- structs of students, courses, and labs have been made for convenience and readability
- The max buffer size is 1024 bytes
- Coloured outputs are produced to make events distinguishable

## Running the Code
- Compile the code using `gcc q1.c -pthread` in the directory where it is stored to create the executable
- Run  the executable using `./a.out` (Followed by `< input.txt` if there is an input file)

## Entities

1. Course:
```c
typedef struct course {
    char *name;         // unique course name

    float interest;     // Interest level in the course
    int max_slots;      // max slots of the course
    bool removed;       // whether the course has been removed or not

    int num_labs;   // number of available labs;
    int *labs;      // array of lab ids

    sem_t wait_end_tute;
    pthread_t course_thread_id;

    sem_t wait_for_seat;
    int ppl_there;
    pthread_mutex_t ppl_lock;
    pthread_cond_t  ppl_sig;

    pthread_cond_t attend;
    pthread_mutex_t attend_lock;
    int num_stud;   // Total number of students
} course;
```

2. Student:
```c
typedef struct student {
    int id;       // student id
    int time;     // time to fill preferences
    int pref[3];  // top 3 preferences, 0 to 3
    enum stud_state state;  // current state of the student as described by the enum
    float calibre;      // calibre of the student
    pthread_t stud_thread_id;
} stud;
```

3. Lab:
```c
typedef struct lab {
    char *name;
    int max_no_times;
    int num_tas;
    int *num_taed;       // number of times the TA took tutorials
    bool *occupied;      // TA is free or not
} lab;
```

## Functioning:

1. Read the course values and student values in the specified format and store them in arrays
2. Initialise the threads, semaphores and locks that are described as part of the structs
3. Create the threads for the students and courses respectively using the`pthread_create` function

**Student Thread:**
- The student fills the preferences with appropriate delay which is simulated using timesspec and `clock_gettime()`
- If the student still hasn't tried a course on their preference list, the thread does the following steps:
    1. Ensure the course hasn't been removed
    2. Allocates the student a seat in the course 
    3. Tests if student wants to select course based on his calibre & interest through `if((rand()%1001) > (course_list[s->pref[pref_indx]].interest)*(s->calibre)*1000 )`
    4. If student still has further preferences, prints out that they have changed preference
    5. Prints out if student can't get his preferred courses
    6. If students selects the course permanently, prints it out

**Course Thread:**

- The course thread is continuously active unless it has been withdrawn
- If there are no waiting students, the thread waits unless signalled by `pthread_cond_wait(&(c->ppl_sig), &(c->ppl_lock));`
- The thread then does TA allocations based on the list of given lab id's and given conditions:
    - The number of times they have TA'd doesnt exceed lab maximum
    - The lab isn't already occupied
- If the lab hasn't found any TA's it prints it out and continues
- If none of the labs are allocated to any TA's, the course is withdrawn, its associated details in the array is updated, and the Event is printed out
- If a TA has beed assigned, the event is printed out and a random number of seats between `1 to max_slots` for that course is allocated
- It then wakes up all the student threads waiting for the seats and enters them into the tute via `sem_post(&c->wait_for_seat);`
- The tute is only started after all slots have been filled using `pthread_cond_wait( &(c->attend), &(c->attend_lock) );` 
- Upon completion of the tute the semaphore is updated by `sem_post(&c->wait_end_tute)` the lab_list is updated and the corresponding event is printed.

4. Finally, The student threads are joined using pthread_join and the semaphores are destroyed after the threads finish working.
