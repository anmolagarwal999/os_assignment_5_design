#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <errno.h>

// Colors
#define ANSI_BLK "\e[1;30m"
#define ANSI_RED "\e[1;31m"
#define ANSI_GRN "\e[1;32m"
#define ANSI_YEL "\e[1;33m"
#define ANSI_BLU "\e[1;34m"
#define ANSI_MAG "\e[1;35m"
#define ANSI_CYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#define create(a, b, c) pthread_create(&a, NULL, b, &c)
#define join(a) pthread_join(a, NULL)
// Global variables

// semaphores
sem_t imaginary_lock;
sem_t *lab_locks;

// struct definitions
typedef struct course {
    char *name;         // unique course name

    float interest;     // Interest level in the course
    int max_slots;      // max slots of the course
    bool removed;       // whether the course has been removed or not

    int num_labs;   // number of available labs;
    int *labs;      // array of lab ids

    sem_t wait_end_tute;
    pthread_t course_thread_id;

    sem_t wait_for_seat;
    int ppl_there;
    pthread_mutex_t ppl_lock;
    pthread_cond_t  ppl_sig;

    pthread_cond_t attend;
    pthread_mutex_t attend_lock;
    int num_stud;   // Total number of students
} course;

typedef struct lab {
    char *name;
    int max_no_times;
    int num_tas;
    int *num_taed;       // number of times the TA took tutorials
    bool *occupied;      // TA is free or not
} lab;

enum stud_state { not_registered, waiting, attending, finished };
typedef struct student {
    int id;       // student id
    int time;     // time to fill preferences
    int pref[3];  // top 3 preferences, 0 to 3
    enum stud_state state;  // current state of the student as described by the enum
    float calibre;      // calibre of the student
    pthread_t stud_thread_id;
} stud;

lab *lab_list;                 // array containing all labs
course *course_list;           // array containing all courses

void *start_course(void *args);
void *start_stud(void *args);

void *start_course(void *args)
{
    course *c = (course *)args;

    while(1)
    {
        int status = 0, curr_ta_id = -1, curr_lab_id = -1, num_taed = -1;
        char *cur_lab_name = NULL;

        // course has no waiting students
        pthread_mutex_lock(&(c->ppl_lock));
        while(!c->ppl_there)
            pthread_cond_wait(&(c->ppl_sig), &(c->ppl_lock));
        pthread_mutex_unlock(&(c->ppl_lock));

        // TA allocation
        int i;
        for(i = 0; i < c->num_labs; i++)
        {
            if(c->labs[i] == -1)
                continue;

            int lab_status = 0;       // 1 means lab is free & 0 means lab is full
            curr_lab_id = c->labs[i];

            sem_wait( &lab_locks[c->labs[i]] );
            cur_lab_name = lab_list[c->labs[i]].name;

            for( int j = 0; j < lab_list[c->labs[i]].num_tas; j++)
            {
                if(lab_list[c->labs[i]].max_no_times > lab_list[c->labs[i]].num_taed[j])
                {
                    if(lab_list[c->labs[i]].occupied[j]){
                        lab_status = 1;
                        continue;
                    }
                    else
                    {
                        num_taed = lab_list[c->labs[i]].num_taed[j] + 1;
                        ++lab_list[c->labs[i]].num_taed[j];
                        curr_ta_id = j;
                        lab_list[c->labs[i]].occupied[j] = true;
                        status = 1; // lab has been assigned
                        break;
                    }
                }
            }
            sem_post(&lab_locks[c->labs[i]]);

            if(status)
                break;
            
            else if(!lab_status)
            {
                // Event 12
                printf( ANSI_RED "Lab %s no longer has students available for TA ship\n" ANSI_RESET, cur_lab_name);
                c->labs[i] = -1;
            }
        }

        // If course is withdrawn
        if(!status)
        {
            int tmp;
            pthread_mutex_lock(&(c->ppl_lock));
            c->removed = true;
            tmp = c->ppl_there;
            c->ppl_there = 0;   // No people left in the course
            pthread_mutex_unlock(&(c->ppl_lock));

            for(int i = 0; i < tmp; i++)
                sem_post(&(c->wait_for_seat));
            
            // Event 10
            printf( ANSI_RED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, c->name);
            return NULL;
        }

        // TA assigned
        else
        {
            // Event 11
            printf( ANSI_CYN "TA %d from lab %s has been allocated to course %s for %dth TA ship\n" ANSI_RESET, curr_ta_id, cur_lab_name, c->name, num_taed);
            int cur_seats = ( rand() % c->max_slots ) + 1;
            int num_alloc = cur_seats;
            // Event 7
            printf( ANSI_BLU "Course %s has been allocated %d seats\n" ANSI_RESET, cur_lab_name, cur_seats);

            // allocating seats to students
            pthread_mutex_lock(&(c->ppl_lock));
            if(cur_seats > c->ppl_there)
                num_alloc = c->ppl_there;
            pthread_mutex_unlock(&(c->ppl_lock));

            for(i = 0; i < num_alloc; i++ )
                sem_post( &c->wait_for_seat );      // wake up student threads that are waiting for seats            

            // Tutorial : Event 8
            printf( ANSI_MAG "Tutorial has started for Course %s with %d seats filled out of %d\n" ANSI_RESET, c->name, num_alloc, cur_seats);
            sleep(2);

            pthread_mutex_lock(&(c->attend_lock));
            while(num_alloc > c->num_stud)
                pthread_cond_wait( &(c->attend), &(c->attend_lock) ); // wait for students to join
            pthread_mutex_unlock(&(c->attend_lock));

            int k = 0;
            while(k < num_alloc)
                sem_post(&c->wait_end_tute), k++;

            // TA leaves course
            sem_wait(&lab_locks[curr_lab_id]);
            lab_list[curr_lab_id].occupied[curr_ta_id] = false;
            sem_post(&lab_locks[curr_lab_id]);
            // Event 9
            printf( ANSI_CYN "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_RESET, curr_ta_id, cur_lab_name, c->name);
        }
    } 
}

void *start_stud(void *args)
{
    stud *s = (stud *)args;

    int pref_indx = 0;      // Index of student's preferred course
    // students fills his preferences after 't' seconds
    struct timespec tspec;
    clock_gettime(CLOCK_REALTIME, &tspec);
    tspec.tv_sec += s->time;
    if(s->state == 0)
    {
        sem_timedwait(&imaginary_lock, &tspec);
        s->state = waiting;
        // Event 1
        printf( ANSI_GRN "Student %d has filled in preferences for course registration\n" ANSI_RESET, s->id);
    }

    while(pref_indx < 3)
    {
        pthread_mutex_lock(&(course_list[s->pref[pref_indx]].ppl_lock));
        course_list[s->pref[pref_indx]].ppl_there += 1;
        pthread_cond_signal(&(course_list[s->pref[pref_indx]].ppl_sig));
        pthread_mutex_unlock(&(course_list[s->pref[pref_indx]].ppl_lock));

        if(course_list[s->pref[pref_indx]].removed) // if course is discontinued
            goto removed;
        if(sem_wait(&(course_list[s->pref[pref_indx]].wait_for_seat)) == -1)
            perror("sem_wait error");
        if(course_list[s->pref[pref_indx]].removed) // Since time delay, have to recheck
            goto removed;
        
        // Event 2
        printf( ANSI_GRN "Student %d has been allocated a seat in course %s\n" ANSI_RESET, s->id, course_list[s->pref[pref_indx]].name);
        s->state = attending; // update state of student

        pthread_mutex_lock(&(course_list[s->pref[pref_indx]].attend_lock));
        ++course_list[s->pref[pref_indx]].num_stud;
        pthread_cond_signal( &(course_list[s->pref[pref_indx]].attend));
        pthread_mutex_unlock( &(course_list[s->pref[pref_indx]].attend_lock));

        if(sem_wait(&(course_list[s->pref[pref_indx]].wait_end_tute)) == -1)
            perror("sem_wait error");

        // decide if he wants to continue
        if((rand()%1001) > (course_list[s->pref[pref_indx]].interest)*(s->calibre)*1000 ) // if random chance > interest, student leaves course
        {
            // Event 3
            printf( ANSI_RED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->pref[pref_indx]].name);

        // if course withdrawn or student withdraws from course    
        removed:
            ++pref_indx;
            if(pref_indx < 3){
                // Event 4
                s->state = waiting;
                printf( ANSI_YEL "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" ANSI_RESET, s->id, course_list[ s->pref[pref_indx-1] ].name, pref_indx, course_list[ s->pref[pref_indx] ].name,  pref_indx+1);
            }
            else{ 
                // Event 6
                printf( ANSI_RED "Student %d could not get any of his preferred courses\n" ANSI_RESET, s->id);
                return NULL;
            }
        }
        else{
            // Event 5
            printf( ANSI_GRN "Student %d has selected course %s permanently\n" ANSI_RESET, s->id, course_list[s->pref[pref_indx]].name);
            s->state = finished;
            return NULL;
        }
    }
    // student didn't find any course: Event 6
    printf( ANSI_RED "Student %d could not get any of his preferred courses\n" ANSI_RESET, s->id);
    return NULL;
}

int main()
{
    srand(time(0));

    // Initializing semaphores to zero
    sem_init(&imaginary_lock, 0, 0);

    int num_students, num_labs, num_courses;
    scanf("%d", &num_students);
    scanf("%d", &num_labs);
    scanf("%d", &num_courses);

    course_list = (course *)malloc(num_courses * sizeof(course));
    if(course_list == NULL){
        printf("malloc failed\n");
        exit(0);
    }

    int i;
    for(i = 0; i < num_courses; i++)
    {
        course_list[i].name = (char *)malloc(sizeof(char *));
        if(course_list[i].name == NULL){
            printf("malloc failed\n");
            exit(0);
        }
        course_list[i].labs = (int *)malloc(num_labs * sizeof(int));
        if(course_list[i].labs == NULL){
            printf("malloc failed\n");
            exit(0);
        }
    }

    // Read course values into course_list
    for(i = 0; i < num_courses; i++)
    {
        scanf("%s", course_list[i].name);
        scanf("%f", &course_list[i].interest);
        scanf("%d", &course_list[i].max_slots);
        scanf("%d", &course_list[i].num_labs);
        for( int j = 0; j < course_list[i].num_labs; j++){
            scanf("%d", &course_list[i].labs[j]);
        }
        course_list[i].removed = false;
        course_list[i].num_stud = 0;
    }

    // Initialise the pthreads
    for(i = 0; i < num_courses; i++)
    {
        pthread_cond_init( &(course_list[i].attend), NULL);
        pthread_mutex_init( &(course_list[i].attend_lock), NULL);
        pthread_mutex_init( &(course_list[i].ppl_lock), NULL);
        sem_init(&course_list[i].wait_end_tute, 0, 0);
        sem_init(&course_list[i].wait_for_seat, 0, 0);
        
        pthread_mutex_lock( &(course_list[i].ppl_lock) );
        course_list[i].ppl_there = 0;
        pthread_mutex_unlock( &(course_list[i].ppl_lock) );
        pthread_cond_init( &(course_list[i].ppl_sig), NULL);

    }

    int student_id = 0;
    stud st[num_students]; // array of students
    for(i = 0; i < num_students; i++)
    {
        float calibre;
        int pref1, pref2, pref3;
        scanf("%f", &calibre);
        scanf("%d", &pref1);
        scanf("%d", &pref2);
        scanf("%d", &pref3);
        scanf("%d", &st[i].time);
        st[i].calibre = calibre, st[i].pref[0] = pref1, st[i].pref[1] = pref2, st[i].pref[2] = pref3;
        st[i].id = student_id, st[i].state = not_registered;
        student_id++;
    }

    lab_list = (lab *)malloc(num_labs*sizeof(lab));
    if(lab_list == NULL){
        printf("malloc failed\n");
        exit(0);
    }
    lab_locks = (sem_t *)malloc( num_labs*sizeof(sem_t) );
    if(lab_locks == NULL){
        printf("malloc failed\n");
        exit(0);
    }

    for(i = 0; i < num_labs; i++)
    {
        lab_list[i].name = (char *)malloc(sizeof(char *));
        scanf("%s", lab_list[i].name);
        scanf("%d", &lab_list[i].num_tas);
        scanf("%d", &lab_list[i].max_no_times);

        lab_list[i].num_taed = (int *)malloc( lab_list[i].num_tas*sizeof(int) );
        if(lab_list[i].num_taed == NULL ){
            printf("malloc failed\n");
            exit(0);
        }

        lab_list[i].occupied = (bool*) malloc(num_labs * sizeof(bool));
        if(lab_list[i].occupied == NULL ){
            printf("malloc failed\n");
            exit(0);
        }

        for( int j = 0; j < lab_list[i].num_tas; j++)
            lab_list[i].num_taed[j] = 0, lab_list[i].occupied[j] = false;
        sem_init(&(lab_locks[i]), 0, 1);
    }
    int j;
    for(j = 0; j < num_students; j++)
        create(st[j].stud_thread_id, start_stud, st[j]);

    for(j = 0; j < num_courses; j++)
        create(course_list[j].course_thread_id, start_course, course_list[j] );
    
    for(j = 0; j < num_students; j++)
        join(st[j].stud_thread_id);

    // destroy semaphore/mutex after all the threads finished working
    sem_destroy(&imaginary_lock);

    free(course_list);
    free(lab_list);
    free(lab_locks);

    return 0;
}