#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <queue>
#include <string>
#include <cstring>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>
using namespace std;

#define PORT 8080

map<int, string> dictionary;
queue<int> Socket;
vector<pthread_mutex_t> mutexLock(100, PTHREAD_MUTEX_INITIALIZER);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void client_handler(int sockfd)
{
	// read the message from client and store it in a buffer
    char buffer[1024] = {0};
	int bytes_received = read(sockfd, buffer, 1024);
    if (bytes_received < 0)
    {
        cerr << "Failed to READ DATA from socket.\n";
        exit(-1);
    }

    // tokenising string w.r.t. space ' '
    vector<string> tokens;
    stringstream buff(buffer);
    string temp;
    while(getline(buff, temp, ' '))
        tokens.push_back(temp);

    string str = "";
    str += to_string(pthread_self()) + ":"; // acquire thread id

    if (tokens.size() > 1)
        if (tokens[1] == "insert"){
            if (tokens.size() != 4)
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (dictionary.count(key) != 1)
                {
                    dictionary[key] = tokens[3];
                    str += "Insertion Successful";
                }
                else
                    str += "Key already exists";
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[1] == "delete"){
            if (tokens.size() != 3)
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (dictionary.count(key) != 1)
                    str += "No such key exists";
                else
                {
                    dictionary.erase(key);
                    str += "Deletion Successful";
                }
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[1] == "concat"){
            if (tokens.size() != 4)
                str += "Incorrect number of arguments";
            else
            {
                int key1 = stoi(tokens[2]), key2 = stoi(tokens[3]);
                pthread_mutex_lock(&mutexLock[key1]);
                pthread_mutex_lock(&mutexLock[key2]);
                if (dictionary.count(key1) != 1 || dictionary.count(key2) != 1)
                    str += "Concat failed as a least one of the keys does not exist";
                else
                {
                    string temp = dictionary[key1];
                    dictionary[key1] += dictionary[key2];
                    dictionary[key2] += temp;
                    str += dictionary[key2];
                }
                pthread_mutex_unlock(&mutexLock[key1]);
                pthread_mutex_unlock(&mutexLock[key2]);
            }
        }
        else if (tokens[1] == "update"){
            if (tokens.size() != 4)
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (dictionary.count(key) == 1)
                {
                    dictionary[key] = tokens[3];
                    str += tokens[3];
                }
                else
                    str += "No such key exists";
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[1] == "fetch"){
            if (tokens.size() != 3)
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[3]);
                pthread_mutex_lock(&mutexLock[key]);
                if (dictionary.count(key) != 1)
                    str += "Key does not exist";
                else
                    str += dictionary[key];
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else
            str += "Invalid command";
    else
        str += "Incorrect number of arguments";

    int bytes_sent = write(sockfd, str.c_str(), str.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
}

void *worker_thread(void *arg)
{
	while (1)
	{
		pthread_mutex_lock(&lock);
		while (!Socket.size())
			pthread_cond_wait(&cond, &lock);    // If queue is empty, thread waits

        int socket_fd = Socket.back();
		Socket.pop();

		pthread_mutex_unlock(&lock);

		client_handler(socket_fd);
		close(socket_fd);
	}
	return NULL;
}

void client_search()
{
	int welc_sockfd;
	struct sockaddr_in servaddr, clientaddr;

	welc_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (welc_sockfd < 0){
        perror("ERROR creating welcoming socket");
        exit(0);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    int port = PORT;
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);

	if ((bind(welc_sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) < 0){
        perror("Error on bind on welcome socket");
        exit(-1);
    }

	if ((listen(welc_sockfd, 5)))
		exit(0);

	socklen_t cli_len = sizeof(clientaddr);

    int client_fd;
	while(1)
	{
		client_fd = accept(welc_sockfd, (struct sockaddr *)&clientaddr, &cli_len);
		if (client_fd < 0)
			exit(0);
		pthread_mutex_lock(&lock);
        Socket.push(client_fd);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
	}
}

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
        cout << "Number of threads not declared";   
	}
	else
    {
		int num_threads = atoi(argv[1]);
        vector<pthread_t> worker_threads(num_threads);

		for (int i = 0; i < num_threads; i++)
			pthread_create(&worker_threads[i], NULL, worker_thread, NULL);

		client_search();

        // Wait for all threads to terminate
		for (int i = 0; i < num_threads; i++)
			pthread_join(worker_threads[i], NULL);
    }
	return 0;
}
