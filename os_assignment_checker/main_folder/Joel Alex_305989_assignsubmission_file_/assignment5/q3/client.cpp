#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <string.h>
#include <netdb.h>
#include <pthread.h>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
using namespace std;

// Colors
#define ANSI_BLK "\e[1;30m"
#define ANSI_RED "\e[1;31m"
#define ANSI_GRN "\e[1;32m"
#define ANSI_YEL "\e[1;33m"
#define ANSI_BLU "\e[1;34m"
#define ANSI_MAG "\e[1;35m"
#define ANSI_CYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#define MAXBUFFERSIZE 1024
#define PORT 8080

typedef struct Data{
    int client_id;
    string command;
} Data;

void sock_read_write(int sockfd, Data& data)
{
    
    int bytes_sent = write(sockfd, data.command.c_str(), data.command.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }

    char buff[MAXBUFFERSIZE] = {0};
    int bytes_received = read(sockfd, buff, MAXBUFFERSIZE);
    if (bytes_received < 0)
    {
        cerr << "Failed to READ DATA from socket.\n";
        exit(-1);
    }

    cout << data.client_id << ": " << buff << endl;
}

void* client(void* args)
{
    Data data = *(Data*)args;

    // Tokenize string and store in vector
    vector<string> tokens;
    stringstream str(data.command);
    string temp;
    while(getline(str, temp, ' '))
        tokens.push_back(temp);

    // Convert string to integer
    sleep(stoi(tokens[0]));

    // If the input is invalid
    if(tokens.size() < 2)
    {
        cout << "Command doesn't exist\n";
        return NULL;
    }

    int sockfd;
    struct sockaddr_in servaddr;

    // Create and verify socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        exit(0);

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    int connect_status = connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr));
    if (connect_status != 0)
        exit(0);

    sock_read_write(sockfd, data);
    close(sockfd);

    return NULL;
}

void begin_process()
{
    // Input number of client requests
    int num_Clients;
    scanf("%d", &num_Clients);
    getchar(); // remove trailing \n

    vector<pthread_t> client_threads(num_Clients);  // Create a thread for each client
    vector<Data> requests(num_Clients);             // Create an array to store all requests

    for(int i = 0; i < num_Clients; i++)
    {
        requests[i].client_id = i;
        getline(cin, requests[i].command);
        pthread_create(&client_threads[i], NULL, client, &requests[i]);
    }

    // Wait for all threads to terminate
    for(int i = 0; i < num_Clients; i++)
        pthread_join(client_threads[i], NULL);
}

int main()
{
    begin_process();
    return 0;
}
