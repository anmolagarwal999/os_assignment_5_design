# MULTI THREADED SERVER
----

## Running the file

1. Compile the server.cpp file using the command `g++ server.cpp -pthread -o server` to create server executable
2. Compile the client.cpp file using the command `g++ client.cpp -pthread -o server` to create client executable
3. Open 2 seperate terminals to run the executables:
    - client: `./client` to create a simulation of multiple clients sending requests
    - server: `./server` followed by max number of threads to simulate the server

## IMPLEMENTATION:

### Client Side
1. Create an array of threads to represent client requests and store the associated data in threads which is read as input from the user
2. We generate each thread and make it run the client handler

3. *Client handler*:
- The client handler tokenises the string in order to check its validity
- It then creates a socket, binds the socket to the port, and listens for connections via the system calls (`socket, bind, and listen`)
- It connects the Client socket to server socket based on predetermined Server Socket (8080).
- Sends and retrieves data via the sockets

4. We wait for all threads to terminate via `pthread_join()`.

### Server Side

**Initialization**
1. We read the max number of threads via the command line and create an array of worker threads to process the client requests
2. We create a socket, bind the socket to the port, and listen for connections via the system calls (`socket, bind, and listen`) similar to the client side but define the server port as 8080
3. We also create a queue to store all client sockets in case all worker threads are busy.
4. We create a map which stores the dictionary of keys and values which can be modified different client requests

**Functioning:**
1. All non-busy working threads check the queue for client requests and upon finding any, will call the client handler function
2. This function ensure the command is valid and tokenises the string in order to retrieve the key
3. It initialises a string which stores the outputs based on the request
5. Modifications are made to the dictionary and the output string is updated accordingly
6. The output string is written to the client socket and worker thread is freed