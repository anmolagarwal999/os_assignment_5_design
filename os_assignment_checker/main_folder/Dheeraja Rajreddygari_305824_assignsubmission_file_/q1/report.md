# Question 1

## Usage
1. To compile, run `gcc q1.c -o q1 -lpthreads`
2. Execute with `./q1`

## A brief explanation of the code
After taking input from the user, the program creates one thread for every course and student in the simulation. 

### working of student threads
1. The thread sleep for the appropriate time before the student fills in course preferences.
2. For the current preference, check if the course is still available. If it is, increase the counter to wait for a 
tutorial slot to become available. Otherwise, go to the next preference and repeat.
3. Once this thread is woken from the wait, check again if the course is available. If it is withdrawn, go to the next preference.
4. If the course is available, decrease the waiting-for-tutorial counter and occupy one of the available tutorial slots
(decrease the number of available slots). Call this "booking a tutorial slot".
5. Signal the course that you are ready for the tutorial to begin, and wait for another conditional variable that signals the tutorial is over.
6. Once the tutorial is done, use the probability specified by `course_interest*student_calibre` to either \
	a. Select the course and exit the simulation, or \
	b. Reject the course, go to the next preference and repeat from step 2. If there are no more preferences left, exit the simulation without being assigned any course :( 

### working of course threads
1. Wait for a TA from one of the acceptable labs for the particular course. If all the labs have no TAs left, exit to simulate 
the course beign removed form the simulation.
2. Once a TA is assigned, allocate a random number of tutorial slots.
3. If there is atleast one student waiting for a tutorial slot, signal them to book a slot for the tutorial. If there are no 
students, wait until there is atleast one student interested in the course and repeat step 3.
4. The thread is now ready to conduct the tutorial. Sleep for 2 seconds to simulate it.
5. Once the tutorial is done, signal to `n` students that the tutorial is done (where `n` is the number of students who attended 
the tutorial).
6. Set the number of tutorial slots to 0 and go back to step 2. If there are no TAs available, set the number of 
interested students to -1 to indicate that the course is withdrawn, and exit the simulation.

