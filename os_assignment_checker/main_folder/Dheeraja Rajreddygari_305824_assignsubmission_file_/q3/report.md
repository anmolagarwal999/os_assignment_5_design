# Question 3

## Usage

### Server side
1. Run `g++ server.cpp -o server -lpthreads` to compile.
2. Run `./server <num_threads>` to begin the server with a pool of `num_threads` worker threads.

### Client
1. Run `g++ client.cpp -o client -lpthreads` to compile.
2. Run `./client` to execute the application.

Ensure the server is running when executign the client program

## A brief explanation of the code
### Server
The server sets up the required number of worker threads (as specified by the arguments) and initializes a welcome socket 
that listens for incoming requests. When it accepts a user request, it is pushed to a queue 
(`client_socket_queue`, form which the workers pop requests for processing). It then signals 
the conditional variable `client_request_available` to let a worker thread know that a request is available for precessing.

The server maintains a global vector of strings that functions as a dictionary. Each entry in the dictionary has a seperate mutex 
lock. Having a seperate lock for each entry ensures that multiple worker threads are able to access the dictionary simultaneously. 
Since there are only 100 entries in the dictionary, it is not expensive to maintain seperate locks for each entry. When some 
worker thread wants to access the dictionary (to read or write) it first acquires the lock indexed by the key of the entry in 
question, and releases it when it is finished.

When a worker threads is created, it waits until a user request is available for processing. Once a request is available, 
it pops the request from the queue and listens for the command from the user. The request is then processed and an 
appropriate response sent back to the user. Once this is done, the worker then terminates the connection and is available 
to process the next user request.

### Client
The client program sets up a number of threads, one for each command to be processed. Each of those threads waits 
for the appropriate time and connects to the server to make the necessary request. This is done to simulate multiple 
different users making independant requests to the server.
