# Operating System Assignment 5
## Question 1
### Compilation
```
gcc -pthread main.c
```
### Implementation
1. We first take the input using the ```take_input()``` function, and store the data in the students, courses and labs structures. 
2. Then we create threads for the courses and students and run them.
3. The courses thread then searches for the available TA's from the labs of the course and then if it finds available TA by looping over the complete set of TA's of the labs. 
4. Then it signals the students waiting for the confirmation get the signal and they try to get a seat alloted for the tutorial.
5. Then the courses tutorial thread starts a tutorial and the students who are enrolled wait for a conditional variable to signal the end of the tutorial.
6. Then the students choose whether they want to finalise the course or not according to probability p that is calculated.
7. Once chosen, the students move on to their next preference.
8. The course thread then checks if the courses have more TA's that it can avail, if not we delete the course, then we also check if any of the student have this course as a preference and is yet to take tutorial of the course. If there is not such student, then also we delete the course, just by changing the variable in the struct.
9. The course thread checks if the number of enrolled students is not 0, if 0 the thread is set to wait for a student to avail finish other tutorial and avail the course.
10. If it is not 0, the tutorial is conducted and then the signalling is done the students attending the tutorial that the tute is done.
