#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<string.h>
#include<time.h>

#define STRING_LEN 1000
#define PEOPLE_LEN 100
#define COURSES_LEN 100
#define LAB_NUM 100
#define QUANTITY_NUM 100
struct Lab
{
    int id;                                 // id for the lab
    char name[STRING_LEN];                  // name for the lab
    int num_students;                       // number of available ta's for the lab
    int ta_course_limit;                    // limit for the number of courses one 
    int ta_current_number[PEOPLE_LEN];
    int ta_occupied[PEOPLE_LEN];
    pthread_mutex_t ta_mutex[PEOPLE_LEN];
    // pthread_cond_t taRunningTutes[PEOPLE_LEN];
};
struct Student
{
    int id;
    int preferences[COURSES_LEN];
    int prefVisited[COURSES_LEN];
    float calibre;
    int current_course;
    int pref_filling_time;
    // pthread_t student_get_course;
    pthread_mutex_t student_lock;
    // pthread_cond_t student_getting_tute;
};
struct Course
{
    char name[STRING_LEN];              // name of the course
    int isCourseAvailable;              // boolean variable to keep track if the course is available
    int id;                             // id of the course
    int num_labs;                       // number of labs course is assigned to
    int labs[LAB_NUM];                  // lab id's of labs assigned
    int totalSlots;                     // total number of slots for tutorial
    int leftSlots;                      // number of slots left
    int currentLab;                     // current lab of the assigned TA
    int currentTA;                      // current TA number of the lab
    float interest;                     // interest of the course
    int isCourseDeleted;                
    // pthread_t course_get_ta;            // thread for the course getting TA
    pthread_cond_t course_available;    // conditional variable foe availibility of the code
    pthread_cond_t runningTutes;        // conditional variable used to run tutes
    pthread_mutex_t course_lock;        // lock fo the course
};
int numStudents, numLabs, numCourses;
struct Lab labss[QUANTITY_NUM];
struct Student students[QUANTITY_NUM];
struct Course courses[QUANTITY_NUM];
pthread_t studentThread[QUANTITY_NUM];
pthread_t courseThread[QUANTITY_NUM];

void *runStudent(void *args);
void *runCourse(void *args);
void takeInput();

void takeInput(){
    // TODO : Initialise all the new threads and conditional variables and gives value to course lab and courses ta as -1
    for( int i = 0; i < numCourses ; i++){
        courses[i].id = i;
        courses[i].currentTA = -1;
        courses[i].currentLab = -1;
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].totalSlots, &courses[i].num_labs);
        for(int j = 0; j<courses[i].num_labs; j++){
            scanf("%d", &courses[i].labs[j]);
        }
        if(pthread_mutex_init(&courses[i].course_lock, NULL)){
            printf("\nError while mutex intialisation.\n");
        }
        if(pthread_cond_init(&courses[i].course_available, NULL)){
            printf("\nError while conditional intialisation.\n");
        }
        if(pthread_cond_init(&courses[i].runningTutes, NULL)){
            printf("\nError while conditional intialisation.\n");
        }
    }
    for( int i = 0; i<numStudents ; i++){
        students[i].id = i;
        if(pthread_mutex_init(&students[i].student_lock, NULL)){
            printf("\nError while mutex intialisation.\n");
        }
        scanf("%f %d %d %d %d", &students[i].calibre, &students[i].preferences[0], &students[i].preferences[1], &students[i].preferences[2], &students[i].pref_filling_time);
    }
    for(int i = 0; i < numLabs ; i++){
        labss[i].id = i;
        scanf("%s %d %d", labss[i].name, &labss[i].num_students, &labss[i].ta_course_limit);
        for(int j = 0; j<labss[i].num_students ; j++){
            labss[i].ta_current_number[j] = 0;
            labss[i].ta_occupied[j] = 0;
        }
    }
}
void *runCourse(void *args){
    int flag = 0;
    int courseID = ((struct Course *)args)->id;
    struct Course *newCourse = (struct Course *)args;
    while(1){
        int getTA = 0;
        int doneSlotsTAs = 0;
        int totalTAs = 0;
        int printer = 0;
        int selectedTA = 0;
        int selectedLab = 0;
        for(int i = 0; i<newCourse->num_labs; i++){
            flag = 0;
            struct Lab *labToCheck = &labss[newCourse->labs[i]];
            int limit = labToCheck->ta_course_limit;
            totalTAs += labToCheck->num_students;
            for(int j = 0; j<labToCheck->num_students; j++){
                pthread_mutex_lock(&labToCheck->ta_mutex[j]);
                if(labToCheck->ta_current_number[j]<limit && labToCheck->ta_occupied[j]!=1){
                    labToCheck->ta_occupied[j] = 1;
                    labToCheck->ta_current_number[j]++;
                    getTA = 1;
                    // printf("lolmaolol %d %s %d\n", labToCheck->ta_current_number[j], labToCheck->name, j);
                    printer = labToCheck->ta_current_number[j];
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                    selectedLab = labToCheck->id;
                    selectedTA = j;
                    flag = 1;
                    courses[courseID].currentLab = labToCheck->id;
                    courses[courseID].currentTA = j;
                    // printf("lmaolol %d\n", courses[courseID].currentLab);
                    printf("TA %d from lab %s has been allocated to course %s for his %d TA ship\n", j, labToCheck->name, newCourse->name, printer+1);
                    break;
                }
                // if(1){
                //     printf("lolmaolol %d %d %d %d %d\n", doneSlotsTAs, totalTAs, courses[courseID].isCourseAvailable, courses[courseID].currentLab, courses[courseID].currentTA);
                // }
                else if(labToCheck->ta_current_number[j] == limit){
                    doneSlotsTAs+=1;
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                    continue;
                }
                else{
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                }
            }
            if(flag){
                break;
            }
        }
        // if(getTA==0){
        //     courses[courseID].isCourseDeleted = 1;
        //     pthread_cond_broadcast(&courses[courseID].course_available);
        //     printf("Course %d doesn't have any TA's eligible and is removed from the course offering.\n", courseID);
        //     break;
        // }
        if(doneSlotsTAs >= totalTAs && courses[courseID].isCourseAvailable==0){
            // printf("%d courses is getting deleted\n", courseID);
            courses[courseID].isCourseDeleted = 1;
            pthread_cond_broadcast(&courses[courseID].course_available);
            printf("Course %d doesn't have any TA's eligible and is removed from the course offering.\n", courseID);
            break;
        }
        courses[courseID].isCourseAvailable = 1;
        // if TA is alloted we run the tutes
        int temp = rand()%courses[courseID].totalSlots;
        if(temp == 0){
            temp = 1;
        }
        printf("Course %s has been alloted %d seats.\n", courses[courseID].name, temp);
        // printf("%d lolmaolol\n", temp);
        pthread_mutex_lock(&courses[courseID].course_lock);
        courses[courseID].leftSlots = temp;
        pthread_mutex_unlock(&courses[courseID].course_lock);
        pthread_cond_broadcast(&courses[courseID].course_available);
        courses[courseID].isCourseAvailable = 0;
        sleep(2);
        pthread_cond_broadcast(&courses[courseID].course_available);
        int isStudentLeft = 0;
        for(int j = 0; j < numStudents ; j++){
            pthread_mutex_lock(&students[j].student_lock);
            // int courseTest = 0
            for(int k = 0; k < 3 ; k++){
                if(students[j].preferences[k] == courseID){
                    // course is there in preference
                    if(students[j].prefVisited[k] == 0){
                        isStudentLeft = 1;
                    }
                }
            }
            pthread_mutex_unlock(&students[j].student_lock);
        }
        if(isStudentLeft == 0){
            // printf("lollmaolol\n"); 
            courses[courseID].isCourseDeleted = 1;
            pthread_cond_broadcast(&courses[courseID].course_available);
            printf("Course %d doesn't have any TA's eligible and is removed from the course offering.\n", courseID);
            break;
        }
        // while(1){
            pthread_mutex_lock(&courses[courseID].course_lock);
            if(temp != courses[courseID].leftSlots){
                printf("Tutorial has started for Course %s with %d seats filled out of %d\n",courses[courseID].name, temp-courses[courseID].leftSlots, temp);
                sleep(1);
                pthread_mutex_unlock(&courses[courseID].course_lock);
                pthread_cond_broadcast(&courses[courseID].runningTutes);
                // break;
            }
            else{
                pthread_mutex_unlock(&courses[courseID].course_lock);
                pthread_cond_broadcast(&courses[courseID].runningTutes);
                // pthread_mutex_lock(&labss[selectedLab].ta_mutex[selectedTA]);
                // labss[selectedLab].ta_current_number[selectedTA]--;
                // labss[selectedLab].ta_occupied[selectedTA] = 0;
                // pthread_mutex_unlock(&labss[selectedLab].ta_mutex[selectedTA]);
            }
        // }
        labss[selectedLab].ta_occupied[selectedTA] = 0;
        courses[courseID].currentLab = -1;
        courses[courseID].currentTA = -1;
    }
}

void *runStudent(void *args){
    int flag = 0;
    int studentID = ((struct Student *)args)->id;
    // sleep(students[studentID].pref_filling_time);
    for(int i = 0; i<3; i++){
        int courseID = students[studentID].preferences[i];
        int coursesAlloted = 0;
        while(1){
            pthread_mutex_lock(&courses[courseID].course_lock);
            if(courses[courseID].isCourseDeleted){
                pthread_mutex_unlock(&courses[courseID].course_lock);
                flag = 1;
                break;
            }
            pthread_mutex_unlock(&courses[courseID].course_lock);
            // add other lock here
            pthread_mutex_lock(&students[studentID].student_lock);
            pthread_cond_wait(&courses[courseID].course_available, &students[studentID].student_lock);
            pthread_mutex_unlock(&students[studentID].student_lock);
            // add other unlock here
            int tempFlag = 1;
            // while(1){
                tempFlag = pthread_mutex_lock(&courses[courseID].course_lock);
                if(courses[courseID].isCourseAvailable){
                    if(courses[courseID].leftSlots>0){
                        // printf("got course %d\n", studentID);
                        // printf("lollmaolol %d %d %d\n", courseID, i, courses[courseID].leftSlots);
                        courses[courseID].leftSlots--;
                        if(courses[courseID].leftSlots == 0){
                            courses[courseID].isCourseAvailable = 0;
                            // pthread_cond_broadcast(&courses[courseID].course_lock);
                        }
                        pthread_mutex_unlock(&courses[courseID].course_lock);
                        coursesAlloted = 1;
                        break;
                    }
                }
                pthread_mutex_unlock(&courses[courseID].course_lock);
                // if(tempFlag != 0){
                //     break;
                // }
            // }
            if(coursesAlloted){
                break;
            }
        }
        if(flag){
            continue;
        }
        printf("Student %d has been allocated a seat in course %s\n", studentID, courses[courseID].name);

        // course alloted
        // running the tutorial
        pthread_mutex_lock(&students[studentID].student_lock);
        students[studentID].prefVisited[i] = 1;
        pthread_cond_wait(&courses[courseID].runningTutes, &students[studentID].student_lock);
        pthread_mutex_unlock(&students[studentID].student_lock);
        // tute attended
        float prob = students[studentID].calibre*courses[courseID].interest;
        int TrueFalse = (rand()%100) < (int)(prob*100);
        if(TrueFalse){
            printf("Student %d has selected course %d permanently.\n", studentID, courseID);
        }else{
            printf("Student %d has withdrawn from the course %s.\n", studentID, courses[courseID].name);
            if(i<2){
                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d).\n", studentID, courses[courseID].name, i+1, courses[students[studentID].preferences[i+1]].name, i+2);
            }
        }
    }
}

int main(){
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);
    takeInput();
    for(int i = 0; i<numStudents; i++){
        // printf("\n");
        pthread_create(&studentThread[i], NULL, runStudent, (void*)(&students[i]));
    }
    // printf("lollmaolol");
    sleep(1);
    for(int i = 0; i<numCourses ; i++){
        // printf("\n");
        pthread_create(&courseThread[i], NULL, runCourse, (void*)(&courses[i]));
        // printf("%s %d %d\n",courses[i].name, courses[i].currentTA, courses[i].currentLab);
    }
    for(int i = 0; i < numStudents ; i++){
        pthread_join(studentThread[i], NULL);
    }
    for(int i = 0; i < numCourses ; i++){
        pthread_join(courseThread[i], NULL);
    }
    fflush(0);
    return 0;
}
