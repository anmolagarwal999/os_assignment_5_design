# Multi threaded Client and Server in C++

In this question, I implemented a basic client-server with multithreading. 

### Compliling the code :
To run the code you need to run the commands :

- g++ -pthread -o server server.cpp
- g++ -pthread -o client client.cpp
- run ./server first and then run ./client in another terminal

The server will start listening on the specified port.
The client program will ask you to first enter the number of commands you want to give. 
The first word of the command should be the sleep time, and then the string you want to pass. 

The client program will simulate the m commands at their repsective times as specified by the user. 


> NOTE : I could not implement the map portion of the assignment as I could not figure out how to make it thread safe






