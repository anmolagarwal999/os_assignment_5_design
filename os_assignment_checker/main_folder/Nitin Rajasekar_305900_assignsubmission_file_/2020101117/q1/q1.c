#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

int students_left;
int done = 0;
pthread_mutex_t c_mutex;
pthread_mutex_t l_mutex;
pthread_mutex_t s_mutex;
pthread_mutex_t g_mutex;
pthread_mutex_t cl_mutex;

struct course //define struct course
{
    int c_id;
    char name[100];

    float interest;
    int lim;
    int max_slots;
    int slots_available;
    int slots_remaining;
    int num_of_labs;
    int lab_array[100];
    struct lab *ll_s;
    int tut_done;
    int tut_start;
    int tut_between;
    int not_enough_TAs;
    int ta_alloc;
    int s_interested;
    int s_temp;
    int seats_allocated;
    pthread_cond_t tut_over;

    char state[100];
    struct course *next;
    pthread_mutex_t mutex;
};

struct lab //define struct lab
{
    int l_id;
    char name[100];
    int ta_num;
    int ta_lim;
    char state[100];
    struct lab *next;
    int free_ta_num;
    int TAs_courses_done[30];
    int TAs_availability[30];
    int all_TAs_done;
    pthread_mutex_t mutex;
};

struct student //define struct student
{
    int s_id;
    float calibre;
    int pref_array[100];
    int fill_time;
    struct course *cc_s;
    int pref_index;
    int allocated;
    int out;

    char state[100];
    struct student *next;
    pthread_mutex_t mutex;
};

struct TA //define struct TA
{
    char state[100];
    struct TA *next;
};

struct student *first_s;
struct lab *first_l;
struct course *first_c; //first instance, no mutex initialised, what begin_c becomes
//struct student* ll_s = first_s->next;
struct ta *first_t;

char *get_c_name_from_num(int index)
{
    char *name;
    struct course *cc_s = first_c->next;

    for (int j = 0; j <= index; j++)
    {
        name = cc_s->name;

        if (j == index)
            break;
        cc_s = cc_s->next;
    }

    return name;
}

void *lab_f(void *argument)
{
    struct lab *arg = (struct lab *)argument;

    return NULL;
}

void *course_f(void *argument)

{
    ///pthread_mutex_lock(&c_mutex);
    struct course *arg = (struct course *)argument;
    int no_of_labs_done = 0;
    int TAs_done = 0;
    int ta_found = 0;
    arg = (struct course *)argument;
    arg->slots_available = rand() % (arg->max_slots) + 1;
    //pthread_mutex_lock(&arg->mutex);
    /*int x;
    pthread_mutex_lock(&mutex_waiting_student);
    for(x=0;x<10;x++)
    {
        printf("X IS %d\n",x);
    }
    pthread_mutex_unlock(&mutex_waiting_student);*/
    //printf("the name is %s\n",ll_s->name);
    while (students_left > 0)
    {
        no_of_labs_done = 0;
        //arg->s_interested=0;
        //printf("1234\n");
        //printf("Students left is %d\n", students_left);
        char *curr_lab_name;
        //pthread_mutex_lock(&c_mutex); // allocating TA to course

        while (no_of_labs_done != arg->num_of_labs && ta_found == 0)
        {
            if (students_left == 0)
                break;
            //("%d %d %d %s  %d\n", no_of_labs_done, arg->num_of_labs, ta_found, arg->name, students_left);

            for (int i = 0; i < arg->num_of_labs; i++)
            {
                arg->ll_s = first_l->next; //each lab ll_s no it isn't modified
                pthread_mutex_lock(&cl_mutex);

                for (int j = 0; j <= arg->lab_array[i]; j++)
                {

                    curr_lab_name = arg->ll_s->name;

                    if (j == arg->lab_array[i])
                        break;
                    arg->ll_s = arg->ll_s->next;
                }
                pthread_mutex_unlock(&cl_mutex);
                /*if (arg->ll_s->all_TAs_done == 1)
                {
                    break;
                }*/
                TAs_done = 0;
                pthread_mutex_lock(&cl_mutex);
                int min = 50;

                for (int a = 0; a < arg->ll_s->ta_num; a++)
                {
                    if (arg->ll_s->TAs_courses_done[a] < min)
                        min = arg->ll_s->TAs_courses_done[a];
                }

                for (int a = 0; a < arg->ll_s->ta_num; a++)
                {
                    //printf("%s %d\n", arg->name, ta_found);
                    //printf("%s %d %d %d %s\n", arg->name, arg->ll_s->TAs_courses_done[a], arg->ll_s->ta_lim, arg->ll_s->TAs_availability[a], arg->ll_s->name);

                    if (arg->ll_s->TAs_courses_done[a] < arg->ll_s->ta_lim && arg->ll_s->TAs_availability[a] == 1 && arg->ll_s->TAs_courses_done[a] == min)
                    {
                        printf("TA %d from lab %s has been allocated to course %s for his %d TA ship\n", a, arg->ll_s->name, arg->name, arg->ll_s->TAs_courses_done[a] + 1);

                        ta_found = 1;
                        arg->ta_alloc = a;
                        arg->ll_s->TAs_courses_done[a]++;
                        arg->ll_s->TAs_availability[a] = -1;

                        break;
                    }
                    if (arg->ll_s->TAs_courses_done[a] == arg->ll_s->ta_lim)
                        TAs_done++;
                }
                pthread_mutex_unlock(&cl_mutex);

                if (ta_found == 1)
                    break;
                if (TAs_done == arg->ll_s->ta_num)
                {
                    pthread_mutex_lock(&cl_mutex);

                    arg->ll_s->all_TAs_done = 1;
                    pthread_mutex_unlock(&cl_mutex);

                    printf("Lab %s does not have any TAs for TA ship\n", arg->ll_s->name);

                    no_of_labs_done++;
                }
            }
        }

        //pthread_mutex_unlock(&c_mutex);
        if (no_of_labs_done == arg->num_of_labs && ta_found == 0)
        {
            printf("Course %s doesn't have any elligible TAs\n", arg->name);
            arg->not_enough_TAs = 1;
            ///pthread_mutex_unlock(&c_mutex);
            pthread_cond_signal(&arg->tut_over);
            //printf("STUDENTS LEFT IS %d %s\n", students_left, arg->name);

            return NULL;
        }

        //pthread_mutex_lock(&c_mutex);
        if (ta_found == 1)
        {
            arg->slots_available = rand() % (arg->max_slots) + 1;
            arg->slots_remaining = arg->slots_available;

            printf("Course %s has been allocated %d seats\n", arg->name, arg->slots_available);
            sleep(0.1);
            arg->seats_allocated = 1;
            //printf("NEW OVER HERE %s %d\n", arg->name, arg->s_interested);

            /*while (arg->s_interested == 0 && students_left != 0)
            {
                printf("NOT INTERESTED IN %s %d\n", arg->name, students_left);
                sleep(5);
            }*/

            //printf("Tutorial has started for course %s with %d seats filled out of %d\n", arg->name, arg->s_interested, arg->slots_available);
            //if (arg->s_temp > 0)
            {
                arg->s_interested = 0;

                printf("Tutorial has started for course %s %d filled at the start, %d can join\n", arg->name, arg->s_temp, arg->slots_available);
            }
            arg->tut_between = 1;

            arg->tut_start = 1;
            //arg->s_interested = 0;

            sleep(2);

            printf("TA %d from lab %s has completed the tutorial for course %s\n", arg->ta_alloc, arg->ll_s->name, arg->name);
            arg->tut_between = 0;
            arg->tut_done = 1;

            arg->ll_s->TAs_availability[arg->ta_alloc] = 1;

            pthread_mutex_lock(&arg->mutex);
            arg->ll_s->TAs_availability[arg->ta_alloc] = 1;
            arg->tut_done = 1;
            done = 1;
            //printf("%s\n", arg->name);
            pthread_cond_signal(&arg->tut_over);
            pthread_mutex_unlock(&arg->mutex);
        }
        //pthread_mutex_unlock(&c_mutex);

        //sleep(100);
        ///pthread_mutex_unlock(&c_mutex);
        //arg->ll_s->TAs_availability[arg->ta_alloc] = 1;

        ta_found = 0;
    }
    //printf("STUDENTS LEFT IS %d %s\n", students_left, arg->name);
    //pthread_mutex_unlock(&arg->mutex);
    //pthread_mutex_unlock(&c_mutex);
    pthread_cond_signal(&arg->tut_over);

    return NULL;
}

void *student_f(void *argument)

{
    //pthread_mutex_lock(&s_mutex);

    struct student *arg = (struct student *)argument;
    sleep(arg->fill_time);
    printf("Student %d has filled in preferences for course registration\n", arg->s_id);
    /*for(int h=0;h<50;h++)
    {
        printf("%d\n",arg->s_id);
    }*/

    sleep(0.2);
    arg->cc_s = first_c->next;
    char *curr_course_name;

    while (arg->allocated == 0 && arg->out == 0)
    {
        arg->cc_s = first_c->next;

        for (int j = 0; j <= arg->pref_array[arg->pref_index]; j++)
        {
            curr_course_name = arg->cc_s->name;
            //printf("%s\n", arg->cc_s->name);

            if (j == arg->pref_array[arg->pref_index])
                break;
            arg->cc_s = arg->cc_s->next;
        }
        //printf("STUDENT NUMBER IS %d\n", arg->s_id);

        if (arg->cc_s->not_enough_TAs == 1)
        {
            if (arg->pref_index == 2)
            {
                printf("Student %d cannot take %s as course has been removed\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]));
                sleep(0.2);

                printf("Student %d couldn't get any of his preferred courses\n", arg->s_id);
                arg->out = 1;
                pthread_mutex_lock(&s_mutex);

                students_left--;
                pthread_mutex_unlock(&s_mutex);
                //printf("STUDENT %d IS OUT\n", arg->s_id);
                return NULL;
            }
            else
            {
                printf("Student %d cannot take %s as course has been removed\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]));
                printf("Student %d has changed his preference from %s to %s\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]), get_c_name_from_num(arg->pref_array[arg->pref_index + 1]));
                arg->pref_index++;
                continue;
            }
        }
        /*while (arg->cc_s->tut_between==1)
        {
            printf("%d\n", arg->s_id);
        }
        arg->cc_s->seats_allocated = 0;

        while (arg->cc_s->seats_allocated == 0 && arg->cc_s->not_enough_TAs == 0)
        {
        }*/
        if (arg->cc_s->slots_available > arg->cc_s->s_interested)
        {
            //printf("E\n");
            //printf("%d %d\n",arg->cc_s->slots_available,arg->cc_s->s_interested);
            printf("Student %d has been allocated to %s\n", arg->s_id, arg->cc_s->name);
            arg->cc_s->s_temp = arg->cc_s->s_temp + 1;

            /*pthread_mutex_lock(&arg->cc_s->mutex);
            arg->cc_s->s_interested = arg->cc_s->s_interested + 1;
            pthread_mutex_unlock(&arg->cc_s->mutex);*/
            //printf("NEW %s %d\n", arg->cc_s->name, arg->cc_s->s_interested);

            //pthread_mutex_lock(&arg->cc_s->mutex);
            //printf("D\n");

            arg->cc_s->tut_done = 0;
            arg->cc_s->tut_start = 0;
            while (arg->cc_s->tut_start == 0 && arg->cc_s->not_enough_TAs == 0)
            {
            }
            while (arg->cc_s->tut_done == 0 && arg->cc_s->not_enough_TAs == 0)
            {
                //printf("wait %d\n", arg->s_id);
            }

            /*pthread_mutex_lock(&arg->cc_s->mutex);

            arg->cc_s->s_interested = arg->cc_s->s_interested - 1;
            pthread_mutex_unlock(&arg->cc_s->mutex);*/
            arg->cc_s->s_temp = arg->cc_s->s_temp - 1;

            //arg->cc_s->tut_done = 0;

            //pthread_mutex_unlock(&arg->cc_s->mutex);
            float probability = arg->calibre * arg->cc_s->interest;
            float random_factor = (rand() % 101) / 100.0;
            //printf("C\n");

            //printf("B\n");
            if (random_factor > probability && arg->pref_index < 2)
            {
                sleep(0.2);

                printf("Student %d has withdrawn from %s\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]));

                printf("Student %d has changed his preference from %s to %s\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]), get_c_name_from_num(arg->pref_array[arg->pref_index + 1]));
                arg->pref_index++;
                continue;
            }
            else if (random_factor > probability && arg->pref_index == 2)
            {
                printf("Student %d has withdrawn from %s\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]));

                printf("Student %d couldn't get any of his preferred courses\n", arg->s_id);
                arg->out = 1;
                pthread_mutex_lock(&s_mutex);

                students_left--;
                pthread_mutex_unlock(&s_mutex);
                //printf("STUDENT %d IS OUT\n", arg->s_id);

                return NULL;
            }
            //////
            else if (arg->cc_s->slots_available > arg->cc_s->s_interested)

            {
                sleep(0.2);

                printf("Student %d has been permanently allocated to %s\n", arg->s_id, get_c_name_from_num(arg->pref_array[arg->pref_index]));
                arg->cc_s->s_interested = arg->cc_s->s_interested + 1;

                arg->allocated = 1;
                pthread_mutex_lock(&s_mutex);

                students_left--;
                pthread_mutex_unlock(&s_mutex);
                //printf("STUDENT %d IS OUT\n", arg->s_id);

                return NULL;
            }
        }

        else
        {
            //printf("F\n");
            if (arg->pref_index < 2)
            {
                /*printf("No slots remaining, student %d moves on to the next preference\n", arg->s_id);
                arg->pref_index = arg->pref_index + 1;
                continue;*/

                printf("No slots remaining, student %d waits to get into %s\n", arg->s_id, arg->cc_s->name);
                arg->cc_s->tut_done = 0;

                while (arg->cc_s->tut_done == 0 && arg->cc_s->not_enough_TAs == 0)
                {
                    //printf("wait %d\n", arg->s_id);
                }
                continue;
            }
            else
            {
                /*printf("No slots remaining, student %d couldn't get any of his preferred courses\n", arg->s_id);
                //printf("%d %d\n", arg->cc_s->slots_available, arg->cc_s->s_interested);
                pthread_mutex_lock(&s_mutex);

                students_left--;
                pthread_mutex_unlock(&s_mutex);
                //printf("STUDENT %d IS OUT\n", arg->s_id);

                return NULL;*/

                printf("No slots remaining, student %d waits to get into %s\n", arg->s_id, arg->cc_s->name);
                arg->cc_s->tut_done = 0;

                while (arg->cc_s->tut_done == 0 && arg->cc_s->not_enough_TAs == 0)
                {
                    //printf("wait %d\n", arg->s_id);
                }
                continue;
            }
        }
        //printf("WHERE\n");
    }
    //pthread_mutex_unlock(&s_mutex);
    //printf("OUTSIDE\n");
    //printf("STUDENT %d IS OUT\n", arg->s_id);

    return NULL;
}

int main()
{
    pthread_mutex_init(&(c_mutex), NULL);
    pthread_mutex_init(&(l_mutex), NULL);
    pthread_mutex_init(&(s_mutex), NULL);
    pthread_mutex_init(&(g_mutex), NULL);

    pthread_mutex_init(&(cl_mutex), NULL);

    srand(time(0));

    int num_students, num_labs, num_courses;
    int j;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    students_left = num_students;

    first_c = (struct course *)malloc(sizeof(struct course));
    first_c->next = NULL;
    struct course *begin_c = first_c;
    for (j = 0; j < num_courses; j++)
    {
        struct course *upcoming = (struct course *)malloc(sizeof(struct course));
        upcoming->c_id = j;
        upcoming->tut_done = 0;
        upcoming->tut_start = 0;
        upcoming->tut_between = 0;

        scanf("%s", upcoming->name);

        scanf("%f", &upcoming->interest);
        scanf("%d", &upcoming->max_slots);
        scanf("%d", &upcoming->num_of_labs);
        for (int i = 0; i < upcoming->num_of_labs; i++)
        {
            scanf("%d", &upcoming->lab_array[i]);
        }
        upcoming->s_interested = 0;
        upcoming->s_temp = 0;
        upcoming->not_enough_TAs = 0;
        upcoming->seats_allocated = 0;
        upcoming->next = NULL;
        begin_c->next = upcoming;
        upcoming->c_id = j + 1;
        begin_c = upcoming;
    }

    first_s = (struct student *)malloc(sizeof(struct student));
    first_s->next = NULL;
    struct student *begin_s = first_s;
    for (j = 0; j < num_students; j++)
    {
        struct student *upcoming = (struct student *)malloc(sizeof(struct student));
        upcoming->s_id = j + 1;
        scanf("%f", &upcoming->calibre);
        for (int i = 0; i < 3; i++)
        {
            scanf("%d", &upcoming->pref_array[i]);
        }
        scanf("%d", &upcoming->fill_time);
        upcoming->pref_index = 0;
        upcoming->out = 0;
        upcoming->allocated = 0;
        upcoming->next = NULL;
        upcoming->s_id = j;
        begin_s->next = upcoming;
        begin_s = upcoming;
    }

    first_l = (struct lab *)malloc(sizeof(struct lab));
    first_l->next = NULL;
    struct lab *begin_l = first_l;
    for (j = 0; j < num_labs; j++)
    {
        struct lab *upcoming = (struct lab *)malloc(sizeof(struct lab));
        scanf("%s", upcoming->name);
        scanf("%d", &upcoming->ta_num);

        scanf("%d", &upcoming->ta_lim);
        upcoming->free_ta_num = 0;
        upcoming->all_TAs_done = 0;
        for (int a = 0; a < 30; a++)
        {
            upcoming->TAs_courses_done[a] = 0;
            upcoming->TAs_availability[a] = 1;
        }

        upcoming->next = NULL;
        upcoming->l_id = j + 1;
        begin_l->next = upcoming;
        begin_l = upcoming;
    }

    pthread_t lab_t[num_labs];
    pthread_t course_t[num_courses];
    pthread_t student_t[num_students];

    begin_c = first_c->next;
    for (j = 0; j < num_courses; j++)
    {
        pthread_mutex_init(&(begin_c->mutex), NULL);
        pthread_create(&course_t[j], NULL, course_f, (void *)begin_c);
        begin_c = begin_c->next;
    }
    begin_s = first_s->next;
    for (j = 0; j < num_students; j++)
    {
        pthread_mutex_init(&(begin_s->mutex), NULL);
        pthread_create(&student_t[j], NULL, student_f, (void *)begin_s);
        begin_s = begin_s->next;
    }

    begin_l = first_l->next;
    for (j = 0; j < num_labs; j++)
    {
        pthread_mutex_init(&(begin_l->mutex), NULL);
        pthread_create(&lab_t[j], NULL, lab_f, (void *)begin_l);
        begin_l = begin_l->next;
    }

    //joining threads

    for (j = 0; j < num_courses; j++)
    {
        pthread_join(course_t[j], NULL);
    }
    for (j = 0; j < num_labs; j++)
    {
        pthread_join(lab_t[j], NULL);
    }
    for (j = 0; j < num_students; j++)
    {
        pthread_join(student_t[j], NULL);
    }

    //destroying threads

    begin_l = first_l->next;
    for (j = 0; j < num_labs; j++)
    {
        pthread_mutex_destroy(&(begin_l->mutex));
        begin_l = begin_l->next;
    }

    begin_c = first_c->next;
    for (j = 0; j < num_courses; j++)
    {
        pthread_cond_signal(&(begin_c->tut_over));

        pthread_mutex_destroy(&(begin_c->mutex));
        begin_c = begin_c->next;
    }
    begin_s = first_s->next;
    for (j = 0; j < num_students; j++)
    {
        pthread_mutex_destroy(&(begin_s->mutex));
        begin_s = begin_s->next;
    }
}
