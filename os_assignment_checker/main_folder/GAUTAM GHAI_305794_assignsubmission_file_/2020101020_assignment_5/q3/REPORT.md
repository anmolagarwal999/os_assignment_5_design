# Running the files
+ For running the server file, do `g++ server.cpp -o server -lpthread`. Run using `./server < no. of threads in threadpool >`.
+ For running client file do `g++ client.cpp -o client -lpthread`. Run using `./client`.

# Working Description
- Every request is sent as a thread from client. Then the threads are joined using `pthread_join`.
- The no. of threads in the server side threadpool are inputted by user
- The server makes use of queue data structure where the incoming requests get accumulted and working threads pick them up.
- No busy waiting, if no. of threads > no. of requests, the extra threads won't consume CPU cycles
- Server side shows output on the console(terminal)
- The client side executable finishes upon sending all requests.
- Server can be killed by pressing Ctrl+C.