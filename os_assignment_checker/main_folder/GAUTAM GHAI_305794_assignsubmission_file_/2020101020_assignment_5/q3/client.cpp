#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pthread_mutex_t lock=PTHREAD_MUTEX_INITIALIZER;
struct command
{
    int time;
    int id;
    string operation;
} typedef command;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *st)
{
    //string s = *(string *)(st);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    command *ops = (command *)st;
    cout << "Connection to server successful" << endl;
    //int i=0;
    //while (i<n)

    string to_send=ops->operation.substr(1,ops->operation.length()-1);
    int x=ops->time;
    sleep(x);
    pthread_mutex_lock(&lock);
   // cout << "Enter msg: ";
    //getline(cin, to_send);
    send_string_on_socket(socket_fd, to_send);
    pthread_mutex_unlock(&lock);
    int num_bytes_read;
    string output_msg;
    
    pthread_mutex_lock(&lock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    //strcpy(str,output_msg);
    //char str[]=
    printf("%d : ",ops->id);
    cout << output_msg << endl;
    //cout << "Received: " << output_msg << endl;
    printf("===\n");
    //cout << "====" << endl;
    pthread_mutex_unlock(&lock);
    //i++;

    return NULL;
    // part;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    scanf("%d", &n);
    //char A[n][100];
    command list[n];
    pthread_mutex_init(&lock,NULL);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &list[i].time);
        getline(cin, list[i].operation);
        list[i].id = i;
        //list[i].operation=
        //getline(cin,A[i]);
        // scanf("%s", A[i]);
        ///cin >> A[i];
        //cout << A[i]<<endl;
    }
    pthread_t th_clients[n];
    for (int i = 0; i < n; i++)
    {
        //pthread_t t;
        pthread_create(&th_clients[i], NULL, begin_process, &list[i]);
        //pthread_join(t,NULL);
        // begin_process(n);
    }
    for (int i = 0; i < n; i++)
    {
        pthread_join(th_clients[i], NULL);
    }
    exit(0);
}