#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

struct Removed_Courses
{
    int course_id;
    struct Removed_Courses *next;
};

struct Removed_Courses *rmc_head = NULL;
pthread_mutex_t rmc_lock = PTHREAD_MUTEX_INITIALIZER;

struct Course *courses;
struct Student *students;
struct Lab *labbs;

struct Course
{
    char name[50];
    float interest_quotient;
    int max_no_of_seats;
    int no_of_seats_allocated;
    int no_of_seats_filled;
    int no_of_labs_to_source_TAs;
    int state;
    int *labs;

    int TA_no;
    char *TA_lab;

    int waiting;

    pthread_mutex_t lock;
    pthread_cond_t student_cond;
    pthread_cond_t course_cond;
    pthread_cond_t tut_cond;
};

struct Student
{
    float calibre_quotient;
    int pref[3];
    int time_taken;
    int course_allocated;

    pthread_mutex_t lock;
};

struct TA
{
    int allocated;
    int nth_TA_ship;

    pthread_mutex_t lock;
};

struct Lab
{
    char name[50];
    int no_of_TAs;
    int no_of_times_a_TA_can_be_assigned;

    int no_of_assignments_left;
    struct TA *TAs;

    pthread_mutex_t lock;
};

void add_rmc(int course_id)
{
    struct Removed_Courses *node = (struct Removed_Courses *)malloc(sizeof(struct Removed_Courses));
    node->course_id = course_id;
    node->next = NULL;

    if (rmc_head == NULL)
    {
        rmc_head = node;
    }
    else
    {
        node->next = rmc_head;
        rmc_head = node;
    }
}

int check_rmc(int course_id)
{
    pthread_mutex_lock(&rmc_lock);
    struct Removed_Courses *ptr = rmc_head;
    while (ptr != NULL)
    {
        pthread_cond_broadcast(&courses[ptr->course_id].course_cond);
        if (ptr->course_id == course_id)
        {
            pthread_mutex_unlock(&rmc_lock);
            return 1;
        }
        ptr = ptr->next;
    }
    pthread_mutex_unlock(&rmc_lock);
    return 0;
}

int check_TA_avail(int course_id)
{
    int i, j, k;
    for (i = 0; i < courses[course_id].no_of_labs_to_source_TAs; i++)
    {
        pthread_mutex_lock(&labbs[courses[course_id].labs[i]].lock);
        if(labbs[courses[course_id].labs[i]].no_of_assignments_left > 0){
            pthread_mutex_unlock(&labbs[courses[course_id].labs[i]].lock);
            return 1;
        }
        pthread_mutex_unlock(&labbs[courses[course_id].labs[i]].lock);
        // for (j = 0; j < labbs[courses[course_id].labs[i]].no_of_TAs; j++)
        // {
        //     pthread_mutex_lock(&labbs[courses[course_id].labs[i]].TAs[j].lock);
        //     if (labbs[courses[course_id].labs[i]].no_of_times_a_TA_can_be_assigned > labbs[courses[course_id].labs[i]].TAs[j].nth_TA_ship)
        //     {
        //         pthread_mutex_unlock(&labbs[courses[course_id].labs[i]].TAs[j].lock);
        //         return 1;
        //     }
        //     pthread_mutex_unlock(&labbs[courses[course_id].labs[i]].TAs[j].lock);
        // }
    }
    return 0;
}

void *student_thread(void *arg)
{
    // struct Student *student = (struct Student *)arg;
    // sleep(student->time_taken);
    // printf("Student %d has filled in preferences for courses registration\n", student->id);
    int index = *(int *)arg;
    free(arg);
    sleep(students[index].time_taken);
    printf("Student %d has filled in preferences for courses registration\n", index);

    int pref = 0, prev_pref = 0;

    while (pref < 3)
    {
        if (check_rmc(students[index].pref[pref]) == 1)
        {
            pref++;
            continue;
        }
        pthread_mutex_lock(&courses[students[index].pref[pref]].lock);
        courses[students[index].pref[pref]].waiting++;
        // sleep(3);
        // pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
        // pthread_mutex_lock(&courses[students[index].pref[pref]].lock);
        pthread_cond_signal(&courses[students[index].pref[pref]].student_cond);
        do
        {
            pthread_cond_wait(&courses[students[index].pref[pref]].course_cond, &courses[students[index].pref[pref]].lock);
        } while (courses[students[index].pref[pref]].no_of_seats_allocated <= courses[students[index].pref[pref]].no_of_seats_filled && check_rmc(students[index].pref[pref]) == 0);
        courses[students[index].pref[pref]].no_of_seats_filled++;
        // pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
        // pthread_mutex_lock(&courses[students[index].pref[pref]].lock);
        if (check_rmc(students[index].pref[pref]) == 1)
        {
            // printf("Student %d has withdrawn from course %s\n", index, courses[students[index].pref[pref]].name);
            // prev_pref = pref;
            while (check_rmc(students[index].pref[pref + 1]) == 1 && pref < 2)
            {
                pref++;
            }
            if (pref < 2 && check_rmc(students[index].pref[pref + 1]) == 0)
            {
                printf(YELLOW"Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n"RESET, index, courses[students[index].pref[prev_pref]].name, prev_pref + 1, courses[students[index].pref[pref + 1]].name, pref + 2);
                pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
                pref++;
                continue;
                // printf("\nCourse name = %s\n", courses[students[index].pref[pref]].name);
            }
            else
            {
                printf(RED"Student %d couldn't get any of his preferred courses\n"RESET, index);
                pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
                return NULL;
            }
        }
        printf(BLUE"Student %d has been allocated a seat in course %s\n"RESET, index, courses[students[index].pref[pref]].name);
        students[index].course_allocated = students[index].pref[pref];
        // sleep(3);
        pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
        // pthread_mutex_lock(&courses[students[index].pref[pref]].lock);
        // pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
        pthread_cond_signal(&courses[students[index].pref[pref]].tut_cond);
        pthread_mutex_lock(&courses[students[index].pref[pref]].lock);
        pthread_cond_wait(&courses[students[index].pref[pref]].course_cond, &courses[students[index].pref[pref]].lock);
        if (students[index].calibre_quotient * courses[students[index].pref[pref]].interest_quotient > (float)((float)(rand()) / ((float)(RAND_MAX) * 2)))
        // if (students[index].calibre_quotient * courses[students[index].pref[pref]].interest_quotient > 0.05)
        {
            printf(GREEN"Student %d has selected the course %s permanently\n"RESET, index, courses[students[index].pref[pref]].name);
            pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
            return NULL;
        }
        else
        {
            printf(MAGENTA"Student %d has withdrawn from course %s\n", index, courses[students[index].pref[pref]].name);
            prev_pref = pref;
            while (check_rmc(students[index].pref[pref + 1]) == 1 && pref < 2)
            {
                pref++;
            }
            if (pref < 2 && check_rmc(students[index].pref[pref + 1]) == 0)
            {
                printf(BLUE"Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n"RESET, index, courses[students[index].pref[prev_pref]].name, prev_pref + 1, courses[students[index].pref[pref + 1]].name, pref + 2);
                pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
                pref++;
                // printf("\nCourse name = %s\n", courses[students[index].pref[pref]].name);
            }
            else
            {
                printf(RED"Student %d couldn't get any of his preferred courses\n"RESET, index);
                pthread_mutex_unlock(&courses[students[index].pref[pref]].lock);
                return NULL;
            }
        }
    }
    printf("Student %d couldn't get any of his preferred courses\n", index);
    return NULL;
}

void *course_thread(void *arg)
{
    int index = *(int *)arg;
    free(arg);
    int i, j;
    while (1)
    {
        pthread_mutex_lock(&courses[index].lock);
        if (check_TA_avail(index) == 0)
        {
            printf(RED"Course %s doesn't have any TA's eligible and is removed from course offerings\n"RESET, courses[index].name);
            courses[index].no_of_seats_allocated = -1;
            add_rmc(index);
            // pthread_mutex_unlock(&courses[index].lock); 
            // pthread_mutex_lock(&courses[index].lock); 
            pthread_mutex_unlock(&courses[index].lock); 
            // sleep(3);
            pthread_cond_broadcast(&courses[index].course_cond);
            // pthread_mutex_unlock(&labbs[courses[index].labs[i]].TAs[j].lock);
            return NULL;
        }
        pthread_mutex_unlock(&courses[index].lock);
        // pthread_cond_wait(&courses[index].student_cond, &courses[index].lock);
        while (courses[index].waiting == 0);
        pthread_mutex_lock(&courses[index].lock);
            // pthread_cond_wait(&courses[index].student_cond, &courses[index].lock);
        
        while (1)
        {
            for (i = 0; i < courses[index].no_of_labs_to_source_TAs; i++)
            {
                for (j = 0; j < labbs[courses[index].labs[i]].no_of_TAs; j++)
                {
                    pthread_mutex_lock(&labbs[courses[index].labs[i]].lock);
                    if (pthread_mutex_trylock(&labbs[courses[index].labs[i]].TAs[j].lock) == 0)
                    {
                        if (labbs[courses[index].labs[i]].TAs[j].allocated == 0)
                        {
                            labbs[courses[index].labs[i]].TAs[j].allocated = 1;
                            printf(BLUE"TA %d from lab %s has been allocated to course %s for his %d TA ship\n"RESET, j, labbs[courses[index].labs[i]].name, courses[index].name, labbs[courses[index].labs[i]].TAs[j].nth_TA_ship);
                            labbs[courses[index].labs[i]].TAs[j].nth_TA_ship++;
                            labbs[courses[index].labs[i]].no_of_assignments_left--;
                            courses[index].TA_no = j;
                            courses[index].TA_lab = labbs[courses[index].labs[i]].name;
                            pthread_mutex_unlock(&labbs[courses[index].labs[i]].lock);
                            break;
                        }
                    }
                    pthread_mutex_unlock(&labbs[courses[index].labs[i]].lock);
                }
                if (j < labbs[courses[index].labs[i]].no_of_TAs)
                    break;
            }
            if (i < courses[index].no_of_labs_to_source_TAs)
                break;
        }
        courses[index].no_of_seats_allocated = rand() % courses[index].max_no_of_seats + 1;
        printf(GREEN"Course %s has been allocated %d seats\n"RESET, courses[index].name, courses[index].no_of_seats_allocated);
        // pthread_mutex_unlock(&courses[index].lock);
        // pthread_mutex_lock(&courses[index].lock);
        pthread_mutex_unlock(&courses[index].lock);
        pthread_cond_broadcast(&courses[index].course_cond);
        while (courses[index].no_of_seats_filled == 0);
            // pthread_cond_wait(&courses[index].tut_cond, &courses[index].lock);
        // sleep(1);
        pthread_mutex_lock(&courses[index].lock);
        if (courses[index].no_of_seats_filled == 1)
            printf(MAGENTA"Tutorial has started for course %s with 1 seat filled out of %d\n"RESET, courses[index].name, courses[index].no_of_seats_allocated);
        else
            printf(MAGENTA"Tutorial has started for course %s with %d seats filled out of %d\n"RESET, courses[index].name, courses[index].no_of_seats_filled, courses[index].no_of_seats_allocated);
        courses[index].waiting -= courses[index].no_of_seats_filled;
        pthread_mutex_unlock(&courses[index].lock);
        sleep(2);
        pthread_mutex_lock(&courses[index].lock);
        printf(CYAN"TA %d from lab %s has completed the tutorial for the course %s\n"RESET, courses[index].TA_no, courses[index].TA_lab, courses[index].name);
        // pthread_cond_broadcast(&courses[index].course_cond);
        labbs[courses[index].labs[i]].TAs[j].allocated = 0;
        pthread_mutex_unlock(&labbs[courses[index].labs[i]].TAs[j].lock);
        courses[index].TA_no = 0;
        courses[index].TA_lab = NULL;
        courses[index].no_of_seats_filled = 0;
        courses[index].no_of_seats_allocated = 0;
        pthread_mutex_unlock(&courses[index].lock);
        // pthread_mutex_lock(&courses[index].lock);
        // pthread_mutex_unlock(&courses[index].lock);
        // sleep(3);
        pthread_cond_broadcast(&courses[index].course_cond);
    }
    return NULL;
}

int main()
{
    int no_of_students, no_of_labs, no_of_courses, i, j, k;
    scanf("%d %d %d", &no_of_students, &no_of_labs, &no_of_courses);

    // struct Course courses[no_of_courses];
    // struct Student students[no_of_students];
    // struct Lab labbs[no_of_labs];

    courses = (struct Course *)malloc(sizeof(struct Course) * no_of_courses);
    students = (struct Student *)malloc(sizeof(struct Student) * no_of_students);
    labbs = (struct Lab *)malloc(sizeof(struct Lab) * no_of_labs);

    for (i = 0; i < no_of_courses; i++)
    {
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest_quotient, &courses[i].max_no_of_seats, &courses[i].no_of_labs_to_source_TAs);
        courses[i].no_of_seats_allocated = 0;
        courses[i].no_of_seats_filled = 0;
        courses[i].state = 0;
        courses[i].TA_no = 0;
        courses[i].TA_lab = NULL;
        courses[i].waiting == 0;
        courses[i].labs = (int *)malloc(sizeof(int) * courses[i].no_of_labs_to_source_TAs);
        for (j = 0; j < courses[i].no_of_labs_to_source_TAs; j++)
        {
            scanf("%d", &courses[i].labs[j]);
        }
        if (pthread_mutex_init(&courses[i].lock, NULL) != 0)
        {
            printf("\n mutex init failed\n");
            return 1;
        }
    }

    for (i = 0; i < no_of_students; i++)
    {
        scanf("%f %d %d %d %d", &students[i].calibre_quotient, &students[i].pref[0], &students[i].pref[1], &students[i].pref[2], &students[i].time_taken);
        students[i].course_allocated = -1;
        if (pthread_mutex_init(&students[i].lock, NULL) != 0)
        {
            printf("\n mutex init failed\n");
            return 1;
        }
    }
    for (i = 0; i < no_of_labs; i++)
    {
        scanf("%s %d %d", labbs[i].name, &labbs[i].no_of_TAs, &labbs[i].no_of_times_a_TA_can_be_assigned);
        labbs[i].no_of_assignments_left = labbs[i].no_of_TAs * labbs[i].no_of_times_a_TA_can_be_assigned;
        labbs[i].TAs = (struct TA *)malloc(sizeof(struct TA) * labbs[i].no_of_TAs);
        for (j = 0; j < labbs[i].no_of_TAs; j++)
        {
            labbs[i].TAs[j].allocated = 0;
            labbs[i].TAs[j].nth_TA_ship = 1;
            if (pthread_mutex_init(&labbs[i].TAs[j].lock, NULL) != 0)
            {
                printf("\n mutex init failed\n");
                return 1;
            }
        }
    }

    pthread_t student_threads[no_of_students];
    for (i = 0; i < no_of_students; i++)
    {
        int *temp = (int *)malloc(sizeof(int));
        *temp = i;
        pthread_create(&student_threads[i], NULL, student_thread, temp);
    }

    pthread_t course_threads[no_of_courses];
    for (i = 0; i < no_of_courses; i++)
    {
        int *temp = (int *)malloc(sizeof(int));
        *temp = i;
        pthread_create(&course_threads[i], NULL, course_thread, temp);
    }

    for (i = 0; i < no_of_students; i++)
    {
        pthread_join(student_threads[i], NULL);
        // printf("\nNo dd for %d\n\n", i); 
    }

    // sleep(20);
}