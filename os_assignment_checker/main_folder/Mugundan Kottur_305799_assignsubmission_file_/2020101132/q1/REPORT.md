# Q1 - Course Allocation

## How to Run

Run the [Makefile](./Makefile) inorder to get the compiled file. Then execute the compiled file, `./q1` and then enter the input according the format specified by the question.

## [q1.c](./q1.c)

There are 2 types of threads: Students and Courses.

- At the beginning, the data of Courses, Students, labs and TAs is stored in respective stuctures. 

- Each Course then goes into a wait for a Student.

- As a Student comes which is interested in a Course comes up, it will first check if the Course has been removed.

- If the Course was not removed, the Course is then awoken from wait and the Student then goes into a wait, waiting the Course to have seats and a TA allocated

- The Course after waking up checks if there are available TAs and loops till it gets one that still has empty slots

- Once the Course finds a TA that is free, the TA is allocated to the Course

- In case there are no TAs that are free, the Course no longer has eligible TAs and is removed. A broadcast is then made to all Students who were waiting on that Course.

- After getting alloted a TA, the seats are alloted randomly and the Students are awoken.

- After a Student wakes up, the Course is then checked, whether it has been removed or not.

- In the case where the Course has been removed, the Students are then made to change preferences and if no more Courses are available, they exit with none of their preferred Courses.

- In the case where the Course has not been removed, the Students are allowed to fill seats till all seats are filled and wake the Course at the same time.

- The Course waits till at least one seat is filled before waking up

- The Students are then made to wait while the Course will then conduct a sleep that acts as the duration of the Course. The Course then wakes the Students up, after which, they can choose if they want to permanently use that Course or switch

- After the tutorial ends, the Course goes back to the beginning and waits for Students

- After a Student runs out of Courses or chooses permanently, the Student exits

- The other Students then repeats the process until they either choose a Course permanently or they run out of preferred Courses

- The main thread then collects all Student threads as they are the ones that need to end. The Course threads automatically end alongside the main thread