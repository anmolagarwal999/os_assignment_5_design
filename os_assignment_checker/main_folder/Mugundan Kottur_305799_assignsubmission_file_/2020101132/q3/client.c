#include <netdb.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#define MAX 1024
#define PORT 8080
#define SA struct sockaddr

struct Data {
	int client_id;
	char *command;
};

void func(int sockfd, struct Data *data)
{
	char buff[MAX];
	int n = 0;
	write(sockfd, data->command, strlen(data->command));
	bzero(buff, sizeof(buff));
	read(sockfd, buff, MAX);
	printf("%d:%s", data->client_id, buff);
}

char **parse(char string[], char delim[], int *no_of_parts)
{
	char *part, *saveptr;
	char temp[1024];
	int i = 0;
	strcpy(temp, string);
	*no_of_parts = 0;
	for (part = strtok_r(temp, delim, &saveptr); part != NULL; part = strtok_r(NULL, delim, &saveptr))
	{
		(*no_of_parts)++;
	}

	strcpy(temp, string);

	char **parts = (char **)calloc((*no_of_parts + 1), sizeof(char *));

	// walk through the tokenized buffer again
	part = string;

	for (part = strtok_r(temp, delim, &saveptr); part != NULL; part = strtok_r(NULL, delim, &saveptr))
	{
		parts[i] = (char *)calloc(strlen(part) + 2, sizeof(char));
		strcpy(parts[i], part);
		i++;
	}
	parts[*no_of_parts] = NULL;
	return parts;
}

void freemem(char ***mem, int no)
{
    for (int i = no; i >= 0; i--)
    {
        free((*mem)[i]);
    }
    free(*mem);
}

void* client(void *arg)
{
	int no_of_parts = 0;
	struct Data *data = (struct Data *)arg;
	char **parts = parse(data->command, " \n", &no_of_parts);
	if(no_of_parts < 2)
	{
		printf("Invalid command\n");
		return NULL;
	}
	sleep(atoi(parts[0]));
	int sockfd, connfd;
	struct sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		exit(0);
	}

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
	{
		fprintf(stderr, "Unable to Connect\n");
		exit(0);
	}

	func(sockfd, data);
	close(sockfd);
	free(data->command);
	free(data);
	freemem(&parts, no_of_parts);
}

int main()
{
	int no_of_clients, i, n;

	scanf("%d", &no_of_clients);

	pthread_t client_threads[no_of_clients];

	getchar();

	for(i = 0; i < no_of_clients; i++)
    {
		struct Data *data = (struct Data *)malloc(sizeof(struct Data));
        data->command = (char*)malloc(sizeof(char) * MAX);
		data->client_id = i;
		n = 0;
		while ((data->command[n++] = getchar()) != '\n');
        pthread_create(&client_threads[i], NULL, client, data);
    }
	for(i = 0; i < no_of_clients; i++)
    {
        pthread_join(client_threads[i], NULL);
    }
	return 0;
}