#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#define MAX 1024
#define PORT 8080
#define SA struct sockaddr

struct Socket
{
	int sockfd;
	struct Socket *next;
	struct Socket *prev;
};

char **dictionary;
pthread_mutex_t dic_lock[100];

struct Socket *head = NULL;
struct Socket *tail = NULL;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void enq(int sockfd)
{
	struct Socket *newSocket = (struct Socket *)malloc(sizeof(struct Socket));
	newSocket->sockfd = sockfd;
	newSocket->next = NULL;
	newSocket->prev = NULL;
	if (head == NULL)
	{
		head = newSocket;
		tail = newSocket;
	}
	else
	{
		tail->next = newSocket;
		newSocket->prev = tail;
		tail = newSocket;
	}
}

int deq()
{
	if (head == NULL)
	{
		return -1;
	}
	int sockfd = head->sockfd;
	if (head == tail)
	{
		head = NULL;
		tail = NULL;
	}
	else
	{
		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
	return sockfd;
}

char **parse(char string[], char delim[], int *no_of_parts)
{
	char *part, *saveptr;
	char temp[1024];
	int i = 0;
	strcpy(temp, string);
	*no_of_parts = 0;
	for (part = strtok_r(temp, delim, &saveptr); part != NULL; part = strtok_r(NULL, delim, &saveptr))
	{
		(*no_of_parts)++;
	}

	strcpy(temp, string);

	char **parts = (char **)calloc((*no_of_parts + 1), sizeof(char *));

	// walk through the tokenized buffer again
	part = string;

	for (part = strtok_r(temp, delim, &saveptr); part != NULL; part = strtok_r(NULL, delim, &saveptr))
	{
		parts[i] = (char *)calloc(strlen(part) + 2, sizeof(char));
		strcpy(parts[i], part);
		i++;
	}
	parts[*no_of_parts] = NULL;
	return parts;
}

void freemem(char ***mem, int no)
{
	for (int i = no; i >= 0; i--)
	{
		free((*mem)[i]);
	}
	free(*mem);
}

void insert(char *value, int key, char *buff)
{
	if (dictionary[key] == NULL)
	{
		dictionary[key] = strdup(value);
		strcat(buff, "Insertion Successful\n");
		// printf("Insertion Successful\n");
	}
	else
	{
		strcat(buff, "Key already exists\n");
		// printf("Key already exists\n");
	}
}

void delete (int key, char *buff)
{
	if (dictionary[key] == NULL)
	{
		strcat(buff, "No such key exists\n");
		// printf("No such key exists\n");
	}
	else
	{
		free(dictionary[key]);
		dictionary[key] = NULL;
		strcat(buff, "Deletion Successful\n");
		// printf("Deletion Successful\n");
	}
}

void update(char *value, int key, char *buff)
{
	if (dictionary[key] != NULL)
	{
		dictionary[key] = strdup(value);
		strcat(buff, value);
		strcat(buff, "\n");
		// printf("Insertion Successful\n");
	}
	else
	{
		strcat(buff, "No such key exists\n");
		// printf("Key already exists\n");
	}
}

void fetch(int key, char* buff)
{
	if(dictionary[key] != NULL)
	{
		strcat(buff, dictionary[key]);
		strcat(buff, "\n");
	}
	else
	{
		strcat(buff, "Key does not exist\n");
	}
}

void concat(int key1, int key2, char *buff)
{
	if (dictionary[key1] != NULL && dictionary[key2] != NULL)
	{
		char *temp1 = (char *)calloc(strlen(dictionary[key1]) + strlen(dictionary[key2]) + 2, sizeof(char));
		char *temp2 = (char *)calloc(strlen(dictionary[key1]) + strlen(dictionary[key2]) + 2, sizeof(char));
		strcpy(temp1, dictionary[key1]);
		strcat(temp1, dictionary[key2]);
		strcpy(temp2, dictionary[key2]);
		strcat(temp2, dictionary[key1]);
		free(dictionary[key1]);
		dictionary[key1] = temp1;
		free(dictionary[key2]);
		dictionary[key2] = temp2;
		strcat(buff, dictionary[key2]);
		strcat(buff, "\n");
	}
	else
	{
		strcat(buff, "Concat failed as at least one of the keys does not exist\n");
	}
}

// use multiple threads to handle clients
void server(int sockfd)
{
	char buff[MAX];
	int n, no_of_parts;
	bzero(buff, MAX);

	// read the message from client and copy it in buffer
	read(sockfd, buff, MAX);

	char **parsed = parse(buff, " \n", &no_of_parts);

	bzero(buff, MAX);

	sprintf(buff, "%lu:", (unsigned long int)pthread_self());
	// sprintf(buff, "%lld:", (long long int)gettid());
	if (strcmp(parsed[1], "insert") == 0)
	{
		if (no_of_parts == 4)
		{
			pthread_mutex_lock(&dic_lock[atoi(parsed[2])]);
			insert(parsed[3], atoi(parsed[2]), buff);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[2])]);
		}
		else
		{
			strcat(buff, "Incorrect number of arguments\n");
		}
	}
	else if (strcmp(parsed[1], "delete") == 0)
	{
		if (no_of_parts == 3)
		{
			pthread_mutex_lock(&dic_lock[atoi(parsed[2])]);
			delete (atoi(parsed[2]), buff);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[2])]);
		}
		else
		{
			strcat(buff, "Incorrect number of arguments\n");
		}
	}
	else if (strcmp(parsed[1], "concat") == 0)
	{
		if (no_of_parts == 4)
		{
			pthread_mutex_lock(&dic_lock[atoi(parsed[2])]);
			pthread_mutex_lock(&dic_lock[atoi(parsed[3])]);
			concat(atoi(parsed[2]), atoi(parsed[3]), buff);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[2])]);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[3])]);
		}
		else
		{
			strcat(buff, "Incorrect number of arguments\n");
		}
	}
	else if (strcmp(parsed[1], "update") == 0)
	{
		if (no_of_parts == 4)
		{
			pthread_mutex_lock(&dic_lock[atoi(parsed[2])]);
			update(parsed[3], atoi(parsed[2]), buff);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[2])]);
		}
		else
		{
			strcat(buff, "Incorrect number of arguments\n");
		}
	}
	else if (strcmp(parsed[1], "fetch") == 0)
	{
		if (no_of_parts == 3)
		{
			pthread_mutex_lock(&dic_lock[atoi(parsed[2])]);
			fetch(atoi(parsed[2]), buff);
			pthread_mutex_unlock(&dic_lock[atoi(parsed[2])]);
		}
		else
		{
			strcat(buff, "Incorrect number of arguments\n");
		}
	}
	else
	{
		strcat(buff, "Incorrect command\n");
	}

	write(sockfd, buff, strlen(buff));
	freemem(&parsed, no_of_parts);
}

void *worker_thread(void *arg)
{
	while (1)
	{
		pthread_mutex_lock(&lock);
		if (head == NULL)
		{
			pthread_cond_wait(&cond, &lock);
		}
		int sockfd = deq();
		pthread_mutex_unlock(&lock);
		server(sockfd);
		close(sockfd);
	}
	return NULL;
}

// Driver function
void client_search()
{
	int sockfd, connfd, len;
	struct sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		exit(0);
	}

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT);

	if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
	{
		fprintf(stderr, "Unable to Bind\n");
		exit(0);
	}

	if ((listen(sockfd, 5)) != 0)
	{
		fprintf(stderr, "Unable to Listen\n");
		exit(0);
	}
	len = sizeof(cli);

	while (1)
	{
		connfd = accept(sockfd, (SA *)&cli, &len);
		if (connfd < 0)
		{
			fprintf(stderr, "Unable to Accept\n");
			exit(0);
		}
		pthread_mutex_lock(&lock);
		enq(connfd);
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&lock);
	}
}

int main(int argc, char *argv[])
{
	dictionary = (char **)calloc(100, sizeof(char *));

	for (int i = 0; i < 100; i++)
	{
		pthread_mutex_init(&dic_lock[i], NULL);
	}

	if (argc > 1)
	{
		int i, no_of_worker_threads = atoi(argv[1]);

		pthread_t worker_threads[no_of_worker_threads];
		for (i = 0; i < no_of_worker_threads; i++)
		{
			pthread_create(&worker_threads[i], NULL, worker_thread, NULL);
		}
		client_search();
		for (i = 0; i < no_of_worker_threads; i++)
		{
			pthread_join(worker_threads[i], NULL);
		}
	}
	else
	{
		printf("Command Line Argument missing\n");
	}

	for (int i = 0; i < 100; i++)
	{
		pthread_mutex_destroy(&dic_lock[i]);
	}
	
	return 0;
}