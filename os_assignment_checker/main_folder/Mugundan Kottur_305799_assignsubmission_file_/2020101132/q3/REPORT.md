# Q3 - Multithreaded Server - Client Communication

## [Client](./client.c)

The file [client.c](./client.c) creates parses the input, waits for the seconds given in the input, and then creates threads for each of those commands.

- Parse the input and find the seconds to wait
- Sleep for the number of seconds mentioned above
- Create a socket and connect to the port
- Send the instruction given to the client to the server through the socket

## [Server](./server.c)

The file [server.c](./server.c) has n worker threads as specified during execution of the compiled file. The main thread will work with the sockets and connections and each instruction from a client to a queue whch the worker threads then work on.

- Create n worker threads that are then made to wait
- Have the main thread wait for connections from clients which are then added to a queue controlled with a lock
- A random worker thread is made to wake up and handle the client request after removing the request from the queue.
- The worker thread sends the response to the client which then prints the response

## How to Run

Run the [Makefile](./Makefile) to get the compiled files. Then execute `./server num` and `./client` in separate terminals where `num` is the number of worker threads are to be used in the server. Make sure to run `./server` with a command line argument of how many worker threads are required.