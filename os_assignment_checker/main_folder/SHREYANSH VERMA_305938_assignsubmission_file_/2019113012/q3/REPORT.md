## Files present:
 - client_sim.cpp : This file handles client section.
 - server_prog.cpp : This file handles server section.

## Instructions to run
 - Client : g++ -o client client_sim.cpp -lpthread
 - ./client
 - Server : g++ -o server server_prog.cpp -lpthread
 - ./server `<No of worker threads>`

## Input format
 - After starting the server wait for `Waiting for a new client to request for a connection` message to appear,initially server will wait till all the worker threads are initialized.
 - After starting the client give the input in the mentioned format.(Start giving input once the server has started else it would throw an error)

## Basic Overview(Logic of the code):
 # Client:
    * In the client part each of the client request acts as a thread,initially input is stored in a vector of   pair containing an integer representing sleep time and the input string.
    * Threads are initialized after taking the input.
    * In the thread function each thread captures a particular index and sends the given message to the server.
    * In this way multiple requests are being made to the server.
    * Corresponding result is then sent back to the client and a mutex lock is kept to print the result just to avoid clutter of the output if it gets preempted without linebreak.

 # Server:
    * Initially while worker threads are being initialized server is put into (5 seconds of sleep).
    * Initially after getting the worker threads the threads are initialized and they are put into wait on a semaphore
    * The semaphore is initially initialized to 0 so that initially all the threads goes on to wait to avoid the busy wait condition.
    * A queue is declared and it is used to store the file descriptors of the different connections between client and server.
    * As soon as a connection is made,the corresponding file descriptor is pushed into the queue (A mutex lock is present to make the queue thread safe) and the mutex is signalled.
    * Each thread function waits on the mutex in an infinite while loop,as soon as it is awakened by the signal it pops one of the file descriptors from the queue and extracts the given string out of it using the defined functions.
    * The string is parsed and it is checked for the relevant operations and corresponding error handling is also done.
    * Map is stored as an array of pair which includes the input string and a flag that determines whether key has already been inserted or not(1 if key is present 0 otherwise).
    * Corresponding operations are done on the map while keeping a mutex lock for each index to make sure it is thread safe.
    * After the performing the given operations a string is sent back to the client describing whether the operation is sucessful or not.
    * After a worker threads performs the task,it is put into a 2 sec sleep as defined in the pdf.


