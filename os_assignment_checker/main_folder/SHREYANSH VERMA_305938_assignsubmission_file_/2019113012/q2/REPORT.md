## Instructions to run:
    * gcc q2.c -lpthread
    * ./a.out

## Threads used
    * person_thr thread to simulate person.
    * goal_thr thread to simulate goal scored by the team.

## Working of simulation
  # Person thread:
   * Struct array for different person is created and the corresponding fields are stored.
   * After that person thread is created and person_sync function is called.
   * After thread enters this function it is  put into sleep specified in the input and a message that person i has reached the stadium is displayed.
   * This thread then waits on a timed semaphore for given number of seconds.
   * There are two infinite while loops present and also two time dependent semaphores present,a person thread first waits on `stud_wait` semaphore and after getting signalled it either exits the simulation if the given time passes or goes on to the next loop
   * Going to the next loop means being allocated a seat in any of the stands,so before going to the next loop the person thread displays that particular message and goes on to wait in the next semaphore `stand_wait` and waits for the given x seconds,while case it can be woken up from sleep when any team scores a goal,a counter is maintained to keep track of persons who are in stands as well as persons who are waiting at the ticket counter.Whenever any team scores a goal,all the waiting memebers of the `stand_wait` semaphores are woken up and then they are checked whether their opponent team has scored more goals than their limit and if it is true,they leave the simulation.
   * Also whenever a person leaves the stands before leaving they wake up all the waiting threads on the semaphore `stud_wait` and the corresponding threads in stud_wait check for the free slot in the stands and thereby select an empty stand according to their constraints.
   * In this way this process keeps on running till all the person have exited the simulation.

  # Goal Thread:
    * All the information regarding teams and probability to score a goal is stored in an struct array.
    * When a thread enters the corresponding thread function it is put into sleep as specified by the input given
    * After that based on the probability if it is greater than 0.5 then that particular teams scores a goal successfully otherwise they are unable to score a goal.
    * Whenever they score a goal all the person thread waiting on the stands are signalled and they are check against the required threshold for opponent team goals.
    * Mutex locks are used at appropriate places to protect the critical sections of the code.

  # Colors Used:
    * Red : It displays all the information related to a not getting seat/leaving for dinner/leaving after watching match.
    * Cyan : It is displayed whenever someone gets a seat in a particular zone.
    * Blue : It represents an event where a particular person reaches the stadium.
