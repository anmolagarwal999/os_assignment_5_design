#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int n_student,n_labs,n_courses,pref_filled = 0,total_stud,total_lab_ta=0;
int pref_filled_arr[] = {0,0,0,0};
int select_student = -1,revert=-1;
sem_t stud_sem[100],stud_after_tut[100];
pthread_mutex_t get_ta,select_student_course,student_pref[100],student_avail[100];
pthread_mutex_t decr = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t signal_stud = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutx_stud = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutx_has_filled = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutx_stud_incr = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutx_stud_decr = PTHREAD_MUTEX_INITIALIZER;

int minimum(int a ,int b)
{
    if(a<b)
    {
        return a;
    }
    return b;
}


typedef struct courses
{
    char course_name[100];
    int course_id;
    int course_itr;
    float interest;
    int mentor;
    int max_slot;
    int no_of_tas;
    int ta_lab_id[100];
    int is_available;
    int got_ta;
    int got_allocated;
    int seats_allocated;
    int end_course;
    int tut_taken;
    int course_complete;
    int signal_end;

}course;

typedef struct student
{
    int course_priority[3];
    int pref_idx;
    float calibre;
    int time_stamp;
    int got_course;
    int has_filled;
    int has_left;

}student;

typedef struct labs
{
    int lab_id;
    char lab_name[100];
    int ta_mentor_avail[100];
    int num_mentor;
    int ta_constraint;
    int ta_tut_no[100];
    int ta_has_completed[100];
    int avail_mentor;


}lab;

course course_arr[1000];
lab lab_arr[1000];
student student_arr[1000];

void *student_sync(void * arg) //Student threadf function
{
    long long int indx = (long long int)arg;
    sleep(student_arr[indx].time_stamp);
    // pthread_mutex_lock(&mutx_has_filled);
    printf(ANSI_COLOR_BLUE "Student %lld has filled in preferences for course registration \n"ANSI_COLOR_RESET,indx );
    pthread_mutex_lock(&student_avail[indx]);
    student_arr[indx].has_filled = 1;
    pthread_mutex_unlock(&student_avail[indx]);
    // pthread_mutex_unlock(&mutx_has_filled);
    while(1)
    {
        sem_wait(&stud_sem[student_arr[indx].course_priority[student_arr[indx].pref_idx]]);  //Wait on the first semaphore.
        pthread_mutex_lock(&student_avail[indx]);
        student_arr[indx].has_filled = 0;
        pthread_mutex_unlock(&student_avail[indx]);
        pthread_mutex_lock(&student_pref[student_arr[indx].course_priority[student_arr[indx].pref_idx]]);
        if(course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_complete==0)
        printf(ANSI_COLOR_BLUE"Student %lld has been allocated a seat for course %s\n"ANSI_COLOR_RESET,indx,course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_name);
        course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].seats_allocated--;
        pthread_mutex_unlock(&student_pref[student_arr[indx].course_priority[student_arr[indx].pref_idx]]);
        sem_wait(&stud_after_tut[student_arr[indx].course_priority[student_arr[indx].pref_idx]]); // Wait on the second semaphore.
        float value = student_arr[indx].calibre*course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].interest;
        pthread_mutex_lock(&student_pref[student_arr[indx].course_priority[student_arr[indx].pref_idx]]);
        course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].seats_allocated++;
        pthread_mutex_unlock(&student_pref[student_arr[indx].course_priority[student_arr[indx].pref_idx]]);
        if((value>0.5)&&(course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_complete==0)) //Decision whether to take a particular course or not.
        {
            printf(ANSI_COLOR_BLUE"Student %lld has selected the course %s permanently\n"ANSI_COLOR_RESET,indx,course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_name);
            pthread_mutex_lock(&decr);
            total_stud--;
            // printf("Total student = %d\n",total_stud);
            if(total_stud==0)   // Checking how many students have the preference filled.
            {
                printf("Simulation exited :)\n");
                exit(0);
            }
            pthread_mutex_unlock(&decr);
            student_arr[indx].has_left=1;
            return NULL;
        }
        else
        {
            if(course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_complete==0)
            printf(ANSI_COLOR_BLUE"Student %lld has withdrawn from course %s\n"ANSI_COLOR_RESET,indx,course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_name);
            student_arr[indx].pref_idx++;
            if(student_arr[indx].pref_idx==3)
            {
                printf(ANSI_COLOR_BLUE"Student %lld could not get any of his preferred course\n"ANSI_COLOR_RESET,indx);
                pthread_mutex_lock(&student_avail[indx]);
                student_arr[indx].has_left=1;   
                pthread_mutex_unlock(&student_avail[indx]);
                pthread_mutex_lock(&decr);
                total_stud--;
                if(total_stud==0)
                {
                    printf("Simulation exited :)\n");
                    exit(0);
                }
                pthread_mutex_unlock(&decr);
                return NULL;
            }
            //Preference change if withdrawn from the above course.
            printf(ANSI_COLOR_BLUE"Student %lld has changed preference from course %s (priority %d) to %s (priority %d) \n"ANSI_COLOR_RESET,indx,course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]-1].course_name,student_arr[indx].pref_idx-1,course_arr[student_arr[indx].course_priority[student_arr[indx].pref_idx]].course_name,student_arr[indx].pref_idx);
            pthread_mutex_lock(&student_avail[indx]);
            // printf("Here enter to change pref = id = %lld\n",indx);
            student_arr[indx].has_filled=1;
            pthread_mutex_unlock(&student_avail[indx]);
            
        }
    }
    // sleep(2);
    
}

void *course_sync(void * arg) // Course thread function
{
    long long int indx = (long long int)arg;
    int flag_check=0,ta_no,lab_no;
    int num_stud_in_course=0;
    sleep(2);
    while(!course_arr[indx].end_course)
    {
        flag_check = 0;
        num_stud_in_course=0;
        // printf("Enter check stud 6 and 7\n");
        // printf("%d %d\n\n",student_arr[6].has_filled,student_arr[7].has_filled);
        // printf("\n\n");
        // for(int i=0;i<n_student;i++)
        // {
            
        //     printf("Student info indx = %d curr_pref=%d\n",i,student_arr[i].has_filled);
            
        // }
        // printf("\n\n");
        for(int i=0;i<n_student;i++) //This loop checks how many students have this course as their current preference
        {
            pthread_mutex_lock(&student_avail[i]);
            if(student_arr[i].has_filled==0)
            {
                pthread_mutex_unlock(&student_avail[i]);
                continue;
            }
            if(student_arr[i].course_priority[student_arr[i].pref_idx]==course_arr[indx].course_id)
            {
                // printf("caught stud indx = %d\n",i);
                // student_arr[i].has_filled=0;
                num_stud_in_course++;
            }
            pthread_mutex_unlock(&student_avail[i]);

        }
        // printf("Check here! course indx  = %lld num of students = %d\n ",indx,num_stud_in_course);
        if(num_stud_in_course==0)
        {
            continue;
        }
        for(int i=0;i<n_labs;i++) //Checking whether a TA is available in any of the labs
        {
            if(course_arr[indx].got_ta)
            {
                break;
            }
            for(int j=0;j<lab_arr[i].num_mentor;j++)
            {
                if(course_arr[indx].got_ta)

                {
                    break;
                }
                pthread_mutex_lock(&get_ta);
                if(lab_arr[i].ta_mentor_avail[j]==1)
                {
                    // sleep(2);
                    lab_arr[i].ta_mentor_avail[j]=0;
                    lab_arr[i].ta_tut_no[j]++;
                    lab_no = i;
                    ta_no = j;
                    course_arr[indx].got_ta = 1;
                    printf(ANSI_COLOR_RED"TA %d from lab %s has been allocated to course %s for his %d TA ship\n"ANSI_COLOR_RESET,j,lab_arr[i].lab_name,course_arr[indx].course_name,lab_arr[i].ta_tut_no[j]);
                    course_arr[indx].seats_allocated = (rand()%(course_arr[indx].max_slot-1))+1;
                    printf(ANSI_COLOR_YELLOW"Course %s has been allocated %d seats\n"ANSI_COLOR_RESET,course_arr[indx].course_name,course_arr[indx].seats_allocated);
                }
                pthread_mutex_unlock(&get_ta);
                if(course_arr[indx].got_ta)
                {
                    break;
                }
            }
            if(course_arr[indx].got_ta)
            {
                break;
            }
        }
        if(course_arr[indx].got_ta) // If a lab has available TAs they are selected for conduction of tutorial
        {
            
            num_stud_in_course = minimum(course_arr[indx].seats_allocated,num_stud_in_course);
            int total_allocated_seats = course_arr[indx].seats_allocated;
            course_arr[indx].seats_allocated=num_stud_in_course;
            int to_restore;
            for(int i=0;i<num_stud_in_course;i++)
            {
                sem_post(&stud_sem[course_arr[indx].course_id]);
            }
            to_restore = course_arr[indx].seats_allocated;
            // printf("Reached before this course name is %s num of course = %d\n",course_arr[indx].course_name,num_stud_in_course);
            while(course_arr[indx].seats_allocated);
            printf(ANSI_COLOR_YELLOW"Tutorial has started for course %s has started with %d slots filled out of %d\n"ANSI_COLOR_RESET,course_arr[indx].course_name,num_stud_in_course,total_allocated_seats);
            sleep(10);
            printf(ANSI_COLOR_RED"TA %d from lab %s has completed the tutorial for course %s\n"ANSI_COLOR_RESET,ta_no,lab_arr[lab_no].lab_name,course_arr[indx].course_name);
            pthread_mutex_lock(&get_ta);
            if(lab_arr[lab_no].ta_tut_no[ta_no]==lab_arr[lab_no].ta_constraint)
            {
                lab_arr[lab_no].avail_mentor--;
                total_lab_ta--;
                if(lab_arr[lab_no].avail_mentor==0)
                {
                    printf(ANSI_COLOR_CYAN"Lab %s no longer has students available for TA ship\n"ANSI_COLOR_RESET,lab_arr[lab_no].lab_name);
                }         
            }
            else
            {
                lab_arr[lab_no].ta_mentor_avail[ta_no]=1;
            }   
            pthread_mutex_unlock(&get_ta);
            for(int i=0;i<num_stud_in_course;i++)
            {
                sem_post(&stud_after_tut[course_arr[indx].course_id]);
            }
            course_arr[indx].got_ta = 0;
            while(course_arr[indx].seats_allocated!=to_restore);
        }
        else
        {
            if(total_lab_ta==0) // If no tas are present the course is removed from the course offering.
            {
                if(course_arr[indx].signal_end==0)
                {
                    printf(ANSI_COLOR_YELLOW"Course %s does not have any TA mentors eligible and is removed from course offerings\n"ANSI_COLOR_RESET,course_arr[indx].course_name);
                    course_arr[indx].signal_end = 1;
                }
            }
            if((total_lab_ta==0)&&(num_stud_in_course!=0))
            {
                course_arr[indx].course_complete = 1;
                for(int i=0;i<num_stud_in_course;i++)
                {
                    sem_post(&stud_sem[course_arr[indx].course_id]);
                }
                course_arr[indx].seats_allocated=num_stud_in_course;
                while(course_arr[indx].seats_allocated);
                int to_restore = num_stud_in_course;
                for(int i=0;i<num_stud_in_course;i++)
                {
                    sem_post(&stud_after_tut[course_arr[indx].course_id]);
                }
                while(course_arr[indx].seats_allocated!=to_restore);
            }
        }
        
        
    }
}
int main()
{
    pthread_t student_thr[100],labs_thr[100],course_thr[100];
    pthread_mutex_init(&get_ta, NULL);
    pthread_mutex_init(&select_student_course, NULL);
    
    char course_name[100];
    scanf("%d %d %d",&n_student,&n_labs,&n_courses);
    total_stud = n_student;
    for(int i=0;i<n_courses;i++)
    {
        sem_init(&stud_sem[i], 0, 0);
        sem_init(&stud_after_tut[i], 0, 0);
        scanf("%s",course_arr[i].course_name);
        course_arr[i].is_available=1;
        course_arr[i].course_id=i;
        course_arr[i].got_allocated=0;
        course_arr[i].tut_taken=0;
        course_arr[i].course_itr = 0;
        scanf("%f %d %d",&course_arr[i].interest,&course_arr[i].max_slot,&course_arr[i].no_of_tas);
        for(int j=0;j<course_arr[i].no_of_tas;j++)
        {
            scanf("%d",&course_arr[i].ta_lab_id[j]);
        }
        course_arr[i].course_complete=0;
        course_arr[i].signal_end=0;
    }
    for(int i=0;i<n_student;i++)
    {
        pthread_mutex_init(&student_pref[i], NULL);
        pthread_mutex_init(&student_avail[i], NULL);
        scanf("%f",&student_arr[i].calibre);
        student_arr[i].got_course=0;
        for(int j=0;j<3;j++)
        {
            scanf("%d",&student_arr[i].course_priority[j]);
        }
        scanf("%d",&student_arr[i].time_stamp);
        student_arr[i].pref_idx = 0;
    }
    for(int i=0;i<n_labs;i++)
    {
        scanf("%s",lab_arr[i].lab_name);
        scanf("%d",&lab_arr[i].num_mentor);
        scanf("%d",&lab_arr[i].ta_constraint);
        for(int j=0;j<lab_arr[i].num_mentor;j++)
        {
            lab_arr[i].ta_mentor_avail[j]=1;
            lab_arr[i].ta_tut_no[j]=0;
            // lab_arr[i].ta_mentor_signal[j]=0;
        }
        lab_arr[i].avail_mentor = lab_arr[i].num_mentor;
        total_lab_ta+=lab_arr[i].avail_mentor;
    }
    // sleep(8);
    for(int i=0;i<n_student;i++)
    {
        pthread_create(&student_thr[i],NULL,student_sync,(void *)(long long int)i);  //Student thread initialization.
        // sleep(2);
    }
    // sleep(1);
    // sleep(8);
    for(int i=0;i<n_courses;i++)
    {
        pthread_create(&course_thr[i],NULL,course_sync,(void *)(long long int)i); //Course thread initialization
        // sleep(2);
    }
    for(int i=0;i<n_student;i++)
    {
        pthread_join(student_thr[i],NULL);   
    }
    for(int i=0;i<n_courses;i++)
    {
        pthread_join(course_thr[i],NULL);
    }
}