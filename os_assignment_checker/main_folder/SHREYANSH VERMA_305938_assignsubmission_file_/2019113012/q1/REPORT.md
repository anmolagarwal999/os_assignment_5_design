## Steps to run
 - gcc q1.c -lpthread
 - ./a.out

## Threads Used:
  * Thread entities:
   * Students thread
   * Course thread

## Colors Used:
   * Red for any info relating to TAs.
   * Blue for any info regarding students
   * Yellow for any info regarding Courses/Tutorials
   * Cyan for any info regarding labs.

## Basic Working (Logic of the code):
 - Structs representing respective entities (students,labs,courses) are created and corresponding struct arrays are created.
 - Two array of semaphores are initialized
 - Each semaphore represents a particular course and students that have that particular course wait on this semaphore.

 # Working of Student Thread:
    - In the student thread function a particular index first waits for the time mentioned in the input after waking up from the sleep it states that the particular student has filled the preference.
    - After that it goes into an infinite while loop and waits on the first semaphore.
    - There are two array of semaphore used for controlling student entity after filling the preference they wait on the first semaphore,they will wait here until the course for which they are waiting has been allocated the seats and then these students are signalled to leave(more of this will is explained in the course thread).
    - After a students has been signalled it states that it has been allocated a seat in that particular course and then it waits on the second semaphore (till the tutorial goes on).
    - After tutorial gets finished based on the probability if it is greater than 0.5 the student select that particular course and exits the simulation otherwise that particular students changes his/her preference and waits on the semaphore of the course in the next preference if their preference are remaining otherwise they exit the simulation.

# Working of Course thread:

 - Each course thread first checks how many students have that particular course as their current preference,if none of the students have this course as their current preference then the loop continues.
 - If a nonzero number of students have this course as their current preference then the course loops through the array of the labs and for each lab checks whether any TA mentor in the lab is available to take the tut.A global variable is also kept to check the total number of available TAs across all the labs,if it is 0 then the course thread ends and it gets removed from the course offering.
 - After a TA is selected (mutex locks are used at all the relevant places to keep critical sections safe) it allocates a random number of seats for that particular course and the corresponding event is printed(Relevant variables are kept in the struct of all the entities)
 - After seat allocation the course signals the mutex (min(No of seats allocated,no of student having this course as their preference)) times so that students waiting on the first semaphore gets cleared.
 - A sleep for 10 seconds is introuduced to simulate conduction of tutorial.
 - After the tutorial takes place students waiting on the second semaphore are signalled and the thread waits on an infinite while loop till all the student either takes this course or change their preference.
 - After this the TA is released and next iteration of the TA selection takes place for this course.

Overall this goes on till all the students are done with seat allocation and then simulation is exited.

