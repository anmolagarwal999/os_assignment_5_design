#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <errno.h>

// Colours
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


// Locks
pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  scan_sig = PTHREAD_COND_INITIALIZER;
pthread_mutex_t scan_lock = PTHREAD_MUTEX_INITIALIZER;
int done = 0;

// Semaphores
sem_t imaginary_lock;
sem_t *lab_locks;

// struct definitions
struct course {
    char *name;                     // unique course name

    sem_t wait_for_seat;            // semaphore for seat allocation
    int ppl_there;                  // Var to check for availability
    pthread_mutex_t ppl_lock;       
    pthread_cond_t  ppl_sig;

    // Details regarding course
    float interest;
    int max_slots;
    bool removed;

    // Lab Details
    int num_labs;
    int *labs;                      

    // Tut locks and threads
    sem_t wait_end_tute;
    pthread_t course_thr_id;

    pthread_cond_t attend;
    pthread_mutex_t attend_lock;


    int num_stud;
};

typedef struct course course;


enum stud_state { not_registered, waiting, attending, finished };   // Gives state of student
struct student {

    int id; 
    int time;                   // Time post filling preference
    int pref[3];                // Preference order and info
    enum stud_state state;  
    float calibre;              // Variable to store Calibre
    pthread_t stud_thread_id;

};
typedef struct student stud;

struct lab {
    char *name;
    int num_tas;
    int max_no_times;
    
    int *num_taed;              // Count for Tuts conducted by TA's
    bool *occupied;             // State of TA: occupied or not occupied
};
typedef struct lab lab;

lab *all_labs;                 // Pointer to array of all labs
course *all_courses;           // Pointer to array of all courses

void destroy()
{
    // destroy semaphore/mutex after all the threads finished working
    pthread_mutex_destroy(&print_lock);
    sem_destroy(&imaginary_lock);
}

void *start_c(void *args){
    
    pthread_mutex_lock(&scan_lock);
    while( done == 0 ){
        pthread_cond_wait(&(scan_sig), &scan_lock);
    }
    pthread_mutex_unlock(&scan_lock);

    course *c = (course *)args;

    while(1){
        
        pthread_mutex_lock(&(c->ppl_lock));                     // Initially, no student is waiting for the course
        while(c->ppl_there == 0)
            pthread_cond_wait(&(c->ppl_sig), &(c->ppl_lock));
        pthread_mutex_unlock(&(c->ppl_lock));

        // Information about Lab's and Ta's
        int status = 0;
        int cur_ta_id = -1;
        int cur_lab_id = -1;
        int num_taed = -1;
        char *cur_lab_name = NULL;

        // This loop begins to allocate TA's
        for( int i = 0; i < c->num_labs; i++){
            int lab_id = c->labs[i];
            if( lab_id == -1 ){
                continue;
            }

            // Few variables to store intermediate data
            cur_lab_id = lab_id;
            sem_wait( &lab_locks[lab_id] );
            int _num_ta = all_labs[lab_id].num_tas;
            int lab_stat = 0;                           
            cur_lab_name = all_labs[lab_id].name;
            
            for( int j = 0; j < _num_ta; j++){   
                int _lab_num_taed, _lab_max_ta;
                _lab_num_taed = all_labs[lab_id].num_taed[j];
                _lab_max_ta = all_labs[lab_id].max_no_times;

                if( _lab_num_taed < _lab_max_ta ){
                    if( all_labs[lab_id].occupied[j] == false ){
                        all_labs[lab_id].num_taed[j] = all_labs[lab_id].num_taed[j] + 1;
                        
                        cur_ta_id = j;
                        all_labs[lab_id].occupied[j] = true;

                        num_taed = all_labs[lab_id].num_taed[j];

                        status = 1;
                        break;
                    }

                    else{
                        lab_stat = 1;
                    }
                }
            }

            sem_post(&lab_locks[lab_id]);

            if( status == 1 ){
                break;
            }
            else if( lab_stat == 0){
                pthread_mutex_lock(&print_lock);
                printf( ANSI_COLOR_GREEN "Lab %s no longer has students available for TA ship" ANSI_COLOR_RESET "\n", cur_lab_name);
                pthread_mutex_unlock(&print_lock);

                c->labs[i] = -1;
            }
        }

        // This block is when course is withdrawn
        if( status == 0 ){

            int tmp = c->ppl_there;

            pthread_mutex_lock(&(c->ppl_lock));
            c->ppl_there = 0;
            c->removed = true;
            pthread_mutex_unlock(&(c->ppl_lock));

            for( int i = 0; i < tmp; i++){
                sem_post(&(c->wait_for_seat));
            }

            pthread_mutex_lock(&print_lock);
            char *name = c -> name; 
            printf(ANSI_COLOR_BLUE "Course %s doesn’t have any TA’s eligible and is removed from course offerings" ANSI_COLOR_RESET "\n", name);
            pthread_mutex_unlock(&print_lock);

            return NULL;
        }

        // TA can be assigned
        else{   

            char suffix[5];
            switch(num_taed){
                case 1:
                    strcpy(suffix, "st");
                    break;
                case 2:
                    strcpy(suffix, "nd");
                    break;
                case 3:
                    strcpy(suffix, "rd");
                    break;
                default:
                    strcpy(suffix, "th");
                    break;

            }   
            
            // Printing Event
            pthread_mutex_lock(&print_lock);
            char *lab_name = cur_lab_name;
            char *name_ = c->name;
            printf(ANSI_COLOR_GREEN "TA %d from lab %s has been allocated to course %s for %d%s TA ship" ANSI_COLOR_RESET "\n", cur_ta_id, lab_name, name_, num_taed, suffix);
            pthread_mutex_unlock(&print_lock);

            int cur_seats = (rand() % c->max_slots) + 1;
            int num_alloc = cur_seats;

            // Printing Event
            pthread_mutex_lock(&print_lock);
            char *name = cur_lab_name;
            int seats = cur_seats;
            printf(ANSI_COLOR_BLUE "Course %s has been allocated %d seats" ANSI_COLOR_RESET "\n", name, seats);
            pthread_mutex_unlock(&print_lock);

            // Allocating seats to students
            pthread_mutex_lock(&(c->ppl_lock));
            if( c->ppl_there < cur_seats ){
                num_alloc = c->ppl_there;
            }

            pthread_mutex_unlock(&(c->ppl_lock));

            for(int i = 0; i < num_alloc; i++){
                sem_post( &c->wait_for_seat );             // wake up student threads waiting for seats
            }

            // Printing Event
            pthread_mutex_lock(&print_lock);
            name = c->name;
            int filled = num_alloc;
            seats = cur_seats;
            printf(ANSI_COLOR_BLUE "Tutorial has started for Course %s with %d seats filled out of %d" ANSI_COLOR_RESET "\n", name, filled, seats);
            pthread_mutex_unlock(&print_lock);

            // Waiting for 1sec
            sleep(1);

            // Ending the Tut
            pthread_mutex_lock(&(c->attend_lock));
            int cur_num_students = c->num_stud;
            while( num_alloc > cur_num_students ){
                pthread_cond_wait( &(c->attend), &(c->attend_lock) );
            }
            pthread_mutex_unlock(&(c->attend_lock));

            for( int i = 0; i < num_alloc; i = i + 1){
                sem_post( &c->wait_end_tute );
            }

            // Freeing the TA's
            sem_wait( &lab_locks[cur_lab_id] );
            all_labs[cur_lab_id].occupied[cur_ta_id] = false;
            sem_post( &lab_locks[cur_lab_id] );

            // Printing Event
            pthread_mutex_lock(&print_lock);
            char *l_name = cur_lab_name;
            name = c->name;
            printf(ANSI_COLOR_BLUE "TA %d from lab %s has completed the tutorial and left the course %s" ANSI_COLOR_RESET "\n", cur_ta_id, l_name, name);
            pthread_mutex_unlock(&print_lock);
        }
    } 
}

// Function to generate Random Number
int get_rand_num(){
    return rand() % 1001;
}

void *start_stud(void *args)
{
    stud *s = (stud *)args;
    struct timespec ts;
    
    // Student filling Preference after 't' secs
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += s->time;

    int id = s->id;
    if( s->state == not_registered ){
        sem_timedwait(&imaginary_lock, &ts);

        // Printing Event
        pthread_mutex_lock(&print_lock);
        printf(ANSI_COLOR_RED "Student %d has filled in preferences for course registration" ANSI_COLOR_RESET "\n", id);
        pthread_mutex_unlock(&print_lock);
       
    }

    int pref_ind;
    pref_ind = 0;

    s->state = waiting;

    while( 3 > pref_ind ){

        // Updating number of students in all_courses
        pthread_mutex_lock(&(all_courses[s->pref[pref_ind]].ppl_lock));
        all_courses[s->pref[pref_ind]].ppl_there = all_courses[s->pref[pref_ind]].ppl_there + 1;
        pthread_cond_signal(&(all_courses[s->pref[pref_ind]].ppl_sig));
        pthread_mutex_unlock(&(all_courses[s->pref[pref_ind]].ppl_lock));

        bool course_pref_state = all_courses[s->pref[pref_ind]].removed;
        if( course_pref_state == true ){
            goto removed;
        }
         
        if( sem_wait(&(all_courses[s->pref[pref_ind]].wait_for_seat)) == -1){
            perror("sem wait error");
        }

        course_pref_state = all_courses[s->pref[pref_ind]].removed;
        if( course_pref_state == true ){
            goto removed;
        }

        // Printing Event
        pthread_mutex_lock(&print_lock);
        char *name = all_courses[s->pref[pref_ind]].name;
        printf(ANSI_COLOR_RED "Student %d has been allocated a seat in course %s" ANSI_COLOR_RESET "\n", id, name);
        pthread_mutex_unlock(&print_lock);

        s->state = attending;

        // Updating number of students
        pthread_mutex_lock( &(all_courses[s->pref[pref_ind]].attend_lock) );
        all_courses[s->pref[pref_ind]].num_stud = all_courses[s->pref[pref_ind]].num_stud + 1;
        pthread_cond_signal( &(all_courses[s->pref[pref_ind]].attend) );
        pthread_mutex_unlock( &(all_courses[s->pref[pref_ind]].attend_lock) );

        if( sem_wait(&(all_courses[s->pref[pref_ind]].wait_end_tute)) == -1){
            perror("sem wait error");
        }

        int rand_num;
        rand_num = get_rand_num();

        int course_poss = (all_courses[s->pref[pref_ind]].interest)*(s->calibre)*1000;
        if( course_poss > rand_num  ){
            pthread_mutex_lock(&print_lock);

            // Printing Event
            char *name = all_courses[s->pref[pref_ind]].name;
            printf(ANSI_COLOR_RED "Student %d has withdrawn from course %s" ANSI_COLOR_RESET "\n", id, name);
            pthread_mutex_unlock(&print_lock);

        // if course withdrawn or student withdraws from course    
        removed:
            pref_ind = pref_ind + 1;
            
            switch (pref_ind){
                case 3:

                    // Printing Event
                    pthread_mutex_lock(&print_lock);
                    printf(ANSI_COLOR_RED "Student %d could not get any of his preferred courses" ANSI_COLOR_RESET "\n", id);
                    pthread_mutex_unlock(&print_lock);
                    return NULL;
                    break;
                
                default:
                    s->state = waiting;

                    // Printing Event
                    pthread_mutex_lock(&print_lock);
                    char *pref1 = all_courses[ s->pref[pref_ind-1] ].name;
                    char *pref2 = all_courses[ s->pref[pref_ind] ].name;
                    printf(ANSI_COLOR_RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", id, pref1, pref_ind, pref2, pref_ind + 1);
                    pthread_mutex_unlock(&print_lock);
                    break;
            }
        }
        else{
            // Printing Event
            pthread_mutex_lock(&print_lock);
            char *name = all_courses[ s->pref[pref_ind] ].name;
            printf(ANSI_COLOR_RED "Student %d has selected course %s permanently" ANSI_COLOR_RESET "\n", id, name);
            pthread_mutex_unlock(&print_lock);

            s->state = finished;
            return NULL;
        }
    }

    // If No course could be found
    pthread_mutex_lock(&print_lock);
    printf(ANSI_COLOR_RED "Student %d could not get any of his preferred courses" ANSI_COLOR_RESET "\n", id);
    pthread_mutex_unlock(&print_lock);
    return NULL;
}

// Getting Input 
void get_input(int *stud, int *lab, int *course){
    scanf("%d %d %d", stud, lab, course);
}

// Inputting the Course Details
void get_course_input(int num_courses, int num_labs){

    for( int i = 0; i < num_courses; i++)
    {   

        char course_name[101];
        int max_slots, total_labs;
        float interest;

        scanf("%s %f %d %d", course_name, &interest, &max_slots, &total_labs);

        all_courses[i].name = (char *)malloc(sizeof(char *));
        strcpy(all_courses[i].name, course_name);
        all_courses[i].interest = interest;
        all_courses[i].max_slots = max_slots;
        all_courses[i].num_labs = total_labs;

        all_courses[i].labs = (int *)malloc( num_labs*sizeof(int) );

        for( int j = 0; j < total_labs; j++)
        {   
            int count_labs;
            scanf("%d", &count_labs);
            all_courses[i].labs[j] = count_labs;
        }

        // Initialising all the locks and semaphores for courses
        pthread_cond_init( &(all_courses[i].attend), NULL);

        pthread_mutex_init( &(all_courses[i].attend_lock), NULL);
        pthread_mutex_init( &(all_courses[i].ppl_lock), NULL);

        int default_val = 0;
        all_courses[i].num_stud = default_val;
        
        pthread_mutex_lock( &(all_courses[i].ppl_lock) );
        all_courses[i].ppl_there = default_val;
        pthread_mutex_unlock( &(all_courses[i].ppl_lock) );

        pthread_cond_init( &(all_courses[i].ppl_sig), NULL);

        sem_init(&all_courses[i].wait_for_seat, default_val, default_val);
        sem_init(&all_courses[i].wait_end_tute, default_val, default_val);

        all_courses[i].removed = false;
    }
}

// Allocating space for all_lab array
void init_lab_stuff(int num_tas, int num_labs, int idx){
    
    all_labs[idx].occupied = (bool *)malloc(num_labs*sizeof(bool));
    
    all_labs[idx].num_taed = (int *)malloc(num_tas*sizeof(int));

}

// Inputting Lab info
void get_lab_input(int num_labs){
    
    for( int i = 0; i < num_labs; i++){

        char name[101];
        int num_tas, max_times;

        scanf("%s %d %d", name, &num_tas, &max_times);

        all_labs[i].name = (char *)malloc(sizeof(char *));
        strcpy(all_labs[i].name, name);
        all_labs[i].num_tas = num_tas;
        all_labs[i].max_no_times = max_times;

        init_lab_stuff(num_tas, num_labs, i);
        
        int defualt_val = 0;
        for( int j = 0; j < all_labs[i].num_tas; j++)
        {
            all_labs[i].num_taed[j] = defualt_val;
            all_labs[i].occupied[j] = defualt_val;
        }

        sem_init(&(lab_locks[i]), 0, 1);
    }
}

// Creates Student Thread
void create_stud_thread(stud st[], int num){

    for( int i = 0; i < num; i++ )
    {
        pthread_create(&(st[i].stud_thread_id), NULL, start_stud, &(st[i]) );
    }

}

// Creates Course Thread
void create_course_thread(int num){

    for( int i = 0; i < num; i++)
    {
        pthread_create(&(all_courses[i].course_thr_id), NULL, start_c, &(all_courses[i]) );
    }

}

// Joins Student Thread
void join_stud_thread(stud st[], int num){

    for( int i = 0; i < num; i++ )
    {
        pthread_join(st[i].stud_thread_id, NULL);
    }
}

// Cancels Course Thread
void cancel_course_thread(int num){

    for( int i = 0; i < num; i++ )
    {
        pthread_cancel(all_courses[i].course_thr_id);
    }
}

int main()
{
    srand(time(0));

    // Initializing Semaphores to zero
    sem_init(&imaginary_lock, 0, 0);

    // Getting Portal Info
    int num_students, num_labs, num_courses;
    get_input(&num_students, &num_labs, &num_courses);

    // Getting Course Info
    all_courses = (course *)malloc(num_courses * sizeof(course));
    get_course_input(num_courses, num_labs);
    
    // Getting Student Info
    stud st[num_students];
    int st_id;
    st_id = 0;
    for( int i = 0; i < num_students; i++, st_id++ ){
        st[i].id = st_id;
        st[i].state = 0;

        float calibre;
        int pref[3], time;

        scanf("%f %d %d %d %d", &calibre, &pref[0], &pref[1], &pref[2], &time);

        st[i].calibre = calibre;
        st[i].pref[0] = pref[0];
        st[i].pref[1] = pref[1];
        st[i].pref[2] = pref[2];
        st[i].time = time;
    }

    // Getting Lab Info
    all_labs = (lab *)malloc(num_labs*sizeof(lab));
    lab_locks = (sem_t *)malloc( num_labs*sizeof(sem_t) );
    get_lab_input(num_labs);
    
    // Simulating the Portal
    pthread_mutex_lock(&scan_lock);
    
    done = 1;
    pthread_cond_signal(&scan_sig);

    pthread_mutex_unlock(&scan_lock);

    create_stud_thread(st, num_students);
    create_course_thread(num_courses);
    
    join_stud_thread(st, num_students);
    cancel_course_thread(num_courses);

    destroy();

    return 0;
}
