#include <cstdio>
#include <iostream>
#include <sstream>
#include <netdb.h>
#include <vector>
#include <string>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>


// Initialising all the global variables and Locks, Semaphores
typedef sockaddr SockAddress;

std::map<int, std::string> dictionary;

std::queue<int> Socket;
std::vector<pthread_mutex_t> mutexLock(100, PTHREAD_MUTEX_INITIALIZER);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;


// Inserting into Dictionary
void insert(std::string value, int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
		dictionary[key] = value;
        str = str + "Insertion Successful";
	}
    else{
        str = str + "Key already exists";
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Deleting From Dictionary
void Delete (int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
        str = str + "No such key exists";
    }
    else{
        dictionary.erase(key);
        str = str + "Deletion Successful";
	}

    pthread_mutex_unlock(&mutexLock[key]);
}

// Updating The Dictionary
void update(std::string value, int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
	if (dict_cout){
		dictionary[key] = value;
        str = str + value;
	}
	else{
        str = str + "No such key exists";
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Concatenating values in dictionary
void concat(int key1, int key2, std::string& str){
    pthread_mutex_lock(&mutexLock[key1]);
    pthread_mutex_lock(&mutexLock[key2]);

    int dict_cout1 = dictionary.count(key1);
    int dict_cout2 = dictionary.count(key2);
    if (!dict_cout1 || !dict_cout2){
        str = str + "Concat failed as a least one of the keys does not exist";
    }
    else{
        std::string value1 = dictionary[key1];
        std::string value2 = dictionary[key2];
        dictionary[key1] = dictionary[key1] + value2;
        dictionary[key2] = dictionary[key2] + value1;
        str = str + value2 + value1;
    }

    pthread_mutex_unlock(&mutexLock[key1]);
    pthread_mutex_unlock(&mutexLock[key2]);
}

// Fetches valus of a given Key, and appends it to str
void fetch(int key, std::string& str){
    pthread_mutex_lock(&mutexLock[key]);

    int dict_cout = dictionary.count(key);
    if (!dict_cout){
        str = str + "Key does not exist";
    }
    else{
        str = str + dictionary[key];
    }

    pthread_mutex_unlock(&mutexLock[key]);
}

// Handling clients using multiple threads
void server(int sockfd){
	// Reading command 
    char buffer[1024];
    for(int i=0; i<1024; i++){
        buffer[i] = 0;
    }
    
	read(sockfd, buffer, 1024);

    std::string str;
    str = "";
    std::vector<std::string> tokens;

    // Converting input to string
    std::stringstream temp(buffer);
    std::string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' '))
        tokens.push_back(intermediate);

    str = str + std::to_string(pthread_self());
    str = str + ":";
    int nParts = tokens.size();

    // Driver Function
    if (tokens.size() > 1){
        if (tokens[1] == "insert"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                insert(tokens[3], token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "delete"){
            if (nParts == 3){
                int token_int = stoi(tokens[2]);
                Delete(token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "concat"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                int token_int_1 = stoi(tokens[3]);
                concat(token_int, token_int_1, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "update"){
            if (nParts == 4){
                int token_int = stoi(tokens[2]);
                update(tokens[3], token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else if (tokens[1] == "fetch"){
            if (nParts == 3){
                int token_int = stoi(tokens[2]);
                fetch(token_int, str);
            }
            else{
                str = str + "Incorrect number of arguments";
            }
        }
        else{
            str = str + "Incorrect command";
        }
    }
    else{
        str = str + "Incorrect number of arguments";
    }

    // Writing
    int str_len = strlen(str.c_str());
    write(sockfd, str.c_str(), str_len);
}

// Handles Worker Threads
void *worker_thread(void *arg){

	while (1){
		pthread_mutex_lock(&lock);

		while (!Socket.size())
			pthread_cond_wait(&cond, &lock);

        int sockfd = Socket.front();
		Socket.pop();

		pthread_mutex_unlock(&lock);
		server(sockfd);
		close(sockfd);
	}
	return NULL;
}

// Looks for Clients
void client_search()
{
	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1){
		exit(0);
    }

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(8080);

    int bind_check = bind(sockfd, (SockAddress *)&servaddr, sizeof(servaddr));
	if (bind_check){
		exit(0);
    }

    int listen_check = listen(sockfd, 5);
	if (listen_check){
		exit(0);
    }

	socklen_t len = sizeof(cli);

	while (1){
		connfd = accept(sockfd, (SockAddress *)&cli, &len);
		if (connfd < 0)
			exit(0);

		pthread_mutex_lock(&lock);
        Socket.push(connfd);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
	}
}

// Creates Worker Threads
void create_thread(int num_worker_threads, std::vector<pthread_t>& worker_threads){

    for(int i = 0; i < num_worker_threads; i++){
        pthread_create(&worker_threads[i], NULL, worker_thread, NULL);
    }

}

// Joins Worker Threads
void join_thread(int num_worker_threads, std::vector<pthread_t>& worker_threads){

    for(int i = 0; i < num_worker_threads; i++){
        pthread_join(worker_threads[i], NULL);
    }

}

int main(int argc, char *argv[]){
	
    if (argc > 1){
		int i, num_worker_threads = atoi(argv[1]);
        std::vector<pthread_t> worker_threads(num_worker_threads);

        create_thread(num_worker_threads, worker_threads);

		client_search();

        join_thread(num_worker_threads, worker_threads);
	}
	else{
        std::cout << "Missing number of threads as argument";
    }
	return 0;
}