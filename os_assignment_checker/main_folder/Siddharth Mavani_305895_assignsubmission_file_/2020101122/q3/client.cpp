#include <cstdio>
#include <netdb.h>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

// Creating Class that stores all the data
class Data{
public:
    Data(int client_id, std::string command)
        : client_id(client_id), command(command) {}

    Data() = default;

	int client_id;
	std::string command;
};

void func(Data& data, int sockfd){
    const char* ptr_to_command = data.command.c_str();
    int command_len = strlen(data.command.c_str());

	write(sockfd, ptr_to_command, command_len);
	char buff[1024] = {0};
	read(sockfd, buff, 1024);

    std::cout << data.client_id << ": " << buff << std::endl;
}

void* client(void* args)
{
    // Tokenizing the given string
    std::vector<std::string> tokens;

    Data data = *(Data*)args;

    std::stringstream temp(data.command);
    std::string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' ')){
        tokens.push_back(intermediate);
    }

    // Printing the token vector
	if(tokens.size() < 2){
        std::cout << "Command doesn't exist\n";
		return NULL;
	}

    // Convert to integer
    int sleep_time = stoi(tokens[0]);
    sleep(sleep_time);

	int sockfd, connfd, check;
	sockaddr_in servaddr, cli;

	// Creating and verfying the socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1){
		exit(0);
    }

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(8080);

	// Connecting the client socket to server socket
    check = connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr));
	if (check != 0){
		exit(0);
    }

	func(data, sockfd);
	close(sockfd);

    return NULL;
}

// Joining the Client Threads
void join_thread(int num_clients, std::vector<pthread_t>& client_threads){

    for(int i = 0; i < num_clients; i++){
        pthread_join(client_threads[i], NULL);
    }

}

int main()
{   
    // Inputting number of clients
	int num_clients;
    std::cin >> num_clients;
    getchar();

    // Create a thread for each client
    std::vector<pthread_t> client_threads(num_clients);
    std::vector<Data> datas(num_clients);

    // Reading input and creating threads
	for(int i = 0, j = 0; i < num_clients; i++, j = 0)
    {
        getline(std::cin, datas[i].command);
        datas[i].client_id = i;
        pthread_create(&client_threads[i], NULL, client, &datas[i]);
    }

    join_thread(num_clients, client_threads);

	return 0;
}
