///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

#include <signal.h>
#include <stdarg.h>
#include <errno.h>
///////////////////////////////////////////////////

#include <sys/time.h>
#include <sys/ioctl.h>
///////////////////////////////////////////////////

#include <netdb.h>
#include <stdio.h>
#include <iostream>
///////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
///////////////////////////////////////////////////

#include <tuple>
#include <map>
#include <iterator>
#include <string>
#include <vector>
#include <queue>  
 ///////////////////////////////////////////////////


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
///////////////////////////////////////////////////

#include <netinet/in.h>
#include <arpa/inet.h>
///////////////////////////////////////////////////

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
///////////////////////////////////////////////////



#include <assert.h>
#include <fcntl.h>

using namespace std;
///////////////////////////////////////////////////



// Primary Colours
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define BLU "\e[0;34m"

  // Ternary colours
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
  // Secondary Colours
#define BLK "\e[0;30m"
#define YEL "\e[0;33m"

// Regular color text

#define ANSI_RESET "\x1b[0m"

///////////////////////////////////////////////////

typedef struct data {
    bool busy;
    string value;
} data;
 ///////////////////////////////////////////////////

typedef struct th_args {
    
} th_args;
///////////////////////////////////////////////////

typedef long long LL;

///////////////////////////////////////////////////
#define MAX_CLIENTS 77
#define PORT_ARG 8001
///////////////////////////////////////////////////
#define part cout << "-----------------------------------" << endl;
#define debug(x) cout << #x << " : " << x << endl

#define pb push_back
///////////////////////////////////////////////////


const LL B_S = 1048576;// 1 MB = 1048576 bytes( This number symbolizes the "Battle of Life")                             // for receiving the string from client
const int initial_message_length = 256;

pthread_mutex_t socketlock = PTHREAD_MUTEX_INITIALIZER; // lock while sending string to socket 
pthread_mutex_t lock[101];                              // lock for  dictionary's key
pthread_mutex_t quelock = PTHREAD_MUTEX_INITIALIZER;    // queue lock
///////////////////////////////////////////////////

pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;      // conditional variable for multiple threads
///////////////////////////////////////////////////
queue<int *> q;                                         //  client fds' addresses queue
map<int, data> mydict;                                  //  common dictionary of server

///////////////////////////////////////////////////


pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } 
    else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    }
            else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    }  
    else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&conditionalvariable, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}

///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  */

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a NEW CLIENT's request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New CLIENT connected to port %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd; //pcli = pclient

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&conditionalvariable);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        pthread_mutex_destroy(&lock[i]);
    }

    return 0;
}







/*
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

///////////////////////////////////////////////////

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////////////////////////

void access_dict(string cmd, int client_socket_fd)
{
    // Tokenize the input command 
    int i = 0, j = 0, l = cmd.length();
    vector<string> tokens;
    tokens.push_back("");

    for (i = 0; i < l; i++) {
        if (cmd[i] == ' ') {
            tokens.push_back("");
            j++;
        } else {
            tokens[j] += cmd[i];
        }
    }

    int key = stoi(tokens[1]);

    // Use map functions to perform required operation
    if (tokens[0] == "insert") {
        if (mydict.find(key) == mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Insertion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "delete") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict.erase(key);
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Deletion successful");
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "No such key exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "update") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&lock[key]);
                mydict[key].value = tokens[2];
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key already exists");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "concat") {
        int key2 = stoi(tokens[2]);
        if (mydict.find(key) != mydict.end() && mydict.find(key2) != mydict.end()) {
            string val1 = mydict[key].value;
            string val2 = mydict[key2].value;
            pthread_mutex_lock(&lock[key]);
            pthread_mutex_lock(&lock[key2]);
                mydict[key].value = val1+val2;
                mydict[key2].value = val2+val1;
            pthread_mutex_unlock(&lock[key]);
            pthread_mutex_unlock(&lock[key2]);
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key2].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Concat failed as at least one of the keys does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    } else if (tokens[0] == "fetch") {
        if (mydict.find(key) != mydict.end()) {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, mydict[key].value);
            pthread_mutex_unlock(&socketlock);
        } else {
            pthread_mutex_lock(&socketlock);
                send_string_on_socket(client_socket_fd, "Key does not exist");
            pthread_mutex_unlock(&socketlock);
        }
    }
}

///////////////////////////////////////////////////

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    int ret_val = 1;

    while (true) 
    {
        // Get command from client
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, B_S);
        ret_val = received_num;
    
        if (ret_val <= 0) {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        cout << "Client sent : " << cmd << endl;
        access_dict(cmd, client_socket_fd);
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

///////////////////////////////////////////////////

void *begin_process(void *inp) {
    while (1) 
    {
        // Wait for queue to have at least 1 client file descriptor address
        pthread_mutex_lock(&quelock);
        while (q.empty()) {
            pthread_cond_wait(&_, &quelock);
        }
        pthread_mutex_unlock(&quelock);

        // Get the first client fd from the queue and process request
        if (!q.empty()) {
            pthread_mutex_lock(&quelock);
            int *client_fd_addr = q.front();
            q.pop();
            pthread_mutex_unlock(&quelock);
            handle_connection(*client_fd_addr);
        }
    }
}
    //////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cout << "Invalid syntax. Usage: " << argv[0] << " <number of worker threads>\n";
        exit(-1);
    }

    int i, j, k, t, n;
    n = atoi(argv[1]);          // Number of clients/threads

    // Initialize locks of max 100 keys in the dictionary
    for(i = 0; i < 101; i++){
        pthread_mutex_init(&lock[i], NULL);
    }

    // Multithreading
    pthread_t connections[n];
    for (i = 0; i < n; i++) {
        pthread_create(&connections[i], NULL, begin_process, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, ess;
  ect  /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    // IP address can be anything (INADDR_ANY) 
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
    (address family) but has no address assigned to it.  bind() assigns
    the address specified by addr to the socket referred to by the file
    descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
    address structure pointed to by addr.  

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests 
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_address_object);
    
    while (1)
    {
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&ess, &cectlilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(ess.sinect_port), inet_ntoa(ess.sinect_addr));
        
        int* pcli = (int*) malloc(sizeof(int)); 
        *pcli = client_socket_fd;

        pthread_mutex_lock(&quelock);
            q.push(pcli);
        pthread_mutex_unlock(&quelock);

        pthread_cond_signal(&_);
    }
    
    close(wel_socket_fd);          

    for(int i = 0; i<101; i++) {
        destroy_pthread_mutex(&lock[i]);
    }

    return 0;
}
*/
