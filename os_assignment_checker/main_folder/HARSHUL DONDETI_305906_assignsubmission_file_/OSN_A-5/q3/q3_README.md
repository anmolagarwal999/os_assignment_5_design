# Server Simulation

   ## Server_Compilation
g++ server.cpp -o server -lpthread

   ## Server_Run
./server num_clients

   ## Server_Logic
It maintains a Global Dictionary implemented as a map of int keys and **struct data** values. <br>
**struct data** stores bool busy(to check if another thread's request is being executed) and string value, which is the string to be stored. <br>
Lock must be held when modifying, deleting or updating dictionary. <br>
Server creates 1 thread to handle each new client, and main function sets up the sockets and listens for the client request. <br>
When a client request is received, the connection is established by the accept() function and the client fd is obtained. The client fd's address is pushed into the global queue, so that the thread can progress in the begin_process() function. <br>
Multiple threads can call handle_connection() or access_dict() without requiring locks, so that simulataneous update is possible. <br>
The dictionary keys, socket and the queue are protected by locks. <br>

*************************************************
--------------------------------------------------

# Client Simulation

   ## Client Compilation
g++ client.cpp -o client -lpthread

   ## Client Run (if input.txt contains the input for client program)
./client < input.txt 

   ## Client Logic
**socket port** is obtained and multiple threads are created. <br>
**begin_process** function handles each threads with main program waiting for the threads to finish. <br>

Input data is stored in the form of time and command for each request line (input by the user). <br>
 The data is stored as a **struct pack** for each thread/client. There is a global vector of packets called requests to handle requests made by multiple clients. <br>
  When sending something to the socket and reading from the socket, lock needs to be acquired, so that only one thread can access the socket at a time. This functionality is implemented through a global mutex lock. <br>
   tie() function is used to receive data from the server as an output message. Lock is acquired when printing the output to the screen. <br>
                   -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+