#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

////////////////////////////////////

string values[1000];
int key[1000];
queue<int> Que;
pthread_cond_t con_var;
pthread_mutex_t push_lock;
pthread_mutex_t pop_lock;
pthread_mutex_t one_lock;

///////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;

        int length = cmd.size();
        char str[1000]={'\0'};
        for (int i = 0; i < length; i++)
        {
            str[i] = cmd[i];
        }

        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back;

        char *harsha[100];
        char *token1 = strtok(str, " \t\n");

        int z = 0;
        while (token1 != NULL)
        {
            harsha[z] = token1;
            token1 = strtok(NULL, " \t");
            z++;
            //printf("Command: %s\n", token1);
        }
        harsha[z] = token1;
        // for (int i = 0; i < z; i++)
        // {
        //     cout << harsha[i] << endl;
        // }
        // // msg_to_send_back = "out of loop";
        // cout << harsha[1] << endl;

        if (strcmp(harsha[1], "insert") == 0)
        {
            // cout << "coming" << endl;

            if (z == 4)
            {
                // cout << "coming" << endl;
                int index = atoi(harsha[2]);
                if (key[index] == -1)
                {
                    // cout << "coming" << endl;
                    key[index] = 1;
                    values[index] = harsha[3];
                    msg_to_send_back = "Insertion successful";
                }
                else
                {
                    msg_to_send_back = " key already exits";
                }
            }
            else
            {
                msg_to_send_back = "incorrect input";
            }
        }
        else if (strcmp(harsha[1], "delete") == 0)
        {
            if (z == 3)
            {
                int index = atoi(harsha[2]);
                if (key[index] != -1)
                {
                    key[index] = -1;
                    values[index].clear();
                    msg_to_send_back = "Deletion successful";
                }
                else
                {
                    msg_to_send_back = "No such key exists";
                }
            }
        }
        else if (strcmp(harsha[1], "update") == 0)
        {
            if (z == 4)
            {
                // cout << "coming" << endl;
                int index = atoi(harsha[2]);
                if (key[index] != -1)
                {
                    // cout << "coming" << endl;
                    values[index] = harsha[3];
                    msg_to_send_back = harsha[3];
                }
                else
                {
                    msg_to_send_back = " key doesnot exits";
                }
            }
            else
            {
                msg_to_send_back = "incorrect input";
            }
        }
        else if (strcmp(harsha[1], "concat") == 0)
        {
            if (z == 4)
            {
                // cout << "coming" << endl;
                int index1 = atoi(harsha[2]);
                int index2 = atoi(harsha[3]);

                if (key[index1] == -1 || key[index2] == -1)
                {
                    msg_to_send_back = " Concat failed as at least one of the keys does not exist";
                }
                else
                {
                    string val = values[index1];
                    values[index1] = values[index1] + values[index2];
                    values[index2] = values[index2] + val;
                    msg_to_send_back = values[index2];
                }
            }
            else
            {
                msg_to_send_back = "incorrect input";
            }
        }
        else if (strcmp(harsha[1], "fetch") == 0)
        {
            if (z == 3)
            {
                // cout << "coming" << endl;
                int index = atoi(harsha[2]);
                if (key[index] != -1)
                {
                    // cout << "coming" << endl;
                    msg_to_send_back = values[index];
                }
                else
                {
                    msg_to_send_back = " key doesnot exits";
                }
            }
            else
            {
                msg_to_send_back = "incorrect input";
            }
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        goto close_client_socket_ceremony;
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *popping(void *inp)
{
    pthread_mutex_lock(&pop_lock);
    while (1)
    {

        if (!Que.empty())
        {

            int curr = Que.front();
            Que.pop();
            pthread_mutex_unlock(&pop_lock);
            handle_connection(curr);
        }
        else
        {
            pthread_cond_wait(&con_var, &pop_lock);
        }
    }
}

int main(int argc, char *argv[])
{

    int num = atoi(argv[1]);
    cout << num << endl;
    pthread_t multithread[num];
    for (int i = 0; i < 1000; i++)
    {
        key[i] = -1;
    }

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    for (int i = 0; i < num; i++) // need to edit;
    {
        pthread_t curr_tid;
        pthread_create(&curr_tid, NULL, popping, (void *)(i));
        multithread[i] = curr_tid;
    }

    while (1)
    {

        printf("Waiting for a new client to request for a connection..\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        else
        {
            pthread_mutex_lock(&one_lock);

            pthread_mutex_lock(&push_lock);
            Que.push(client_socket_fd);
            pthread_mutex_unlock(&push_lock);
            pthread_mutex_unlock(&one_lock);
            pthread_cond_signal(&con_var);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        // handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}