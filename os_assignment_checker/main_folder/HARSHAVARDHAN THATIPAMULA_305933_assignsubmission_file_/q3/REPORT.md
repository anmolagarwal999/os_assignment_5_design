# Question 3: Multithreaded client and server [30 points]


## Description of the Question

- In this Question implementation of Multithreaded  client and server is made sucessfully.
- like my program works on executing multiple clients on a single server.
- In my implementation For each query of client a separate thread will be created and completes the work successfully.
- To make the impression of multiclient can be used at a time, I connected to server every time and disconnects from the    server after completing the job.
- In successful connection of client to the server, he user request thread can communicate with the
    assigned worker thread using a TCP socket. 
- I programmed to files : server.cpp and client.cpp .
- where server.cpp works on server part like recieving the request and send the appropreate message and client works on client side like sending the request.

## Execution 
- open two terminals one for server and other for client and go to the desired folder

```
<harsha@harsha-Vostro-3590:~/Desired folder/> server.cpp -o server -lpthread
<harsha@harsha-Vostro-3590:~/Desired folder/> ./server <no.of threads need to be created>

```

```
<harsha@harsha-Vostro-3590:~/Desired folder/> client.cpp -o client -lpthread
<harsha@harsha-Vostro-3590:~/Desired folder/> ./client

```

- in the client terminal we can input...

- for exiting out of program, run the following command :

```
<harsha@harsha-Vostro-3590:~> exit

```

this is all about how to execute the program.

## Logic Behind the Program.


### Server file

- Intially I declared to two arrays one for keys and othe for values to main dictionary scene.
- Intially I assigned all keys to be -1.
- Here the primary aim of the Question that is Multi thread system.
- I copied the <number of worker threads in the thread pool> to a variable and intialized a threadpool array of that size using pthread_t.
- Then I created and that number of threads and using pthread_create I sent them all to a function where the handle_connection implemented obeying not to enter more than one thread at a time and busy scheduling .
- Intially while receving input from the client, I sent them all to a Que.
- Using mutex lock and unlock I prevented not to happen any cause of deadlocks or multi thread invovling at atime.
- in other fuction I poped from Que and implemented them according to happen desired functions as per questions like insert,update,delete,fetch,concat using general logics and tokenzing them
- After implementing specific program send msg will be sent to client.
- Using multihthreads I used to perform different queries at a time.

## Client file

-  Intially I taken the input as per the question.
- I saved that variable and created that number of threads .
- I created a struct to store the time command and input values separetely for easy implementation.
- Calling a pthread_create for all the threads I sent every Query in a loop independently to a function where it connectes to the server and send the request.
- Finally it recives the information from the server and prints.

- These all about implementation of logic

