#include<bits/stdc++.h>
#include<sys/socket.h>

#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>
#define SERVER_PORT 8081
using namespace std;

vector<string> input_cmd;

void *client_thread(void *args){
    string s = *static_cast<string*>(args);

    //creating a socket
    int soc = socket(AF_INET, SOCK_STREAM, 0);
    if(soc < 0){
        cerr << "Error while creating socket.\n";
        exit(-1);
    }

    //initialising addresses and port numbers
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    int port = SERVER_PORT;
    server_addr.sin_port = htons(port);

    //connecting to server
    int connect_status = connect(soc, (struct sockaddr*)&server_addr, sizeof(server_addr));

    if(connect_status < 0){
        cerr << "Error occurred while connecting.\n" ;
        exit(-1);
    }

    int num_bytes = write(soc, s.c_str(), s.length());
    if(num_bytes < 0){
        cerr << "Failed to send data on socket.\n";
        exit(-1);
    }
    //cout << s << endl;
    cout << "Message sent successfully\n";
    close(soc);
    pthread_exit(NULL);
}



int main(){
    //no.of user requests
    int m;
    cin >> m;
    cin.ignore();

    string s;
    for(int i=0; i<m; i++){
        getline(cin, s);
        //cout << s << endl;
        fflush(stdin);
        input_cmd.push_back(s);
    }
    
    pthread_t thread_id[m];
    /*for(int i=0; i<m; i++){
        //int idx = i;
        pthread_create(&thread_id[i], NULL, client_thread, &(input_cmd[i]));
    }*/
    int time = 1;
    int temp = 1;
    for(int i=0; i<m;){
        int count = 0;
        while(i<m){
            string word = "";
            for(auto part : input_cmd[i]){
                if(part != ' '){
                    word = word + part;
                }else{
                    break;
                }
            }
            temp = stoi(word);
            if(temp == time){
                pthread_create(&thread_id[i], NULL, client_thread, &(input_cmd[i]));
                i++;
                count++;
            }else{
                break;
            }
        }
        sleep(1);
        time++;
    }
    for(int i=0; i<m; i++){
        pthread_join(thread_id[i], NULL);
    }

    return 0;
}