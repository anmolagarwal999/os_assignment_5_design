#include<bits/stdc++.h>
#include<sys/socket.h>

#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>
using namespace std;

queue<string> q;
map<int, string> m;

void *server_thread(void *args){
    if(!q.empty()){
        string s = q.front();
        q.pop();
        //parsing the input command
        vector<string> cmd_parts;
        string word = "";
        for(auto part : s){
            if(part != ' '){
                word = word + part;
            }else{
                cmd_parts.push_back(word);
                word = "";
            }
        }
        cmd_parts.push_back(word);
        string msg, value;
        int key, key1;
        if(cmd_parts[1] == "insert"){
            key = stoi(cmd_parts[2]);
            value = cmd_parts[3];
            if(m.find(key) == m.end()){
                m[key] = value;
                msg = "Insertion successful";
            }else{
                msg = "Key already exists";
            }
        }else if(cmd_parts[1] == "delete"){
            key = stoi(cmd_parts[2]);
            if(m.find(key) != m.end()){
                m.erase(key);
                msg = "Deletion successful";
            }else{
                msg = "No such key exists";
            }
        }else if(cmd_parts[1] == "update"){
            key = stoi(cmd_parts[2]);
            value = cmd_parts[3];
            auto it = m.find(key);
            if(it != m.end()){
                it->second = value;
                msg = value;
            }else{
                msg = "Key does not exist";
            }
        }else if(cmd_parts[1] == "concat"){
            key = stoi(cmd_parts[2]);
            key1 = stoi(cmd_parts[3]);
            auto it = m.find(key);
            auto it1 = m.find(key1);
            value = it->second;
            if(it != m.end() && it1 != m.end()){
                it->second = it->second+it1->second;
                it1->second = it1->second+value;
                msg = it1->second;
            }else{
                msg = "Concat failed as at least one of the keys does not exist";
            }
        }else if(cmd_parts[1] == "fetch"){
            key = stoi(cmd_parts[2]);
            auto it = m.find(key);
            if(it != m.end()){
                msg = it->second;
            }else{
                msg = "Key does not exist";
            }
        }else{
            msg = "Invalid Command";
        }
    }
}

int main(int argc, char *argv[]){
    int n = atoi(argv[0]);
    
    /*pthread_t thread_id[n];
    for(int i=0; i<n; i++){
        pthread_create(&thread_id[i], NULL, server_thread, NULL);
    }*/

    int server_soc = socket(AF_INET, SOCK_STREAM, 0);
    if(server_soc < 0){
        cerr << "Error while creating socket.\n";
        exit(-1);
    }

    struct sockaddr_in server_addr, client_addr;
    int port = 8081;
    bzero((char *)&server_addr, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);

    if((bind(server_soc, (struct sockaddr *)&server_addr, sizeof(server_addr))) < 0){
        cerr << "Error while binding the socket.\n";
        exit(-1);
    }

    listen(server_soc, n);
    socklen_t client_obj_len = sizeof(client_addr);
    
    while (1){
        int connection = accept(server_soc, (struct sockaddr *)&client_addr, &client_obj_len);

        if(connection < 0){
            cerr << "Error while accepting the connection.\n";
            exit(-1);
        }
        //cout << "Connection established successfully.\n";

        string input_cmd;
        input_cmd.resize(100);

        int bytes_received = read(connection, &input_cmd[0], 99);
        //cout << bytes_received << endl;
        if(bytes_received <=0){
            cerr << "Error receiving the message.\n";
            exit(-1);
        }
        input_cmd[bytes_received] = 0;
        input_cmd.resize(bytes_received);
        
        q.push(input_cmd);
        cout << input_cmd << endl;
        if(input_cmd == "stop"){
            break;
        }

    }
    /*for(int i=0; i<n; i++){
        pthread_join(thread_id[i], NULL);
    }*/
    close(server_soc);
}