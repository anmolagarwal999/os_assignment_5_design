# Question-3: #

### Instructions to run the files: ###
First compile and run the “server.cpp” file
- To compile use the command : g++ -pthread -o server server.cpp
- To run use the command: ./server <no.of worker threads in thread pool>
  
Now compile and run the “client.cpp” file
- To compile use the command: g++ -pthread -o client client.cpp
- To run use the command: ./client

### client.py ###
- First we take the input and store the input commands in a vector of strings.
- Now, according to the time at which the request has to be sent, we create as many number of threads as no.of user commands at particular time.
- And then call the "client_thread" function for each thread.
- In this function, each thread will create a socket and connect to server.
- After connecting to the server, it sends each user command as a string to server.

### server.py ###
- First we create a socket, bind it to the port, and the main thread starts listening to the port.
- After accpeting the client threads, it receives the data sent by them and it pushes them in a queue.
- Now, a pool of n threads are created and executes "server_thread" function.
- In this function, each thread will now pops the user command.
- Now, it parses the user command and finds the type of command, key and values.
- Now, it executes the command and sends back the user thread a response.
- For synchronisation, we need to use mutex locks and semaphores.