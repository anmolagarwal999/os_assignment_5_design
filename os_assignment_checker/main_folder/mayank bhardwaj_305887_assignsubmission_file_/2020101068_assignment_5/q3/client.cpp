#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////


typedef long long int LL;
const LL MOD = 1000000007;
#define pb push_back

///////////////////////////////
#define SERVER_PORT 8002
////////////////////////////////////

const LL buff_sz = 1048578;
///////////////////////////////////////////////////

pthread_mutex_t mutex_s, mutex_p, mutex_c;

typedef struct thread_details
{
    string s; // Stores request given to the thread
    int thread_index; // Stores index of thread
} td;

int m_delay = 0;


pair<string, int> Read_S_From_Socket(int file_descriptor, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_intake = read(file_descriptor, &output[0], bytes - 1);
    if (bytes_intake <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    output[bytes_intake] = 0;
    output.resize(bytes_intake);

    return {output, bytes_intake};
}

int send_string_on_socket(int file_descriptor, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(file_descriptor, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int Get_Socket_Fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_file_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_file_descriptor < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_file_descriptor, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    // printf(BGRN "Connected to server\n" ANSI_RESET);
    return socket_file_descriptor;
}
////////////////////////////////////////////////////////

void start() {
    int m;     
    string input;
    getline(cin, input);
    m = stoi(input);

    vector<string>myVec(m + 1);
    pthread_t thread_ids[m + 1];
    
    for (int i = 0; i < m; i++) {
        getline(cin, input);
        myVec[i] = input;
        int t = atoi((input.substr(0, input.find(" "))).c_str());
        if (m_delay > t)
            m_delay = t;
    }

    myVec[m] = to_string(m_delay + 1) + " fetch " + to_string(2) + " " + to_string(4);

    for (int i = 0; i <= m; i++) {
        pthread_t curr_thread_id;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->s = myVec[i];
        thread_input->thread_index = i;
        pthread_create(&curr_thread_id, NULL, begin_process, (void *)(thread_input));
        thread_ids[i] = curr_thread_id;
        if (i == m)
            break;
    }

   for (int i = 0; i < m; i++) 
        pthread_join(thread_ids[i], NULL);
}

void *begin_process(void *inp) {
    struct sockaddr_in server_obj;
    string input = ((struct thread_details *)inp)->s;
    sleep(atoi((input.substr(0, input.find(" "))).c_str()));

    pthread_mutex_lock(&mutex_c);
    int socket_file_descriptor = Get_Socket_Fd(&server_obj);
    send_string_on_socket(socket_file_descriptor, input);
    pthread_mutex_unlock(&mutex_c);

    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = Read_S_From_Socket(socket_file_descriptor, buff_sz);

    pthread_mutex_lock(&mutex_p);
    cout << ((struct thread_details *)inp)->thread_index << ":" << gettid() << ":" << output_msg.c_str() << endl;
    pthread_mutex_unlock(&mutex_p);
    return NULL;
}


int main(int argc, char *argv[])
{
    pthread_mutex_init(&mutex_p, NULL);
    pthread_mutex_init(&mutex_s, NULL);
    pthread_mutex_init(&mutex_c, NULL);

    start();
    return 0;
}