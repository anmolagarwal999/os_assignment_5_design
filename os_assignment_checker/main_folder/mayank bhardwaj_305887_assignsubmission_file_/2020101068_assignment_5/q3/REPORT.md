# Report for question 3
## Roll Number: 2020101068

### In client.cpp
struct Storing_thread_details
```cpp
typedef struct thread_details
{
    string s; // Stores request given to the thread
    int idx; // Stores index of thread
} td;
```

#### - create_threads() function
-- Mutex variable used- scan_mutex, print_mutex, condition_mutex.
-- I initialized above the three Mutex variables then I took the input for the number_of_user requests(m) and stored the corresponding requests into a vector of strings.
-- Then I created, m threads and called the my_begin_process().
-- After this I joined the threads.

#### - my_begin_process() function
-- First I extracted the time_delay of each request using below code 
```cpp
int time_delay = atoi((individual_string.substr(0, individual_string.find(" "))).c_str());
sleep(time_delay);
```
-- To get the socket_fd and send the string on socket, I called the mutex_lock on condition_mutex
```cpp
pthread_mutex_lock(&condition_mutex);
    int socket_fd = get_socket_fd(&server_obj);
    send_string_on_socket(socket_fd, individual_string);
    pthread_mutex_unlock(&condition_mutex);
```
-- To print the thread id I used gettid() and used print_mutex variable.
```cpp
pthread_mutex_lock(&print_mutex);
    printf("%d:%d:%s",thread_idx,gettid(),output_msg.c_str());
    pthread_mutex_unlock(&print_mutex);
    cout << endl;
```

#### In server_prog.cpp
```cpp
#define THREAD_POOL_SIZE 100  
#define MAX_CLIENTS 100
pthread_t thread_pool[THREAD_POOL_SIZE]; // array of threads
queue<int>q; // q storing client_socket_id
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; // mutex variable for q
pthread_cond_t condition_var = PTHREAD_COND_INITIALIZER; // condition variable
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER; // mutex variable for accessing keys-value pair in vector 

vector<pair<string, string>> vector_of_pairs; // vector storing key value pairs

```
#### In main()
-- I created n worker threads where n = argv[1]
```cpp
// Create n worker threads to give signal when the q is not empty
    for (int i = 0; i < atoi(argv[1]); i++) {
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }
```
-- In while loop, I used mutex variable to push client_socket_id into q
```cpp
pthread_mutex_lock(&mutex);
q.push(client_socket_fd);
pthread_mutex_unlock(&mutex);
pthread_cond_signal(&condition_var);
```

#### In thread_function()
-- this function will conditionally wait till the q is empty else it will pop the client_socket_id 
```cpp
void* thread_function(void* ptr) {
    while (true) {
        
        // Called mutex lock on queue
        pthread_mutex_lock(&mutex);
        pthread_cond_wait(&condition_var, &mutex);
        int pclient = q.front();
        
        // If queue is empty then wait 
        if (q.empty()) {
            pthread_cond_wait(&condition_var, &mutex);
            pclient = q.front(); 
            q.pop();
        }else { // Else pop the client_socket_id
            pclient = q.front(); 
            q.pop();
        }
        pthread_mutex_unlock(&mutex);
        
        handle_connection(pclient);
    }
    return NULL;
}
```


#### In handle_connection() 
```cpp
string msg_to_send_back = input_handler(cmd); // stores the output string 
```

#### In input_handler()
-- In this function I tokenized the input string on the basis of space and stored the string token in tokenisedStrings vector. Then it call the insert, delete, update, concat, fetch functions on the basis of tokenisedString[2]. 
```cpp
string input_handler(string input) {
    vector<string>tokenisedStrings; // stores tokenised strings
    cout <<"INPUT : " << input << endl;
    // Tokeinising the string on basis of space
    string word = "";
    for (auto x : input) 
    {
        if (x == ' ')
        {
            tokenisedStrings.push_back(word);
            word = "";
        }
        else {
            word = word + x;
        }
    }
    tokenisedStrings.push_back(word);
    
    if (tokenisedStrings[1] == "insert")
        return Insert_S(tokenisedStrings[2], tokenisedStrings[3]);
    
    else if (tokenisedStrings[1] == "delete")
        return Delete_S(tokenisedStrings[2]);
    
    else if (tokenisedStrings[1] == "update")
        return Update_S(tokenisedStrings[2], tokenisedStrings[3]);

    else if (tokenisedStrings[1] == "concat")
        return Concate_S(tokenisedStrings[2], tokenisedStrings[3]);
    
    return Get_String(tokenisedStrings[2]);
}
```

#### In Insert_S()
-- This command creates a new “key” on the server’s dictionary and set its value as <value>. If the “key” already exists on the server, then an appropriate error stating “Key already exists” is displayed. If successful, “Insertion successful” is displayed.
```cpp
string Insert_S(string key, string val) {
    string myMsg;
    pthread_mutex_lock(&mutex2);

    for (int i = 0; i < vector_of_pairs.size(); i++) {
        if (vector_of_pairs[i].first == key) {
            myMsg = "Key already exists";
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    
    myMsg = "Insertion successful";
    vector_of_pairs.push_back(make_pair(key, val));
    pthread_mutex_unlock(&mutex2);

    return myMsg;
}
```

#### In Delete_S()
-- This function removes the <key> from the dictionary. If no key with the name <key> exists, then an error is displayed “No such key exists” . If successful, message is displayed as “Deletion successful”.
```cpp
string Delete_S(string key) {
    string myMsg;
    int position = -1;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++) {
        if (vector_of_pairs[i].first == key) {
            position = i;
            vector_of_pairs.erase(vector_of_pairs.begin() + position);
            myMsg = "Deletion successful";
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    myMsg = "No such key exists";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}
```

#### In Update_S()
-- This function is updates the value corresponding to <key> on the server’s dictionary and set its value as <value>. If the “key” does not exist on the server, then an appropriate error “Key does not exist” is displayed. If successful, the updated value of the key
is displayed.
```cpp
string Update_S(string key, string val) {
    string myMsg;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++) {
        if (vector_of_pairs[i].first == key) {
            myMsg = val;
            vector_of_pairs[i].second = val;
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }
    myMsg = "Key does not exist";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}
```

#### In Concate_S()
-- This function updates the key should be {key1: value_1+value_2, key_2: value_2+value_1}. If either of the keys do not exist in the dictionary, an error message “Concat failed as at least one of the keys does not exist” is displayed. Else, the final value of key_2 is displayed.
```cpp
string Concate_S(string key1, string key2) {
    string myMsg;
    int position1 = -1, position2 = -1;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++) {
        if (vector_of_pairs[i].first == key1)
            position1 = i;
        if (vector_of_pairs[i].first == key2)
            position2 = i;
    }

    if (position1 == -1 || position2 == -1) {
        myMsg = "Concat failed as at least one of the keys does not exist";
        pthread_mutex_unlock(&mutex2);
        return myMsg;
    }

    string temp = vector_of_pairs[position1].second;
    vector_of_pairs[position1].second += vector_of_pairs[position2].second;
    vector_of_pairs[position2].second += temp;

    myMsg = vector_of_pairs[position2].second;
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}
```

#### In Get_String()
-- This function displays the value corresponding to the key if it exists at the connected server, and an error “Key does not exist” otherwise.
```cpp
string Get_String(string key) {
    string myMsg;
    pthread_mutex_lock(&mutex2);
    for (int i = 0; i < vector_of_pairs.size(); i++) {
        if (vector_of_pairs[i].first == key) {
            myMsg = vector_of_pairs[i].second;
            pthread_mutex_unlock(&mutex2);
            return myMsg;
        }
    }

    myMsg = "Key does not exist";
    pthread_mutex_unlock(&mutex2);
    return myMsg;
}
```


