# The Clasico Experience
## Name - Mayank Bhardwaj
## Roll no - 2020101068
The problem ask us to simulate situation where<br>
1. Spectators are buying thier tickets
2. Teams are scoring goals
3. Spectators are leaving beacuse of some reason

# Running the program
You can use
```sh
gcc -g -pthread q2.c
./a.out
```

# Explanation of what i have done
- This problem can be broadly broken down into 2 parts, one from the spectators viewpoint and one in terms of the goals in the match.
- So I create ```num_people``` threads which handle the routine of every spectator via the function ```person_routine``` which takes the spectator structure of its respective spectator as an argument.

# Working
- I have basically implemented only one part that is the arrival of the spectators on the stadium and printing that on the terminal .I have shown it through ```RED``` colour.
- Here is the code snippet for that part
``` cpp
pthread_t spectators[num_people];
    int counter_num_people=0;


    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_create(&spectators[counter_num_people++],NULL,&person_routine,(void*)&PERSON[i][j])!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }
    int counter_people=0;

    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_join(spectators[counter_people++],NULL)!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }
```
- For doing the above functionality the code snippet for the ```person_routine``` function is given below
``` cpp
void* person_routine(void* arg)
{   
    struct person *my_persons = (struct person*)arg;
    int time_to_reach_stadium= (*my_persons).arrival_time;
    sleep(time_to_reach_stadium);
    printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n",(*my_persons).name);
    return NULL;
}
```

