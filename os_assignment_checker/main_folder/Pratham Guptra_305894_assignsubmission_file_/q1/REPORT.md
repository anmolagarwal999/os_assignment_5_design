#REPORT

**Assumptions**
- the simulation is done by making threads for each `course` and `student`, and dynamic data arrays storing details of all `courses`, `students` and `labs`.
- the program terminates when all students have either selected a course permanently, or could not get any of their preferred courses, i.e. all student threads have returned to the main thread. This can be changed by un-commenting the `pthread_join()` statements for the course threads, so as to terminate the program only when all threads have returned, i.e when the courses are withdrawn due to lack of TA mentors.
- Every course thread can choose the TA parallely, only locking the lab it is currently searching in. The TA with the **least tutoring instances**(bonus question) from the lab is selected by calling the `available` function. The course thread initiates a `semaphore` with value equal to the slots allocated by the TA. The thread waits for a small `SLOTS_FILLING` time.
- The student threads after registering, wait on the semaphore corresponding to their current preferred course. 
- If selected, the execution of the student thread is temporarily suspended using a conditional variable that signals from the course thread, once the tutorial is completed after a period of `TUT_TIME`.
- Then, each student thread that completed the tutorial, calls the `decide` function to decide on selecting the said course permanently or moving to their next preference.
- the course thread returns once all TAs from labs that the course accepts have `EXHAUSTED`, i.e. have tutored the maximum number of times allowed in their respective labs.
- in case a course preferred by a student is `dropped`, the preference of the student waiting on it shifts to the next.
- in case no student comes to wait on a course during the `SLOTS_FILLING` time, the tutorial is normally carried out, with 0 participants by the selected TA.