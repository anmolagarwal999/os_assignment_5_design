#REPORT

- **CLIENTS.CPP**
	- client spawns `M user threads`, each of which connects to the server individually, on different sockets, to simulate distinct clients. 
	- Then, each locking a mutex, takes a line as input from the terminal which is sent onto the socket.
	- the blocking read system call waits for the server to send the `result` string onto the socket so that the user can print it.

- **SERVER.CPP**
	- server spawns `N worker threads` that wait on the size of the queue of input strings to be > 0, using a conditional variable.
	- Then, after accepting a connection from the a client(user thread), `handle_connection` function pushes the received string on the socket to the queue.
	- A push to the queue is accompanied by 'signal' over the conditional variable to the worker threads, one of which unblocks and pops the string out of the queue
	- The worker threads are capable of operating on client strings and carrying out necessary actions on the `dictionary` simultaneously, while ensuring thread safety by locking only the particularly required keys.
	- once the operation is done, the worker thread sends the `result` string to the respective `client socket` and closes it.
