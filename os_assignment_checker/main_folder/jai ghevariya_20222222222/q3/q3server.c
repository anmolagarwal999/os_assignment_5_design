#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
sem_t x, y;
pthread_t tid;
pthread_t writerthreads[100];
pthread_t readerthreads[100];
int readercount = 0;
void* reader(void* param){
    int i = *(int*)param;
    sem_wait(&y);
    sem_wait(&x);
    printf("Reader %d is reading\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);
}
void* writer(void* param){
    int i = *(int*)param;
    sem_wait(&y);
    sem_wait(&x);
    printf("Writer %d is writing\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);
}
int main(){
int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[1024];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    pthread_t tid;
    int i = 0;
    sem_init(&x, 0, 1);
    sem_init(&y, 0, 1);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        printf("ERROR opening socket");
        exit(1);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = 1234;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("ERROR on binding");
        exit(1);
    }
    listen(sockfd, 5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0)
    {
        printf("ERROR on accept");
        exit(1);
    }
    while (1)
    {
        bzero(buffer, 1024);
        n = read(newsockfd, buffer, 1023);
        if (n < 0)
        {
            printf("ERROR reading from socket");
            exit(1);
        }
        if (strcmp(buffer, "read") == 0)
        {
            pthread_create(&tid, NULL, reader, &i);
            readercount++;
            i++;
        }
        else if (strcmp(buffer, "write") == 0)
        {
            pthread_create(&tid, NULL, writer, &i);
            i++;
        }
        else if (strcmp(buffer, "exit") == 0)
        {
            break;
        }
    }
    close(newsockfd);
    close(sockfd);
    return 0;


}