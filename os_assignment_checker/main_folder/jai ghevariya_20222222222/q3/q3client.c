
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
void *clienthread(void *args)
{
    int sock = *(int *)args;
    char buffer[1024];
    int n;
    while (1)
    {
        bzero(buffer, 1024);
        n = read(sock, buffer, 1023);
        if (n < 0)
        {
            printf("ERROR reading from socket");
            exit(1);
        }
        printf("%s\n", buffer);
    }
}
int main()
{
    int sock, csock;
    struct sockaddr_in server, client;
    int c;
    pthread_t thread_id;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        printf("Could not create socket");
        return 1;
    }
    server.sin_addr.s_addr = inet_addr("");
    server.sin_family = AF_INET;
    server.sin_port = htons(8080);
    if (bind(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("bind failed. Error");
        return 1;
    }
    listen(sock, 5);
    c = sizeof(struct sockaddr_in);
    while (1)
    {
        csock = accept(sock, (struct sockaddr *)&client, (socklen_t *)&c);
        if (pthread_create(&thread_id, NULL, clienthread, &csock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
    }
    return 0;
}
