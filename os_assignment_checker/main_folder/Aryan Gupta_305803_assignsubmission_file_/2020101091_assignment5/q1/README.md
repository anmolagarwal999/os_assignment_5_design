# Q1->
Run code by gcc q1.c -lpthread

I impemented the model where we can start the tutorial with zero students .


In this problem , I defined structs for Labs , courses  and students and each course and student have their independent thread .
- I will only wait till for the student thread to finish their execution cause our job is to assign course (if possible) to each students .
- In the main function , I call both the student and course thread function handler .
- student thread :
    First i wait for the time for student entry .
    Then in infinite while loop till the priority becomes 3 (that is all choices are exhausted), I do the following :
        I lock the student and check if course is available , else I increase the priority ( and handle the case if priority becomes 3 , that is when the student gets none of his preferred choices)
        If course is available , I wait until signal from course thread .
        Now two things can happen , either the student will compete the course or the course might get removed 
        Through the if condition of whether course of student is equal to what  it should be , we handle both case 
        If student did a course , we calculate probability and check if the student wants to take that course permanently or not using that probability we just calculated else if probability is less , we increase the priority and then unlock student .
        Similarly if course was removed , we just increase the priority and check for the priority 3 case .

- Course thread : 
    Again we start an infinite for loop and do the following ;
        First we check the TA_times of all the TA's of the Labs assigned to this course and check whether this course is to be removed or not (that is all TA's are exhausted). If course is removed , then we print the same and break from the loop .
        Before breaking , we also go through all students who have that course and send them signal so that they can increase their priority . 

        Else we go through all TA's and the Labs and then for every TA's , we check if the TA is free and his no of times is less than max limit , then we lock that TA for this course and go through all student - check if they are free and whether they have the course as the one we are iterating through . If all conditions are satisfied from student side , then we lock this student and assign him this course.
        After doing this for all student , we print the required things , and conduct the tutorial and then unlock TA and student(and send signal) . 

