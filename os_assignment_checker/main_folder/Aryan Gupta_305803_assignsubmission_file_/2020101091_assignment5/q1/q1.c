#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
// Colours
#define RED printf("\033[0;31m");
#define YELLOW printf("\033[0;33m");
#define BLUE printf("\033[0;34m");
#define MAGENTA printf("\033[0;35m");
#define GREEN printf("\033[0;32m");
#define RESET printf("\033[0m");

typedef struct Lab
{
    char name[50];
    int number_of_TAs;
    int max_limit_possible;
    int cur_TA_times[50];
    pthread_mutex_t TA_lock[50];
} Lab;

typedef struct Course
{
    pthread_t tid;
    int course_index;
    char course_name[50];
    int labs_available[50];
    double interest;
    int total_Labs;
    int total_slots;
} Course;

typedef struct Student
{
    pthread_t tid;
    int student_index;
    int courses[3];
    double calibre;
    int cur_priority;
    int course;
    int time;
    bool is_available;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} Student;
Lab Labs[1001];
Course courses[1001];
Student students[1001];
bool course_available[1001];
int number_of_students = 0, number_of_labs = 0, number_of_courses = 0;
void *studentThread(void *arg);
void *courseThread(void *arg);
int main(void)
{
    memset(course_available, true, sizeof(course_available));

    scanf("%d %d %d", &number_of_students, &number_of_labs, &number_of_courses);
    for (int i = 0; i < number_of_courses; i++)
    {
        scanf("%s %lf %d %d", courses[i].course_name, &courses[i].interest, &courses[i].total_slots, &courses[i].total_Labs);
        courses[i].course_index = i;
        for (int j = 0; j < courses[i].total_Labs; j++)
            scanf("%d", &courses[i].labs_available[j]);
    }

    for (int i = 0; i < number_of_students; i++)
    {
        scanf("%lf %d %d %d %d", &students[i].calibre, &students[i].courses[0], &students[i].courses[1], &students[i].courses[2], &students[i].time);
        students[i].cur_priority = -1;
        students[i].student_index = i;
        students[i].course = -1;
        students[i].is_available = false;
        pthread_mutex_init(&students[i].mutex, NULL);
        pthread_cond_init(&students[i].cond, NULL);
    }

    for (int i = 0; i < number_of_labs; i++)
    {
        int j;
        scanf("%s %d %d", Labs[i].name, &j, &Labs[i].max_limit_possible);
        Labs[i].number_of_TAs = j;
        for (int k = 0; k < j; k++)
            pthread_mutex_init(&Labs[i].TA_lock[k], NULL);
    }
    srand(time(0));
    for (int i = 0; i < number_of_courses; i++)
    {
        pthread_create(&courses[i].tid, NULL, courseThread, &courses[i]);
    }
    for (int i = 0; i < number_of_students; i++)
    {
        pthread_create(&students[i].tid, NULL, studentThread, &students[i]);
    }
    for (int i = 0; i < number_of_students; i++)
    {
        pthread_join(students[i].tid, NULL);
    }
}
// The thread for students
void *studentThread(void *arg)
{
    Student *S = (Student *)arg;
    sleep(S->time);
    YELLOW
    printf("Student %d has filled in preferences for course registration\n", S->student_index);
    RESET
    S->is_available = true;
    S->cur_priority = 0;
    while (S->cur_priority <= 2)
    {
        pthread_mutex_lock(&S->mutex);
        if (course_available[S->courses[S->cur_priority]] == true)
        {
            pthread_cond_wait(&S->cond, &S->mutex);
        }
        if (S->course == S->courses[S->cur_priority])
        {
            double p = S->calibre * courses[S->course].interest;
            double ra = (rand() % 100);
            double r = (ra / (100.00));
            if (p >= r)
            {
                BLUE
                    printf("Student %d has selected the course %s permanently\n", S->student_index, courses[S->course].course_name);
                RESET
                S->is_available = false;
                pthread_mutex_unlock(&S->mutex);
                break;
            }
            else
            {
                S->cur_priority++;
                if (S->cur_priority == 3)
                {
                    S->is_available = false;
                    GREEN
                    printf("Student %d has withdrawn from course %s \n", S->student_index, courses[S->courses[S->cur_priority - 1]].course_name);
                    RESET

                    BLUE
                        printf("Student %d couldn’t get any of his preferred courses\n", S->student_index);
                    RESET
                    pthread_mutex_unlock(&S->mutex);
                    break;
                }
                else
                {
                    S->is_available = true;

                    MAGENTA
                    printf("Student %d has withdrawn from course %s \n", S->student_index, courses[S->courses[S->cur_priority - 1]].course_name);
                    RESET
                    GREEN
                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, courses[S->course].course_name, S->cur_priority, courses[S->courses[S->cur_priority]].course_name, S->cur_priority + 1);
                    RESET
                    S->course = -1;
                }
            }
        }
        else
        {
            S->cur_priority++;
            if (S->cur_priority == 3)
            {
                S->is_available = false;
                RED
                    printf("Student %d couldn’t get any of his preferred courses\n", S->student_index);
                RESET
                pthread_mutex_unlock(&S->mutex);
                break;
            }
            S->is_available = true;

            BLUE
                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, courses[S->courses[S->cur_priority - 1]].course_name, S->cur_priority, courses[S->courses[S->cur_priority]].course_name, S->cur_priority + 1);
            RESET
            S->course = -1;
        }

        pthread_mutex_unlock(&S->mutex);
    }
}

void *courseThread(void *arg)
{
    Course *C = (Course *)arg;
    while (true)
    {
        int cz = 0;
        for (int i = 0; i < C->total_Labs; i++)
        {
            int lab = C->labs_available[i];
            for (int j = 0; j < Labs[lab].number_of_TAs; j++)
            {
                if (Labs[lab].cur_TA_times[j] < Labs[lab].max_limit_possible)
                {
                    cz++;
                }
            }
        }
        if (!cz)
        {
            course_available[C->course_index] = false;
            MAGENTA
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", C->course_name);
            RESET
            for (int i = 0; i < number_of_students; i++)
            {
                if (students[i].is_available == true && students[i].courses[students[i].cur_priority] == C->course_index)
                {
                    pthread_cond_signal(&students[i].cond);
                }
            }
            break;
        }
        int which_taken[101];
        int how_many = 0;
        for (int l = 0; l < C->total_Labs; l++)
        {
            for (int t = 0; t < Labs[C->labs_available[l]].number_of_TAs; t++)
            {
                pthread_mutex_lock(&Labs[C->labs_available[l]].TA_lock[t]);
                if (Labs[C->labs_available[l]].cur_TA_times[t] < Labs[C->labs_available[l]].max_limit_possible)
                {
                    int chosen = 0;
                    int total = (rand() % C->total_slots) + 1;
                    for (int i = 0; i < number_of_students; i++)
                    {
                        int c = students[i].courses[students[i].cur_priority];
                        if (chosen < total && students[i].is_available == true && c == C->course_index)
                        {

                            pthread_mutex_lock(&students[i].mutex);
                            students[i].is_available = false;
                            chosen++;
                            students[i].course = C->course_index;
                            which_taken[how_many] = i;
                            how_many++;
                        }
                    }
                    int ch = Labs[C->labs_available[l]].cur_TA_times[t] + 1;
                    if (ch == 1)
                    {
                        MAGENTA
                        printf("TA %d from the lab %d has been allocated to course %s for his %dst TA ship\n", t, C->labs_available[l], C->course_name, Labs[C->labs_available[l]].cur_TA_times[t] + 1);
                        RESET
                    }
                    else if (ch == 2)
                    {
                        MAGENTA
                        printf("TA %d from the lab %d has been allocated to course %s for his %dnd TA ship\n", t, C->labs_available[l], C->course_name, Labs[C->labs_available[l]].cur_TA_times[t] + 1);
                        RESET
                    }
                    else if (ch == 3)
                    {
                        MAGENTA
                        printf("TA %d from the lab %d has been allocated to course %s for his %drd TA ship\n", t, C->labs_available[l], C->course_name, Labs[C->labs_available[l]].cur_TA_times[t] + 1);
                        RESET
                    }
                    else
                    {
                        MAGENTA
                        printf("TA %d from the lab %d has been allocated to course %s for his %dth TA ship\n", t, C->labs_available[l], C->course_name, Labs[C->labs_available[l]].cur_TA_times[t] + 1);
                        RESET
                    }
                    YELLOW
                    printf("Course %s has been allocated %d seats\n", C->course_name, total);
                    RESET
                    for (int i = 0; i < how_many; i++)
                    {
                        int s = which_taken[i];
                        GREEN
                        printf("Student %d has been allocated a seat in course %s\n", s, C->course_name);
                        RESET
                    }
                    YELLOW
                    printf("Tutorial has started for Course %s with %d seats filled out of %d\n", C->course_name, chosen, total);
                    RESET

                    ///// conducting tutorial
                    sleep(6);

                    /// conducting tutorial
                    Labs[C->labs_available[l]].cur_TA_times[t]++;
                    for (int i = 0; i < how_many; i++)
                    {
                        int s = which_taken[i];
                        pthread_mutex_unlock(&students[s].mutex);
                        pthread_cond_signal(&students[s].cond);
                    }
                    pthread_mutex_unlock(&Labs[C->labs_available[l]].TA_lock[t]);
                    GREEN
                    printf("TA %d from lab %s has completed the tutorial and left the course %s\n", t, Labs[C->labs_available[l]].name, C->course_name);
                    RESET
                    int over = 0;
                    for (t = 0; t < Labs[C->labs_available[l]].number_of_TAs; t++)
                    {
                        if (Labs[C->labs_available[l]].cur_TA_times[t] < Labs[C->labs_available[l]].max_limit_possible)
                        {
                            over++;
                        }
                    }
                    if (!over)
                    {
                        MAGENTA
                        printf("Lab %s no longer has TA available for TA'ship\n", Labs[C->labs_available[l]].name);
                        RESET
                    }
                    how_many = 0;
                }
                else
                {
                    pthread_mutex_unlock(&Labs[C->labs_available[l]].TA_lock[t]);
                }
            }
        }
    }
}