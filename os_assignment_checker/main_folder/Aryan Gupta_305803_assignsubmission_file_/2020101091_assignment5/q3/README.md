# Q3 - >
Run q3_c.cpp for client function and q3_s.cpp for server function . 
Run g++ -std=c++17 q3_c.cpp -lpthread  for client function
and g++ -std=c++17 q3_s.cpp -lpthread for server function 

In the client side , 
     we took all input and then created as many threads as no of clients. 
     Now in the client thread function , we create the socket and then sleep for the time as given in input for that input . 
     After that , we lock this client so that only he is able to send command to the server . 
     After sending the command , we unlock and then again lock it for reading the output from server . 
     Now we just print whatever output we got from server and exit the thread .
     This program exits after all the clients have been dealt with .
In server side , 
    We use a map of {int,string} to maintain the dictionary . 
    We create as many threads as there are workers and also maintain queue to store the commands .
    After this we call the thread function and server intialise funtion .
    In server init function :
        - We create the socket and invoke listen
        - Now we have an infinite for loop, if we get a request , we lock the queue , push the commonad into it and again unlock . 
    In the thread function :
        - We have an infite for loop , and check  if queue has any entry , if it has we select it and then pop it else we wait for signal from the init server function .
        - Now in the handler function , we just break the command into individual words and handle all the commands like :
            Insert : We just check if key is present is map or not , it not we create that key else print key already exist .
            Concat : Only if both keys are presnet , we make the changes else not .
            and so on ..
            After processing the request , we write back the appropriate message back to the client . 

 