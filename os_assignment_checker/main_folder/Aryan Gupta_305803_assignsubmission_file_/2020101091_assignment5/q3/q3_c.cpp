#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
typedef pair<int, int> II;
typedef vector<II> VII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define PB push_back
#define MP make_pair
#define F first
#define S second
#define SZ(a) (int)(a.size())
#define ALL(a) a.begin(), a.end()
#define SET(a, b) memset(a, b, sizeof(a))
#define FOR(i, a, b) for (int i = (a); i < (b); ++i)
#define REP(i, n) FOR(i, 0, n)
#define si(n) scanf("%d", &n)
#define dout(n) printf("%d\n", n)
#define sll(n) scanf("%lld", &n)
#define lldout(n) printf("%lld\n", n)
#define fast_io                       \
    ios_base::sync_with_stdio(false); \
    cin.tie(NULL)
#define endl "\n"
const long long mod = 1e9 + 7;

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <assert.h>
#include <tuple>
#include <pthread.h>
int port_no = 8001;
struct Client
{
    int index;
    int time;
    pthread_t tid;
    string command;
    pthread_mutex_t mutex;
};
vector<struct Client> client;
pthread_mutex_t print_protection;

void *client_thread(void *(arg))
{
    int identity = *(int *)arg;
    struct sockaddr_in server_obj;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = port_no;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    // https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    int s = client[identity].time;
    sleep(s);

    pthread_mutex_lock(&client[identity].mutex);
    int bytes_sent = write(socket_fd, client[identity].command.c_str(), client[identity].command.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        pthread_mutex_unlock(&client[identity].mutex);

        return NULL;
    }
    pthread_mutex_unlock(&client[identity].mutex);
    pthread_mutex_lock(&client[identity].mutex);
    string output;
    output.resize(1048576);

    int bytes_received = read(socket_fd, &output[0], 1048575);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        pthread_mutex_unlock(&client[identity].mutex);
        return NULL;
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    pthread_mutex_unlock(&client[identity].mutex);
    pthread_mutex_lock(&print_protection);
    cout << client[identity].index << ":" << gettid() << ":" << output << endl;
    pthread_mutex_unlock(&print_protection);
    return NULL;
}

signed main()
{
    print_protection = PTHREAD_MUTEX_INITIALIZER;
    int no_of_clients;
    cin >> no_of_clients;
    client = vector<struct Client>(no_of_clients);
    FOR(i, 0, no_of_clients)
    {
        client[i].index = i;
        client[i].mutex = PTHREAD_MUTEX_INITIALIZER;
        int time;
        cin >> time;
        client[i].time = time;
        string c;
        getline(cin, c);
        client[i].command = c;
    }
    FOR(i, 0, no_of_clients)
    {
        int *index = new int;
        *index = client[i].index;
        pthread_create(&client[i].tid, NULL, client_thread, (void *)index);
    }
    FOR(i, 0, no_of_clients)
    {
        pthread_join(client[i].tid, NULL);
    }
}
