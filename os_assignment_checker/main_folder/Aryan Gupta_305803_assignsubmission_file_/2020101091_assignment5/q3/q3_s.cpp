#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
typedef pair<int, int> II;
typedef vector<II> VII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define PB push_back
#define MP make_pair
#define F first
#define S second
#define SZ(a) (int)(a.size())
#define ALL(a) a.begin(), a.end()
#define SET(a, b) memset(a, b, sizeof(a))
#define FOR(i, a, b) for (int i = (a); i < (b); ++i)
#define REP(i, n) FOR(i, 0, n)
#define si(n) scanf("%d", &n)
#define dout(n) printf("%d\n", n)
#define sll(n) scanf("%lld", &n)
#define lldout(n) printf("%lld\n", n)
#define fast_io                       \
    ios_base::sync_with_stdio(false); \
    cin.tie(NULL)
#define endl "\n"
const long long mod = 1e9 + 7;
int no_servers;
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <assert.h>
#include <tuple>
#include <pthread.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
int port_no = 8001;
map<int, string> mm;
queue<int *> q;
pthread_mutex_t q_lock;
pthread_cond_t cond_var;
vector<pthread_mutex_t> locks;
// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
void init();
void *server_thread(void *(arg));
int get_num(string A)
{
    stringstream geek(A);

    // The object has the value 12345 and stream
    // it to the integer x
    int key = 0;
    geek >> key;
    return key;
}
int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}
void solve(string A, int client_in)
{
    vector<string> B;
    istringstream ss(A);
    string word;
    while (ss >> word)
    {
        B.PB(word);
    }
    int num = B.size();
    if (num == 0)
    {
        return;
    }
    if (B[0] == "insert")
    {
        if (B.size() != 3)
        {
            cout << "Error : Insert should have 3 arguments \n";
            send_string_on_socket(client_in, "Error : Insert should have 3 arguments");
            return;
        }
        int key = get_num(B[1]);
        pthread_mutex_lock(&locks[key]);
        if (mm.find(key) != mm.end())
        {
            cout << "Key already exists" << endl;
            send_string_on_socket(client_in, "Key already exists");
        }
        else
        {
            mm[key] = B[2];
            cout << "Insertion successful" << B[2] << " at " << key << endl;
            send_string_on_socket(client_in, "Insertion successful");
        }
        pthread_mutex_unlock(&locks[key]);
    }
    else if (B[0] == "concat")
    {
        if (B.size() != 3)
        {
            cout << "Error : Insert should have 3 arguments \n";
            send_string_on_socket(client_in, "Error : Insert should have 3 arguments");
            return;
        }
        int key1 = get_num(B[1]);
        int key2 = get_num(B[2]);
        pthread_mutex_lock(&locks[key1]);
        pthread_mutex_lock(&locks[key2]);
        if (mm.find(key1) != mm.end() && mm.find(key2) != mm.end())
        {
            string temp = mm[key1];
            mm[key1] = temp + mm[key2];
            mm[key2] = mm[key2] + temp;
            cout << "Concatenation Succesful" << endl;
            send_string_on_socket(client_in, mm[key2]);
        }
        else
        {
            cout << "Either one of the keys is not present" << endl;
            send_string_on_socket(client_in, "Concat failed as at least one of the keys does not exist");
        }
        pthread_mutex_unlock(&locks[key2]);
        pthread_mutex_unlock(&locks[key1]);
    }
    else if (B[0] == "fetch")
    {
        if (B.size() != 2)
        {
            cout << "Error : Insert should have 2 arguments \n";
            send_string_on_socket(client_in, "Error : Insert should have 2 arguments");
            return;
        }
        int key = get_num(B[1]);
        pthread_mutex_lock(&locks[key]);
        if (mm.find(key) == mm.end())
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_in, "Key does not exist");
        }
        else
        {
            cout << "Fetch Succesful" << endl;
            send_string_on_socket(client_in, mm[key]);
        }
        pthread_mutex_unlock(&locks[key]);
    }
    else if (B[0] == "update")
    {
        if (B.size() != 3)
        {
            cout << "Error : Insert should have 3 arguments \n";
            send_string_on_socket(client_in, "Error : Insert should have 3 arguments");
            return;
        }
        int key = get_num(B[1]);
        pthread_mutex_lock(&locks[key]);
        if (mm.find(key) == mm.end())
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_in, "Key does not exist");
        }
        else
        {
            mm[key] = B[2];
            cout << "Update Succesful" << endl;
            send_string_on_socket(client_in, mm[key]);
        }
        pthread_mutex_unlock(&locks[key]);
    }
    else if (B[0] == "delete")
    {
        if (B.size() != 2)
        {
            cout << "Error : Insert should have 2 arguments \n";
            send_string_on_socket(client_in, "Error : Insert should have 2 arguments");
            return;
        }
        int key = get_num(B[1]);
        pthread_mutex_lock(&locks[key]);
        if (mm.find(key) == mm.end())
        {
            cout << "Key doesn't exist" << endl;
            send_string_on_socket(client_in, "No such key exists");
        }
        else
        {
            mm.erase(key);
            cout << "Deletion Succesful" << endl;
            send_string_on_socket(client_in, "Deletion successful");
        }
        pthread_mutex_unlock(&locks[key]);
    }
    else
    {
        cout << "Incorrect operation" << endl;
        send_string_on_socket(client_in, "Incorrect operation");
    }
}
void *server_thread(void *(arg))
{
    while (true)
    {
        pthread_mutex_lock(&q_lock);
        while (q.size() == 0)
        {
            pthread_cond_wait(&cond_var, &q_lock);
        }
        int *client_in = q.front();
        q.pop();
        pthread_mutex_unlock(&q_lock);
        string output;
        output.resize(1048576);

        int bytes_received = read(*client_in, &output[0], 1048576);
        if (bytes_received <= 0)
        {
            cerr << "Failed to read data from socket. \n";
        }

        output[bytes_received] = 0;
        output.resize(bytes_received);
        solve(output, *client_in);
    }
}
void init()
{
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(1);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = port_no;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number);

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(1);
    }

    listen(wel_socket_fd, 100);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {

        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(1);
        }
        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        int *idx = new int;
        *idx = client_socket_fd;
        pthread_mutex_lock(&q_lock);
        q.push(idx);
        pthread_mutex_unlock(&q_lock);
        pthread_cond_signal(&cond_var);
    }
    close(wel_socket_fd);
}
int main(int argc, char *argv[])
{
    mm.clear();
    no_servers = stoi(argv[1]);
    locks = vector<pthread_mutex_t>(101);
    q_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_t server_th[no_servers];
    cond_var = PTHREAD_COND_INITIALIZER;
    FOR(i, 0, 101)
    {
        pthread_mutex_init(&locks[i], NULL);
    }
    FOR(i, 0, no_servers)
    {
        pthread_create(&server_th[i], NULL, server_thread, NULL);
    }
    init();
}