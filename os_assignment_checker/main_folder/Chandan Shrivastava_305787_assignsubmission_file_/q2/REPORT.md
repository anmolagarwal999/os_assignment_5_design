# Operating System and Networks
## Asssignment 5
---
## Question 2
To run the simulation, you need to run the following command:
```bash
gcc 2.c -o a
./a
```

Approach:
1. Created a function named reach_stadium to check whether a person reached the stadium or not.
2. Created a function alloc_zone which check whether a person has reached or not and assigns zone to that person.
3. Created a function goalscorer which check whether a team has scored a goal or not based on the probability.
4. Created a function person_timeout which checks whether a person has reached his waiting time threshold or watch time threshold. If they have reached the threshold then they will leave the stadium.
5. Created a function bad_perf which check whether a person has reached his bad performance of his team threshold or not. If they have reached the threshold then they will leave the stadium.
6. Created a function exit_sim which check whether all persons have left the stadium or not.
7. All function are handle by seperate threads. Used mutex to synchronize the threads. USed color codes to print the output.