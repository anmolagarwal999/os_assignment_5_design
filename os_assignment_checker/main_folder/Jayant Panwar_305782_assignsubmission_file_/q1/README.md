# Submitted by Jayant Panwar, 2019114013

## Content

The content of the q1 directory is as follows:

- `Report_2019114013`: contains the brief explanation of the logic behind the implementation
- `Makefile`: contains the necessary code for compiling the files with relevant flags
- `main.c`: contains the starter code for the program
- `headers.h`: contains all the libraries used and the structs of the major entities like Courses, Students, Labs
- `init.c, init.h`: deals with invoking initializations of entities and invoking the creation of their threads
- `labs.c, labs.h`: deals with initialization, thread-creation, and simulation of Lab entity
- `students.c, students.h`: deals with initialization, thread-creation, and simulation of Student entity
- `courses.c, courses.h`: deals with initialization, thread-creation, and simulation of Courses entity. Also contains BONUS implementation logic.
- `colors.h`: lists the colors used for printing event information during simulation

## How to run the code

For running the code:

```c
make
```

this will make the executable and then just type:

```c
./q1
```

This will run the executable file and the inputs can be provided according to the question.
Note you can also remove the compiled object files if you want to compile a fresh executable by typing:

```c
make clean
```
