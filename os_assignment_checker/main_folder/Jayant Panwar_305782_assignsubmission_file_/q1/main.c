#include "headers.h"
#include "init.h"

int randomRange(int l, int r)
{
  if (l > r)
  {
    return l;
  }

  return (rand() % (r - l + 1)) + l;
}

int main()
{
  scanf("%d %d %d", &Students, &Labs, &Courses);

  initialization();

  int x = 0;
  int simOver = 0;

  while (x < Students)
  {
    pthread_join(student_thread[x], 0);
    x++;
  }

  simOver = 1;
  x = 0;

  while (x < Students && simOver)
  {
    if (students[x]->curr_pref != -1)
    {
      simOver = 0;
    }
    x++;
  }

  if (simOver)
  {
    printf("Simulation Completed!");
    printf("\n");
  }

  return 0;
}