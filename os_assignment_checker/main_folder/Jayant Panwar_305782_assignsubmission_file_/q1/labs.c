#include "headers.h"
#include "colors.h"
#include "labs.h"

void *labSim(void *a)
{
  Lab *l = (Lab *)a;
  while (l->ta_worthy != 2 && l->ta_limit == false)
  {
    pthread_mutex_lock(&l->mutex);
    l->ta_worthy = 0;
    l->ta_limit = true;

    for (int i = 0; i < l->n_tas; i++)
    {
      if (l->curr_times[i] > l->max_times)
      {
        continue;
      }
      l->ta_limit = false;
      l->ta_worthy = 1;
    }

    // EVENT 12
    if (!l->ta_worthy && l->ta_limit == true)
    {
      l->ta_worthy = 2;
      printf(COLOR_BRMAGENTA "Lab %s no longer has students available for TA ship\n" COLOR_RESET, l->name);
    }
    pthread_mutex_unlock(&l->mutex);
  }

  return 0;
}

void createLabThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&lab_thread[i], 0, labSim, college_labs[i]);
  }
}

void initLab(int i)
{
  int UID = i;
  Lab *l = (Lab *)malloc(sizeof(Lab));
  scanf("%s %d %d", l->name, &l->n_tas, &l->max_times);

  l->uid = UID;
  l->ta_worthy = 0;
  l->ta_limit = false;

  int numTAs = l->n_tas;
  int j = 0;
  while (j < numTAs)
  {
    l->curr_times[j] = 0;
    l->availability[j] = true;
    j++;
  }

  pthread_mutex_init(&l->mutex, 0);

  college_labs[i] = l;
}