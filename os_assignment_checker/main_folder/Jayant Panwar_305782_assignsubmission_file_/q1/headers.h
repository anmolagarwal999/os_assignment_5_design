#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int Students; // no of students
int Labs;     // no of labs
int Courses;  // no of courses

typedef struct Student
{
  int uid;

  double callibre;
  int arr_time;
  int finalised;
  int curr_pref;
  int curr_alloc;
  int pref[3]; // stores all the three preferences

  pthread_mutex_t mutex;
} Student;

typedef struct Course
{
  int uid;

  double interest;
  int course_max_slots;
  int p;
  int d;
  int ta_allocated;
  char name[100];
  int course_labs[100];

  int lab_allocated;
  bool tutorial;
  int tut_seats;
  int courseValid;

  pthread_mutex_t mutex;
} Course;

typedef struct Lab
{
  int uid;

  char name[100];
  int curr_times[100];
  bool availability[100];

  int n_tas;
  int max_times;
  int ta_worthy;
  bool ta_limit;

  pthread_mutex_t mutex;
} Lab;

Student **students;
Course **courses;
Lab **college_labs;

pthread_t *student_thread;
pthread_t *course_thread;
pthread_t *lab_thread;

int randomRange(int l, int r);