#include "headers.h"
#include "colors.h"
#include "courses.h"
#include <stdbool.h>

void *courseSim(void *a)
{
  Course *t = (Course *)a;
  bool running = true;

  while (running)
  {
    pthread_mutex_lock(&t->mutex);
    int no_labs = t->p;
    int k = 0;
    while (k < no_labs)
    {
      Lab *l = college_labs[t->course_labs[k]];
      char *labname = l->name;
      if (strlen(labname) > 80)
      {
        labname = l->name;
      }
      pthread_mutex_lock(&l->mutex);
      for (int i = 0; i < l->n_tas; i++)
      {
        // checking if the TAs are available or not
        int maxTimes = l->max_times;
        int currTimes = l->curr_times[i];
        if (l->availability[i] == true && currTimes < maxTimes)
        {
          // checking in case the TA is not allocated yet
          if (t->ta_allocated == -1)
          {

            // BONUS part
            int equalOpp = 1;
            int numTAs = l->n_tas;
            for (int j = 0; j < numTAs; j++)
            {
              if (j == i)
              {
                continue;
              }
              if (l->curr_times[i] - l->curr_times[j] > 0)
              {
                equalOpp = 0;
              }
            }

            if (!equalOpp)
            {
              pthread_mutex_lock(&l->mutex);
              break;
            }

            t->ta_allocated = i;
            t->lab_allocated = l->uid;

            // printing the TAship with proper numbering, EVENT 11
            switch (l->curr_times[i])
            {
            case 0:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 1:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dnd TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 2:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %drd TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 3:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            default:
              break;
            }
            l->availability[i] = false;
            l->curr_times[i]++;
            break;
          }
        }
      }
      pthread_mutex_unlock(&l->mutex);
      k++;
    }

    int allocTAs = t->ta_allocated;
    if (allocTAs >= 0 && t->tutorial == false)
    {
      bool checkTute = true;
      int maxSlots = t->course_max_slots;
      t->d = randomRange(1, maxSlots);
      t->tutorial = checkTute;
      t->tut_seats = t->d;
      // EVENT 7
      int seats = t->d;
      printf(COLOR_WHITE "Course %s has been allotted %d seats\n" COLOR_RESET, t->name, seats);
    }

    if (t->tutorial)
    {
      int w = 0;
      int x = 0;
      while (x < Students)
      {
        int UID = t->uid;
        if (students[x]->curr_pref == UID)
        {
          w++;
        }
        x++;
      }

      if (w != 0 && (w + t->d == t->tut_seats || t->d == 0))
      {
        // EVENT 8
        int totalSeats = t->tut_seats;
        int filledSeats = totalSeats - t->d;
        printf(COLOR_BRRED "Tutorial has started for %s with %d seats filled out of %d\n" COLOR_RESET, t->name, filledSeats, totalSeats);
        // sleeping for 2 seconds to mimic the tutorial duration
        sleep(2);

        // resettting values after tutorial finished
        int reset = 0;
        t->tutorial = false;
        t->d = reset;
        t->tut_seats = reset;

        // TA has left
        // EVENT 9
        printf(COLOR_BRGREEN "TA %d from lab %s has completed the tutorial and left the course %s\n" COLOR_RESET, t->ta_allocated, college_labs[t->lab_allocated]->name, t->name);
        int flag = -1;
        t->ta_allocated = flag;
        t->lab_allocated = flag;

        x = 0;
        while (x < Students)
        {
          int currentPref = students[x]->curr_pref;
          int currentAlloc = students[x]->curr_alloc;
          if (currentPref >= 0 && currentAlloc == t->uid)
          {
            double probability;
            Student *s = students[x];

            pthread_mutex_lock(&s->mutex);
            probability = s->callibre;
            probability *= t->interest;

            bool finalised = (rand() % 100) < (probability * 100);

            if (finalised)
            {
              // EVENT 5
              int UID = s->uid;
              printf(COLOR_MAGENTA "Student %d has selected %s permanently\n" COLOR_RESET, UID, t->name);
              s->curr_pref = flag;
            }
            else
            {
              // EVENT 3
              printf(COLOR_YELLOW "Student %d has withdrawn from course %s\n" COLOR_RESET, s->uid, t->name);

              if (s->curr_pref == s->pref[0])
              {
                s->curr_alloc = -1;
                s->curr_pref = s->pref[1];
                // EVENT 4
                printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" COLOR_RESET, s->uid, courses[s->pref[0]]->name, courses[s->pref[1]]->name);
              }
              else if (s->curr_pref == s->pref[1])
              {
                s->curr_alloc = -1;
                s->curr_pref = s->pref[2];
                // EVENT 4
                printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" COLOR_RESET, s->uid, courses[s->pref[1]]->name, courses[s->pref[2]]->name);
              }
              else
              {
                // EVENT 6
                int UID = s->uid;
                int flag = -1;
                printf(COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" COLOR_RESET, UID);
                s->curr_pref = flag;
              }
            }

            pthread_mutex_unlock(&s->mutex);
          }
          x++;
        }
      }
    }

    if (t->courseValid > 0)
    {
      t->courseValid = 0;

      int num_labs = t->p;
      int k = 0;

      while (k < num_labs)
      {
        if (college_labs[t->course_labs[k]]->ta_worthy != 2)
        {
          t->courseValid = 1;
        }
        k++;
      }
    }

    if (t->courseValid == 0)
    {
      int flag = -1;
      t->courseValid = flag;
      // EVENT 10
      printf(COLOR_BRYELLOW "Course %s doesn't have any TA's eligible and is removed from course offerings\n" COLOR_RESET, t->name);
    }

    pthread_mutex_unlock(&t->mutex);
  }

  return 0;
}

void createCourseThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&course_thread[i], 0, courseSim, courses[i]);
  }
}

void initCourse(int i)
{
  int UID = i;
  Course *t = (Course *)malloc(sizeof(Course));

  scanf("%s %lf %d %d", t->name, &t->interest, &t->course_max_slots, &t->p);

  int num_labs = t->p;
  for (int j = 0; j < num_labs; j++)
  {
    scanf("%d", &t->course_labs[j]);
  }

  int flag = -1;
  t->uid = UID;
  t->ta_allocated = flag;
  t->d = 0;
  t->tutorial = false;
  t->tut_seats = 0;
  t->lab_allocated = flag;
  t->courseValid = 1;

  pthread_mutex_init(&t->mutex, 0);

  courses[i] = t;
}