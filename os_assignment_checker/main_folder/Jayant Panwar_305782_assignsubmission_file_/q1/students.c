#include "headers.h"
#include "students.h"
#include "colors.h"
#include <stdbool.h>

void allocateSlot(Student *s, bool waiting)
{
  while (s->curr_pref != -1 && waiting)
  {
    int i = 0;
    while (i < Courses)
    {
      pthread_mutex_lock(&courses[i]->mutex);

      if (s->curr_alloc == -1 && waiting && courses[i]->d > 0)
      {
        if (courses[i]->ta_allocated >= 0 && courses[i]->uid == s->curr_pref)
        {
          pthread_mutex_lock(&s->mutex);
          int UID = courses[i]->uid;
          s->curr_alloc = UID;
          pthread_mutex_unlock(&s->mutex);

          // EVENT 2
          UID = s->uid;
          printf(COLOR_GREEN "Student %d has been allocated a seat in course %s\n" COLOR_RESET, s->uid, courses[i]->name);

          courses[i]->d--;
          UID = s->uid;

          pthread_mutex_unlock(&courses[i]->mutex);
          break;
        }
      }

      pthread_mutex_unlock(&courses[i]->mutex);
      i++;
    }

    if (s->curr_pref != -1 && courses[s->curr_pref]->courseValid <= 0)
    {
      int UID;
      UID = s->uid;
      // EVENT 3
      printf(COLOR_YELLOW "Student %d has withdrawn from course %s\n" COLOR_RESET, UID, courses[s->curr_pref]->name);
      pthread_mutex_lock(&s->mutex);
      if (s->curr_pref == s->pref[0])
      {
        s->curr_alloc = -1;
        s->curr_pref = s->pref[1];
        // EVENT 4
        printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" COLOR_RESET, UID, courses[s->pref[0]]->name, courses[s->pref[1]]->name);
      }
      else if (s->curr_pref == s->pref[1])
      {
        s->curr_alloc = -1;
        s->curr_pref = s->pref[2];
        // EVENT 4
        printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" COLOR_RESET, s->uid, courses[s->pref[1]]->name, courses[s->pref[2]]->name);
      }
      else
      {
        // EVENT 6
        printf(COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" COLOR_RESET, s->uid);
        int flag = -1;
        s->curr_pref = flag;
      }
      pthread_mutex_unlock(&s->mutex);
    }
  }

  return;
}

void *studentSim(void *a)
{
  int time;
  Student *s = (Student *)a;

  time = s->arr_time;
  sleep(time);

  // EVENT 1
  printf(COLOR_RED "Student %d has filled in preferences for course registration\n" COLOR_RESET, s->uid);

  allocateSlot(s, true);
  return 0;
}

void createStudentThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&student_thread[i], 0, studentSim, students[i]);
  }
}

void initStudent(int i)
{
  int UID = i;
  Student *s = (Student *)malloc(sizeof(Student));

  scanf("%lf %d %d %d %d", &s->callibre, &s->pref[0], &s->pref[1], &s->pref[2], &s->arr_time);

  s->curr_pref = s->pref[0];

  s->uid = UID;
  s->finalised = 0;
  s->curr_alloc = -1;

  pthread_mutex_init(&s->mutex, 0);

  students[i] = s;
}