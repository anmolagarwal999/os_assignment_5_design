#include "headers.h"
#include "students.h"
#include "labs.h"
#include "courses.h"
#include "init.h"

void initializeEntities()
{
  int x = 0;

  while (x < Courses)
  {
    initCourse(x);
    x++;
  }
  x = 0;

  while (x < Students)
  {
    initStudent(x);
    x++;
  }
  x = 0;

  while (x < Labs)
  {
    initLab(x);
    x++;
  }
}

void createThreads()
{
  int x = 0;

  while (x < Students)
  {
    createStudentThreads(x, 0);
    x++;
  }
  x = 0;

  while (x < Courses)
  {
    createCourseThreads(x, 0);
    x++;
  }
  x = 0;

  while (x < Labs)
  {
    createLabThreads(x, 0);
    x++;
  }
}

void initialization()
{
  srand(time(0));

  courses = (Course **)malloc(sizeof(Course *) * Courses);
  course_thread = (pthread_t *)malloc(sizeof(pthread_t) * Courses);

  students = (Student **)malloc(sizeof(Student *) * Students);
  student_thread = (pthread_t *)malloc(sizeof(pthread_t) * Students);

  college_labs = (Lab **)malloc(sizeof(Lab *) * Labs);
  lab_thread = (pthread_t *)malloc(sizeof(pthread_t) * Labs);

  initializeEntities();
  createThreads();
}