// Each person is a thread
// Each TA is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "student.c"
#include "course.c"

void main()
{
    printf(COLOR_YELLOW "Simulation has started!\n" COLOR_RESET);
    input();
    printf("------------------------------------------------------------------------\n");

    // Thread for each student
    thStudent = (pthread_t*)malloc(numStudents * sizeof(pthread_t));
    for(int i = 0; i < numStudents; i++)
    {
        pthread_create(&thStudent[i], NULL, studentFunction, &i);
        usleep(20);
    }

    sleep(1);

    // Thread for each course
    thCourse = (pthread_t*)malloc(numCourses * sizeof(pthread_t));
    for(int i = 0; i < numCourses; i++)
    {
        pthread_create(&thCourse[i], NULL, courseFunction, &i);
        usleep(20);
    }

    threadExit();
}
    

void threadExit()
{
    // Exit student threads
    for (int i = 0; i < numStudents; i++)
    {
        pthread_exit(thStudent[i]);
    }
    // Exit course threads
    for (int i = 0; i < numCourses; i++)
    {
        pthread_exit(thCourse[i]);
    }
    // Exit student threads
    for (int i = 0; i < numLabs; i++)
    {
        pthread_exit(thLab[i]);
    }
}