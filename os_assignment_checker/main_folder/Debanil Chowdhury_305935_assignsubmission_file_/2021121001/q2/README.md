# Assignment 5 (Concurrency and Networking)
## By **Debanil Chowdhury** (Roll No. **2021121001**)
<br>

* The entire code is written in main.c.
* Reasonable assumptions are made for things like length of current path, number of commands, command length, etc.
* The code is to be compiled with gcc flag -pthread i.e the command to compile - <br>
```
gcc main.c -pthread
```
* Then code can run with <br>
```
./a.out
```
