#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>

#define ERROR_COLOR "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

#define MAX 128
#define HOME 0
#define AWAY 1
#define NEUTRAL -1
#define GOAL_NO_CHANCE -1
#define GOAL_SUCCESS 1
#define GOAL_FAIL 0

typedef struct Zone Zone;
void *waitForPatienceToRunOut(void *arg);
void *waitForSeat(void *arg);
void *waitForMaxGoals(void *arg);

typedef struct Person
{
    int peopleID;   // index of person in global array of people
    int groupID;    // index of person's group in global groups array
    int personID;   // index of person in his group's people array
    char name[MAX]; // name of person
    int T;          // time of entry at match
    int fan_of;     // fan of home neutral or away team
    int P;          // patience
    int max_goals;  // number of goals to make person angry
    Zone* z;         // which zone person is in
} Person;

typedef struct Zone
{
    char zoneName;      // either H A or N
    Person people[MAX]; // array of people in zone
    int population;     // number of people currently in zone
    int capacity;       // capacity of zone
} Zone;

typedef struct Group
{
    int groupID;        // index of group in global groups array
    Person people[MAX]; // array of people in group
    int population;     // number of people in group
} Group;

typedef struct Team
{
    int which;  // number of team as defined in #define
    char *name; // name of team
    int score;  // score of team
} Team;

typedef struct TimeRunOutCheck
{
    int time;
    pthread_t checkerThread;
    Person checkerPerson;
    int returnvalue;        // return value for pthread_exit
    pthread_t checkseat[3]; // threads waiting for seat
} TimeRunOutCheck;          // Data Structure for argument to pass to pthread_create when waiting for time to run out

typedef struct SeatCheck
{
    Zone z;
    pthread_t checkerThread;
    Person checkerPerson;
    int returnvalue;    // return value for pthread_exit
    sem_t sem;          // for synchronisation
    pthread_t other[3]; // other wait for seat threads
    pthread_mutex_t *seatlock;
    pthread_t timecheckthread;
} SeatCheck; // Data Structure for argument to pass to pthread_create when waiting for seat

typedef struct GoalCheck
{
    pthread_t checkerThread;
    int score;
    int Team;
    //char name[MAX];
    char *name;
} GoalCheck; // Data Structure for argument to pass to pthread_create when waiting for goals

Team h = {HOME, "FC Messilona", 0}; // Home Team declaration
Team a = {AWAY, "Benzdrid CF", 0};  // Away Team declaration

Group groups[MAX];  // global array containing all groups
Person people[MAX]; // global array containing all people

int GOALS[2][MAX];   // GOAL[team][time] == success or failure
Zone A, H, N;        // Zones
int num_people = 0;  // total number of people
int num_groups;      // total number of groups
int X;               // Maximum patience
int noOfGoalChances; // Number of oppoertunities to score

int CLOCK = 0; // Timepassed since start of match
pthread_mutex_t clocklock;
pthread_cond_t clockcheck;
pthread_mutex_t points;
pthread_cond_t pointcheck;

void inputZoneCapacitiesXNumGroups()
{
    scanf(" %d %d %d %d %d", &H.capacity, &A.capacity, &N.capacity, &X, &num_groups);
}

Person inputPersonInfo(int id, int ID)
{
    Person p;
    p.groupID = ID;
    p.personID = id;
    p.peopleID = num_people;
    num_people++;
    people[p.peopleID] = p;
    p.name[0] = '\0';
    char C;
    do
    {
        C = getchar();
    } while (isspace(C) != 0);
    int i = 0;
    while (isspace(C) == 0)
    {
        p.name[i] = C;
        i++;
        p.name[i] = '\0';
        C = getchar();
    }
    do
    {
        C = getchar();
    } while (C != 'H' && C != 'A' && C != 'N');
    if (C == 'H')
    {
        p.fan_of = HOME;
    }
    else if (C == 'A')
    {
        p.fan_of = AWAY;
    }
    else if (C == 'N')
    {
        p.fan_of = NEUTRAL;
    }
    char *str;
    size_t size = 10;
    str = (char *)malloc(size);
    getline(&str, &size, stdin);
    sscanf(str, "%d %d %d", &p.T, &p.P, &p.max_goals);
    return p;
}

Group inputGroupInfo(int id)
{
    Group g;
    g.groupID = id;
    scanf(" %d", &(g.population));
    for (int i = 0; i < g.population; i++)
    {
        g.people[i] = inputPersonInfo(i, g.groupID);
    }
    return g;
}

void inputGoalScoringChances()
{
    char team;
    int time = 0;
    float probability;
    for (time; time < MAX; time++)
    {
        GOALS[HOME][time] = GOAL_NO_CHANCE;
        GOALS[AWAY][time] = GOAL_NO_CHANCE;
    }
    scanf(" %d", &noOfGoalChances);
    for (int i = 0; i < noOfGoalChances; i++)
    {
        char C;
        while (1)
        {
            C = getchar();
            if (C == 'H' || C == 'A')
            {
                team = C;
                break;
            }
        }
        char *str;
        size_t size = 10;
        str = (char *)malloc(size);
        getline(&str, &size, stdin);
        sscanf(str, "%d %f", &time, &probability);
        if (rand() <= probability * RAND_MAX)
        {
            if (team == 'H')
            {
                GOALS[HOME][time] = GOAL_SUCCESS;
            }
            if (team == 'A')
            {
                GOALS[AWAY][time] = GOAL_SUCCESS;
            }
        }
        else
        {
            if (team == 'H')
            {
                GOALS[HOME][time] = GOAL_FAIL;
            }
            if (team == 'A')
            {
                GOALS[AWAY][time] = GOAL_FAIL;
            }
        }
    }
}

void print_score(int j)
{
    if (GOALS[HOME][j] == GOAL_SUCCESS)
    {
        pthread_mutex_lock(&points);
        h.score++;
        printf("%s have scored their %dth goal.\n", h.name, h.score);
        pthread_mutex_unlock(&points);
        pthread_cond_broadcast(&pointcheck);
    }
    else if (GOALS[AWAY][j] == GOAL_SUCCESS)
    {
        pthread_mutex_lock(&points);
        a.score++;
        printf("%s have scored their %dth goal.\n", a.name, a.score);
        pthread_mutex_unlock(&points);
        pthread_cond_broadcast(&pointcheck);
    }
    else if (GOALS[HOME][j] == GOAL_FAIL)
    {
        printf("%s missed their chance to score their %dth goal.\n", h.name, h.score + 1);
    }
    else if (GOALS[AWAY][j] == GOAL_FAIL)
    {
        printf("%s missed their chance to score their %dth goal.\n", a.name, a.score + 1);
    }

    return;
}

void *score_thread(void *arg)
{
    while (1)
    {
        sleep(1);
        pthread_mutex_lock(&clocklock);
        CLOCK++;
        print_score(CLOCK);
        pthread_mutex_unlock(&clocklock);
        pthread_cond_broadcast(&clockcheck);
    }
    return NULL;
}

void *person_thread(void *arg)
{
    Person *P = (Person *)arg;
    sleep(P->T);
    // After this person arrives at stadium
    pthread_mutex_lock(&clocklock);
    while (CLOCK < P->T)
    {
        pthread_cond_wait(&clockcheck, &clocklock);
    }
    pthread_mutex_unlock(&clocklock);
    printf("%s has reached the stadium.\n", P->name);
    // Person starts waiting for seat
    sem_t maxcapacity;
    pthread_t waitforpatiencetorunout;
    SeatCheck seat;
    seat.checkerThread = (pthread_t) pthread_self;
    seat.checkerPerson = *P;
    seat.returnvalue = 0;
    seat.sem = maxcapacity;
    pthread_t waitforseat[3];
    pthread_mutex_t getseatlock;
    pthread_mutex_init(&getseatlock, NULL);
    TimeRunOutCheck time = {P->P, (pthread_t) pthread_self, *P, 0};
    for (int i = 0; i < 3; i++)
    {
        time.checkseat[i] = waitforseat[i];
    }
    pthread_create(&waitforpatiencetorunout, NULL, waitForPatienceToRunOut, &time);
    seat.seatlock = &getseatlock;
    seat.timecheckthread = waitforpatiencetorunout;
    for (int i = 0; i < 3; i++)
    {
        seat.other[i] = waitforseat[i];
    }
    switch (P->fan_of)
    {
    case HOME:
        sem_init(&maxcapacity, 0, H.capacity + N.capacity);
        pthread_create(&waitforseat[HOME + 1], NULL, waitForSeat, &seat);
        pthread_create(&waitforseat[NEUTRAL + 1], NULL, waitForSeat, &seat);
        break;
    case AWAY:
        sem_init(&maxcapacity, 0, A.capacity);
        pthread_create(&waitforseat[AWAY + 1], NULL, waitForSeat, &seat);
        break;
    case NEUTRAL:
        sem_init(&maxcapacity, 0, A.capacity + H.capacity + N.capacity);
        pthread_create(&waitforseat[HOME + 1], NULL, waitForSeat, &seat);
        pthread_create(&waitforseat[NEUTRAL + 1], NULL, waitForSeat, &seat);
        pthread_create(&waitforseat[AWAY + 1], NULL, waitForSeat, &seat);
        break;
    }

    // Now person will either wait for maximum time to run out or goals to be scored
    pthread_t waitformaxgoals;
    GoalCheck G = {(pthread_t) pthread_self, P->max_goals, P->fan_of, P->name};
    pthread_create(&waitformaxgoals, NULL, waitForMaxGoals, &G);

    sleep(X);
    pthread_kill(waitformaxgoals, SIGKILL);
    printf("%s watched the match for %d seconds and left.\n", P->name, X);
    printf("%s is waiting for his friends at the gate.\n", P->name);

    return NULL;
}

void *waitForMaxGoals(void *arg)
{
    GoalCheck *G = (GoalCheck *)arg;
    Team x;
    if (G->Team == HOME)
    {
        x = h;
    }
    if (G->Team == AWAY)
    {
        x = a;
    }

    pthread_mutex_lock(&points);
    while (x.score < G->score)
    {
        pthread_cond_wait(&pointcheck, &points);
    }
    pthread_mutex_unlock(&points);
    pthread_kill(G->checkerThread, SIGKILL);
    printf("%s is leaving due to bad performance of his team.\n", G->name);
    printf("%s is waiting for his friends at the gate.\n", G->name);
}

void *waitForSeat(void *arg)
{
    SeatCheck *seat = (SeatCheck *)arg;
    sem_wait(&(seat->sem));
    pthread_mutex_lock(seat->seatlock);
    seat->z.population++;
    seat->checkerPerson.z = &(seat->z);
    pthread_kill(seat->timecheckthread, SIGKILL);
    if (seat->z.zoneName == 'N')
    {
        pthread_kill(seat->other[AWAY + 1], SIGKILL);
        pthread_kill(seat->other[HOME + 1], SIGKILL);
    }
    else if (seat->z.zoneName == 'A')
    {
        pthread_kill(seat->other[HOME + 1], SIGKILL);
        pthread_kill(seat->other[NEUTRAL + 1], SIGKILL);
    }
    else if (seat->z.zoneName == 'H')
    {
        pthread_kill(seat->other[AWAY + 1], SIGKILL);
        pthread_kill(seat->other[NEUTRAL + 1], SIGKILL);
    }
    pthread_mutex_unlock(seat->seatlock);
    printf("%s got a seat in Zone %c.\n", seat->checkerPerson.name, seat->checkerPerson.z->zoneName);
}

void *waitForPatienceToRunOut(void *arg)
{
    TimeRunOutCheck *T = (TimeRunOutCheck *)arg;
    sleep(T->time);
    pthread_kill(T->checkseat[0], SIGKILL);
    pthread_kill(T->checkseat[1], SIGKILL);
    pthread_kill(T->checkseat[2], SIGKILL);
    printf("%s could not get a seat.\n", T->checkerPerson.name);
    pthread_kill(T->checkerThread, SIGKILL);
    pthread_exit(&(T->returnvalue));
}

void *group_thread(void *arg)
{
    Group *G = (Group *)arg;
    // start all person thread
    pthread_t all_person[G->population];
    for (int i = 0; i < G->population; i++)
    {
        pthread_create(&all_person[i], NULL, person_thread, &G->people[i]);
    }
    // wait for all person threads to finish
    for (int i = 0; i < G->population; i++)
    {
        pthread_join(all_person[i], NULL);
    }
    // group leaving for dinner
    printf("Group %d is leaving for dinner\n", G->groupID);

    return NULL;
}

int main()
{
    inputZoneCapacitiesXNumGroups();
    for (int i = 0; i < num_groups; i++)
    {
        groups[i] = inputGroupInfo(i);
    }
    inputGoalScoringChances();
    A.population = 0;
    H.population = 0;
    N.population = 0;

    pthread_t match;
    pthread_t all_groups[num_groups];
    pthread_mutex_init(&clocklock, NULL);
    pthread_mutex_init(&points, NULL);
    pthread_cond_init(&clockcheck, NULL);
    pthread_cond_init(&pointcheck, NULL);

    pthread_create(&match, NULL, score_thread, NULL);
    for (int i = 0; i < num_groups; i++)
    {
        pthread_create(&all_groups[i], NULL, group_thread, &groups[i]);
    }

    // pthread_join(match, NULL);
    for (int i = 0; i < num_groups; i++)
    {
        pthread_join(all_groups[i], NULL);
    }

    printf("EXIT SIMULATION\n");
    return 0;
}