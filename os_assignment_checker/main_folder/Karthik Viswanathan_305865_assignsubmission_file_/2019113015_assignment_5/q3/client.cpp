#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream>
#include <bits/stdc++.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

pthread_mutex_t send_lock = PTHREAD_MUTEX_INITIALIZER;

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl


typedef struct query
{
    int id;
    int time;
    string query;
    int socket_fd;
} query;
pthread_t *q_tid;
///////////////////////////////
#define SERVER_PORT 8005
////////////////////////////////////

const LL buff_sz = 1048576;
struct query q_argv[1000];
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void* begin_process(void *arg)
{
    int id = (uintptr_t) arg;
    sleep(q_argv[id].time);
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    string msg = q_argv[id].query;
    //cout<<"sent "<<msg<<endl;
    send_string_on_socket(socket_fd, msg);
    string output_msg;
    int num_bytes_read;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    pthread_mutex_lock(&send_lock);
    cout <<output_msg << endl;
    pthread_mutex_unlock(&send_lock);
    return NULL;
}

int main(int argc, char *argv[])
{

    int i, j, k, t, n;
    int queries;
    scanf("%d \n", &queries);
    for (int i = 0; i < queries; i++)
    {
        q_argv[i].id = i;
        cin >> q_argv[i].time;
        getline(cin, q_argv[i].query);
    }
    pthread_t *q_tid = (pthread_t *) malloc(sizeof(pthread_t) * queries);
    for (int i = 0; i < queries; i++)
    {
        if (pthread_create(&q_tid[i], NULL, &begin_process, (void *)i))
        {
            perror("Error initializing students");
            return -1;
        }
    }
    for (int i = 0; i < queries; i++) 
    {
        pthread_join(q_tid[i], NULL);
    }
    return 0;
}