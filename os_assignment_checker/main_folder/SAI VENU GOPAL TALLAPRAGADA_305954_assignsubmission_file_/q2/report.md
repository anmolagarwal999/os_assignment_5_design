You can simply run my code by using the command "gcc q2.c -lpthread"

I have implemented this code by considering the people/spectators as threads and I used semaphores to take care of the constraints that are there on spectators like waiting time for seat allocation and bad performance by their supporting team. 

I took care of the constraints by taking sem_timedwait as there is constraint based on both time and the outcome.

I have a time counter which is also a thread. This will take care the time elapsed in the simulation.

I have implemented the goal scoring by creating a probability function which will give the outcome based on the probability given. Now this is the second time where I used a semaphore. Here I will use sem_post on all the semaphores where there is a goal constraint. 

