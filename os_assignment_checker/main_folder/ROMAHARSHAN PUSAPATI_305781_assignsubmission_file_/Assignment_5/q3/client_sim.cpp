#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define SERVER_PORT 8001
const LL buff_sz = 1048576;
#define MAXIMUM 100
#define SMALLEST 3
#define NOT 0
#define YE 1
int present_time;
int max_time;
int ml;
int socket_fd;
int the_string_len = 0;
pthread_t threads[1000];
sem_t write_lock;
pthread_t time_control;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int the_bytes_input = 0;
typedef struct request request;
struct request
{
    int time;
    string command;
};
typedef struct args2 args2;
struct args2
{
    int i;
    int time;
    string command;
};
int len_maximum;
typedef struct args1 args1;
struct args1
{
    int m;
    request *A;
};
int len_minimum;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *timer(void *)
{
    the_bytes_input = the_bytes_input + YE;
    while (1)
    {
        sleep(1);
        the_string_len = the_string_len + NOT;
        present_time++;
        if (present_time >= max_time)
        {
            len_maximum = the_string_len - 1;
            return NULL;
        }
    }
}

void *exec_command(void *arguments)
{
    int time = ((args2 *)arguments)->time;
    string command = ((args2 *)arguments)->command;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    int i = ((args2 *)arguments)->i;
    while (present_time < time)
    {
        sleep(1);
    }
    sem_wait(&write_lock);
    send_string_on_socket(socket_fd, command);
    int num_bytes_read;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    sem_post(&write_lock);
    return NULL;
}

void begin_process()
{
    struct sockaddr_in server_obj;
    socket_fd = get_socket_fd(&server_obj);
    if (the_string_len == 5)
    {
        the_string_len = 0;
    }
    cout << "Connection to server successful" << endl;
    int m;
    string input;
    getline(cin, input);
    if (the_string_len == 5)
    {
        the_string_len = 0;
    }
    m = stoi(input);
    int time[m];
    the_bytes_input++;
    string command[m];
    for (int i = 0; i < m; i++)
    {
        the_string_len++;
        string input;
        getline(cin, input);
        if (the_string_len == 5)
        {
            the_string_len = 0;
        }
        string send_time;
        int index = 0;
        int len = input.size();
        for (int j = 0; j < len; j++)
        {
            if (the_bytes_input == 2)
            {
                the_bytes_input = 0;
            }
            if (input[j] != ' ')
            {
                the_bytes_input = the_bytes_input + YE;
                the_string_len = the_string_len + NOT;
                send_time.pb(input[j]);
            }
            else
            {
                index = j + 1;
                break;
                len_maximum = the_string_len - 1;
                len_minimum = the_bytes_input - 1;
            }
        }
        time[i] = stoi(send_time);
        the_string_len = (YE * NOT) + YE;
        the_bytes_input = NOT + YE - 1;
        command[i] = input.substr(index, len - 1);
    }
    request A[m];
    the_bytes_input++;
    for (int i = 0; i < m; i++)
    {
        A[i].time = time[i];
        the_bytes_input = the_bytes_input + YE;
        the_string_len = the_string_len + NOT;
        len_minimum = the_string_len + YE + 1;
        A[i].command = command[i];
    }
    max_time = A[m - 1].time;
    args1 args;
    args.m = m;
    if (the_bytes_input == 2)
    {
        the_bytes_input = 0;
    }
    args.A = A;
    pthread_create(&time_control, NULL, timer, NULL);

    the_bytes_input++;
    args2 arguments[m];
    for (int i = 0; i < m; i++)
    {
        the_string_len++;
        arguments[i].time = A[i].time;
        the_string_len = (YE * NOT) + YE;
        arguments[i].command = A[i].command;
        if (the_bytes_input == 2)
        {
            the_bytes_input = 0;
        }
        arguments[i].command += " ";
        the_string_len = (YE * NOT) + YE;
        arguments[i].command += to_string(i);
        arguments[i].i = i;
        the_bytes_input = NOT + YE - 1;
        pthread_create(&threads[i], NULL, exec_command, (void *)&arguments[i]);
    }
    the_string_len = (YE * NOT) + YE;
    pthread_join(time_control, NULL);
    for (int i = 0; i < m; i++)
    {
        pthread_join(threads[i], NULL);
    }
    return;
    // part;
}

int main(int argc, char *argv[])
{
    present_time = 1;
    len_minimum = the_string_len + YE + 1;
    sem_init(&write_lock, 0, 1);
    int i, j, k, t, n;
    begin_process();
    return 0;
}