# OSNW Assignment-5 QUESTION-3
- This question is implemented using C++.
- There are two codes for this programme which are for client and the server named as client_sim.cpp and server_prog.cpp respectively.
- To run the program first we have to compile server_prog.cpp using the command "g++ server_prog.cpp -lpthread". Then we have to run the executional file corresponding to server_prog.cpp and also input <number of worker threads in the thread pool>.
- After doing the above step then on another terminal and compile client_sim.cpp using the command "g++ client_sim.cpp -lpthread". Then we have to run the executional file corresponding to client_sim.cpp and also input as follow:
- The first line of input would be the total number of user requests throughout the simulation (m).
- The next ‘m’ lines contain description of the user requests in non-decreasing order of the first token <Time in sec after which the request to connect to the server is to be made> <cmd with appropriate arguments>