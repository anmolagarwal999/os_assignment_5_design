# OSNW Assignment-5 QUESTION-2 
## The Code can be simply executed after using the command "gcc q2.c -lpthread"
### After running the above command the executional file "a.out" is created and executing it we can enter into the programme.
#### INPUT FORMAT:

- The first line contains 3 integers describing the capacities of the 3 zones.
- The next line contains the value of SPECTATING TIME ie X.
- The next line contains the total number of groups i.e. ‘num_groups’.
- This is followed by a description for each of the groups.
- The 1st line for each group description will be the number of people within the group. Let this number be ‘k’.
- This is followed by ‘k’ lines with each line describing a person.
- Each person is described as follows: <Name of person> <Home/Away/neutral denoted by H/A/N> <Time at which he reaches the stadium> <Patience Time> <Number of goals which the opponent needs to score to make the person enraged>
- For neutral fans, the value of input given for <Number of goals which the opponent needs to score to make the person enraged> will be -1.
- Next line contains the NUMBER OF GOALSCORING CHANCES ‘G’ throughout the match.
- Next ‘G’ lines contains the description of each chance using 3 tokens:
- Desc: <Team with the chance ie H/A> <Time in sec elapsed since the beginning of the match after which the chance was created> <Probability that the chance actually results in a goal>
- You can assume that no 2 chances are created at the same instant i.e. the second token for each chance’s description would be distinct.
- You can assume that the chances are inputted in increasing order of times at which they were created.
- Please assume that the match starts the moment the simulation begins
- Also, probability would be a number between 0 and 1 (inclusive).
