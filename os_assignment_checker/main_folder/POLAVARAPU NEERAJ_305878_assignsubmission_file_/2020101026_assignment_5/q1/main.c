#include "headers.h"

pthread_mutex_t mutex1[numberofcourses_max];
pthread_mutex_t mutex[numberofcourses_max];
pthread_mutex_t queuesizelock[numberofcourses_max];

sem_t *slot[numberofstudents_max];
sem_t *tut[numberofstudents_max];
sem_t labcount[numberoflabs_max];
sem_t stud[numberofstudents_max];
sem_t courseslots[numberofcourses_max];

int main()
{
    scaninput();

    // initiated locks and variables
    for (int i = 0; i < numberofstudents; i++)
    {
        sem_init(&stud[i], 0, 0);
        slot[i] = (sem_t *)malloc(numberofcourses * sizeof(sem_t));
        tut[i] = (sem_t *)malloc(numberofcourses * sizeof(sem_t));
        for (int j = 0; j < numberofcourses; j++)
        {
            sem_init(&slot[i][j], 0, 0);
            sem_init(&tut[i][j], 0, 0);
        }
    }

    for (int i = 0; i < numberoflabs; i++)
    {
        sem_init(&labcount[i], 0, numberoftas[i]);
        labqueuesize[i] = numberoftas[i];
        for (int j = 0; j < numberoftas[i]; j++)
        {
            tacount[i][j] = 0;
        }
        for (int j = 0; j < numberoftas[i]; j++)
        {
            labpush(i, j);
        }
        accesslab[i] = 1;
        talab[i] = 0;
        labmax[i] = numberoftas[i] * limit[i];
    }

    for (int i = 0; i < numberofcourses; i++)
    {
        sem_init(&courseslots[i], 0, 0);
        pthread_mutex_init(&mutex[i], NULL);
        pthread_mutex_init(&mutex1[i], NULL);
        pthread_mutex_init(&queuesizelock[i], NULL);
        queuesize[i] = 0;
        coursedead[i] = 0;
        arr[i] = 0;
    }
    // initiated locks and variables

    srand(time(0));
    srand(time(0));

    //initialised and created thread for each student, course, lab
    pthread_t students[numberofstudents];
    pthread_t courses[numberofcourses];
    pthread_t labs[numberoflabs];
    for (long int i = 0; i < numberofstudents; i++)
    {
        int x = pthread_create(&students[i], NULL, student, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    for (long int i = 0; i < numberofcourses; i++)
    {
        int x = pthread_create(&courses[i], NULL, &course, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    for (long int i = 0; i < numberoflabs; i++)
    {
        int x = pthread_create(&labs[i], NULL, &lab, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }
    //initialised and cread thread for each student, course, lab done.

    //making the main function thread to run until all other threads close.
    for (int i = 0; i < numberofstudents; i++)
        pthread_join(students[i], NULL);

    /// destroying locks
    for (int i = 0; i < numberofcourses; i++)
    {
        pthread_mutex_destroy(&queuesizelock[i]);
        pthread_mutex_destroy(&mutex[i]);
        pthread_mutex_destroy(&mutex1[i]);
        sem_destroy(&courseslots[i]);
    }
    for (int i = 0; i < numberofstudents; i++)
    {
        sem_destroy(&stud[i]);
        for (int j = 0; j < numberofcourses; j++)
        {
            sem_destroy(&slot[i][j]);
            sem_destroy(&tut[i][j]);
        }
    }
    for (int i = 0; i < numberoflabs; i++)
        sem_destroy(&labcount[i]);
    /// destroying locks
}
