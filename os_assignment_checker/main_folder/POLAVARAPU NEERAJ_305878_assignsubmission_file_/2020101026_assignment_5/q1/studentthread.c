#include "headers.h"

void *student(void *arg)
{
    int id = (long int)arg;
    sleep(timepref[id]);
    printf(COLOR_GREEN "Student %d has filled in preferences for course registration\n" COLOR_RESET, id);
    for (int prefid = 0; prefid < 3; prefid++)
    {
        pthread_mutex_lock(&mutex[pref[id][prefid]]);
        push(pref[id][prefid], id);
        pthread_mutex_unlock(&mutex[pref[id][prefid]]);

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }
        else
        {
            sem_wait(&stud[id]);
        }

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }

        sem_wait(&slot[id][pref[id][prefid]]);
        printf(COLOR_BLUE "Student %d has been allocated a seat in course %s\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);

        sem_wait(&tut[id][pref[id][prefid]]);

        float prob = interest[id] * calibreofstudent[pref[id][prefid]];
        float random;
        random = (rand() % 100 + 1) / (float)100;
        if (prob >= random)
        {
            printf(COLOR_CYAN "Student %d has selected course %s permanently\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);
            return NULL;
        }
        else
        {
            if (prefid < 2)
            {
                printf(COLOR_MAGENTA "Student %d has withdrawn from the course %s\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]]);
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
            }
            else
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
            }
        }
    l1:;
    }
    return NULL;
}
