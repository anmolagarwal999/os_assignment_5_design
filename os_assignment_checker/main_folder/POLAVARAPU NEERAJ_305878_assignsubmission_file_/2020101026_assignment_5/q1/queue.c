#include "headers.h"

void push(int id, int value)
{
    struct queue *temp;
    temp = (struct queue *)malloc(sizeof(struct queue));

    if (temp != NULL)
    {
        temp->data = value;
        if (front[id] == NULL)
        {
            front[id] = temp;
            rear[id] = temp;
            front[id]->next = NULL;
            rear[id]->next = NULL;
        }
        else
        {
            rear[id]->next = temp;
            rear[id] = temp;
            rear[id]->next = NULL;
        }

        queuesize[id]++;
        if (queuesize[id] == 1 && arr[id] == 1)
        {
            sem_post(&courseslots[id]);
        }
    }
}

int pop(int id)
{
    if (front[id] != NULL)
    {
        int x = front[id]->data;
        front[id] = front[id]->next;
        if (front[id] == NULL)
        {
            rear[id] = NULL;
        }
        queuesize[id]--;
        return x;
    }
    else
        return -20;
}

void display(int id)
{
    struct queue *ptr;
    ptr = front[id];
    printf(COLOR_BLUE "ID-%d = " COLOR_RESET, id);
    if (front[id] == NULL)
    {
        printf(COLOR_BLUE "Empty queue\n" COLOR_RESET);
    }
    else
    {
        while (ptr != NULL)
        {
            printf(COLOR_BLUE "%d" COLOR_RESET, ptr->data);
            ptr = ptr->next;
            if (ptr != NULL)
                printf("-");
        }
        printf("\n");
    }
}

void labpush(int id, int value)
{
    struct queue *temp;
    temp = (struct queue *)malloc(sizeof(struct queue));

    if (temp != NULL)
    {
        temp->data = value;
        if (labfront[id] == NULL)
        {
            labfront[id] = temp;
            labrear[id] = temp;
            labfront[id]->next = NULL;
            labrear[id]->next = NULL;
        }
        else
        {
            labrear[id]->next = temp;
            labrear[id] = temp;
            labrear[id]->next = NULL;
        }
    }
}

int labpop(int id)
{
l1:;
    if (labfront[id] != NULL)
    {
        int x = labfront[id]->data;
        labfront[id] = labfront[id]->next;
        if (labfront[id] == NULL)
        {
            labrear[id] = NULL;
        }
        labqueuesize[id]--;
        return x;
    }
    return -20;
}
