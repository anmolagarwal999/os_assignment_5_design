#include "headers.h"

void *course(void *arg)
{
    int itlabs = 0;
    int id = (long int)arg;

    while (1)
    {
        if (queuesize[id] == 0)
        {
            arr[id] = 1;
            sem_wait(&courseslots[id]);
            arr[id] = 0;
        }
    l1:;

        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
        {
            itlabs++;
            if (itlabs < nooflabs[id])
                goto l1;
        }

        pthread_mutex_lock(&queuesizelock[id]);
        int filled;
        int slots = rand() % maxslots[id] + 1;
        if (slots <= queuesize[id])
            filled = slots;
        else
            filled = queuesize[id];
        pthread_mutex_unlock(&queuesizelock[id]);

        if (itlabs >= nooflabs[id])
        {
            coursedead[id] = 1;
            struct queue *temp = front[id];
            while (temp != NULL)
            {
                int y = temp->data;
                sem_post(&stud[y]);
                temp = temp->next;
            }
            printf(COLOR_BLUE "Course %s does not have any TA mentors eligible and is removed from course offerings\n" COLOR_RESET, nameofcourse[id]);
            return NULL;
        }

        struct queue *temp = front[id];
        for (int j = 0; j < filled; j++)
        {
            int y = temp->data;
            sem_post(&stud[y]);
            temp = temp->next;
        }

        pthread_mutex_lock(&mutex1[listoflabs[id][itlabs]]);
        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
            goto l1;
        sem_wait(&labcount[listoflabs[id][itlabs]]);
        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
            goto l1;
        int x = labpop(listoflabs[id][itlabs]);
        tacount[listoflabs[id][itlabs]][x]++;
        talab[listoflabs[id][itlabs]]++;
        pthread_mutex_unlock(&mutex1[listoflabs[id][itlabs]]);

        printf(COLOR_RED "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" COLOR_RED, x, labname[listoflabs[id][itlabs]], nameofcourse[id], tacount[listoflabs[id][itlabs]][x]);

        printf(COLOR_MAGENTA "Course %s has been allocated %d seats\n" COLOR_RESET, nameofcourse[id], slots);

        int it = 0;
        int arrdone[10000];
        for (int j = 0; j < filled; j++)
        {
            pthread_mutex_lock(&mutex[id]);
            int x = pop(id);
            pthread_mutex_unlock(&mutex[id]);
            arrdone[it] = x;
            it++;
            sem_post(&slot[x][id]);
        }

        printf(COLOR_CYAN "Tutorial has started for Course %s with %d seats filled out of %d\n" COLOR_YELLOW, nameofcourse[id], filled, slots);

        sleep(2);

        for (int j = 0; j < filled; j++)
            sem_post(&tut[arrdone[j]][id]);

        printf(COLOR_YELLOW "TA %d from lab %s has completed the tutorial for course %s\n" COLOR_RESET, x, labname[listoflabs[id][itlabs]], nameofcourse[id]);
        if (tacount[listoflabs[id][itlabs]][x] < limit[listoflabs[id][itlabs]])
        {
            pthread_mutex_lock(&mutex1[listoflabs[id][itlabs]]);
            labpush(listoflabs[id][itlabs], x);
            sem_post(&labcount[listoflabs[id][itlabs]]);
            pthread_mutex_unlock(&mutex1[listoflabs[id][itlabs]]);
        }
    }
    return NULL;
}
