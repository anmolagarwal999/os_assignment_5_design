#include "headers.h"

void scaninput()
{
    scanf("%d %d %d", &numberofstudents, &numberoflabs, &numberofcourses);
    for (int i = 0; i < numberofcourses; i++)
    {
        scanf("%s", nameofcourse[i]);
        scanf("%f", &interest[i]);
        scanf("%d", &maxslots[i]);
        scanf("%d", &nooflabs[i]);
        for (int j = 0; j < nooflabs[i]; j++)
        {
            scanf("%d", &listoflabs[i][j]);
        }
    }
    for (int i = 0; i < numberofstudents; i++)
    {
        scanf("%f", &calibreofstudent[i]);
        for (int j = 0; j < 3; j++)
            scanf("%d", &pref[i][j]);
        scanf("%d", &timepref[i]);
    }

    for (int i = 0; i < numberoflabs; i++)
    {
        scanf("%s", labname[i]);
        scanf("%d", &numberoftas[i]);
        scanf("%d", &limit[i]);
    }

    // numberofstudents = 10;
    // numberoflabs = 3;
    // numberofcourses = 4;
    // strcpy(nameofcourse[0], "SMAI");
    // strcpy(nameofcourse[1], "NLP");
    // strcpy(nameofcourse[2], "CV");
    // strcpy(nameofcourse[3], "DSA");
    // interest[0] = 0.8;
    // interest[1] = 0.95;
    // interest[2] = 0.9;
    // interest[3] = 0.75;
    // maxslots[0] = 3;
    // maxslots[1] = 4;
    // maxslots[2] = 2;
    // maxslots[3] = 5;
    // nooflabs[0] = 2;
    // nooflabs[1] = 1;
    // nooflabs[2] = 2;
    // nooflabs[3] = 3;
    // listoflabs[0][0] = 0;
    // listoflabs[0][1] = 2;
    // listoflabs[1][0] = 0;
    // listoflabs[2][0] = 1;
    // listoflabs[2][1] = 2;
    // listoflabs[3][0] = 0;
    // listoflabs[3][1] = 1;
    // listoflabs[3][2] = 2;
    // calibreofstudent[0] = 0.8;
    // calibreofstudent[1] = 0.6;
    // calibreofstudent[2] = 0.85;
    // calibreofstudent[3] = 0.5;
    // calibreofstudent[4] = 0.75;
    // calibreofstudent[5] = 0.95;
    // calibreofstudent[6] = 0.4;
    // calibreofstudent[7] = 0.1;
    // calibreofstudent[8] = 0.85;
    // calibreofstudent[9] = 0.3;
    // pref[0][0] = 0;
    // pref[1][0] = 3;
    // pref[2][0] = 2;
    // pref[3][0] = 1;
    // pref[4][0] = 0;
    // pref[5][0] = 1;
    // pref[6][0] = 3;
    // pref[7][0] = 0;
    // pref[8][0] = 1;
    // pref[9][0] = 0;

    // pref[0][1] = 3;
    // pref[1][1] = 1;
    // pref[2][1] = 1;
    // pref[3][1] = 2;
    // pref[4][1] = 2;
    // pref[5][1] = 0;
    // pref[6][1] = 0;
    // pref[7][1] = 3;
    // pref[8][1] = 0;
    // pref[9][1] = 1;

    // pref[0][2] = 1;
    // pref[1][2] = 2;
    // pref[2][2] = 0;
    // pref[3][2] = 3;
    // pref[4][2] = 1;
    // pref[5][2] = 2;
    // pref[6][2] = 2;
    // pref[7][2] = 1;
    // pref[8][2] = 3;
    // pref[9][2] = 2;

    // timepref[0] = 1;
    // timepref[1] = 3;
    // timepref[2] = 1;
    // timepref[3] = 2;
    // timepref[4] = 3;
    // timepref[5] = 2;
    // timepref[6] = 3;
    // timepref[7] = 2;
    // timepref[8] = 1;
    // timepref[9] = 1;

    // strcpy(labname[0], "PRECOG");
    // strcpy(labname[1], "CVIT");
    // strcpy(labname[2], "RRC");

    // numberoftas[0] = 3;
    // numberoftas[1] = 4;
    // numberoftas[2] = 1;

    // limit[0] = 1;
    // limit[1] = 2;
    // limit[2] = 3;
}