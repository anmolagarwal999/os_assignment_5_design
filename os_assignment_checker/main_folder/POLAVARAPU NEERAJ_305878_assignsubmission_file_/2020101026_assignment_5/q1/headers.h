#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_RESET "\x1b[0m"

#define numberofcourses_max 10000
#define numberofstudents_max 10000
#define numberoflabs_max 10000
#define numberoftas_max 10000

struct queue
{
    int data;
    struct queue *next;
};

struct queue *front[numberofcourses_max];
struct queue *rear[numberofcourses_max];

struct queue *labfront[numberofcourses_max];
struct queue *labrear[numberofcourses_max];

long int queuesize[numberofcourses_max];
int labqueuesize[numberoflabs_max];
int tacount[numberoflabs_max][numberoftas_max];
int numberofstudents;
int numberoflabs;
int numberofcourses;

char nameofcourse[numberofcourses_max][1000];
float interest[numberofcourses_max];
int maxslots[numberofcourses_max];
int nooflabs[numberofcourses_max];
int listoflabs[numberofcourses_max][1000];

float calibreofstudent[numberofstudents_max];
int pref[numberofstudents_max][3];
int timepref[numberofstudents_max];

char labname[numberoflabs_max][1000];
int numberoftas[numberoflabs_max];
int limit[numberoflabs_max];

void *student(void *arg);
void *course(void *arg);
void *lab(void *arg);
void scaninput();
int pop(int id);
void push(int id, int value);
void display(int id);

int statusoflab[numberoflabs_max];
int labqueue[numberoflabs_max];
int labpop(int id);
void labpush(int id, int value);

int accesslab[numberoflabs_max];
int coursedead[numberofcourses_max];

pthread_mutex_t mutex[numberofcourses_max];
pthread_mutex_t mutex1[numberofcourses_max];
pthread_mutex_t queuesizelock[numberofcourses_max];

sem_t *slot[numberofstudents_max];
sem_t *tut[numberofstudents_max];
sem_t labcount[numberoflabs_max];
sem_t stud[numberofstudents_max];
sem_t courseslots[numberofcourses_max];

int arr[numberofcourses_max];
int talab[numberoflabs_max];
int labmax[numberoflabs_max];