#include "headers.h"

void scaninput()
{
    scanf("%d", &numberof_h);
    scanf("%d", &numberof_a);
    scanf("%d", &numberof_n);
    scanf("%d", &spectime);
    scanf("%d", &numberofgroups);

    int z = 0;
    for (int i = 0; i < numberofgroups; i++)
    {
        int x;
        scanf("%d", &x);
        groupsize[i] = x;
        for (int j = 0; j < x; j++)
        {
            scanf("%s %c", nameofperson[z], &teamofperson[z]);
            //scanf("%d", &jointime[z]);
            // scanf("%c", &teamofperson[z]);
            scanf("%d", &jointime[z]);
            scanf("%d", &patiencetime[z]);
            scanf("%d", &irrigoals[z]);
            if (teamofperson[z] == 'A')
            {
                arr_a[n_a] = z;
                n_a++;
            }
            if (teamofperson[z] == 'H')
            {
                arr_h[n_h] = z;
                n_h++;
            }
            groupid[z] = i;
            z++;
        }
    }
    numberofpersons = z;
    scanf("%d", &chances);
    for (int i = 0; i < chances; i++)
    {
        scanf(" %c", &chanceteam[i]);
        scanf("%d", &chancetime[i]);
        scanf("%f", &chanceprob[i]);
    }

    // numberof_h = 2;
    // numberof_a = 1;
    // numberof_n = 2;
    // spectime = 3;
    // strcpy(nameofperson[0], "Vaibhav");
    // teamofperson[0] = 'N';
    // jointime[0] = 3;
    // patiencetime[0] = 2;
    // irrigoals[0] = -1;

    // strcpy(nameofperson[1], "Sarthak");
    // teamofperson[1] = 'H';
    // jointime[1] = 1;
    // patiencetime[1] = 3;
    // irrigoals[1] = 2;

    // strcpy(nameofperson[2], "Ayush");
    // teamofperson[2] = 'A';
    // jointime[2] = 2;
    // patiencetime[2] = 1;
    // irrigoals[2] = 4;

    // strcpy(nameofperson[3], "Rachit");
    // teamofperson[3] = 'H';
    // jointime[3] = 1;
    // patiencetime[3] = 2;
    // irrigoals[3] = 4;

    // strcpy(nameofperson[4], "Roshan");
    // teamofperson[4] = 'N';
    // jointime[4] = 2;
    // patiencetime[4] = 1;
    // irrigoals[4] = -1;

    // strcpy(nameofperson[5], "Adarsh");
    // teamofperson[5] = 'A';
    // jointime[5] = 1;
    // patiencetime[5] = 2;
    // irrigoals[5] = 1;

    // strcpy(nameofperson[6], "Pranav");
    // teamofperson[6] = 'N';
    // jointime[6] = 3;
    // patiencetime[6] = 1;
    // irrigoals[6] = -1;

    // chances = 5;
    // chanceteam[0] = 'H';
    // chancetime[0] = 1;
    // chanceprob[0] = 1;

    // chanceteam[1] = 'A';
    // chancetime[1] = 2;
    // chanceprob[1] = 0.95;

    // chanceteam[2] = 'A';
    // chancetime[2] = 3;
    // chanceprob[2] = 0.5;

    // chanceteam[3] = 'H';
    // chancetime[3] = 5;
    // chanceprob[3] = 0.85;

    // chanceteam[4] = 'H';
    // chancetime[4] = 6;
    // chanceprob[4] = 0.4;

    // numberofpersons = 7;

    // n_a = 2;
    // n_h = 2;
    // arr_a[0] = 2;
    // arr_a[1] = 5;
    // arr_h[0] = 1;
    // arr_h[1] = 3;
    // groupsize[0] = 3;
    // groupsize[1] = 4;

    // groupid[0] = 0;
    // groupid[1] = 0;
    // groupid[2] = 0;
    // groupid[3] = 1;
    // groupid[4] = 1;
    // groupid[5] = 1;
    // groupid[6] = 1;
}

/*
2 1 2
3
2
3
Vibhav N 3 2 -1
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4
*/