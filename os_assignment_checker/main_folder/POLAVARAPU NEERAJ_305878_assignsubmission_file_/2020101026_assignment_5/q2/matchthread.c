#include "headers.h"

void *match(void *arg)
{
    int time = 0;
    for (int i = 0; i < chances; i++)
    {
        int t = chancetime[i] - time;
        sleep(t);
        float random = (rand() % 100 + 1) / (float)100;
        if (chanceprob[i] > random)
        {
            if (chanceteam[i] == 'H')
            {
                goalscount_h++;
                printf("Team H has scored their %dth goal\n", goalscount_h);
                for (int j = 0; j < n_a; j++)
                {
                    if (irrigoals[arr_a[j]] == goalscount_h)
                    {
                        sem_post(&goallock[arr_a[j]]);
                    }
                }
            }
            if (chanceteam[i] == 'A')
            {
                goalscount_a++;
                printf("Team A has scored their %dth goal\n", goalscount_a);
                for (int j = 0; j < n_h; j++)
                {
                    if (irrigoals[arr_h[j]] == goalscount_a)
                    {
                        sem_post(&goallock[arr_h[j]]);
                    }
                }
            }
        }
        else
        {
            if (chanceteam[i] == 'H')
            {
                printf("Team H missed the chance to score their %dth goal\n", goalscount_h + 1);
            }
            if (chanceteam[i] == 'A')
            {
                printf("Team A missed the chance to score their %dth goal\n", goalscount_a + 1);
            }
        }

        time = chancetime[i];
    }
    return NULL;
}
