#include "headers.h"

void *person(void *arg)
{
    int id = (long int)arg;
    sleep(jointime[id]);

    printf(COLOR_RED "%s has reached the stadium\n" COLOR_RESET, nameofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += patiencetime[id];
    int x;
    if (teamofperson[id] == 'A')
    {
        x = sem_timedwait(&mutex_a, &tm[id]);
    }
    if (teamofperson[id] == 'H')
    {
        x = sem_timedwait(&mutex_h, &tm[id]);
    }
    if (teamofperson[id] == 'N')
    {
        x = sem_timedwait(&mutex_n, &tm[id]);
    }

    if (x == -1 && errno == ETIMEDOUT)
    {
        printf(COLOR_YELLOW "%s could not get a seat\n" COLOR_RESET, nameofperson[id]);
        goto l1;
    }

    printf(COLOR_MAGENTA "%s has got a seat in zone %c\n" COLOR_RESET, nameofperson[id], teamofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += spectime;

    if (teamofperson[id] == 'H' || teamofperson[id] == 'A')
    {
        int y = sem_timedwait(&goallock[id], &tm[id]);
        if (y == -1 && errno == ETIMEDOUT)
        {
            printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
            goto l2;
        }
        else if (y == 0)
        {
            printf(COLOR_MAGENTA "%s is leaving due to bad performance of his team\n" COLOR_RESET, nameofperson[id]);
        }
        sem_post(&goallock[id]);
    }
    else
    {
        sleep(spectime);
        printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
    }
l2:;
    if (teamofperson[id] == 'A')
        sem_post(&mutex_a);
    if (teamofperson[id] == 'H')
        sem_post(&mutex_h);
    if (teamofperson[id] == 'N')
        sem_post(&mutex_n);
l1:;

    // pthread_mutex_lock(&mutexgrouplock[groupid[id]]);
    // groupcount[groupid[id]]++;
    // if (groupcount[groupid[id]] == groupsize[groupid[id]])
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     cond[groupid[id]] = 1;
    //     pthread_cond_signal(&c[groupid[id]]);
    // }
    // else
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     while (cond[groupid[id]] == 0)
    //         pthread_cond_wait(&c[groupid[id]], &mutexgrouplock[groupid[id]]);
    // }

    printf(COLOR_BLUE "%s is leaving for dinner\n" COLOR_RESET, nameofperson[id]);
    return NULL;
}
