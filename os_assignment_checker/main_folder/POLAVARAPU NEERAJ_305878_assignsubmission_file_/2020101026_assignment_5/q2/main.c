#include "headers.h"

pthread_mutex_t mutexgrouplock[10000];
pthread_cond_t c[10000];

int main()
{
    n_a = 0;
    n_h = 0;
    srand(time(0));
    scaninput();
    goalscount_a = 0;
    goalscount_h = 0;
    for (int i = 0; i < numberofgroups; i++)
    {
        groupcount[i] = 0;
    }
    for (int i = 0; i < numberofgroups; i++)
    {
        pthread_mutex_init(&mutexgrouplock[i], NULL);
        pthread_cond_init(&c[i], NULL);
        cond[i] = 0;
    }

    sem_init(&mutex_a, 0, numberof_a);
    sem_init(&mutex_h, 0, numberof_h);
    sem_init(&mutex_n, 0, numberof_n);
    for (int i = 0; i < numberofpersons; i++)
    {
        sem_init(&goallock[i], 0, 0);
    }
    pthread_t persons[numberofpersons];
    pthread_t matches;

    for (long int i = 0; i < numberofpersons; i++)
    {
        int x = pthread_create(&persons[i], NULL, person, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    int y = pthread_create(&matches, NULL, match, NULL);
    if (y != 0)
        printf("Thread can't be created : [%s]\n", strerror(y));

    for (int i = 0; i < numberofpersons; i++)
    {
        pthread_join(persons[i], NULL);
    }

    for (int i = 0; i < numberofpersons; i++)
    {
        sem_destroy(&goallock[i]);
    }
    sem_destroy(&mutex_a);
    sem_destroy(&mutex_h);
    sem_destroy(&mutex_n);

    for (int i = 0; i < numberofgroups; i++)
    {
        pthread_mutex_destroy(&mutexgrouplock[i]);
        pthread_cond_destroy(&c[i]);
    }
}
