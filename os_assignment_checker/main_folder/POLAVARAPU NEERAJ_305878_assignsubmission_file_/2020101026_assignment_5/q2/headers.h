#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#define numberofpersons_max 10000

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_RESET "\x1b[0m"

#define numberofcourses_max 10000
#define numberofstudents_max 10000
#define numberoflabs_max 10000
#define numberoftas_max 10000

struct queue
{
    int data;
    struct queue *next;
};

struct queue *front[numberofcourses_max];
struct queue *rear[numberofcourses_max];

int numberof_h;
int numberof_a;
int numberof_n;

int spectime;

char nameofperson[numberofpersons_max][10000];
char teamofperson[numberofpersons_max];
int jointime[numberofpersons_max];
int patiencetime[numberofpersons_max];
int irrigoals[numberofpersons_max];

int chances;
int numberofpersons;

char chanceteam[10000];
int chancetime[10000];
float chanceprob[10000];

sem_t mutex_a;
sem_t mutex_h;
sem_t mutex_n;
sem_t goallock[1000];

void scaninput();
void *person(void *arg);

struct timespec tm[1000];
void *match(void *arg);

int goalscount_a;
int goalscount_h;

int arr_a[10000];
int arr_h[10000];

int n_a;
int n_h;

int groupsize[10000];
int groupid[10000];
int numberofgroups;

int groupcount[10000];

pthread_mutex_t mutexgrouplock[10000];
pthread_cond_t c[10000];

int cond[10000];
