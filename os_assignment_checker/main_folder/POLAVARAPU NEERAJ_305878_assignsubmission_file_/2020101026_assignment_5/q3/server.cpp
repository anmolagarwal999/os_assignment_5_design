#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr

char dictionary[1000][10000];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

char *editor(char *command, int a, int b, char *value)
{
    char *message;
    message = (char *)malloc(sizeof(char) * 1000);
    if (strcmp(command, "insert") == 0)
    {
        if (strcmp(dictionary[a], "") != 0)
            strcpy(message, "Key already exists");
        else
        {
            strcpy(dictionary[a], value);
            strcpy(message, "Insertion successful");
            return message;
        }
        return message;
    }
    if (strcmp(command, "delete") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "No such key exists");
        else
        {
            strcpy(dictionary[a], "");
            strcpy(message, "Deletion successful");
            return message;
        }
        return message;
    }
    if (strcmp(command, "update") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "Key does not exist");
        else
        {
            strcpy(dictionary[a], value);
            strcpy(message, value);
            return message;
        }
        return message;
    }
    if (strcmp(command, "concat") == 0)
    {
        if (strcmp(dictionary[a], "") == 0 || strcmp(dictionary[b], "") == 0)
            strcpy(message, "Concat failed as at least one of the keys does not exist");
        else
        {
            char temp[1000];
            strcpy(temp, dictionary[a]);
            strcat(dictionary[a], dictionary[b]);
            strcat(dictionary[b], temp);
            strcpy(message, dictionary[b]);
            return message;
        }
        return message;
    }
    if (strcmp(command, "fetch") == 0)
    {
        if (strcmp(dictionary[a], "") == 0)
            strcpy(message, "Key does not exist");
        else
        {
            strcpy(message, dictionary[a]);
            return message;
        }
        return message;
    }
    return message;
}

void func(int sockfd, int id)
{
    char buff[MAX];
    bzero(buff, MAX);
    read(sockfd, buff, sizeof(buff));
    char command[1000];
    int a, b;
    char value[1000];

    char *token = strtok(buff, " ");
    strcpy(command, token);
    int y = strcmp(command, "insert");
    if (strcmp(command, "insert") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        strcpy(value, token);
        b = -20;
    }
    if (strcmp(command, "update") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        strcpy(value, token);
        b = -20;
    }
    if (strcmp(command, "delete") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        b = -20;
    }
    if (strcmp(command, "concat") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        token = strtok(NULL, " ");
        b = atoi(token);
    }
    if (strcmp(command, "fetch") == 0)
    {
        token = strtok(NULL, " ");
        a = atoi(token);
        b = -20;
    }

    pthread_mutex_lock(&mutex);
    char message[1000];
    strcpy(message, editor(command, a, b, value));
    bzero(buff, MAX);
    sprintf(buff, "%d:%s", id, message);
    write(sockfd, buff, sizeof(buff));
    pthread_mutex_unlock(&mutex);
}

int sockfd, connfd;
socklen_t len;
struct sockaddr_in servaddr, cli;

void connecttoserver()
{
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    //defining
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
    //defining done
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    if ((listen(sockfd, 100)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
}

void *serverthread(void *arg)
{
    int id = (long int)arg;
    while (1)
    {
        len = sizeof(cli);
        connfd = accept(sockfd, (SA *)&cli, &len); //accept()
        func(connfd, id);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&mutex, NULL);
    if (argc != 2)
    {
        printf("NOT THE CORRECT ARGUMENTS\n");
        exit(0);
    }
    connecttoserver();
    int n = atoi(argv[1]);
    pthread_t servers[n];
    for (long int i = 0; i < n; i++)
    {
        pthread_create(&servers[i], NULL, serverthread, (void *)i);
    }
    for (int i = 0; i < n; i++)
    {
        pthread_join(servers[i], NULL);
    }
    close(sockfd);
    pthread_mutex_destroy(&mutex);
}
