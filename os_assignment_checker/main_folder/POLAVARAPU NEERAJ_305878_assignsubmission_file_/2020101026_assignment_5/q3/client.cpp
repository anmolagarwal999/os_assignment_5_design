#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr

int clienttime[10000];
char clientcommand[10000][10000];
int m;

void func(int sockfd, int id)
{
	char buff[MAX];
	bzero(buff, sizeof(buff));
	strcpy(buff, clientcommand[id]);
	write(sockfd, buff, sizeof(buff));
	bzero(buff, sizeof(buff));
	read(sockfd, buff, MAX);
	printf("%d:%s\n", id, buff);
}

int connecttoserver(int id)
{
	int sockfd;
	struct sockaddr_in servaddr, cli;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		printf("socket creation failed...\n");
		exit(0);
	}

	//defining
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);
	//defining done
	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
	{
		printf("connection with the server failed...\n");
		exit(0);
	}

	return sockfd;
}

void *clientthread(void *arg)
{
	int id = (long int)arg;
	sleep(clienttime[id]);
	int sockfd = connecttoserver(id);
	func(sockfd, id);
	close(sockfd);
	return NULL;
}

void scaninput()
{
	scanf("%d", &m);
	for (int i = 0; i < m; i++)
	{
		scanf("%d ", &clienttime[i]);
		scanf("%[^\n]", clientcommand[i]);
	}
	// m = 11;
	// clienttime[0] = 1;
	// strcpy(clientcommand[0], "insert 1 hello");
	// clienttime[1] = 2;
	// strcpy(clientcommand[1], "insert 1 hello");
	// clienttime[2] = 2;
	// strcpy(clientcommand[2], "insert 2 yes");
	// clienttime[3] = 2;
	// strcpy(clientcommand[3], "insert 3 no");
	// clienttime[4] = 3;
	// strcpy(clientcommand[4], "concat 1 2");
	// clienttime[5] = 3;
	// strcpy(clientcommand[5], "concat 1 3");
	// clienttime[6] = 4;
	// strcpy(clientcommand[6], "delete 3");
	// clienttime[7] = 5;
	// strcpy(clientcommand[7], "delete 4");
	// clienttime[8] = 6;
	// strcpy(clientcommand[8], "concat 1 4");
	// clienttime[9] = 7;
	// strcpy(clientcommand[9], "update 1 final");
	// clienttime[10] = 8;
	// strcpy(clientcommand[10], "concat 1 2");
}

int main()
{
	scaninput();
	pthread_t clients[m];
	for (long int i = 0; i < m; i++)
	{
		pthread_create(&clients[i], NULL, clientthread, (void *)i);
	}
	for (int i = 0; i < m; i++)
	{
		pthread_join(clients[i], NULL);
	}
}
