//making client side for socket programming
#include <bits/stdc++.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include "client.hpp"
using namespace std;
#define MAX_REQUEST_LENGTH 1024
#define PORT 8080


user_request* input_request;
vector <string> parse(string str) // parses the string and stores it in a vector
{
    vector <string> result;
    string word = "";
    for (auto x : str) 
    {
        if (x == ' ')
        {
            result.push_back(word);
            word = "";
        }
        else {
            word = word + x;
        }
    }
    result.push_back(word);
    return result;
}

void* client_thread_sim(void* arg)
{
    int* index= (int*)arg;
    sleep(input_request[*index].request_time);
    int sockfd;
    sockaddr_in servaddr;
    //create socket and verify it's connection
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    //set servaddr attributes
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        exit(EXIT_FAILURE);
    }
    //connect client to server
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("connection with server failed");
        exit(EXIT_FAILURE);
    }
    //send request to server
    string request = input_request[*index].request_string;
    char buffer[MAX_REQUEST_LENGTH];
    // write data to the server
    strcpy(buffer, request.c_str());
    int read_size = write(sockfd, buffer, strlen(buffer));
    if (read_size == -1)
    {
        perror("write failed");
        exit(EXIT_FAILURE);
    }
    bzero(buffer, MAX_REQUEST_LENGTH);
    //read from server
    read_size = read(sockfd, buffer, MAX_REQUEST_LENGTH);
    if (read_size == 0)
    {
        //client disconnected
        perror("read failed");
        exit(EXIT_FAILURE);
    }
    //print response from server
    cout << (*index) <<":"<< buffer;
    close(sockfd);
    pthread_exit(NULL);
}
int main()
{
    //first take input of number of user requests
    int num_user_requests;
    cin>>num_user_requests;
    // make num_user_requests threads
    pthread_t client_threads[num_user_requests];
    // make an array of user_request structs
    input_request=(user_request*)malloc(sizeof(user_request)*num_user_requests);
    for(int i=0; i < num_user_requests ;i++)
    {
       // first take input of the time of request
       cin>> input_request[i].request_time;
       // set the request_index to i
       input_request[i].request_index = i;
       // take input of the actual command
       string s;
       getline(cin,s);
       s=s.substr(1);
       input_request[i].request_string=s;
       input_request[i].request_command = parse(s);
       int* a=(int *) malloc(sizeof(int));
       *a=i;
       pthread_create(&client_threads[i],NULL,client_thread_sim,a);
    }
    for(int i=0; i < num_user_requests ;i++)
    {
        pthread_join(client_threads[i],NULL);
    }
}
