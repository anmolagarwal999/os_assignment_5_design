#ifndef CLIENT__HPP
#define CLIENT__HPP
#include <string> 
#include <vector>
using namespace std;
struct user_request{
    int request_time; // time after which the request is sent to the server
    int request_index; // the request of the index, to be used when printing the request number in the client side
    string request_string;
    vector <string> request_command; // stores the parsed command   
};
#endif

