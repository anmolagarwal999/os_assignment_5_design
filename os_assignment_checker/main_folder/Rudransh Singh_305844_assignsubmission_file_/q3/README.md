# Multithreaded client and server

We essentially have two kinds of threads, a client thread and a server thread.

We first take the input of the number of user requests, take input of all of these requests, and create a client thread for each of them. in `server.cpp` we create a queue of sockets, every worker thread gets his job by dequeing and then performs the corresponding actions on the dictionary.

In server.cpp we also maintain a dictionary which is just a hashtable of type `<int,value>` where `value` is basically a struct consisting of a string and a mutex lock. We need this mutex lock in order to keep the dictionary threadsafe. We initialize the dictionary by initializing all the mutex locks, and setting the string values to "" (the empty string). We also have a mutex lock for the queue, in order to keep it threadsafe

1. Client Thread
   - The client threads will first try to create a socket using the `socket` function
   - Once that is done, they will try to connect to the server using `connect` function.
   - We then send our client request to the server using the `write` function
   - We then recieve back our output using the `read` function and then display it with the appropriate index number.
2. Client_Seach function ( in server.cpp)
   - The client search function first creates a socket 
   - It then binds the socket to the server
   - It then listens for connection requests
   - If it receieves a connection request, it enqueues the fd of the socket which wishes to connect to the queue( we would of course, first lock the queue by means of a mutex lock)
3. Worker_thread function 
   - The worker threads looks in the queue
   - If the queue is not empty, it grabs an element using dequeue ( we first lock the queue )
   - it then processes the operation on the dictionary
4. processoperation function
   - It reads the request from the client through the socket fd
   - it then parses the request
   - If the request was an insert, then it inserts the element into the dictionary. Similarly if it was a delete operation then it would delete the key from the dictionary, and so on.
   - It then writes back with the appropriate message back to the client