#ifndef SERVER_HPP
#define SERVER_HPP
#include <pthread.h>
#include <string>
using namespace std;
//we first need to define the value struct for the dictionary
struct value {
    string s;
    pthread_mutex_t valuelock; // since dictionary won't be thread safe, therefore we need a lock for each value
};

// the dictionary would be defined as <int, value>

#endif