#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <pthread.h>
//we define lab, course, and students structs here
typedef struct TA TA;
struct TA{
    int num_course_taken; // number of courses taken by TA
    pthread_mutex_t TA_lock; //lock for TA
};
typedef struct lab lab;
struct lab{
    char    lab_name[20]; // name of the lab
    int     lab_TA_limit; // number of TA's allowed in the lab
    TA*      TA_list; // this will be a dynamically allocated array which will store the number of courses taken by each TA   
    int     lab_TA_max_times; // max number of times a TA can take a course 
    int     num_TAs_available; // number of TA's available in the lab (will be decremented, if a TA has reached his limit for number of courses to teach)
};
typedef struct course course;
struct course{
    char    course_name[20]; // name of the course
    double  interest_quotient; // interest quotient of the course
    int     max_slots_for_tut; // max number of slots for tutorial that can be assigned by a TA
    int     accepted_lab_num; // number of accepted labs for the course
    int*    accepted_lab_list; // is a list of size num_labs, 1 if the lab is accepted by the course, 0 otherwise
    int     withdrawn; // 1 if the course is withdrawn, 0 otherwise
    pthread_mutex_t course_lock; // lock for the course
    pthread_mutex_t course_tut_lock; // lock for the course's tutorial
    int     num_students_waiting_for_alloc; // number of students waiting for allocation to course
    int     num_students_waiting_for_tut; // number of students waiting for tutorial to be assigned
    pthread_cond_t course_condition; // condition variable for the course, will be invoked when seats are allocated to students
    pthread_cond_t tut_condition; // condition variable for the tutorial, will be invoked when students are invited for tutorial
};
typedef struct student student;
struct student{
    double  student_calibre; // student's calibre
    int     student_preferences[3]; // student's preferences for the three courses
    int     student_registration_time; // student's registration time
};
#endif
