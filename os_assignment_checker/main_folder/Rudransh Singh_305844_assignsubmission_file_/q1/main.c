//Course Allocation Portal using threads
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "structures.h"
#include "colors.h"

//MIN macro for minimum
#define MIN(a,b) (((a)<(b))?(a):(b))

//define number of labs, students and courses as global variables
int num_labs;
int num_students;
int num_courses;

//pointers to the lab, student and course arrays
lab *inputlabs;
student *inputstudents;
course *inputcourses;

//function to search for TA given input as course_id from the list of accepted labs
int search_TA(int course_id, int* lab_num, int* TA_num) // lab and TA_num passed by reference 
{
    int TA_present = 0; // denotes whether TA is avaialable in some lab or not, he might be busy with some other course tut, but he is present
    while(1)
    {
        for(int i=0;i<num_labs;i++)// go through all the labs
        {
            if(inputcourses[course_id].accepted_lab_list[i])// check if the lab is accepted by the course
            {
                if(inputlabs[i].num_TAs_available>0) // check if the lab has any TA available 
                {
                    TA_present = 1;
                    for(int j=0;j<inputlabs[i].lab_TA_limit;j++)
                    {
                        int try=pthread_mutex_trylock(&inputlabs[i].TA_list[j].TA_lock);
                        if(try==0)
                        {
                            *lab_num = i;
                            *TA_num = j;
                            return 1;
                        }
                    }
                }
            }
        }
        if(TA_present==1) // this condition means that the TA was avaiable in some lab, but he is being used by some other course 
        {
            TA_present=0; // set TA_present back to zero, and search again( goes back through the while )
        }
        else // this means that there are absolutely no TAs available, in any lab, meaning that the course has to be withdrawn 
        {
            return 0;
        }
    }
}
//make simulation for student thread
void* student_simulation(void *arg)
{
    int* index=(int*)arg;
    //sleep until the student_registration_time
    sleep(inputstudents[*index].student_registration_time);
    printf(GREEN"Student %d has filled in preferences for course registration\n"RESET,*index);
    for(int i=0;i<3;i++)
    {
        // choose the ith preference
        int course_pref_no=inputstudents[*index].student_preferences[i];
        //check if the course is withdrawn or not, if it is withdrawn then skip the course
        if(inputcourses[course_pref_no].withdrawn==1)
        {
            if(i!=2)
            {
            printf(BLUE"Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n"RESET,*index,inputcourses[course_pref_no].course_name,i+1,inputcourses[inputstudents[*index].student_preferences[i+1]].course_name,i+2);
            }
            continue;
        }
        // now student waits to be allocated to the course
        //first lock the course thread
        pthread_mutex_lock(&inputcourses[course_pref_no].course_lock);
        //since student is waiting for the course, increment the number of students waiting for the course
        inputcourses[course_pref_no].num_students_waiting_for_alloc++;
        //wait for the course to be allocated to the student
        pthread_cond_wait(&inputcourses[course_pref_no].course_condition,&inputcourses[course_pref_no].course_lock);
        //pthread_unlock
        //waits
        //pthread_lock
        //once the course is allocated to the student, decrement the number of students waiting for the course
        inputcourses[course_pref_no].num_students_waiting_for_alloc--;
        //check again whether the course is withdrawn or not
        if(inputcourses[course_pref_no].withdrawn==1)
        {
            pthread_mutex_unlock(&inputcourses[course_pref_no].course_lock);

            
            if(i!=2)
            {
            printf(BLUE"Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n"RESET,*index,inputcourses[course_pref_no].course_name,i+1,inputcourses[inputstudents[*index].student_preferences[i+1]].course_name,i+2);
            }
            continue;
        }
        //if the course is not withdrawn, then student is allocated to the course
        printf("Student %d has been allocated a seat in course %s\n"RESET,*index,inputcourses[course_pref_no].course_name);
        pthread_mutex_unlock(&inputcourses[course_pref_no].course_lock);
        //now student is allocated to the course, he waits for the tutorial to take place
        pthread_mutex_lock(&inputcourses[course_pref_no].course_tut_lock);
        //increment the number of students waiting for the tutorial
        inputcourses[course_pref_no].num_students_waiting_for_tut++;
        //wait for the tutorial to take place
        pthread_cond_wait(&inputcourses[course_pref_no].tut_condition,&inputcourses[course_pref_no].course_tut_lock);
        pthread_mutex_unlock(&inputcourses[course_pref_no].course_tut_lock);
        // now that student has finished the tutorial, he gets a propbablity of whether he likes the course or not
        double liked_the_course=inputcourses[course_pref_no].interest_quotient*inputstudents[*index].student_calibre;
        // now we take a random number between 0 and 1, if it is greater than liked_the_course then student likes the course
        // else student does not like the course
        //we use drand to generate a random number between 0 and 1
        double random_number=rand()%100+1;
        if(random_number>liked_the_course*100) // then the student does not like the course and changes his preference
        {
            printf(BLUE"Student %d has withdrawn from couse %s\n"RESET,*index,inputcourses[course_pref_no].course_name);
            if(i!=2)
            {
            printf(BLUE"Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n"RESET,*index,inputcourses[course_pref_no].course_name,i+1,inputcourses[inputstudents[*index].student_preferences[i+1]].course_name,i+2);
            continue;
            }
        }
        else
        {
            //student likes the course, so he chooses the course permanently and exits the simulation
            printf("Student %d has selected course %s permanently\n"RESET,*index,inputcourses[course_pref_no].course_name);
            pthread_exit(NULL);
        }
        
    }
    //if the student has not been allocated to any course, then he exits the simulation
    printf(MAGENTA"Student %d couldn't get any of his preferred courses\n"RESET,*index);
    pthread_exit(NULL);
    free(arg);
}

pthread_mutex_t TA_lock;
// make course simulation, course chooses TAs from available labs, TA take random number of students between 1 and course_max_slots
void* course_simulation(void *arg)
{
    int* index=(int*)arg;
    while(1)
    {
    //go through all the labs, and find first lab which is avaialable, and select a TA from that
    int lab_num=-1; // if a suitable TA is found, then his lab_id is stored here
    int TA_num=-1; // if a suitable TA is found, then his TA_id of that particular lab is stored here
    int was_TA_found=search_TA(*index, &lab_num, &TA_num);
    //wait until you have students waiting for the course
    while(inputcourses[*index].num_students_waiting_for_alloc==0);
    if(was_TA_found==0) // if no TA was found, then the course is withdrawn
    {

        //set the withdraw variable of the course to 1
        inputcourses[*index].withdrawn=1;
        pthread_mutex_lock(&inputcourses[*index].course_lock);
        //signal all the students waiting for the course to be allocated
        pthread_cond_broadcast(&inputcourses[*index].course_condition);
        pthread_mutex_unlock(&inputcourses[*index].course_lock);
        printf(RED"Course %s doesn't have any TA's eligible and is removed from course offerings\n"RESET,inputcourses[*index].course_name);
        pthread_exit(NULL);
    }
    else
    {
        //first increase the number of times the TA has been assigned to a course
        inputlabs[lab_num].TA_list[TA_num].num_course_taken++;
        //print which TA was taken for the course and from which lab
        printf("TA %d from lab %s has been allocated to course %s for his %d th TA ship\n"RESET,TA_num,inputlabs[lab_num].lab_name,inputcourses[*index].course_name,inputlabs[lab_num].TA_list[TA_num].num_course_taken);
        // now check whether the TA has reached the maximum number of courses he can take
        if(inputlabs[lab_num].TA_list[TA_num].num_course_taken==inputlabs[lab_num].lab_TA_max_times)
        {
            //if the TA has reached the maximum number of courses he can take, then he is not available for the next course
            inputlabs[lab_num].num_TAs_available--;
        }
        //check if the lab has any more TAs to conduct courses
        if(inputlabs[lab_num].num_TAs_available==0)
        {
            //if the lab has no more TAs to conduct courses, then the lab is not available for the next course
            printf(RED"Lab %s no longer has students available for TA ship\n"RESET,inputlabs[lab_num].lab_name);
        }
        // now allocate random number of seats for the tut
        int D=rand()%(inputcourses[*index].max_slots_for_tut)+1;
        printf("Course %s has been allocated %d seats\n"RESET,inputcourses[*index].course_name,D);
        inputcourses[*index].num_students_waiting_for_tut=0;
        //signal all the students waiting for the tutorial to take place
        pthread_mutex_lock(&inputcourses[*index].course_lock);
        int num_students_waiting=inputcourses[*index].num_students_waiting_for_alloc;
        // now signal D times the course condition
        for(int i=0;i<D;i++)
        {
            pthread_cond_signal(&inputcourses[*index].course_condition);
        }
        pthread_mutex_unlock(&inputcourses[*index].course_lock);
        //now wait for the tutorial to take place
        while(inputcourses[*index].num_students_waiting_for_tut < MIN(D,num_students_waiting));
        printf(CYAN"Tutorial has started for Course %s with %d seats filled out of %d\n"RESET,inputcourses[*index].course_name,inputcourses[*index].num_students_waiting_for_tut,inputcourses[*index].max_slots_for_tut);
        sleep(1);

        // now broadcast to all student threads that the tutorial has ended
        pthread_mutex_lock(&inputcourses[*index].course_tut_lock);
        printf(GREEN"TA %d from lab %s has completed the tutorial and left the course %s\n"RESET,TA_num,inputlabs[lab_num].lab_name,inputcourses[*index].course_name);
        pthread_cond_broadcast(&inputcourses[*index].tut_condition);
        pthread_mutex_unlock(&inputcourses[*index].course_tut_lock);

        // Release the TA lock
        if(inputlabs[lab_num].lab_TA_max_times != inputlabs[lab_num].TA_list[TA_num].num_course_taken)
        {
            pthread_mutex_unlock(&inputlabs[lab_num].TA_list[TA_num].TA_lock);
        }

    }
    }
}




//take input of courses, students, and labs
void take_input()
{
    scanf("%d", &num_students);
    scanf("%d", &num_labs);
    scanf("%d", &num_courses);
    inputlabs=(lab*)malloc(num_labs*sizeof(lab));
    inputstudents=(student*)malloc(num_students*sizeof(student));
    inputcourses=(course*)malloc(num_courses*sizeof(course));
    //first take input of all courses, their course names, their interest quotients, max slots, and number of labs from which TAs can be assigned
    for(int i=0;i<num_courses;i++)
    {
        scanf("%s", inputcourses[i].course_name); // take input of course name
        scanf("%lf", &inputcourses[i].interest_quotient); // take input of interest quotient
        scanf("%d", &inputcourses[i].max_slots_for_tut); // take input of max slots for tut
        scanf("%d", &inputcourses[i].accepted_lab_num); // take input of number of labs from which TAs can be assigned
        inputcourses[i].accepted_lab_list=(int*)malloc(num_labs*sizeof(int)); // allocate memory for accepted lab list
        for(int j=0;j<inputcourses[i].accepted_lab_num;j++)
        {
            int temp;
            scanf("%d",&temp);
            inputcourses[i].accepted_lab_list[temp]=1; // 1 indicates that the lab can be used for assigning TAs
        }
    }
    //take input of all students, their calibre, their preferences, and their registration times
    for(int i=0;i<num_students;i++)
    {
        scanf("%lf", &inputstudents[i].student_calibre); // take input of student calibre
        for(int j=0;j<3;j++)
        {
            scanf("%d", &inputstudents[i].student_preferences[j]); // take input of student preferences
        }
        scanf("%d", &inputstudents[i].student_registration_time); // take input of student registration time
    }
    //take input of all labs, their lab names, their max TA limits, and the max number of tiems a TA can take a course
    for(int i=0;i<num_labs;i++)
    {
        scanf("%s", inputlabs[i].lab_name); // take input for lab name
        scanf("%d", &inputlabs[i].lab_TA_limit); // take input for lab TA limit
        inputlabs[i].TA_list=(TA*)malloc(inputlabs[i].lab_TA_limit*sizeof(TA)); // allocate memory for TA list
        for(int j=0;j<inputlabs[i].lab_TA_limit;j++)
        {
            inputlabs[i].TA_list[j].num_course_taken=0; // initialize TA course taken to 0
        }
        scanf("%d", &inputlabs[i].lab_TA_max_times); // take input for lab TA max times
        inputlabs[i].num_TAs_available=inputlabs[i].lab_TA_limit; // initialize number of TAs available to TA limit
    }

}

int main()
{
    srand(time(0));
    //initialize mutex for TA
    pthread_mutex_init(&TA_lock,NULL);
    //take input of courses, students, and labs
    take_input();
    //make thread for every student
    pthread_t student_threads[num_students];
    // now initialize all locks and conditions for every course, and TA
    for(int i=0;i<num_courses;i++)
    {
        pthread_mutex_init(&inputcourses[i].course_lock,NULL);
        pthread_cond_init(&inputcourses[i].course_condition,NULL);
        pthread_mutex_init(&inputcourses[i].course_tut_lock,NULL);
        pthread_cond_init(&inputcourses[i].tut_condition,NULL);
    }
    for(int i=0;i<num_labs;i++)
    {
        for(int j=0;j<inputlabs[i].num_TAs_available;j++)
        {
            pthread_mutex_init(&inputlabs[i].TA_list[j].TA_lock,NULL);
        }
    }
    //make thread for every course
    pthread_t course_threads[num_courses];
    for(int i=0;i<num_students;i++)
    {
        int* a=(int*)malloc(sizeof(int));
        *a=i;
        pthread_create(&student_threads[i],NULL,student_simulation,a); // create all student threads
    }
    for(int i=0;i<num_courses;i++)
    {
        int* a=(int*)malloc(sizeof(int));
        *a=i;
        pthread_create(&course_threads[i],NULL,course_simulation,a); // create all course threads
    }
    
    for(int i=0;i<num_students;i++)
    {
        pthread_join(student_threads[i],NULL); // wait for all student threads to finish
    }
    for(int i=0;i<num_courses;i++)
    {
        pthread_cancel(course_threads[i]); // wait for all course threads to finish
    }

    //destroy all locks and conditions for every course, and TA
    for(int i=0;i<num_courses;i++)
    {
        pthread_mutex_destroy(&inputcourses[i].course_lock);
        pthread_cond_destroy(&inputcourses[i].course_condition);
        pthread_mutex_destroy(&inputcourses[i].course_tut_lock);
        pthread_cond_destroy(&inputcourses[i].tut_condition);
    }
    for(int i=0;i<num_labs;i++)
    {
        for(int j=0;j<inputlabs[i].num_TAs_available;j++)
        {
            pthread_mutex_destroy(&inputlabs[i].TA_list[j].TA_lock);
        }
    }
    
}
