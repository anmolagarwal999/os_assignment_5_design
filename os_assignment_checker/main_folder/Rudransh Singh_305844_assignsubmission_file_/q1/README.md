# Course Allocation Portal

## Threads used 

- **Student** Thread ( for every student )
- **Course** Threads ( for every course )

The course threads run indefinitely, and once all the student threads are finished, we know that the simulation has ended, so we kill the threads one by one using `pthread_cancel` 

Functions used 

- `student_simulation` for simulating the student for every thread 
- `course_simulation` for simulating every course
- `search_TA` for searching for a TA in a particular lab

###### How the `search TA` function works

It basically goes through the list of all accepted labs, each lab has a variable called `num_TAs_available` which basically indicates the number of TAs that can tutor( as in, they haven't exhausted all of their TAships ). If a TA has exhausted his TAships, then we decrement this value for the particular lab. Now, for each lab, we check the value of `num_TAs_available` , if it is positive, it indicates that there is atleast one TA available to teach, and we set a marker called `TA_found` to ` ,we then iterate through all TAs of this particular lab, and perform a trylock, if the lock is successful then we have succesfully obtained a TA, store his TA and lab number and return. If not, then there are two possible scenarios, when exiting the for loop

- it exits the for loop with `TA_found` =1 , meaning that there was a TA available for tutoring a course, but he might be tutoring some other course at this time. Therefore we try to search again.
- It exists the for loop with `TA_found` =0, meaning that there was no TA available in any of the accepted labs, meaning that the course must be withdrawn. We have a variable called `withdrawn` for every course, we set this to 1 and send a signal to all students.

###### The General Outline of the Course and Student Threads is as follows

**Student Thread**

- sleeps till his registration time
- It now iterates through his three preferences.
- every course has a `num_students_waiting_for_alloc`. The student acquires the lock for the course and increments this variable, meaning that it has now registered for the course, and waits for the tutorial to happen. Once it receives the signal, it can now proceed to take part in the tutorial and therefore decrements the courses `num_students_waiting_for_alloc`
- Now, the course might have been withdrawn, so we just check for that condition as well.
- Student now takes part in the tut, and increments the `num_students_waiting_for_tut`variable, and then waits for signal.
- If it receives the signal, it means that the tut has ended, we now take a random number between 1 to 100, if the number is greater than `liked_the_course*100` then the student did not like the course, and hence proceeds to withdraw and switches his preference, else he finalizes the course and exits the simulation.
- In case the student didn't like the course, or if the course was withdrawn, then we just skip the preference (using `continue`) . If he exhausts all of his preferences, then we exit the simulation.

**Course Thread**

- First waits for atleast one student to register, since we did not have any restrictions on busy waiting for the course thread therefore it shouldn't be a problem
-  First searches for an appropriate TA from all the labs.
- If a TA was not found, then set the `withdrawn` variable of the course to 1, and the course thread stops.
- If a TA was found, we increment the number of courses he took
- We then check if the number of courses he took is equal to the max number of courses he can take. If it is equal, then we reduce the `num_TAs_available` for that particular lab.
- We then choose a random number of students ( say `D`) between 1 and the max number of tut slots.
- We then send D signals to students, thereby waking up exactly D students, conduct the tutorial( sleeping for 1 sec) and then broadcast to every student that the tutorial has ended.
- Also, if the TA is free to take more courses, then we release his lock. Otherwise we don't , this is to ensure that he doesn't get accidentally used up by another course, even if he has been exhausted his TAships.
