# The Clasico Experience

We have essentially three threads here

1.  **Goal Thread**
   -  Simulates the goal scoring by sleeping for the appropriate time
   - Scores the goal or misses it by choosing a random number between 1 to 100, if the number is greater than the probablity of scoring a goal, then it is a miss, otherwise it is a goal
   - If it is a goal, increment the goals_count array of the appropriate index depending on whether it was the home or away team that scored. 
   - It also broadcasts the goal to every spectator thread
2. **Spectator Thread**
   - Sleeps until the time he reaches the stadium
   - Does a `cond_timedwait` until his patience, if he doesn't receive a signal in the time frame, it means that the spectator did not get a seat
   - If he does get a signal, he then does a `sem_trywait` on his corresponding zones ( an away fan can sit in the away and neutral zone, so he does a trywait on those zone threads)
   - Once he gets a seat, he now does a timedwait again, this time if he doesn't receive a signal, it means that he has exhausted his spectating time, and proceeds to leave the stadium
   - If he recieves a signal, it means that a goal has been scored. It checks whether the number of goals scored by the opposition is more than the spectator's `rage lim` . If it is, then he leaves the stadium, if not then we wait again.
   - Once the spectator leaves the stadium, we perform a sem post on the zone he was allocated the seat in, indicating that the number of seats has increased.
3. **Zone thread**
   - The zone thread essentially checks whether there is a seat available or not by means of a `sem_getvalue` if there is a seat avaialble, then he signals the corresponding fans.
   - Once all the spectator threads are finished executing, we kill the zone threads by means of a pthread_cancel 