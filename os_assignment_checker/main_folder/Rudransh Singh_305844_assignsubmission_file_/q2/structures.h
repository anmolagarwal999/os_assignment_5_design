#ifndef STRUCTURES__H
#define STRUCTURES__H
typedef struct spectator spectator; // struct for spectator 
struct spectator{
    char name_of_spectator[100]; // name of the spectator 
    char type_of_spectator; // type of the spectator ( whether home (H) or Neutral (N) or Away (A) )
    int time_to_reach_stadium; // time to reach the stadium
    int patience; // patience of the spectator( time after which he leaves if he doesn't get a seat )
    int rage_lim; // number of goals his team can concede before he leaves

};

typedef struct group group; // struct for group

struct group{
    int group_number; // group number
    int group_size; // group size
    spectator *group_member; // array of pointers to the spectators in the group
};

typedef struct goals goals; // struct for goals

struct goals{
    int time_of_goal; // time of the goal
    double probability_of_goal; // probability of the goal
    char team_id; // team id of the team that could score the goal
};

#endif
