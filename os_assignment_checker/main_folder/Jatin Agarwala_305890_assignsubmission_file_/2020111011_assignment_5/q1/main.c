
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"

/*

COLOUR CODING USED-

Magenta for Student filling preference, Student getting allocated
Yellow for Student withdrawn, changed preference
Green for Student chose permanently, or couldn't get preference

Cyan for seat allocation
Blue for TA started, TA completed
LIGHT_PURPLE for no eligible TAs in course

Purple for ta allocated
LIGHT_PURPLE for TAs no longer available in lab

Orange for visualising Bonus

White for extra statements
Red for any error or debugging messages
*/

#define MAX_STR 255

pthread_mutex_t printLock;

typedef struct ta TA;
struct ta
{
    int ID;                     // ID of the TA
    int tutsTaken;              // Number of tutorials taken so far
    int maxTuts;                // Maximum number of tutorials allowed in the lab
    pthread_mutex_t taLock;     // Lock for the TA
};

typedef struct course Course;
struct course
{
    char* name;                 // name of the course
    float interest;             // interest of the course
    int maxSlots;               // maximum slots allowed per tutorial in the course
    int numLabs;                // Number of labs from which TAs can be taken
    int* labs;                  // IDs of the labs from which TAs can be taken
    int numAvailableLabs;       // number of labs which have TAs available
    int numWaitingForTut;       // Number of students waiting for a tutorial in the course
    int* waitingIDs;            // IDs of the students waiting for a tutorial in the course
    int state;                  // 0 = waiting for TA, 1 = conducting, 2 = withdrawn

    pthread_mutex_t courseLock; // lock for the course
};

typedef struct student Student;
struct student
{
    int ID;                         // ID of the student
    float calibre;                  // calibre of the student
    int preferences[3];             // the preferences of the students, [0] being highest
    int timeRegister;               // time at which student registers
    int state;                      // 0 = not registered/withdrawn, 1 = waiting for seat, 2 = attending tut, 3 = selected/exited
    char finalCourse[MAX_STR];      // the name of the course the student finally chose

    pthread_mutex_t studentLock;    // lock for the student
    pthread_cond_t studentWait;     // conditional variable for the student
};
int studID = 0;

typedef struct lab Lab;
struct lab
{
    int ID;             // ID of the lab
    char* name;         // name of the lab
    int numTAs;         // number of TAs in the lab
    TA** TAinfo;        // array of TA*, which stores the information of each TA
    int limit;          // maximum number of tutorials a TA can take in the lab
    int currT;          // current number of tutorials taken by every TA
    int numAvailable;   // decremented as TAs reach limit
    int state;          // 0 = at least 1 TA left, 1 = no TAs left

    pthread_mutex_t  labLock;
    pthread_mutex_t  updateLabLock; 

};
int labID = 0;

Course* initCourse()
{
    Course* temp = (Course*)malloc(sizeof(Course));
    temp->name = (char*)malloc(MAX_STR);
    return temp;
}

Student* initStudent()
{
    Student* temp = (Student*)malloc(sizeof(Student));
    temp->ID = studID;
    studID++;
    return temp;
}

Lab* initLab()
{
    Lab* temp = (Lab*)malloc(sizeof(Lab));
    temp->name = (char*)malloc(MAX_STR);
    temp->ID = labID;
    labID++;
    return temp;
}

TA* initTA()
{
    TA* temp = (TA*)malloc(sizeof(TA));
    return temp;
}

Course** courseArray;
Student** studentArray;
Lab** labArray;
int s,l,c;
int minRegTime = INT_MAX;
int studentsLeft;

// for Bonus
int* labFlags;
pthread_mutex_t* flagLocks;

// void swapStudents(int i, int j)
// {
//     int IDTemp = studentArray[i]->ID;
//     float calibreTemp = studentArray[i]->calibre;
//     int preferencesTemp[3];
//     for(int k=0;k<3;k++)
//         preferencesTemp[k] = studentArray[i]->preferences[k];
//     int timeRegisterTemp = studentArray[i]->timeRegister;   

//     studentArray[i]->ID = studentArray[j]->ID;
//     studentArray[i]->calibre = studentArray[j]->calibre;
//     for(int k=0;k<3;k++)
//         studentArray[i]->preferences[k] = studentArray[j]->preferences[k];
//     studentArray[i]->timeRegister = studentArray[j]->timeRegister;

//     studentArray[j]->ID = IDTemp;
//     studentArray[j]->calibre = calibreTemp;
//     for(int k=0;k<3;k++)
//         studentArray[j]->preferences[k] = preferencesTemp[k];
//     studentArray[j]->timeRegister = timeRegisterTemp;    
//     return;
// }

// void sortStudentArray(int n)
// {
//     int i, j;
//     for(i=0;i<n-1;i++)
//         for(j=0;j<n-i-1;j++)
//             if (studentArray[j]->timeRegister > studentArray[j+1]->timeRegister)
//                 swapStudents(j,j+1);
//     return;
// }

void readInput()    // function to read the input into the corresponding structs
{
    scanf("%d %d %d", &s, &l, &c);
    courseArray = (Course**)malloc(sizeof(Course*)*c);
    studentArray = (Student**)malloc(sizeof(Student*)*s);
    labArray = (Lab**)malloc(sizeof(Lab*)*l);

    for(int i=0;i<c;i++)
    {
        courseArray[i] = initCourse();
        scanf("%s",courseArray[i]->name);
        scanf("%f %d %d",&(courseArray[i]->interest), &(courseArray[i]->maxSlots), &(courseArray[i]->numLabs));
        courseArray[i]->labs = (int*)malloc(sizeof(int)*(courseArray[i]->numLabs));
        for(int j=0;j<courseArray[i]->numLabs;j++)
            scanf("%d",&(courseArray[i]->labs[j]));
        pthread_mutex_init(&(courseArray[i]->courseLock), NULL);
        courseArray[i]->state = 0;
        courseArray[i]->numWaitingForTut = 0;
        courseArray[i]->numAvailableLabs = courseArray[i]->numLabs;
        courseArray[i]->waitingIDs=(int*)malloc(sizeof(int)*s);
        for(int j=0;j<s;j++)
            courseArray[i]->waitingIDs[j] = -1;

        // printf("Course %s has %f %d %d and %d labs:\n",courseArray[i]->name, courseArray[i]->interest, courseArray[i]->maxSlots, courseArray[i]->numLabs,courseArray[i]->numLabs);
        // for(int j=0;j<courseArray[i]->numLabs;j++)
        // {
        //     printf("%d\n",courseArray[i]->labs[j]);
        // }
    }
    for(int i=0;i<s;i++)
    {
        studentArray[i] = initStudent();
        studentArray[i]->state = 0;
        strcpy(studentArray[i]->finalCourse, "Could not get his preferred courses");
        scanf("%f", &(studentArray[i]->calibre));
        for(int j=0;j<3;j++)
            scanf("%d",&(studentArray[i]->preferences[j]));
        scanf("%d",&(studentArray[i]->timeRegister));
        if(studentArray[i]->timeRegister < minRegTime)
            minRegTime = studentArray[i]->timeRegister;
        pthread_mutex_init(&(studentArray[i]->studentLock), NULL);
        pthread_cond_init(&(studentArray[i]->studentWait), NULL);
    }
    for(int i=0;i<l;i++)
    {
        labArray[i] = initLab();
        labArray[i]->state = 0;
        pthread_mutex_init(&(labArray[i]->labLock), NULL);
        pthread_mutex_init(&(labArray[i]->updateLabLock), NULL);
        scanf("%s %d %d", labArray[i]->name, &(labArray[i]->numTAs), &(labArray[i]->limit));
        labArray[i]->currT = 0;
        labArray[i]->numAvailable = labArray[i]->numTAs;
        labArray[i]->TAinfo = (TA**)malloc(sizeof(TA*)*labArray[i]->numTAs);
        for(int j=0;j<labArray[i]->numTAs;j++)
        {
            labArray[i]->TAinfo[j] = initTA();
            labArray[i]->TAinfo[j]->ID = j;
            labArray[i]->TAinfo[j]->maxTuts = labArray[i]->limit;
            labArray[i]->TAinfo[j]->tutsTaken = 0;
            pthread_mutex_init(&(labArray[i]->TAinfo[j]->taLock), NULL);
        }
    }
    // sortStudentArray(s);
    return;
}

void simulateTut(Lab* currLab, TA* currTA, Course* currCourse)  // function to simulate a tutorial after a TA has been found
{
    int D = (rand()%currCourse->maxSlots)+1;
    pthread_mutex_lock(&printLock);
    printf(CYAN "Course %s has been allocated %d seats\n" RESET, currCourse->name, D);
    pthread_mutex_unlock(&printLock);

    int allocatedIDs[D];
    int W = 0;
    // if(currCourse->numWaitingForTut >= D)
    //     W = D;
    // else
    //     W = currCourse->numWaitingForTut;

    for(int i=0;i<s;i++)
    {
        if(W==D || currCourse->numWaitingForTut == 0)
            break;
        if(currCourse->waitingIDs[i]!=-1)
        {
            Student* currStudent = studentArray[currCourse->waitingIDs[i]];
            pthread_mutex_lock(&(currStudent->studentLock));

            currStudent->state = 2;
            allocatedIDs[W] = currStudent->ID;
            W++;
            pthread_mutex_lock(&printLock);
            printf(MAGENTA "Student %d has been allocated a seat in course %s\n" RESET, currStudent->ID, currCourse->name);
            pthread_mutex_unlock(&printLock);
            currCourse->numWaitingForTut--;
            currCourse->waitingIDs[i]=-1;
        }
    }

    pthread_mutex_lock(&printLock);
    printf(BLUE "Tutorial has started for course %s with %d seats filled out of %d\n" RESET, currCourse->name, W, D);
    pthread_mutex_unlock(&printLock);
    sleep(5);

    for(int i=0;i<W;i++)
    {
        Student* currStudent = studentArray[allocatedIDs[i]];
        currStudent->state = 2;
        double prob = currStudent->calibre*currCourse->interest;
        if((double)((rand() % 10000) / 10000.0) <= prob)    // accepted
        {
            pthread_mutex_lock(&printLock);
            printf(GREEN "Student %d has selected the course %s permanently\n" RESET,currStudent->ID, currCourse->name);
            pthread_mutex_unlock(&printLock);
            strcpy(currStudent->finalCourse, currCourse->name);
            currStudent->state = 3;
        }
        else
        {
            pthread_mutex_lock(&printLock);
            printf(YELLOW "Student %d has withdrawn from course %s\n" RESET, currStudent->ID, currCourse->name);
            pthread_mutex_unlock(&printLock);
            currStudent->state = 0;
        }
        pthread_mutex_unlock(&(currStudent->studentLock));
        pthread_cond_broadcast(&(currStudent->studentWait));
    }

    pthread_mutex_lock(&printLock);
    printf(BLUE "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, currTA->ID, currLab->name, currCourse->name);
    pthread_mutex_unlock(&printLock);

    if(currTA->tutsTaken == currLab->limit)
    {
        pthread_mutex_lock(&(currLab->labLock));
        currLab->numAvailable--;
        pthread_mutex_unlock(&(currLab->labLock));
    }
    return;
}

void getTA(Course* currCourse)      // function to find a TA for a course, called by every course thread
{
    if(currCourse->numAvailableLabs == 0)
        return;
    for(int i=0;i<currCourse->numLabs;i++)
    {
        if(currCourse->labs[i] == -1)
            continue;

        // printf("course %s trying to allocate lab %d\n",currCourse->name, currCourse->labs[i]);
        Lab* tempL = labArray[currCourse->labs[i]];
        // printf("course %s allocated lab %d\n",currCourse->name, currCourse->labs[i]);
        if(tempL->numAvailable == 0)
        {
            currCourse->labs[i] = -1;
            currCourse->numAvailableLabs--;
            continue;
        }
        int flag = 0;
        for(int j=0;j<tempL->numTAs;j++)
        {
            TA* tempT = tempL->TAinfo[j];
            pthread_mutex_lock(&(tempT->taLock));
            pthread_mutex_lock(&(tempL->updateLabLock));
            if(tempT->tutsTaken <= tempL->currT)
            {
                tempT->tutsTaken++;
                pthread_mutex_unlock(&(tempL->updateLabLock));
                flag = 1;

                pthread_mutex_lock(&flagLocks[tempL->ID]);
                labFlags[tempL->ID] = 0;
                pthread_mutex_unlock(&flagLocks[tempL->ID]);

                pthread_mutex_lock(&printLock);
                printf(BLUE "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" RESET,j,tempL->name,currCourse->name,tempT->tutsTaken);
                pthread_mutex_unlock(&printLock);

                pthread_mutex_lock(&(currCourse->courseLock));

                currCourse->state = 1;
                // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);

                simulateTut(tempL, tempT, currCourse);

                currCourse->state = 0;
                // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);

                pthread_mutex_unlock(&(currCourse->courseLock));
                pthread_mutex_unlock(&(tempT->taLock));
                break;
            }
            else
                pthread_mutex_unlock(&(tempL->updateLabLock));
            pthread_mutex_unlock(&(tempT->taLock));
        }
        if(tempL->numAvailable == 0)
        {
            // printf(RED "Course %s is waiting for lock of lab %s\n" RESET,currCourse->name, tempL->name);
            pthread_mutex_lock(&tempL->labLock);
            // printf(RED "Course %s has acquired lock of lab %s\n" RESET,currCourse->name, tempL->name);
            if(tempL->state == 0)
            {
                pthread_mutex_lock(&printLock);
                printf(LIGHT_PURPLE "Lab %s no longer has students available for TA ship\n" RESET, tempL->name);
                pthread_mutex_unlock(&printLock);
                tempL->state = 1;
            }
            pthread_mutex_unlock(&tempL->labLock);
            pthread_mutex_lock(&(currCourse->courseLock));
            currCourse->numAvailableLabs--;
            currCourse->labs[i] = -1;
            if(currCourse->numAvailableLabs == 0)
            {
                pthread_mutex_lock(&printLock);
                printf(LIGHT_PURPLE "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" RESET, currCourse->name);
                pthread_mutex_unlock(&printLock);

                currCourse->state = 2;
                // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);
                for(int i=0;i<s;i++)
                {
                    if(currCourse->numWaitingForTut == 0)
                        break;
                    if(currCourse->waitingIDs[i]!=-1)
                    {
                        Student* currStudent = studentArray[currCourse->waitingIDs[i]];
                        pthread_mutex_lock(&(currStudent->studentLock));

                        pthread_mutex_lock(&printLock);
                        printf(YELLOW "Student %d has withdrawn from course %s\n" RESET, currStudent->ID, currCourse->name);
                        pthread_mutex_unlock(&printLock);

                        currStudent->state = 0;
                        currCourse->numWaitingForTut--;
                        currCourse->waitingIDs[i]=-1;
                        pthread_mutex_unlock(&(currStudent->studentLock));
                        pthread_cond_broadcast(&(currStudent->studentWait));
                    }
                }
            }
            else
            {
                currCourse->state = 0;
                // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);
            }
            pthread_mutex_unlock(&(currCourse->courseLock));
        }
        // if pthread_mutex_trylock acquires lock, it returns 0. Otherwise, someone else has the lock and it doesn't acquire the lock
        pthread_mutex_lock(&(tempL->updateLabLock));
        if(flag == 0)
        {
            pthread_mutex_lock(&flagLocks[tempL->ID]);
            if(labFlags[tempL->ID] != 1)
            {
                int check = 0;
                for(int id=0;id<tempL->numTAs;id++)
                {
                    if(tempL->TAinfo[id]->tutsTaken == tempL->currT)
                    {
                        labFlags[tempL->ID] = 0;
                        check = 1;
                        break;
                    }
                }
                if(check)
                {
                    pthread_mutex_unlock(&flagLocks[tempL->ID]);
                    pthread_mutex_unlock(&(tempL->updateLabLock));
                    continue;
                }
                tempL->currT++;
                labFlags[tempL->ID] = 1;

                pthread_mutex_lock(&printLock);
                printf(ORANGE "BONUS: Current tut limit for lab %s incremented from %d to %d\n" RESET, tempL->name, tempL->currT, tempL->currT + 1);
                for(int id=0;id<tempL->numTAs;id++)
                {
                    printf(ORANGE "BONUS: Number of tutorials taken by TA %d is %d\n" RESET, id, tempL->TAinfo[id]->tutsTaken);
                }
                pthread_mutex_unlock(&printLock);

                if(tempL->currT > tempL->limit)
                {
                    // if(tempL->numAvailable > 0)
                    //     printf(RED "ERROR IN SETTING NUMBER OF AVAILABLE TAs IN LAB %s\n" RESET, tempL->name);
                    tempL->numAvailable = 0;
                }
            }
            pthread_mutex_unlock(&flagLocks[tempL->ID]);
        }
        pthread_mutex_unlock(&(tempL->updateLabLock));
    }
    return;
}

void* courseRoutine(void* arg)      // function called by every course thread
{
    Course* currCourse = (Course*)arg;
    pthread_mutex_lock(&currCourse->courseLock);
    currCourse->state = 0;
    // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);
    pthread_mutex_unlock(&currCourse->courseLock);
    if(s>0)
        sleep(minRegTime);

    while(studentsLeft && currCourse->state!=2 && currCourse->numAvailableLabs>0)
    {
        // if(currCourse->numWaitingForTut == 0)
        // {
        //     pthread_mutex_lock(&currCourse->tutLock);
        //     while(currCourse->numWaitingForTut == 0)
        //     {
        //         pthread_cond_wait(&currCourse->tutWait,&currCourse->tutLock);
        //     }
        //     pthread_mutex_unlock(&currCourse->tutLock);
        // }
        // printf("Course %s looking for TA when available labs are %d and interested students are : ",currCourse->name, currCourse->numAvailableLabs);
        // for(int i=0;i<s;i++)
        // {
        //     if(currCourse->waitingIDs[i] != -1)
        //         printf("%d ", currCourse->waitingIDs[i]);
        // }
        // printf("\n");
        getTA(currCourse);
        // sleep(1);
    }
    
    if(currCourse->state != 2)
    {
        currCourse->state = 2;
        pthread_mutex_lock(&printLock);
        printf(LIGHT_PURPLE "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" RESET, currCourse->name);   
        pthread_mutex_unlock(&printLock);

        for(int i=0;i<s;i++)
        {
            if(currCourse->numWaitingForTut == 0)
                break;
            if(currCourse->waitingIDs[i]!=-1)
            {
                Student* currStudent = studentArray[currCourse->waitingIDs[i]];
                pthread_mutex_lock(&(currStudent->studentLock));

                pthread_mutex_lock(&printLock);
                printf(YELLOW "Student %d has withdrawn from course %s\n" RESET, currStudent->ID, currCourse->name);
                pthread_mutex_unlock(&printLock);

                currStudent->state = 0;
                currCourse->numWaitingForTut--;
                currCourse->waitingIDs[i]=-1;
                pthread_mutex_unlock(&(currStudent->studentLock));
                pthread_cond_broadcast(&(currStudent->studentWait));
            }
        }
    }
    // printf(RED "Course %s in state %d\n" RESET, currCourse->name, currCourse->state);
    return NULL;
}

void* studentRoutine(void* arg)     // function called by every student thread
{
    int flag = 0;
    Student* currStudent = (Student*)arg;
    if(currStudent->timeRegister > 0)
        sleep(currStudent->timeRegister);
    pthread_mutex_lock(&printLock);
    printf(MAGENTA "Student %d has filled in preferences for course registration\n" RESET, currStudent->ID);
    pthread_mutex_unlock(&printLock);

    int i;
    for(i=0;i<3;i++)    //iterating over preferences
    {
        int currPref = currStudent->preferences[i];
        if(i>0)
        {
            pthread_mutex_lock(&printLock);
            printf(YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,
            currStudent->ID, courseArray[currStudent->preferences[i-1]]->name, i-1, courseArray[currStudent->preferences[i]]->name, i);
            pthread_mutex_unlock(&printLock);
        }
        pthread_mutex_lock(&(courseArray[currPref]->courseLock));
        if(courseArray[currPref]->numAvailableLabs == 0)
        {
            pthread_mutex_lock(&printLock);
            printf(YELLOW "Student %d has withdrawn from course %s\n" RESET, currStudent->ID, courseArray[currStudent->preferences[i]]->name);
            pthread_mutex_unlock(&printLock);
            pthread_mutex_unlock(&courseArray[currPref]->courseLock);
            continue;
        }

        // pthread_mutex_lock(&courseArray[currPref]->tutLock);

        courseArray[currPref]->numWaitingForTut++;
        for(int j=0;j<s;j++)
        {
            if(courseArray[currPref]->waitingIDs[j] == -1)
            {
                courseArray[currPref]->waitingIDs[j] = currStudent->ID;
                break;
            }
        }
        // pthread_mutex_lock(&courseArray[currPref]->tutLock);
        // pthread_cond_broadcast(&courseArray[currPref]->tutWait);

        pthread_mutex_lock(&(currStudent->studentLock));
        currStudent->state = 1;
        pthread_mutex_unlock(&(courseArray[currPref]->courseLock));      // check for possible deadlock since student holding courseLock while waiting for studentLock
        while(currStudent->state != 0 && currStudent->state != 3)
        {
            // sleep(1);
            // printf(RED "Student %d in state %d\n" RESET, currStudent->ID, currStudent->state);
            pthread_cond_wait(&(currStudent->studentWait), &(currStudent->studentLock));
            // printf(RED "Student %d in state %d\n" RESET, currStudent->ID, currStudent->state);
        }
        if(currStudent->state == 3)
        {
            flag = 1;
            break;
        }
        pthread_mutex_unlock(&(currStudent->studentLock));
    }
    if(flag == 0)
    {
        pthread_mutex_lock(&printLock);
        printf(GREEN "Student %d could not get any of his preferred courses\n" RESET, currStudent->ID);
        pthread_mutex_unlock(&printLock);
    }
    // else the printf was made after the tutorial
    studentsLeft--;
    pthread_mutex_lock(&printLock);
    printf("After exit of Student %d, number of students left: %d\n", currStudent->ID, studentsLeft);
    pthread_mutex_unlock(&printLock);
    return NULL;
}

int main()
{
    srand(time(0));
    pthread_mutex_init(&printLock,NULL);
    readInput();
    labFlags = (int*)malloc(sizeof(int)*l);
    flagLocks = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t)*l);
    for(int i=0;i<l;i++)
    {
        labFlags[i] = 1;
        pthread_mutex_init(&flagLocks[i],NULL);
    }

    studentsLeft = s;
    pthread_t courseThreads[c];
    pthread_t studentThreads[s];
    
    for(int i=0;i<s;i++)
        pthread_create(&(studentThreads[i]),NULL,&studentRoutine,studentArray[i]);
    for(int i=0;i<c;i++)
        pthread_create(&(courseThreads[i]),NULL,&courseRoutine,courseArray[i]);

    for(int i=0;i<s;i++)
        pthread_join(studentThreads[i],NULL);

    pthread_mutex_lock(&printLock);
    printf("All Student threads have joined!\n");
    pthread_mutex_unlock(&printLock);

    for(int i=0;i<c;i++)
        pthread_join(courseThreads[i],NULL);
    printf("All Course threads have joined!\n");
    printf("Simulation has been completed\n");

    // Additional output displaying course chosen by each student
    printf("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
    printf("Final course allocations are:\n");
    for(int i=0;i<s;i++)
        printf("Student %d: %s\n",studentArray[i]->ID, studentArray[i]->finalCourse);
    

    return 0;
}
