
There is a struct each for every Course, Student, TA and Lab
The values, mutex locks and conditional variables associated with the entities are present in their structs

There is a thread for every student which goes in a 3 loop for loop to iterate over the preferences
Once a preference is finalised, the state of the student is changed and the loop is broken
A student enrolls in the course by inserting itself into the list of students waiting for the course, and incrementing the numWaitingForTut value of the course
(The course lock ensures only 1 thread updates values of the course at a time)

After this, the student calls a cond_wait, waiting for the course thread to carry out a tutorial and give the lock back
The course thread updates the state of the student to inform the student thread of the outcome of the tutorial
If the course runs out of TAs while the student is waiting, then too the course activates the students waiting so that there is no deadlock

There is a thread for every course which continuously calls the getTA function as long as there are students left in the simulation and TAs that can take tutorials
The getTA function iterates over the possible labs which can provide a TA for the course
As soon as a TA is found, the tutorial is simulated using the simulateTut function

The simulateTut function finds the waiting students and simulates whether they would like the course using the probability given
It then signals all the conditional variables of the student threads to inform them that the tutorial has completed
A sleep of 5 seconds is made to simulate a tutorial taking place to ensure that a lot of tutorials are not instantly carried out with 0 seats filled
(Note that this sleep duration can be reduced to a value like 2 seconds without affecting the outcome, and it was set to 5 only due to the Moodle post suggestion)

After a tutorial, the value of the tuts taken is incremented for the TA, and the TA is made unavailable if the limit is reached
After the course runs out of TAs, it informs all students threads waiting for a tutorial in the course (if any), and changes its state so that new students can check it and dont have to wait

Besides, there is also a mutex lock for printing to ensure only 1 thread prints at one time

Bonus:
The bonus is implemented using a value currT in structLab which is initially set to 0
currT stores the current number of tuts taken by every TA, and TAs who have taken more than currT tuts are not selected
Everytime there are no TAs available with tutsTaken == currT, it means every TA has taken currT + 1 tutorials and currT is incremented
There is a lock to ensure that multiple course threads do not increment currT at the same time after not finding any TAs

A course can only increment currT is the labFlag of that lab is set to 0
The intial value of this flag is 1, which is set to 0 every time a course thread finds a TA for a tutorial.
Once currT is incremented after a course cannot, the labFlag is set to 1.

This ensures that a TA can only take up his 't+1'th TA-ship only after all the TAs in the lab have taken at least 't' TA-ships
