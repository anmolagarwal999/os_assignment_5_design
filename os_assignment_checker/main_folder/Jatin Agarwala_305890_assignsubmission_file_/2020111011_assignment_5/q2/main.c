#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <string.h>


#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"

#define MAX_LENGTH 1024

typedef struct person Person;
struct person               // struct for spectator
{
    int team;               // 0 for HOME, 1 for NEUTRAL, 2 for AWAY
    char name[MAX_LENGTH];  // name
    int arrivalTime;        // time of arrival
    int patience;           // patience value
    int numGoals;           // number of goals after which he leaves
    int group;              // group number
    int seatAllocated;      // 0 for HOME, 1 for NEUTRAL, 2 for AWAY

    int state;                  // 0 = not reached, 1 = waiting, 2 = watching, 3 = exited
    pthread_mutex_t personLock; // lock for person
    sem_t goalSem;              // semaphore to keep track of goals scored against team
    struct timespec ts;         // used for sem_timedwait
};

typedef struct goalChance GoalChance;
struct goalChance
{
    int team;               // 0 for AWAY, 2 for HOME
    int timeElapsed;        // time after which the chance is created
    float probability;      // probability of scoring
};

int H, A, N;
int spectatingTime;
int numGoalChances = 0;
int hGoals = 0;
int aGoals = 0;

int numGroups;
int* numPerGroup;

sem_t hSem, aSem, nSem;
sem_t* groupSems;
Person*** allPersons;
pthread_t** personThreads;
pthread_t* groupThreads;
pthread_t* goalThread;
pthread_mutex_t printLock;
GoalChance** allGoals;

void* homeSeat(void* arg)   // called when a thread is looking for seat in zone H
{
    Person* currPerson = (Person*)arg;
    if(!sem_timedwait(&hSem,&currPerson->ts))
    {
        pthread_mutex_lock(&(currPerson->personLock));
        if(currPerson->state == 1)
        {
            // printf(CYAN "%s is here\n" RESET,currPerson->name);
            currPerson->state = 2;
            currPerson->seatAllocated = 0;
        }
        else
        {
            sem_post(&hSem);
            pthread_mutex_unlock(&(currPerson->personLock));
            return NULL;
        }
        pthread_mutex_unlock(&(currPerson->personLock));
        
        pthread_mutex_lock(&printLock);
        printf(LIGHT_PURPLE "%s has got a seat in zone H\n" RESET,currPerson->name);
        pthread_mutex_unlock(&printLock);
    }
    return NULL;
}
void* awaySeat(void* arg)   // called when a thread is looking for seat in zone A
{
    Person* currPerson = (Person*)arg;
    if(!sem_timedwait(&aSem,&currPerson->ts))
    {
        pthread_mutex_lock(&(currPerson->personLock));
        if(currPerson->state == 1)
        {
            // printf(CYAN "%s is here\n" RESET,currPerson->name);
            currPerson->state = 2;
            currPerson->seatAllocated = 2;
        }
        else
        {
            sem_post(&aSem);
            pthread_mutex_unlock(&(currPerson->personLock));
            return NULL;
        }
        pthread_mutex_unlock(&(currPerson->personLock));
        
        pthread_mutex_lock(&printLock);
        printf(LIGHT_PURPLE "%s has got a seat in zone A\n" RESET,currPerson->name);
        pthread_mutex_unlock(&printLock);
    }
    return NULL;
}
void* neutralSeat(void* arg)    // called when a thread is looking for seat in zone N
{
    Person* currPerson = (Person*)arg;
    if(!sem_timedwait(&nSem,&currPerson->ts))
    {
        pthread_mutex_lock(&(currPerson->personLock));
        if(currPerson->state == 1)
        {
            // printf(CYAN "%s is here\n" RESET,currPerson->name);
            currPerson->state = 2;
            currPerson->seatAllocated = 1;
        }
        else
        {
            sem_post(&nSem);
            pthread_mutex_unlock(&(currPerson->personLock));
            return NULL;    
        }
        pthread_mutex_unlock(&(currPerson->personLock));
        
        pthread_mutex_lock(&printLock);
        printf(LIGHT_PURPLE "%s has got a seat in zone N\n" RESET,currPerson->name);
        pthread_mutex_unlock(&printLock);
    }
    return NULL;
}

void allocate(Person* currPerson)   // function to allocate a seat and simulate spectating
{
    pthread_t* threads = (pthread_t*)malloc(sizeof(pthread_t)*3);

    if (clock_gettime(CLOCK_REALTIME, &(currPerson->ts)) == -1) {
        perror("clock_gettime");
        // exit(0);
    }
    currPerson->ts.tv_sec += currPerson->patience;

    if(currPerson->team == 0)
    {
        pthread_create(&(threads[0]),NULL,homeSeat,currPerson);
        pthread_create(&(threads[1]),NULL,neutralSeat,currPerson);
        pthread_join(threads[0],NULL);
        pthread_join(threads[1],NULL);
    }
    else if(currPerson->team == 1)
    {
        pthread_create(&(threads[0]),NULL,neutralSeat,currPerson);
        pthread_create(&(threads[1]),NULL,homeSeat,currPerson);
        pthread_create(&(threads[2]),NULL,awaySeat,currPerson);
        pthread_join(threads[0],NULL);
        pthread_join(threads[1],NULL);
        pthread_join(threads[2],NULL);
    }
    else
    {
        pthread_create(&threads[0],NULL,awaySeat,currPerson);
        pthread_join(threads[0],NULL);
    }

    if(currPerson->seatAllocated == -1)
    {
        pthread_mutex_lock(&printLock);
        printf(LIGHT_PURPLE "%s couldn't get a seat\n" RESET,currPerson->name);
        pthread_mutex_unlock(&printLock);
        currPerson->state = 3;
    }
    else
    {
        if(clock_gettime(CLOCK_REALTIME, &(currPerson->ts)) == -1) {
            perror("clock_gettime");
            // exit(0);
        }
        currPerson->ts.tv_sec += spectatingTime;

        int flag = 0;
        if(currPerson->team != 1)
        {
            for(int i=0;i<currPerson->numGoals;i++)
            {
                if(!sem_timedwait(&(currPerson->goalSem),&currPerson->ts))
                {
                    continue;
                }
                else
                {
                    flag = 1;
                    break;
                }
            }
            if(flag == 0)
            {
                pthread_mutex_lock(&printLock);
                printf(GREEN "%s is leaving due to bad performance of his team\n" RESET,currPerson->name);
                pthread_mutex_unlock(&printLock);
                pthread_mutex_lock(&(currPerson->personLock));
                currPerson->state = 3;
                pthread_mutex_unlock(&(currPerson->personLock));
            }
            else
            {
                pthread_mutex_lock(&printLock);
                printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET,currPerson->name,spectatingTime);
                pthread_mutex_unlock(&printLock);
                pthread_mutex_lock(&(currPerson->personLock));
                currPerson->state = 3;
                pthread_mutex_unlock(&(currPerson->personLock));
            }
        }
        else
        {
            sleep(spectatingTime);
            pthread_mutex_lock(&printLock);
            printf(GREEN "%s watched the match for %d seconds and is leaving\n" RESET,currPerson->name,spectatingTime);
            pthread_mutex_unlock(&printLock);
            currPerson->state = 3;
        }

        if(currPerson->seatAllocated == 0)
            sem_post(&hSem);
        else if(currPerson->seatAllocated == 1)
            sem_post(&nSem);
        else if(currPerson->seatAllocated == 2)
            sem_post(&aSem);
    }

    pthread_mutex_lock(&printLock);
    printf(BLUE "%s is waiting for their friends at the exit\n" RESET,currPerson->name);
    pthread_mutex_unlock(&printLock);

    sem_post(&(groupSems[currPerson->group]));

    return;
}

void* personRoutine(void* arg)  // function called by person thread
{
    Person* currPerson = (Person*)arg;
    sleep(currPerson->arrivalTime);

    currPerson->state = 1;
    pthread_mutex_lock(&printLock);
    printf(RED "%s has reached the stadium\n" RESET,currPerson->name);
    pthread_mutex_unlock(&printLock);

    allocate(currPerson);

    return NULL;
}

void* groupRoutine(void* arg)   // function called by group thread to keep track of people from each group
{
    int groupNum = *(int*)arg;
    for(int i=0;i<numPerGroup[groupNum];i++)
        sem_wait(&(groupSems[groupNum]));
    
    pthread_mutex_lock(&printLock);
    printf(ORANGE "Group %d is leaving for dinner\n" RESET,groupNum+1);
    pthread_mutex_unlock(&printLock);

    return NULL;
}

void* goalRoutine(void* arg)    // function to simulate goal scoring, called by every goal thread
{
    GoalChance* currGoal = (GoalChance*)arg;
    sleep(currGoal->timeElapsed);

    if((double)((rand() % 10000) / 10000.0) <= currGoal->probability)    // Goal Scored
    {
        if(currGoal->team == 2)     // Home scored
        {
            hGoals++;
            pthread_mutex_lock(&printLock);
            printf("Team H has scored their %dth goal\n",hGoals);
            pthread_mutex_unlock(&printLock);   
        }
        else
        {
            aGoals++;
            pthread_mutex_lock(&printLock);
            printf("Team A has scored their %dth goal\n",aGoals);
            pthread_mutex_unlock(&printLock);   
        }
        for(int i=0;i<numGroups;i++)
        {
            for(int j=0;j<numPerGroup[i];j++)
            {
                Person* currPerson = allPersons[i][j];
                pthread_mutex_lock(&(currPerson->personLock));
                if(currPerson->team == currGoal->team)
                    sem_post(&(currPerson->goalSem));
                pthread_mutex_unlock(&(currPerson->personLock));
            }
        }
    }
    else
    {
        if(currGoal->team == 2)     // Home missed
        {
            pthread_mutex_lock(&printLock);
            printf("Team H missed the chance to score their %dth goal\n",hGoals+1);
            pthread_mutex_unlock(&printLock);   
        }
        else
        {
            pthread_mutex_lock(&printLock);
            printf("Team A missed the chance to score their %dth goal\n",aGoals+1);
            pthread_mutex_unlock(&printLock);   
        }
    }

    return NULL;
}

int main()
{
    srand(time(0));
    scanf("%d %d %d",&H, &A, &N);
    sem_init(&hSem,0,H);
    sem_init(&aSem,0,A);
    sem_init(&nSem,0,N);

    scanf("%d",&spectatingTime);
    scanf("%d", &numGroups);

    groupSems = (sem_t*)malloc(sizeof(sem_t)*numGroups);
    allPersons = (Person***)malloc(sizeof(Person**)*numGroups);
    groupThreads = (pthread_t*)malloc(sizeof(pthread_mutex_t)*numGroups);
    personThreads = (pthread_t**)malloc(sizeof(pthread_mutex_t*)*numGroups);

    numPerGroup = (int*)malloc(sizeof(int)*numGroups);
    
    for(int i=0;i<numGroups;i++)
    {
        scanf("%d",&(numPerGroup[i]));
        allPersons[i] = (Person**)malloc(sizeof(Person*)*numPerGroup[i]);
        personThreads[i] = (pthread_t*)malloc(sizeof(pthread_mutex_t)*numPerGroup[i]);

        sem_init(&(groupSems[i]),0,0);

        for(int j=0;j<numPerGroup[i];j++)
        {
            allPersons[i][j] = (Person*)malloc(sizeof(Person));
            pthread_mutex_init(&(allPersons[i][j]->personLock),NULL);
            allPersons[i][j]->group = i;
            allPersons[i][j]->state = 0;
            allPersons[i][j]->seatAllocated = -1;
            scanf("%s",allPersons[i][j]->name);
            char c;
            scanf(" %c",&c);
            if(c == 'H')
                allPersons[i][j]->team = 0;
            else if(c == 'N')
                allPersons[i][j]->team = 1;
            else
                allPersons[i][j]->team = 2;
            scanf("%d %d %d",&(allPersons[i][j]->arrivalTime),&(allPersons[i][j]->patience),&(allPersons[i][j]->numGoals));
            sem_init(&(allPersons[i][j]->goalSem),0,0);
            pthread_create(&(personThreads[i][j]), NULL, personRoutine, allPersons[i][j]);
        }
        int* groupNum = (int*)malloc(sizeof(int));
        *groupNum = i;
        pthread_create(&(groupThreads[i]),NULL,groupRoutine,groupNum);
    }

    scanf("%d",&numGoalChances);

    allGoals = (GoalChance**)malloc(sizeof(GoalChance*)*numGoalChances);
    goalThread = (pthread_t*)malloc(sizeof(pthread_t)*numGoalChances);

    for(int i=0;i<numGoalChances;i++)
    {
        allGoals[i] = (GoalChance*)malloc(sizeof(GoalChance));
        char c;
        scanf(" %c",&c);
        if(c == 'H')
            allGoals[i]->team = 2;
        else
            allGoals[i]->team = 0;
        scanf("%d %f",&(allGoals[i]->timeElapsed),&(allGoals[i]->probability));
        pthread_create(&(goalThread[i]),NULL,goalRoutine,allGoals[i]);
    }

    for(int i=0;i<numGoalChances;i++)
        pthread_join(goalThread[i], NULL);

    for(int i=0;i<numGroups;i++)
        for(int j=0;j<numPerGroup[i];j++)
            pthread_join(personThreads[i][j], NULL);

    for(int i=0;i<numGroups;i++)
        pthread_join(groupThreads[i],NULL);
    
    // for(int i=0;i<numGroups;i++)
    // {
    //     printf("%d\n",numPerGroup[i]);
    //     for(int j=0;j<numPerGroup[i];j++)
    //     {
    //         printf("%s %d %d %d %d\n",allPersons[i][j]->name,allPersons[i][j]->team,
    //         allPersons[i][j]->arrivalTime,allPersons[i][j]->patience,allPersons[i][j]->numGoals);
    //     }
    // }

    return 0;
}
