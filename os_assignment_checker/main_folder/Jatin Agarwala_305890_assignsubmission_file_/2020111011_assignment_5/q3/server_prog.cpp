#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <queue>
#include <vector>
#include <sstream>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
int MAX_CLIENTS = 10;
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cout << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent <= 0)
        cout << RED "Send failed of " << s.c_str() << RESET << endl;
    else
        cout << GREEN "Send succesful of "<< s.c_str()<< RESET << endl;

    return bytes_sent;
}

void handle_connection(int client_socket_fd);

///////////////////////////////

queue<int> Clients;
pthread_mutex_t clientLock;
pthread_cond_t clientWait;

char* dictionary[101];
pthread_mutex_t keyLocks[101];

// might be better to get locks before calling the function, and releasing only after sending reply to client

int insertKey(int key, char value[])
{
    if(dictionary[key] != NULL)
    {
        pthread_mutex_unlock(&keyLocks[key]);
        return 0;
    }
    dictionary[key] = (char*)malloc(strlen(value) + 1);
    strcpy(dictionary[key], value);
    return 1;
}

int deleteKey(int key)
{
    if(dictionary[key] == NULL)
    {
        pthread_mutex_unlock(&keyLocks[key]);
        return 0;
    }
    free(dictionary[key]);
    dictionary[key] = NULL;
    return 1;   
}

int updateKey(int key, char* value)
{
    if(dictionary[key] == NULL)
    {
        pthread_mutex_unlock(&keyLocks[key]);
        return 0;
    }
    free(dictionary[key]);
    dictionary[key] = (char*)malloc(strlen(value) + 1);
    strcpy(dictionary[key], value);
    return 1;    
}

char* concatKey(int key1, int key2)
{
    if(dictionary[key1] == NULL || dictionary[key2] == NULL)
    {
        pthread_mutex_unlock(&keyLocks[key1]);
        pthread_mutex_unlock(&keyLocks[key2]);
        return NULL;
    }
    char* k12 = (char*)malloc(strlen(dictionary[key1])+strlen(dictionary[key2])+1);
    char* k21 = (char*)malloc(strlen(dictionary[key1])+strlen(dictionary[key2])+1);
    
    strcpy(k12,dictionary[key1]);
    strcpy(k21,dictionary[key2]);
    strcat(k12,dictionary[key2]);
    strcat(k21,dictionary[key1]);

    free(dictionary[key1]);
    free(dictionary[key2]);
    dictionary[key1] = (char*)malloc(strlen(k12)+1);
    dictionary[key2] = (char*)malloc(strlen(k21)+1);
    strcpy(dictionary[key1],k12);
    strcpy(dictionary[key2],k21);

    return dictionary[key2];
}

char* fetchKey(int key)
{
    return dictionary[key];
}

int handleClient(int currClient)
{
    char input[buff_sz] = {0};
    cout << ORANGE "Initiating read from "<<currClient<< RESET << endl;

    int bytes_received = read(currClient, input, buff_sz - 1);
    if (bytes_received <= 0)
    {
        cout <<RED "Failed to read data from socket. \n" RESET;
    }
    cout << GREEN "Read succesful from "<<currClient<< RESET << endl;


    vector<string> args;
    stringstream argStream(input);
    string currArg;
    
    string msg_to_send_back = to_string(pthread_self());
    msg_to_send_back += ":";

    while(getline(argStream,currArg,' '))
    {
        args.pb(currArg);
    }
    int numArgs = args.size();

    if(numArgs == 3)    // delete, fetch
    {
        pthread_mutex_lock(&keyLocks[stoi(args[2])]);
        if(args[1] == "delete")
        {
            if(deleteKey(stoi(args[2])))
                msg_to_send_back+="Deletion successful";
            else
                msg_to_send_back+="No such key exists";
        }
        else if(args[1] == "fetch")
        {
            char* retVal = fetchKey(stoi(args[2]));
            if(retVal == NULL)
                msg_to_send_back+="Key does not exist";
            else
                msg_to_send_back+=retVal;
        }
        else
        {
            cout << "Incorrect arguments\n";
            msg_to_send_back += "Incorrect arguments";
        }
        pthread_mutex_unlock(&keyLocks[stoi(args[2])]);
    }
    else if(numArgs == 4)   // insert, update, concat
    {
        pthread_mutex_lock(&keyLocks[stoi(args[2])]);
        if(args[1] == "insert")
        {
            char arg3[buff_sz] = {0};
            strcpy(arg3,args[3].c_str());
            if(insertKey(stoi(args[2]),arg3))
                msg_to_send_back += "Insertion successful";
            else
                msg_to_send_back += "Key already exists";
        }
        else if(args[1] == "update")
        {
            char arg3[buff_sz];
            strcpy(arg3,args[3].c_str());
            if(updateKey(stoi(args[2]),arg3))
                msg_to_send_back += args[3];
            else
                msg_to_send_back += "Key does not exist";
        }
        else if(args[1] == "concat")
        {
            pthread_mutex_lock(&keyLocks[stoi(args[3])]);
            char* retVal = concatKey(stoi(args[2]),stoi(args[3]));
            if(retVal)
                msg_to_send_back += retVal;
            else
                msg_to_send_back += "Concat failed as at least one of the keys does not exist";
            pthread_mutex_unlock(&keyLocks[stoi(args[3])]);
        }
        else
        {
            cout << "Incorrect arguments\n";
            msg_to_send_back += "Incorrect arguments";
        }
        pthread_mutex_unlock(&keyLocks[stoi(args[2])]);
    }
    else
    {
        cout << "Incorrect number of arguments"<<"\n";
        msg_to_send_back += "Incorrect number of arguments";       
    }

    cout << ORANGE "Initiating send of "<< msg_to_send_back<< RESET << endl;
    send_string_on_socket(currClient,msg_to_send_back);

    close(currClient);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return 1;
}

void getClient()
{
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            printf( RED"ERROR while accept() system call occurred in SERVER" RESET);
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        
        // handle_connection(client_socket_fd);
        pthread_mutex_lock(&clientLock);
        Clients.push(client_socket_fd);
        pthread_mutex_unlock(&clientLock);
        pthread_cond_broadcast(&clientWait);
    }    
}

void* threadRoutine(void* arg)
{
    while(true)
    {
        pthread_mutex_lock(&clientLock);
        while(Clients.empty())
        {
            pthread_cond_wait(&clientWait,&clientLock);
        }
        int currClient = Clients.front();
        Clients.pop();
        pthread_mutex_unlock(&clientLock);
        // if(handleClient(currClient) == -1)
        //     cout << "Server failed for client " << currClient << '\n';
        handleClient(currClient);
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    if(argc != 2)
    {
        perror(RED "Incorrect number of arguements\n" RESET);
        return -1;
    }

    for(int i=0;i<=100;i++)
    {
        dictionary[i] = NULL;
        pthread_mutex_init(&keyLocks[i],NULL);
    }
    pthread_mutex_init(&clientLock,NULL);
    pthread_cond_init(&clientWait, NULL);

    int numThreads = atoi(argv[1]);
    MAX_CLIENTS = numThreads+1;
    pthread_t threads[numThreads];

    for(int i=0;i<numThreads;i++)
        pthread_create(&threads[i],NULL,threadRoutine,NULL);
    getClient();

    return 0;
}

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);        // cmd holds input string
        ret_val = received_num;                                                             // ret_val holds no. of bytes
        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Parsing of cmd and calling of functions here
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }

        char input[buff_sz] = {0};
        strcpy(input,cmd.c_str());

        vector<string> args;
        stringstream argStream(input);
        string currArg;

        while(getline(argStream,currArg,' '))
        {
            args.pb(currArg);
        }
        int numArgs = args.size();

        string msg_to_send_back = to_string(pthread_self());
        msg_to_send_back += ":";

        if(numArgs == 3)    // delete, fetch
        {
            pthread_mutex_lock(&keyLocks[stoi(args[2])]);
            if(args[1] == "delete")
            {
                if(deleteKey(stoi(args[2])))
                    msg_to_send_back+="Deletion successful";
                else
                    msg_to_send_back+="No such key exists";
            }
            else if(args[1] == "fetch")
            {
                char* retVal = fetchKey(stoi(args[2]));
                if(retVal == NULL)
                    msg_to_send_back+="Key does not exist";
                else
                    msg_to_send_back+=retVal;
            }
            else
            {
                cout << "Incorrect arguments\n";
                msg_to_send_back += "Incorrect arguments";
            }
            pthread_mutex_unlock(&keyLocks[stoi(args[2])]);
        }
        else if(numArgs == 4)   // insert, update, concat
        {
            pthread_mutex_lock(&keyLocks[stoi(args[2])]);
            if(args[1] == "insert")
            {
                char arg3[buff_sz] = {0};
                strcpy(arg3,args[3].c_str());
                if(insertKey(stoi(args[2]),arg3))
                    msg_to_send_back += "Insertion successful";
                else
                    msg_to_send_back += "Key already exists";
            }
            else if(args[1] == "update")
            {
                char arg3[buff_sz];
                strcpy(arg3,args[3].c_str());
                if(updateKey(stoi(args[2]),arg3))
                    msg_to_send_back += args[3];
                else
                    msg_to_send_back += "Key does not exist";
            }
            else if(args[1] == "concat")
            {
                pthread_mutex_lock(&keyLocks[stoi(args[3])]);
                char* retVal = concatKey(stoi(args[2]),stoi(args[3]));
                if(retVal)
                    msg_to_send_back += retVal;
                else
                    msg_to_send_back += "Concat failed as at least one of the keys does not exist";
                pthread_mutex_unlock(&keyLocks[stoi(args[3])]);
            }
            else
            {
                cout << "Incorrect arguments\n";
                msg_to_send_back += "Incorrect arguments";
            }
            pthread_mutex_unlock(&keyLocks[stoi(args[2])]);
        }
        else
        {
            cout << "Incorrect number of arguments"<<"\n";
            msg_to_send_back += "Incorrect number of arguments";       
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}
