#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sstream>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

pthread_mutex_t printLock;

pthread_t* clientThreads;

typedef struct indexInput IndexInput;
struct indexInput
{
    int index;
    string input;
};

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        // cout << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << CYAN "We are sending "  << s<< RESET<< endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);

    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void* begin_process(void* arg)
{
    IndexInput* currInput = (IndexInput*)arg;
    string msg_to_send_back = currInput->input;

    char input[buff_sz] = {0};
    strcpy(input,msg_to_send_back.c_str());

    stringstream argStream(input);
    string currArg;
    
    int check = 0;
    while(getline(argStream,currArg,' '))
    {
        if(check == 0)
        {
            check = 1;
            sleep(stoi(currArg));
            break;
        }
    }

    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    // printf(BGRN "%d: Connected to server\n" ANSI_RESET, currInput->index);

    // cout << "Connection to server successful" << endl;
    
    // cout << msg_to_send_back << endl;
    // cout << ORANGE <<currInput->index<<": Initiating send from "<< to_string(pthread_self()) <<RESET << endl;

    send_string_on_socket(socket_fd, msg_to_send_back);
    // if(send_string_on_socket(socket_fd, msg_to_send_back) > 0)
    //     cout << GREEN <<currInput->index<<": Send succesful from "<< to_string(pthread_self()) <<RESET << endl;
    // else
    //     cout << RED <<currInput->index<<": Send failed from "<< to_string(pthread_self()) <<RESET << endl;;

    int num_bytes_read;
    string output_msg;

    // cout << ORANGE <<currInput->index<<": Initiating read from "<< to_string(pthread_self()) <<RESET << endl;

    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    // if(num_bytes_read <= 0)
    //     cout << RED<<currInput->index<<": Read failed from "<< to_string(pthread_self()) << RESET << endl;
    // else
    //     cout << GREEN <<currInput->index<<": Read succesful from "<< to_string(pthread_self()) <<RESET << endl;

    pthread_mutex_lock(&printLock);
    cout << currInput->index << ":" << output_msg << endl;
    pthread_mutex_unlock(&printLock);

    // cout << "====" << endl;
    
    // msg_to_send_back = "exit";
    // send_string_on_socket(socket_fd, msg_to_send_back);
    // if(send_string_on_socket(socket_fd, msg_to_send_back) > 0)
    //     cout << GREEN <<currInput->index<<": Send succesful from "<< to_string(pthread_self()) <<RESET << endl;
    // else
    //     cout << RED <<currInput->index<<": Send failed from "<< to_string(pthread_self()) <<RESET << endl;;
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&printLock,NULL);
    string num;
    getline(cin,num);
    int numThreads = stoi(num);

    clientThreads = (pthread_t*)malloc(sizeof(pthread_t)*(numThreads + 1));
    
    IndexInput inputLines[numThreads];
    for(int i=0;i<numThreads;i++)
    {
        inputLines[i].index = i;
        getline(cin, inputLines[i].input);
        
        pthread_create(&clientThreads[i],NULL,begin_process,&inputLines[i]);
    }

    for(int i=0;i<numThreads;i++)
        pthread_join(clientThreads[i],NULL);

    return 0;
}