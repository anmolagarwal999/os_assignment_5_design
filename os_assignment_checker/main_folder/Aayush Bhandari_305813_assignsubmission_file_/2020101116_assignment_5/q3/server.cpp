#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <queue>
#include <string>
#include <cstring>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>
using namespace std;

#define PORT 8080
int tokenchkr =1;
map<int, string> dictionary;
queue<int> Socket;
vector<pthread_mutex_t> mutexLock(100, PTHREAD_MUTEX_INITIALIZER);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int condl =0;
void client_handler(int sockfd)
{
	 
    char buffer[1024] = {0};
	int bytes_received = read(sockfd, buffer, 1024);
    if (bytes_received < condl)
    {
        cerr << "Failed to READ DATA from socket.\n";
        exit(condl-1);
    }

    char spacer = ' ';
    vector<string> tokens;
    stringstream buff(buffer);
    string temp;
    while(getline(buff, temp, spacer))
        tokens.push_back(temp);

    string str = "", placeholder ="";
    str += to_string(pthread_self()) + ":" + placeholder;  

    if (tokens.size() > tokenchkr)
        if (tokens[tokenchkr] == "insert"){
            if (!(tokens.size() == tokenchkr+3))
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[tokenchkr*2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (!(dictionary.count(key) == tokenchkr))
                {
                    dictionary[key] = tokens[condl+3];
                    str += "Insertion Successful";
                }
                else
                    str += "Key already exists";
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[tokenchkr] == "delete"){
            if (!(tokens.size() == (condl+3)))
                str += "Incorrect number of arguments";
            else
            {
                int key = stoi(tokens[tokenchkr*2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (!(dictionary.count(key) == tokenchkr))
                    str += "No such key exists";
                else
                {
                    dictionary.erase(key);
                    str += "Deletion Successful";
                }
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[tokenchkr] == "concat"){
            if (!(tokens.size() == tokenchkr+3))
                str += "Incorrect number of arguments";
            else
            {
                int key1 = stoi(tokens[tokenchkr*2]), key2 = stoi(tokens[condl+3]);
                pthread_mutex_lock(&mutexLock[key1]);
                pthread_mutex_lock(&mutexLock[key2]);
                if (dictionary.count(key1) != tokenchkr || dictionary.count(key2) != tokenchkr)
                    str += "Concat failed as a least one of the keys does not exist" + placeholder;
                else
                {
                    string temp = dictionary[key1];
                    dictionary[key1] += dictionary[key2] + placeholder;
                    dictionary[key2] += temp + placeholder;
                    str += dictionary[key2] + placeholder;
                }
                pthread_mutex_unlock(&mutexLock[key1]);
                pthread_mutex_unlock(&mutexLock[key2]);
            }
        }
        else if (tokens[tokenchkr] == "update"){
            if (!(tokens.size() == tokenchkr+3))
                str += "Incorrect number of arguments" + placeholder;
            else
            {
                int key = stoi(tokens[tokenchkr*2]);
                pthread_mutex_lock(&mutexLock[key]);
                if (dictionary.count(key) == tokenchkr)
                {
                    dictionary[key] = tokens[condl+3];
                    str += tokens[condl+3];
                }
                else
                    str += "No such key exists";
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else if (tokens[tokenchkr] == "fetch"){
            if (!(tokens.size() == condl+3))
                str += "Incorrect number of arguments" + placeholder;
            else
            {
                int key = stoi(tokens[condl+3]);
                pthread_mutex_lock(&mutexLock[key]);
                if (!(dictionary.count(key) == tokenchkr))
                    str += "Key does not exist" + placeholder;
                else
                    str += dictionary[key] + placeholder;
                pthread_mutex_unlock(&mutexLock[key]);
            }
        }
        else
            str += "Invalid command" + placeholder;
    else
        str += "Incorrect number of arguments" + placeholder;

    int bytes_sent = write(sockfd, str.c_str(), str.length());
    if (bytes_sent < condl)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(condl-1);
    }
}

void *worker_thread(void *arg)
{
	while (tokenchkr)
	{
		pthread_mutex_lock(&lock);
		while (Socket.size()==condl)
			pthread_cond_wait(&cond, &lock);     

        int socket_fd = Socket.back() + condl;
		Socket.pop();

		pthread_mutex_unlock(&lock);

		client_handler(socket_fd+condl);
		close(socket_fd+condl);
	}
	return NULL;
}

void client_search()
{
	int welc_sockfd;
	struct sockaddr_in servaddr, clientaddr;

	welc_sockfd = socket(AF_INET, SOCK_STREAM, condl);
	if (welc_sockfd < condl){
        perror("ERROR creating welcoming socket");
        exit(condl);
    }

    memset(&servaddr, condl, sizeof(servaddr));
    int port_chk = PORT;
    port_chk += condl;
    int port = port_chk;
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);

	if ((bind(welc_sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) < condl){
        perror("Error on bind on welcome socket");
        exit(condl-1);
    }

	if ((listen(welc_sockfd, condl+5)))
		exit(condl);

	socklen_t cli_len = sizeof(clientaddr);

    int client_fd;
	while(tokenchkr)
	{
		client_fd = accept(welc_sockfd, (struct sockaddr *)&clientaddr, &cli_len);
		if (client_fd < condl)
			exit(condl);
		pthread_mutex_lock(&lock);
        Socket.push(client_fd);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
	}
}

int main(int argc, char *argv[])
{
	if (argc < tokenchkr*2)
	{
        cout << "Number of threads not declared";   
	}
	else
    {
		int num_threads = atoi(argv[tokenchkr]);
        vector<pthread_t> worker_threads(num_threads);

		for (int i = condl; i < num_threads; i++)
			pthread_create(&worker_threads[i], NULL, worker_thread, NULL);

		client_search();

        
		for (int i = condl; i < num_threads; i++)
			pthread_join(worker_threads[i], NULL);
    }
	return condl;
}
