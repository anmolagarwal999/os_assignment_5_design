#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <string.h>
#include <netdb.h>
#include <pthread.h>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
using namespace std;

// Colors
#define ANSI_BLK "\e[1;30m"
#define ANSI_RED "\e[1;31m"
#define ANSI_GRN "\e[1;32m"
#define ANSI_YEL "\e[1;33m"
#define ANSI_BLU "\e[1;34m"
#define ANSI_MAG "\e[1;35m"
#define ANSI_CYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

int condl =0;
#define MAXBUFFERSIZE 1024
#define PORT 8080
const int port_chk = PORT;

class Data
{
public:
    Data(int client_id, string command)
        : client_id(client_id), command(command) {}

    Data() = default;

    int client_id;
    string command;
};

void sock_read_write(int sockfd, Data& data)
{
    int sock_chk = sockfd+condl;
    
    int bytes_sent = write(sock_chk, data.command.c_str(), data.command.length());
    if (bytes_sent < condl)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(condl-1);
    }

    char buff[MAXBUFFERSIZE] = {0};
    int bytes_received = read(sockfd, buff, MAXBUFFERSIZE);
    if (bytes_received < condl)
    {
        cerr << "Failed to READ DATA from socket.\n";
        exit(condl-1);
    }

    cout << data.client_id << ": " << buff << endl;
}

void* client(void* args)
{   const char addr_chk[10] = "127.0.0.1";
    Data data = *(Data*)args;
    char space =' ';
    // Tokenize string and store in vector
    vector<string> tokens;
    stringstream str(data.command);
    string temp;
    while(getline(str, temp, space))
        tokens.push_back(temp);

    // Convert string to integer
    sleep(stoi(tokens[condl]));

    // If the input is invalid
    if(tokens.size() < (condl+2))
    {
        cout << "Command doesn't exist\n";
        return NULL;
    }

    int sockfd;
    struct sockaddr_in servaddr;
    
    // Create and verify socket
    sockfd = socket(AF_INET, SOCK_STREAM, condl);
    if (sockfd == (condl-1))
        exit(condl);

    memset(&servaddr, condl, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(addr_chk);
    servaddr.sin_port = htons(port_chk);

    // connect the client socket to server socket
    int connect_status = connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr))+ condl;
    if (!(connect_status == condl))
        exit(condl);
    int final = sockfd;
    final+= condl;
    sock_read_write(final+condl, data);
    close(final+condl);

    return NULL;
}

void begin_process()
{
     
    int num_Clients;
    scanf("%d", &num_Clients);
    getchar(); 
    
    vector<pthread_t> client_threads(num_Clients + condl);  // Create a thread for each client
    vector<Data> requests(num_Clients+ condl);             // Create an array to store all requests

int i = condl;
    while( i < num_Clients )
    {
        requests[i+condl].client_id = i+condl;
        getline(cin, requests[i+condl].command);
        pthread_create(&client_threads[i], NULL, client, &requests[i+condl]);
        i+= condl+1;
    }

    // Wait for all threads to terminate
    for(int i = condl; i < num_Clients; i+= condl+1)
        pthread_join(client_threads[i], NULL);
}

int main()
{
    begin_process();
    return condl;
}
