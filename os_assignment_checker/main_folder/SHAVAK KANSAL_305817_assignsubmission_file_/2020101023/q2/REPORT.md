# Data Structures

## Struct Person 

> name - Name of person 
>
> type - type of fan H,N,A
>
> time_reached - time when person reaches stadium 
>
> patience_time - how long person waits
>
> goal_limit - how many goals a person can tolerate

## Struct Group 

> id  - id of group 
>
> size - number of people in the group 
>
> members - list of members

## Struct locks_stuff

locks_stuff contains all the variables used for synchronization and the global state variables such as goals , capacities.

> capacity[3] - Semaphore counter to keep track of the the seats left in each zone
>
> fans_type[3] - Each person waits on his `fan_type` conditional variable.
>
> fans_type_mutex[3] - Locks associated with fans_type conditional variables
>
>  goal_cond - conditional variable which is signaled to indicate when a goal occurs for the scenario in which a person might leave due to too much goals from opposite team
>
>  goal_cond_mutex - lock associated with goal_cond
>
> goals_count[3] - counter for keeping track of goals, the goal counter for neutral team is just for ease of code no significance
>
> goals_count_mutex - lock associated with goals_count to make the counter thread safe



# Working of threads

## Spectator Thread

This thread emulates an individual stimulator, firstly the thread uses the `gettimeofday` function to initialize the object now with the current time, then we increment the second field in the object with the patience time associated with the spectator, in order to simulate the behavior of the observer wherein he/she leaves if he/she doesn't get a ticket, use use `pthread_cond_timewait` which will wait on the conditional variable until the time specified in the time variable `timeToWate`, this function takes in absolute time rather than relative time. 

If the conditional variable doesn't get signaled in time then rt is set to 110, in indicate timeout in which case appropriate message is displayed.

Since the thread has woken up it indicates there is a seat available, in which case depending on the type of the spectator it waits on the appropriate capacities semaphore.

In case the spectator doesn't get a seat it goes back to waiting on the conditional variable `pthread_cond_timewait`.

Now to simulate the fact the spectator leaves after a fixed time or before if the enemy team gets enough goals we use `pthread_cond_timewait` again similarly as above on the conditional variable `sync_stuff.goal_cond`, every time there  is a goal the thread wakes up and checks if enough goals have been scored if enough goals are scored we exit the thread to simulate the behavior of leaving the stadium, if enough goals haven't been scored the thread goes back to waiting on the conditional variable `sync_stuff.goal_cond`.

## Zone thread 

Zone thread simulates the process of selling tickets for the respective zone, depending on  the type of zone indicated by the if-else-if-else code segment, unless every spectator has left (indicated by the variable done).

## Goal Thread

Goal thread is created for every goal chance, it uses the rand function to simulate chance and then updates the `goals_count` array, using the lock associated with it to keep it thread safe, it also signal broadcasts on the goal conditional variable to indicate a goal to every thread. 