## OS Assignment 5 - Concurrency, Parallelism and Networking

#### Sneha Raghava  Raju 
#### Roll Number: 2020101125

### Q3 - Multithreaded client and server

There are two file `client.cpp` and `server.cpp`

The `client.cpp` program has four main functions:
- string read_string_from_socket(int fd)
- int send_string_on_socket(int fd, const string &s)
- void *get_socket_fd(void *ind)
- int main(int argc, char *argv[])

In the main function the number of requests is read. And then each of the requests are read and put into a struct array called requests. Then pthread_create is used to create the threads and finally pthread_join to join them.

The get_socket function creates a socket and gets in file descriptor. It then connects to the server using the connect() function.

The program then sleeps till the request execution time, that was specified by the user.

Then using a mutex lock and the send_string_on_socket function the command entered by the user is sent to the server.

The `server.cpp` program has four main functions:
- string read_string_from_socket(int fd)
- int send_string_on_socket(int fd, const string &s)
- void handle_connection(int client_socket_fd, int indx)
- void *thread_func(void *arg) 
- int main(int argc, char *argv[])

In the main function the number of worker threads is read. And then a thread pool with that many threads is created. The array of dictionary nodes is initialized; the nodes hold the key value pairs that are entered by the user.

Then a socket is created, binded and then we listen for connection requests. We accept requests using the accept() function. The requests are pushed into a queue. 

The handle_connection function calls read_string_from_socket to get the command. It then does the appropiate steps depending on if the command is 'insert', 'delete', 'update', etc.

An output message is sent on the socket using send_string_on_socket().


