#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstdio>
#include <sstream>

#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;

pthread_mutex_t terminal_lock = PTHREAD_MUTEX_INITIALIZER;

// client request struct
struct request
{

    int time;           //time in sec at which to execute the request
    string command;     //input command (eg: 'insert')
    pthread_t thread;
    int fd;             //file descriptor
    int index;          //request index
    pthread_mutex_t client_lock;

};
//typedef struct c_request c_request;
vector <request> requests;

string read_string_from_socket(int fd)
{
   std::string output;
    output.resize(buff_sz);

    int bytes_received = read(fd, &output[0], buff_sz - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = '\0';
    output.resize(bytes_received);

    return output;
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

void *get_socket_fd(void *ind)
{
    struct sockaddr_in server_obj;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }

    int index = *(int*) ind;
    requests[index].fd = socket_fd;

    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order
    
    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //sleep till 'time' when the request is to be executed
    int sleep_time = requests[index].time;
    sleep(sleep_time);

    pthread_mutex_lock(&requests[index].client_lock);
    if(send_string_on_socket(requests[index].fd, requests[index].command)<0) 
    {
        fprintf(stderr, BRED "Error: Could not send message" ANSI_RESET "\n"); 
        return NULL;
    }
    string str = read_string_from_socket(requests[index].fd);
    pthread_mutex_unlock(&requests[index].client_lock);    

    pthread_mutex_lock(&terminal_lock);
    printf("%d:%s\n", index, str.c_str());
    pthread_mutex_unlock(&terminal_lock);

    // //part;
    // // printf(BGRN "Connected to server\n" ANSI_RESET);
    // // part;
    // return socket_fd;
    return NULL;
}

////////////////////////////////////////////////////////

// void begin_process()
// {
//     struct sockaddr_in server_obj;
//     int socket_fd = get_socket_fd(&server_obj);


//     cout << "Connection to server successful" << endl;
    
//     while (true)
//     {
//         string to_send;
//         cout << "Enter msg: ";
//         getline(cin, to_send);
//         send_string_on_socket(socket_fd, to_send);
//         int num_bytes_read;
//         string output_msg;
//         //tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
//         cout << "Received: " << output_msg << endl;
//         cout << "====" << endl;
//     }
//     // part;
// }

int main(int argc, char *argv[])
{
    int num_of_requests = 0;

    string num_of_requests_str;
    getline(cin, num_of_requests_str);

    num_of_requests = stoi(num_of_requests_str);
    
    for(int i = 0; i < num_of_requests; i++)
    {
        string input_cmd;
        getline(cin, input_cmd);

        struct request req;

        pthread_mutex_init(&req.client_lock, NULL);

        req.index = i;

        istringstream parsed_cmd(input_cmd);

        string time;
        parsed_cmd >> time;

        req.time = stoi(time);

        string cmd = input_cmd.substr(input_cmd.find(' ') + 1, input_cmd.length());
        req.command = cmd;

        requests.push_back(req);
        //requests[i] = req;
    }

    int x = 0;

    while(x < num_of_requests)
    {
        int *indx = (int*)malloc(sizeof(int)); 
        *indx = x;
        pthread_create(&requests[x].thread, NULL, get_socket_fd, (void *)indx);
        //free(indx);
        x++;
    }

    x = 0;
    while(x < num_of_requests)
    {
        pthread_join(requests[x].thread, NULL);
        x++;
    }

    //int i, j, k, t, n;
    //begin_process();
    
    return 0;
}