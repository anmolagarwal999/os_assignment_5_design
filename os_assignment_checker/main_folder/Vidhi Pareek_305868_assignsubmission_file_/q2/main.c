#include "headers.h"
void checkinput()
{
    printf("%d %d %d\n", cap_a, cap_h, cap_n);
    for (int i = 0; i < 3; i++)
    {
        printf("%c %d\n", all_zones[i].zone, all_zones[i].capacity);
    }
    for (int i = 0; i < total_people; i++)
    {
        printf("%s %c %d %d %d %d %d %d %d\n", &all_people[i].name[0], all_people[i].team, all_people[i].arrive, all_people[i].left, all_people[i].entry_time, all_people[i].seated, all_people[i].cur_seat, all_people[i].patience_time, all_people[i].goals);
    }
    for (int i = 0; i < total_goals; i++)
    {
        printf("%c %d %f\n", all_goals[i].team, all_goals[i].time, all_goals[i].prob);
    }
    // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
}
int main(void)
{
    scanf("%d %d %d %d", &cap_h, &cap_a, &cap_n, &spec_time);
    for (int i = 0; i < 3; i++)
    {
        if (i == 0)
        {
            all_zones[i].capacity = cap_h;
            all_zones[i].zone = 'H';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
        else if (i == 1)
        {
            all_zones[i].capacity = cap_a;
            all_zones[i].zone = 'A';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
        else if (i == 2)
        {
            all_zones[i].capacity = cap_n;
            all_zones[i].zone = 'N';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
    }
    int num_grps, num_ppl, i = 0;
    scanf("%d", &num_grps);
    while (num_grps--)
    {
        int num_ppl;
        scanf("%d", &num_ppl);
        total_people += num_ppl;
        while (num_ppl--)
        {
            scanf("%s %c %d %d %d", &all_people[i].name[0], &all_people[i].team, &all_people[i].entry_time, &all_people[i].patience_time, &all_people[i].goals);
            all_people[i].arrive = 0;
            all_people[i].left = 0;
            all_people[i].seated = 0;
            all_people[i].cur_seat = -1;
            pthread_mutex_init(&all_people[i].people_mutex, NULL);
            pthread_cond_init(&all_people[i].people_cond, NULL);
            // if (i == 2)
            //     printf("%s %c %d %d %d %d %d %d %d\n", &all_people[i].name[0], all_people[i].team, all_people[i].arrive, all_people[i].left, all_people[i].entry_time, all_people[i].seated, all_people[i].cur_seat, all_people[i].patience_time, all_people[i].goals);
            i++;
        }
        // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
    }
    count_people = total_people;
    scanf("%d", &total_goals);
    int goals = total_goals;
    i = 0;
    while (goals--)
    {
        scanf(" %c %d %f", &all_goals[i].team, &all_goals[i].time, &all_goals[i].prob);
        i++;
    }
    // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
    // checkinput();
    // return 0;
    pthread_mutex_init(&goal_mutex, NULL);
    pthread_cond_init(&goal_cond, NULL);
    pthread_t all_people_threads[total_people];
    start = time(NULL);
    for (int i = 0; i < total_people; i++)
    {
        pthread_create(&all_people_threads[i], NULL, people_init, &all_people[i]);
    }
    pthread_t all_zones_threads[3];
    for (int i = 0; i < 3; i++)
    {
        pthread_create(&all_zones_threads[i], NULL, zone_init, &all_zones[i]);
    }
    pthread_t goal;
    pthread_create(&goal, NULL, goal_init, all_goals);
    pthread_t goal_update;
    pthread_create(&goal_update, NULL, update_goal_info, NULL);
    pthread_join(goal, NULL);
    pthread_join(goal_update, NULL);
    for (int i = 0; i < total_people; i++)
    {
        pthread_join(all_people_threads[i], NULL);
    }
    for (int i = 0; i < 3; i++)
    {
        pthread_join(all_zones_threads[i], NULL);
    }
    return 0;
}