#ifndef HEADERS_H
#define HEADERS_H
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <semaphore.h>
#include "people.h"
#include "zone.h"
#include "goal.h"
#define H 0
#define A 1
#define N 2
int cap_a , cap_h , cap_n , spec_time;
pthread_mutex_t total_people_mutex;
time_t start;
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
#endif