#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
pthread_cond_t c = PTHREAD_COND_INITIALIZER;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
time_t start;
int goals[2];
int goals_num;
typedef struct person
{
    char name[100];
    char fan;
    int reach_time;
    int patience_time;
    int num_goals;
} person;
typedef struct goal
{
    char team;
    int time;
    float prob;
} teams;
void *people_alot(void *input)
{
    person *p = (person *)(input);
    sleep(p->reach_time - (time(NULL) - start));
    pthread_mutex_lock(&m);
    printf(ANSI_COLOR_RED "%s %d REACHED" ANSI_COLOR_RESET "\n", p->name, time(NULL) - start);
    pthread_mutex_unlock(&m);
}
void * goal_count(void * input)
{
    teams* t = (teams*)(input);
    for(int i=0 ; i<goals_num ; i++)
    {
        t->time
    }
}
int main()
{
    int cap_a, cap_h, cap_n, spec_time;
    scanf("%d %d %d %d", &cap_h, &cap_a, &cap_n, &spec_time);
    int num_grps, num_ppl;
    person *prsn = NULL;
    teams *team = NULL;
    scanf("%d", &num_grps);
    int ppl = 0, i = 0;
    while (num_grps--)
    {
        int num_ppl;
        scanf("%d", &num_ppl);
        ppl += num_ppl;
        if (prsn == NULL)
        {
            prsn = (person *)(calloc(ppl, sizeof(person)));
        }
        else
        {
            prsn = (person *)(realloc(prsn, ppl));
        }
        while (num_ppl--)
        {
            scanf("%s %c %d %d %d", &prsn[i].name, &prsn[i].fan, &prsn[i].reach_time, &prsn[i].patience_time, &prsn[i].num_goals);
            i++;
        }
    }
    int num_goals;
    i = 0;
    scanf("%d", &num_goals);
    goals_num = num_goals;
    team = (teams *)(calloc(num_goals, sizeof(teams)));
    // printf("%d\n",num_goals);
    while (num_goals--)
    {
        scanf(" %c %d %f", &team[i].team, &team[i].time, &team[i].prob);
        // printf("%d\n",num_goals);
        i++;
    }
    start = time(NULL);
    pthread_t people_threads[ppl];
    for (int i = 0; i < ppl; i++)
        pthread_create(&people_threads[i], NULL, people_alot, prsn + i);
    pthread_t goal;
    pthread_create(&goal, NULL, goal_count, team);    
    for (int i = 0; i < ppl; i++)
        pthread_join(people_threads[i], NULL);
    pthread_join(goal, NULL);    
    return 0;
}
