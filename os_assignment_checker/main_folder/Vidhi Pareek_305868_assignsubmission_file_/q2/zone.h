#ifndef ZONE_H
#define ZONE_H
typedef struct zone 
{
    char zone;
    int capacity;  
    pthread_mutex_t zone_mutex;
}zone;
zone all_zones[3];
void* zone_init(void*input);
#endif
