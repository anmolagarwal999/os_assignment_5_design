#include "headers.h"
void *goal_init(void *input)
{
    int prev =0;
    goal *g = (goal *)(input);
    for (int i = 0; i < total_goals; i++)
    {
        g=&all_goals[i];
        struct timespec wait;
        clock_gettime(CLOCK_REALTIME, &wait);
        wait.tv_sec += g->time - prev ;
        prev = g->time;
        pthread_mutex_lock(&goal_mutex);
        int rt_val = pthread_cond_timedwait(&goal_cond, &goal_mutex, &wait);
        pthread_mutex_unlock(&goal_mutex);
        float random_value = ((float)rand() * 1.0) / RAND_MAX;
        if (rt_val == ETIMEDOUT)
        {
            //printf("\n\n--%c--\n\n",g->team);
            if (g->team == 'H')
            {
                h_goal++;
                if (random_value <= g->prob)
                {
                    goal_h++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team H scored their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start , h_goal);
                }
                else
                {
                    missed_h++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team H missed the chance to score their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start , h_goal);
                }
            }
            else
            {
                a_goal++;
                if (random_value <= g->prob)
                {
                    goal_a++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team A scored their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start ,a_goal);
                }
                else
                {
                    missed_a++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team A missed the chance to score their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start ,a_goal);
                }
            }
        }
    }
    return NULL;
}

void *update_goal_info(void *input)
{
    while (count_people)
    {
        for (int i = 0; i < total_people; i++)
        {
            if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 1)
            {
                if (all_people[i].team == 'H' && goal_a == all_people[i].goals)
                {
                    pthread_mutex_lock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
                if (all_people[i].team == 'A' && goal_h == all_people[i].goals)
                {
                    pthread_mutex_lock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
        }
    }
    return NULL;
}