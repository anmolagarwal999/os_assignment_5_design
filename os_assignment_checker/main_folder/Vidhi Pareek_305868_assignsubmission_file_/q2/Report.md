# Question 2

## main.c
Takes input , makes thread for
<br>1. Spectators
<br>2. Zones
<br>3. Goals - One thread for updating goal values another for signaling spectator threads about goals.

## Spectator threads
Files associated : people.c , people.h 
<br>Thread begins , sleep halts executing until arrival time .
<br>Using pthread_cond_timedwait , thread waits till patience times. It moves to further code only if seat has been allocated or time has expired. If time expires , spectator leaves and variables are updated.
<br>If spectator stays, another conditional time wait is used for a period of spectating time. If it is signaled when opposite team scores more goals, it leaves.
<br>Otherwise when time slice expires, it leaves.

## Zone threads
Files associated : zone.c , zone.h
<br>Loops through all the people to find out a waiting person for a particular zone. Signal is sent to the thread and further search is restarted.

## Goal threads
<br>One thread loops through all goal possibilities , waits in the middle time using timedwait and updates global goal variables.
<br>Another thread uses the global goal variables to send updation messages to spectator threads.

Note: While printing statements like , "Team Ti has scored their Yth goal" or an opposite statement to this , Y(separately for both teams) has been initiated from one and incremented each time. This would facilitate in knowing which chance had been displayed.