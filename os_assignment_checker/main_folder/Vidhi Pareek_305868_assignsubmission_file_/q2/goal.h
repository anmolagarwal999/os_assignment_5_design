#ifndef GOAL_H
#define GOAL_H
typedef struct goal
{
    char team;
    int time;  
    float prob;
}goal;
goal all_goals[50];
int goal_h , goal_a;
int a_goal , h_goal;
int missed_h , missed_a;
pthread_mutex_t goal_mutex;
pthread_cond_t goal_cond;
int total_goals;
void* goal_init(void*input);
void* update_goal_info(void *input);
#endif
