#ifndef PEOPLE_H
#define PEOPLE_H
typedef struct people 
{
    char name[100];
    char team;
    int arrive;
    int left;
    int entry_time;
    int seated;
    int cur_seat;
    int patience_time;
    int goals; 
    pthread_mutex_t people_mutex;
    pthread_cond_t people_cond;
}people;
people all_people[500];
int total_people , count_people;
void* people_init(void*input);
#endif
