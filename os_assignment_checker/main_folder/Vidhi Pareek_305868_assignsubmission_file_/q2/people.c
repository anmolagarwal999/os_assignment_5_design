#include "headers.h"
void *people_init(void *input)
{
    people *p = (people *)input;
    sleep(p->entry_time);
    printf(ANSI_COLOR_RED "t=%ld : %s has reached the stadium" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name);
    struct timespec timeToWait;
    clock_gettime(CLOCK_REALTIME, &timeToWait);
    timeToWait.tv_sec += p->patience_time;
    pthread_mutex_lock(&p->people_mutex);
    p->arrive=1;
    p->seated=0;
    int rt = pthread_cond_timedwait(&p->people_cond, &p->people_mutex, &timeToWait);
    pthread_mutex_unlock(&p->people_mutex);
    //printf("%s --- %d ---- %d\n",p->name , rt , p->seated);
    if (rt != ETIMEDOUT)
    {
        char zone;
        if (p->cur_seat == H)
        {
            zone = 'H';
        }
        else if (p->cur_seat == A)
        {
            zone = 'A';
        }
        else if (p->cur_seat == N)
        {
            zone = 'N';
        }
        
        printf(ANSI_COLOR_GREEN "t=%ld : %s has got a seat in zone %c" ANSI_COLOR_RESET "\n", time(NULL)-start , p->name, zone);
        clock_gettime(CLOCK_REALTIME, &timeToWait);
        timeToWait.tv_sec += spec_time;
        pthread_mutex_lock(&p->people_mutex);
        int rt2 = pthread_cond_timedwait(&p->people_cond, &p->people_mutex, &timeToWait);
        pthread_mutex_unlock(&p->people_mutex);
        if (rt2 == 0)
        {
            printf(ANSI_COLOR_MAGENTA "t=%ld : %s is leaving due to bad defensive performance of his team" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name);
            pthread_mutex_lock(&total_people_mutex);
            count_people--;
            p->left = 1;
            pthread_mutex_unlock(&total_people_mutex);
            pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
            all_zones[p->cur_seat].capacity++;
            pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);
            printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
            return NULL;
        }
        else if (rt2 == ETIMEDOUT)
        {
            printf(ANSI_COLOR_BLUE "t=%ld : %s watched the match for %d seconds and is leaving" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name, spec_time);
            pthread_mutex_lock(&total_people_mutex);
            count_people--;
            p->left = 1;
            pthread_mutex_unlock(&total_people_mutex);
            
            pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
            //printf("comes 2\n");
            all_zones[p->cur_seat].capacity++;
            pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);
            printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
            return NULL;
        }
        else
        {
            perror("Error");
            return NULL;
        }
    }
    else if (rt == ETIMEDOUT)
    {
        printf(ANSI_COLOR_YELLOW "t=%ld : %s couldn't get a seat" ANSI_COLOR_RESET "\n", time(NULL)-start , p->name);
        pthread_mutex_lock(&total_people_mutex);
        count_people--;
        p->left = 1;
        pthread_mutex_unlock(&total_people_mutex);
        /*
        pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
        printf("comes 2\n");
        all_zones[p->cur_seat].capacity++;
        pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);*/
        printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
        return NULL;
    }
    else
    {
        perror("Error");
        return NULL;
    }
    
    return NULL;
}
