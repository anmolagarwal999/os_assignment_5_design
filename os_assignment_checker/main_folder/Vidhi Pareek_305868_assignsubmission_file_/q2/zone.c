#include "headers.h"
void *zone_init(void *input)
{
    zone *z = (zone *)(input);
    //sleep(1);
    //  printf("%c\n",z->zone);
    while (count_people)
    {
        for (int i = 0; i < total_people; i++)
        {
            // printf("here1");

            // select by team

            if ((z - all_zones) == 1)
            {
                // printf("here2");
                // if (strcmp(all_people[i].name, "Ayush") == 0)
                //     printf("%d %d %d %c\n", all_people[i].arrive , all_people[i].seated , z->capacity , all_people[i].team);
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity != 0 && all_people[i].team != 'H')
                {
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 1;
                    z->capacity--;
                    //printf("t=%ld : %s arrived in zone %d\n", time(NULL) - start , all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
                
            }
            else if ((z - all_zones) == 0)
            {
                // printf("here3");
                // if (strcmp(all_people[i].name, "Rachit") == 0)
                //     printf("%d %d %d %c\n", all_people[i].arrive , all_people[i].seated , z->capacity , all_people[i].team);
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity > 0 && all_people[i].team != 'A')
                {
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    // if (strcmp(all_people[i].name, "Rachit") == 0)
                    //     printf("%s ", all_people[i].name);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 0;
                    z->capacity--;
                    //printf("%s arrived in zone %d\n", all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
            if ((z - all_zones) == 2)
            {
                // printf("here1");
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity != 0)
                {
                    // printf("enters here\n");
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 2;
                    z->capacity--;
                    //printf("%s arrived in zone %d\n", all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
        }
    }
    return NULL;
}