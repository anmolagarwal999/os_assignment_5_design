#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

// inet_addr
#include <arpa/inet.h>
#include <unistd.h>

// For threading, link with lpthread
#include <pthread.h>
#include <semaphore.h>
#define insert 0
#define delete 1
#define update 2
#define concat 3
#define fetch 4
typedef struct data
{
int id;
int request_num;
char cmd[10];
char value[10000];
int socket_num;
int key1;
int key2;
} data;
data data_array[1000];
pthread_t tid[1000];
// Function to send data to
// server socket.
typedef struct info
{
    char array[1000];
    int id;
}info;
void *clienthread(void *args)
{
    info information;
    data client_request = *((data *)args);
    int network_socket;
    int num = client_request.id;
    sleep(client_request.request_num);		
    // Create a stream socket
    network_socket = socket(AF_INET,
                            SOCK_STREAM, 0);

    // Initialise port number and address
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(8989);

    // Initiate a socket connection
    int connection_status = connect(network_socket,
                                    (struct sockaddr *)&server_address,
                                    sizeof(server_address));

    // Check for connection error
    if (connection_status < 0)
    {
        puts("Error\n");
        return 0;
    }

    //printf("Connection established\n");

    // Send data to the socket
    send(network_socket, &client_request,
         sizeof(client_request), 0);
char array[100];
    recv(network_socket,&information, sizeof(information), 0);
    printf("%d:%d:%s\n",num , information.id , information.array);
    // Close the connection
    close(network_socket);
    pthread_exit(NULL);

    return 0;
}

// Driver Code
int main()
{
    int num_requests;
    scanf("%d", &num_requests);
    for (int i = 0; i < num_requests; i++)
    {
        data_array[i].id=i;
        data_array[i].key1=-1;
        data_array[i].key2=-1;
        scanf("%d %s", &data_array[i].request_num, &data_array[i].cmd[0]);
        if (strcmp(data_array[i].cmd, "insert") == 0)
        {
            scanf("%d %s", &data_array[i].key1, &data_array[i].value[0]);
        }
        if (strcmp(data_array[i].cmd, "delete") == 0)
        {
            scanf("%d", &data_array[i].key1);
        }
        if (strcmp(data_array[i].cmd, "update") == 0)
        {
            scanf("%d %s", &data_array[i].key1, &data_array[i].value[0]);
        }
        if (strcmp(data_array[i].cmd, "concat") == 0)
        {
            scanf("%d %d", &data_array[i].key1, &data_array[i].key2);
        }
        if (strcmp(data_array[i].cmd, "fetch") == 0)
        {
            scanf("%d", &data_array[i].key1);
        }
    }
    for (int i = 0; i < num_requests; i++)
    {
        pthread_create(&tid[i], NULL, clienthread, &data_array[i]);
    }
        for (int i = 0; i < num_requests; i++)
    {
        pthread_join(tid[i], NULL);
    }
    return 0;
}
