#include <arpa/inet.h>
#include <bits/stdc++.h>
using namespace std;
// For threading, link with lpthread
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

typedef struct data
{
    int id;
    int request_num;
    char cmd[10];
    char value[10000];
    int socket_num;
    int key1;
    int key2;
} data;
data data_recieved[1000];
pthread_t serverthreads[1000];
map<int, string> dictionary;
pthread_mutex_t lock_keys[200];
pthread_mutex_t l;
// Reader Function
int i = 0;
typedef struct info
{
    char array[1000];
    int id;
} info;
void *server(void *param)
{
    data *d = (data *)param;
    //printf("%d %d",d->key1 , d->key2);
    //printf("Thread %d %scame\n", d->id, d->cmd);
    string s = "";
    char array[100] = "helloc";
    info information;
    information.id = d->id;
    if (strcmp(d->cmd, "insert") == 0)
    {
        pthread_mutex_lock(&lock_keys[d->key1]);
        //printf("%d key locked\n",d->key1);
        int o;

        for (o = 0; o < strlen(d->value); o++)
        {
            s = s + d->value[o];
        }
        if (dictionary.find(d->key1) == dictionary.end())
        {
            dictionary.insert(pair<int, string>(d->key1, s));
            strcpy(information.array, "Insertion successful");
        }
        else
        {
            strcpy(information.array, "Key already exists");
        }
        //send(d->socket_num, array, sizeof(array), 0);
        send(d->socket_num, &information, sizeof(info), 0);
        pthread_mutex_unlock(&lock_keys[d->key1]);
        //printf("%d key unlocked\n",d->key1);
    }
    if (strcmp(d->cmd, "fetch") == 0)
    {
        pthread_mutex_lock(&lock_keys[d->key1]);
        if (dictionary.find(d->key1) == dictionary.end())
        {
            strcpy(information.array, "Key does not exist");
        }
        else
        {
            map<int, string>::iterator itr;
            itr = dictionary.find(d->key1);
            string s = itr->second;
            int o = 0;
            for (o = 0; o < s.length(); o++)
            {
                array[o] = s[o];
            }
            array[o] = '\0';
        }
        //send(d->socket_num, array, sizeof(array), 0);
        send(d->socket_num, &information, sizeof(info), 0);
        pthread_mutex_unlock(&lock_keys[d->key1]);
    }
    if (strcmp(d->cmd, "update") == 0)
    {
        pthread_mutex_lock(&lock_keys[d->key1]);
        if (dictionary.find(d->key1) == dictionary.end())
        {
            strcpy(information.array, "Key does not exist");
        }
        else
        {
            int o;

            for (o = 0; o < strlen(d->value); o++)
            {
                s = s + d->value[o];
            }
            dictionary.erase(d->key1);
            dictionary.insert(pair<int, string>(d->key1, s));
            strcpy(information.array, d->value);
        }
        //send(d->socket_num, array, sizeof(array), 0);
        send(d->socket_num, &information, sizeof(info), 0);
        pthread_mutex_unlock(&lock_keys[d->key1]);
    }
    if (strcmp(d->cmd, "delete") == 0)
    {
        pthread_mutex_lock(&lock_keys[d->key1]);
        //printf("%d key locked\n",d->key1);
        if (dictionary.find(d->key1) == dictionary.end())
        {
            strcpy(information.array, "No such key exists");
        }
        else
        {
            dictionary.erase(d->key1);
            strcpy(information.array, "Deletion successful");
        }
        send(d->socket_num, &information, sizeof(info), 0);
        //send(d->socket_num, array, sizeof(array), 0);
        pthread_mutex_unlock(&lock_keys[d->key1]);
        //printf("%d key unlocked\n",d->key1);
    }
    if (strcmp(d->cmd, "concat") == 0)
    {
        pthread_mutex_lock(&lock_keys[d->key1]);
        pthread_mutex_lock(&lock_keys[d->key2]);
        if ((dictionary.find(d->key1) == dictionary.end()) || (dictionary.find(d->key2) == dictionary.end()))
        {
            strcpy(information.array, "Concat failed as at least one of the keys does not exist");
        }
        else
        {
            string s1 = dictionary.find(d->key1)->second;
            string s2 = dictionary.find(d->key2)->second;
            dictionary.erase(d->key1);
            dictionary.erase(d->key2);
            dictionary.insert(pair<int, string>(d->key1, s1 + s2));
            dictionary.insert(pair<int, string>(d->key2, s2 + s1));
            string store = "";
            store = s2 + s1;
            int t = 0;
            for (t = 0; t < store.length(); t++)
            {
                information.array[t] = store[t];
            }
            information.array[t] = '\0';
            //strcpy(information.array, "Concat failed as at least one of the keys does not exist");
        }
        //send(d->socket_num, array, sizeof(array), 0);
        send(d->socket_num, &information, sizeof(info), 0);
        pthread_mutex_unlock(&lock_keys[d->key1]);
        pthread_mutex_unlock(&lock_keys[d->key2]);
    }
    //printf("Thread %d %sexiting\n", d->id, d->cmd);
    //send(d->socket_num, &information, sizeof(info), 0);
    sleep(1);
    return NULL;
}

// Driver Code
int main(int arg_num, char **args)
{
    //printf("%s\n" , args[1]);
    // Initialize variables
    int serverSocket, newSocket;
    struct sockaddr_in serverAddr;
    struct sockaddr_storage serverStorage;

    socklen_t addr_size;
    pthread_mutex_init(&l, NULL);
    for (int i = 0; i < 100; i++)
    {
        pthread_mutex_init(&lock_keys[i], NULL);
    }
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(8989);

    // Bind the socket to the
    // address and port number.
    bind(serverSocket,
         (struct sockaddr *)&serverAddr,
         sizeof(serverAddr));

    // Listen on the socket,
    // with 40 max connection
    // requests queued
    if (listen(serverSocket, 32767) != 0)
        printf("Error\n");

    // Array for thread
    while (1)
    {
        addr_size = sizeof(serverStorage);

        // Extract the first
        // connection in the queue
        newSocket = accept(serverSocket,
                           (struct sockaddr *)&serverStorage,
                           &addr_size);
        recv(newSocket, &data_recieved[i], sizeof(data), 0);
        data_recieved[i].socket_num = newSocket;
        //printf("\nrecieved : %d %s\n", data_recieved[i].id, data_recieved[i].cmd);
        data_recieved[i].id = i;
        pthread_create(&serverthreads[i], NULL, server, &data_recieved[i]);
        i++;

        if (i >= atoi(args[1]))
        {
            // Update i
            i = 0;

            while (i < atoi(args[1]))
            {
                // Suspend execution of
                // the calling thread
                // until the target
                // thread terminates
                pthread_join(serverthreads[i++],
                             NULL);
            }

            // Update i
            i = 0;
        }
    }

    return 0;
}
