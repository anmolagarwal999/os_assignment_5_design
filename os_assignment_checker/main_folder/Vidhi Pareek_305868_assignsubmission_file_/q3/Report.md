# Question 3
## To Run
```g++ client.cpp -o client -lpthread```
```g++ server.cpp -o server -lpthread```

## Server Program
serverSocket- hold the return of socket function
<br>Other variables are initialised
<br>bind: After the creation of the socket, the bind function binds the socket to the address and port number specified in addr.
<br>listen: It puts the server socket in a passive mode, where it waits for the client to approach the server to make a connection.
<br>After accepting each connection the server creates a thread to handle the user request.
<br>The number of threads are restricted by the command line argument passed to the server program.
<br><br>Functioning of server threads
<br>They recieve struct with data andinstructions , accordingly they move to their respective commands and dictionary is modified.
<br>An arrayof mutex is used to protect each key.
<br>Data is sent and recieved using system calls.
<br>recv recieves data and send sends data. 

## Client Program
<br>This program first takes the input about total number of requests and information associated with each request.
<br>Then, it creates threads equal to number of requests.
<br>Each thread acts as a separate user and requests the server for connection after the time mentioned in input.
<br>Using send it sends the struct with commands and information.
<br>It then waits for aresponse from the server.
<br>It recieves the thread id and response statement using recv.
<br>Prints output and exits.