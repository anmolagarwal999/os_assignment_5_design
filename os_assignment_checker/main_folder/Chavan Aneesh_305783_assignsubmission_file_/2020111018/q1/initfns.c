#include "defs.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

TA* init_TA(int id){
    TA* t = (TA*) malloc(sizeof(TA)); 
    if(t == NULL){
        perror("init_TA");
        return NULL;
    }

    t->id = id;
    t->p_TAships = (int*) malloc(sizeof(int));
    t->p_free = (int*) malloc(sizeof(int));
    *(t->p_TAships) = 0;
    *(t->p_free) = 1;
    if(pthread_mutex_init(&(t->mutex), NULL) != 0){
        perror("init_TA mutex");
        free(t);
        return NULL;
    }

    return t;
}

void clear_TA(TA* t){
    if(pthread_mutex_destroy(&(t->mutex)) != 0)
        perror("clear_TA");
    free(t->p_TAships);
    free(t->p_free);
    free(t);

    pthread_mutex_destroy(&t->mutex);

    return;
}

void print_TA(TA* t){
    printf("id: %d\ttaships: %d\tbusy: %d\n", t->id, *(t->p_TAships), *(t->p_free));
}

Lab* init_lab(int id, char* name, int num_TAs, int TA_limit){
    Lab* l = (Lab*) malloc(sizeof(Lab)); 
    if(l == NULL){
        perror("init_lab");
        return NULL;
    }

    l->id = id;
    l->name = (char*) malloc(strlen(name) + 1);
    strcpy(l->name, name);
    l->num_TAs = num_TAs;
    l->p_any_free = (int*) malloc(sizeof(int));
    *l->p_any_free = 1;
    l->p_num_waiting = (int*) malloc(sizeof(int));
    *l->p_num_waiting = 0;
    l->cnt_TA_limit = 0;
    l->TA_limit = TA_limit;
    l->p_selected_TA = (int*) malloc(sizeof(int));
    *(l->p_selected_TA) = -1;

    l->TA_list = (TA**) malloc(sizeof(TA*) * num_TAs);
    if (l->TA_list == NULL){
        perror("init_lab");
        return NULL;
    }
    for (int i = 0; i < num_TAs; i++){
        l->TA_list[i] = init_TA(i);
    }

    int a = sem_init(&l->course_waiting, 0, 0);
    int b = sem_init(&l->TA_ready, 0, 0);
    int c = sem_init(&l->TA_ack, 0, 0);    

    if(a||b||c != 0){
        perror("sem_init");

        free(l->name);

        for(int i = 0; i < num_TAs; i++){
            clear_TA(l->TA_list[i]);
        }
        free(l->TA_list);

        sem_destroy(&(l->course_waiting));
        sem_destroy(&(l->TA_ready));
        sem_destroy(&(l->TA_ack));

        return NULL;
    }

    pthread_mutex_init(&l->mutex_any_free, NULL);
    pthread_mutex_init(&l->mutex_num_waiting, NULL);


    return l;
}

void clear_lab(Lab* l){
    free(l->name);
    free(l->p_selected_TA);
    free(l->p_any_free);
    free(l->p_num_waiting);

    for(int i = 0; i < l->num_TAs; i++){
        clear_TA(l->TA_list[i]);
    }
    free(l->TA_list);

    int a = sem_destroy(&(l->course_waiting));
    int b = sem_destroy(&(l->TA_ready));
    int c = sem_destroy(&(l->TA_ack));

    if (a||b||c != 0)
        perror("clear_lab");

    pthread_mutex_destroy(&l->mutex_any_free);
    pthread_mutex_destroy(&l->mutex_num_waiting);


    free(l);
}

void print_Lab(Lab* l){
    printf("id: %d\n%s\nnum tas: %d\ncnt_limit: %d\n", l->id, l->name, l->num_TAs, l->cnt_TA_limit);
    for(int i = 0; i < l->num_TAs; i++){
        print_TA(l->TA_list[i]);
    }
    printf("ta limit: %d\nselected ta: %d\n", l->TA_limit, *(l->p_selected_TA));
}

Course* init_course(int id, char* name, float interest, int max_slots, int num_labs, int* lab_list){
    Course* c = (Course*) malloc(sizeof(Course));
    if(c == NULL){
        perror("init_course");
        return NULL;
    }

    c->id = id;
    c->name = (char*) malloc(strlen(name) + 1);
    strcpy(c->name, name);
    c->interest = interest;
    c->course_max_slot = max_slots;
    c->num_labs = num_labs;
    
    c->lab_list = (int*) malloc(sizeof(int) * num_labs);
    for(int i = 0; i < num_labs; i++){
        c->lab_list[i] = lab_list[i];
    }

    c->D = 1000;
    c->TA_present = 0;
    c->p_available = (int*) malloc(sizeof(int));
    *(c->p_available) = 1;
    c->cnt_TA_id = -2;
    c->cnt_lab_id = -2;
    c->p_cnt_waiting = (int*) malloc(sizeof(int));
    *(c->p_cnt_waiting) = 0;

    if(sem_init(&(c->waiting), 0, 0) != 0){
        perror("init_course");
        free(c->name);
        free(c->lab_list);
    }

    if(sem_init(&(c->tute_starting), 0, 0) != 0){
        perror("init_course");
        free(c->name);
        free(c->lab_list);
    }

    if(pthread_mutex_init(&(c->mutex_cnt_waiting), NULL) != 0){
        perror("init_course mutex");
        free(c->name);
        free(c->lab_list);
        return NULL;
    }

    return c;
}

void clear_course(Course* c){
    free(c->name);
    free(c->lab_list);
    free(c->p_available);
    free(c->p_cnt_waiting);

    if(sem_destroy(&(c->waiting))){
        perror("clear_course");
        return;
    }

    pthread_mutex_destroy(&c->mutex_cnt_waiting);

    free(c);

    return;
}

void print_Course(Course* c){
    printf("id: %d\n%s\ninterest: %f\nmax_slots: %d\nnum_labs %d\nlab ids:\n", c->id, c->name, c->interest, c->course_max_slot, c->num_labs);
    for(int i = 0; i < c->num_labs; i++){
        printf("%d\n", c->lab_list[i]);
    }
    printf("D: %d\nta_present: %d\navailable: %d\n\n", c->D, c->TA_present, *(c->p_available));
}

Student* init_student(int id, int delay, int course1, int course2, int course3, float calibre){
    Student* s = (Student*) malloc(sizeof(Student));

    s->id = id;
    s->p_cnt_pref = (int*) malloc(sizeof(int));
    *(s->p_cnt_pref) = 0;
    s->courses[0] = course1;
    s->courses[1] = course2;
    s->courses[2] = course3;
    s->calibre = calibre;

    s->delay = delay;
    s->p_enrolled = (int*) malloc(sizeof(int));
    s->p_registered = (int*) malloc(sizeof(int));

    *(s->p_enrolled) = 0;
    *(s->p_registered) = 0;

    return s;
}

void clear_student(Student* s){
    free(s->p_cnt_pref);
    free(s->p_enrolled);
    free(s->p_registered);
    free(s);

    return;
}

void print_Student(Student* s){
    printf("id: %d\ncnt_pref: %d\ncourse1: %d\ncourse2: %d\ncourse3: %d\n", s->id, *(s->p_cnt_pref), s->courses[0], s->courses[1], s->courses[2]);
    printf("calibre: %f\ndelay: %d\nenrolled: %d\nregistered: %d\n\n", s->calibre, s->delay, *(s->p_enrolled), *(s->p_registered));
}

void print_all(int n_c, int n_s, int n_l ,Student** all_students, Lab** all_labs, Course** all_courses){
    for(int i = 0; i < n_c; i++){
        print_Course(all_courses[i]);
    }

    printf("\n");

    for(int i = 0; i < n_s; i++){
        print_Student(all_students[i]);
    }

    printf("\n");

    for(int i = 0; i < n_l; i++){
        print_Lab(all_labs[i]);
    }
}