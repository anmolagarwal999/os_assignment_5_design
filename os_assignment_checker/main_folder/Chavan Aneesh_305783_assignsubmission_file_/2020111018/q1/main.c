#include "defs.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

Student** all_students;
Lab** all_labs;
Course** all_courses;

int total_students, total_labs, total_courses;

// pthread_mutex_t mutex_all_init = PTHREAD_MUTEX_INITIALIZER;
// int all_init = 0;

int main(void){
    scanf("%d %d %d", &total_students, &total_labs, &total_courses);
    pthread_t student_tids[total_students], lab_tids[total_labs], course_tids[total_courses];

    // array of all entities
    all_students = (Student**) malloc(sizeof(Student*) * total_students);
    all_labs = (Lab**) malloc(sizeof(Lab*) * total_labs);
    all_courses = (Course**) malloc(sizeof(Course*) * total_courses);


    // parse courses
    for(int i = 0; i < total_courses; i++){
        char name_buf[100];
        float interest;
        int max_slots, num_labs;
        scanf("%s %f %d %d", name_buf, &interest, &max_slots, &num_labs);
        int lab_ids[num_labs];
        for(int j = 0; j < num_labs; j++){
            scanf("%d", &(lab_ids[j]));
        }

        all_courses[i] = init_course(i, name_buf, interest, max_slots, num_labs, lab_ids);

        name_buf[0] = '\0';
    }

    // parse students
    for(int i  = 0; i < total_students; i++){
        float calibre;
        int c1, c2, c3, delay;
        scanf("%f %d %d %d %d", &calibre, &c1, &c2, &c3, &delay);

        all_students[i] = init_student(i, delay, c1, c2, c3, calibre);
    }

    // parse labs
    for(int i = 0; i < total_labs; i++){
        char name_buf[100];
        int num_TAs, limit;

        scanf("%s %d %d", name_buf, &num_TAs, &limit);
        
        all_labs[i] = init_lab(i, name_buf, num_TAs, limit);

        for(int j = 0; j < num_TAs; j++){
            all_labs[i]->TA_list[j] = init_TA(j);
        }

        name_buf[0] = '\0';
    }

//--  create threads  ------------------------------------------------------------------------------------------------------------------------------------------------//

    // labs
    for(int i = 0; i < total_labs; i++){
        printf("Creating lab %d %s\n", all_labs[i]->id, all_labs[i]->name);
        pthread_create(&(lab_tids[all_labs[i]->id]), NULL, lab_thread, (void*) &(all_labs[i]->id));
    }

    // courses
    for(int i = 0; i < total_courses; i++){
        printf("Creating course %d %s\n", all_courses[i]->id, all_courses[i]->name);
        pthread_create(&(course_tids[all_courses[i]->id]), NULL, course_thread, (void*) &(all_courses[i]->id));
    }

    // students
    for(int i = 0; i < total_students; i++){
        printf("Creating student %d\n", all_students[i]->id);
        pthread_create(&(student_tids[all_students[i]->id]), NULL, student_thread, (void*) &(all_students[i]->id));
    }


    // print_all(total_courses, total_students, total_labs, all_students, all_labs, all_courses);

//--  clean up  ------------------------------------------------------------------------------------------------------------------------------------------------//

    for(int i = 0; i < total_students; i++){
        pthread_join(student_tids[i], NULL);
    }    

    printf("Simulation complete, all students have exited\n");

    for(int i = 0; i < total_students; i++)
        clear_student(all_students[i]);

    for(int i = 0; i < total_labs; i++)
        clear_lab(all_labs[i]);

    for(int i = 0; i < total_courses; i++)
        clear_course(all_courses[i]);

}

// debugging