Compile the program by executing "make"
Run it by executing "./q1" and entering input.

The bonus part has been implemented

I have used case 1 for tutorials with 0 students.
The TA conducts the tute anyway, the delay has been adjusted to 5

<hr>
<h3>Colours list</h3>
<ul>
<li>Students: Red</li>
<li>Students leaving the simulation: Blue</li>
<li>TAs: Green</li>
<li>Courses: Yellow</li>
<li>Labs: Purple</li>
<li>General/main.c: White</li>
</ul>

<hr>
<br>

<h2>Overview</h2>

Each entity has a struct that contains each corresponding parameter, semaphore and mutex. 
Labs, courses and students are all simulated by threads. The main thread reads sure input and initialises global arrays of all students, labs and courses. After this all threads are initiated using the functions in threads.c . All shared data is stored in malloc'd memory
<br>
Each thread manages exactly one struct from these global arrays. An ID supplied as an argument tells it which one.
<p>
Labs wait for courses to signal them, after which they provide TAs until none remain. After this, they are 'dormant' and are blocked until the main thread ends.
</p>

<p>
Courses continuously loop through the following three 'phases',
<ol>
<li>Look for a TA</li>
<li>Set D, wait for enough students</li>
<li>Conduct the tutorial, let the TA go</li>
</ol>
until each of their labs is exhausted. At this point they are withdrawn and all waiting students are signalled to move on. As per the question, if no students are currently interested, the tute is still conducted.
</p>

<p>
Students loko for each of their three courses one by one. They wait until the tute starts and wait until the course thread signals them that the tute is either over or is withdrawn. If the tute goes through, their chances of selecting the course are calculated using the <u>CUTOFF</u> macro defined in defs.h . If they withdraw, they try the other courses in their preferences. If they choose a course or dont gt any preffered ones, the thread exits. The main thread waits for all student threads to join and then ends the simulation.
</p>

<br>

<h2>Labs and courses</h2>

<p>The interaction between labs and courses is a producer-consumer problem.</p>
Each lab has a list of TA structs, each of which store a mutex, an int to represent if they are busy and the current no. of TAships. When a course needs a TA, it iterates through all of its labs, and if a lab has any TAs left signals <u>lab.waiting</u>, increments <u>lab.num_waiting</u> and waits for <u>lab.TA_ready</u> .
</p>
<p>
Each lab waits for its lab.waiting semaphore to prevent busy waiting. It then searches through its list of TAs to find one that is busy and still eligible for TAship. It then locks the TAs mutex and increments its TAship count and marks it as busy. Each lab has a <u>selected_TA</u> attribute that stores the ID of the TA selected by the lab. It stores the chosen TAs ID in this attribute and signals <u>lab.ta_ready</u> and waits for <u>lab.TA_ack</u>. If a TA is at the current limit, the lab checks if every other TA has also reached that limit. If so, the TA is selected and the cnt_limit is raised.
</p>
<p>
The blocked course thread is now woken up. It doubles checks that the lab is still available, and copies the TAs and the lab's ID into attributes in its own struct and signals <u>TA_ack</u>, indicating that copying is over and that the lab is free to look for another TA if needed. The course then changes its <u>TA_present</u> attribute to 1. In case the lab wasnt available after double checking it assumes there arent TAs left and it moves onto the next lab. TA_ack is not signalled in this case.
</p>
<p>
If a course cant find a free TA, it checks if there are any TAs left. If not, it changes its <u>lab.any_free</u> to 0. It then signals lab.TA_ready once for each waiting course (num stores in num_waiting). The thread then returns.
</p>

<br>

<h2>Students and courses</h2>
<p>
This problem is more basic than that between the labs and courses. 
After the initial delay, the student changes its 'registered' atrribute to 1.
Each student goes through each course in order of preference, signals that courses <u>course.waiting</u> and waits for <u>course.tute_starting</u>. <u>course.num_waiting</u> is also incremented. <u>student.enrolled</u>. The appropriate mutexes are locked
</p>
<p>
After the course has a TA, rand() is used to generate a value for D. The course then checks num_waiting to see if there are >= D students waiting. If so, it starts the tute. Else, it goes through all_students and checks if there is any student that is currently looking for this course but hasnt enrolled yet. If one is found it waits and starts the tute otherwise. The course stores the <u>final_tute_size</u>, waits for 3 seconds and then signals all waiting students. The student double checks to see if the lab is still available. The students chances of joining are then calculated. This is used to determine if the student moves on to the next course or selects the course and exits the simulation.
</p>
<p>
If a course finds that all of its labs are no longer available, it is withdrawn similar to a lab. Each student is signalled and they find that course.available == 0. They then move onto the next course.
</p>