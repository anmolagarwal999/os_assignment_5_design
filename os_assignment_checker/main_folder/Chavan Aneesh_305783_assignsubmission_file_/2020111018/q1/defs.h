#ifndef __DEFS__
#define __DEFS__

#include <pthread.h>
#include <semaphore.h>

#define STUDENT_COLOR               "\x1b[31m"
#define STUDENT_SUCCESS_COLOR       "\x1b[34m"
#define TA_COLOR                    "\x1b[32m"
#define COURSE_COLOR                "\x1b[33m"
#define LAB_COLOR                   "\x1b[35m"
#define NORMAL_COLOR                "\x1b[0m"

#define CUTOFF                      0.55f
#define TUTE_DELAY 5
#define COURSE_LOOK_DELAY 1
#define STUDENT_REGISTER_DELAY 1


void start_lab(int lab_id);
void start_student(int student_id);
void start_course(int course_id);


// structs

struct s_TA{
    int id;                     // ta id, assigned sequentially
    int* p_TAships;                // current no. of ta ships, init to 0
    int* p_free;                   // is this ta currently free
    pthread_mutex_t mutex;
};
typedef struct s_TA TA;

struct s_lab{
    int id;                     // global lab id
    char* name;                 
    
    int num_TAs;                // no. of assigned tas
    int* p_any_free;               // are any tas free
    pthread_mutex_t mutex_any_free; 

    int* p_num_waiting;             // how many courses are waiting for a ta
    pthread_mutex_t mutex_num_waiting;
    
    TA** TA_list;                // array of all assigned tas
    int TA_limit;               // limit on no of TAships

    int cnt_TA_limit;           // TAs need to wait until all TAs are at this limit

    sem_t course_waiting;       // binary sem to store how many courses are waiting for a ta
    int* p_selected_TA;            // id of the selected ta if any, otherwise set to -1
    sem_t TA_ready;             // binary sem to signal if a ta is selected
    sem_t TA_ack;               // binary sem, has the course copied the ta id
};
typedef struct s_lab Lab;

struct s_course{
    int id;
    char* name;
    float interest;
    int course_max_slot;

    int num_labs;
    int* lab_list;              // list of the ids of associated labs

    int D;                      // size of tute
    int TA_present;             // is a ta ready to take a tute
    int* p_available;              // is the course still available to students

    int cnt_TA_id;
    int cnt_lab_id;

    sem_t waiting;
    sem_t tute_starting;

    int* p_cnt_waiting;
    pthread_mutex_t mutex_cnt_waiting;
};
typedef struct s_course Course;

struct s_student{
    int id;
    int* p_cnt_pref;               // index of current preference
    int courses[3];             // array of course ids 
    float calibre;

    int delay;                  // delay before the student enters
    int* p_enrolled;               // is the student waiting for a tute
    int* p_registered;             // is the student looking for a course
};
typedef struct s_student Student;

// initfns

TA*         init_TA(int id);
Lab*        init_lab(int id, char* name, int num_TAs, int TA_limit);
Course*     init_course(int id, char* name, float interest, int max_slots, int num_labs, int* lab_list);
Student*    init_student(int id, int delay, int course1, int course2, int course3, float calibre);
void        clear_TA(TA* t);
void        clear_lab(Lab* l);
void        clear_course(Course* c);
void        clear_student(Student* s);

void        print_TA(TA* t);
void        print_Lab(Lab* l);
void        print_Course(Course* c);
void        print_Student(Student* s);
void        print_all(int n_c, int n_s, int n_l ,Student** all_students, Lab** all_labs, Course** all_courses);

// threads.c

void* student_thread(void* vp_s_id);
void* lab_thread(void* vp_l_id);
void* course_thread(void* vp_c_id);

#endif

/*
studno      labno       courseno

courses: name  float  max  lab_n  lab1 lab2 ...

student: calibre   c1  c2  c3 delay

lab:    name    num_TAs     limit   
*/

