#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>

extern Student** all_students;
extern Lab** all_labs;
extern Course** all_courses;

extern int total_students;

extern pthread_mutex_t mutex_all_init;
extern int all_init;

void* student_thread(void* vp_s_id){
    int s_id = *(int*) vp_s_id;
    Student *s = all_students[s_id];

    sleep(s->delay);
    // student now enters registration
    fprintf(stdout, STUDENT_COLOR"Student %d has filled in preferences for course registration\n"NORMAL_COLOR,
            s->id);

    for(int i = 0; i < 3; i++){
        *(s->p_cnt_pref) = i;

        Course* c = all_courses[s->courses[i]];

        // is the tute still available
        if(*(c->p_available) == 1){
            fprintf(stdout, STUDENT_COLOR"Student %d has been allocated a seat in course %d\n"NORMAL_COLOR,
                    s->id, c->id);
            sem_post(&c->tute_starting);

            *s->p_enrolled = 1;

            pthread_mutex_lock(&c->mutex_cnt_waiting);
            (*c->p_cnt_waiting)++;
            pthread_mutex_unlock(&c->mutex_cnt_waiting);
            
            sem_wait(&c->waiting);

            // student has completed the tute
            *s->p_enrolled = 1;
            if (*(c->p_available)){
                float prob = s->calibre * c->interest;
                if (prob < CUTOFF){
                    // student withdraws
                    fprintf(stdout, STUDENT_COLOR"Student %d has withdrawn from %s (course %d)\n"NORMAL_COLOR,
                            s->id, c->name, c->id);
                }
                else{
                    // student accepts this course and exits the simulation
                    fprintf(stdout, STUDENT_SUCCESS_COLOR"Student %d has selected %s (course %d) permanently\n"NORMAL_COLOR,
                            s->id, c->name, c->id);
                    
                    return NULL;
                }
            }
        }

        if (i != 2){
            fprintf(stdout, STUDENT_COLOR"Student %d has changed current preference from %s (id %d) to %s (id %d)\n"NORMAL_COLOR,
                    s->id, c->name, c->id, all_courses[s->courses[i+1]]->name, all_courses[s->courses[i+1]]->id);
        }

        sleep(STUDENT_REGISTER_DELAY);
    }

    // student hasnt chosen any of the three courses
    fprintf(stdout, STUDENT_SUCCESS_COLOR"Student %d couldn't get any of their preferred courses\n"NORMAL_COLOR,
            s->id);
    return NULL;    
}


void* course_thread(void* vp_c_id){    
    int c_id = *(int*) vp_c_id;
    Course *c = all_courses[c_id];

    printf(COURSE_COLOR"Course %d %s started\n"NORMAL_COLOR, c_id, c->name);

    // loop represents the cycle of:    find TA
    //                                  wait for students
    //                                  run tute
    //                                  return TA

    while(1){
        // Loop to select a TA
        printf(COURSE_COLOR"Course %d %s looking for a TA\n"NORMAL_COLOR, c_id, c->name);
        sleep(COURSE_LOOK_DELAY);
        while(c->TA_present == 0){
            for(int l_id = 0; l_id < c->num_labs; l_id++){
                Lab *l = all_labs[c->lab_list[l_id]];

                // only checks labs with available TAs
                pthread_mutex_lock(&l->mutex_any_free);
                if(*l->p_any_free){
                    pthread_mutex_unlock(&l->mutex_any_free);

                    pthread_mutex_lock(&l->mutex_num_waiting);
                    (*l->p_num_waiting)++;
                    sem_post(&l->course_waiting);
                    pthread_mutex_unlock(&l->mutex_num_waiting);

                    sem_wait(&l->TA_ready);


                    // double check, in case the lab ran out before this TA request
                    pthread_mutex_lock(&l->mutex_any_free);
                    if (*l->p_any_free){
                        pthread_mutex_unlock(&l->mutex_any_free);

                        c->cnt_TA_id = *(l->p_selected_TA);
                        c->cnt_lab_id = l->id;
                        c->TA_present = 1;
                        sem_post(&l->TA_ack);
                        break;                                  // TA found, exiting this loop
                    }
                    else{
                        pthread_mutex_unlock(&l->mutex_any_free);
                    }
                }
                else{
                    pthread_mutex_unlock(&l->mutex_any_free);
                }
            }

            if(c->TA_present == 1)
                break;

            // all labs checked once, checking
            int any_left = 0;
            for(int l_id = 0; l_id < c->num_labs; l_id++){
                Lab *l = all_labs[c->lab_list[l_id]];

                pthread_mutex_lock(&l->mutex_any_free);
                if(*l->p_any_free == 1){
                    pthread_mutex_unlock(&l->mutex_any_free);
                    any_left = 1;
                    break;
                }
                else{
                    pthread_mutex_unlock(&l->mutex_any_free);
                }
            }

            if(any_left == 0){
                fprintf(stdout, COURSE_COLOR"Course %d has no TAs and is no longer offered\n"NORMAL_COLOR, 
                        c->id);
                *(c->p_available) = 0;
                while(1){
                    sem_wait(&c->tute_starting);
                    sem_post(&c->waiting);
                }
            }
        }

        fprintf(stdout, TA_COLOR"Ta %d from lab %d has been allocated to course %d for TA ship no. %d\n"NORMAL_COLOR, 
                c->cnt_TA_id, c->cnt_lab_id, c->id, *(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->p_TAships));
        // return NULL;
        if(c->cnt_lab_id == -1 || c->cnt_TA_id == -1)
            assert(0);

        // Loop to set up a tutorial
        srand(time(NULL));
        c->D = (rand() % c->course_max_slot) + 1;

        fprintf(stdout, COURSE_COLOR"Course %d has been allocated %d seats\n"NORMAL_COLOR, 
                c->id, c->D);

        int final_tute_size;

        assert(c->cnt_TA_id != -1 && c->cnt_lab_id != -1);

        // loop to check if there are enough students to begin
        while(1){
            int in_queue;
            pthread_mutex_lock(&c->mutex_cnt_waiting);
            in_queue = *c->p_cnt_waiting;
            pthread_mutex_unlock(&c->mutex_cnt_waiting);

            // fprintf(stdout, COURSE_COLOR"Course %d's tutorial has started the search\n"NORMAL_COLOR, c->id);

            if(in_queue >= c->D){
                final_tute_size = c->D;
                break;
            }
            else{
                // fprintf(stdout, COURSE_COLOR"Course %d's tutorial is checking for students\n"NORMAL_COLOR, c->id);
                // check if any students have this course but havent registered
                int any_not_enrolled = 0;

                // check if there is at least one student who has yet to enroll
                for (int i = 0; i < total_students; i++){
                    Student* s = all_students[i];
                    if (s->courses[*s->p_cnt_pref] == c->id  && *(s->p_enrolled) == 0){
                        any_not_enrolled = 1;
                        // fprintf(stdout, COURSE_COLOR"Course %d's is waiting on a student\n"NORMAL_COLOR, c->id);
                        break;
                    }
                }

                if (any_not_enrolled == 0){
                    pthread_mutex_lock(&c->mutex_cnt_waiting);
                    final_tute_size = *c->p_cnt_waiting;
                    pthread_mutex_unlock(&c->mutex_cnt_waiting);
                    break;
                }
            }
        }

        fprintf(stdout, COURSE_COLOR"Course %d's tutorial has started with %d seats\n"NORMAL_COLOR, 
                c->id, final_tute_size);

        // start the tute
        sleep(TUTE_DELAY);
        for (int i = 0; i < final_tute_size; i++){
            sem_wait(&c->tute_starting);
            sem_post(&c->waiting);
        }

        pthread_mutex_lock(&c->mutex_cnt_waiting);
        *c->p_cnt_waiting -= final_tute_size;
        pthread_mutex_unlock(&c->mutex_cnt_waiting);

        pthread_mutex_lock(&(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->mutex));
        *(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->p_free) = 1;
        pthread_mutex_unlock(&(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->mutex));
    
        fprintf(stdout, TA_COLOR"Ta %d from lab %d left course %d\n"NORMAL_COLOR, 
                c->cnt_TA_id ,c->cnt_lab_id, c->id);

        c->cnt_TA_id = -1;
        c->cnt_lab_id = -1;
        c->TA_present = 0;
    }
}

void* lab_thread(void* vp_l_id){
    int l_id = *(int*) vp_l_id;
    Lab *l = all_labs[l_id];
    
    // loop represents normal function
    while(1){
        // unblocked when a course is waiting, first checks for a free TA, assigns it to the course
        sem_wait(&l->course_waiting);
        for(int ta_found = 0; ta_found != 1;){      // stores if a ta was successfully found or not

            for(int ta_id = 0; ta_id < l->num_TAs; ta_id++){
                TA *cnt_TA = l->TA_list[ta_id];

                pthread_mutex_lock(&cnt_TA->mutex);

                if(*(cnt_TA->p_free) && (*(cnt_TA->p_TAships) < l->cnt_TA_limit) && *(cnt_TA->p_TAships) < l->TA_limit){
                    *(l->p_selected_TA) = cnt_TA->id;
                    *(cnt_TA->p_free) = 0;
                    (*(cnt_TA->p_TAships))++;
                    pthread_mutex_unlock(&cnt_TA->mutex);    

                    sem_post(&l->TA_ready);
                    sem_wait(&l->TA_ack);

                    pthread_mutex_lock(&l->mutex_num_waiting);
                    (*l->p_num_waiting)--;
                    pthread_mutex_unlock(&l->mutex_num_waiting);

                    *(l->p_selected_TA) = -1;
                    ta_found = 1;
                    break;
                }
                // ta is at ta limit, check if all others are at ta limit
                else if(*(cnt_TA->p_free) && *(cnt_TA->p_TAships) >= l->cnt_TA_limit && *(cnt_TA->p_TAships) < l->TA_limit){
                    int all_at_max = 1;
                    for (int i = 0; i < l->num_TAs; i++){
                        if (i != ta_id){
                            TA *t = l->TA_list[i];
                            // pthread_mutex_lock(&t->mutex);
                            if (*(t->p_TAships) < l->cnt_TA_limit){
                                // pthread_mutex_unlock(&t->mutex);
                                all_at_max = 0;
                                break;
                            }
                            // else
                                // pthread_mutex_unlock(&t->mutex);
                        }
                    }

                    if (all_at_max){
                        l->cnt_TA_limit++;

                        *(l->p_selected_TA) = cnt_TA->id;
                        *(cnt_TA->p_free) = 0;
                        (*(cnt_TA->p_TAships))++;
                        pthread_mutex_unlock(&cnt_TA->mutex);    

                        sem_post(&l->TA_ready);
                        sem_wait(&l->TA_ack);

                        pthread_mutex_lock(&l->mutex_num_waiting);
                        (*l->p_num_waiting)--;
                        pthread_mutex_unlock(&l->mutex_num_waiting);

                        *(l->p_selected_TA) = -1;
                        ta_found = 1;
                        break;
                    }
                    else{
                        pthread_mutex_unlock(&cnt_TA->mutex);
                    }
                }
                else{
                    pthread_mutex_unlock(&cnt_TA->mutex);
                }
                
            }

            // check if all tas are at the lab limit, if they are the lab shuts down
            int any_left = 0;
            for(int ta_id = 0; ta_id < l->num_TAs; ta_id++){
                TA *cnt_TA = l->TA_list[ta_id];
                if(*(cnt_TA->p_TAships) < l->TA_limit){
                    any_left = 1;
                    break;
                }
            }

            if(any_left == 0){
                fprintf(stdout, LAB_COLOR"Lab %d no longer has students available for TA ship\n"NORMAL_COLOR, l_id);
                
                pthread_mutex_lock(&l->mutex_any_free);
                *(l->p_any_free) = 0;
                pthread_mutex_unlock(&l->mutex_any_free);

                pthread_mutex_lock(&l->mutex_num_waiting);
                while(*(l->p_num_waiting) > 0){
                    sem_post(&l->TA_ready);
                }
                pthread_mutex_unlock(&l->mutex_num_waiting);
                
                return NULL;
            }

            // printf("lab %d looping again\n", l->id);
        }
    }
}