#include <iostream>
#include <bits/stdc++.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

#include "dictionary.h"


// returns 0 on success, 1 on failure
int operation_insert(Entry *dictionary, unsigned int key, std::string value){
    if (key > 100){
        std::cerr << "Invalid key\n";
        return 1;
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == false){
        dictionary[key].in_use = true;
        dictionary[key].value = value;

        pthread_mutex_unlock(&dictionary[key].mutex);
        return 0;        
    }
    else{
        pthread_mutex_unlock(&dictionary[key].mutex);
        return 1;
    }
}


int operation_delete(Entry *dictionary, unsigned int key){
    if (key > 100){
        std::cerr << "Invalid key\n";
        return 1;
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        dictionary[key].in_use = false;
        dictionary[key].value = "";

        pthread_mutex_unlock(&dictionary[key].mutex);
        return 0;        
    }
    else{
        pthread_mutex_unlock(&dictionary[key].mutex);
        return 1;
    }
}


std::pair<int, std::string> operation_update(Entry *dictionary ,unsigned int key, std::string value){
    std::pair<int, std::string> p;
    
    if (key > 100){
        std::cerr << "Invalid key\n";
        p.first = 1;
        p.second = "";
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        dictionary[key].value = value;

        p.first = 0;
        p.second = value;
    }
    else{
        p.first = 1;
        p.second = "";
    }

    pthread_mutex_unlock(&dictionary[key].mutex);
    return p;
}


std::pair<int, std::string> operation_concat(Entry *dictionary , unsigned int key1, unsigned int key2){
    std::pair<int, std::string> p;
    
    if (key1 > 100 || key2 >100){
        std::cerr << "Invalid key(s)\n";
        p.first = 1;
        p.second = "";
    }

    int l_key = (key1 > key2) ? key2 : key1;
    int g_key = (key1 > key2) ? key1 : key2;
    
    pthread_mutex_lock(&dictionary[l_key].mutex);
    pthread_mutex_lock(&dictionary[g_key].mutex);
    if (dictionary[l_key].in_use == true && dictionary[g_key].in_use == true){
        std::string a = dictionary[l_key].value;

        a += dictionary[g_key].value;
        dictionary[g_key].value += dictionary[l_key].value;
        dictionary[l_key].value = a;

        p.first = 0;
        p.second = dictionary[key2].value;
    }
    else{
        p.first = 1;
        p.second = "";
    }

    pthread_mutex_unlock(&dictionary[g_key].mutex);
    pthread_mutex_unlock(&dictionary[l_key].mutex);
    return p;
}

std::pair<int, std::string> operation_fetch(Entry *dictionary, unsigned int key){
    std::pair<int, std::string> p;
    
    if (key > 100){
        std::cerr << "Invalid key\n";
        p.first = 1;
        p.second = "";
    }
    
    pthread_mutex_lock(&dictionary[key].mutex);
    if (dictionary[key].in_use == true){
        p.first = 0;
        p.second = dictionary[key].value;
    }
    else{
        p.first = 1;
        p.second = "";
    }
    pthread_mutex_unlock(&dictionary[key].mutex);
    return p;   
}