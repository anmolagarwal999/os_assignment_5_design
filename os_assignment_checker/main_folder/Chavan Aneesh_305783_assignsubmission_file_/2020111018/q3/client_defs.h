#ifndef __ADJDSKJFHLKSJDFHDS
#define __ADJDSKJFHLKSJDFHDS

#include <string>
#include <bits/stdc++.h>
#include <vector>

struct s_request{
    int id;
    int delay;
    char type;
    int key1;
    int key2;
    std::string value;
};
typedef s_request Request;

int print_q(std::vector<Request> v, int num_request);

int parse_requests(std::vector<Request> *v, int num_requests);
int connect_to_dict(int portno);                                // connects to the server, returns the socket fd
int send_request(Request r, int sockfd);
int send_exit_request(int portno);

void* request_thread(void* vp_r);

#endif

/*
11
1 insert 21 hello
2 insert 21 hello
2 insert 22 yes
2 insert 23 no
3 concat 21 22
3 concat 21 23
4 delete 23
5 delete 24
6 concat 21 24
7 update 21 final
8 concat 21 22


2
1 insert 1 hello
2 insert 1 bye

*/