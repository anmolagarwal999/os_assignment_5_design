compile by running 'make'
clear executables by running 'make clean'

execute server with './server {num worker} {port number}'
execute server with './client {port number}

The files related to the server and the dictionary are defined in server.cpp, dictionary.cpp and dictionary.h

The dictionary is defined as an array of struct Entry s
Each entry consists of an int id, string value and a mutex to prevent race conditions.
Operations done on the dictionary are defined by functions starting with 'dictionary'. Each of them locks the mutex of the Entry they access. Concat first accesses the lower keys entry and then the higher keys entry. This prevents deadlocks as there is no way for two concats to wait on each others mutexes.

On starting the server, it first creates 'num_worker' worker threads and stores each of their tids. Each worker is defined by a struct Worker that stores an int id, its pthread, a bool is_busy and a mutex to access it.
The main thread starts listening for connect requests. When one is found, it pushes the FILE ptr to a globally defined queue and posts a semaphore job_available. This wakes up a free worker thread. The worker thread pops the file ptr and reads the request sent to it (by a client request thread). It parses the message and runs the appropriate function on the dictionary. These functions return a 0 for a success and a 1 for a failure. If a value has to be returned, a pair of <0/1, value> is returned instead. Depending on the returned values, the worker thread sleeps for 2 seconds and then writes to the FILE*, closes it and waits on the job_available semaphore again. If all threads are occupied, file ptrs are stored in the queue and the semaphore count keeps increasing.

The format for the passed message is:

command_code key1 {key2/value}

the command codes are:
0 insert
1 delete
2 update
3 concat
4 fetch
5 exit (has no other parameters and is used to shut down the server)

The files and functions related to the client are client.cpp, client_defs.cpp and client_defs.h
On starting the client, it parses the input and stores each request in a vector of Request structs with parse_requests(). These store the parameters of each request along with a request id. The main thread then creates 'num_requests' threads, each of which accesses one request. The main thread then waits It sleeps to simulate the delay, connects to the server with connect_to_dict(), constructs the message to be sent to the server and sends it with send_request. After every request has been sent, the main thread then executes send_exit_request. This sends an exit message to the server twice, to ensure it shuts down.

When the server receives an exit code, it updated the global bool signalling that all input is cleared. On the next accpeted connection, it breaks instead killing all worker threads.