#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <vector>
#include "client_defs.h"

int portno;

int main(int argc, char** argv){
    if (argc != 2){
        std::cout << "Usage: ./client <portno>\n";
        return 1;
    }

    portno = atoi(argv[1]);

    std::vector<Request> v;
    int num_requests;

    std::cin >> num_requests;

    if(parse_requests(&v, num_requests)){
        std::cout << "Error in reading input\n";
        return 1;
    }

    // print_q(v, num_requests);

    pthread_t request_tids[num_requests];
    for (int i = 0; i < num_requests; i++){
        pthread_create(&request_tids[i], NULL, request_thread, (void*) &(v.at(i)));
    }    

    for (int i = 0; i < num_requests; i++){
        pthread_join(request_tids[i], NULL);
    }

    std::cout << "exiting\n";
    if(send_exit_request(portno)){
        std::cout << "error in exiting\n";
        return 1;
    }

    return 0;
}

void* request_thread(void* vp_r){
    Request r = *(Request*) vp_r;

    sleep(r.delay);
    int sockfd = connect_to_dict(portno);
    if (sockfd < 0){
        std::cout << "Connection error in request thread " << r.id << "\n";
        return NULL;
    }

    int err = send_request(r, sockfd);
    if (err < 0){
        std::cout << "Error while communicating with server\n";
        return NULL;
    }

    return NULL;
}