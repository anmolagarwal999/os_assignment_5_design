#include <iostream>
#include <bits/stdc++.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "dictionary.h"

Entry dict[DICT_SIZE];
std::queue<FILE*> socketFP_queue;
pthread_mutex_t mutex_queue;

bool all_processed = false;
pthread_mutex_t mutex_all_processed;
Worker *worker_status;
sem_t job_available;

bool input_finished = false;
pthread_mutex_t mutex_input_finished;

void* worker_thread(void* vp_id);

int main(int argc, char** argv){
    // argv[1] holds port to use
    if (argc != 3){
        std::cout << "Usage: ./server <n> <port number>";
        return 1;
    }
    // init all
    for (int i = 0; i < DICT_SIZE; i++){
        dict[i].key = i;
        dict[i].in_use = false;
        dict[i].value = "";
        pthread_mutex_init(&dict[i].mutex, NULL);
    }
    
    pthread_mutex_init(&mutex_input_finished, NULL);
    pthread_mutex_init(&mutex_all_processed, NULL);
    pthread_mutex_init(&mutex_queue, NULL);
    sem_init(&job_available, 0, 0);

    int port_no = atoi(argv[2]);
    int num_workers = atoi(argv[1]);

    worker_status = (Worker*) malloc(sizeof(Worker) * num_workers); 
    for (int i = 0; i < num_workers; i++){
        worker_status[i].id = i;
        pthread_mutex_init(&worker_status[i].mutex, NULL);
        worker_status->is_busy = false;
        pthread_create(&(worker_status[i].tid), NULL, worker_thread, &worker_status[i]);
    }

    // set up ports
    int acceptfd, commfd, clilen, n;
    char buf[256];

    struct sockaddr_in serv_addr, cli_addr;

    // create a socket, store fd in acceptfd
    acceptfd = socket(AF_INET, SOCK_STREAM, 0);
    if (acceptfd < 0){
        perror("scoket error: ");
        std::cout << "Error in socket\n";
        return 1;
    }

    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port_no);
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    // serv_addr.sin_zero is just zero padding

    // bind fd to socket
    if (bind(acceptfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0){
        perror("bind error: ");
        return 1;
    }
    else
        std::cout << "server bound\n";

    // listen to that boundfile no.
    listen(acceptfd, num_workers);

    // keep listening for clients
    while(1){
        pthread_mutex_lock(&mutex_input_finished);
        if (input_finished){
            pthread_mutex_unlock(&mutex_input_finished);
            break;
        }
        pthread_mutex_unlock(&mutex_input_finished);

        clilen = sizeof(cli_addr);
        commfd = accept(acceptfd, (struct sockaddr*) &cli_addr, (socklen_t*) &clilen);
        if (commfd < 0){
            std::cout << "Error in accept\n";
            return 1;
        }
        FILE* commFP = fdopen(commfd, "r+");

        sem_post(&job_available);
        socketFP_queue.push(commFP);
    }
        
    // clear dict
    for (int i = 0; i < DICT_SIZE; i++)
        pthread_mutex_destroy(&dict[i].mutex);


    for(int i = 0 ; i < num_workers; i++){
        pthread_mutex_destroy(&worker_status[i].mutex);
    }
        free(worker_status);

    return 0;
}

void* worker_thread(void* vp_id){
    // FILE* commFP = (FILE*) vp_fd;
    Worker* w = (Worker*) vp_id;

    while(1){
        sem_wait(&job_available);
        pthread_mutex_lock(&mutex_queue);
        FILE* commFP = socketFP_queue.front();
        socketFP_queue.pop();
        pthread_mutex_unlock(&mutex_queue);

        pthread_mutex_lock(&w->mutex);
        w->is_busy = true;
        pthread_mutex_unlock(&w->mutex);

        // fprintf(commFP ,"thread created\n");
        // fflush(commFP);

        // std::cout << "worker thread " << w->id << " processing command\n";

        // init buffer
        char* stor = (char*) malloc(256);
        bzero(stor, 256);
        size_t n = 256;

        // read msg
        int char_read = getline(&stor, &n, commFP);
        stor[char_read - 1] = '\0';

        // std::cout << "Message recieved: " << stor << "\n";
        // transmit ack and exit routine
        if (char_read < 0){
            std::cout << "read error\n";
            fclose(commFP);
            return NULL;
        }

        // n = fprintf(commFP, "Message recieved: \"%s\"!", stor);
        // if (n < 0){
        //     std::cout << "write error\n";
        //     fclose(commFP);
        //     return NULL;
        // }

        // modify dict
        sleep(2);

        int command;
        int key1 = 0, key2 = 0;
        char value[256];
        bzero((void*) value, 256);

        int ret = 0;
        std::pair<int, std::string> p;

        // send tid
        fprintf(commFP, "%d:", w->id);

        command = stor[0] - '0';
        char* command_info = &(stor[2]);
        switch (command){
            case 0:
                sscanf(command_info, "%d %s", &key1, value);
                ret = operation_insert(dict, key1, value);
                if(!ret){
                    fprintf(commFP, "Insertion successful!");
                }
                else{
                    fprintf(commFP, "Key already exists!");
                }

                
                break;

            case 1:
                sscanf(command_info, "%d", &key1);
                ret = operation_delete(dict, key1);
                if(!ret){
                    fprintf(commFP, "Deletion successful");
                }
                else{
                    fprintf(commFP, "Key doesn't exist");
                }

                
                break;

            case 2:
                sscanf(command_info, "%d %s", &key1, value);
                p = operation_update(dict, key1, value);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "Key doesn't exist");
                }

                
                break;

            case 3:
                sscanf(command_info, "%d %d", &key1, &key2);
                p = operation_concat(dict, key1, key2);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "At least one of the keys doesn't exist");
                }

                
                break;

            case 4:
                sscanf(command_info, "%d", &key1);
                p = operation_fetch(dict, key1);
                if(!p.first){
                    fprintf(commFP, "%s", p.second.c_str());
                }
                else{
                    fprintf(commFP, "The key doesn't exist");
                }
                
                
                break;

            case 5:
            default:
                pthread_mutex_lock(&mutex_input_finished);
                input_finished = true;
                pthread_mutex_unlock(&mutex_input_finished);
                fclose(commFP);
                return NULL;
                break;
        }

        fflush(commFP);
        fclose(commFP);
    }
}

/*
void* worker_thread(void* args){
    while(1){
        int command;
        int key1, key2;
        std::string value;

        std::pair<int, std::string> p;

        std::cin >> command;
        switch (command){
            case 0:
                std::cin >> key1 >> value;
                std::cout << "Insert returned: " << operation_insert(dict, key1, value) << "\n";
                break;
            case 1:
                std::cin >> key1;
                std::cout << "Delete returned: " << operation_delete(dict, key1) << "\n";
                break;
            case 2:
                std::cin >> key1 >> value;
                p = operation_update(dict, key1, value);
                std::cout << "Update returned: " << p.first << " and " << p.second << "\n";
                break;
            case 3:
                std::cin >> key1 >> key2;
                p = operation_concat(dict, key1, key2);
                std::cout << "concat returned: " << p.first << " and " << p.second << "\n";
                break;
            case 4:
                std::cin >> key1;
                p = operation_fetch(dict, key1);
                std::cout << "fetch returned: " << p.first << " and " << p.second << "\n";
                break;
            case 5:
            default:
                return NULL;
                break;
        } 

        fflush(stdin);
    }
}
*/