#include <iostream>
#include <bits/stdc++.h>
#include <pthread.h>
#include <unistd.h>

    int count = 0;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void* thread(void* p_count){
    int *p = (int*) p_count; 
    for(int i = 0; i < 5; i++){
        pthread_mutex_lock(&m);
        std::cout << *p << " updating count to " << ++(*&count) << '\n';
        pthread_mutex_unlock(&m);
    }
    return NULL;
}

int main(void){

    pthread_t tds[5];

    pthread_mutex_init(&m, NULL);

    for (int i = 0; i < 5; i ++){
        std::pair<int, int*> p;
        // p.first = i;
        // p.second = &count;
        pthread_create(&tds[i], NULL, thread, (void*) &i);
        // sleep(1);
    }

    for(int i = 0; i < 5; i++)
        pthread_join(tds[i], NULL);

    std::cout << "final count is " << count <<'\n';
    
}

