#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <bits/stdc++.h>

#include "client_defs.h"

int print_q(std::vector<Request> v, int num_request){
    for (int i = 0; i < num_request; i++){
        Request r = v.at(i);

        std::cout << "Request " << r.id << ":\n";
        std::cout << "delay: " << r.delay << " type: " << r.type << "\n";
        std::cout << r.key1 << " " << r.key2 << " " << r.value << "\n\n";
    }

    return 0;
}

int parse_requests(std::vector<Request> *v, int num_requests){
    for (int i = 0; i < num_requests; i++){
        Request r;

        char command[20];
        command[0] = '\0';
        r.id = i;
        r.key1 = 0;
        r.key2 = 0;
        r.value = "";

        scanf("%d %s", &r.delay, command);

        if (!strcmp(command,"insert"))
            r.type = '0';
        else if(!strcmp(command,"delete"))
            r.type = '1';
        else if(!strcmp(command,"update"))
            r.type = '2';
        else if(!strcmp(command,"concat"))
            r.type = '3';
        else if(!strcmp(command,"fetch"))
            r.type = '4';
        else{
            return 1;
        }

        switch(r.type){
            case '0':         // add
            case '2':         // update
                std::cin >> r.key1 >> r.value;
                break;
            case '1':         // del
            case '4':         // fetch
                std::cin >> r.key1;
                break;
            case '3':         // concat
                std::cin >> r.key1 >> r.key2;
                break;
            case '5':         // exit simulation
                break;
            default:
                std::cout << "Invalid input";
                return -1;
                break;
        }

        (*v).push_back(r);
    }

    return 0;
}

int connect_to_dict(int portno){
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buf[256];
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server = gethostbyname("localhost");

    // setup serv_addr
    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char*) server->h_addr, (char*) &serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        std::cout << "error in connecting\n";
        return -1;
    }

    // std::cout << "successfully connected\n";

    return sockfd;
}

std::string my_itoa(int x){
    char buf[100];
    sprintf(buf, "%d", x);
    std::string s = buf;
    return s;
}

int send_request(Request r, int sockfd){
    std::string msg = "";
    msg += r.type;

    switch(r.type){
        case '0':         // add
        case '2':         // update
            msg += " ";
            msg += my_itoa(r.key1);
            msg += " ";
            msg += r.value;
            break;
        case '1':         // del
        case '4':         // fetch
            msg += " ";
            msg += my_itoa(r.key1);
            break;
        case '3':         // concat
            msg += " ";
            msg += my_itoa(r.key1);
            msg += " ";
            msg += my_itoa(r.key2);
            break;
        case '5':         // exit simulation
            break;
    }

    // left over for as server.cpp uses getline
    msg += '\n';

    int n = dprintf(sockfd, "%s", msg.c_str());
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    char buf[256];
    bzero(buf,256);
    n = read(sockfd,buf,255);
    if (n < 0) {
        std::cout << "error in reading\n";
        return 1;
    }
    printf("%d:%s\n", r.id, buf);

    return 0;
}

int send_exit_request(int portno){
    char msg[4] = "5\n";

    int sockfd = connect_to_dict(portno);    
    int n = dprintf(sockfd, "%s", msg);
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    sleep(3);

    sockfd = connect_to_dict(portno);    
    n = dprintf(sockfd, "%s", msg);
    if (n < 0) {
        std::cout << "error in writing\n";
        return 1;
    }

    return 0;
}