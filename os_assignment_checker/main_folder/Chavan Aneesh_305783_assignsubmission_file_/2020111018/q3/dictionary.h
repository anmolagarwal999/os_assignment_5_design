#ifndef __ASSN_DICT_HEEEEEEEEEEADER
#define __ASSN_DICT_HEEEEEEEEEEADER

#include <pthread.h>
#include <bits/stdc++.h>
#include <stdlib.h>

#define DICT_SIZE 101
#define LISTENPORTNO 8005

struct s_dict_entry{
    int key;
    bool in_use = false;
    std::string value;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
};
typedef struct s_dict_entry Entry;

struct s_worker_status{
    int id;
    pthread_t tid;
    bool is_busy;
    pthread_mutex_t mutex;
};
typedef struct s_worker_status Worker;

int                         operation_insert(Entry *dictionary, unsigned int key, std::string value);
int                         operation_delete(Entry *dictionary, unsigned int key);
std::pair<int, std::string> operation_update(Entry *dictionary, unsigned int key, std::string value);
std::pair<int, std::string> operation_concat(Entry *dictionary, unsigned int key1, unsigned int key2);
std::pair<int, std::string> operation_fetch(Entry *dictionary, unsigned int key);

#endif