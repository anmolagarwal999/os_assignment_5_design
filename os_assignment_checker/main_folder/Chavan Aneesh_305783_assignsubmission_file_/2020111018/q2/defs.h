#ifndef __DEFS__
#define __DEFS__

#include <pthread.h>
#include <semaphore.h>

#define FAN_COLOR               "\x1b[31m"
#define FAN_LEAVES_COLOR       "\x1b[34m"
#define ZONE_COLOR                    "\x1b[32m"
#define GROUP_COLOR                "\x1b[33m"
#define GAME_COLOR                   "\x1b[35m"
#define NORMAL_COLOR                "\x1b[0m"


enum teams{home, away, neutral};

struct s_goal_token{
    int team;
    int time;
    float prob;
};
typedef struct s_goal_token Goal;

struct s_zone{
    int team;                          // 'H' or 'A' or 'N'
    int capacity;                       

    // int* p_cnt_waiting;                 // how many are waiting for a ticket
    // pthread_mutex_t mutex_waiting;       

    int* p_cnt_seated;                  // how many people seated
    pthread_mutex_t mutex_seated;       // respective mutex

    sem_t fan_leaves;                    // wakes the thread up when a fan leaves
                                        // used to evaluate whether the cnd_variables are to be signalled
};
typedef struct s_zone Zone;

struct s_fan{
    int id;

    int team;
    char* name;
    int delay;                          // T 
    int patience;                       // P
    int tilt;

    int group_num;

    sem_t waiting;
};
typedef struct s_fan Fan;

struct s_group{
    int id;

    Fan* list;
    int members;            // number ofpeople

    sem_t* members_waiting;

    int* p_waiting;
    pthread_mutex_t mutex_waiting;
    pthread_cond_t cond_waiting;
};
typedef struct s_group Group;

void print_group(Group G);
void print_fan(Fan f);
void print_zones(Zone* p_z);
void print_goals(Goal *p_g, int num_goals);

void* zone_thread(void *vp_z_id);           // contains the zone
void* fan_thread(void *vp_fan_coords);      // int[2] containing group id and fan id
void* game_thread(void*);
void* group_thread(void *vp_g_id);

void wakeup_fans(int zone_id);

#endif

/*

2 1 2
4
3
3
A N 3 2 -1
B H 1 3 2
C A 2 1 4
4
D H 1 2 4
E N 2 1 -1
F A 1 2 1
G N 3 1 -1
4
t1 H 1 2 8
t2 H 1 2 8
t3 H 1 2 8
t4 H 1 2 8
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4

*/

/*

H.cap  A.cap   N.cap
patience
group_num

group 1 size
name team delay patience tilt
.
.
.
last group
...
<Few lines describing the  last group>

num tokens
1st token team   time   prob

*/