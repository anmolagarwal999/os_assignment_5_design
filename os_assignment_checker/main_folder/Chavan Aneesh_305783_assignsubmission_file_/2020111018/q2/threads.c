#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

// extern all global variables from main.c
extern int* p_home_score;
extern int* p_away_score;
extern pthread_mutex_t mutex_score;

extern pthread_cond_t cond_goal_scored;
extern pthread_mutex_t mutex_goal_scored;

// whether there is a seat free for some group of fans
extern int seat_free_in_zone[3];

// signals a group of fans if a seat is free for them
extern pthread_cond_t cond_seat_free[3];
extern pthread_mutex_t mutex_seat_free[3];

extern Goal* goals;
extern Zone zones[3];
extern Group* groups;
extern int num_tokens;
extern int X;

// simulates goals and wakes up spectators watching
void* game_thread(void* v){
    printf(GAME_COLOR"The match has started\n"NORMAL_COLOR);

    srand(time(NULL));

    // wait until each token
    for(int cnt_token = 0; cnt_token < num_tokens; cnt_token++){
        if (cnt_token == 0)
            sleep(goals[cnt_token].time);
        else{
            int dead_time = goals[cnt_token].time - goals[cnt_token-1].time;
            sleep(dead_time);
        }

        // goal scored?
        float roll = (float) rand()/RAND_MAX;

        if (roll <= goals[cnt_token].prob){
            ++(*(goals[cnt_token].team == home ? p_home_score : p_away_score));

            printf(GAME_COLOR"The %s team has scored! The score is %d-%d\n"NORMAL_COLOR, 
                     (goals[cnt_token].team == home ? "home" : "away"), *p_home_score, *p_away_score);
        }
        else {
            printf(GAME_COLOR"The %s team missed their a chance at a goal! The score is still %d-%d\n"NORMAL_COLOR, 
                   (goals[cnt_token].team == home ? "home" : "away"), *p_home_score, *p_away_score);
        }

        pthread_mutex_lock(&mutex_goal_scored);
        pthread_cond_broadcast(&cond_goal_scored);
        pthread_mutex_unlock(&mutex_goal_scored);
    }
}

// signals fans that a ticket is available
void* zone_thread(void *vp_z_id){
    int zone_id = *(int*) vp_z_id;
    Zone *z = &zones[zone_id];
    printf(ZONE_COLOR"Zone %d starting\n"NORMAL_COLOR, zone_id);

    // keep broadcasting until the zone is full
    while(1){
        pthread_mutex_lock(&z->mutex_seated);
        if (*z->p_cnt_seated < z->capacity){
            pthread_mutex_unlock(&z->mutex_seated);

            // alert all appropirate threads that a seat is free
            wakeup_fans(z->team);
        }
        else {
            // zone is filled, exit while loop
            assert(*z->p_cnt_seated <= z->capacity);
            break;
        }
    }

    // printf(ZONE_COLOR"Zone filled, entering wait mode %d\n"NORMAL_COLOR, z->team);

    // zone now waits for a fan to leave before broadcasting cond
    pthread_mutex_unlock(&z->mutex_seated);
    while(1){
        sem_wait(&z->fan_leaves);

        printf(ZONE_COLOR"A fan has left zone %d\n"NORMAL_COLOR, z->team);
        pthread_mutex_lock(&z->mutex_seated);
        (*z->p_cnt_seated)--;
        pthread_mutex_unlock(&z->mutex_seated);

        wakeup_fans(z->team);
    }
}

void wakeup_fans(int zone_id){
    switch(zone_id){
        case home:
        case neutral:
            pthread_mutex_lock(&mutex_seat_free[home]);
            pthread_cond_broadcast(&cond_seat_free[home]);
            pthread_mutex_unlock(&mutex_seat_free[home]);
            
            pthread_mutex_lock(&mutex_seat_free[neutral]);
            pthread_cond_broadcast(&cond_seat_free[neutral]);
            pthread_mutex_unlock(&mutex_seat_free[neutral]);

            break;

        case away:
            pthread_mutex_lock(&mutex_seat_free[away]);
            pthread_cond_broadcast(&cond_seat_free[away]);
            pthread_mutex_unlock(&mutex_seat_free[away]);
            
            pthread_mutex_lock(&mutex_seat_free[neutral]);
            pthread_cond_broadcast(&cond_seat_free[neutral]);
            pthread_mutex_unlock(&mutex_seat_free[neutral]);
            
            break;

        default:
            printf("incorrect team for wakeup_fans\n");
            assert(0);
    }
}

void* fan_thread(void *vp_fan_coords){
    int* coords = (int*) vp_fan_coords;
    Fan *f = &groups[coords[0]].list[coords[1]];

    printf("(%d,%d)\n", f->group_num, f->id);

    sleep(f->delay);
    printf(FAN_COLOR"%s (%d,%d) has reached the stadium\n"NORMAL_COLOR,
            f->name, f->group_num, f->id);
    
    // fan casually approaches ticketbooth after delay, set timespec to wait until patience runs out
    int seated = -1, err = 0;
    struct timespec wait_until;
    clock_gettime(CLOCK_REALTIME, &wait_until);
    wait_until.tv_sec += f->patience;

    // wait until a seat is available or times out
    while(seated == -1){
        // the entire process is a critical section, mutex_seat_free used for this
        pthread_mutex_lock(&mutex_seat_free[f->team]);
        err = pthread_cond_timedwait(&cond_seat_free[f->team], &mutex_seat_free[f->team], &wait_until);

        if (err == 0){
            // seat is available, try and look for a seat in allowed zones
            if (f->team == home || f->team == neutral){
                int available = 0;
                pthread_mutex_lock(&zones[home].mutex_seated);
                if(*zones[home].p_cnt_seated  < zones[home].capacity){
                    // seat available, buying it rn
                    (*zones[home].p_cnt_seated)++;
                    seated = home;
                }
                pthread_mutex_unlock(&zones[home].mutex_seated);

                if (seated == -1){
                    pthread_mutex_lock(&zones[neutral].mutex_seated);
                    if(*zones[neutral].p_cnt_seated  < zones[neutral].capacity){
                        // seat available, buying it rn
                        (*zones[neutral].p_cnt_seated)++;
                        seated = neutral;
                    }
                    pthread_mutex_unlock(&zones[neutral].mutex_seated);
                }
            }

            if (f->team == away || f->team == neutral){
                if (seated == -1){
                    pthread_mutex_lock(&zones[away].mutex_seated);
                    if(*zones[away].p_cnt_seated  < zones[away].capacity){
                        // seat available, buying it rn
                        (*zones[away].p_cnt_seated)++;
                        seated = away;
                    }
                    pthread_mutex_unlock(&zones[away].mutex_seated);
                }
            }
        }
        else if (err == ETIMEDOUT){
            pthread_mutex_unlock(&mutex_seat_free[f->team]);
            break;
        }
        else {
            printf("fan %d group %d timed cond wait error\n", f->id, f->group_num);
            assert(0);
        }

        pthread_mutex_unlock(&mutex_seat_free[f->team]);
    }   

    // fan has been seated in zone 'seated'
    if (seated != -1){
        
        switch(seated){
            case home:
                printf(ZONE_COLOR"%s has been seated in the home zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            case neutral:
                printf(ZONE_COLOR"%s has been seated in the neutral zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            case away:
                printf(ZONE_COLOR"%s has been seated in the away zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            default:
                assert(0);
                break;
        }

        // prepare timespec to mark leaving time
        int time_to_go = 0;
        struct timespec watch_until;
        clock_gettime(CLOCK_REALTIME, &watch_until);
        watch_until.tv_sec += X;

        // loop to simulate watching the match
        while(1){
            // check if team is inting
            if (f->team == home || f->team == away){
                pthread_mutex_lock(&mutex_score);
                int frustrated = *(f->team == home ? p_away_score : p_home_score) >= f->tilt;
                pthread_mutex_unlock(&mutex_score);

                if (frustrated){
                    printf(FAN_COLOR"%s is leaving zone %d due to his teams poor performance\n"NORMAL_COLOR, f->name, seated);
                    sem_post(&zones[seated].fan_leaves);
                    break;
                }
            }

            // wait until a goal is scored, check for ragequit or timeout
            pthread_mutex_lock(&mutex_goal_scored);
            time_to_go = pthread_cond_timedwait(&cond_goal_scored, &mutex_goal_scored, &watch_until);

            if(time_to_go == ETIMEDOUT){
                pthread_mutex_unlock(&mutex_goal_scored);
                printf(FAN_COLOR"%s watched the match for %d seconds and is leaving zone %d\n"NORMAL_COLOR, f->name, X, seated);
                sem_post(&zones[seated].fan_leaves);
                break;
            }
            // if not timed out, loops and checks the score again
            pthread_mutex_unlock(&mutex_goal_scored);
        }
    }

    // no seat, fan has timed out and is leaving
    else {
        assert(err == ETIMEDOUT);
        printf(FAN_COLOR"%s couldn't get a seat\n"NORMAL_COLOR,
            f->name);
    }

    printf(FAN_LEAVES_COLOR"%s is waiting for their group\n"NORMAL_COLOR, f->name);

    sem_post(&groups[f->group_num].members_waiting[f->id]);    

    
    sem_wait(&f->waiting);
    // pthread_mutex_unlock(&groups[f->group_num].mutex_waiting);

    return NULL;
}

void* group_thread(void *vp_g_id){
    int group_id = *(int*) vp_g_id;

    Group G = groups[group_id];

    for (int i = 0; i < G.members; i++){
        sem_wait(&G.members_waiting[i]);
    }

    printf(GROUP_COLOR"Group %d is leaving\n"NORMAL_COLOR, group_id + 1);

    for (int i = 0; i < G.members; i++){
        sem_post(&G.list[i].waiting);
    }

    return NULL;
}