#include "defs.h"
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

int* p_home_score = NULL;                           // ints storing the score
int* p_away_score = NULL;
pthread_mutex_t mutex_score = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t cond_goal_scored = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_goal_scored = PTHREAD_MUTEX_INITIALIZER;

// cond variables that zone threads broadcast when a seat opens up
int seat_free_in_zone[3];
pthread_cond_t cond_seat_free[3];
pthread_mutex_t mutex_seat_free[3];

int num_tokens;
Goal* goals = NULL;
Zone zones[3];

Group* groups = NULL;
int X;

int main(void){
//-------------------------------------init entities--------------------------------------------------------------------------

    // init scores
    p_home_score = (int*) malloc(sizeof(int));
    p_away_score = (int*) malloc(sizeof(int));
    *p_home_score = 0;
    *p_away_score = 0;
    pthread_mutex_init(&mutex_score, NULL);

    pthread_cond_init(&cond_goal_scored, NULL);
    pthread_mutex_init(&mutex_goal_scored, NULL);

    // init zones and related sems/mutexes/cvs
    for(int i = 0; i < 3; i++){
        seat_free_in_zone[i] = 0;
        pthread_cond_init(&cond_seat_free[i], NULL);
        pthread_mutex_init(&mutex_seat_free[i], NULL);

        zones[i].team = i;
        zones[i].p_cnt_seated = (int*) malloc(sizeof(int));
        *zones[i].p_cnt_seated = 0;

        pthread_mutex_init(&zones[i].mutex_seated, NULL);
        sem_init(&zones[i].fan_leaves, 0, 0);
    }

//--------------------------------------parse input-------------------------------------------------------
    scanf("%d %d %d", &(zones[home].capacity), &(zones[away].capacity), &(zones[neutral].capacity));
    scanf("%d", &X);

    // parse groups
    int num_groups;
    scanf("%d", &num_groups);
    groups = (Group*) malloc(sizeof(Group) * num_groups);

    for(int i = 0; i < num_groups; i++){
        int members;
        scanf("%d", &members);
        groups[i].id = i;
        groups[i].list = (Fan*) malloc(sizeof(Fan) * members);
        groups[i].members = members;
        
        groups[i].p_waiting = (int*) malloc(sizeof(int));

        groups[i].members_waiting = (sem_t*) malloc(sizeof(sem_t) * members);

        Fan* fl = groups[i].list;

        for(int j = 0; j < members; j++){
            sem_init(&groups[i].members_waiting[j], 0, 0);

            sem_init(&groups[i].list[j].waiting, 0, 0);

            char name[100];
            char team;
            int P, T, delay;
            scanf("%s %c %d %d %d", name, &team, &delay, &P, &T);

            fl[j].id = j;
            fl[j].delay = delay;
            fl[j].group_num = i;
            fl[j].patience = P;
            fl[j].tilt = T;

            fl[j].name = (char*) malloc(strlen(name) + 1);
            strcpy(fl[j].name, name);

            switch(team){
                case 'H':
                    fl[j].team = home;
                    break;
                case 'A':
                    fl[j].team = away;
                    break;
                case 'N':
                    fl[j].team = neutral;
                    break;
                default:
                    printf("Error in reading fan %d,%d\n", i, j);
                    assert(0);
                    break;
            }
        }
    }

    // parse goals
    scanf("%d\n", &num_tokens);
    goals = (Goal*) malloc(sizeof(Goal) * num_tokens);

    for(int i = 0; i < num_tokens; i++){
        char buf, team;                     // buf is to read the extra \n
        int time;
        float prob;

        scanf("\n%c %d %f", &team, &time, &prob);

        if (team == 'H')
            goals[i].team = home;
        else if (team == 'A')
            goals[i].team = away;
        else{
            printf("%c read\n", team);
            assert(0);
        }

        goals[i].time = time;
        goals[i].prob = prob;
    }


//-------------------------------------init threads--------------------------------------------------------------------------                                        
    pthread_t zone_tids[3];
    for (int i = 0; i < 3; i++){
        pthread_create(&zone_tids[i], NULL, zone_thread, (void*) &zones[i].team);

        printf("Zone %d created\n", i);
    }
    
    pthread_t** fan_tids = (pthread_t**) malloc(sizeof(pthread_t) * num_groups);
    pthread_t* group_tids = (pthread_t*) malloc(sizeof(pthread_t) * num_groups);

    for (int i = 0; i < num_groups; i++){
        fan_tids[i] = (pthread_t*) malloc(sizeof(pthread_t) * groups[i].members);
        Group g = groups[i];

        printf("group %d created\n", i);

        pthread_create(&group_tids[i], NULL, group_thread, (void*) &groups[i].id);

        for(int j = 0; j < groups[i].members; j++){

            int *coords = (int*) malloc(sizeof(int) * 2);
            coords[0] = i; coords[1] = j; 

            int err = pthread_create(&fan_tids[i][j], NULL, fan_thread, (void*) coords);
            if(err != 0){
                perror("Fan thread");
            }

            printf("Fan %s created\n", g.list[j].name);
        }
    }

    pthread_t game_tid;
    pthread_create(&game_tid, NULL, game_thread, NULL);

//-------------------------------------wait and cleanup--------------------------------------------------------------------------                        

    for (int i = 0; i < num_groups; i++){
        pthread_join(group_tids[i], NULL);
        for(int j = 0; j < groups[i].members; j++)
            pthread_join(fan_tids[i][j], NULL);
    }

    printf("The simulation is over, all fans have exited\n");

    pthread_cancel(game_tid);
    for(int i = 0; i < 3; i++)
        pthread_cancel(zone_tids[i]);

    // cleanup

    free(p_away_score);
    free(p_home_score);
    pthread_mutex_destroy(&mutex_score);

    pthread_cond_destroy(&cond_goal_scored);
    pthread_mutex_destroy(&mutex_goal_scored);

    for(int i = 0; i < 3; i++){
        pthread_cond_destroy(&cond_seat_free[i]);
        pthread_mutex_destroy(&mutex_seat_free[i]);

        free(zones[i].p_cnt_seated);
        pthread_mutex_destroy(&zones[i].mutex_seated);

        sem_destroy(&zones[i].fan_leaves);
    }    

    for(int i = 0; i < num_groups; i++){
        Fan* fl = groups[i].list;
        for(int j = 0; j < groups[i].members; j++){
            free(fl[j].name);
            sem_destroy(&groups[i].members_waiting[j]);

        }
        free(fan_tids[i]);
        free(fl);

        free(groups[i].members_waiting);
    }

    free(group_tids);
    free(fan_tids);

    free(groups);
    free(goals);
}