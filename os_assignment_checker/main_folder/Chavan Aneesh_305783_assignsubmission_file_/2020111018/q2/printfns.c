#include "defs.h"
#include <stdio.h>
#include <stdlib.h>

void print_fan(Fan f){
    printf("%s, team: %d\n", f.name, f.team);
    printf("delay: %d   patience: %d   tilt: %d\n", f.delay, f.patience, f.tilt);
    printf("group %d\n", f.group_num);
}

void print_group(Group G){
    printf("%d members\n", G.members);
    for(int i = 0; i < G.members; i++){
        print_fan(G.list[i]);
        printf("\n");
    }
}

void print_zones(Zone *p_z){
    for(int i = 0; i < 3; i++){
        printf("Team: %d, capacity: %d\n", p_z[i].team, p_z[i].capacity);
        pthread_mutex_lock(&p_z[i].mutex_seated);
        printf("%d seated currently\n\n", *p_z[i].p_cnt_seated);
        pthread_mutex_unlock(&p_z[i].mutex_seated);
    }
}

void print_goals(Goal *p_g, int num_goals){
    for(int i = 0; i < num_goals; i++){
        Goal g = p_g[i];
        printf("team: %d, time: %d, prob: %f\n", g.team, g.time, g.prob);
    }
}