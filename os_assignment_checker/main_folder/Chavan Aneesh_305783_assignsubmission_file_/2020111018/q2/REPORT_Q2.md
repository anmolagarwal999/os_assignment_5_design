Compile by executing 'make'
Execute using ./q2

I have implemented the bonus part

<hr>
<h3>Colours list</h3>
<ul>
<li>Fan: Red</li>
<li>Fans leaving the simulation: Blue</li>
<li>Zones: Green</li>
<li>Group: Yellow</li>
<li>Goals: Purple</li>
<li>General/main.c: White</li>
</ul>

<hr>
<br>

<h2>Overview</h2>

<p>
Similar to question 1, all entities have a struct associated with them, declared in a global array. The strict has all attributes, related semaphore, mutexes and conditional variables.
All fans and zones are simulated by threads. There is one thread for each group that waits for all fans to exit so the group can exit together. There is also one thread that calculates whether goals are made. No thread ever busy-waits. There are some external semaphores and conditional variables used to prevent busy waiting. These are detailed later on.
</p>
<br>

<p>
Fan threads first sleep for a time corresponding to their delay. A timespec is created with the time until which they will wait. They then approach the ticketbooth and timedwait on one of the three 'cond_seat_free' CVs depending on the team they support. The CVs signal the threads when a seat opens up. THe zone threads broadcast the appropriate CVs if a seat opens up when a fan leaves a zone. After broadcasting, all woken up threads are in contention for the free seat. Each one checks if there is space in the zone by comparing the current no. seated wiht the capactity. The first one to reach the seat occupies it. The seated variable stores the zone it is seated in. If the thread times out it prints the event message and waits at the exit. 
</p>
<p>
The fan thread then enters the "watch" loop. A  timespec that stores long until they time out is created. It checks the score first and leaves if the opponent score is too high. Score and mutexes are stored in global variables. It then timed_waits on a global CV cond_goal_scored. When a goal is made this processes, the goal thread broadcasts this CV, waking up all threads. They loop and check the score again. If not frustrated, they continue timed waiting. If they are frustrated or time out, they print the appropriate message and leave the group.
</p>
<p>
They then post the semaphore members_waiting[i] in the group struct to which they belong. The group then exits when all semaphores have been posted. The simulation ends when all groups exit (assuming the match goes on even after the fans leave, and no tokens after a point doesnt imply the match ending, just a slow game).
</p>

<hr>

<p>
When a fan leaves due to frustration or timeout, they signal the fan_leaves mutex of the zone they are seated in. The zone then signals all fans that are allowed to sit there that a seat is open with the wakeup_fans function.
</p>
<p>
Goals are parsed and stored in a Group struct. THe game thread waits reads the time until the next goal and waits until then. After it wakes up it generates a random number and uses it to check if the goal attempt is successful. It broadcasts the cond_goal_scored CV. This wakes up all seated fans.
</p>
