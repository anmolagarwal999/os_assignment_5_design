#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sstream>
/////////////////////////////
#include <iostream>
#include <queue>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;

typedef struct dic
{
    int key;
    string value;
} dict;

vector<dict> vec;       //dictionary stored in server
queue<int> que_clients; //list of client requests accepted

pthread_mutex_t que_push = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t que_pop = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t signal_work = PTHREAD_COND_INITIALIZER;

///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void tokenise(string str, vector<string> &vec)
{
    vec.resize(0);
    string copy = str;
    stringstream ss(copy); //convert my_string into string strea

    vector<string> tokens;
    string temp_str;

    while (getline(ss, temp_str, ' '))
    {
        tokens.push_back(temp_str);
    }

    for (int i = 0; i < tokens.size(); i++)
    {
        vec.push_back(tokens[i]);
    }
}

void handle_connection(int client_socket_fd)
{
    int received_num, sent_num;
    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd, msg_to_send_back;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        // printf("Read something\n");
        // cout << "Client sent : " << cmd << endl;

        if (ret_val <= 0)
        {
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        vector<string> tokens;
        tokenise(cmd, tokens);
        for(int i = 0;i < tokens.size();i++)
            cout << tokens[i] << " ";
        cout << endl;

        msg_to_send_back.clear();
        if (tokens.size() == 3)
        {
            if (tokens[0] == "insert")
            {
                int test = 0;
                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[1]))
                    {
                        msg_to_send_back = "Key already exists";
                        test = 1;
                    }
                }
                if (test == 0)
                {
                    dict mypoint = {stoi(tokens[1]), tokens[2]};
                    vec.push_back(mypoint);
                    msg_to_send_back = "Insertion successful";
                }
            }
            else if (tokens[0] == "concat")
            {
                int key1 = stoi(tokens[1]);
                int key2 = stoi(tokens[2]);

                string val1, val2;
                int test1 = 0;
                int test2 = 0, in1, in2;

                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[1]))
                    {    val1 = vec[i].value;
                        in1 = i;
                        test1 = 1;
                    }
                }
                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[2]))
                    {    val2 = vec[i].value;
                        in2 = i;
                        test2 = 1;
                    }
                }

                if (test1 == 1 && test2 == 1)
                {
                    vec[in1].value = val1 + val2;
                    vec[in2].value = val2 + val1;
                    msg_to_send_back = vec[in2].value;
                }
                else
                {
                    msg_to_send_back = "Concat failed as at least one of the keys does not exist";
                }
            }
            else if (tokens[0] == "update")
            {
                int key = stoi(tokens[1]);

                string val;
                int in;
                int test;
                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[1]))
                    {    val = vec[i].value;
                        in = i;
                        test = 1;
                    }
                }

                if (test == 1)
                {
                    vec[in].value = tokens[2];
                    msg_to_send_back = vec[in].value;
                }
                else
                {
                    msg_to_send_back = "Key does not exist";
                }
            }
        }

        else if (tokens.size() == 2)
        {
            if (tokens[0] == "delete")
            {
                int key = stoi(tokens[1]);

                string val;
                int in;
                int test;
                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[1]))
                    {
                        in = i;
                        test = 1;
                    }
                }

                if (test == 1)
                {
                    vec.erase(vec.begin() + in);
                    msg_to_send_back = "Deletion successful";
                }
                else
                {
                    msg_to_send_back = "No such key exists";
                }
            }

            else if (tokens[0] == "fetch")
            {

                int key = stoi(tokens[1]);
                int in;
                int test;
                for (int i = 0; i < vec.size(); i++)
                {
                    if (vec[i].key == stoi(tokens[1]))
                    {    in = i;
                        test = 1;
                    }
                }
                if (test == 1)
                {
                    int key = stoi(tokens[1]);
                    msg_to_send_back = vec[in].value;
                }
                else 
                {
                    msg_to_send_back = "Key does not exist";
                }
            }
            else
            {
                msg_to_send_back = "Incorrect";
            }
        }
        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
        cout << msg_to_send_back << endl;
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        break;
    }
close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
}

void *working(void *arg)
{
    int client_socket_fd;
    pthread_mutex_lock(&que_pop);
    while (1)
    {
        if (que_clients.empty())
        {
            pthread_cond_wait(&signal_work, &que_pop);
        }
        else
        {
            client_socket_fd = que_clients.front();
            que_clients.pop();
            pthread_mutex_unlock(&que_pop);
            handle_connection(client_socket_fd);
        }
    }
}

int main(int argc, char *argv[])
{
    int wel_socket_fd /*listens to the requests using this socket*/, client_socket_fd /*Communicates to the client using this scoket*/, port_number /*port number used by server to listen to incoming requests*/;
    socklen_t clilen;

    int no_workers = atoi(argv[1]);
    pthread_t user_threads[no_workers]; //'m' number of user threads
    for (int i = 0; i < no_workers; i++)
        pthread_create(&user_threads[i], NULL, working, (void *)(long long int)i);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0); //intialsing a socket struct
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj)); //to specify listening addresses,ports a struct is used and intilaise to 0
    port_number = PORT_ARG;                               //port used to listen to incoming requests
    serv_addr_obj.sin_family = AF_INET;                   //AF_INET specifies whether u need ipv4 or ipv6 while communicating
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;  //mention a specific address you wanted to listen or listen to any addresses present over netwroking interface for ur local machine
    serv_addr_obj.sin_port = htons(port_number); //process specifies port(big-endian and small-endian conversion)

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    //bind the socket with all initilisations so far we created
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS); //MAX-CLIENTS you can keep them in que and 1000's of clients came just reject lol
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //**********accept is a blocking call***********
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            //destroy everything before exit
            exit(-1);
        }

        //we need to send signal to worker threads that some job has come
        //no worker thread can pop some job in the mean while time

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&que_push);
        que_clients.push(client_socket_fd);
        pthread_mutex_unlock(&que_push);

        pthread_cond_signal(&signal_work); //sending signal that job arrived
    }

    close(wel_socket_fd);
    return 0;
}