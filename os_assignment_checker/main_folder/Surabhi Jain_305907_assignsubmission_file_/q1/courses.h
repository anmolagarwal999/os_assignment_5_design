#ifndef COURSES_X
#define COURSES_X

void *initialize_course(void *ptr);

void end_tut(int c_id);

void remove_course(int c_id);
void conduct_tut(int c_id);
int course_exhausted_chances(int c_id);
int check_tut_cond_n_conduct(int c_id);
void seek_ta_n_conduct(int c_id);

#endif