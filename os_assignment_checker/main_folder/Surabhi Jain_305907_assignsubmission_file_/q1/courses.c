#include "headers.h"

void *initialize_course(void *ptr)
{

    int id = *((int *)ptr);
    // printf("inside course thread %d\n", id);

    sem_init(&(course_list[id].slot_fill_wait), 0, 1);
    sem_init(&(course_list[id].available_s), 0, 1);
    sem_init(&(course_list[id].conducting_tut), 0, 0);
    sem_init(&(course_list[id].ta_alloted), 0, 0);
    sem_init(&(course_list[id].rslots_assigned), 0, 0);

    course_list[id].ta_present = -1;
    course_list[id].filled_slots = 0;
    course_list[id].available = 1;
    course_list[id].available = 0;

    // course_list[id].available = -1;
    course_list[id].removed = 0;
    // printf(ANSI_RED "NO OF LABS FROM WHICH COURSE %d ACCEPTS TA IS _____%d\n" ANSI_RESET, id, course_list[id].no_labs);
    for (int i = 0; i < course_list[id].no_labs; i++)
    {
        course_list[id].labs[i].total_mentors = lab_list[course_list[id].labs[i].lab_id].total_mentors;
        course_list[id].labs[i].tutoring_limit = lab_list[course_list[id].labs[i].lab_id].tutoring_limit;
        for (int j = 0; j < course_list[id].labs[i].total_mentors; j++)
        {
            course_list[id].labs[i].ta_free[j] = lab_list[course_list[id].labs[i].lab_id].ta_free[j];
        }
    }

    //only seek course when the priorities have been filled
    seek_ta_n_conduct(id);
    return NULL;
}

void end_tut(int c_id)
{
    //restore slots

    //
    pthread_mutex_lock(&(course_list[c_id].mutex));
    // course_list[c_id].filled_slots = max_slots - course_list[c_id].filled_slots;
    printf("TUTORIAL ENDED\n");
    printf(ANSI_RED "TA %d from lab %d has completed the tutorial and left the course %s\n" ANSI_RESET, course_list[c_id].current_ta, course_list[c_id].labs[course_list[c_id].current_lab_id].lab_id, course_list[c_id].name);

    course_list[c_id].available = 1;

    course_list[c_id].labs[course_list[c_id].current_lab_id].ta_free[course_list[c_id].current_ta] = 1;

    course_list[c_id].current_ta = -1;
    course_list[c_id].filled_slots = 0;
    course_list[c_id].rand_slots = 0;
    course_list[c_id].ta_present = -1;
    // sem_destroy(&(course_list[c_id].rslots_assigned));
    // sem_init(&(course_list[c_id].rslots_assigned), 0, 0);

    sem_destroy(&(course_list[c_id].slot_fill_wait));
    sem_destroy(&(course_list[c_id].available_s));

    sem_destroy(&(course_list[c_id].conducting_tut));

    sem_destroy(&(course_list[c_id].ta_alloted));
    sem_destroy(&(course_list[c_id].rslots_assigned));

    sem_init(&(course_list[c_id].slot_fill_wait), 0, 1);
    sem_init(&(course_list[c_id].available_s), 0, 1);
    sem_init(&(course_list[c_id].conducting_tut), 0, 0);
    sem_init(&(course_list[c_id].ta_alloted), 0, 0);
    sem_init(&(course_list[c_id].rslots_assigned), 0, 0);

    pthread_mutex_unlock(&(course_list[c_id].mutex));
}

void remove_course(int c_id)
{

    course_list[c_id].removed = 1;
}

void conduct_tut(int c_id)
{

    // sem_wait(&(course_list[c_id].conducting_tut));

    sleep(7);

    // sem_post(&(course_list[c_id].conducting_tut));
}

int course_exhausted_chances(int c_id)
{
    for (int i = 1; i <= num_labs; i++)
    {
        for (int j = 0; j < 10; j++)
        {

            if (lab_list[i].ta_terms[j] < lab_list[i].tutoring_limit)
            {
                return 0; //no
            }
        }
        // pthread_mutex_lock(&(course_list[c_id].mutex));
        lab_list[i].active = 0;
        // pthread_mutex_unlock(&(course_list[c_id].mutex));

        printf(ANSI_BLUE "Lab %d no longer has students available for TA ship\n" ANSI_RESET, i);
    }

    printf(ANSI_YELLOW "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, course_list[c_id].name); //u can also print this whether this func returns 1
    course_list[c_id].removed = 1;
    return 1; //yes it can no longer conduct tut
}

int check_tut_cond_n_conduct(int c_id)
{

    // ○ Let W be the number of students who currently have course C as their current preference.
    // ○ If W>=D, fill the D slots and start conducting the tutorial.
    // ○ If W<D, then leave the remaining (D-W) slots unfilled and start conducting the tutorial.
    // printf(ANSI_MAGENTA "inside the check_tut_cond_n_conduct() for course %d\n " ANSI_RESET, c_id);

    pthread_mutex_lock(&(course_list[c_id].mutex));

    //     Decide D as a random integer between 1 and “course_max_slots” (inclusive).

    int d = rand_bet(1, course_list[c_id].max_slot);
    course_list[c_id].rand_slots = d;

    printf(ANSI_CYAN "Course %d has been allocated %d seats\n" ANSI_RESET, c_id, course_list[c_id].rand_slots);
    sem_post(&(course_list[c_id].rslots_assigned));
    // printf(ANSI_YELLOW "\n\n initilaizing rand_slot semaphore for course %d\n" ANSI_RESET, c_id);
    sem_init(&(course_list[c_id].f_slots), 0, course_list[c_id].rand_slots);
    int x = 0;
    sem_getvalue(&(course_list[c_id].f_slots), &x);
    // printf("course_list[c_id].f_slots sempahore value form get value fn-->%d\n", x);
    // printf("course_list[c_id].filled_slots ---> %d\n", course_list[c_id].filled_slots);

    pthread_mutex_unlock(&(course_list[c_id].mutex));
    // sem_post(&(course_list[student_list[student_id].current_pref].ta_alloted));

    //once slots are assigned wait for the students to fill in the slots

    sleep(5);
    // “Tutorial has started for Course c_i with k seats filled out of j”

    while (course_list[c_id].filled_slots == 0)
    {
        sem_wait(&(course_list[c_id].f_slots));
    }
    // sem_wait(&(course_list[c_id].f_slots));

    printf(ANSI_YELLOW "Tutorial has started for Course %d with %d seats filled out of %d\n" ANSI_RESET, c_id, course_list[c_id].filled_slots, course_list[c_id].rand_slots);

    conduct_tut(c_id);

    // TA t_j from lab l_k has completed the tutorial and left the course c_i
    end_tut(c_id);
}

void seek_ta_n_conduct(int c_id)
{

    //check if any ta is available or not
    //assumed that there is no common ta between labs

    //for all the labs in that partciular course check which ta is available
    printf(ANSI_CYAN "COURSE %d STARTING TO SEEK TA\n" ANSI_RESET, c_id);
    while (course_exhausted_chances(c_id) != 1)
    {
        //select a ta
        // printf(ANSI_CYAN "COURSE %d STARTING TO SEEK TA --inside while loop\n" ANSI_RESET, c_id);

        // printf(ANSI_MAGENTA "inside course exhaused while loop of course %d\n" ANSI_RESET, c_id);

        for (int i = 0; i < course_list[c_id].no_labs; i++)
        {
            // printf(ANSI_YELLOW "inside the 1st if statemnet course exhaused while loop of course %d\n" ANSI_RESET, c_id);

            if (lab_list[course_list[c_id].labs[i].lab_id].active == 1)
            {
                // printf(ANSI_CYAN "inside the 2st if statemnet course exhaused while loop of course %d\n" ANSI_RESET, c_id);
                // printf("TOTAL MENTORS OF LAB %d ---->  %d\n", course_list[c_id].labs[i].lab_id, lab_list[course_list[c_id].labs[i].lab_id].total_mentors);
                // printf("TOTAL MENTORS OF LAB %d ---->  %d\n", course_list[c_id].labs[i].lab_id, course_list[c_id].labs[i].total_mentors);

                for (int j = 0; j < course_list[c_id].labs[i].total_mentors; j++)

                {
                    // printf("ok------------------\n");
                    // printf("course_list[c_id].labs[i].ta_free[j]------>%d\n", course_list[c_id].labs[i].ta_free[j]);
                    // printf("course_list[c_id].labs[i].ta_terms[j]--->%d\n", course_list[c_id].labs[i].ta_terms[j]);
                    // printf("course_list[c_id].labs[i].tutoring_limit--->%d\n", course_list[c_id].labs[i].tutoring_limit);

                    // if (course_list[c_id].labs[i].ta_free[j] == 1 && (course_list[c_id].labs[i].ta_terms[j] < course_list[c_id].labs[i].tutoring_limit))
                    if (lab_list[course_list[c_id].labs[i].lab_id].ta_free[j] == 1 && (course_list[c_id].labs[i].ta_terms[j] < course_list[c_id].labs[i].tutoring_limit))

                    {

                        //assign this ta to course

                        // printf(ANSI_RED "bruh\n" ANSI_RESET);
                        //change course to unavailable
                        //wait for the stuednts for the ta to be alloted is over - students can now resume confirming and withrawing course
                        sem_post(&(course_list[c_id].ta_alloted));
                        sem_getvalue(&(course_list[c_id].ta_alloted), &garbage);
                        // printf("course_list[c_id].ta_alloted--->%d\n", garbage);
                        printf(ANSI_YELLOW "TA %d from lab %s has been allocated to course %s,%d for %d th TA ship\n" ANSI_RESET, j, lab_list[course_list[c_id].labs[i].lab_id].name, course_list[c_id].name, c_id, course_list[c_id].labs[i].ta_terms[j]);

                        course_list[c_id].available = 0; //-----------------------????semaphore or is it to be locked under mutex??????????????????????????????????????????????????
                        course_list[c_id].ta_present = 1;
                        // printf("\ntA PRESENT__>%d\n", course_list[c_id].ta_present);
                        course_list[c_id].current_lab_id = course_list[c_id].labs[i].lab_id;
                        // course_list[c_id].current_lab_name = course_list[c_id].labs[i].name;

                        strcpy(course_list[c_id].current_lab_name, course_list[c_id].labs[i].name);

                        pthread_mutex_lock(&(course_list[c_id].mutex));

                        course_list[c_id].labs[i].ta_terms[j]++;
                        course_list[c_id].labs[i].ta_free[j] = 0; //or should i make this a semaphore since other courses which had earlier got this ta due to race

                        pthread_mutex_unlock(&(course_list[c_id].mutex));
                        break;
                    }
                }
                // printf("\n\n*********************************TOTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
                // check_tut_cond_n_conduct(c_id);
            }
            // printf("\n\n*********************************TOTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
            // check_tut_cond_n_conduct(c_id);
        }
        printf("\n\n*********************************TUTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
        check_tut_cond_n_conduct(c_id);
    }

    if (course_exhausted_chances(c_id) == 1)
    {
        //remove_course(c_id);
        //here  u need to print the current status of the students
        exit(0);
    }
}
