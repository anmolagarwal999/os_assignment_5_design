#include "headers.h"

int num_courses = 0;
int num_students = 0; ////////

int num_labs = 0;
int total_tuts = 0;
int tuts_left = 0;
int garbage = -1;

void print_input()
{

    printf("%d %d %d\n", num_students, num_labs, num_courses);

    for (int i = 1; i <= num_courses; i++)
    {
        printf("%s %.2f %d %d ", course_list[i].name, course_list[i].interest_quotient, course_list[i].max_slot, course_list[i].no_labs);
        for (int j = 0; j < course_list[i].no_labs; j++)
        {
            printf("%d ", course_list[i].labs[j].lab_id);
        }
        printf("\n");
    }

    for (int i = 1; i <= num_students; i++)
    {
        printf("%.2f %d %d %d %d", student_list[i].calibre_quotient, student_list[i].prefs[1], student_list[i].prefs[2], student_list[i].prefs[3], student_list[i].submission_time);
        // printf("%.2f  %d %d %d %d", student_list[i].calibre_quotient,student_list[i].pref1,student_list[i].pref2,student_list[i].pref3,student_list[i].submission_time);
        printf("\n");
    }

    for (int i = 1; i <= num_labs; i++)
    {
        printf("%s ", lab_list[i].name);
        printf("%d ", lab_list[i].total_mentors);
        printf("%d\n", lab_list[i].tutoring_limit);
    }
}

void input()
{
    //line 1
    //<num_students> <num_labs> <num_courses>
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    // printf("num_labs=%d\n", num_labs);

    for (int i = 1; i <= num_courses; i++)
    {

        course_list[i].course_id = i;
        scanf("\n%[a-zA-Z]", course_list[i].name);

        scanf("%f %d %d", &course_list[i].interest_quotient, &course_list[i].max_slot, &course_list[i].no_labs);
        for (int j = 0; j < course_list[i].no_labs; j++)
        {
            scanf("%d", &course_list[i].labs[j].lab_id);
        }
    }
    // printf("\n");

    // //input 3
    // //<calibre quotient of first student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
    for (int i = 1; i <= num_students; i++)
    {
        student_list[i].id = i;
        scanf("%f %d %d %d %d", &student_list[i].calibre_quotient, &student_list[i].prefs[1], &student_list[i].prefs[2], &student_list[i].prefs[3], &student_list[i].submission_time);
        // printf("%.2f  %d %d %d %d", student_list[i].calibre_quotient,student_list[i].pref1,student_list[i].pref2,student_list[i].pref3,student_list[i].submission_time);
    }

    for (int i = 1; i <= num_labs; i++)
    {
        scanf("\n%[a-zA-Z]", lab_list[i].name);
        scanf("%d", &lab_list[i].total_mentors);
        scanf("%d", &lab_list[i].tutoring_limit);
    }
    // // printf("\n");

    // print_input();

    if (num_students == 0)
    {
        printf(ANSI_RED "No student available.\n" ANSI_RESET);
        printf("Simulation Over.\n");

        exit(0);
    }

    if (num_courses == 0)
    {
        printf(ANSI_RED "No course is available to register.\n" ANSI_RESET);
        printf("Simulation Over.\n");

        exit(0);
    }

    if (num_labs == 0)
    {
        printf(ANSI_RED "No lab available to provide mentors.\n" ANSI_RESET);
        printf("Simulation Over\n");

        exit(0);
    }
}

int main()
{

    srand(time(NULL));

    input();

    initialize_labs_x();

    total_tut_possible();
    // printf("total tuts---%d\n", total_tuts);
    tuts_left = total_tuts;

    //initilazie  all the labs ,students,courses

    for (int i = 1; i <= num_courses; i++)
    {

        pthread_mutex_init(&(course_list[i].mutex), NULL);

        // course_list[i].th_course = pthread_create(&(course_list[i].th_course), NULL, initialize_course, (void *)(&i));
        if (pthread_create(&(course_list[i].th_course), NULL, initialize_course, (void *)(&i)) != 0)
        {
            perror("ok1");
        }
        // printf("created course thread %d\n", i);
    }
    // sleep(1);

    for (int i = 1; i <= num_students; i++)
    {

        pthread_mutex_init(&(student_list[i].mutex), NULL);

        // student_list[i].filled = -1;

        // student_list[i].th_student = pthread_create(&(student_list[i].th_student), NULL, initialize_student, (void *)(&i));
        if (pthread_create(&(student_list[i].th_student), NULL, initialize_student, (void *)(&i)) != 0)
        {
            perror("ok2");
        }

        // printf("created student thread %d\n", i);
        //perror
    }

    for (int i = 1; i <= num_labs; i++)
    {

        pthread_mutex_init(&lab_list[i].mutex, NULL);
        initialize_labs_x();
    }

    for (int i = 1; i <= num_courses; i++)
    {

        // pthread_join(pthread_t thread, void **value_ptr);
        // printf("bruh-courses\n");
        if (pthread_join(course_list[i].th_course, NULL) != 0)
        {
            // perror("");
        }
        // pthread_join(course_list[i].th_course, NULL);
    }
    for (int i = 1; i <= num_students; i++)
    {
        // printf("bruh-students\n");

        // pthread_join(student_list[i].th_student, NULL);
        if (pthread_join(student_list[i].th_student, NULL) != 0)
        {
            // perror("");
        }
    }

    printf(ANSI_GREEN "Simulation Over\n" ANSI_RESET);
    // fflush(stdout);

    return 0;
}
