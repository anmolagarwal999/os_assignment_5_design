#ifndef LABS_X
#define LABS_X

void initialize_labs_x(void);
void initialize_tas(int lab_id);
int total_tut_possible(void);

#endif