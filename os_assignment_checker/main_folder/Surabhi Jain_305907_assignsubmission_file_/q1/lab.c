#include "headers.h"

//initialize all the labs
void initialize_labs_x(void)
{

    for (int i = 1; i <= num_labs; i++)
    {
        lab_list[i].active = 1;
    }

    //initialize all tas in a lab
    for (int i = 1; i <= num_labs; i++)
    {
        // int id = lab_list[i].lab_id;
        // printf(ANSI_BLUE "lab %d about to initilaize tas--\n" ANSI_RESET, i);
        // initialize_tas(id);
        // initialize_tas(id);
        initialize_tas(i);

        // printf(ANSI_YELLOW "initialized tas for lab %d\n" ANSI_RESET, i);
    }
}
//WHEN WILL THE LAB SEEK TA???????

//initialize tas in a lab
void initialize_tas(int lab_id)
{

    // int total_tas = lab_list[lab_id].total_mentors;
    // printf("total tas in lab %d is %d\n", lab_id, total_tas);
    // printf("total tas in lab %d is %d\n", lab_id, lab_list[lab_id].total_mentors);

    for (int i = 0; i < lab_list[lab_id].total_mentors; i++)
    {
        //  total_tas=lab_list[lab_id].tas[i].available=1;
        // printf(ANSI_RED "initilaising ta_free and ta_terms for lab %d\n" ANSI_RESET, lab_id);
        lab_list[lab_id].ta_terms[i] = 0;
        lab_list[lab_id].ta_free[i] = 1;
    }
}

// int is_active(int lab_id){
//     for(int i=0;i<=lab_list[lab_id].total_mentors;i++){
//         //check tutoring limit
//         if(lab_list[lab_id].ta_terms[i]<lab_list[lab_id].tutoring_limit){
//             //lab is active
//         }
//     }
//     //lab is not active
//     return 0;

// }

int total_tut_possible(void)
{

    //loop over all the labs
    for (int i = 1; i <= num_labs; i++)
    {
        total_tuts += (lab_list[i].total_mentors * (lab_list[i].tutoring_limit));
    }

    return total_tuts;
}