#include "global.h"
// #include "func.h"
#include "headers.h"

void *initialize_student(void *ptr)
{

    int id = *((int *)ptr);
    // printf("inside student thread %d\n", id);

    for (int i = 0; i < 10; i++)
    {
        student_list[id].withrawn[i] = 0;
    }

    student_list[id].filled = 0;
    student_list[id].current_pref = student_list[id].prefs[1];
    student_list[id].current_pref_no = 1;

    student_list[id].confirm = -1;
    student_list[id].past_pref = -1;
    // student_list[id].removed = -1;
    //--------------------------------------------------------------------------------------------------------------------------------------
    student_list[id].exit = 0;
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //only seek course when the priorities have been filled
    sleep(student_list[id].submission_time);
    seek_course(id);
    return NULL;
}

//return 1 if student wants to exit

int change_pref_or_exit(int student_id)
{

    if (student_list[student_id].current_pref_no < 3)
    {

        student_list[student_id].current_pref_no++;
        student_list[student_id].past_pref = student_list[student_id].current_pref; // dont name last_pref
        student_list[student_id].current_pref = student_list[student_id].prefs[student_list[student_id].current_pref_no];

        printf(ANSI_RED "Student %d has changed current preference from %d (priority %d) to %d (priority %d)\n" ANSI_RESET, student_id, student_list[student_id].current_pref, student_list[student_id].current_pref_no - 1, student_list[student_id].past_pref, student_list[student_id].current_pref_no);
    }
    else
    {
        //exit the simulation
        student_list[student_id].exit = 1;
        printf(ANSI_CYAN "Student either didn’t get any of their preferred courses or has withdrawn from them and has exited the simulation\n" ANSI_RESET);

        return student_list[student_id].exit;
    }
    return student_list[student_id].exit;
}

int confirm_course(int student_id, int course_id)
{

    int confirm_x = 0;

    double x = course_list[course_id].interest_quotient;
    double y = student_list[student_id].calibre_quotient;

    student_list[student_id].like_prob = x * y;

    double rand_x = gen_rand();
    if (rand_x >= student_list[student_id].like_prob)
    {
        confirm_x = 1;
    }
    else
    {
        confirm_x = 0;
    }
    return confirm_x;
}

void seek_course(int student_id)

{
    // printf(ANSI_BLUE "INSIDE SEEK COURSE \n" ANSI_RESET);

    while (student_list[student_id].confirm != 1 && student_list[student_id].withrawn[3] != 1)
    {
        // printf(ANSI_YELLOW "INSIDE SEEK COURSE WHILE LOOP\n" ANSI_RESET);

        printf(ANSI_RED "Student %d filled in preferences for course registration\n" ANSI_RESET, student_id);

        //CHECK IF COURSE IS REMOVED FROM THE PORTAL OR NOT
        if (course_list[student_list[student_id].current_pref].removed == 1)
        {
            // printf("student id %d-->CHECHING IF CURRENT PREF IS REMOVED\n", student_id);

            if (change_pref_or_exit(student_id) == 1)
            {
                printf("STUDENT %d EXITING.............\n", student_id);
                return;
            }
        }

        //WAIT FOR THE COURSE TO BECOME AVAILABLE

        // pthread_mutex_lock(&(student_list[student_id].mutex));

        // printf("student id %d-->current pref is %d\n", student_id, student_list[student_id].current_pref);

        if (course_list[student_list[student_id].current_pref].available != 1)
        {
            // printf("student id %d-->CHECHING IF CURRENT PREF IS AVAILABLE\n", student_id);

            sem_wait(&(course_list[student_list[student_id].current_pref].available_s));
            printf("Current pref is now available\n");
            course_list[student_list[student_id].current_pref].available = 1;
        }
        // pthread_mutex_unlock(&(student_list[student_id].mutex));

        // pthread_mutex_lock(&(student_list[student_id].mutex));

        if (course_list[student_list[student_id].current_pref].ta_present != 1)
        {

            //WAITING FOR TA TO BE ALLOTED
            // printf("student id %d-->here\n", student_id);
            sem_wait(&(course_list[student_list[student_id].current_pref].ta_alloted));
            // printf("ta allocated\n");
        }
        // pthread_mutex_lock(&(student_list[student_id].mutex));
        fflush(stdout);
        // printf("student id %d-->WAITING FOR SLOTS TO BE ASSIGNED in course %d\n", student_id, student_list[student_id].current_pref);
        pthread_mutex_lock(&(student_list[student_id].mutex));
        int y = 0;
        sem_getvalue(&(course_list[student_list[student_id].current_pref].rslots_assigned), &y);
        // printf(ANSI_YELLOW "\nbefore calling sem_wait current value of semaphore rslots_assigned for course %d is---%d\n\n" ANSI_RESET, student_list[student_id].current_pref, y);
        // pthread_mutex_unlock(&(student_list[student_id].mutex));
        sem_wait(&(course_list[student_list[student_id].current_pref].rslots_assigned));
        // pthread_mutex_lock(&(student_list[student_id].mutex));

        sem_getvalue(&(course_list[student_list[student_id].current_pref].rslots_assigned), &y);
        // printf(ANSI_YELLOW "\nafter calling sem_wait current value of semaphore rslots_assigned for course %d is---%d\n\n" ANSI_RESET, student_list[student_id].current_pref, y);
        pthread_mutex_unlock(&(student_list[student_id].mutex));
        // printf("rslots assigned for course %d------------------------------------------------------------------------------------------------------------------------\n", student_list[student_id].current_pref);
        // printf("course_list[student_list[student_id].current_pref].ta_present ---->%d\n\n", course_list[student_list[student_id].current_pref].ta_present);
        // printf("waiting for ta to be alloted #######################################################\n");

        // if (course_list[student_list[student_id].current_pref].ta_present != 1)
        // {

        //     //WAITING FOR TA TO BE ALLOTED
        //     printf("student id %d-->here\n", student_id);
        //     sem_wait(&(course_list[student_list[student_id].current_pref].ta_alloted));
        //     printf("ta allocated\n");
        // }
        // sem_wait(&(course_list[student_list[student_id].current_pref].ta_present));
        if (course_list[student_list[student_id].current_pref].ta_present == 1) /*a ta is present*/
        {
            //WAIT WHILE RAND SLOTS ARE NOT ASSIGNED
            // while (course_list[student_list[student_id].current_pref].rand_slots == 0)
            // {
            // }

            if (course_list[student_list[student_id].current_pref].filled_slots < course_list[student_list[student_id].current_pref].rand_slots)
            {

                printf(ANSI_GREEN "Student %d has been allocated a seat in course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                pthread_mutex_lock(&(student_list[student_id].mutex));
                int confirm = confirm_course(student_id, course_list[student_list[student_id].current_pref].course_id);

                if (confirm)
                {

                    printf(ANSI_MAGENTA "Student %d wants to select course %d permanently\n" ANSI_RESET, student_id, student_list[student_id].current_pref);

                    // pthread_mutex_lock(&(course_list[student_list[student_id].current_pref].mutex));
                    // pthread_mutex_lock(&(student_list[student_id].mutex));

                    course_list[student_list[student_id].current_pref].filled_slots++;
                    sem_post(&(course_list[student_list[student_id].current_pref].f_slots));
                    // printf("after course_list[student_list[student_id].current_pref].filled_slots++;-->filled slots==  %d\n", course_list[student_list[student_id].current_pref].filled_slots);

                    printf(ANSI_YELLOW "Student %d has selected course %d permanently\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                    pthread_mutex_unlock(&(student_list[student_id].mutex));

                    // pthread_mutex_unlock(&(course_list[student_list[student_id].current_pref].mutex));

                    //exit the simulation
                    printf(ANSI_GREEN "Student %d exiting simulation\n" ANSI_RESET, student_id);

                    pthread_exit(NULL);
                    // return;
                }

                else
                {
                    printf(ANSI_BLUE "Student %d has withdrawn from course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                    student_list[student_id].withrawn[student_list[student_id].current_pref_no] = 1;

                    if (change_pref_or_exit(student_id) == 1)
                    {
                        return;
                    }
                }

                //if seats are allocated the student will exit the simulation
            }
            else
            {

                printf(ANSI_BLUE "Student %d has withdrawn from course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                student_list[student_id].withrawn[student_list[student_id].current_pref_no] = 1;
                if (change_pref_or_exit(student_id) == 1)
                {
                    return;
                }
            }
        }
    }

    printf(ANSI_GREEN "STUDENT %d EXITED THE SIMULATION\n" ANSI_RESET, student_id);
    pthread_exit(NULL);
    // return;
}
