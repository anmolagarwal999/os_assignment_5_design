#ifndef STUDENTS_X
#define STUDENTS_X

void *initialize_student(void *ptr);
int change_pref_or_exit(int student_id);
int confirm_course(int student_id, int course_id);
void seek_course(int student_id);

#endif