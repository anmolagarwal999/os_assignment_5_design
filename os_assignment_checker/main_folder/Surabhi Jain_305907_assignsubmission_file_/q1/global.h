#ifndef GLOBALS
#define GLOBALS

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

#define ANSI_RED "\x1b[31m"
#define ANSI_GREEN "\x1b[32m"
#define ANSI_YELLOW "\x1b[33m"
#define ANSI_BLUE "\x1b[34m"
#define ANSI_MAGENTA "\x1b[35m"
#define ANSI_CYAN "\x1b[36m"
#define ANSI_RESET "\x1b[0m"

#define max_courses 10
#define max_labs 10
#define max_students 25

extern int num_courses;
extern int num_students; ////////

extern int num_labs;

extern int tutoring_delay_time;

extern int total_tuts; //total tuts possible
extern int tuts_left;
extern int garbage;

struct lab
{
    // pthread_t th_lab;
    int lab_id;
    char name[10];
    int total_mentors;
    int tutoring_limit; //total no of times a ta can tutor considering all courses
    pthread_mutex_t mutex;
    // int ta_cur_term[10];
    int ta_terms[10];
    int ta_free[10]; //whether a ta is free or taking a tutorail

    //  struct ta tas[total_mentors+1];
    // int tas[total_mentors+1];
    // int tas[10];
    // struct ta tas[10];

    int active; //All people in the lab have exhausted the limit on the number of times someone can accept a TAship and
    //so, the lab will no longer send out any TAs
};

struct course
{
    pthread_t th_course;

    int course_id;
    char name[50];
    float interest_quotient;
    int no_labs; //no of labs from which the course accepts ta
    struct lab labs[10];
    int ta_present; //id of the ta which is currently tutoring(only one ta at a time can tutor)
    int max_slot;   //maximum no of students who can attend the course

    //initialize filled slots to 0 at the beginning of the program

    int filled_slots;

    pthread_mutex_t mutex;
    sem_t slot_fill_wait;
    sem_t f_slots; //current slots assigned according to the number of students and d
    int available;
    sem_t available_s;
    int current_ta;       //number will vary according to the lab chosen
    sem_t conducting_tut; ////1 or 0
    int rand_slots;
    int d;
    sem_t ta_alloted;          //1 or 0
    sem_t rslots_assigned;     //whether d is calcualted or not (0 or 1)
    int current_lab_id;        //of the current ta
    char current_lab_name[20]; //of the current ta
    int removed;
};

struct student
{

    pthread_t th_student;
    int id;
    int confirm;

    struct course courses[10]; //courses preference of the students in decreasing order of priority
    int withrawn[10];          //pref withrawn or not         ///if the student has withrawn from the course

    //withrawn is indexed according to pref no not course no
    //time after which he fills course preferences
    int submission_time;

    int filled; //becomes_available; //only becomes available after filling in course priorities

    float calibre_quotient;
    // int pref1;
    // int pref2;
    // int pref3;

    int prefs[4];

    int current_pref; //wait for this//this will store id not name

    int current_pref_no;
    int past_pref;
    double like_prob; //probability of liking the course=interest_of_the_course*student_calibre
    pthread_mutex_t mutex;
    int exit; //yes or no (1 for yes, 0 for no)
};

struct ta
{
    int id;
    int available;
    int terms;
    int free;
};

struct course course_list[max_courses];
struct lab lab_list[max_labs];
struct student student_list[max_students];
struct ta ta_list[20];

#endif