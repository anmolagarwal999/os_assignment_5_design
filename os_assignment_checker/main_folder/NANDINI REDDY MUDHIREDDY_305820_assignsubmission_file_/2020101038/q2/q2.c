#include <stdio.h>
#include "pthread.h"
#include "semaphore.h"
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

int time_in;
int H_Capacity,A_Capacity,N_Capacity;
int X;
int Ngroups;
int GscoreChances;
int H_seats,A_seats,N_seats;
int H_wait,A_wait,N_wait;
int Y=0;
int h=0,a=0,n=0;
int ft;


pthread_t allocating_zone[1000];
pthread_t exit_zone[1000];
pthread_t goal_chances[1000];

pthread_mutex_t Hlock;
pthread_mutex_t Alock;
pthread_mutex_t Nlock;
pthread_mutex_t condition1;
pthread_mutex_t condition2;
pthread_mutex_t condition3;
pthread_mutex_t lock1;
pthread_mutex_t lock2;
pthread_mutex_t lock3;

// functions for threads 
typedef struct people People;

struct groups {
    int Npeople;
    struct people {
     char Pname[25];
     char Ptype;
     int timeReached;
     int P;
     int Ngoals;
     int Spectating_time;
     int zone_alloc_status;                  // zone allocation
     int exit_status;          // to find exited or not
     int allocated_zone;       //  H->1 , A->2 , N->3
    } info[1000];    
};

typedef struct GoalScoring Gscoring;
struct GoalScoring {
      char team;
      int tElapsed;
      float prob;
};


void* exit_person(void* args);
void* person_reached_stadium(void* args);
void* Zone_allocate(void* args);
void Teams_Gchances(void* args);


#define ANSI_RED "\033[1;31m"
#define ANSI_MAGENTA "\033[1;35m"
#define ANSI_DEFAULT "\033[0m"
#define ANSI_BRIGHT_BLUE "\e[1;94m"
#define ANSI_GREEN_BOLD "\033[1;32m"
#define ANSI_COLOR_CYAN    "\x1b[36m"

// ************************************Person p_i has reached the stadium**************Person p_i has got a seat in zone z_j********************************************************
void* person_reached_stadium(void* args)
{
      int tReached = ((struct people *)args)->timeReached;
      sleep(tReached);
      char Name[26];
      strcpy(Name,((struct people *)args)->Pname);
      printf(ANSI_RED"t=%d : %s has reached the stadium\n"ANSI_DEFAULT,tReached,Name);

      if(((struct people *)args)->Ptype == 'A')
      {
          pthread_mutex_lock(&Alock);
           if(A_seats == 0)
           {
                int val = pthread_cond_wait(&condition1,&Alock);
                if(val == 0 && (((struct people *)args)->zone_alloc_status == 0) && (((struct people *)args)->exit_status == 0))
                {
                    ((struct people *)args)->allocated_zone = 2;
                   A_seats--;
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone A\n"ANSI_DEFAULT,ft,Name);     // x to be find
                }
           }
           else if(A_seats > 0)
           {
               ((struct people *)args)->allocated_zone = 2;
               A_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone A\n"ANSI_DEFAULT,tReached,Name);
           }
           pthread_mutex_unlock(&Alock);
      }
      else if(((struct people *)args)->Ptype == 'H')
      {
            pthread_mutex_lock(&Hlock);
            pthread_mutex_lock(&Nlock);
        
            if(H_seats == 0 && N_seats == 0)
            {
                // if peeps waiting for H Zone < N Zone
                pthread_mutex_lock(&lock1);
                pthread_mutex_lock(&lock3);  // lock3 for N
              if (H_wait < N_wait)
              {
                    H_wait++;
                    pthread_mutex_unlock(&lock1);
                    pthread_mutex_unlock(&lock3);
                    pthread_mutex_unlock(&Nlock);
                    int val1 = pthread_cond_wait(&condition2,&Hlock);
                 if((val1 == 0) && (((struct people *)args)->zone_alloc_status == 0 && (((struct people *)args)->exit_status == 0)))
                  {
                    ((struct people *)args)->allocated_zone = 1;
                   H_seats--;
                   pthread_mutex_unlock(&Hlock);
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone H\n"ANSI_DEFAULT,ft,Name);     // x to be find
                   pthread_mutex_lock(&lock1);
                   H_wait--;
                   pthread_mutex_unlock(&lock1);
                  }
              }
              else
              {
                   N_wait++;
                    pthread_mutex_unlock(&lock1);
                    pthread_mutex_unlock(&lock3);
                    pthread_mutex_unlock(&Hlock);
                   int val2 = pthread_cond_wait(&condition3,&Nlock);
                if((val2 == 0) && (((struct people *)args)->zone_alloc_status == 0) && (((struct people *)args)->exit_status == 0))
                {
                   ((struct people *)args)->allocated_zone = 3;
                   N_seats--;
                   pthread_mutex_unlock(&Nlock);
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone N\n"ANSI_DEFAULT,ft,Name);     // x to be find
                  pthread_mutex_lock(&lock3);
                  N_wait--;
                  pthread_mutex_unlock(&lock3);
                }
              }
                // if peeps waiting for H Zone >= N Zone
            }
            else if(H_seats > 0)
            {
                ((struct people *)args)->allocated_zone = 1;
                H_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone H\n"ANSI_DEFAULT,tReached,Name);
            }
            else if(N_seats > 0)
            {
                ((struct people *)args)->allocated_zone = 3;
               N_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone N\n"ANSI_DEFAULT,tReached,Name);
            }
             pthread_mutex_unlock(&Nlock);
            pthread_mutex_unlock(&Hlock);
      }
      else if(((struct people *)args)->Ptype == 'N')
      {
            pthread_mutex_lock(&Hlock);
            pthread_mutex_lock(&Alock);
            pthread_mutex_lock(&Nlock);
            
           
           if(H_seats == 0 && A_seats == 0 && N_seats == 0)
            {
               pthread_mutex_lock(&lock1); 
               pthread_mutex_lock(&lock2); 
               pthread_mutex_lock(&lock3); 
               if(H_wait <= A_wait && H_wait < N_wait)
               {
                   pthread_mutex_unlock(&lock2); 
                   pthread_mutex_unlock(&lock3); 
                 H_wait++;
                 pthread_mutex_unlock(&lock1); 
                 pthread_mutex_unlock(&Alock); 
                 pthread_mutex_unlock(&Nlock); 
                 //if peeps waiting for H Zone <= N Zone , H Zone < A Zone
                 int ret1 = pthread_cond_wait(&condition2,&Hlock);
                if((ret1 == 0) && (((struct people *)args)->zone_alloc_status == 0) && (((struct people *)args)->exit_status == 0))
                {
                    ((struct people *)args)->allocated_zone = 1;
                   H_seats--;
                   pthread_mutex_unlock(&Hlock);
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone H\n"ANSI_DEFAULT,ft,Name);     // x to be find
                   pthread_mutex_lock(&lock1);
                   H_wait--;
                   pthread_mutex_unlock(&lock1);
                }
               }
               else if(A_wait < H_wait && A_wait < N_wait)
               {
                 pthread_mutex_unlock(&lock1); 
                   pthread_mutex_unlock(&lock3); 
                 A_wait++;
                 pthread_mutex_unlock(&lock2); 
                 pthread_mutex_unlock(&Hlock); 
                 pthread_mutex_unlock(&Nlock); 
                 int ret2 = pthread_cond_wait(&condition1,&Alock);
                if((ret2 == 0) && (((struct people *)args)->zone_alloc_status == 0) && (((struct people *)args)->exit_status == 0))
                {
                    ((struct people *)args)->allocated_zone = 2;
                   A_seats--;
                   pthread_mutex_unlock(&Alock); 
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone A\n"ANSI_DEFAULT,ft,Name);     // x to be find
                   pthread_mutex_lock(&lock2);
                   A_wait--;
                   pthread_mutex_unlock(&lock2);
                }

               }
               else if(N_wait <= A_wait && N_wait <= H_wait)
               {
                   pthread_mutex_unlock(&lock1); 
                   pthread_mutex_unlock(&lock2); 
                   N_wait++;
                   pthread_mutex_unlock(&lock3); 
                   pthread_mutex_unlock(&Hlock); 
                   pthread_mutex_unlock(&Alock); 
                   int ret3 = pthread_cond_wait(&condition3,&Nlock);
                if((ret3 == 0) && (((struct people *)args)->zone_alloc_status == 0) && (((struct people *)args)->exit_status == 0))
                {
                   ((struct people *)args)->allocated_zone = 3;
                   N_seats--;
                   pthread_mutex_unlock(&Nlock); 
                   ((struct people *)args)->zone_alloc_status = 1;
                   printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone N\n"ANSI_DEFAULT,ft,Name);     // x to be find
                   pthread_mutex_lock(&lock3);
                   N_wait--;
                   pthread_mutex_unlock(&lock3);
                }
               }
            }
           else if(H_seats > 0)
            {
                ((struct people *)args)->allocated_zone = 1;
                H_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone H\n"ANSI_DEFAULT,tReached,Name);
            }
            else if(A_seats > 0)
           {
               ((struct people *)args)->allocated_zone = 2;
               A_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone A\n"ANSI_DEFAULT,tReached,Name);
           }
           else if(N_seats > 0)
            {
                ((struct people *)args)->allocated_zone = 3;
               N_seats--;
               ((struct people *)args)->zone_alloc_status = 1;
               printf(ANSI_MAGENTA"t=%d : %s has got a seat in zone N\n"ANSI_DEFAULT,tReached,Name);
            }


            pthread_mutex_unlock(&Hlock);
            pthread_mutex_unlock(&Alock);
            pthread_mutex_unlock(&Nlock);
      }

      
}
//     *****************************************************Ends*******************************************************************


//      *************************************************Starts************************************************************************
void* exit_person(void* args)
{
    // condition 1  ->  Person p_i couldn’t get a seat
    // condition 2 ->  Person p_i is leaving due to the bad defensive performance of his team
    // condition 3  ->   Person p_i watched the match for X seconds and is leaving
        int tReached = ((struct people *)args)->timeReached;
        int tPatience = ((struct people *)args)->P;
        sleep(tReached+tPatience); 
        if(((struct people *)args)->zone_alloc_status == 0)
        {
            printf(ANSI_BRIGHT_BLUE"t=%d : %s couldn’t get a seat\n"ANSI_DEFAULT,tReached+tPatience,((struct people *)args)->Pname);
            ((struct people *)args)->exit_status = 1;
            ft = tReached+tPatience;
        }
        else
        {
            // for bad defensive shoud be implemented

            sleep(X-tPatience);
            if(((struct people *)args)->zone_alloc_status == 1)
            {
                 if(((struct people *)args)->Ptype == 'A')
                 {
                    ((struct people *)args)->exit_status = 1;
                     pthread_mutex_lock(&Alock);
                     A_seats++;
                     pthread_cond_signal(&condition1);
                     pthread_mutex_unlock(&Alock);
                     printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X);
                     ft = tReached+X;
                 }
                 else if (((struct people *)args)->Ptype == 'H')
                 {
                     if(((struct people *)args)->allocated_zone == 1)
                     {
                         ((struct people *)args)->exit_status = 1;
                        pthread_mutex_unlock(&Hlock);
                         H_seats++;
                         pthread_cond_signal(&condition2);
                         pthread_mutex_unlock(&Hlock);
                         printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X);
                         ft = tReached+X;
                     }
                     else if(((struct people *)args)->allocated_zone == 3)
                     {
                         ((struct people *)args)->exit_status = 1;
                         pthread_mutex_lock(&Nlock);
                         N_seats++;
                         pthread_cond_signal(&condition3);
                         pthread_mutex_unlock(&Nlock);
                         printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X);
                         ft = tReached+X;
                     }
                 }
                 else if(((struct people *)args)->Ptype == 'N')
                 {
                     if(((struct people *)args)->allocated_zone == 1)
                     {
                         ((struct people *)args)->exit_status = 1;
                         pthread_mutex_lock(&Hlock);
                         H_seats++;
                         pthread_cond_signal(&condition2);
                         pthread_mutex_unlock(&Hlock);
                         printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X);
                         ft = tReached+X;
                     }
                     else if(((struct people *)args)->allocated_zone == 2)
                     {
                       ((struct people *)args)->exit_status = 1;
                       pthread_mutex_lock(&Alock);
                       A_seats++;
                       pthread_cond_signal(&condition1);
                       pthread_mutex_unlock(&Alock);
                       printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X); 
                       ft = tReached+X;
                     }
                     else if(((struct people *)args)->allocated_zone == 3)
                     {
                         ((struct people *)args)->exit_status = 1;
                         pthread_mutex_lock(&Nlock);
                         N_seats++;
                         pthread_cond_signal(&condition3);
                         pthread_mutex_unlock(&Nlock);
                         printf(ANSI_GREEN_BOLD"t=%d : %s watched the match for %d seconds and is leaving\n"ANSI_DEFAULT,tReached+X,((struct people *)args)->Pname,X);
                         ft = tReached+X;
                     }
                 }
                 
            }

        }







}
// *********************************************************Ends******************************************************************

void Teams_Gchances(void* args)
{
   int Time = ((Gscoring *)args)->tElapsed;
   float probability = ((Gscoring*)args)->prob;
   char team = ((Gscoring*)args)->team;
   sleep(Time);
  if(team == 'H')
  {
     if(probability > 0.5)
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c have scored their %d goal\n"ANSI_DEFAULT,Time,team,++h);
     }
     else
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c missed the chance to score their %d goal\n"ANSI_DEFAULT,Time,team,++h);
     }
  }
  else if(team == 'A')
  {
     if(probability > 0.5)
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c have scored their %d goal\n"ANSI_DEFAULT,Time,team,++a);
     }
     else
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c missed the chance to score their %d goal\n"ANSI_DEFAULT,Time,team,++a);
     }
  }
  else if(team == 'N')
  {
     if(probability > 0.5)
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c have scored their %d goal\n"ANSI_DEFAULT,Time,team,++n);
     }
     else
     {
      printf(ANSI_COLOR_CYAN"t=%d : Team %c missed the chance to score their %d goal\n"ANSI_DEFAULT,Time,team,++n);
     }
  }
}


int main()
{
    pthread_mutex_init(&Alock,NULL);
    pthread_mutex_init(&Hlock,NULL);
    pthread_mutex_init(&Nlock,NULL);
    pthread_mutex_init(&condition1,NULL);
    pthread_mutex_init(&condition2,NULL);
    pthread_mutex_init(&condition3,NULL);
    pthread_mutex_init(&lock1,NULL);
    pthread_mutex_init(&lock2,NULL);
    pthread_mutex_init(&lock3,NULL);
    time_in = clock();
    scanf("%d%d%d",&H_Capacity,&A_Capacity,&N_Capacity);
    scanf("%d",&X);
    scanf("%d",&Ngroups);

    struct groups Group[Ngroups];
    for (int i = 0; i < Ngroups;i++)
    {
        scanf("%d",&Group[i].Npeople);
          for (int j = 0; j < Group[i].Npeople; j++)
          {
              scanf("%s",Group[i].info[j].Pname);
              //printf("Pname : %s\n",Group[i].info[j].Pname);
              scanf(" %c",&Group[i].info[j].Ptype);
              //printf("Ptype : %c\n",Group[i].info[j].Ptype);
              scanf("%d",&Group[i].info[j].timeReached);
              scanf("%d",&Group[i].info[j].P);
              scanf("%d",&Group[i].info[j].Ngoals);
              Group[i].info[j].zone_alloc_status = 0;
              Group[i].info[j].allocated_zone = -1;         // Not allocated
              Group[i].info[j].exit_status = 0;
          }
         // printf("\n==loop ended==\n");
    }
     scanf("%d",&GscoreChances);

    Gscoring  temp[GscoreChances];
    
    for(int i=0;i < GscoreChances;i++)
    {
       scanf(" %c",&temp[i].team);
       scanf("%d",&temp[i].tElapsed);
       scanf("%f",&temp[i].prob);   
    }

    H_seats = H_Capacity;
    A_seats = A_Capacity;
    N_seats = N_Capacity;
    H_wait = 0;
    A_wait = 0;
    N_wait = 0;
    int k = 0;
    for (int i = 0; i < Ngroups;i++)
    {
          for (int j = 0; j < Group[i].Npeople; j++)
          {

               pthread_create(&allocating_zone[k++],NULL,person_reached_stadium,(void*)&Group[i].info[j]);
          }
    }
    int r = 0;
    for (int i = 0; i < Ngroups;i++)
    {
          for (int j = 0; j < Group[i].Npeople; j++)
          {

               pthread_create(&exit_zone[r++],NULL,exit_person,(void*)&Group[i].info[j]);
          }
    }

    for(int t=0; t < GscoreChances; t++)
    {
        pthread_create(&goal_chances[t],NULL,Teams_Gchances,(void*)&temp[t]);
    }
    for(int t=0; t < GscoreChances; t++)
    {
        pthread_join(goal_chances[t],NULL);
    }
    for (int i = 0; i < k; i++)
    {
       pthread_join(allocating_zone[i],NULL);
    }



    for (int i = 0; i < r; i++)
    {
       pthread_join(exit_zone[i],NULL);
    }

     pthread_mutex_destroy(&Alock);
     pthread_mutex_destroy(&Hlock);
     pthread_mutex_destroy(&Nlock);
     pthread_mutex_destroy(&condition1);
     pthread_mutex_destroy(&condition2);
     pthread_mutex_destroy(&condition3);
     pthread_mutex_destroy(&lock1);
     pthread_mutex_destroy(&lock2);
     pthread_mutex_destroy(&lock3);

}