 
# Q3

Client-server using socket

## running

make sure that port 8001 is free and server and client being run in different terminals is better.

```bash
g++ -o server server.cpp -lpthread
g++ -o client client.cpp -lpthread
./server <num_threads> &
./client
```

## Usage

the first line is the number of tasks t, and the following t lines will contain tasks
* insert <num> <msg> - inserts a key value pair
* delete <num> - delets a key value pair
* update <num> <msg> - updates a key value pair
* fetch <num> - fetches the value of the key
* concat <num> <num> - concatenate the values of the given keys
