# Q3) Multithreaded client and server

This question involves 2 parts.
The server side and the client side.

### The server side:
    The main thread is responsible for handling requests. We maintain a global queue. The main thread (which handles requests) pushes client socket IDs into the the queue. These are popped by the worker threads when they are free. The worker threads are assigned to respective clients after this. We pass a command line argument which describes the number of worker threads we shall be using in the server side.

### The client side:
    We take the input here and establish the communication between server and client. We make use of the provided functionality in order to send commands and recieve the output msg.

We make use of semaphor in this question so as to ensure that we are not popping from an empty queue. We keep an array of strings and maintain this on the server side. Based on commands from the client, we make changes to it.

Steps for compiling and executing this code:

We open 2 terminals: for server and client respectively.
1. We first set up the server on the first terminal
For compiling
g++ q3_server.cpp -o server
for executing (m here is the required number of worker threads).
./server m

2. for setting up the client
g++ q3_client.cpp -o client
For executing.
./client

we now paste the input in the client side and can observe the communication between server and client on this terminal.
