#include <bits/stdc++.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

#include <pthread.h>
#include <semaphore.h>

////////////////////////////

vector<pthread_t> workers;
queue<int> q;
pthread_mutex_t mutexForQueue;
sem_t semaphoreForQueue;
vector<string> arr;
vector<pthread_mutex_t> mutexForArr;

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    // cout << "In readstring_from_socket function \n";
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    // cout << "In readstring_from_socket function part 2\n";

    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void *handle_connection(void *arguments)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    // cout << "COnnection handled hmm" << endl;
    int received_num, sent_num;

    /* read message from client */
    while (true)
    {
        sem_wait(&semaphoreForQueue);
        pthread_mutex_lock(&mutexForQueue);
        int client_socket_fd = q.front();
        q.pop();
        pthread_mutex_unlock(&mutexForQueue);

        int ret_val = 1;
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        cout << "read string on socket" << cmd << " \n";

        ret_val = received_num;
        int indx = 0, key = 0;
        string command = "", arg = "";
        int i;
        int prev = 0;
        for (i = 0; i < cmd.length(); i++)
        {
            if (cmd[i] == ' ')
            {
                break;
            }
        }
        for (prev; prev < i; prev++)
        {
            indx = indx * 10 + (cmd[prev] - '0');
        }
        prev++;
        for (i = i + 1; i < cmd.length(); i++)
        {
            if (cmd[i] == ' ')
                break;
        }
        for (prev; prev < i; prev++)
        {
            command += cmd[prev];
        }
        prev++;
        for (i = i + 1; i < cmd.length(); i++)
        {
            if (cmd[i] == ' ')
                break;
        }
        for (prev; prev < i; prev++)
        {
            key = key * 10 + (cmd[prev] - '0');
        }
        prev++;
        for (i = i + 1; i < cmd.length(); i++)
            arg += cmd[i];

        // cout << indx << " " << command << " " << key << " " << arg << endl;
        string responseString = "";
        if (command == "insert")
        {
            if (arr[key] == "")
            {
                pthread_mutex_lock(&mutexForArr[key]);
                arr[key] = arg;
                pthread_mutex_unlock(&mutexForArr[key]);
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": Insertion successful";
            }
            else
            {
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": Key already exists.";
            }
        }
        else if (command == "concat")
        {
            int key2 = stoi(arg);
            if (arr[key] != "" && arr[key2] != "")
            {
                pthread_mutex_lock(&mutexForArr[key]);
                pthread_mutex_lock(&mutexForArr[key2]);
                string temp = arr[key];
                arr[key] += arr[key2];
                arr[key2] += temp;
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + " : " + arr[key2];
                pthread_mutex_unlock(&mutexForArr[key]);
                pthread_mutex_unlock(&mutexForArr[key2]);
            }
            else
            {
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": Concat failed as at least one of the keys does not exist.";
            }
        }
        else if (command == "delete")
        {
            if (arr[key] != "")
            {
                pthread_mutex_lock(&mutexForArr[key]);
                arr[key] = "";
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": Deletion successful.";
                pthread_mutex_unlock(&mutexForArr[key]);
            }
            else
            {
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": No such key exists.";
            }
        }
        else if (command == "update")
        {
            if (arr[key] != "")
            {
                pthread_mutex_lock(&mutexForArr[key]);
                arr[key] = arg;
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + " : " + arg;
                pthread_mutex_unlock(&mutexForArr[key]);
            }
            else
            {
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": No such key exists.";
            }
        }
        else if (command == "fetch")
        {
            if (arr[key] != "")
            {
                pthread_mutex_lock(&mutexForArr[key]);
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + " : " + arr[key];
                pthread_mutex_unlock(&mutexForArr[key]);
            }
            else
            {
                responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": No such key exists.";
            }
        }
        else
        {
            responseString = to_string(indx) + " : " + to_string(pthread_self()) + ": Invalid Command";
        }

        // debug(ret_val);
        // printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }

        // cout << "Client sent : " << cmd << endl;
        // if (cmd == "exit")
        // {
        //     cout << "Exit pressed by client" << endl;
        //     goto close_client_socket_ceremony;
        // }

        string msg_to_send_back = responseString;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            goto close_client_socket_ceremony;
        }
        // close(client_socket_fd);
    }
close_client_socket_ceremony:
    // close(client_socket_fd);

    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    int i, j, k, t, n;
    ////////////////////////////////////////////////////////////////////////s
    int m = 0;
    if (argc != 2)
    {
        cout << "Invalid Command!";
    }
    else
    {
        m = atoi(argv[1]);
    }
    workers.resize(m);
    pthread_mutex_init(&mutexForQueue, NULL);
    sem_init(&semaphoreForQueue, 0, 0);
    arr.assign(105, "");
    mutexForArr.assign(105, PTHREAD_MUTEX_INITIALIZER);
    for (int i = 0; i < m; i++)
    {
        pthread_create(&workers[i], NULL, handle_connection, NULL);
    }
    ////////////////////////////////////////////////////////////////////////e

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        //////////////////////////////////////////////////pushing into queue
        pthread_mutex_lock(&mutexForQueue);
        q.push(client_socket_fd);
        sem_post(&semaphoreForQueue);
        pthread_mutex_unlock(&mutexForQueue);

        //......////////////////////////////////////////////////////////////
        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(workers[i], NULL);
    }
    close(wel_socket_fd);
    return 0;
}