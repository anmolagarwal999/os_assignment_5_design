# Q1) An alternate course allocation Portal 
This question requires us to do allocation of students, TAs from various labs to certains courses.
We have 2 types of threads.
    - Student Threads
    - Course Threads

We first obtain the input and initialize all threads.

## Course Thread:
    In the course thread, we start by looking for TAs. 
    - If we find a TA, we lock him to take the session for that course and look for students.
    - If we do not find a TA who is free, but find a TA who can take tutorial in future, we keep waiting.
    - If none of the allowed TAs can take tutorial in future, we say that the course has ended.
    1. If the course ends, we start an infinite loop and find students who have this ended course as current preference. We signal them that this course can't be taken, and increase their priority.
    2. If we find a TA, we look for students who have the current course as their preference. We can choose a maximum of "seat" number of students per tutorial. We select them and start tutorial.
    3. We signal to the student thread that the tutorial is over, after which the student thread decided whether the course has been allotted or not based on whether the probability is greater than 0.5 or not.
    4. If probability is less than 0.5, we say that the course has not been allotted.
    5. We increase the count of the number of tutorial sessions taken by the TA and then free the TA to take other sessions.

## Student Thread:
    We start by making the thread sleep by the amount of time specified in the input. At the end of this sleep, we initialize the current priority to 0, and the process starts.
    - We acquire the inTutorial lock. Now the conditional wait is based on the condition that tutorial has ended or not. Once the tutorial ends, the course thread signals and the thread acquires the mutex again. 
    - We now see if current priority of student is same as before the wait. If same, we check the probability and decide whether the student gets the course or not.
    - If priority has changed, we check if other preferences are available. If other preferences are available, we update the current preference, otherwise end the loop and hence the thread.

## For Labs:
    In each lab, we store the number of time a TA has taken the session. If the TA reaches their limit, they can't take any other tutorial. We also maintain a lock corresponding to each TA which signified whether the TA is taking a session or not.


After all the student threads end, we end the program.
Instructions for compiling and running the code:

For compiling
g++ main.c -lpthreads

For running
./a.out
- paste the input and hit enter to see the process of allocation in real time.
