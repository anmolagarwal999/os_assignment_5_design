#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include "q1.h"

////////////////////////////////////////////////////////////////////////
// global variables
int nStudents, nCourses, nLabs;
struct student *students;
struct course *courses;
struct lab *semLabs;

void getInput()
{
    scanf("%d%d%d", &nStudents, &nLabs, &nCourses);
    ///////////////////////////////////////////////
    // course input
    courses = (struct course *)malloc(nCourses * sizeof(struct course));
    for (int i = 0; i < nCourses; i++)
    {
        courses[i].labsForCourse = NULL;
        scanf("%s%f%d%d", courses[i].courseName, &courses[i].interest, &courses[i].maxSlots, &courses[i].numLabs);
        courses[i].labsForCourse = (int *)malloc(courses[i].numLabs * sizeof(int));
        courses[i].courseNum = i;
        for (int j = 0; j < courses[i].numLabs; j++)
        {
            scanf("%d", &courses[i].labsForCourse[j]);
        }
    }

    //////////////////////////////////////////////////
    // student input
    students = (struct student *)malloc(nStudents * sizeof(struct student));
    for (int i = 0; i < nStudents; i++)
    {
        scanf("%f", &students[i].calibre);
        for (int j = 0; j < 3; j++)
            scanf("%d", &students[i].coursePreference[j]);
        scanf("%d", &students[i].waitTime);
        students[i].rollNo = i;
        students[i].currPriority = -1;
        students[i].assigned = 0;
        students[i].doneWithCourse = 0;
        pthread_mutex_init(&students[i].inTutorial, NULL);
        pthread_cond_init(&students[i].tutCompleted, NULL);
        pthread_mutex_init(&students[i].studentLock, NULL);
        pthread_cond_init(&students[i].updatesMade, NULL);
    }

    //////////////////////////////////////////////////
    // Labs
    semLabs = (struct lab *)malloc(nLabs * sizeof(struct lab));
    for (int i = 0; i < nLabs; i++)
    {
        scanf("%s%d%d", semLabs[i].labName, &semLabs[i].totTA, &semLabs[i].maxTimes);
        semLabs[i].labNo = i;
        semLabs[i].numLabsTaken = (int *)malloc(sizeof(int) * semLabs[i].totTA);
        semLabs[i].taLocks = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * semLabs[i].totTA);
        for (int j = 0; j < semLabs[i].totTA; j++)
        {
            pthread_mutex_init(&semLabs[i].taLocks[j], NULL);
            semLabs[i].numLabsTaken[j] = 0;
        }
    }
    printf("done with input.\n");
}

void *studentThread(void *args)
{
    // an infinite loop until we get the course or run out of pref.
    // printf("here in student\n");
    struct student *currStudent = (struct student *)args;
    int roll = currStudent->rollNo;
    sleep(currStudent->waitTime);
    printf("Student %d has registered\n", currStudent->rollNo);
    currStudent->currPriority = 0;
    int itr = 0;
    while (1)
    {
        itr++;
        // signal to the course locking this student that updates have been made

        int currentPriority = currStudent->currPriority;
        if (currentPriority == 3)
        {
            RED
                printf("Student %d did not get any preferred course.\n", roll);
            WHITE
            break;
        }

        currStudent->currPreference = currStudent->coursePreference[currentPriority];
        currStudent->doneWithCourse = 0;
        // printf("Acquired Lock of tut for student %d\n", currStudent->rollNo);
        pthread_mutex_lock(&currStudent->inTutorial);
        // wait until course sends u back.
        // while (currStudent->doneWithCourse == 0)
        pthread_cond_wait(&currStudent->tutCompleted, &currStudent->inTutorial);

        // change has been made.
        if (currStudent->currPriority != currentPriority)
        {
            currentPriority = currStudent->currPriority;
            if (currentPriority == 3)
            {
                RED
                    printf("Student %d did not get any preferred course.\n", roll);
                WHITE
                break;
            }
            else
            {
                GREEN
                int pref1 = currStudent->coursePreference[currentPriority - 1];
                int pref2 = currStudent->coursePreference[currentPriority];
                printf("Student %d has changed preference from %s to %s. \n", currStudent->rollNo, courses[pref1].courseName, courses[pref2].courseName);
                WHITE
            }
        }
        else
        {
            double prob = currStudent->calibre * courses[currStudent->coursePreference[currentPriority]].interest;
            if (prob < 0.5)
            {
                currStudent->currPriority++;
                currentPriority = currStudent->currPriority;
                if (currentPriority == 3)
                {
                    RED
                        printf("Student %d did not get any preferred course.\n", roll);
                    WHITE
                    break;
                }
                else
                {
                    GREEN
                    int pref1 = currStudent->coursePreference[currentPriority - 1];
                    int pref2 = currStudent->coursePreference[currentPriority];
                    printf("Student %d has changed preference from %s to %s.\n", currStudent->rollNo, courses[pref1].courseName, courses[pref2].courseName);
                    WHITE
                }
            }
            else
            {
                CYAN
                    currStudent->currPriority = 3;
                printf("Student %d has been assigned %s permanently.\n", roll, courses[currStudent->coursePreference[currentPriority]].courseName);
                WHITE
                break;
            }
        }
        pthread_mutex_unlock(&currStudent->inTutorial);
        pthread_cond_signal(&currStudent->updatesMade);
    }
    pthread_cond_signal(&students[roll].updatesMade);
    pthread_exit(NULL);
}

void *courseThread(void *args)
{
    struct course *currCourse = (struct course *)args;
    // printf("%d\n", currCourse->courseNum);
    while (1)
    {
        bool TApossible = false;
        bool TAfound = false;
        int l = -1, t = -1;
        for (int i = 0; i < currCourse->numLabs; i++)
        {
            int labNum = currCourse->labsForCourse[i];
            for (int j = 0; j < semLabs[labNum].totTA; j++)
            {
                if (semLabs[labNum].numLabsTaken[j] < semLabs[labNum].maxTimes)
                {
                    TApossible = true;
                }
                pthread_mutex_lock(&semLabs[labNum].taLocks[j]);
                if (semLabs[labNum].numLabsTaken[j] < semLabs[labNum].maxTimes)
                {
                    TAfound = true;
                    l = labNum;
                    t = j;
                    break;
                }
                pthread_mutex_unlock(&semLabs[labNum].taLocks[j]);
            }
            if (TAfound)
                break;
        }
        if (TApossible)
        {
            if (TAfound)
            {
                // printf("COurse %d found TA",currCourse->courseNum);
                int seats = (random() % currCourse->maxSlots) + 1;
                // search students
                int studentsFound = 0;
                bool eligible[nStudents];
                for (int i = 0; i < nStudents; i++)
                {
                    eligible[i] = false;
                    pthread_mutex_lock(&students[i].studentLock);
                    if (students[i].currPriority != -1 && students[i].currPriority != 3)
                    {
                        int currPref = students[i].coursePreference[students[i].currPriority];
                        if (currPref == currCourse->courseNum)
                        {
                            eligible[i] = true;
                            studentsFound++;
                        }
                        else
                        {
                            pthread_mutex_unlock(&students[i].studentLock);
                        }
                    }
                    else
                    {
                        pthread_mutex_unlock(&students[i].studentLock);
                    }
                    if (studentsFound == seats)
                        break;
                }
                if (studentsFound > 0)
                {
                    MAGENTA
                    printf("TA %d from Lab %s has been allocated to course %s.\n", t, semLabs[l].labName, currCourse->courseName);
                    WHITE

                    YELLOW
                    printf("Tutorial has started for course %s with %d students.\n", currCourse->courseName, studentsFound);
                    WHITE
                    // take tut
                    sleep(2);
                    // printf("DONE sleeping %d", currCourse->courseNum);
                    for (int i = 0; i < nStudents; i++)
                    {
                        if (eligible[i])
                        {
                            students[i].doneWithCourse = 1;
                            pthread_cond_signal(&students[i].tutCompleted);
                            // sleep(1);
                            // pthread_cond_wait(&students[i].updatesMade, &students[i].studentLock);
                            pthread_mutex_unlock(&students[i].studentLock);
                        }
                    }
                    semLabs[l].numLabsTaken[t]++;
                    MAGENTA
                    printf("TA %d from Lab %s has left tut of course %s after his %d TA-ship.\n", t, semLabs[l].labName, currCourse->courseName, semLabs[l].numLabsTaken[t]);
                    WHITE
                }
                pthread_mutex_unlock(&semLabs[l].taLocks[t]);
            }
            else
                continue;
        }
        else
        {
            YELLOW
            printf("Course %s has ended, and no further tutorials can be held.\n",currCourse->courseName);
            WHITE
            while (1)
            {
                // keep signaling this course's students that u won't get anything here :(
                for (int i = 0; i < nStudents; i++)
                {
                    pthread_mutex_lock(&students[i].studentLock);
                    if (students[i].currPriority != -1 && students[i].currPriority != 3)
                    {
                        int currPref = students[i].coursePreference[students[i].currPriority];
                        if (currPref == currCourse->courseNum)
                        {
                            students[i].currPriority++;
                            students[i].doneWithCourse = 1;
                            pthread_cond_signal(&students[i].tutCompleted);
                            // sleep(1);
                            // pthread_cond_wait(&students[i].updatesMade, &students[i].studentLock);
                            pthread_mutex_unlock(&students[i].studentLock);
                        }
                    }
                    pthread_mutex_unlock(&students[i].studentLock);
                }
            }
        }
    }
    pthread_exit(NULL);
}

void initializingThreads()
{
    // printf("Here 1");
    for (int i = 0; i < nCourses; i++)
    {
        pthread_create(&courses[i].tid, NULL, courseThread, &courses[i]);
    }
    // printf("Here 2");

    for (int i = 0; i < nStudents; i++)
    {
        pthread_create(&students[i].tid, NULL, studentThread, &students[i]);
    }
}

void waitingForStudentThreads()
{
    for (int i = 0; i < nStudents; i++)
    {
        pthread_join(students[i].tid, NULL);
    }
}

int main(void)
{
    getInput();
    initializingThreads();
    int i = 0;

    // sleep(5);
    // for (int j = 0; j < nStudents; j++)
    // {
    //     pthread_cond_signal(&students[j].tutCompleted);
    // }
    waitingForStudentThreads();

    return 0;
}