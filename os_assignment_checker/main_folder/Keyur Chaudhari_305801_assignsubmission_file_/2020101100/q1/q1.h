

struct student
{
    pthread_t tid;
    int rollNo;
    pthread_mutex_t inTutorial;
    pthread_mutex_t studentLock;
    float calibre;
    int coursePreference[3];
    int currPreference;
    int currPriority;
    pthread_cond_t tutCompleted;
    pthread_cond_t updatesMade;
    int waitTime;
    int assigned;
    int doneWithCourse;
};

struct course
{
    pthread_t tid;
    float interest;
    int maxSlots;
    int numLabs;
    int *labsForCourse;
    char courseName[50];
    int courseNum;
};

struct lab
{
    int totTA;
    int labNo;
    int maxTimes;
    pthread_mutex_t *taLocks;
    int *numLabsTaken;
    char labName[50];
};

// for output formatting
#define RED printf("\033[0;31m");
#define GREEN printf("\033[0;32m");
#define YELLOW printf("\033[0;33m");
#define BLUE printf("\033[0;34m");
#define MAGENTA printf("\033[0;35m");
#define CYAN printf("\033[0;36m");
#define WHITE printf("\033[0m");