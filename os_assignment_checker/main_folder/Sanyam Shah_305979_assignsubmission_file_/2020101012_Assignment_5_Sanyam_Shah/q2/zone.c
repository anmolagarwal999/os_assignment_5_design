#include "main.h"

void *handle_zone(void *arg)
{
    zone *curr_zone = (zone *)arg;
    while (1)
    {
        for (int i = 0; i < num_people; i++)
        {
            if (curr_zone->curr_cap < curr_zone->max_cap && slist[i].seat == 0 && slist[i].entered == 1)
            {
                int x = rand()%2;
                if (x==1)
                {
                    pthread_mutex_lock(zonelock + curr_zone->id);
                    if (slist[i].team == curr_zone->zone_id || slist[i].team == 'N' || (curr_zone->zone_id == 'N' && slist[i].team == 'H'))
                    {
                        pthread_mutex_lock(speclock + i);
                        slist[i].zone = curr_zone->zone_id;
                        slist[i].seat = 1;
                        curr_zone->curr_cap++;
                        printf(BLU "%s has got a seat in zone %c\n" RESET, slist[i].name, curr_zone->zone_id);
                        pthread_mutex_unlock(speclock + i);
                        pthread_cond_signal(seat_cond + i);
                    }
                    pthread_mutex_unlock(zonelock + curr_zone->id);
                }
                else
                {
                    pthread_mutex_lock(zonelock + curr_zone->id);
                    if ((curr_zone->zone_id == 'N' && slist[i].team == 'H') || slist[i].team == curr_zone->zone_id || slist[i].team == 'N')
                    {
                        pthread_mutex_lock(speclock + i);
                        slist[i].zone = curr_zone->zone_id;
                        slist[i].seat = 1;
                        curr_zone->curr_cap++;
                        printf(BLU "%s has got a seat in zone %c\n" RESET, slist[i].name, curr_zone->zone_id);
                        pthread_mutex_unlock(speclock + i);
                        pthread_cond_signal(seat_cond + i);
                    }
                    pthread_mutex_unlock(zonelock + curr_zone->id);
                }
                
                
            }
        }
    }
}