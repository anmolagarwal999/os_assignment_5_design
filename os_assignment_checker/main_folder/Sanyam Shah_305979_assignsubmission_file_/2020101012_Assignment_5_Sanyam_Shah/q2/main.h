#ifndef __MAIN__H__
#define __MAIN__H__

#include "colors.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <semaphore.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <stdbool.h>

#define debug(x) printf("#%d : %d\n",x,x);
#define MAX_SPECTATORS 512
#define MAX_GROUPS 512
#define MAX_LENGTH 512
#define GOALING_CHANCES 1024

clock_t before;

int spectating_time;
int h_goals;
pthread_mutex_t glock;
int a_goals;
int num_groups;
int num_people;
int G;

typedef struct zone
{
    int id;
    int max_cap;
    char zone_id;
    int curr_cap;
}zone;

typedef struct spectator
{
    int id;
    int group_no;
    int entered;
    char name[MAX_LENGTH];
    char team;
    char zone;
    int T;
    int P;
    int goals;
    int seat;
}person;

typedef struct goal_chance
{
    char team;
    int time;
    double prob;
}gchance;

pthread_t spectator[MAX_SPECTATORS];
pthread_mutex_t speclock[MAX_SPECTATORS];
pthread_cond_t seat_cond[MAX_SPECTATORS];
pthread_cond_t enraged[MAX_SPECTATORS];
pthread_t a_zon;
pthread_t h_zon;
pthread_t n_zon;
pthread_t gol;
zone A;
zone H;
zone N;
pthread_mutex_t zonelock[3];
person slist[MAX_SPECTATORS];
gchance glist[GOALING_CHANCES];

void* handle_person(void* arg);
void* handle_zone(void* arg);
void* handle_goals(void* arg);


#endif  //!__MAIN__H__