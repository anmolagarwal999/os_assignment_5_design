#include "main.h"

int rng(int a, int b)
{ // [a, b]
    int dif = b - a + 1;
    int rnd = rand() % dif;
    return (a + rnd);
}

int main()
{
    srand(time(0));
    pthread_mutex_init(&glock,NULL);
    A.id = 0;
    H.id = 1;
    N.id = 2;
    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_init(zonelock+i,NULL);   
    }
    a_goals = 0;
    h_goals = 0;
    scanf("%d%d%d", &H.max_cap, &A.max_cap, &N.max_cap);
    H.curr_cap = 0;
    A.curr_cap = 0;
    N.curr_cap = 0;
    H.zone_id = 'H';
    N.zone_id = 'N';
    A.zone_id = 'A';
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);
    num_people = 0;
    for (int i = 0; i < num_groups; i++)
    {
        int k;
        scanf("%d", &k);

        for (int j = 0; j < k; j++)
        {
            slist[num_people + j].group_no = i;
            slist[num_people + j].seat = 0;
            slist[num_people + j].id = num_people + j;
            slist[num_people+j].entered = 0;
            scanf("%s%s%d%d%d", slist[num_people + j].name, &slist[num_people + j].team, &slist[num_people + j].T, &slist[num_people + j].P, &slist[num_people + j].goals);
            // printf("%s %c %d %d %d\n",slist[num_people + j].name, slist[num_people + j].team, slist[num_people + j].T, slist[num_people + j].P, slist[num_people + j].goals);
            pthread_mutex_init(speclock + num_people + j, NULL);
            pthread_cond_init(seat_cond + num_people + j, NULL);
            pthread_cond_init(enraged + num_people + j, NULL);

        }
        num_people += k;
    }
    scanf("%d", &G);
    for (int i = 0; i < G; i++)
    {
        scanf("%s%d%lf", &glist[i].team, &glist[i].time, &glist[i].prob);
    }
    before = clock();

    pthread_create(&a_zon, NULL, handle_zone, &A);
    pthread_create(&h_zon, NULL, handle_zone, &H);
    pthread_create(&n_zon, NULL, handle_zone, &N);
    pthread_create(&gol, NULL, handle_goals, glist);

    for (int i = 0; i < num_people; i++)
    {
        pthread_create(spectator + i, NULL, handle_person, slist + i);
    }

    for (int i = 0; i < num_people; i++)
    {
        pthread_join(spectator[i], NULL);
    }

    printf(RED "SIMULATION OVER!!!\n" RESET);

    return 0;
}
