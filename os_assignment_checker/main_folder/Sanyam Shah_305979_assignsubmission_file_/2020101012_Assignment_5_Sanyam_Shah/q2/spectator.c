#include "main.h"

void* handle_person(void* arg)
{
    int rc;
    struct timespec ts;
    struct timeval tp;    
    person* human = (person*)arg;
    clock_t init = before;
    clock_t difference;
    do
    {
        difference = clock() - init;
        difference /= CLOCKS_PER_SEC;
    } while (difference < human->T);
    // printf("%ld %d\n",difference,human->T);
    printf(BLU "%s has reached the stadium\n" RESET,human->name);
    human->entered = 1;
    pthread_mutex_lock(speclock+human->id);
    rc =  gettimeofday(&tp, NULL);
    ts.tv_sec  = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += (human->P);
    rc = pthread_cond_timedwait(seat_cond+human->id,speclock+human->id,&ts);
    if (rc==ETIMEDOUT && human->seat==0)
    {
        printf(MAG "%s could not get a seat\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        sleep(1);
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        return NULL;
    }
    
    if (human->team=='A')
    {
        rc =  gettimeofday(&tp, NULL);
        ts.tv_sec  = tp.tv_sec;
        ts.tv_nsec = tp.tv_usec * 1000;
        ts.tv_sec += spectating_time;
        if (h_goals>=human->goals)
        {
            printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
        if (rc==ETIMEDOUT)
        {
            printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    else if (human->team=='H')
    {
        rc =  gettimeofday(&tp, NULL);
        ts.tv_sec  = tp.tv_sec;
        ts.tv_nsec = tp.tv_usec * 1000;
        ts.tv_sec += spectating_time;
        if (a_goals>=human->goals)
        {
            printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
        if (rc==ETIMEDOUT)
        {

            printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
    if (rc==ETIMEDOUT)
    {
        printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
    if (human->zone=='A')
    {
        pthread_mutex_lock(zonelock);
        A.curr_cap--;
        pthread_mutex_unlock(zonelock);
    }
    else if (human->zone=='H')
    {
        pthread_mutex_lock(zonelock+1);
        H.curr_cap--;
        pthread_mutex_unlock(zonelock+1);
    }
    else if (human->zone=='N')
    {
        pthread_mutex_lock(zonelock+2);
        N.curr_cap--;
        pthread_mutex_unlock(zonelock+2);
    }    
    
    printf(GRN "%s is leaving for dinner\n" RESET,human->name);
    pthread_mutex_unlock(speclock+human->id);
    return NULL;
}