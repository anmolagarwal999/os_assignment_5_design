# Assignment 5 - Concurrency, Parallelism and Networking

---

## Question 2: The Clasico Experience

In this simulation, a football match is taking place at the Gachibowli Stadium. There are 2 teams involved: FC Messilona (Home Team) and Benzdrid CF (Away Team). People from all over the city are coming to the stadium to watch the match. You have to create a simulation where people enter the stadium, buy tickets to a particular zone (stand), watch the match and then exit.

There are 4 main entities in this simulation:

+ Zones (3 Zones in all: H, A, and N)
+ Person (Spectator: H (Home) Fan, A (Away) Fan, or N (Neutral) Fan)
+ Goals scored by the Home/Away Teams
+ Groups (of friends to which a person belongs)

##### How to run the program?

```sh
$make 
$./main
```

##### How to delete all the executables?

```shell
$make clean
```

### Input Format

```
<Capacity of Zone H> <Capacity of Zone A> <Capacity of Zone N>
<Spectating time X>
<Number of groups>
<Number of people in group 1>
<Name of person 1> <H/N/A> <Time of reaching stadium> <Patience time for person 1> <Num goals>
.
.
.
<Name of last person in group> <H/N/A> <Time of reaching stadium> <Patience time for last person> <Num
goals>
<Few lines describing group 2>
…
<Few lines describing the last group>
<Number of goal scoring chances throughout the match>
<Team with the first chance> <Time elapsed> <Probability of chance being converted into a goal>
.
.
.
<Team with the last chance> <Time elapsed> <Probability of chance being converted into a goal>
```

##### Assumptions:

MAX_SPECTATORS = 512
MAX_GROUPS = 512
MAX_STRING_LENGTH = 512
MAX_GOALING_CHANCES = 1024

### Code Explanation:

#### List of files:

`Makefile` : Used to compile and build the executable file.

`colors.h` : Used to color the output to the terminal corresponding to different events of given simulation.

`main.h` : Contains all the required headers for the given simulation to run and also the declaration of structs, pthread_t, pthread_mutex_t, and pthread_cond_t which are later used throughout all the files (threads to be specific) to execute the required functions of the program. 

`main.c`: It contains two functions: 1) `Random Number Generator function` and 2) `main` function. It contains the code for necessary input required provided by the user and also initialization of certain other variables as mntione din main.h file.

`zone.c`; Contains implementation to handle threads of each zone i.e 3 in all, H Zone, A Zone, and N Zone.

`spectator.c`: Contains implementation of tracking progress of each spectator via their respective individually assigned thread from the time spectator enters the simulations, gets a ticket, to exiting from the stadium

`goals.c`: Monitors the goal scoring chances of both the teams, and sends signals to the spectator thread if the opponent teams’ goals exceed the given limit for a spectator.

#### Literal Explanation:

`main.c`:  Initially, we seed the random number generator with the given number (in our case time(0)) which will be later on used to determine non deterministically whether the goals are scored or not based on the probabilistic aspects of the goal scoring chance of the team as per the input provided.

Here, we initialize certain variables as provided in main.h file based on the program logic and others are initialized based on the input provided by the user as per the input format. Here, we create thread for each zone, each spectator and one for handling goals in the football match that takes place simultaneosly with other things.

We exit the simulation, once all the spectators exit the stadium after arriving there. Hence, we also called pthread_join function in order to wait for each spectator thread to exit stadium before completing the simulation. 

`zone.c`: For each zone, we enter an infinite while loop where we go through all the possible spectators and check if they bought the ticket and if the current occupancy of zone is less than its maximum capacity, then we let the spectator in based on their eligible zones. We also send a signal to the spectator thread once the seat in that zone is confirmed so that the spectator can stop waiting. Since it is also mentioned that the fans do not have any preference among the eligible zones, we use the rand() function to take care of it.

`spectator.c`: For each spectator we maintain a timespec and timeval struct which we later use for the conditional timed wait (via pthread_cond_timedwait() function). Initially, we wait for time T before letting the spectator enter the stadium using clock() functionality. Then using the timedwait condition for P seconds, we wait to get a seat in the eligible zones. If we do not get a signal interrupt, an errno ETIMEDOUT is returned which signifies that the duration for waiting has passed and now the spectator can exit the stadium. 

Then depending on the team supported by the spectator, we cond_timedwait on the number of goals scored by the opponent. We preempt the speculating time duration if the opponent team scores more than the goals that the fan can bear to watch. The spectator leaves the stadium owing to the bad defensive performance of his team and in doing so it empties one of the seat in zone alowing other spectators waiting to enjoy the match. But in case, the speculating time gets over, then the spectator would simply move out of the stadium and empty his/her seat. 

`goals.c`: Based on the time at which a goaling chance presents itself we non-deterministically decide whether goal is scored or not. We again use clock() functionality to achieve the objective. Based on the probability given we use rand() function to decide which team scores the goal. For every goal scored we check the list of present spectators in stadium and send them a signal indicating that the goals scored by the opponent team is more than that they expected.
