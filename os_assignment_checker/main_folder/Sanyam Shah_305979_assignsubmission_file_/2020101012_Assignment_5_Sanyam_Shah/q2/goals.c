#include "main.h"

void *handle_goals(void *arg)
{
    gchance *glist = (gchance *)arg;
    for (int i = 0; i < G; i++)
    {
        clock_t init = before;
        clock_t difference;
        do
        {
            difference = clock() - init;
            difference /= CLOCKS_PER_SEC;
        } while (difference < glist[i].time);
        
        int x = rand() % 100;
        double prob = glist[i].prob * 100.00f;
        if (x <= prob)
        {
            pthread_mutex_lock(&glock);
            if (glist[i].team == 'A')
            {
                a_goals++;
                if (a_goals%10==1)
                {
                    printf("Team %c has scored their %dst goal\n",glist[i].team,a_goals);
                }
                else if (a_goals%10==2)
                {
                    printf("Team %c has scored their %dnd goal\n",glist[i].team,a_goals);                    
                }
                else if (a_goals%10==3)
                {
                    printf("Team %c has scored their %drd goal\n",glist[i].team,a_goals);
                }
                else
                {
                    printf("Team %c has scored their %dth goal\n",glist[i].team,a_goals);
                }
                for (int j = 0; j < num_people; j++)
                {
                    if (slist[j].team=='H' && slist[j].goals<=a_goals )
                    {
                        pthread_cond_signal(enraged+j);
                    }
                }
                
                
            }
            else if(glist[i].team == 'H')
            {
                h_goals++;
                if (h_goals%10==1)
                {
                    printf("Team %c has scored their %dst goal\n",glist[i].team,h_goals);
                }
                else if (h_goals%10==2)
                {
                    printf("Team %c has scored their %dnd goal\n",glist[i].team,h_goals);                    
                }
                else if (h_goals%10==3)
                {
                    printf("Team %c has scored their %drd goal\n",glist[i].team,h_goals);
                }
                else
                {
                    printf("Team %c has scored their %dth goal\n",glist[i].team,h_goals);
                }
                for (int j = 0; j < num_people; j++)
                {
                    if (slist[j].team=='A' && slist[j].goals<=h_goals )
                    {
                        pthread_cond_signal(enraged+j);
                    }
                }
            }
            
            pthread_mutex_unlock(&glock);
        }
        else
        {
            pthread_mutex_lock(&glock);
            if (glist[i].team == 'A')
            {
                if (a_goals%10==1)
                {
                    printf("Team %c missed the chance to score their %dnd goal\n",glist[i].team,a_goals+1);
                }
                else if (a_goals%10==2)
                {
                    printf("Team %c missed the chance to score their %drd goal\n",glist[i].team,a_goals+1);                    
                }
                else if (a_goals%10==0)
                {
                    printf("Team %c missed the chance to score their %dst goal\n",glist[i].team,a_goals+1);
                }
                else
                {
                    printf("Team %c missed the chance to score their %dth goal\n",glist[i].team,a_goals+1);
                }
                
            }
            else if(glist[i].team == 'H')
            {
                if (h_goals%10==1)
                {
                    printf("Team %c missed the chance to score their %dnd goal\n",glist[i].team,h_goals+1);
                }
                else if (h_goals%10==2)
                {
                    printf("Team %c missed the chance to score their %drd goal\n",glist[i].team,h_goals+1);                    
                }
                else if (h_goals%10==0)
                {
                    printf("Team %c missed the chance to score their %dst goal\n",glist[i].team,h_goals+1);
                }
                else
                {
                    printf("Team %c missed the chance to score their %dth goal\n",glist[i].team,h_goals+1);
                }
            }
            
            pthread_mutex_unlock(&glock);
        }
        
    }
    return NULL;
}