#include "server.h"

void* handle_connection(void* args)
{
    pthread_mutex_lock(&mainlock);
    int socket_fd = *((int*)args);
    input in;
    recv(socket_fd,&in,sizeof(in),0);
    output out;
    pthread_mutex_lock(dict_lock+in.key);
    string str;
    
    str = to_string(in.id)+":"+to_string(pthread_self())+":";
    if (strcmp(in.command,"insert")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {    
            dict[in.key] = "";
            int j = 0;
            while(in.val[j]!='\0')
            {
                dict[in.key] += in.val[j];
                j++;
            }
            str += "Insertion successful";

        }
        else
        {
            str += "Key already exists";
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"delete")==0)
    {

        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "No such key exists";
        }
        else
        {
            dict[in.key] = "-1";
            str += "Deletion successful";
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"update")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "Key does not exist";
        }
        else
        {
            dict[in.key] = "";
            int j = 0;
            while(in.val[j]!='\0')
            {
                dict[in.key] += in.val[j];
                j++;
            }
            str += in.val;
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"concat")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0 || dict[in.key2]=="-1")
        {
            str += "Concat failed as at least one of the keys does not exist";
        }
        else
        {
            string temp = dict[in.key];
            dict[in.key] += dict[in.key2];
            dict[in.key2] += temp;   
            str += dict[in.key2];
        }
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else if (strcmp(in.command,"fetch")==0)
    {
        if (strcmp(dict[in.key].c_str(),"-1")==0)
        {
            str += "Key does not exist";
        }
        else
        {
            str += dict[in.key];
        }        
        for (int i = 0; i < str.length(); i++)
        {
            out.str[i] = str[i];
        }
        out.str[str.length()] = '\0';
        send(socket_fd,&out,sizeof(out),0);
    }
    else
    {
        str = "Invalid command";
    }
    pthread_mutex_unlock(dict_lock+in.key);
    pthread_mutex_unlock(&mainlock);
    // sleep(2);
    return NULL;
}

void* thread_function(void* args)
{
    while (1)
    {
        int *client;
        pthread_mutex_lock(&que_lock);
        if ((client=pop())==NULL)
        {
            pthread_cond_wait(&que_cond,&que_lock);
            client = pop();
        }
        pthread_mutex_unlock(&que_lock);
        if (client!=NULL)
        {
            handle_connection(client);
        }
    }
    return NULL;
}

int main(int argc, char* argv[])
{
    pthread_mutex_init(&mainlock,NULL);
    que_cond = PTHREAD_COND_INITIALIZER;
    if (argc!=2)
    {
        fprintf(stderr,"Invalid number of arguments\n");
        exit(1);
    }

    pthread_mutex_init(&que_lock,NULL);
    for (int i = 0; i < 101; i++)
    {
        dict[i] = "-1";
        pthread_mutex_init(dict_lock+i,NULL);
    }
    

    int num_workers = atoi(argv[1]);
    for(int i=0;i<num_workers;i++)
    {
        pthread_mutex_init(workerlock+i,NULL);
        pthread_create(&thread_pool[i], NULL, thread_function, NULL);
    }

    int wel_socket_fd, port_number;
    socklen_t clilen;
    
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); 

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(wel_socket_fd, MAX_CLIENTS);
    std::cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while(1)
    {
        int client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR on accept");
            exit(-1);
        }
        pthread_mutex_lock(&que_lock);
        push(&client_socket_fd);
            
        pthread_cond_signal(&que_cond);
        pthread_mutex_unlock(&que_lock);
    }
    
}