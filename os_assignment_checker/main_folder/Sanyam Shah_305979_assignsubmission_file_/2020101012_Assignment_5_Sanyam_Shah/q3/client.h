#ifndef __CLIENT__H__
#define __CLIENT__H__

#include <stdio.h>
#include <time.h>
#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include <chrono>
using namespace std;
using namespace std::chrono;


#define debug(x) cout << #x << " : " << x << endl;

#define SERVER_PORT 8001
#define  MAX_LENGTH 512

clock_t before;
typedef struct wrap
{
    int id;
    long long time;
    char command[10];
    int key;
    int key2;
    char val[MAX_LENGTH];
}input;

typedef struct ret
{
    char str[MAX_LENGTH];
}output;

pthread_mutex_t userlock[256];
pthread_mutex_t mainlock;

#endif  //!__CLIENT__H__