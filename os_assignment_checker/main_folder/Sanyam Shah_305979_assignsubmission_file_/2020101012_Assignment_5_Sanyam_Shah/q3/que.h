#ifndef __QUE__H__
#define __QUE__H__

struct node {
    struct node* next;
    int* client_socket;
};

typedef struct node node_t;

void push(int* client_socket);
int* pop();

#endif  //!__QUE__H__