#ifndef __SERVER__H__
#define __SERVER__H__

#include "que.h"
#include <stdio.h>
#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <semaphore.h>

#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;

#define MAX_CLIENTS 500
#define PORT_ARG 8001
#define  MAX_LENGTH 512
#define MAX_THREAD_POOL_SIZE 256

pthread_t thread_pool[MAX_THREAD_POOL_SIZE]; 

typedef struct ret
{
    char str[MAX_LENGTH];
}output;

typedef struct wrap
{
    int id;
    long long time;
    char command[10];
    int key;
    int key2;
    char val[MAX_LENGTH];
}input;

string dict[102];
pthread_mutex_t dict_lock[102];
pthread_mutex_t workerlock[256];
pthread_mutex_t mainlock;
pthread_cond_t que_cond;
pthread_mutex_t que_lock;

#endif  //!__SERVER__H__
