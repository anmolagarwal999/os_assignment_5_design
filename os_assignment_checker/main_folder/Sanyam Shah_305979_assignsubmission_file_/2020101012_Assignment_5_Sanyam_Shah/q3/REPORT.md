# Assignment 5 - Concurrency, Parallelism and Networking

---

## Question 3: Multithreaded client and server

**Context:** Multiple clients making requests to a single server program with is own reserved worker thread pool. Here, we implement a simple scenario where multiple clients are making requests to a single server regarding changes in global dictionary. 

##### How to run the server program?

```sh
$g++ -g server.cpp que.c -o server -lm -pthread
$./server <Number of Worker Threads>
```

##### How to run the client program?

```shell
$g++ -g client.cpp -o client -lm -pthread
$./server	
```

#### Client Program

+ Each user request has 2 main characteristics:
  + The time at which the request has been made
  + The nature of the request/the command issued
+ There are ‘m’ user requests throughout the course of the simulation.
+ Each of these user request threads will then try to connect with the server independent of each other (to give
  the impression of multiple users from different parts of the world, sending commands to the same server).
+ Once the connection to the server has been made, the user request thread can communicate with the
  assigned worker thread using a TCP socket.
+ Whatever response the user request thread receives from the server, it must output it.

#### Server Program

+ The server will maintain a dictionary.
+ The dictionary values must be common across all clients. The key-value pairs created by one client should be
  visible to all other connected clients.
+ The server has a multithreaded architecture (with a pool of worker threads).
+ As soon as the server starts, it spawns these ‘n’ worker threads. Then, the server begins to listen for client requests to connect. 
+ Whenever a new client’s connection request is accepted (using the accept() system call), the server is expected to then designate one of the worker threads to deal with the client’s request.
+ The worker threads, while dealing with the client requests, might need to perform read/write operations on the
  dictionary.

### Input Format

```sh
<Total number of user requests throughout the simulation (M)>
<Time in sec after which the request to connect to the server is to be made> <cmd with appropriate arguments>
.
.
.
```

### Commands available for the user:

We constrain the ‘keys’ to be non-negative integers (<=100) and ‘values’ to be non-empty strings containing
only alphabets and numbers.

+ **insert \<key\> \<value\>** : This command is supposed to create a new “key” on the server’s dictionary and set its
  value as \<value\>.
+ **delete \<key\>** : This command is supposed to remove the **\<key\>** from the dictionary.
+ **update \<key\> \<value\>** : This command is supposed to update the value corresponding to **\<key\>** on the
  server’s dictionary and set its value as **\<value\>**.
+ **concat \<key1\> \<key2\>** : Let values corresponding to the keys before execution of this command be {key1:
  value_1, key_2:value_2}. Then, the corresponding values after this command's execution should be *{key1:*
  *value_1+value_2, key_2: value_2+value_1}.*
+ **fetch \<key\>** : must display the value corresponding to the key if it exists at the connected server.

##### Assumptions:

MAX_LENGTH = 512 
MAX_THREAD_POOL_SIZE = 256

### Code Explanation:

#### List of files:

`que.h` : Contains the struct node of the linked-list implementation of queue and queue related function prototypes as well.

`que.c`: Contains the implementation of queue data structure which is made thread-safe in server.cpp by using appropriate locks.

`server.h`; Contains appropriate header files along with user defined library “que.h”, necessary structs which are then used to send and revieve information server side, and certain condition variables, locks and thread variables.

`server.cpp`: Contains server side implementation of this program

`client.h`: Contains appropriate header files, necessary structs which are then used to send and revieve information client side, and certain condition variables, locks and thread variables.

`client.cpp`: Contains client side implementation of this program

#### Literal Explanation:

`que.c`: Contains the lisked list implementation of push and pop functionalities of FIFO data structure queue with appropriately defined nodes in the linked list.

`client.cpp`: At the beginning we take note of time with the help of high_resolution_clock provided by the chrono header file. Then we initialize appropriate variables as per the logic of the program. Depending on the name of the command, we take the remaining arguments and initialize the remaining elements of the struct.

After each input command, we wait if required as per the timestamp of the command and then create a thread for each user request. Each thread is executed by the handleuser function which takes in the input makes connection with the server, sends input to the server, and recieves the output from the server which is later on printed at the terminal. We use a mainlock so that the data stream is not interrupted when connection is made and when data is being sent or recieved. 

`server.cpp`: Here we take input from the command line argument to determine the number of threads in the worker pool. Then we create all those threads at once at the beginning of the program. Then we create a new socket at the port number 8001, bind it, and keep listening to it. Then we enter the while loop where we accept the connections when approached by clients. Once accepted, we keep pushing the user requests in the queue using the que_lock in order to make it thread-safe. Once enqueued, we send a signal to the condition variable associated with the queue. 

For each worker thread that we create, all of them wait on the condition variable, and try to pop the queue once the lock is acquired. Once the queue is not empty and pop operation is successful, handle_connection function is called, which recieves the intended input from the client connection, operates on it based on the command name. and then sends the formatted output string back to the client with correct concatenations so all that client would have to do is print the recieved string. 
