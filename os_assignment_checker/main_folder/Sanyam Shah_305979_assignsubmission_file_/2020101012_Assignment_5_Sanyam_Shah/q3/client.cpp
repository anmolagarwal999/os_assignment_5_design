#include "client.h"

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
    // pthread_mutex_lock(&mainlock);
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    // pthread_mutex_unlock(&mainlock);
    return socket_fd;
}

void* handleuser(void* args)
{
    input *in = (input*)args;
    struct sockaddr_in server_obj;
    // pthread_mutex_lock(userlock+in->id);
    pthread_mutex_lock(&mainlock); 
    int socket_fd = get_socket_fd(&server_obj);
    pthread_mutex_unlock(&mainlock);

    // printf("%s\n",in->command);
    pthread_mutex_lock(&mainlock); 
    send(socket_fd,in,sizeof(input),0);
    // pthread_mutex_unlock(&mainlock);

    output out;
    // pthread_mutex_lock(&mainlock); 
    recv(socket_fd,&out,sizeof(output),0);
    pthread_mutex_unlock(&mainlock);

    printf("%s\n",out.str);
    // pthread_mutex_unlock(&mainlock);
    // pthread_mutex_unlock(userlock+in->id);
    return NULL;
}

int main(int argc, char *argv[])
{    
    auto start = high_resolution_clock::now();
    pthread_mutex_init(&mainlock,NULL);
    int m;
    scanf("%d",&m);
    pthread_t users[m+2];
    for (int i = 0; i < m; i++)
    {
        pthread_mutex_init(userlock+i,NULL);
    }
    input args[m];
    for (int i = 0; i < m; i++)
    {
        args[i].id = i;
        scanf("%lld%s",&args[i].time,args[i].command);
        // printf("%d %s\n",args[i].time,args[i].command);
        if(strcmp(args[i].command,"insert")==0)
        {
            scanf("%d%s",&args[i].key,args[i].val);
        }
        else if(strcmp(args[i].command,"delete")==0)
        {
            scanf("%d",&args[i].key);
        }
        else if(strcmp(args[i].command,"update")==0)
        {
            scanf("%d%s",&args[i].key,args[i].val);
        }
        else if(strcmp(args[i].command,"concat")==0)
        {
            scanf("%d%d",&args[i].key,&args[i].key2);
        }
        else if(strcmp(args[i].command,"fetch")==0)
        {
            scanf("%d",&args[i].key);
        }
        else
        {
            cout << "Invalid command" << endl;
        }
        // if (i> 0 and args[i].time==args[i].time)
        // {
        //     pthread_create(users+i,NULL,handleuser,(void*)&args[i]);
        //     continue;
        // }
        
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<seconds>(stop - start);
        // if (duration.count() < args[i].time)
        // {
        //     usleep(args[i].time - duration.count());
        // }        
        // printf("%lld %ld\n",args[i].time,duration.count());
        if (args[i].time-duration.count()>0)
        {
            sleep(args[i].time-duration.count());
        }
        else
        {
            usleep(50);
        }
        
        pthread_create(users+i,NULL,handleuser,args+i);
        // sleep(1);
    }
    for (int i = 0; i < m; i++)
    {
        pthread_join(users[i],NULL);
    }
    
    return 0;
}