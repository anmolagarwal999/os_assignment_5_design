#include "main.h"

int rng(int a, int b) { // [a, b]
	int dif = b - a + 1 ; 
	int rnd = rand() % dif ;
	return (a + rnd);
}

int	main()
{
    srand(time(0));

    scanf("%d%d%d",&num_studs,&num_labs,&num_courses);

    for (int i = 0; i < num_courses; i++)
    {
        c_list[i].id = i;
        c_list[i].removed = false;
        scanf("%s%f%d%d",c_list[i].name,&c_list[i].interest,&c_list[i].max_slots,&c_list[i].total_labs);
        c_list[i].lab_ids = (int*)malloc(sizeof(int)*(c_list[i].total_labs));
        for (int j = 0; j < c_list[i].total_labs; j++)
        {
            scanf("%d",&c_list[i].lab_ids[j]);
        }
        pthread_mutex_init(courseMutex+i,NULL);
    }
    for (int i = 0; i < num_studs; i++)
    {
        s_list[i].done = false;
        s_list[i].course_allocated = -1;
        s_list[i].registered = false;
        s_list[i].id = i;
        s_list[i].curr_pref = 0;
        s_list[i].attended = false;
        pthread_cond_init(allocated + i,NULL);
        scanf("%f%d%d%d%d",&s_list[i].calibre,&s_list[i].pref[0],&s_list[i].pref[1],&s_list[i].pref[2],&s_list[i].time);
        pthread_mutex_init(studentMutex+i,NULL);
    }
    for (int i = 0; i < num_labs; i++)
    {
        l_list[i].id = i;
        l_list[i].flag = true;
        scanf("%s%d%d",l_list[i].name,&l_list[i].capacity,&l_list[i].max_limit);
        l_list[i].lab_TAs = (int*)malloc(sizeof(int)*l_list[i].capacity);
        l_list[i].lk_TAs = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t)*l_list[i].capacity);
        for (int j = 0; j < l_list[i].capacity; j++)
        {
            l_list[i].lab_TAs[j] = 0;
            pthread_mutex_init(l_list[i].lk_TAs+j, NULL);
        }
    }
    before = clock();

    for (int i = 0; i < num_studs; i++)
    {
        pthread_create(&studentThread[i], NULL, handle_students,s_list+i);
    }
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&courseThread[i], NULL, handle_courses,c_list+i);
    }
    for (int i = 0; i < num_studs; i++)
    {
        pthread_join(studentThread[i],NULL);

        // DESTROY BEFORE U SUBMIT

        pthread_mutex_destroy(studentMutex+i);
    }

    printf("Simulation over!\n");
    return 0;
}
