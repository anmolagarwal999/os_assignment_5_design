#include "main.h"

void *handle_students(void *arg)
{
    clock_t difference;
    student *stud = (student *)arg;
    do
    {
        difference = clock() - before;
        difference /= CLOCKS_PER_SEC;
    } while (difference < stud->time);
    printf(RED "Student %d has filled in preferences for course registration\n" RESET, stud->id);
    stud->registered = true;
    while (1)
    {
        pthread_mutex_lock(studentMutex+stud->id);
        while (stud->attended==false)
        {
            pthread_cond_wait(allocated+stud->id,studentMutex+stud->id);
            if (c_list[stud->pref[stud->curr_pref]].removed==true)
            {
                break;
            }
            
        }
        if (stud->curr_pref >= 3)
        {
            printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
            stud->done = true;
            pthread_mutex_unlock(studentMutex+stud->id);
            break;
        }
        while (c_list[stud->pref[stud->curr_pref]].removed==true)
        {
            
            stud->curr_pref++;
            if (stud->curr_pref >= 3)
            {
                printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                stud->done = true;
                pthread_mutex_unlock(studentMutex+stud->id);
                return NULL;
            }
            stud->attended = false;
            printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
        }
        
        
        
        
        if (stud->attended == true)
        {
            int r = rand() % 100;
            float prob = (c_list[stud->pref[stud->curr_pref]].interest) * (stud->calibre) * 100.00f;
            if (r <= prob)
            {
                printf(CYN "Student %d has selected course %s permanently\n" RESET, stud->id, c_list[stud->pref[stud->curr_pref]].name);
                stud->done = true;
                pthread_mutex_unlock(studentMutex+stud->id);
                break;
            }
            else
            {
                printf(CYN "Student %d has withdrawn from course %s\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref]].name);
                
                stud->curr_pref++;
                if (stud->curr_pref >= 3)
                {
                    printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                    stud->done = true;
                    pthread_mutex_unlock(studentMutex+stud->id);
                    break;
                }
                stud->attended = false;
                printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
                while (c_list[stud->pref[stud->curr_pref]].removed==true)
                {
                    
                    stud->curr_pref++;
                    if (stud->curr_pref >= 3)
                    {
                        printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                        stud->done = true;
                        pthread_mutex_unlock(studentMutex+stud->id);
                        return NULL;
                    }
                    stud->attended = false;
                    printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
                }
            }
        }
        pthread_mutex_unlock(studentMutex+stud->id);
    }
    
    return NULL;
}
