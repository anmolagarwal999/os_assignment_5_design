# Assignment 5 - Concurrency, Parallelism and Networking

---

## Question 1: An alternate course allocation Portal

Here an unconventional course registration system for the college is setup where a student can take trial classes of a course and can withdraw and opt for a different course if he/she does not like the course. Different labs in the college have been asked to provide students who can act as course TA mentors temporarily and take the trial tutorials. 

There are 4 main entities in this simulation:

+ Courses offered
+ Labs (each consisting of a set of TAs)
+ TAs (each belonging to exactly one lab and is available to take tutorials for courses for which the respective lab has been shortlisted)
+ Students (each participate in course registration process and attend tutorials if allocated the seat for that course)

##### How to run the program?

```sh
$make
$./main
```

##### How to delete all the executables?

```shell
$make clean
```

### Input Format

```
<num_students> <num_labs> <num_courses>
<name of course 1> <interest quotient for course 1> <maximum number of slots which can be allocated by a TA> <number of labs
from which course accepts TA> [list of lab IDs]
.
.
.
<name of the last course> <interest quotient for the last course> <maximum number of slots which can be allocated by a TA>
<number of labs from which course accepts TA> [list of lab IDs]
<calibre quotient of first student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
.
.
<calibre quotient of last student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
<name of first lab> <number of students/TA mentors in first lab> <number of times a member of the lab can TA in a course>
.
.
.
<name of last lab> <number of students.TA Mentors in the last lab> <number of times a member of the lab can TA in a course>
```

##### Assumptions:

MAX_STUDENTS = 500
MAX_LABS = 500
MAX_COURSES = 500
MAX_STRING_LENGTH = 75  

### Code Explanation:

#### List of files:

`Makefile` : Used to compile and build the executable file.

`colors.h` : Used to color the output to the terminal corresponding to different events of given simulation.

`main.h` : Contains all the required headers for the given simulation to run and also the declaration of structs, pthread_t, pthread_mutex_t, and pthread_cond_t which are later used throughout all the files to execute the required functions of the program. 

`main.c`: It contains two functions: 1) `Random Number Generator function` and 2) `main` function. It contains the code for necessary input required provided by the user.

`course.c`; Contains implementation to handle threads of each course and also handles the assignment of TAs to their respective courses and selecting eligible students during course allocation

`student.c`: Contains implementation of tracking progress of each student via their respective individually assigned thread from the time student registers for the course allocation process till the time it exits the simulation process.

#### Literal Explanation:

`main.c`:  Initially, we seed the random number generator with the given number (in our case time(0)) which will be later on used for non deterministic selecion of students for a course and allocation of maximum possible seas for a course aat a time. 

Then we take in the input as per the predefined input format amd also initialize the predefined structs, pthread_t, pthread_mutex_t, and pthread_cond_t so that they can be used by new threads. Then we create thread for each student and then for each course where we execute handle_students and handle_courses respectively using pthread_create function where we also pass on the input information via student and course struct respectively.

Condition for the simulation to finish is that all students must finish their registration process, either by selecting a course as per their preference or withdraw from all courses. Thus we wait for all threads to join the parent thread of the process using pthread_join functiom. 

`course.c`: For each course thread we run an infinite while loop, since we are not expected to terminate the thread in order to exit the simulation. For each course thread, running parallely with others, we sequentially go through each of the assigned lab, and through each of the available TAs associated with that lab. We initially check whether the TA has exceeded his/her maximum limit to counduct a tutorial for any course.

Since the **BONUS** is implemented for this question, when a selected member (M) of the lab who has done ‘t’ TAships, can take up his ‘t+1’ th TA ship only when all the other members within the lab have also done ‘t’ TA ships or are currently doing  their ‘t’ th TA ship. If the said condition is not satisfied, then M will wait for his fellow members so that the condition
can be satisfied. We check this condition by going through all the student mentors of selected lab and see if the difference between the currently selected TA and others do not exceed 1. If it does we skip through the loop and continu eto search for the other TA.

We also use locks for each TA before starting the tutorial. Once the TA is selected, we use RNG to  generate the possible number of seats available for that course. Then, for all the students we check that if he/she is registered, not exited the simulation, currently attending any other course, to see if this student is currently waiting for any seat. Once checked, we check that if his current preferemce matches to that of the given course and whether filled seats are less than total allocated seats. If a student passes all these constraints, he/she is selected for that course. Once all the students are selected, we start the tutorial. 

I have implemented **Case-1** as per the moodle announcement made regarding whether a tutorial can start with 0 seats filled  or not. For each tutorial i put that respecive thread to sleep for 5 seconds to provide the feel for simulation of tutorial being taking place currently. 

Once the tutorial is done, student lock is unlocked and a signal is sent to the conditional wait in student thread, to ask him/her to make a decision to either selecct from the course or withdraw from it.

Using few boolean parameters, we also check whether a lab has sufficient number of TAs availble or not and also whether course has been removed or not because of exhaustion of all the labs provided to the course. If course is removed, the I again enter the infinite loop where I keep on sending signals to students who have currently opted for that course.

`student.c`: For each student thread, we wait for the mentioned time to pass before beginning with the process of registration with the help of `clock()` functionality. Then we enter an infinite while loop which we’ll exit using break statements, once the student has decided to exit the simualtion. We use locks for each student before proceeding to execute its fuctionality. We use pthread_cond_wait function in order to wait for the course to either get removed or to wait for the course of student’s preference allocating him/her a seat in a tutorial. 

Since we are given three preferences overall we, go through the loop until and unless the students current preference exceeds 3 (i.e stud->curr_pref >= 3) which would result into student not getting any of his/her preferred course. We use non deterministic method as stated in pdf to determine whether a student will accept or withdraw from a course (course_interest*student_calibre). If the student withdraws from the given course, we change its preference by incrementing stud->curr_pref.  

We return from the student thread once student is unable to get any of the preferred course or selects a coure permanently.

