#ifndef __MAIN__H__
#define __MAIN__H__

#include "colors.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <semaphore.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdbool.h>

#define debug(x) printf("#%d : %d\n",x,x);

#define MAX_STUDENTS 500
#define MAX_LABS 500
#define MAX_COURSES 500
#define MAX_STRING_LENGTH 75  


typedef struct student
{
    int id;
    bool registered;
    float calibre;
    int pref[3];
    int curr_pref;
    int course_allocated;
    bool attended;
    int time;
    bool done;
} student;

typedef struct course
{
    int id;
    char name[MAX_STRING_LENGTH];
    float interest;
    int max_slots;
    int total_labs;
    int* lab_ids;
    bool removed;
} course;

typedef struct lab
{
    int id;
    char name[MAX_STRING_LENGTH];
    int capacity;
    int* lab_TAs;
    pthread_mutex_t* lk_TAs;
    int max_limit;
    bool flag;
} lab;

clock_t before;

pthread_cond_t allocated[MAX_STUDENTS];

int num_studs;
int num_labs;
int num_courses;

course c_list[MAX_COURSES];
pthread_t courseThread[MAX_COURSES];
pthread_mutex_t courseMutex[MAX_COURSES];

lab l_list[MAX_LABS];

student s_list[MAX_STUDENTS];
pthread_t studentThread[MAX_STUDENTS];
pthread_mutex_t studentMutex[MAX_STUDENTS];

int rng(int l, int r);
void* handle_students(void* arg);
void* handle_labs(void* arg);
void* handle_courses(void* arg);

#endif  //!__MAIN__H__