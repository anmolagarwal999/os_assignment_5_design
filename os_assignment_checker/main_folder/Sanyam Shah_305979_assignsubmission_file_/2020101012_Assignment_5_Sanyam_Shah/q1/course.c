#include "main.h"

void *handle_courses(void *arg)
{
    course *cors = (course *)arg;
    while (1)
    {
        bool anyTA = false;
        for (int i = 0; i < cors->total_labs; i++)
        {
            bool chk = true;
            for (int j = 0; j < l_list[cors->lab_ids[i]].capacity; j++)
            {
                if (l_list[cors->lab_ids[i]].lab_TAs[j] < l_list[cors->lab_ids[i]].max_limit && l_list[cors->lab_ids[i]].flag == true)
                {
                    pthread_mutex_lock(l_list[cors->lab_ids[i]].lk_TAs + j);
                    chk = false;
                    anyTA = true;
                    l_list[cors->lab_ids[i]].lab_TAs[j]++;
                    if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 1)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dst TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 2)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dnd TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 3)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %drd TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dth TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }

                    int slots = rng(1, cors->max_slots);
                    printf(BLU "Course %s has been allocated %d seat(s)\n" RESET, cors->name, slots);
                    int w = 0;
                    // printf("%s\n",cors->name);
                    while (w==0)
                    {
                    	for (int k = 0; k < num_studs; k++)
		            {                                                        
		                if (s_list[k].registered == true && s_list[k].done == false && s_list[k].pref[s_list[k].curr_pref] == cors->id && s_list[k].attended == false && w < slots)
		                {
		                    pthread_mutex_lock(studentMutex + k);
		                    printf("Student %d has been allocated a seat in course %s\n", k, cors->name);
		                    // s_list[k].course_allocated = cors->id;
		                    s_list[k].attended = true;
		                    // pthread_cond_signal(allocated+k);
		                    // s_list[k].attended = true;
		                    w++;
		                    // pthread_mutex_unlock(studentMutex + k);
		                }

		            }
		     }
                    

                    printf(YEL "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, cors->name, w, slots);
                    sleep(2);
                    printf(BLU "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name);
                    
                    for (int k = 0; k < num_studs; k++)
                    {                          
                        pthread_mutex_unlock(studentMutex + k);
                        pthread_cond_signal(allocated + k);
                        
                    }
                    pthread_mutex_unlock(l_list[cors->lab_ids[i]].lk_TAs + j);
                }
            }
            if (chk && l_list[cors->lab_ids[i]].flag == true)
            {
                printf(WHT "Lab %s no longer has students available for TA ship\n" RESET, l_list[cors->lab_ids[i]].name);
                l_list[cors->lab_ids[i]].flag = false;
            }
        }
        if (anyTA == false)
        {
            cors->removed = true;
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", cors->name);
            while (1)
            {                
                for (int i = 0; i < num_studs; i++)
                {
                    if (s_list[i].pref[s_list[i].curr_pref] == cors->id)
                    {
                        // pthread_mutex_lock(studentMutex + i);
                        pthread_cond_signal(allocated + i);
                        // pthread_mutex_unlock(studentMutex + i);
                    }
                }
            }
        }
    }
}
