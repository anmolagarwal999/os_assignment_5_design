# Q1: An Alternate Course Allocation Portal
> Vishva Saravanan R
> 2019113019

## Code Structure & Logic
### Global Variables and Custom Data Types
The program takes the total number of students, labs and courses, along with 
their description as input. These are stored in arrays having global scope of
their respective type. There are 4 custom data types defined: `Course`, `Student`,
`TA` and `Lab` to store attributes of courses, students, TAs and labs respectively.
Each one of these types also have a `mutex` attribute to help with thread-safe access
to the instances.

### Multithreading
After input parsing and global variable population, a number of threads equal to that
of the total number of students in the simulation are spawned, with the intent of
executing the **Student Routine**.  
Additional threads are spawned for each course in the simulation, executing the
**Course Routine**; and for each lab in the simulation, executing the **Lab Routine**.
The student threads are joined, so that the main thread will not complete execution 
and terminate the program before the simulation has ended; whereas the course threads 
are left unjoined. This is because the runtime of the simulation depends only on the 
number of students remaining in the simulation at any given point of time.  
The program terminates once all student threads have finished execution.

### Student Routine
Each student thread initially sleeps for `fill_time` number of seconds, to simulate
the time taken for ther student to register on the portal. A flag `course_finalized`
is maintained to check whether the student finalized a course. After the registration
delay, the student iterates through each one of their course preferences, checking
whether the selected course has been withdrawn. If not withdrawn, the student waits
until the course starts accepting students by use of a conditional variable belonging
to the `Course` instance.  
Once the course starts accepting, the student conditionally waits for an available slot. 
On getting a slot, the student decrements the availability counter by 1 while under a 
mutex lock. If the course gets withdrawn while the student is waiting for the course 
to start accepting or to get an available slot, the student is moved on to their next 
preference. Once allocated, the student conditionally waits until the tutorial starts.  
The thread sleeps for the duration of the tutorial to simulate the student attending
a tutorial, after which whether or not the student finalizes the course is 
probabilistically determined.  
If the student decides to finalize the course, it is announced and the student exits
the simulation. Else, the student moves on to repeat the above routine with their next 
preference. If there are no more preferences, the student's lack of allocation of a 
course is announced, and the student exits the simulation.

### Course Routine
Each course thread continuously manages the progression of the course. It starts off
by checking if the course has been withdrawn at the beginning of every iteration. If
it has been withdrawn, the thread initiates its termination sequence; setting some of
its attributes to account for the fact that this course will not be accepting any
students. If not, the course iterates over each of its eligible labs, and further
iterates over each TA mentor in each lab, looking for eligible TAs. An eligible TA is
one who isn't engaged in another course and has not exhausted the quota of TAships set
by their respective labs. Available TAs are identified using the `pthread_mutex_trylock()`
method while sequentially iterating over the array of potential TAs in a lab. Each TA's
`TAships` attribute is compared with that of the lab to determine their eligibility.  
If no eligible TAs are found for the course, it is announced that the course is
withdrawn and the thread initiates its termination sequence.  
Once an eligible TA has been found, their `TAships` attribute is incremented by 1.
A random number of slots are allocated for the tutorial, such that the number of slots
lies between 1 and the course's `max_slots` attribute (provided as part of the input).
Once the number of slots has been determined, the TA starts accepting students to the
course, and broadcasts a signal to the student threads waiting to get accepted. There
is then a slight delay in which the thread sleeps to give ample time for students to
register to the course. Once woken, the course stops accepting students, and the 
tutorial is started. The course routine sleeps for the duration of the tutorial after 
broadcasting the information of the tutorial's start to the student threads. Upon
completion, the tutorial is ended and the TA associated with the course is relieved
of their duties.

## Assumptions and Arbitrary Constants
- The simulation allows for tutorials of different courses to be conducted at the
  same time.
- The simulation allows for several courses to fill their slots with the students at
  the same time.
- The simulation allows for different courses to choose TAs parallely.
- After a TA allocates seats for a course tutorial, they wait for **2 seconds** before
  beginning the tutorial, to let students fill up the slots.
- Each tutorial lasts **6 seconds**.
- There is a post-tutorial delay of **1 second** for students to decide to 
  finalize/withdraw from the corresponding course.
- Each course searches for a TA sequentially in the order in which eligible labs were 
  input and in increasing order of TA index within a lab.
