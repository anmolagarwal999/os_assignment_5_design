#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define INPUT_BUFFER_MAX 1024
#define TUTORIAL_WAIT 2
#define TUTORIAL_DURATION 6
#define POST_TUTORIAL_DELAY 1

// utility functions & variables {{{
const int ANSI_RED = 31;
const int ANSI_GREEN = 32;
const int ANSI_YELLOW = 33;
const int ANSI_BLUE = 34;
const int ANSI_PURPLE = 35;
const int ANSI_CYAN = 36;
const int ANSI_WHITE = 37;

void cprintf(int color, char* format, ...) {
    char* color_code = calloc(16, sizeof(char));
    char* color_format = calloc(strlen(format) + 25, sizeof(char));
    sprintf(color_code, "\x1B[%dm", color);
    strcat(color_format, color_code);
    strcat(color_format, format);
    strcat(color_format, "\x1B[0m");

    va_list args;
    va_start(args, format);
    vprintf(color_format, args);
    va_end(args);
}
// }}}

// custom data types {{{
typedef struct Course {
    int id;
    char* name;
    float interest;
    int max_slots;
    int num_labs;
    int* labs;

    int withdrawn;
    int accepting_students;
    int tut_in_progress;
    int available_slots;
    pthread_mutex_t mutex;
    pthread_cond_t accepting_cond;
    pthread_cond_t tut_in_progress_cond;
    pthread_cond_t available_cond;
} Course;

typedef struct Student {
    int id;
    float calibre;
    int prefs[3];
    int fill_time;

    pthread_mutex_t mutex;
} Student;

typedef struct TA {
    int id;
    int TAships;

    pthread_mutex_t mutex;
} TA;

typedef struct Lab {
    int id;
    char* name;
    int num_TAs;
    int max_TAships;

    TA* TAs;
    pthread_mutex_t mutex;
    pthread_cond_t exhausted_cond;
} Lab;
// }}}

// global variables {{{
int g_num_students, g_num_labs, g_num_courses;
Student* g_students;
Course* g_courses;
Lab* g_labs;
// }}}

// probabilistically determine whether a student finalizes a course {{{
int finalized(struct Course course, struct Student student) {
    double probability = course.interest * student.calibre;
    double choice = (double)rand() / (double)RAND_MAX;
    return (choice <= probability ? 1 : 0);
}
// }}}

// student routine {{{
void* student_routine(void* args) {
    Student* student = (Student*)args;

    // flag to check if student has finalized a course
    int course_finalized = 0;

    // wait till student registers
    sleep(student->fill_time);
    cprintf(ANSI_WHITE, "Student %d has filled in preferences for course registration\n",
            student->id);

    // iterate through preferences
    int prev_course_i = -1;
    for (int i = 0; i < 3; i++) {
        Course* course = &g_courses[student->prefs[i]];

        // check if course has been withdrawn
        pthread_mutex_lock(&course->mutex);
        int withdrawn = course->withdrawn;
        pthread_mutex_unlock(&course->mutex);

        // if not withdrawn {{{
        if (!withdrawn) {
            // announce preference change
            if (prev_course_i > -1) {
                cprintf(ANSI_YELLOW,
                        "Student %d has changed current preference from %s (priority %d) to %s "
                        "(priority %d)\n",
                        student->id, g_courses[student->prefs[prev_course_i]].name,
                        prev_course_i + 1, course->name, i + 1);
            }

            // wait until course starts accepting students
            pthread_mutex_lock(&course->mutex);
            while (!course->accepting_students) {
                pthread_cond_wait(&course->accepting_cond, &course->mutex);
            }
            // move to next preference if course has been removed
            if (course->accepting_students < 0) {
                pthread_mutex_unlock(&course->mutex);
                goto end_course;
            }
            pthread_mutex_unlock(&course->mutex);

            // wait until there are available tutorial slots
            pthread_mutex_lock(&course->mutex);
            while (!course->available_slots) {
                pthread_cond_wait(&course->available_cond, &course->mutex);
            }
            // move to next preference if course has been removed
            if (course->available_slots < 0) {
                pthread_mutex_unlock(&course->mutex);
                goto end_course;
            }
            // continue enrolling in course
            course->available_slots--;
            pthread_mutex_unlock(&course->mutex);
            /* pthread_cond_broadcast(&course->available_cond); */

            // announce allocation
            cprintf(ANSI_PURPLE, "Student %d has been allocated a seat in course %s\n", student->id,
                    course->name);

            // wait until tutorial starts
            pthread_mutex_lock(&course->mutex);
            while (!course->tut_in_progress) {
                pthread_cond_wait(&course->tut_in_progress_cond, &course->mutex);
            }
            pthread_mutex_unlock(&course->mutex);

            // attend tutorial
            pthread_mutex_lock(&course->mutex);
            while (course->tut_in_progress) {
                pthread_cond_wait(&course->tut_in_progress_cond, &course->mutex);
            }
            pthread_mutex_unlock(&course->mutex);

            // post tutorial delay
            sleep(POST_TUTORIAL_DELAY);

            // finalize or withdraw from current course
            if (finalized(*course, *student)) {
                course_finalized = 1;
                cprintf(ANSI_GREEN, "Student %d has selected the course %s permanently\n",
                        student->id, course->name);
                break;
            } else {
                cprintf(ANSI_CYAN, "Student %d has withdrawn from the course %s\n", student->id,
                        course->name);
            }
        }
        // }}}

    end_course:
        pthread_mutex_lock(&course->mutex);
        if (!g_courses[student->prefs[i]].withdrawn) prev_course_i = i;
        pthread_mutex_unlock(&course->mutex);
    }

    // remove student from simulation
    if (!course_finalized) {
        cprintf(ANSI_RED, "Student %d could not get any of their preferred courses\n", student->id);
    }

    return NULL;
}
// }}}

// course routine {{{
void* course_routine(void* args) {
    Course* course = (Course*)args;

    while (1) {
        // check if course has been withdrawn
        pthread_mutex_lock(&course->mutex);
        int withdrawn = course->withdrawn;
        pthread_mutex_unlock(&course->mutex);

        if (!withdrawn) {
            // flag to check if TA has been assigned
            int TA_assigned = 0;

            // iterate over each eligible lab
            for (int i = 0; (i < course->num_labs) && (!TA_assigned); i++) {
                Lab* lab = &g_labs[course->labs[i]];

                // iterate over each TA in lab
                for (int j = 0; (j < lab->num_TAs) && (!TA_assigned); j++) {
                    TA* ta = &lab->TAs[j];

                    // find a TA who isn't engaged in another course already
                    if (pthread_mutex_trylock(&ta->mutex) == 0) {
                        // check if TA has exceeded lab's max TAship limit
                        pthread_mutex_lock(&lab->mutex);
                        int max_TAships = lab->max_TAships;
                        pthread_mutex_unlock(&lab->mutex);

                        if (ta->TAships < max_TAships) {
                            // allocate TA to course
                            TA_assigned = 1;
                            ta->TAships++;

                            // announce TA allocation
                            cprintf(ANSI_GREEN,
                                    "TA %d from lab %s has been allocated to course %s for their "
                                    "%d%s TAship\n",
                                    ta->id, lab->name, course->name, ta->TAships,
                                    (ta->TAships % 10 == 1   ? "st"
                                     : ta->TAships % 10 == 2 ? "nd"
                                     : ta->TAships % 10 == 3 ? "rd"
                                                             : "th"));

                            // allocate seats for tutorial
                            int D = (rand() % course->max_slots) + 1;
                            pthread_mutex_lock(&course->mutex);
                            course->available_slots = D;
                            pthread_mutex_unlock(&course->mutex);
                            pthread_cond_broadcast(&course->available_cond);
                            cprintf(ANSI_CYAN, "Course %s has been allocated %d seats\n",
                                    course->name, D);

                            // start admitting students
                            pthread_mutex_lock(&course->mutex);
                            course->accepting_students = 1;
                            pthread_mutex_unlock(&course->mutex);
                            pthread_cond_broadcast(&course->accepting_cond);

                            // wait for students to enrol into the course
                            sleep(TUTORIAL_WAIT);

                            // stop admitting students
                            pthread_mutex_lock(&course->mutex);
                            course->accepting_students = 0;
                            pthread_mutex_unlock(&course->mutex);
                            pthread_cond_broadcast(&course->accepting_cond);

                            // conduct tutorial
                            pthread_mutex_lock(&course->mutex);
                            int available_slots = course->available_slots;
                            course->tut_in_progress = 1;
                            pthread_mutex_unlock(&course->mutex);
                            pthread_cond_broadcast(&course->tut_in_progress_cond);

                            cprintf(ANSI_PURPLE,
                                    "Tutorial has started for Course %s with %d seats filled out "
                                    "of %d\n",
                                    course->name, (D - available_slots), D);
                            sleep(TUTORIAL_DURATION);

                            // end tutorial
                            pthread_mutex_lock(&course->mutex);
                            course->tut_in_progress = 0;
                            pthread_mutex_unlock(&course->mutex);
                            pthread_cond_broadcast(&course->tut_in_progress_cond);
                            cprintf(
                                ANSI_YELLOW,
                                "TA %d from lab %s has completed the tutorial for the course %s\n",
                                ta->id, lab->name, course->name);
                        }

                        // relieve TA
                        pthread_mutex_unlock(&ta->mutex);
                    }
                }
            }

            // withdraw course if no TA mentors available
            if (!TA_assigned) {
                pthread_mutex_lock(&course->mutex);
                course->withdrawn = 1;
                cprintf(
                    ANSI_RED,
                    "Course %s does not have any TA mentors eligible and is removed from course "
                    "offerings\n",
                    course->name);
                pthread_mutex_unlock(&course->mutex);
                break;
            }
        }
    }

    // never accept students again
    pthread_mutex_lock(&course->mutex);
    course->withdrawn = 1;
    course->available_slots = -1;
    course->accepting_students = -1;
    pthread_mutex_unlock(&course->mutex);
    pthread_cond_broadcast(&course->available_cond);
    pthread_cond_broadcast(&course->accepting_cond);

    return NULL;
}
// }}}

int main() {
    srand(time(NULL));

    // parse input {{{
    scanf("%d %d %d", &g_num_students, &g_num_labs, &g_num_courses);
    g_students = calloc(g_num_students, sizeof(struct Student));
    g_courses = calloc(g_num_courses, sizeof(struct Course));
    g_labs = calloc(g_num_labs, sizeof(struct Lab));

    for (int i = 0; i < g_num_courses; i++) {
        Course course;
        course.id = i;
        course.name = calloc(INPUT_BUFFER_MAX, sizeof(char));
        scanf("%s %f %d %d", course.name, &course.interest, &course.max_slots, &course.num_labs);
        course.labs = calloc(course.num_labs, sizeof(int));
        for (int j = 0; j < course.num_labs; j++) scanf("%d", &course.labs[j]);

        course.withdrawn = 0;
        course.accepting_students = 0;
        course.tut_in_progress = 0;

        pthread_mutex_init(&course.mutex, NULL);
        pthread_cond_init(&course.accepting_cond, NULL);
        pthread_cond_init(&course.tut_in_progress_cond, NULL);
        pthread_cond_init(&course.available_cond, NULL);

        g_courses[i] = course;
    }

    for (int i = 0; i < g_num_students; i++) {
        Student student;
        student.id = i;
        scanf("%f %d %d %d %d", &student.calibre, &student.prefs[0], &student.prefs[1],
              &student.prefs[2], &student.fill_time);

        pthread_mutex_init(&student.mutex, NULL);

        g_students[i] = student;
    }

    for (int i = 0; i < g_num_labs; i++) {
        Lab lab;
        lab.id = i;
        lab.name = calloc(INPUT_BUFFER_MAX, sizeof(char));
        scanf("%s %d %d", lab.name, &lab.num_TAs, &lab.max_TAships);

        lab.TAs = calloc(lab.num_TAs, sizeof(TA));
        for (int j = 0; j < lab.num_TAs; j++) {
            TA ta;
            ta.id = j;
            ta.TAships = 0;

            pthread_mutex_init(&ta.mutex, NULL);

            lab.TAs[j] = ta;
        }

        pthread_mutex_init(&lab.mutex, NULL);
        pthread_cond_init(&lab.exhausted_cond, NULL);

        g_labs[i] = lab;
    }
    // }}}

    // initialize student threads {{{
    pthread_t student_threads[g_num_students];
    for (int i = 0; i < g_num_students; i++) {
        pthread_t* target_thread = &student_threads[i];
        Student* target_student = &g_students[i];
        if (pthread_create(target_thread, NULL, &student_routine, target_student)) {
            perror("Error creating student thread");
        }
    }
    // }}}

    // initialize course threads {{{
    pthread_t course_threads[g_num_courses];
    for (int i = 0; i < g_num_courses; i++) {
        pthread_t* target_thread = &course_threads[i];
        Course* target_course = &g_courses[i];
        if (pthread_create(target_thread, NULL, &course_routine, target_course)) {
            perror("Error creating course thread");
        }
    }
    // }}}

    // join threads {{{
    for (int i = 0; i < g_num_students; i++) {
        if (pthread_join(student_threads[i], NULL)) {
            perror("Error joining student thread");
        }
    }
    // }}}

    printf("\nSimulation complete.\n");

    return 0;
}
