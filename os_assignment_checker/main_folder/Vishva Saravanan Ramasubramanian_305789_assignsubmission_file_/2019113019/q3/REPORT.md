# Q3: Multithreaded Client and Server
> Vishva Saravanan R
> 2019113019

## Configuration
- The server and client target **port 8001**.
- The default number of server workers is **10**.
- The maximum number of clients that can connect to the server at once is **4**.
- The delay in response from a worker thread to the client is **2 seconds**.

## Code Structure & Logic
### Server
The server takes as argument the number of worker threads to maintain in the threadpool.
Upon initialization, the server spawns the specified number of worker threads executing
**Worker Routine**, which conditionally wait on `request_queue`, a global variable that 
maintains the incoming requests to the server. After initializing the worker threads, 
the socket server is initialized, and the welcoming socket is set up for clients to 
connect. The server listens on the address and port as configured in the program, 
and upon accepting a connection request, adds the request along with the client's socket
file descriptor to the `request_queue`.

#### Worker Routine
Each worker thread conditionally waits for the `request_queue` to become non-empty, upon
which a request is extracted from it while locking its mutex. The extracted request is
then parsed, and the corresponding command is executed by **Dictionary Helper** methods.
After execution, the appropriate response along with the ID of the worker thread serving
this request, is sent to the client through its file descriptor. The connection to the client 
is then terminated.

### Dictionary Helper
The server maintains a global dictionary, `DICTDB`, for working with the commands sent by
clients. This dictionary is thread-safe, as all methods that operate on it make use of mutex
locks before accessing values.  
It possesses the following methods:

- `execute_query(command)`: Parses the command string and determines the function to execute,
along with the required parameters. Handles errors for insufficient number of arguments and invalid
commands.

- `q_insert(key, value)`: Creates a new `key` on the server’s dictionary and set its
value as `value`. In case `key` already exists on the server, then an appropriate error 
stating "Key already exists" is displayed.  
If successful, "Insertion successful" is displayed.

- `q_delete(key)`: Removes the `key` from the dictionary. If no key with the name
`key` exists, then an error stating "No such key exists" is displayed.  
If successful, the message "Deletion successful" is displayed.

- `q_update(key, value)`: Updates the value corresponding to `key` on the
server’s dictionary and set its value as `value`. In case `key` does not exist on the 
server, then an appropriate error stating "Key does not exist" is displayed.  
If successful, the updated value of the key is displayed.

- `q_concat(key1, key2)`: Let values corresponding to the keys before execution of this command be `{key1: 
value1, key2: value2}`. Then, the corresponding values after this command's execution are `{key1:
value1+value2, key2: value2+value1}`. If either of the keys do not exist in the dictionary, an error
message "Concat failed as at least one of the keys does not exist" is displayed. Else,
the final value of `key2` is displayed. 

- `q_fetch(key)`: Displays the value corresponding to the key if it exists in the dictionary, and an error
"Key does not exist" otherwise.

### Client
The custom class `Request` is defined to store attributes of every request to be
performed during execution.  
The client takes input of the total number of requests to be performed, and the delay and
commands associated with each of the requests. Each input request is pushed into a list
after being parsed, after which a thread is spawned for each of the requests which 
execute the **Client Routine**. All the client threads are joined to make sure the program 
does not terminate before all the requests are complete.

#### Client Routine
Each client thread takes a request from the list, and sleeps for `delay` seconds, as
specified in the input.
It then connects to the server using the address and port as configured in the program, 
and upon successful connection, writes into the server's file descriptor the command to 
be executed. Upon getting a response from the server, the client writes it to `stdout`, 
closes connection to the server and terminates the thread.

## Assumptions and Arbitrary Constants
- The port 8001 is not in use by any other application.
