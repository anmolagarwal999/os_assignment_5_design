## Building & Running
Server:
```
make server
./server <number of worker threads in pool>
```

Client:
```
make client
./client
```
