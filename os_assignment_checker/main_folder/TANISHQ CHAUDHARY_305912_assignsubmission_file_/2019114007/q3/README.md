# Client Sever

## Using
- `clear && make client && ./client` to start the client
- `clear && make server && ./server 69` starts a server with 69 threads
- `clear && make clean && clear` to clear out the `.o` files

Can use: `clear && make client && ./client < ./tests/client_test.txt` to test the client
