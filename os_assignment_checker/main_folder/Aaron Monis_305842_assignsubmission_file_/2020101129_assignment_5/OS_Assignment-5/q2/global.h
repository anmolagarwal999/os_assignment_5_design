#ifndef GLOBAL_H
#define GLOBAL_H

#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

// #include "course.h"
#include "match.h"
#include "group.h"

void *shareMem(size_t size);
int random_number(int min, int max);
int zone_1, zone_2, zone_3;
pthread_mutex_t *mutex_1; 
pthread_mutex_t *mutex_2;
pthread_mutex_t * mutex_3;
int spectate_time;

#endif