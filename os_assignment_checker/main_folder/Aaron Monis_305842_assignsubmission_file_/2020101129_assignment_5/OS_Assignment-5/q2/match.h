#ifndef MATCH_H
#define MATCH_H

#include <pthread.h>

typedef struct match match;

struct match {
    int number_of_chances;
    int *time;
    double *chance;
    char *HNA;
    int home_score;
    int away_score;
    pthread_mutex_t *mutex;
};

pthread_mutex_t *student_list_lock;
match *m;

void *match_thread_init(match *m);
void match_init(int n);

#endif