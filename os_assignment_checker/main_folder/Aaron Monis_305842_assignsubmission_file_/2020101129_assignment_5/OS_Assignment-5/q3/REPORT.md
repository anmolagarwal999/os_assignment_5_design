# Multithreaded client and server

We use semaphores to handle the resource of worker threads we have at the server end. This is to prevent deadlocks and race-conditions.

`client.cpp`: Handles the client side, makes requests to the server and prints the response it gets.
`server.cpp`: Handles the server side, receives requests to connect, makes the connection and lets a thread handle it. A thread then services the request before giveng the required response to the client. We then close the connection.
`dict.cpp`: Functions to handle the dictionary operations with proper error handling. We use locks to ensure race conditions do not occur.

## The Logic

**Server Side**: The server sets up the required number of worker threads given as a commandline argument. We then create a socket and wait for connections from the client to start distributing requests to the threads. When a connection is made we send the socket file descriptor to a worker thread (if available) who handles the request and makes the update or retrieval from the dictionary. We then get the result of the operation and generate the response to be sent to the client. We send a space-delimited string containing the response string, the boolean result of the operation (0 - fail, 1 - success) and the ID (zero-indexed) of the thread handling the request. We use semaphores to handle the allocation of the worker threads as we consider it to be a limited resource and want to avoid deadlocks. We do not wait on the worker thread so that we can immediately move to handle the next client connection. The worker thread then closes the connection and prints out a message and ends the thread.

**Client Side**: We get the input string containing the command. We extract the time and wait for the time to arrive. When it does we give the command, request number to the client thread process. In it we set up a connection to the server and send string containing the index of the client request and the request itself. We then wait for the response from the server before printing it.
We then wait on all the threads to wait for all requests to complete.