#include "dict.hpp"
#include "utils.hpp"

pthread_mutex_t *dict_lock;

Pair *init_dict()
{
    // Initialise the dict
    Pair *dict = (Pair *)shareMem(101 * sizeof(Pair));
    dict_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    for (int i = 0; i < 101; i++)
    {
        dict[i].key = i;
        dict[i].str = NULL;
        dict[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(dict[i].mutex, NULL);
    }
    pthread_mutex_init(dict_lock, NULL);

    return dict;
}
int dict_insert(Pair *dict, int key, string s)
{
    // Insert into the dict, return a 0 if there is an error
    pthread_mutex_lock(dict[key].mutex);
    if (dict[key].str != NULL)
    {
        pthread_mutex_unlock(dict[key].mutex);
        return 0;
    }
    char *str = (char *)calloc(s.length() + 1, sizeof(char));
    strcpy(str, s.c_str());
    dict[key].str = str;
    pthread_mutex_unlock(dict[key].mutex);
    return 1;
}
int dict_delete(Pair *dict, int key)
{
    pthread_mutex_lock(dict[key].mutex);
    if (dict[key].str == NULL)
    {
        pthread_mutex_unlock(dict[key].mutex);
        return 0;
    }
    free(dict[key].str);
    dict[key].str = NULL;
    pthread_mutex_unlock(dict[key].mutex);
    return 1;
}
char *dict_update(Pair *dict, int key, string s)
{
    pthread_mutex_lock(dict[key].mutex);
    if (dict[key].str == NULL)
    {
        pthread_mutex_unlock(dict[key].mutex);
        return NULL;
    }
    free(dict[key].str);
    char *str = (char *)calloc(s.length() + 1, sizeof(char));
    strcpy(str, s.c_str());
    dict[key].str = str;
    pthread_mutex_unlock(dict[key].mutex);
    return str;
}
char *dict_concat(Pair *dict, int key1, int key2)
{
    pthread_mutex_lock(dict[key1].mutex);
    pthread_mutex_lock(dict[key2].mutex);
    if (dict[key1].str == NULL || dict[key2].str == NULL)
    {
        pthread_mutex_unlock(dict[key1].mutex);
        pthread_mutex_unlock(dict[key2].mutex);
        return NULL;
    }
    int n1 = strlen(dict[key1].str);
    int n2 = strlen(dict[key2].str);
    char *str1 = (char *)calloc(n1 + n2 + 1, sizeof(char));
    char *str2 = (char *)calloc(n1 + n2 + 1, sizeof(char));
    strcpy(str1, dict[key1].str);
    strcat(str1, dict[key2].str);
    strcpy(str2, dict[key2].str);
    strcat(str2, dict[key1].str);
    free(dict[key1].str);
    free(dict[key2].str);
    dict[key1].str = str1;
    dict[key2].str = str2;
    pthread_mutex_unlock(dict[key1].mutex);
    pthread_mutex_unlock(dict[key2].mutex);
    return dict[key2].str;
}
char *dict_fetch(Pair *dict, int key)
{
    pthread_mutex_lock(dict[key].mutex);
    if (dict[key].str == NULL)
    {
        pthread_mutex_unlock(dict[key].mutex);
        return NULL;
    }
    pthread_mutex_unlock(dict[key].mutex);
    return dict[key].str;
}