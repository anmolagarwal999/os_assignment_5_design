#include "utils.hpp"

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
vector<string> parse_string(string s)
{
    string delim = " ";
    vector<string> words{};

    size_t pos = 0;
    while ((pos = s.find(delim)) != string::npos) {
        words.push_back(s.substr(0, pos));
        s.erase(0, pos + delim.length());
    }
    if (!s.empty())
        words.push_back(s.substr(0, pos));

    return words;
}