#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
#include "utils.hpp"
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
#define endl "\n"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8081
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
typedef struct
{
    string str;
    int index;
} args_struct;

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    // Read bytes from socket and resize the string read accordingly
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);
    // Return the number of bytes and the string itself
    return {output, bytes_received};
}

int send_string_on_socket(int fd, string &s)
{
    // Send data on socket
    char *str = (char *)calloc(buff_sz, 1);
    strcpy(str, s.c_str());
    int bytes_sent = write(fd, str, buff_sz);
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        free(str);
        // return "
        exit(-1);
    }
    free(str);
    return bytes_sent;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); // convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    // https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    // part;
    //  printf(BGRN "Connected to server\n" ANSI_RESET);
    //  part;
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *arg)
{
    // First get the args - the command and the index
    args_struct a = *(args_struct *)arg;
    string s = a.str;
    int cmd_index = a.index;
    // Set up the socket
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);

    // printf(BCYN "Connection to server successful" ANSI_RESET "\n");
    // printf("%d\n", cmd_index);
    size_t pos = 0;
    string delim = " ";
    string index;
    index = to_string(cmd_index);
    string s1 = index + " " + s;// Prepare string to send
    // printf("%s\n", s1.c_str());
    send_string_on_socket(socket_fd, s1);
    int num_bytes_read;
    string output_msg;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    int result;
    LL id;
    // Get the id, result and the output from the server-sent string
    if ((pos = output_msg.find(delim)) != string::npos)
    {
        id = stoll(output_msg.substr(0, pos));
        output_msg.erase(0, pos + delim.length());
    }
    if ((pos = output_msg.find(delim)) != string::npos)
    {
        result = stoi(output_msg.substr(0, pos));
        output_msg.erase(0, pos + delim.length());
    }
    if ((pos = output_msg.find("\n")) != string::npos)
    {
        output_msg.erase(pos);
    }
    if (result)
    {
        printf(BYEL "%d" BGRN ":" BCYN "%lld" BGRN ":%s" ANSI_RESET "\n", cmd_index, id, output_msg.c_str());
    }
    else
    {
        printf(BYEL "%d" BMAG ":" BCYN "%lld" BMAG ":%s" ANSI_RESET "\n", cmd_index, id, output_msg.c_str());
    }
    // send_string_on_socket(socket_fd, "Ack");
    // part;
}

int main(int argc, char *argv[])
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    int i, j, k, t, n;
    string num;
    getline(cin, num);
    n = stoi(num);
    vector<string> list(n);
    for (int i = 0; i < n; i++)
    {
        getline(cin, list[i]);
    }
    pthread_t *client_requests = (pthread_t *)shareMem(sizeof(pthread_t) * n); // setup the client requests
    t = 1;
    string delim = " ";
    int request_time;
    for (int i = 0; i < n; i++)
    {
        size_t pos = 0;
        if ((pos = list[i].find(delim)) != string::npos) 
        {
            request_time = stoi(list[i].substr(0, pos));
            list[i].erase(0, pos + delim.length());
        }
        else
        {
            printf(BRED "Input incorrect\n" ANSI_RESET);
            exit(-1);
        }
        while (t != request_time)
        {
            sleep(1);
            t++;
        }
        // Make the request when the time it was made arrives
        args_struct *client_args = (args_struct *)calloc(1, sizeof(args_struct));
        client_args->str = list[i];
        client_args->index = i;
        pthread_create(&client_requests[i], NULL, begin_process, (void *)client_args);
        // printf("%s\n", list[i].c_str());
    }
    for (int i = 0; i < n; i++) // Wait for all requests to finish
    {
        pthread_join(client_requests[i], NULL);
    }
    return 0;
}