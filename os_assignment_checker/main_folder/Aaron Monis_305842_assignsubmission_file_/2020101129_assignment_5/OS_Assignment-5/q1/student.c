#include "global.h"

void student_list_init(int n)
{
    // Initialize students details with the input and initialize locks
    student_list = (student *)shareMem(n * sizeof(student));
    float calibre;
    int pref1, pref2, pref3;
    int time_to_fill = 0;
    for (int i = 0; i < n; i++)
    {
        student_list[i].id = i;
        scanf("%f %d %d %d %d", &calibre, &pref1, &pref2, &pref3, &time_to_fill);
        student_list[i].calibre = calibre;
        student_list[i].pref_1 = pref1;
        student_list[i].pref_2 = pref2;
        student_list[i].pref_3 = pref3;
        student_list[i].time_to_fill = time_to_fill;
        student_list[i].tut_attendance = -1;
        student_list[i].permanent_selection = -1;
        student_list[i].curr_preference = -1;
        student_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].cond = (pthread_cond_t *)shareMem(sizeof(pthread_cond_t));
        student_list[i].tut_done = 0;
        pthread_mutex_init(student_list[i].mutex, NULL);
        pthread_mutex_init(student_list[i].lock, NULL);
        pthread_cond_init(student_list[i].cond, NULL);
    }
    student_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(student_list_lock, NULL);
}
void allot_preferences(student *s)
{
    // If there is no initial preference we simply give the first choice as the current one
    // Change to the next one is this function is called
    // Show that the student withdrwas the course and switches or leaves the list if the choices are done
    if (s->curr_preference == -1)
    {
        s->curr_preference = s->pref_1;
    }
    else if (s->curr_preference == s->pref_1)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_2;
        printf(BMAG "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_RESET, s->id, course_list[s->pref_1].name, course_list[s->pref_2].name);
    }
    else if (s->curr_preference == s->pref_2)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_3;
        printf(BMAG "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_RESET, s->id, course_list[s->pref_2].name, course_list[s->pref_3].name);
    }
    else
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        printf(BBLK "Student %d couldn’t get any of his preferred courses\n" ANSI_RESET, s->id);
        s->id = -1;
    }
    if (s->id != -1 && course_list[s->curr_preference].id == -1) // If the student is still active but the course he chooses isnt we call the funtion again to either switch or leave
    {
        allot_preferences(s);
    }
}
void student_init(student *s)
{
    //printf("Student %d\n", s->id);
    sleep(s->time_to_fill);
    printf(BGRN "Student %d has filled in preferences for course registration\n" ANSI_RESET, s->id);
    for(int i = 0; i < 4; i++)
    {
        // Allot the student preferences to change the students choice or make him choose
        pthread_mutex_lock(s->mutex);
        allot_preferences(s);
        pthread_mutex_unlock(s->mutex);
        if(s->id == -1)// Leave if the student is invalid
        {
            return;
        }
        pthread_mutex_lock(s->lock);
        while (s->tut_done == 0)
        {
            pthread_cond_wait(s->cond, s->lock);// Wait for the tut to finish or the course to be removed
        }
        pthread_mutex_unlock(s->lock);
        s->tut_done = 0;
        pthread_cond_init(s->cond, NULL);
        if(course_list[s->curr_preference].id == -1)// Continue if the course was invalid
        {
            pthread_mutex_unlock(s->mutex);
            continue;
        }
        // MAke the random choice based on the possibility that we can calculate
        float prob_permanent = s->calibre * course_list[s->curr_preference].interest * 100;
        int random_number = rand() % 100 + 1;
        if (prob_permanent >= (float)random_number)
        {
            printf(BGRN "Student %d has selected the course %s permanently\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
            s->id = -1;
            pthread_mutex_unlock(s->mutex);
            return;
        }
        pthread_mutex_unlock(s->mutex);
    }
}