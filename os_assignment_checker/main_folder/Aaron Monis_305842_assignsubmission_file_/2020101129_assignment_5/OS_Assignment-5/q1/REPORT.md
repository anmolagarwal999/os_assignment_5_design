# An Alternate Course Allocation Portal

We make the use of locks to avoid deadlocking and race conditions. We also use a condition variable to signal if a tutorial is over. This variable helps avoid busy waiting.

`course.c` simulates the functioning of the course explained below.
`labs.c` initializes the lab resource.
`student.c` simulates the student. We simulate the student filling the form attending tutorials and changing preferences. We also simulate making choices after the tutorial and leaving the simulator if no choice was taken or if a course has been chosen permanently.

## My Logic

My logic behind this code is to simulate each course and student independently. I assume the labs to be a resouce instead of simulating it. I start the simulator for each course. In each tutorial I first allocate slots to it. Then I search for TAs to take the course. If there are no TAs to take it I simply wait for one. If all TAs I can use have been exhausted I remove the course from the sim.
If the TA was alloted to the course I then move on to alloting students to take the tut. I choose only those who have the current preference as the course and flag them as attending the tutorial for the course. I wait if no students can be allocated at that time. If all students are done I simply exit the course thread indicating the end of the sim.
To remove a course I need to indicate that the students are done with the tutorial (even if it hasnt happened) to trigger a change in the preferences.
Once the students have been alloted to the course I start the tutorial (a few seconds). After that I deallocate the TA making him available if required. To deallocate students I signal them as having being done with the tut. I then start a new tutorial.

In the students thread I first allot the preferences (first preference) of the student. After that I wait for a signal to indicate the tutorial is done. Once the signal is received we must first check if the course has been removed. If it has we simply allot the next preference as the current preference.
If the tutorial did occur for the course then we randomly decide if the student takes the course or not. If he does we remove him from the simulator (exit the thread). If he doesnt we allocate new preferences to the student. While alloting preferences we have to check if the course selected as the current preference is invalid or not. If it is we have to reallot the preferences once again.
We keep changing preferences, waiting for the tut, choosing the course permanently or not until the student is out of the simulator or he has exhausted his preferences. If he has we can simply remove him.
