#ifndef LABS_H
#define LABS_H

typedef struct
{
    int id;
    char name[100];
    int student_TA_number;
    int *TA_available;
    int *TA_allotments;
    int number_of_TAships;
    pthread_mutex_t *mutex;
} Labs;

Labs *labs_list;
pthread_mutex_t *labs_list_lock;

void labs_list_init(int n);
void labs_init(Labs *l);

#endif