#ifndef COURSE_H
#define COURSE_H

typedef struct
{
    int id;
    char name[100];
    float interest;
    int maximum_slots;
    int number_of_labs;
    int *labs_accepted;
    int TA_index;
    pthread_mutex_t *mutex;
} course;

course *course_list;
pthread_mutex_t *course_list_lock;

void course_list_init(int n);
int allot_TAs(course *c);
void dealloc_TA(course *c, int lab_index, int TA_index);
int dealloc_students(course *c, int max_slots);
int allot_students(course *c, int max_slots);
void course_init(course *c);

#endif