#ifndef STUDENT_H
#define STUDENT_H

typedef struct
{
    int id;
    float calibre;
    int pref_1;
    int pref_2;
    int pref_3;
    int permanent_selection;
    int curr_preference;
    int tut_attendance;
    int time_to_fill;
    pthread_mutex_t *mutex;
    pthread_mutex_t *lock;
    pthread_cond_t *cond;
    int tut_done;
} student;

student *student_list;
pthread_mutex_t *student_list_lock;

void student_list_init(int n);
void student_init(student *s);
void allot_preferences(student *s);

#endif