#include "global.h"

extern int num_students, num_courses, num_labs;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course_list_init(num_courses);
    student_list_init(num_students);
    labs_list_init(num_labs);
    pthread_t *student_threads = (pthread_t *)shareMem(num_students * sizeof(pthread_t));
    for (int i = 0; i < num_students; i++)
    {
        pthread_create(&student_threads[i], NULL, (void *)student_init, &student_list[i]);
    }
    pthread_t *course_threads = (pthread_t *)shareMem(num_courses * sizeof(pthread_t));
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&course_threads[i], NULL, (void *)course_init, &course_list[i]);
    }
    pthread_t *labs_threads = (pthread_t *)shareMem(num_labs * sizeof(pthread_t));
    for (int i = 0; i < num_labs; i++)
    {
        pthread_create(&labs_threads[i], NULL, (void *)labs_init, &labs_list[i]);
    }
    for (int i = 0; i < num_students; i++) {
        pthread_join(student_threads[i], NULL);
    }
    for (int i = 0; i < num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }
    for (int i = 0; i < num_labs; i++) {
        pthread_join(labs_threads[i], NULL);
    }
}