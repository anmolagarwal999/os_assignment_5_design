#include "global.h"

void labs_list_init(int n)
{
    // Setup the labs information with the input
    labs_list = (Labs *)shareMem(n * sizeof(Labs));
    for (int i = 0; i < n; i++)
    {
        labs_list[i].id = i;
        scanf("%s %d %d", labs_list[i].name, &labs_list[i].student_TA_number, &labs_list[i].number_of_TAships);
        labs_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(labs_list[i].mutex, NULL);
        labs_list[i].TA_available = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_available[j] = 1;
        }
        labs_list[i].TA_allotments = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_allotments[j] = 0;
        }
    }
    labs_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(labs_list->mutex, NULL);
}
void labs_init(Labs *l)
{
    ; // Do nothing here
}