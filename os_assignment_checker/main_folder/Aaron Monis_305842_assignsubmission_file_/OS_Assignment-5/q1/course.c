#include "global.h"

void course_list_init(int n)
{
    // We initialize the course struct here, we initialize the values of a course with the input and also initialize the locks
    course_list = (course *)shareMem(n * sizeof(course));
    for (int i = 0; i < n; i++)
    {
        course_list[i].id = i;
        // Get course details
        scanf("%s %f %d %d", course_list[i].name, &course_list[i].interest, &course_list[i].maximum_slots, &course_list[i].number_of_labs);
        course_list[i].labs_accepted = (int *)shareMem(course_list[i].number_of_labs * sizeof(int));
        for (int j = 0; j < course_list[i].number_of_labs; j++)
        {
            // Get the labs from which the course can take TAs
            scanf("%d", &course_list[i].labs_accepted[j]);
        }
        course_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(course_list[i].mutex, NULL);
    }
    course_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(course_list_lock, NULL);
}
int allot_TAs(course *c)
{
    bool remove_course = true;
    for (int i = 0; i < c->number_of_labs; i++)
    {
        // We first iterate over all the labs from which TAs can be chosen
        pthread_mutex_lock(labs_list[c->labs_accepted[i]].mutex);
        if (labs_list[c->labs_accepted[i]].id == -1)
        {
            pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
            continue;
        }
        bool lab_no_slots = true;
        for (int j = 0; j < labs_list[c->labs_accepted[i]].student_TA_number; j++)
        {
            if (labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                remove_course = false;
                lab_no_slots = false;
            }
            // Choose a TA if the TA is not exhausted and he is available
            if (labs_list[c->labs_accepted[i]].TA_available[j] == 1 && labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                // Update TA stats and print details
                labs_list[c->labs_accepted[i]].TA_available[j] = 0;
                labs_list[c->labs_accepted[i]].TA_allotments[j]++;
                c->TA_index = j;
                printf(BYEL "TA %d from lab %s allocated to course %s for his %d" ANSI_RESET, j, labs_list[c->labs_accepted[i]].name, c->name, labs_list[c->labs_accepted[i]].TA_allotments[j]);
                switch (labs_list[c->labs_accepted[i]].TA_allotments[j] % 10)
                {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                }
                printf(BYEL " TAship\n" ANSI_RESET);
                pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
                // Return the lab from which the TA is alloted
                return labs_list[c->labs_accepted[i]].id;
            }
        }
        // If there are no slots in the Lab it indicates that all the TAs are exhausted so it is no longer possible to use this lab
        // Indicate this lab is invalid and print an error
        if (lab_no_slots)
        {
            labs_list[c->labs_accepted[i]].id = -1;
            printf(BBLK "Lab %s no longer has students available for TA ship\n" ANSI_RESET, labs_list[c->labs_accepted[i]].name);
        }
        pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
    }
    // If nolabs were found to be available at all, it implies that we can no longer offer this course
    // return the correpsonding status
    if (remove_course)
    {
        return -2;
    }
    // Indicate no TA was found but is still there
    return -1;
}
void dealloc_TA(course *c, int lab_index, int TA_index)
{
    // Make a TA available again
    pthread_mutex_lock(labs_list[lab_index].mutex);
    labs_list[lab_index].TA_available[TA_index] = 1;
    pthread_mutex_unlock(labs_list[lab_index].mutex);
    return;
}
int dealloc_students(course *c, int max_slots)
{
    pthread_mutex_lock(student_list_lock);
    for (int i = 0; i < num_students; i++)
    {
        if (student_list[i].id == -1)
        {
            continue;
        }
        if (student_list[i].tut_attendance == c->id) // If the student is attending the current course remove himfrom the tut and signal that the tut is done
        {
            pthread_mutex_lock(student_list[i].lock);
            pthread_mutex_lock(student_list[i].mutex);
            student_list[i].tut_attendance = -1;
            student_list[i].curr_preference = c->id;
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            student_list[i].tut_done = 1;
            // printf("Tut Done\n");
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            pthread_cond_signal(student_list[i].cond);
            pthread_mutex_unlock(student_list[i].lock);
            pthread_mutex_unlock(student_list[i].mutex);
        }
    }
    pthread_mutex_unlock(student_list_lock);
}
int allot_students(course *c, int max_slots)
{
    int t = max_slots; // max slots that can be given
    while (t == max_slots) // Keep repeating is no slots are filled
    {
        bool all_done = true;
        for (int i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(student_list[i].mutex);
            if (student_list[i].id == -1) // If student is off the list skip
            {
                pthread_mutex_unlock(student_list[i].mutex);
                continue;
            }
            else
            {
                all_done = false;
            }
            if (t == 0)
            {
                pthread_mutex_unlock(student_list[i].mutex); // break once the slots are filled
                return t;
            }
            if (student_list[i].curr_preference == c->id) // Change the students preference to -1 and inidcate he is in a tut.
            {
                student_list[i].tut_attendance = c->id;
                student_list[i].curr_preference = -1;
                t--;
            }
            pthread_mutex_unlock(student_list[i].mutex);
        }
        if (all_done) // Exit the thread if no students remain
        {
            pthread_exit(0);
        }
    }
    return t;
}
void remove_course(course *c)
{
    // To remove a course we go over the students list and remove those whose current preference is the course
    // We then signal that the tut is done even if it wasnt to start the student thread again
    for (int i = 0; i < num_students; i++)
    {
        pthread_mutex_lock(student_list[i].mutex);
        if (student_list[i].curr_preference != c->id || student_list[i].id == -1)
        {
            pthread_mutex_unlock(student_list[i].mutex);
            continue;
        }
        pthread_mutex_lock(student_list[i].lock);
        student_list[i].tut_done = 1;
        pthread_cond_signal(student_list[i].cond);
        pthread_mutex_unlock(student_list[i].lock);
        pthread_mutex_unlock(student_list[i].mutex);
    }
}
void course_init(course *c)
{
    while (true)
    {
        // At every point the course prepares to start a new tut, we choose a random number for the number of slots the tut can have.
        int d = random_number(1, c->maximum_slots);
        printf(BGRN "Course %s has been allocated %d seats\n" ANSI_RESET, c->name, d);
        bool all_done = true;
        // We then allocate TAs to the course by choosing from the list of labs available to it.
        int result = allot_TAs(c);
        while (result < 0)
        {
            // If the return value is -2, it indicates the course can no longer be offered so we declare it invalid and print an error message
            if (result == -2)
            {
                printf(BRED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, c->name);
                remove_course(c);
                c->id = -1;
                return;
            }
            else if (result == -1)
            {
                // TAs have not been allocated but can be, so repeat the process.
                result = allot_TAs(c);
            }
        }
        if (result >= 0)
        {
            int slots_left;
            // We now allot students
            slots_left = allot_students(c, d);
            printf(BCYN "Tutorial has started for course %s with %d slots filled out of %d\n" ANSI_RESET, c->name, d - slots_left, d);
            for (int i = 0; i < 5; i++)
            {
                sleep(1);
            }
            printf(BMAG "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_RESET, c->TA_index, labs_list[result].name, c->name);
            fflush(stdout);
            // Deallocate TAs and then students
            dealloc_TA(c, result, c->TA_index);
            dealloc_students(c, d);
        }
    }
}