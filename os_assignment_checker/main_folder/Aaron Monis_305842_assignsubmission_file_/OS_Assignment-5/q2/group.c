#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void groups_init(int n)
{
    // Initialize the groups
    groups_list = (group *)malloc(n * sizeof(group));
    for (int i = 0; i < n; i++)
    {
        groups_list[i].id = i + 1;
        scanf("%d", &groups_list[i].num_members);
        groups_list[i].members_list = (person *)malloc(groups_list[i].num_members * sizeof(person));
        char str[1000];
        for (int j = 0; j < groups_list[i].num_members; j++)
        {
            // Take the input for all groups concerining their inforamtion
            // Also get the information about each member of the group
            scanf("%s %c %d %d %d", groups_list[i].members_list[j].name, &groups_list[i].members_list[j].HNA, &groups_list[i].members_list[j].time, &groups_list[i].members_list[j].patience, &groups_list[i].members_list[j].goals);
            groups_list[i].members_list[j].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
            pthread_mutex_init(groups_list[i].members_list[j].mutex, NULL);
        }
        groups_list[i].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(groups_list[i].mutex, NULL);
    }
}
void *group_thread_init(group *g)
{
    // Start a thread for each member and simulate it completely
    pthread_t *member_threads = (pthread_t *)malloc(sizeof(pthread_mutex_t) * g->num_members);
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_create(&member_threads[i], NULL, person_init, &g->members_list[i]);
    }
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_join(member_threads[i], NULL);
    }
    printf(BGRN "Group %d is leaving for dinner\n" ANSI_RESET, g->id); // Once we have finished all threads we can make the group leave all at once for dinner

    return NULL;
}
void *person_init(person *p)
{
    int t = 0;
    while (p->time > 0) // Wait for arrival at the stadium
    {
        sleep(1);
        p->time--;
        t++;
    }
    printf(BGRN "%s has reached the stadium\n" ANSI_RESET, p->name); // Arrived
    // Wait for a seat to be alloted and exhaust the patience as well
    // If the patience is exhaused go to the exit (gate)
    // Search for a seat based on preference
    if (p->HNA == 'H')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else if (p->HNA == 'N')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            pthread_mutex_lock(mutex_2);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_2 > 0)
            {
                zone_2--;
                p->seating = 2;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_2);
            if (zone_2 > 0)
            {
                zone_2--;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                p->seating = 2;
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    int watch_time = spectate_time;
    // Spectate the match and leave after it
    while (watch_time--)
    {
        // If the team plays terribly leave
        if ((p->HNA == 'H' && m->away_score >= p->goals) || (p->HNA == 'A' && m->home_score >= p->goals))
        {
            printf(BBLK "%s is leaving due to the bad defensive performance of his team\n" ANSI_RESET, p->name);
            goto exit;
        }
        sleep(1);
        t++;
    }
    // display that the person has left after watching for some time
    printf(BBLU "%s watched the match for %d seconds and is leaving\n" ANSI_RESET, p->name, spectate_time);
    if (p->seating == 1) // Indicate that the seat where the person was is now available
    {
        zone_1++;
    }
    else if (p->seating == 2)
    {
        zone_2++;
    }
    else
    {
        zone_3++;
    }
exit:;
    sleep(1);
    t++;
    printf(BCYN "%s is waiting for their friends at the exit\n" ANSI_RESET, p->name); // Wait at the exit for friends

    return NULL;
}