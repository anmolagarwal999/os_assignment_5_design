#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void match_init(int n)
{
    // Init match details
    m = (match *)malloc(sizeof(match));
    m->number_of_chances = n;
    m->time = (int *)malloc(sizeof(int) * n);
    m->chance = (double *)malloc(sizeof(double) * n);
    m->HNA = (char *)malloc(sizeof(char) * n);
    for(int i=0;i<n;i++)
    {
        scanf(" %c %d %lf", &m->HNA[i], &m->time[i], &m->chance[i]);
    }
    m->home_score = 0;
    m->away_score = 0;
}
void *match_thread_init(match *m)
{
    int t = 0;
    int i = 0;
    // Go over all the chances that occured
    while (i < m->number_of_chances)
    {
        while (t < m->time[i]) // Wait for the chance to occur
        {
            t++;
            sleep(1);
        }
        // Make a random choice to indicate if the goal was scored
        int n = random_number(1, 100);
        int chance = m->chance[i] * 100;
        if (n <= chance)
        {
            int s;
            // Increase the correspnding score if the goal was scored
            if (m->HNA[i] == 'H')
            {
                m->home_score += 1;
                s = m->home_score;
            }
            else
            {
                m->away_score += 1;
                s = m->away_score;
            }
            printf(BYEL "Team %c have scored their %d" ANSI_RESET, m->HNA[i], s);
            switch (s)
            {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
            }
            printf(BYEL " goal\n" ANSI_RESET);
        }
        else
        {
            // display a missed chance
            int s;
            if (m->HNA[i] == 'H')
            {
                s = m->home_score;
            }
            else
            {
                s = m->away_score;
            }
            printf(BCYN "Team %c missed the chance to score their %d" ANSI_RESET, m->HNA[i], s + 1);
            switch (s + 1)
            {
                case 0:
                    printf(BCYN "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BCYN "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BCYN "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BCYN "rd" ANSI_RESET);
                    break;
                default:
                    printf(BCYN "th" ANSI_RESET);
                    break;
            }
            printf(BCYN " goal\n" ANSI_RESET);
        }
        i++;
    }
}