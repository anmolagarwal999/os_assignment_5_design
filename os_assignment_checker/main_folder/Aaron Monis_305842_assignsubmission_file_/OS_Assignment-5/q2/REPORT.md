# The Clasico Experience

We use locks to avoid race conditions while dealing with the zone availability.

`group.c`: Handle the group. Make threads for each person and simulate all stages of the match for them. We then wait for all members of the group to get to the exit before leaving.
`match.c`: Handles the match. Goes over the chances, makes choices to upadate the score.

## My Logic

We simulate the match and the groups of people independently. We run the match on a thread and at the time the goal-scoring chances occur we make a random choice to decide whether the goal occurs or not. If we decide it does, then the team for whom the chance occured gets their score incremented by 1, if not we continue after printing the required message.

Most of the work is done on the groups side. We run the group on a thread and then run the people in that group in seperate threads. We then simulate the person arriving at the stadium. When he does we wait to assign him a slot at his prefferred zone. If he exhausts his patience waiting we send him to the exit and make him wait at the gate for his friends. If he does get a seat we send him to the stadium. We then check the score of the persons preffered team. If they are playing poorly we make him leave and send him to the exit. Once he exhausts his spectate time we send him to the exit. At the exit we wait for the friends in the group so that they can leave together (**bonus question**).

When we create threads for each person in the group thread, we join those threads so that we can wait for them to run to completion. Once they do we can infer that the group is done and remove them (send them to dinner).

Here the match and groups are simulated on threads. We also simulate the persons in the groups. However, the zone is considered a resource.