#include "global.h"

extern int zone_1, zone_2, zone_3;
extern int spectate_time;
extern pthread_mutex_t *mutex_1;
extern pthread_mutex_t *mutex_2;
extern pthread_mutex_t *mutex_3;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &zone_1, &zone_2, &zone_3);
    scanf("%d", &spectate_time);
    int number_of_groups;
    scanf("%d", &number_of_groups);
    groups_init(number_of_groups);
    int number_of_chances;
    scanf("%d", &number_of_chances);
    match_init(number_of_chances);
    mutex_1 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_2 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_3 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(mutex_1, NULL);
    pthread_mutex_init(mutex_2, NULL);
    pthread_mutex_init(mutex_3, NULL);
    pthread_t *groups = (pthread_t *)malloc(number_of_groups * sizeof(pthread_t));
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_create(&groups[i], NULL, group_thread_init, (void *)&groups_list[i]);
    }
    pthread_t *matchup = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create(matchup, NULL, match_thread_init, (void *)m);
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_join(groups[i], NULL);
    }
    pthread_join(*matchup, NULL);
    // Wait for all the group threads and the match to finish
}