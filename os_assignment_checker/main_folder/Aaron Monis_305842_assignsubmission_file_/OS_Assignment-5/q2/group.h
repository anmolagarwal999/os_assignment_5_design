#ifndef GROUP_H
#define GROUP_H

#include <pthread.h>

typedef struct person person;
typedef struct group group;

struct person {
    char name[100];
    char HNA;
    int time;
    int patience;
    int goals;
    pthread_mutex_t *mutex;
    int seating;
};

struct group {
    int id;
    int num_members;
    person *members_list;
    pthread_mutex_t *mutex;
};

group *groups_list;
pthread_mutex_t *student_list_lock;

void *group_thread_init(group *g);
void groups_init(int n);
void *person_init(person *p);

#endif