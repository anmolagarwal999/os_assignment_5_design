#ifndef DICT_H
#define DICT_H

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

typedef struct Pair Pair;

struct Pair {
    int key;
    char *str;
    pthread_mutex_t *mutex;
};
extern pthread_mutex_t *dict_lock;

Pair *init_dict();
int dict_insert(Pair *dict, int key, string s);
int dict_delete(Pair *dict, int key);
char *dict_update(Pair *dict, int key, string s);
char *dict_concat(Pair *dict, int key1, int key2);
char *dict_fetch(Pair *dict, int key);

#endif