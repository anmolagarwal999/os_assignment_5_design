#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include "utils.hpp"
#include "dict.hpp"
#include <pthread.h>
#include <semaphore.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define SIZE 1048576
#define PORT_ARG 8081
#define MAX_CLIENTS 20

pthread_t *worker_threads;
int *workers_active;
sem_t *workers_available;
Pair *dict;
int number_of_workers;

typedef struct {
    int client_socket;
    string cmd;
    LL thread_id;
    int index;
} request_data;

int send_string_on_socket(int fd, const string &s, int result, int thread);
pair<string, int> read_string_from_socket(const int &fd, int bytes);

void *handle_request(void *args)
{
    // Get the cmd and client_socket from the args
    request_data *args_list = (request_data *)args;
    int client_fd = args_list->client_socket;
    vector<string> cmd_args = parse_string(args_list->cmd);
    string s;
    // Get the command sent and work on it
    if(cmd_args[0] == "insert")
    {
        int key1 = stoi(cmd_args[1]);
        int result;
        result = dict_insert(dict, key1, cmd_args[2]);
        if(result)
        {
            s = "Insertion successful\n";
            send_string_on_socket(client_fd, s, 1, args_list->thread_id);
        }
        else
        {
            s = "Key already exists\n";
            send_string_on_socket(client_fd, s, 0, args_list->thread_id);
        }
    }
    else if(cmd_args[0] == "delete")
    {
        int key = stoi(cmd_args[1]);
        int result;
        result = dict_delete(dict, key);
        if(result)
        {
            s = "Deletion successful\n";
            send_string_on_socket(client_fd, s, 1, args_list->thread_id);
        }
        else
        {
            s = "No such key exists\n";
            send_string_on_socket(client_fd, s, 0, args_list->thread_id);
        }
    }
    else if(cmd_args[0] == "update")
    {
        int key = stoi(cmd_args[1]);
        char *result;
        result = dict_update(dict, key, cmd_args[2]);
        if(result == NULL)
        {
            s = "Key does not exist\n";
            send_string_on_socket(client_fd, s, 0, args_list->thread_id);
        }
        else
        {
            s = result;
            send_string_on_socket(client_fd, s, 1, args_list->thread_id);
        }
    }
    else if(cmd_args[0] == "concat")
    {
        int key1 = stoi(cmd_args[1]);
        int key2 = stoi(cmd_args[2]);
        char *result;
        result = dict_concat(dict, key1, key2);
        if(result == NULL)
        {
            s = "Concat failed as at least one of the keys does not exist\n";
            send_string_on_socket(client_fd, s, 0, args_list->thread_id);
        }
        else
        {
            s = result;
            send_string_on_socket(client_fd, s, 1, args_list->thread_id);
        }
    }
    else if(cmd_args[0] == "fetch")
    {
        int key = stoi(cmd_args[1]);
        char *result;
        result = dict_fetch(dict, key);
        if(result == NULL)
        {
            s = "Key does not exist\n";
            send_string_on_socket(client_fd, s, 0, args_list->thread_id);
        }
        else
        {
            s = result;
            send_string_on_socket(client_fd, s, 1, args_list->thread_id);
        }
    }
    // string ack;
    // int ack_num;
    // tie(ack, ack_num) = read_string_from_socket(client_fd, SIZE);
    // if(ack != "Ack")
    // {
    //     printf(BRED "No acknowledgement\n" ANSI_RESET);
    //     exit(-1);
    // }
    sleep(2); // Wait for 2 secs
    close(client_fd); // Disconnect from the client
    printf(BRED "Disconnected from client %d" ANSI_RESET "\n", args_list->index);
    workers_active[args_list->thread_id] = 0;
    sem_post(workers_available); // Indicate that a worker is available
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    // Read a string from the socket
    std::string output;
    output.resize(bytes);
    int i = 0;
    while(read(fd, &output[i], 1) == 1)
    {
        // printf("%c\n", output[i]);
        i++;
        if(i == bytes || output[i - 1] == '\0' || output[i - 1] == '\n')
        {
            output[i - 1] = '\0';
            break;
        }
    }
    // printf("Bytes received: %d\n", i);
    // printf("%s\n", output.c_str());
    if (i <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[i] = 0;
    output.resize(i);
    // debug(output);
    return {output, i};
}

int send_string_on_socket(int fd, const string &s, int result, int thread)
{
    // debug(s.length());
    char str[10];
    sprintf(str, "%d", result);
    char str1[10];
    sprintf(str1, "%d", thread);
    string s2 = str1;
    string s3 = str;
    string s1 = s2 + " " + s3 + " " + s;
    int bytes_sent = write(fd, s1.c_str(), s1.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################
    // Create a connection and let the worker thread handle it

    int received_num, sent_num;
    int cmd_num;
    string cmd;
    string msg_to_send_back;
    int sent_to_client;

    /* read message from client */
    int ret_val = 1;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, SIZE);
    if (received_num <= 0)
    {
        printf(BRED "Server could not read msg sent from client\n" ANSI_RESET);
        close(client_socket_fd);
        return;
    }
    ssize_t pos = 0;
    string delim = " ";
    int index;
    if ((pos = cmd.find(delim)) != string::npos)
    {
        index = stoi(cmd.substr(0, pos)); // Get the indiex of command sent by client
        cmd.erase(0, pos + delim.length());
    }
    //printf("Client sent : %s\n", cmd.c_str());
    request_data *args = (request_data *)shareMem(1 * sizeof(request_data));
    args->client_socket = client_socket_fd;
    args->cmd = cmd;
    args->index = index;
    sem_wait(workers_available); // Wait for a worker to be available
    int n;
    for(int i = 0; i < number_of_workers; i++) // Find the first available worker 
    {
        if(workers_active[i] == 0)
        {
            n = i;
            workers_active[i] = 1;
            break;
        }
    }
    args->thread_id = n;
    pthread_create(&worker_threads[n], NULL, handle_request, (void *)(args)); // Create a thread but dont wait on it
    // Move to the next request
    // pthread_join(worker_threads[n], NULL);


    ////////////////////////////////////////
    // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
    // Yes. The client will get the data that was sent before the FIN packet that closes the socket.
    // return NULL;
}

int main(int argc, char *argv[])
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    if (argc <= 1)
    {
        printf(BRED "Number of worker threads not specified\n" ANSI_RESET);
        exit(-1);
    }
    number_of_workers = atoi(argv[1]);
    worker_threads = (pthread_t *)shareMem(number_of_workers * sizeof(pthread_t));
    workers_active = (int *)shareMem(sizeof(int) * number_of_workers);
    for (int i = 0; i < number_of_workers; i++)
    {
        // printf("Worker\n");
        workers_active[i] = 0;
    }
    workers_available = NULL;
    workers_available = (sem_t *)shareMem(sizeof(sem_t));
    sem_init(workers_available, 0, number_of_workers);

    dict = init_dict();
    

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    // CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
        of the server process. When the server “hears” the knocking, it creates a new door—
        more precisely, a new socket that is dedicated to that particular client.
        */
        // accept is a blocking call
        client_socket_fd = 0;
        bzero(&client_addr_obj, sizeof(client_addr_obj));
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            printf(BRED "ERROR while accept() system call occurred in SERVER" ANSI_RESET);
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}