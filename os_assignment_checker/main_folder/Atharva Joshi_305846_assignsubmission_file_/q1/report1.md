

# Structures
In the q1.h file, I established the student, course, and lab structures.


For each course and student, I've created a topic.
After all threads have been created, I create a thread called time thread that executes a function that mostly keeps track of time by incrementing a variable called time now.
The following are the functions that each student and goal course call:


# TIME THERAD
To keep track of time, the time thread broadcasts a signal every second.
This is accomplished by incrementing a variable after a second of sleep.
Every 5 seconds, it additionally checks the exit condition and terminates the programme accordingly.


# Student THREAD
Each student uses a conditional variable that is activated every second by the Time thread broadcast.
The student's status is subsequently changed to "WAITING_COURSE1," meaning "waiting for course 1."


# Threads for the course
The course thread begins with a search for TA, then performs a tutorial with alloted seat and then based on probablity it allots students course.