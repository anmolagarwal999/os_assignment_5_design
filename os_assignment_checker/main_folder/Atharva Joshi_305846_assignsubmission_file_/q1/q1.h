#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


////STUDENTS STATUS////////////////////
#define NOT_YET_REGISTERED            0
#define ATTENDING_TUTORIAL_COURSE1    1
#define ATTENDING_TUTORIAL_COURSE2    2
#define ATTENDING_TUTORIAL_COURSE3    3  
#define WAITING_COURSE1               4
#define WAITING_COURSE2               5
#define WAITING_COURSE3               6   
#define PERMANENT_COURSE1             7
#define PERMANENT_COURSE2             8
#define PERMANENT_COURSE3             9
#define STUDENT_EXIT                 10

////COURSE STATUS////////////////////
#define WAIT_TA       0
#define WAIT_SLOT     1
#define TUT_ON        2
#define WITHDRAWN        3

////LAB STATUS////////////////////
#define KHTM         1
#define NT_EXHSTD     0

////TA STATUS////////////////////////
#define NT_ALTD       0
#define TA_BUSY       1




typedef struct{
    int course_id;
    char name[50];
    float interest;
    int course_max_slot;
    int no_of_labs;
    int *labs_;
    int status;
}course_struct;
typedef course_struct *ptr_course_struct;


typedef struct{
int student_id;
float calibre;
int p1 ;
int p2 ;
int p3 ;
int time;
int status;
}student_struct;
typedef student_struct *ptr_student_struct;

typedef struct {
    int lab_id;
    char name[50];
    int **TA;  //status,limit
    int TA_num;
    int maximum;
    int status;
    int current_TA ;
}lab_struct;
typedef lab_struct *ptr_lab_struct;

int num_courses;
int num_students;
int num_labs;
int courses_left;

ptr_course_struct *course;
ptr_student_struct *student;
ptr_lab_struct *lab;
pthread_t time_thread;
pthread_mutex_t lock_student;
pthread_mutex_t lock_checkTA;
pthread_mutex_t *lock_lab;
pthread_cond_t check_time = PTHREAD_COND_INITIALIZER;
pthread_cond_t check_TA=PTHREAD_COND_INITIALIZER;

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"