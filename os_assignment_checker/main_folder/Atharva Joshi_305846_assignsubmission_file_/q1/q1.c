#include "q1.h"
#define FOR(i, l) for (int(i) = 0; (i) < (l); (i)++)

int time_now = 0;

int update_lab_status()
{
    FOR(labs,num_labs)
    {
        int flag = 1;
        FOR(tas,lab[labs]->TA_num){
            if (lab[labs]->maximum > lab[labs]->TA[tas][1] ){
                flag = 0;
                break;
            }
        }
        if (flag){
            pthread_mutex_lock(&lock_lab[labs]);
            lab[labs]->status = KHTM;
            pthread_mutex_unlock(&lock_lab[labs]);
        }
        if(flag){
            printf(BLUE);
            printf("Lab %s no longer has students available for TA ship\n", lab[labs]->name);
            printf(RESET);
        }
    }
    return 0;
}

void *timer_func()
{
    while (1){
        sleep(1);
        time_now++;
        pthread_cond_broadcast(&check_time);
        if (time_now % 5 == 0)
        {
            int student_done = 0, course_done = 0;
            int remain_std[num_students];
            FOR(i,num_courses){
                if (course[i]->status != WITHDRAWN)
                    course_done++;
            }
            FOR(i,num_students){
                if (student[i]->status!=PERMANENT_COURSE1&&student[i]->status!=PERMANENT_COURSE2&&student[i]->status!=PERMANENT_COURSE3&&student[i]->status!=STUDENT_EXIT)
                {
                    remain_std[student_done] = i;
                    student_done++;
                }
            }
            if (student_done && course_done)
            {
                int flag = 1 ;
                FOR(i,student_done)
                {
                    if (student[remain_std[i]]->status < WAITING_COURSE1 || (student[remain_std[i]]->status == WAITING_COURSE1 && course[student[remain_std[i]]->p1]->status != WITHDRAWN)){
                        flag = 0 ;
                    }
                    else if((student[remain_std[i]]->status == WAITING_COURSE2 && course[student[remain_std[i]]->p2]->status != WITHDRAWN) ){
                        flag = 0 ;
                    }
                    else if(student[remain_std[i]]->status == WAITING_COURSE3 && course[student[remain_std[i]]->p3]->status != WITHDRAWN){
                        flag = 0 ;
                    }
                }
                if(flag){
                printf("Deadlock was handled when students where left but their course preferences were withdrawn\n");
                return NULL;
                }
            }
            else
                return NULL;
        }}
    return NULL;
}




int TA_available(int course_num, int *current_TA, int *current_lab)
{
    int is_TA_left = 0;
    FOR(l,course[course_num]->no_of_labs)
    {
        int allowed_lab = course[course_num]->labs_[l];
        if (lab[allowed_lab]->status == KHTM){continue;}
        FOR(i,lab[allowed_lab]->TA_num)
        {
            if (lab[allowed_lab]->TA[i][1] < lab[allowed_lab]->maximum && lab[allowed_lab]->TA[i][0] == NT_ALTD){
                pthread_mutex_lock(&lock_lab[allowed_lab]);
                if (lab[allowed_lab]->TA[i][1] < lab[allowed_lab]->maximum && lab[allowed_lab]->TA[i][0] == NT_ALTD )
                {
                    *current_lab = allowed_lab;
                    *current_TA = i;
                    lab[*current_lab]->TA[*current_TA][1]++;
                    lab[*current_lab]->TA[*current_TA][0] = TA_BUSY;
                    pthread_mutex_unlock(&lock_lab[allowed_lab]);
                    return 0;
                }
                else
                {
                    pthread_mutex_unlock(&lock_lab[allowed_lab]);
                    continue;
                }
            }
            if (lab[allowed_lab]->maximum > is_TA_left == 0 && lab[allowed_lab]->TA[i][1]){
                is_TA_left = 1;
            }
        }
    }
    *current_TA = -1;
    *current_lab = -1;
    if (!is_TA_left){return -2;}
    return -1;
}
void *course_routine(void *index)
{
    int current_TA, current_lab;
    int course_id = *(int *)index;
    while (1)
    {
        int eligible = TA_available(course_id, &current_TA, &current_lab);
        if (eligible == -2)
        {
            course[course_id]->status = WITHDRAWN;
            courses_left--;
            printf(RED);
            printf("Course %s doesnt have any TAs eligible and is removed from the course offerings\n", course[course_id]->name);
            printf(RESET);
            return NULL;
        }
        while (courses_left > 1&&current_TA < 0)
        {
            int check_once = 1;
            pthread_mutex_lock(&lock_checkTA);
            while (check_once)
            {
                check_once = 0;
                pthread_cond_wait(&check_TA, &lock_checkTA);
            }
            pthread_mutex_unlock(&lock_checkTA);
            eligible = TA_available(course_id, &current_TA, &current_lab);
        }
        if (eligible == -2)
        {
            course[course_id]->status = WITHDRAWN;
            courses_left--;
            printf(RED);
            printf("Course %s doesnt have any TAs eligible and is removed from the course offerings\n", course[course_id]->name);
            printf(RESET);
            return NULL;
        }
        printf(CYAN "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" RESET, current_TA, lab[current_lab]->name, course[course_id]->name, lab[current_lab]->TA[current_TA][1]);
        update_lab_status();
        course[course_id]->status = WAIT_SLOT;
        int slots = (rand() % (course[course_id]->course_max_slot)) + 1;
        printf(YELLOW);
        printf("Course %s has been alloted %d seats\n", course[course_id]->name, slots);
        printf(RESET);
        int W = 0;
        int student_interested[num_students];
        FOR(s,num_students)
        {
            if(W>=slots){break;}
            if (student[s]->p1 == course_id&&student[s]->status == WAITING_COURSE1){
                W++;
                student_interested[W-1] = s;
            }
            else if ( student[s]->p2 == course_id&&student[s]->status == WAITING_COURSE2){
                W++;
                student_interested[W-1] = s;
            }
            else if (student[s]->p3 == course_id&&student[s]->status == WAITING_COURSE3){
                W++;
                student_interested[W-1] = s;
            }
        }

        course[course_id]->status = TUT_ON;
        FOR(s,W)
        {
            printf(MAGENTA "Student %d has been allocated a seat in course %s\n"RESET, student_interested[s], course[course_id]->name);
            if(student[student_interested[s]]->status == WAITING_COURSE1 ){student[student_interested[s]]->status = ATTENDING_TUTORIAL_COURSE1;}
            if(student[student_interested[s]]->status == WAITING_COURSE2 ){student[student_interested[s]]->status = ATTENDING_TUTORIAL_COURSE2;}
            if(student[student_interested[s]]->status == WAITING_COURSE3 ){student[student_interested[s]]->status = ATTENDING_TUTORIAL_COURSE3;}
        }
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", course[course_id]->name, W, slots);
        float probability;
        sleep(5);
        FOR(s,W)
        {
            probability = 1 ;
            probability *= (course[course_id]->interest) ;
            probability *= (student[student_interested[s]]->calibre);
            probability *= 10000 ;
            int x = rand() % 10000 + 1;
            if (probability >= x)
            {
                if(student[student_interested[s]]->status == WAITING_COURSE1 ){student[student_interested[s]]->status = PERMANENT_COURSE1;}
                if(student[student_interested[s]]->status == WAITING_COURSE2 ){student[student_interested[s]]->status = PERMANENT_COURSE2;}
                if(student[student_interested[s]]->status == WAITING_COURSE3 ){student[student_interested[s]]->status = PERMANENT_COURSE3;}
                printf("Student %d has selected course %s permanently\n", student_interested[s], course[course_id]->name);
            }
            else
            {
                printf("Student %d has withdrawn from course %s\n", student_interested[s], course[course_id]->name);
                if (ATTENDING_TUTORIAL_COURSE3 == student[student_interested[s]]->status)
                {
                    student[student_interested[s]]->status = STUDENT_EXIT;
                    printf(RED);
                    printf("Student %d couldnt get any of his preferred courses\n", student_interested[s]);
                    printf(RESET);
                    continue;
                }
                if (ATTENDING_TUTORIAL_COURSE2 == student[student_interested[s]]->status){
                    printf(YELLOW);
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->p2]->name, course[student[student_interested[s]]->p3]->name);
                    printf(RESET);
                }
                else{
                    printf(YELLOW);
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->p1]->name, course[student[student_interested[s]]->p2]->name);
                    printf(RESET);
                }
                student[student_interested[s]]->status += 4; 
                if ( course[student[student_interested[s]]->p2]->status == WITHDRAWN && student[student_interested[s]]->status == WAITING_COURSE2 )
                {
                    student[student_interested[s]]->status = WAITING_COURSE3;
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->p2]->name, course[student[student_interested[s]]->p3]->name);
                }
                if (student[student_interested[s]]->status == WAITING_COURSE3 && course[student[student_interested[s]]->p3]->status == WITHDRAWN )
                {
                    student[student_interested[s]]->status = STUDENT_EXIT;
                    printf("Student %d couldnt get any of his preferred courses\n", student_interested[s]);
                }
            }
        }
        course[course_id]->status = WAIT_TA;
        lab[current_lab]->TA[current_TA][0] = NT_ALTD;
        printf(YELLOW);
        printf("TA %d from lab %s has completed the tutorial and left the course %s\n", current_TA, lab[current_lab]->name, course[course_id]->name);
        printf(RESET);
        pthread_cond_broadcast(&check_TA);
    }
    return NULL;
}

void *student_routine(void *index){
    pthread_mutex_lock(&lock_student);
    int i = *(int *)index;
    while (student[i]->time > time_now){
        pthread_cond_wait(&check_time, &lock_student);}
    student[i]->status = WAITING_COURSE1;
    pthread_mutex_unlock(&lock_student);

    printf(GREEN "Student %d has filled in preferences for course registration\n"RESET, i);
    return NULL;
}

int main(){
    scanf("%d", &num_students);
    scanf("%d", &num_labs);
    scanf("%d", &num_courses);
    courses_left = num_courses;
    course = (malloc(sizeof(ptr_course_struct) * num_courses));
    course = (ptr_course_struct *)course;
    FOR(i,num_courses)
    {
        course[i] = malloc(sizeof(course_struct));
        course[i] = (ptr_course_struct)course[i];
        course[i]->status = WAIT_TA;
        course[i]->course_id = i;
        scanf("%s",course[i]->name);
        scanf("%f",&course[i]->interest);
        scanf("%d",&course[i]->course_max_slot);
        scanf("%d",&course[i]->no_of_labs);
        course[i]->labs_ = malloc(sizeof(int) * course[i]->no_of_labs);
        course[i]->labs_ = (int*)course[i]->labs_;
        for (int j = 0; j < course[i]->no_of_labs; ++j)
        {
            scanf("%d", &course[i]->labs_[j]);
        }
    }

    student = (malloc(sizeof(ptr_student_struct) * num_students));
    student = (ptr_student_struct *)student;
    FOR(i,num_students)
    {
        student[i] = malloc(sizeof(student_struct));
        student[i] = (ptr_student_struct)student[i];
        student[i]->status = NOT_YET_REGISTERED;
        student[i]->student_id = i;
        scanf("%f",&student[i]->calibre);
        scanf("%d",&student[i]->p1);
        scanf("%d",&student[i]->p2);
        scanf("%d",&student[i]->p3);
        scanf("%d",&student[i]->time);
    }

    lab = (malloc(sizeof(ptr_lab_struct) * num_labs));
    lab = (ptr_lab_struct *)lab;

    FOR(i,num_labs)
    {
        lab[i] = malloc(sizeof(lab_struct));
        lab[i] = (ptr_lab_struct)lab[i];
        lab[i]->lab_id = i;
        scanf("%s",lab[i]->name);
        scanf("%d",&lab[i]->TA_num);
        scanf("%d",&lab[i]->maximum);
        lab[i]->TA = malloc(sizeof(int *) * lab[i]->TA_num);
        lab[i]->TA = (int **)lab[i]->TA ;
        for (int j = 0; j < lab[i]->TA_num; ++j)
        {
            lab[i]->TA[j] = malloc(sizeof(int) * 2);
            lab[i]->TA[j] = (int *)lab[i]->TA[j];
            lab[i]->TA[j][1] = 0;
            lab[i]->TA[j][0] = NT_ALTD;
        }
        lab[i]->status = KHTM;
        if (lab[i]->maximum && lab[i]->TA_num)
        {
            lab[i]->status = NT_EXHSTD;
        }
    }
    pthread_mutex_init(&lock_checkTA, NULL);
    pthread_t thread_courses[num_courses];
    pthread_t thread_students[num_students];
    pthread_mutex_init(&lock_student, NULL);
    srand(time(0));
    lock_lab = malloc(sizeof(pthread_mutex_t) * num_labs);
    lock_lab = (pthread_mutex_t *)lock_lab;
    FOR(labs,num_labs){pthread_mutex_init(&lock_lab[labs], NULL);}
    FOR(i,num_students)
    {
        int *a ;
        a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_students[i], NULL, &student_routine, a) != 0)
        {
            printf("COULDNT CREATE THREAD\n");
            return 0;
        }
    }
    FOR(i,num_courses)
    {
        int *a ;
        a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_courses[i], NULL, &course_routine, a) != 0){
            printf("COULDNT CREATE THREAD\n");
            return 0;
        }
    }
    if (pthread_create(&time_thread, NULL, &timer_func, NULL) != 0){
        printf("COULDNT CREATE THREAD\n");
        return 0;
    }
    FOR(i,num_courses){
        pthread_join(thread_courses[i], NULL);
    }
    FOR(i,num_students)
    {
        pthread_join(thread_students[i], NULL);
    }
    pthread_join(time_thread, NULL);
    pthread_mutex_destroy(&lock_checkTA);
    pthread_mutex_destroy(&lock_student);
    FOR(labs,num_labs){
        pthread_mutex_destroy(&lock_lab[labs]);
    }
    pthread_cond_destroy(&check_time);
    pthread_cond_destroy(&check_TA);
    FOR(i,num_courses){
        free(course[i]);
    }
    free(course);
    FOR(i,num_students){
        free(student[i]);
    }
    free(student);
    FOR(i,num_labs)
    {
        FOR(j,lab[i]->TA_num){
            free(lab[i]->TA[j]);
        }
        free(lab[i]);
    }
    free(lock_lab);
    free(lab);

    return 0;
}

void take_lab_input(ptr_lab_struct labb){
        labb->lab_id = 0;
        scanf("%s",labb->name);
        scanf("%d",&labb->TA_num);
        scanf("%d",&labb->maximum);
        labb->TA = malloc(sizeof(int *) * labb->TA_num);
        labb->TA = (int **)labb->TA ;
        for (int j = 0; j < labb->TA_num; ++j)
        {
            labb->TA[j] = malloc(sizeof(int) * 2);
            labb->TA[j] = (int *)labb->TA[j];
            labb->TA[j][1] = 0;
            labb->TA[j][0] = NT_ALTD;
        }
        labb->status = KHTM;
        if (labb->maximum && labb->TA_num)
        {
            labb->status = NT_EXHSTD;
        }
}

void take_course_input(int i){
        course[i] = malloc(sizeof(course_struct));
        course[i] = (ptr_course_struct)course[i];
        course[i]->status = WAIT_TA;
        course[i]->course_id = i;
        scanf("%s",course[i]->name);
        scanf("%f",&course[i]->interest);
        scanf("%d",&course[i]->course_max_slot);
        scanf("%d",&course[i]->no_of_labs);
        course[i]->labs_ = malloc(sizeof(int) * course[i]->no_of_labs);
        course[i]->labs_ = (int*)course[i]->labs_;
        for (int j = 0; j < course[i]->no_of_labs; ++j)
        {
            scanf("%d", &course[i]->labs_[j]);
        }
}

void take_student_input(int i){
    student[i] = (ptr_student_struct)malloc(sizeof(student_struct));
        student[i]->status = NOT_YET_REGISTERED;
        student[i]->student_id = i;
        scanf("%f",&student[i]->calibre);
        scanf("%d",&student[i]->p1);
        scanf("%d",&student[i]->p2);
        scanf("%d",&student[i]->p3);
        scanf("%d",&student[i]->time);
}