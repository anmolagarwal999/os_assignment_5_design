#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <unistd.h>

#define HOME    0
#define AWAY    1
#define NEUTRAL 2


struct person {
    int t ;
    int r ;
    char team ;
    int support ;
    char name[30] ;
    int patience;
};
typedef struct person person ;


struct goal_chance {
    char team_c ;
    int team ;
    float p ;
    int t ;
};
typedef struct goal_chance goal_chance ;

struct argument{
    int group_index ;
    int person_index ;
};
typedef struct argument argument ;

struct group {
    int num_people ;
    person** people ;
    int num_left ;
};
typedef struct group group ;


int capacity_H ;
int capacity_A ;
int capacity_N ;
int filled_H ;
int filled_A ;
int filled_N ;
int num_group ;
int spectating_time ;
int home_team_goal ;
int away_team_goal ;
int num_goal_scoring_chance ;
group** groups;
goal_chance** goal_chances ;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t timer = PTHREAD_COND_INITIALIZER ;
pthread_cond_t checkseat = PTHREAD_COND_INITIALIZER ;
int time_count ;

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"