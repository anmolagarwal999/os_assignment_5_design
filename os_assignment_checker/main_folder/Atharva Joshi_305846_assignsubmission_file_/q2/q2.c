#include"q2.h"
void* time_counter(){
    while(1){
        sleep(1);
        time_count++;
        pthread_cond_broadcast(&timer);
        int flag=1;
        for(int i=0;i<num_group;i++){
            if(groups[i]->num_left!=groups[i]->num_people){flag=0;break;}
        }
        if(flag){break;}
    }
    return NULL;
}

void* people_routine(void* arg){
    argument curr_arg = *(argument*)arg ;
    int curr_group_index = curr_arg.group_index ;
    int curr_person_index = curr_arg.person_index ;
    group* curr_group = groups[curr_group_index] ;
    person* curr_person = curr_group->people[curr_person_index] ;
    pthread_mutex_lock(&lock);
    while(time_count<curr_person->t){
        pthread_cond_wait(&timer,&lock);
    }
    pthread_mutex_unlock(&lock);
    printf(GREEN);
    printf("t=%d : %s has reached the stadium\n",time_count,curr_person->name);
    printf(RESET);
    int start_watch_time = -1 ;
    int stand = -1 ;    
    while(time_count<=curr_person->t+curr_person->patience){
        if(curr_person->support==HOME){
            pthread_mutex_lock(&lock);
            if(capacity_H!=filled_H){filled_H++;stand=HOME;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone H\n",time_count,curr_person->name);
                printf(RESET);
            }
            else if(capacity_N!=filled_N){filled_N++;stand=NEUTRAL;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone N\n",time_count,curr_person->name);
                printf(RESET);
            }
            pthread_mutex_unlock(&lock);
            if(start_watch_time!=-1){goto WATCH;}
            pthread_mutex_lock(&lock);
            int check_once = 1;
            while(check_once){
                check_once = 0 ;
                pthread_cond_wait(&timer,&lock);
            }
            pthread_mutex_unlock(&lock);
            
        }
        else if(curr_person->support==AWAY){
            
            pthread_mutex_lock(&lock);
            if(capacity_A!=filled_A){filled_A++;stand=AWAY;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone A\n",time_count,curr_person->name);
                printf(RESET);
            }
            pthread_mutex_unlock(&lock);
            if(start_watch_time!=-1){goto WATCH;}
            pthread_mutex_lock(&lock);
            int check_once = 1;
            while(check_once){
                check_once = 0 ;
                pthread_cond_wait(&timer,&lock);
            }
            pthread_mutex_unlock(&lock);
        }
        else if(curr_person->support==NEUTRAL){
            
            pthread_mutex_lock(&lock);
            if(capacity_H!=filled_H){filled_H++;stand=HOME;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone H\n",time_count,curr_person->name);
                printf(RESET);
            }
            else if(capacity_N!=filled_N){filled_N++;stand=NEUTRAL;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone N\n",time_count,curr_person->name);
                printf(RESET);
            }
            else if(capacity_A!=filled_A){filled_A++;stand=AWAY;start_watch_time=time_count;
                printf(YELLOW);
                printf("t=%d : %s has got a seat in zone A\n",time_count,curr_person->name);
                printf(RESET);
            }
            pthread_mutex_unlock(&lock);
            if(start_watch_time!=-1){goto WATCH;}
            pthread_mutex_lock(&lock);
            int check_once = 1;
            while(check_once){
                check_once = 0 ;
                pthread_cond_wait(&timer,&lock);
            }
            pthread_mutex_unlock(&lock);
        }
    }
    printf(RED);
    printf("t=%d : %s could not get a seat\n",time_count,curr_person->name);
    printf(RESET);
    goto END ;


WATCH:
    while(start_watch_time+spectating_time>time_count){
        if(curr_person->support==HOME){
            if(home_team_goal<= away_team_goal - curr_person->r ){
                printf(BLUE);
                printf("t=%d : %s is leaving due to bad performance of their team\n",time_count,curr_person->name);
                printf(RESET);
                goto END ;
            }
        }
        if(curr_person->support==AWAY){
            if(away_team_goal<= home_team_goal - curr_person->r ){
                printf(BLUE);
                printf("t=%d : %s is leaving due to bad performance of their team\n",time_count,curr_person->name);
                printf(RESET);
                goto END ;
            }
        }
        pthread_mutex_unlock(&lock);
        int check_once = 1;
        while(check_once){
            check_once = 0 ;
            pthread_cond_wait(&timer,&lock);
        }
        pthread_mutex_unlock(&lock);
    }
    printf(CYAN);
    printf("t=%d : %s watched the match for %d seconds and is leaving\n",time_count,curr_person->name,spectating_time);
    printf(RESET);

END:
    pthread_mutex_lock(&lock);
    if(stand==HOME){filled_H--;}
    if(stand==AWAY){filled_A--;}
    if(stand==NEUTRAL){filled_N--;}
    pthread_mutex_unlock(&lock);
    pthread_cond_broadcast(&timer);
    curr_group->num_left++;
    printf(MAGENTA);
    printf("t=%d : %s is waiting for their friends at the exit\n",time_count,curr_person->name);
    printf(RESET);
    if(curr_group->num_left==curr_group->num_people){
        printf(BLUE);
        printf("t=%d : Group %d left\n",time_count,curr_group_index+1);
        printf(RESET);
    }
    return NULL;
}

void* chance_routine(void* arg){
    int i = *(int*)arg ;
    pthread_mutex_lock(&lock);
    while(time_count<goal_chances[i]->t){
        pthread_cond_wait(&timer,&lock);
    }
    float x = rand() % 100 + 1; 
    float prob = goal_chances[i]->p * 100 ;
    if(x<=prob){
        printf(GREEN);
        printf("t=%d : Team %c scored their ",time_count,goal_chances[i]->team_c);
        if(goal_chances[i]->team==HOME){
            home_team_goal++;
            if(home_team_goal%10==1){printf("%dst goal\n",home_team_goal);}
            else if(home_team_goal%10==2){printf("%dnd goal\n",home_team_goal);}
            else if(home_team_goal%10==3){printf("%drd goal\n",home_team_goal);}
            else{printf("%dth goal\n",home_team_goal);}
            pthread_cond_broadcast(&timer);
        }
        if(goal_chances[i]->team==AWAY){
            away_team_goal++;
            if(away_team_goal%10==1){printf("%dst goal\n",away_team_goal);}
            else if(away_team_goal%10==2){printf("%dnd goal\n",away_team_goal);}
            else if(away_team_goal%10==3){printf("%drd goal\n",away_team_goal);}
            else{printf("%dth goal\n",away_team_goal);}
            pthread_cond_broadcast(&timer);
        }
        printf(RESET);
    }
    else{
        printf(RED);
        printf("t=%d : Team %c missed the chance to score their ",time_count,goal_chances[i]->team_c);
        if(goal_chances[i]->team==HOME){
            if(home_team_goal%10==0){printf("%dst goal\n",home_team_goal+1);}
            else if(home_team_goal%10==1){printf("%dnd goal\n",home_team_goal+1);}
            else if(home_team_goal%10==2){printf("%drd goal\n",home_team_goal+1);}
            else{printf("%dth goal\n",home_team_goal+1);}
        }
        if(goal_chances[i]->team==AWAY){
            if(away_team_goal%10==0){printf("%dst goal\n",away_team_goal+1);}
            else if(away_team_goal%10==1){printf("%dnd goal\n",away_team_goal+1);}
            else if(away_team_goal%10==2){printf("%drd goal\n",away_team_goal+1);}
            else{printf("%dth goal\n",away_team_goal+1);}
        }
        printf(RESET);
    }
    pthread_mutex_unlock(&lock);
}

int main(){
    pthread_mutex_init(&lock,NULL);
    srand(time(0));
    time_count =  0 ;
    filled_H = 0 ;
    filled_A = 0 ;
    filled_N = 0 ;
    home_team_goal = 0 ;
    away_team_goal = 0 ;
    scanf("%d %d %d",&capacity_H,&capacity_A,&capacity_N);
    scanf("%d",&spectating_time);
    scanf("%d",&num_group);

    groups = (group**)malloc(sizeof(group*)*num_group);
    for(int i=0;i<num_group;i++){
        groups[i] = (group*)malloc(sizeof(group));
        scanf("%d",&groups[i]->num_people);
        groups[i]->people = (person**)malloc(sizeof(person*)*groups[i]->num_people);
        for(int j=0;j<groups[i]->num_people;j++){
            groups[i]->people[j] = (person*)malloc(sizeof(person));
            scanf("%s %c %d %d %d",groups[i]->people[j]->name,&groups[i]->people[j]->team,&groups[i]->people[j]->t,&groups[i]->people[j]->patience,&groups[i]->people[j]->r);
            if(groups[i]->people[j]->team=='H'){groups[i]->people[j]->support=HOME;}
            if(groups[i]->people[j]->team=='A'){groups[i]->people[j]->support=AWAY;}
            if(groups[i]->people[j]->team=='N'){groups[i]->people[j]->support=NEUTRAL;}
        }
        groups[i]->num_left=0;
    }
    scanf("%d",&num_goal_scoring_chance);
    goal_chances = (goal_chance**)malloc(sizeof(goal_chance*)*num_goal_scoring_chance);
    for(int i=0;i<num_goal_scoring_chance;i++){
        goal_chances[i] = (goal_chance*)malloc(sizeof(goal_chance));
        scanf("\n%c %d %f",&goal_chances[i]->team_c,&goal_chances[i]->t,&goal_chances[i]->p);
        if(goal_chances[i]->team_c=='H'){goal_chances[i]->team=HOME;}
        if(goal_chances[i]->team_c=='A'){goal_chances[i]->team=AWAY;}
    }
    pthread_t ** people ;
    people = (pthread_t**)malloc(sizeof(person*)*num_group);
    for(int i=0;i<num_group;i++){
        people[i] = (pthread_t*)malloc(sizeof(person)*groups[i]->num_people);
    }
    pthread_t time_thread ;
    pthread_t chances[num_goal_scoring_chance] ;
    for(int i=0;i<num_group;i++){
        for(int j=0;j<groups[i]->num_people;j++){
            argument* curr_arg ;
            curr_arg = (argument*)malloc(sizeof(argument));
            curr_arg->group_index = i ;
            curr_arg->person_index = j ;
            if(pthread_create(&people[i][j], NULL, &people_routine,curr_arg )!=0){
            printf("COULDNT CREATE THREAD\n");
            }
        }
    }
    for(int i=0;i<num_goal_scoring_chance;i++){
        int *curr_arg ;
        curr_arg = (int*)malloc(sizeof(int));
        *curr_arg = i ;
        if(pthread_create(&chances[i], NULL, &chance_routine,curr_arg )!=0){
            printf("COULDNT CREATE THREAD\n");
        }
    }
    if(pthread_create(&time_thread,NULL,&time_counter,NULL)!=0){
        printf("COULDNT CREATE THREAD\n");
    }
    for(int i=0;i<num_group;i++){
        for(int j=0;j<groups[i]->num_people;j++){
            pthread_join(people[i][j],NULL);
        }
    }
    pthread_join(time_thread,NULL);
    for(int i=0;i<num_group;i++){
        for(int j=0;j<groups[i]->num_people;j++){
            free(groups[i]->people[j]);
        }
        free(groups[i]);
    }
    for(int i=0;i<num_goal_scoring_chance;i++){
        free(goal_chances[i]);
    }
    pthread_mutex_destroy(&lock);
    
}