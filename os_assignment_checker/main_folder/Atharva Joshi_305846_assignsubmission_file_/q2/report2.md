
I have used 4 structures
->  person: it stores the time of arrival, goal difference, team, name and patience of each person
-> goal_chance: it stores the team, the time and probablity
-> argument: stores cerain argument which i passed to a thread
-> group:, stores , number of people, an array storing information for these people and number of people who left 




I am using three types of thread in this problem

-> person thread
-> goal chances thread
-> timer thread

the timer thread keeps track of time and brodcasts a signal whenever a second passes
person thread keeps track of a person, when it arrives, when it gets a seat, when it leaves
goal chance thread, using the probablity at the given time it calculates whether goal has been scored and not and brodcasts a fignal accordingly.

HOW THINGS WORK?

the person and chances thread gets a signal at every second and checks whether their time of arrival or time of chance has been reached, if yes then they proceed accordingly

now, if a goal is scored, it triggers the same 'timer' conditional variable, because a person sitting in a stand only leaves either because of goal difference or time, thus triggerin the same timer conditional variable made implementation easy.

In a similar manner, whenever a person leaves a stand it triggers a timer variable, because when a person is waiting for a seat , it either leaves because of patience time or gets a seat if a seat is empty thus using the same timer variable made implementaion easy.