# Q1 Report

This question has 4 main entities in play: 

* Course
* Student
* Lab
* Teaching Assistant

We build a structure for each and give them their required attributes.

```c
typedef struct mentor
{
    int id;               // appointing id when called
    int available;        // to avoid other courses from using this (0 if available)
    pthread_mutex_t mutex;  // mutex for each TA
    pthread_cond_t cond;    // condition variable for each TA
} mentor;

typedef struct course
{
    pthread_t Courses;      // course Thread
    pthread_mutex_t allmut; // mutex for all students
    pthread_mutex_t mutex;  // mutex for each course
    pthread_cond_t cond;    // condition variable to start tute
    pthread_cond_t sadmis;  // condition variable for start admission
    int available;        // 0 if Ta is available, 1 if not
    char course_name[DON];  // course name
    float interestq;      // interest quotient
    int course_max_slots; // max slots for students in the course
    int ass_lab;          // number of associated labs
    int current_lab_index;// current lab id
    int labid[DON];     // lab ids
    int students_alloted; // number of students alloted to the course before a tute
    int rand_seat;        // random seat capacity
    int start_tute;       // 0 if TA not appointed, 1 if appointed
    int start_admission;  // 0 if rand seat not allocated, 1 if allocated
} course;


typedef struct lab
{
    pthread_mutex_t mutex;  // mutex for each lab
    char lab_name[DON];  // lab name
    int num_fellow; // number of TA fellows working at the lab
    int ta_quota;   // number of time TAship allowed
    int curr_ta_index; // current id of TA (fellows indexed from 0)
    int available;     // 0/1 if available/not , this enables other courses to which this lab is associated to know if this lab is exhausted or not
    int finished_quota; // min number of tutes done by all fellows (to implement bonus)
    mentor fellow[DON]; // array of fellow TA fellows
} lab;

typedef struct student
{
    pthread_cond_t cond;    // condition variable for each student
    pthread_mutex_t mutex;  // mutex for each student
    pthread_t Student;      // student thread
    float calibreq; // calibre quotient of each student
    int cid[3];     // array prefered courses 
    int time;       // time taken to register on portal
    int status;     // -1 if not registered, 1,2,3 per course , 4 if course selected and 5 if no course selected
    int available;  // 0 if available, 1 if not available
} student;
```

* We create threads for students and courses and only when each student exits simulation, we end the program.

* The student thread is operated by `void *student_enroll(void *args)` function and the course thread is operated by `void *course_play(void *args)`.

* We create an array of course, lab and students. The TAs are integrated inside struct lab hence we don't create array of TAs.

  ```c
  course *course_thread[DON]; // array of course threads
  lab *lab_thread[DON];       // array of lab threads
  student *student_thread[DON];// array of student threads
  ```

  

## Student Enroll `void *student_enroll(void *args)`

**It consists of 4 parts :**

1. Student fills preference after some duration
2. Student waits for seat allocation of current preference course
3. Student attends tutorial and chooses course permanently and leaves simulation else moves to next preference
4. Student doesn't select any course and leaves simulation

### Part 1:

The thread sleeps for some duration and then student fills in the preference.

```c
sleep(student_thread[student_id]->time);        // waits for student to register on portal
printf(ANSI_DEFAULT "Student %d has filled in preference for courses %s , %s and %s in decreasing order of priority for course registration form.\n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
```

### Part 2:

* First we check if the course is available or not. If it isn't available we move on to next preference course or we remove the student from simulation if the unavailable course was his/her last preference course.

  ```c
  if(course_thread[course_id]->available == 0){...}
  else{
  // student couldnt attend this course coz withdrawn
  printf(ANSI_BLUE"Student %d has withdrawn from course %s \n", student_id, course_thread[course_id]->course_name);
  c++;
  if(c == 3){ // exits simulation if last preference course was withdrawn
  student_thread[student_id]->status = 5;
  student_thread[student_id]->available = 1;
  printf(ANSI_RED"Student %d couldn’t get any of his preferred courses from %s, %s and %s, Hence exited the simulation :( \n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
  dup_students--;
  break;
  }
  ```

  

* We use `pthread_cond_wait` and conditional variable `course_thread[course_id]->sadmis` (which is an attribute of course struct) to put the student thread to sleep while the course admission hasn't started. This avoid busy wait. We also check if students allotted to the course tutorial don't exceed the random limit set.

  ```c
  pthread_mutex_lock(&course_thread[course_id]->allmut);      // updates seats alloted     
  while(course_thread[course_id]->start_admission == 0 ){  // waits for course to start admission
  pthread_cond_wait(&course_thread[course_id]->sadmis, &course_thread[course_id]->allmut);
  }
  if(course_thread[course_id]->students_alloted <= course_thread[course_id]->rand_seat){  
  course_thread[course_id]->students_alloted++;
  student_thread[student_id]->status = c+1;
  pthread_mutex_unlock(&course_thread[course_id]->allmut);
  ...
  }
  ```

  

* Once the Seat is allotted to student in his current preference course, we update `student_alloted` attribute of course thread with appropriate mutex locks `course_thread[course_id]->allmut` and `student_thread[student_id]->mutex`.

### Part 3:

* After the seat is allotted to student, that student thread is put to sleep till the tutorial for that course is finished, using `pthread_cond_wait` and conditional variable `course_thread[course_id]->cond` and when the signal is broadcasted, the thread wakes up.

  ```c
  pthread_mutex_lock(&student_thread[student_id]->mutex);              
  while(course_thread[course_id]->start_tute == 0){   // waits for tutes to finish  pthread_cond_wait(&course_thread[course_id]->cond, &student_thread[student_id]->mutex);
  }
  pthread_mutex_unlock(&student_thread[student_id]->mutex);
  ```

* We randomly generate threshold `rand_thresh` to decide if the student selects the course or not. If he/she selects the course, the student is removed from simulation, if they don't select the course, they move to their next preference course and if the recently rejected course was their last preference course, student is removed from the simulation.

  ```c
  // randomly generates threshold and checks if student has selected the course
  float prob = course_thread[course_id]->interestq * student_thread[student_id]->calibreq;
  float rand_thresh = (float)(rand() % 50 + 20 ) / 100.00;
  
  if(prob >= rand_thresh){    // student selects the course and exits the simulation
      printf(ANSI_GREEN "Student %d has selected course %s permanently with probability %.2f >= %.2f(thresh) \n", student_id, course_thread[course_id]->course_name, prob, rand_thresh);
      printf(ANSI_RED"Student %d has exited simulation\n", student_id);
      // update students information
      student_thread[student_id]->status = 4;
      student_thread[student_id]->available = 1;
      dup_students--;
      break;
  }
  else{   // student does not select the course
      
      printf(ANSI_BLUE"Student %d has withdrawn from course %s with probability %.2f < %.2f(thresh) \n", student_id, course_thread[course_id]->course_name, prob, rand_thresh);
      student_thread[student_id]->status++;
      c++;
      // student either changes his preference or exits the simulation
      if(c != 3)printf(ANSI_YELLOW"Student %d has changed current preference from course %s (priority %d) to course %s (priority %d)\n", student_id, course_thread[student_thread[student_id]->cid[c-1]]->course_name, c, course_thread[student_thread[student_id]->cid[c]]->course_name, c+1);
      else if(c == 3){
          dup_students--;
          student_thread[student_id]->status = 5;
          student_thread[student_id]->available = 1;
          printf(ANSI_RED "Student %d couldn’t get any of his preferred courses from %s, %s and %s, Hence exited the simulation :(\n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
          dup_students--;
          break;
      }
  }
  ```

### Part 4

* If the Student has attended all 3 courses tutorial but didn't select either course permanently, the student is removed from the simulation.

  ```c
  else{
      // student couldnt attend this course coz withdrawn
      printf(ANSI_BLUE"Student %d has withdrawn from course %s \n", student_id, course_thread[course_id]->course_name);
      c++;
      if(c == 3){ // exits simulation if last preference course was withdrawn
          student_thread[student_id]->status = 5;
          student_thread[student_id]->available = 1;
          printf(ANSI_RED"Student %d couldn’t get any of his preferred courses from %s, %s and %s, Hence exited the simulation :( \n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
          dup_students--;
          break;
      }
  }
  ```



## Course Play `void *course_play(void *args)`

**It consists of 5 parts:**

1. We allot TA from one of the associated labs to course.
2. We update the lab details.
3. We randomly generate seats for the tutorial and start admissions.
4. We Take tutorial with randomly generated duration.
5. We remove the course if no TAs from any associated labs are available.

### Part 1:

* We check if the current lab index of course `course_thread[course_id]->current_lab_index` is less than number of associated labs (making sure course has labs with TAs available).

*  We note down details of TA taking the course and allot this TA to the course.

  ```c
  pthread_mutex_lock(&lab_thread[lab_id]->mutex);     // notes TAship number and increaments current lab ta for other courses to use it simultaneously
  int taship_num = lab_thread[lab_id]->finished_quota;
  if(current_ta_index < lab_thread[lab_id]->num_fellow - 1)taship_num++;
  
  lab_thread[lab_id]->curr_ta_index = (current_ta_index + 1) % lab_thread[lab_id]->num_fellow;
  pthread_mutex_unlock(&lab_thread[lab_id]->mutex);
  ```

### Part 2:

* In order to apply **BONUS** we make sure TAs are allotted to each course in sequential order. We maintain `finished_quota`  attribute in the lab struct, which hold the value of minimum number of tutorials conducted by all TAs associated to that specific lab. This helps us keep track of current number of TAship.

* Once the `finished_quota` equals `ta_quota` the lab is declared unavailable as all TAs associated to it are exhausted.

  ```c
  
  pthread_mutex_lock(&lab_thread[lab_id]->mutex);
  int current_ta_index = lab_thread[lab_id]->curr_ta_index;   // notes the current TA index and updates lab info
  if(lab_thread[lab_id]->finished_quota == lab_thread[lab_id]->num_fellow - 1 && lab_thread[lab_id]->available == 0){
      lab_thread[lab_id]->available = 1;
      printf(ANSI_RED"Lab %s no longer has students available for TA ship\n", lab_thread[lab_id]->lab_name);
      continue;
  }   // BONUS: TA alloted in such a way that a TA P gets 't+1'th TAship only when all other TAs have finished 't' TAships
  else if(lab_thread[lab_id]->curr_ta_index == lab_thread[lab_id]->num_fellow - 1 && lab_thread[lab_id]->available == 0){
      lab_thread[lab_id]->curr_ta_index = 0;
      lab_thread[lab_id]->finished_quota++;
  }
  else if(lab_thread[lab_id]->available == 0){
      lab_thread[lab_id]->curr_ta_index++;
  }
  pthread_mutex_unlock(&lab_thread[lab_id]->mutex);
  ```

### Part 3:

* We use `rand()` to generate random number of tutorial seats between 1 and `course_max_slot` and we use `pthread_cond_broadcast` paired with `course_thread[course_id]->mutex` mutex locks and `course_thread[course_id]->sadmis` conditional variable to wake up all the sleeping student threads waiting for seat allocation in their preference course.

  ```c
  course_thread[course_id]->rand_seat = (rand()%course_thread[course_id]->course_max_slots) + 1;  // randomly allocates seats to course
  printf(ANSI_CYAN"Course %s has been allocated %d seats.\n", course_thread[course_id]->course_name, course_thread[course_id]->rand_seat);
  
  course_thread[course_id]->start_tute = 0;
  course_thread[course_id]->students_alloted = 0;
  
  pthread_mutex_lock(&course_thread[course_id]->mutex);   // signals to start student admission to course
  course_thread[course_id]->start_admission = 1;
  pthread_cond_broadcast(&course_thread[course_id]->sadmis);  
  pthread_mutex_unlock(&course_thread[course_id]->mutex);
  sleep(5);
  ```

* We wait for 5 seconds for students to take admission to the course.

### Part 4:

* We use `rand()` to generate random tutorial time between 1 and 5 seconds and wait for those seconds for tutorial to finish. Once the tutorial is finished, we use `pthread_cond_broadcast` paired with `course_thread[course_id]->mutex` mutex locks and `course_thread[course_id]->cond` conditional variable to wake up all the sleeping student threads waiting for tutorials to finish.

  ```c
  // starts Tutorials
  printf(ANSI_MAGENTA"Tutorial has started for Course %s with %d seats filled out of %d\n", course_thread[course_id]->course_name, course_thread[course_id]->students_alloted, course_thread[course_id]->rand_seat);
  int tute_time = rand()%5 + 1;
  sleep(tute_time);
  printf(ANSI_GREEN "TA %d from lab %s has completed the tutorial and left the course %s\n", lab_thread[lab_id]->curr_ta_index, lab_thread[lab_id]->lab_name, course_thread[course_id]->course_name);
  
  // signals all students to that tutorial is over
  pthread_mutex_lock(&course_thread[course_id]->mutex);
  course_thread[course_id]->start_tute = 1;
  pthread_cond_broadcast(&course_thread[course_id]->cond);
  pthread_mutex_unlock(&course_thread[course_id]->mutex);
  ```

### Part 5:

* The course is removed once all TAs working in all the associated labs have exhausted their tutorial quota.

  ```c
  pthread_mutex_lock(&course_thread[course_id]->mutex);
  course_thread[course_id]->available = 1;
  pthread_mutex_unlock(&course_thread[course_id]->mutex);
  printf(ANSI_RED"Course %s doesn't have any TA's eligible and is removed from course offerings\n",course_thread[course_id]->course_name);
  return NULL;
  ```

 

## Main `int main()`

We take input according to the specified format and initialize the necessary mutex locks, threads, conditional variables and join student threads.