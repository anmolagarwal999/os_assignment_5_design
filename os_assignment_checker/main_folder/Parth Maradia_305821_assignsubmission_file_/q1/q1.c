//to compile: gcc <filename>.c -lpthread
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>


#define ANSI_CYAN "\033[1;36m"
#define ANSI_RED "\033[1;31m"
#define ANSI_GREEN "\033[1;32m"
#define ANSI_YELLOW "\033[1;33m"
#define ANSI_BLUE "\033[1;34m"
#define ANSI_MAGENTA "\033[1;35m"
#define ANSI_DEFAULT "\033[0m"

#define DON 1000

typedef struct mentor
{
    int id;               // appointing id when called
    int available;        // to avoid other courses from using this (0 if available)
    pthread_mutex_t mutex;  // mutex for each TA
    pthread_cond_t cond;    // condition variable for each TA
} mentor;

typedef struct course
{
    pthread_t Courses;      // course Thread
    pthread_mutex_t allmut; // mutex for all students
    pthread_mutex_t mutex;  // mutex for each course
    pthread_cond_t cond;    // condition variable to start tute
    pthread_cond_t sadmis;  // condition variable for start admission
    int available;        // 0 if Ta is available, 1 if not
    char course_name[DON];  // course name
    float interestq;      // interest quotient
    int course_max_slots; // max slots for students in the course
    int ass_lab;          // number of associated labs
    int current_lab_index;// current lab id
    int labid[DON];     // lab ids
    int students_alloted; // number of students alloted to the course before a tute
    int rand_seat;        // random seat capacity
    int start_tute;       // 0 if TA not appointed, 1 if appointed
    int start_admission;  // 0 if rand seat not allocated, 1 if allocated
} course;


typedef struct lab
{
    pthread_mutex_t mutex;  // mutex for each lab
    char lab_name[DON];  // lab name
    int num_fellow; // number of TA fellows working at the lab
    int ta_quota;   // number of time TAship allowed
    int curr_ta_index; // current id of TA (fellows indexed from 0)
    int available;     // 0/1 if available/not , this enables other courses to which this lab is associated to know if this lab is exhausted or not
    int finished_quota; // min number of tutes done by all fellows (to implement bonus)
    mentor fellow[DON]; // array of fellow TA fellows
} lab;

typedef struct student
{
    pthread_cond_t cond;    // condition variable for each student
    pthread_mutex_t mutex;  // mutex for each student
    pthread_t Student;      // student thread
    float calibreq; // calibre quotient of each student
    int cid[3];     // array prefered courses 
    int time;       // time taken to register on portal
    int status;     // -1 if not registered, 1,2,3 per course , 4 if course selected and 5 if no course selected
    int available;  // 0 if available, 1 if not available
} student;

int num_courses, num_labs, num_students, dup_students; // number of courses, labs, and students
bool stop_courses;  // to stop all courses

course *course_thread[DON]; // array of course threads
lab *lab_thread[DON];       // array of lab threads
student *student_thread[DON];// array of student threads


void *student_enroll(void *args){
    int student_id = *(int *)args;
    int c = 0;
    sleep(student_thread[student_id]->time);        // waits for student to register on portal
    printf(ANSI_DEFAULT "Student %d has filled in preference for courses %s , %s and %s in decreasing order of priority for course registration form.\n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);

    pthread_mutex_lock(&student_thread[student_id]->mutex);
    student_thread[student_id]->available = 0; // student available for course seat allocation
    pthread_mutex_unlock(&student_thread[student_id]->mutex);

    while(student_thread[student_id]->status != 4 || student_thread[student_id]->status != 5){
        
        int course_id = student_thread[student_id]->cid[c];     // course id of preferred course
        
        if(course_thread[course_id]->available == 0){           // if course is available
            
            pthread_mutex_lock(&course_thread[course_id]->allmut);      // updates seats alloted
            
            while(course_thread[course_id]->start_admission == 0 ){  // waits for course to start admission
                pthread_cond_wait(&course_thread[course_id]->sadmis, &course_thread[course_id]->allmut);
            }

            if(course_thread[course_id]->students_alloted <= course_thread[course_id]->rand_seat){
                
                course_thread[course_id]->students_alloted++;
                student_thread[student_id]->status = c+1;
                
                pthread_mutex_unlock(&course_thread[course_id]->allmut);

                printf(ANSI_YELLOW"Student %d has been allotted to course %s \n", student_id, course_thread[course_id]->course_name);

                pthread_mutex_lock(&student_thread[student_id]->mutex);              
                while(course_thread[course_id]->start_tute == 0){   // waits for tutes to finish
                    pthread_cond_wait(&course_thread[course_id]->cond, &student_thread[student_id]->mutex);
                }
                pthread_mutex_unlock(&student_thread[student_id]->mutex);

                // randomly generates threshold and checks if student has selected the course
                float prob = course_thread[course_id]->interestq * student_thread[student_id]->calibreq;
                float rand_thresh = (float)(rand() % 50 + 20 ) / 100.00;
                
                if(prob >= rand_thresh){    // student selects the course and exits the simulation
                    printf(ANSI_GREEN "Student %d has selected course %s permanently with probability %.2f >= %.2f(thresh) \n", student_id, course_thread[course_id]->course_name, prob, rand_thresh);
                    printf(ANSI_RED"Student %d has exited simulation\n", student_id);
                    // update students information
                    student_thread[student_id]->status = 4;
                    student_thread[student_id]->available = 1;
                    dup_students--;
                    break;
                }
                else{   // student does not select the course
                    
                    printf(ANSI_BLUE"Student %d has withdrawn from course %s with probability %.2f < %.2f(thresh) \n", student_id, course_thread[course_id]->course_name, prob, rand_thresh);
                    student_thread[student_id]->status++;
                    c++;
                    // student either changes his preference or exits the simulation
                    if(c != 3)printf(ANSI_YELLOW"Student %d has changed current preference from course %s (priority %d) to course %s (priority %d)\n", student_id, course_thread[student_thread[student_id]->cid[c-1]]->course_name, c, course_thread[student_thread[student_id]->cid[c]]->course_name, c+1);
                    else if(c == 3){
                        dup_students--;
                        student_thread[student_id]->status = 5;
                        student_thread[student_id]->available = 1;
                        printf(ANSI_RED "Student %d couldn’t get any of his preferred courses from %s, %s and %s, Hence exited the simulation :(\n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
                        dup_students--;
                        break;
                    }
                }
            }
            else{
                pthread_mutex_lock(&student_thread[student_id]->mutex);
                while(course_thread[course_id]->students_alloted != 0){
                    pthread_cond_wait(&course_thread[course_id]->sadmis,&course_thread[course_id]->allmut);
                }
                pthread_mutex_unlock(&student_thread[student_id]->mutex);
                pthread_mutex_unlock(&course_thread[course_id]->allmut);
                continue;
            }
        }
        else{
            // student couldnt attend this course coz withdrawn
            printf(ANSI_BLUE"Student %d has withdrawn from course %s \n", student_id, course_thread[course_id]->course_name);
            c++;
            if(c == 3){ // exits simulation if last preference course was withdrawn
                student_thread[student_id]->status = 5;
                student_thread[student_id]->available = 1;
                printf(ANSI_RED"Student %d couldn’t get any of his preferred courses from %s, %s and %s, Hence exited the simulation :( \n", student_id, course_thread[student_thread[student_id]->cid[0]]->course_name, course_thread[student_thread[student_id]->cid[1]]->course_name, course_thread[student_thread[student_id]->cid[2]]->course_name);
                dup_students--;
                break;
            }
        }
    }
}

void *course_play(void *args){
    int course_id = *(int *)args;
    
    while(course_thread[course_id]->available == 0 && course_thread[course_id]->current_lab_index < course_thread[course_id]->ass_lab ){
        if(!stop_courses){  // stops course if all students exited simulation
            pthread_exit(NULL);
            exit(1);
            return NULL;
        }
        else if(dup_students == 0){
            printf(ANSI_GREEN"All students have exited simulation\n");
            stop_courses = false;
            pthread_exit(NULL);
            exit(1);
        }
        
        int lab_id = course_thread[course_id]->labid[course_thread[course_id]->current_lab_index];  // lab id in sequential order

        if(lab_thread[lab_id]->available == 1){     // if lab is not available
            pthread_mutex_lock(&course_thread[course_id]->mutex);
            course_thread[course_id]->current_lab_index++;
            pthread_mutex_unlock(&course_thread[course_id]->mutex);
            continue;
        }
        else if(lab_thread[lab_id]->available == 0){    // if lab is available

            pthread_mutex_lock(&lab_thread[lab_id]->mutex);
            int current_ta_index = lab_thread[lab_id]->curr_ta_index;   // notes the current TA index and updates lab info
            if(lab_thread[lab_id]->finished_quota == lab_thread[lab_id]->num_fellow - 1 && lab_thread[lab_id]->available == 0){
                lab_thread[lab_id]->available = 1;
                printf(ANSI_RED"Lab %s no longer has students available for TA ship\n", lab_thread[lab_id]->lab_name);
                continue;
            }   // BONUS: TA alloted in such a way that a TA P gets 't+1'th TAship only when all other TAs have finished 't' TAships
            else if(lab_thread[lab_id]->curr_ta_index == lab_thread[lab_id]->num_fellow - 1 && lab_thread[lab_id]->available == 0){
                lab_thread[lab_id]->curr_ta_index = 0;
                lab_thread[lab_id]->finished_quota++;
            }
            else if(lab_thread[lab_id]->available == 0){
                lab_thread[lab_id]->curr_ta_index++;
            }
            pthread_mutex_unlock(&lab_thread[lab_id]->mutex);

            pthread_mutex_lock(&lab_thread[lab_id]->mutex);     // notes TAship number and increaments current lab ta for other courses to use it simultaneously
            int taship_num = lab_thread[lab_id]->finished_quota;
            if(current_ta_index < lab_thread[lab_id]->num_fellow - 1)taship_num++;

            lab_thread[lab_id]->curr_ta_index = (current_ta_index + 1) % lab_thread[lab_id]->num_fellow;
            pthread_mutex_unlock(&lab_thread[lab_id]->mutex);

            // course is alloted seats and TA takes tutes
            pthread_mutex_lock(&lab_thread[lab_id]->fellow[current_ta_index].mutex);
            printf(ANSI_YELLOW"TA %d from lab %s has been allocated to course %s for TAship no. = %d \n", current_ta_index, lab_thread[lab_id]->lab_name, course_thread[course_id]->course_name, taship_num);
            sleep(2);
            course_thread[course_id]->rand_seat = (rand()%course_thread[course_id]->course_max_slots) + 1;  // randomly allocates seats to course
            printf(ANSI_CYAN"Course %s has been allocated %d seats.\n", course_thread[course_id]->course_name, course_thread[course_id]->rand_seat);
            
            course_thread[course_id]->start_tute = 0;
            course_thread[course_id]->students_alloted = 0;

            pthread_mutex_lock(&course_thread[course_id]->mutex);   // signals to start student admission to course
            course_thread[course_id]->start_admission = 1;
            pthread_cond_broadcast(&course_thread[course_id]->sadmis);  
            pthread_mutex_unlock(&course_thread[course_id]->mutex);

            // while(course_thread[course_id]->students_alloted == 0){ // waits for atleast 1 student to be alloted and also checks if stop_courses is true
            //     // printf(ANSI_DEFAULT"waiting %s\n",course_thread[course_id]->course_name);
            //     if(!stop_courses){
            //         pthread_exit(NULL);
            //         exit(1);
            //     }
            //     pthread_cond_broadcast(&course_thread[course_id]->sadmis);  
            // }
            sleep(5);
            pthread_mutex_lock(&course_thread[course_id]->mutex);   // stops taking student admission to course
            course_thread[course_id]->start_admission = 0;
            pthread_mutex_unlock(&course_thread[course_id]->mutex);
            // starts Tutorials
            printf(ANSI_MAGENTA"Tutorial has started for Course %s with %d seats filled out of %d\n", course_thread[course_id]->course_name, course_thread[course_id]->students_alloted, course_thread[course_id]->rand_seat);
            int tute_time = rand()%5 + 1;
            sleep(tute_time);
            printf(ANSI_GREEN "TA %d from lab %s has completed the tutorial and left the course %s\n", lab_thread[lab_id]->curr_ta_index, lab_thread[lab_id]->lab_name, course_thread[course_id]->course_name);
            
            // signals all students to that tutorial is over
            pthread_mutex_lock(&course_thread[course_id]->mutex);
            course_thread[course_id]->start_tute = 1;
            pthread_cond_broadcast(&course_thread[course_id]->cond);
            pthread_mutex_unlock(&course_thread[course_id]->mutex);
            
            pthread_mutex_unlock(&lab_thread[lab_id]->fellow[current_ta_index].mutex);   
        }
    }
    pthread_mutex_lock(&course_thread[course_id]->mutex);
    course_thread[course_id]->available = 1;
    pthread_mutex_unlock(&course_thread[course_id]->mutex);
    printf(ANSI_RED"Course %s doesn't have any TA's eligible and is removed from course offerings\n",course_thread[course_id]->course_name);
    return NULL;
}


int main()
{
    printf("----------------------------------Welcome to the New Course Registeration Portal----------------------------------\n");
    stop_courses = true;
    srand(time(0));
    // printf("\n Enter number of students , number of labs and number of courses: ");
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    dup_students = num_students;
    if(num_courses == 0 || num_labs == 0 || num_students == 0)
    {
        printf("\nInvalid input. Please enter valid input.\n");
        return(0);
    }

    // printf("\n Enter course details:");
    for (int i = 0; i < num_courses; i++)
    {
        course_thread[i] = (struct course *)malloc(sizeof(struct course));
        scanf("%s", course_thread[i]->course_name);
        scanf("%f", &course_thread[i]->interestq);
        scanf("%d", &course_thread[i]->course_max_slots);
        scanf("%d", &course_thread[i]->ass_lab);

        pthread_cond_init(&course_thread[i]->sadmis, NULL);     // init condition variable for student admission
        pthread_cond_init(&course_thread[i]->cond, NULL);       // init condition variable for tutorial
        
        pthread_mutex_init(&course_thread[i]->mutex, NULL);       // init course mutex
        pthread_mutex_init(&course_thread[i]->allmut, NULL);      // init all mutex
        // init course attributes
        course_thread[i]->students_alloted = 0;
        course_thread[i]->start_tute = 0;
        course_thread[i]->current_lab_index = 0;
        course_thread[i]->start_admission = 0;
        if(course_thread[i]->ass_lab !=0)course_thread[i]->available = 0;
        else course_thread[i]->available = 1;

        for (int j = 0; j < course_thread[i]->ass_lab; j++)
        {
            scanf("%d", &course_thread[i]->labid[j]);
        }
    }

    // printf("\n Enter student details: ");
    for (int i = 0; i < num_students; i++)
    {
        student_thread[i] = (struct student *)malloc(sizeof(struct student));
        scanf("%f", &student_thread[i]->calibreq);
        scanf("%d", &student_thread[i]->cid[0]);
        scanf("%d", &student_thread[i]->cid[1]);
        scanf("%d", &student_thread[i]->cid[2]);
        scanf("%d", &student_thread[i]->time);
        student_thread[i]->status = -1;
        pthread_mutex_init(&student_thread[i]->mutex, NULL);       // init student mutex
    }

    // printf("\n Enter lab details: ");
    for (int i = 0; i < num_labs; i++)
    {
        lab_thread[i] = (struct lab *)malloc(sizeof(struct lab));
        scanf("%s", lab_thread[i]->lab_name);
        scanf("%d", &lab_thread[i]->num_fellow);
        scanf("%d", &lab_thread[i]->ta_quota);
        if(lab_thread[i]->num_fellow > 0){
            lab_thread[i]->available = 0;
        }
        else lab_thread[i]->available = 1;
        
        lab_thread[i]->curr_ta_index = 0;
        lab_thread[i]->finished_quota = 0;
    }

    for(int i = 0;i < num_labs;i++){
        pthread_mutex_init(&lab_thread[i]->mutex, NULL);        // init lab mutex
        for(int j = 0;j < lab_thread[i]->num_fellow;j++){
            lab_thread[i]->fellow[j].available = 0;
            pthread_mutex_init(&lab_thread[i]->fellow[j].mutex, NULL);      // init fellow mutex
        }
    }

    
    for(int i = 0; i < num_students; i++)
    {
        int *arg = malloc(sizeof(*arg));
        *arg = i;
        int err = pthread_create(&student_thread[i]->Student, NULL, student_enroll, arg);   // create student thread
        if(err != 0)perror("student thread create");
    }
    
    for(int i = 0; i < num_courses; i++){
        int *arg = malloc(sizeof(*arg));
        *arg = i;
        int err = pthread_create(&course_thread[i]->Courses, NULL, course_play, arg);       // create course thread
        if(err!= 0)perror("course thread create");
    }

    for(int i = 0;i < num_students;i++){
        pthread_join(student_thread[i]->Student, NULL);         // join student thread
    }
    
    return 0;
}
