# Q2 REPORT

This problem has 2 simulations, one is spectator and second is goals in match. So we create a thread of spectator `spec_thread` which is fed to `spec_sim` function to operate on it. We create a thread of goals `Goals` which is fed to `goal_sim` to operate on it.

## Spectator Simulation `spec_sim`

This can be further broken down to following subparts:

* Spectator arrives at stadium
* Spectator waits for a seat
* Spectator watches the match
* Spectator leaves (goes to exit gate)

### Spectator arrives at stadium

We make the spectator thread `spec_thread` sleep till the time of arrival.

```c
sleep(s.time);
printf(ANSI_DEFAULT "t=%ld : %s has reached the stadium\n", time_from_start(), s.name);
```

### Spectator waits for a seat

We use `sem_timedwait`(takes semaphore and timespec struct as argument and locks the semaphore). The timespec structure is used to store patience level of waiting spectator.

``` c
struct timespec tspec1, tspec2;
clock_gettime(CLOCK_REALTIME, &tspec1); // get current value of clock and stores it in variable
tspec1.tv_sec += s.patience;        // adds patience to the time

```

* `sem_timedwait` acts like a wait function but with a timer. If it gets `sem_post` before timeout, it returns 0.

* Home Fans can sit in Home and Neutral zone, Neutral Fans can sit in all 3 zones and Away Fans can only sit in Away Zone. So to accommodate the above situation, we create 3 semaphores:

  * HN for Home Fans

  * HNA for Neutral Fans

  * A for Away Fans

  * We initialize these semaphores in `init_sem()`function.

    ```c
    void init_sem(){    // initializes semaphores
        sem_init(&HN, 0, zoneH.seats + zoneN.seats);
        sem_init(&HNA, 0, zoneH.seats + zoneN.seats + zoneA.seats);
        sem_init(&A, 0, zoneA.seats);
    }
    ```

* **FOR HOME FANS:** 

  * We use `sem_timedwait` for semaphore `HN` and wait for patience limit to receive a post on it.
  * If time out takes place, the fan exits stadium via `exit_gate`
  * If a post is received, we lock `Home_lock` and `Neutral_lock` (mutex locks) to check the available capacity in Home and Neutral Zone. If seats are available in both, we choose a seat randomly, else allot the seat available in one zone.
  * After the seat is allotted, we update value of filled seats in that zone, and we use `sem_wait` on another semaphore, since if Home Zone seat is selected from Home or Neutral Zone pool  , we'll have to decrement  the available seat count in Home,Neutral, Away Zone pool as it includes Home Zone seats too. Hence we do `sem_wait(&HNA)`
  * We unlock the mutex locks and move to next step.
  * ***Similar procedure is followed for Neutral and away fans***

### Spectator watches the match

* We  use `pthread_cond_timedwait` which is wait function implemented based on timer (`spectating_time`). We store watch time `spectating_time` to a `timespec`variable.

* We use 2 conditional variables `Hgoal_cond` and `Agoal_cond`, protected by their respective mutex locks `Home_goal_lock` and `Away_goal_lock`. 

* For spectator of Type **'H'**:

  ```c
  pthread_mutex_lock(&Home_goal_lock);
  ll t0 = time_from_start();
  while (Home_goals < s.rage_lim && j != ETIMEDOUT)   // waits/sleeps thread till home goal condition broadcasted
  	j = pthread_cond_timedwait(&Hgoal_cond, &Home_goal_lock, &tspec2);      
  ll t1 = time_from_start();         
  pthread_mutex_unlock(&Home_goal_lock);
  ```

* We wait for signal broadcast on the CV from thread `Goals`, and check if time out took place or the goals by opponent team exceeded rage_limit of spectator.

* We even store the time during which spectator was waiting for signal, so that we can leave loop even if the rage_limit or timeout condition doesn't take place but the watch/spectating time is over.

  ```c
  if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finishes watch time
  {
  	s.seated = 0;
      printf(ANSI_GREEN "t=%ld : %s watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
  }
  else    // gets angry
  {
  	printf(ANSI_GREEN "t=%ld : %s is leaving due to poor performance of his team\n", time_from_start(), s.name);
      s.seated = 0;
  }
  ```

* ***Similar procedure is followed for Away Spectator, But for Neutral spectator we just make them sleep till their watch time and then they leave stadium(exit_gate)***

* For updating the vacated seats:

  ```c
  // updates zones capacity filled
  if (s.zone_alloted == 'N')      
  {
      pthread_mutex_lock(&Neutral_lock);
      zoneN.filled-=1;
      pthread_mutex_unlock(&Neutral_lock);
      sem_post(&HNA);
      sem_post(&HN);
  }
  else if(s.zone_alloted == 'A'){
      pthread_mutex_lock(&Away_lock);
      zoneA.filled-=1;
      pthread_mutex_unlock(&Away_lock);
      sem_post(&HNA);
      sem_post(&A);
  }
  else
  {
      pthread_mutex_lock(&Home_lock);
      zoneH.filled-=1;
      pthread_mutex_unlock(&Home_lock);
      sem_post(&HN);
      sem_post(&HNA);
  }
  ```

### Spectator Leaves

```c
s.exiting = 1;
printf(ANSI_YELLOW "t=%ld : %s is leaving for dinner\n" , time_from_start(), s.name);
printf(ANSI_MAGENTA"t=%ld : reached exit gate\n", time_from_start());
```

We print the message and leave the function.



## Goals Simulation `goal_sim`

We traverse `goal_chnc`array and sleep till next goal chance occurs.

```c
ll current_time = time_from_start(),time_to_wait = g[i].start_time - current_time;  // stores current time and wait time
if (time_to_wait)           // waits if wait time is not zero
sleep(time_to_wait);
```

* When goal scoring time takes place, we generate a threshold(float) between 0 and 1 for goal probability. If the probability exceeds the threshold, the goal is scored and corresponding conditional variable signals are broadcasted.

  ```c
  float max = (float)RAND_MAX;    // stores max value of random number generator
  float randn = (float)rand();    // stores random number
  float probability = randn / max; // stores probability of goal in float
  ```

  ```c
  if (probability <= g[i].prob)
   {
      char team[20] = "";
      if(g[i].type == 'H')strcpy(team, "Home");
      else strcpy(team, "Away");
  
  	printf(ANSI_CYAN "t=%ld : %s Team has scored their %s goal\n", time_from_start(), team, goals_count);
  // updates goal count and broadcasts goal condition
  if (g[i].type == 'A')
       {
       pthread_mutex_lock(&Away_goal_lock);
       Away_goals+=1;
       pthread_cond_broadcast(&Agoal_cond);
       pthread_mutex_unlock(&Away_goal_lock);          
        }
  else
        {
        pthread_mutex_lock(&Home_goal_lock);
        Home_goals+=1;
        pthread_cond_broadcast(&Hgoal_cond);
        pthread_mutex_unlock(&Home_goal_lock);
         }
     }
  else        // team missed goal chance
     {
        printf(ANSI_CYAN"t=%ld : Team %c missed the chance to score their %s goal\n", time_from_start(), g[i].type, goals_count);
     }
  ```



## Main

We take input, initialize CV, mutex locks, threads, semaphores and join the threads.

