# README

### Name: Parth Maradia, Roll no.: 2020111006
<br>

## To compile and run q1
```
$ gcc q1.c -lpthread
```
```
$ ./a.out
```

### Then enter the inputs in exact format.
<br>

## To compile and run q2
```
$ gcc q2.c -lpthread
```
```
$ ./a.out
```

### Then enter the inputs in exact format.