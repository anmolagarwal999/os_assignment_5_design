# Question 1
## Code Logic
Initially, we can make custom data types to store details about courses, students, TAs and labs. Each of these will also have a mutex attribute to enable thread safe access. 
Once this is over, program execution can begin with spawning as many as threads as there are students and then adding more threads for executing the routines of courses and labs.
Student threads are left joined to make sure program runs until all student threads finish execution. 
### Student routine
Initially each student will sleep for some time to mimic the time taken by them to choose a course. 
Then a flag will be maintained for each course to see if a student has finalised that said course.
Then, we iterate through the preferences to see the status of courses. 
If withdrawn, student moves on to next course, else students waits until course starts accepting students. 
Once course starts accepting students, the number of available slots decrements by one. 
Assuming that the coruse withdraws suddenly while the students waits, students will be moved to next preference. 
Then we introduce an atificial sleep again to mimic a student attending a tutorial and then the chance that a student picks the course will be randomly determined. 
This routine repeats until student gets a course or in the case where all possible seats are taken, student will be informed about the lack of allocation of course and student thread will terminate.
### Lab routine
Lab thread will just wait for all its TAs to exhaust their TAship quota and then increments its maximum number of TAships by 1 for the whole lab. 
### Course routine
The course routine iterates over all its eligible labs to find for TAs. Once it finds an eligible TA, it assigns them to the course.
A TA will be eligible only if they aren't already actively engaged in another course or have not exhausted their maximum TAships quota set by their lab. 
In the case where, no eligible TAs are found, the course can be withdrawn and thread will be terminated. 
Now, the eligible TAs get their number of TAships counter incremented and a random number of slots for students between set bounbdaries is opened and TA starts accepting students by broadcasting a signal to those student threads which are waiting. 
Then another artifical sleep is introduced to mimic the proceeding of a tutorial which is again conveyed to students by broadcasting a signal to the student threads. 
Once this tutorial ends, TA is relieved ending the course routine. 