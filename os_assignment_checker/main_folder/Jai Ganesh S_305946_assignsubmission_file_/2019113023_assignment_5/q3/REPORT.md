# Question 3
## Code Logic
There will be 2 files, namely server.cpp and client.cpp each mimicing server and client routines.
A random port will be chosen so that the server and client and communicate.
### Server routine
Server will be initialized with a number of threads which will be given as input. 
These threads will conditional wait on a queue which has all the incoming requests to the server.
Once all these are done, server's socket and its welcome socket will be setup and when server receives a request, it adds that request along with the client socket file descriptor in the queue.
Now workers, who will wait conditionally for the queue to get a new request. 
Once a request is pushed into queue, the queue's mutex will be locked to make it safe from threading
issue and then the command will be executed by a function and appropriate response will be sent 
along with ID of the thread which excuted the said command along with the ID of the request itself. 
Once this is done, connection to client will be closed.
This above said function will have a parser to seperate words by white space.
This function then checks for the operation requested, which can be
- insert
- delete
- update
- concat
- fetch

and will execute the operation on the global dictionary which has keys and corresponding values,
which will undergo changes accordingly from the descriptionsof the above commands (found in question PDF).
### Client routine
A new data type will be made for storing details of a request.
As mentioned in the question PDF, initially client will take an input to determine number of requests to perform. 
These threads spawned to handle the requests will be joined so that the program does not terminate before all requests are executed. 
Once the input is received to the client, it will be put on sleep for an amount of time specified by the input 
and then proceeds to connect to the server using the address which will be provided to it. 
Once connection request is successful, client will write to the server as to what command to execute 
and upon receiving an answer, client writes to output, 
closes its connection to server and terminates the thread. 