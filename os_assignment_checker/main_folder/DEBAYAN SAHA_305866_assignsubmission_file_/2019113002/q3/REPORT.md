## How to run code
For running the server go to `ser folder` 
Run: 
$ `g++ ser.cpp -o ser -pthread`
$ `./ser <number of worker threads in the thread pool>`
For running the client go to `cli folder` 
Run: 
$ ` g++ cli.cpp -o cli -pthread`
$ `./cli`

## Input format
Input in the client once the server has started 
The first line of input would be the total number of user requests `m`.
The next `m` lines contain description of the user requests
The commands have to be entered in following manner:
`<Time><cmd>`
`Time` in sec after which the request to connect to the server is to be made
`cmd` with appropriate arguments

## Client
* The client request acts as a thread, initially input is stored in a vector of integer which stores the time for sleep and vector of string to store the commands.
* Threads are initialized after taking the input.
* The connection between the clients and server starts after taking complete intput.
* In the `begin_process` function each thread captures a particular index and sends the given command to the server after sleeping for the given amount.
* In this way multiple requests are being made to the server simulating multiple clients.
* Corresponding result from the server is then sent back to the client.

## Server
* Server is put to sleep for 2 seconds to wait till the initialisation of the worker threads is completed.
* A semaphore is initialized to 0 so that all the threads goes on to wait to avoid the busy wait condition.
* A queue is declared to store the file descriptors of the different clients.
* As soon as a connection is made, the corresponding file descriptor is pushed into the queue (A mutex lock is present to make the queue thread safe) and the mutex is signalled.
* Each thread function waits on the mutex in an infinite while loop, as soon as it is awakened by the signal it pops the file descriptors from the queue and extracts the given commands out of it using the defined functions to a `struct input`.
* The `struct input`  is checked for the relevant operations and error handling is done to check if the commands are in order of `<cmd><arguments>`.
* Map is stored as an array in which empty array means that the key has not been initialised.
* Corresponding operations are done on the map by sending it to corresponding functions where there is mutex lock for each index to make sure it is thread safe.
* After the performing the given operations the result is sent back to the client.
* After a worker threads performs the task, it is put into a 2 sec sleep.