#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <pthread.h>
#include <semaphore.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 10
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

string keyArray[102];

queue <int> qu;
pthread_mutex_t qu_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t server_threads[102];
pthread_mutex_t keyArray_lock[102];
sem_t thread_sem;

struct input
{
   int key, key2;
   string command;
   string msg;
};

int isNumber(string s) //to check if the string contains number
{
    for (int i = 0; i < s.length(); i++)
        if (isdigit(s[i]) == false)
            return 0;
 
    return 1;
}

pair<input, int> removeDupWord(string str) // to split the input into separate words
{
    // Used to split string around spaces.
    struct input instruc;
    istringstream ss(str);
  
    string word; // for storing each word
  
    ss >> word;
    instruc.command = word;
    ss >> word;
    if(isNumber(word))
    {
        instruc.key = stoi(word);
    }
    else
        return {instruc, 0};
    
    if(instruc.command == "insert" || instruc.command == "update")
    {   
        ss >> word;
        instruc.msg = word;
    }
    else if(instruc.command == "concat")
    {
        ss >> word;
        if(isNumber(word))
        {   
            instruc.key2 = stoi(word);
        }
        else
            return {instruc, 0};

    }
    // cout<<instruc.time_towait<<instruc.command<<instruc.key<<instruc.msg;
    return {instruc, 1};
    
}

string insert_key(int key, string msg)
{
    string send_back;
    pthread_mutex_lock(&keyArray_lock[key]);
    if (keyArray[key] == "")
    {
        keyArray[key] = msg;
        send_back =  "Insertion successful";
    }
    else
    {
        send_back = "Key already exists";
    }
    pthread_mutex_unlock(&keyArray_lock[key]);
    return send_back;
}

string delete_key(int key)
{
    string send_back;
    pthread_mutex_lock(&keyArray_lock[key]);
    if (keyArray[key] != "")
    {
        keyArray[key] = "";
        send_back = "Deletion successful";
    }
    else
    {
        send_back = "No such key exists";
    }
    pthread_mutex_unlock(&keyArray_lock[key]);
    return send_back;
}

string update_key(int key, string msg)
{
    string send_back;
    pthread_mutex_lock(&keyArray_lock[key]);
    if (keyArray[key] != "")
    {
        keyArray[key] = msg;
        send_back = msg;
    }
    else
    {
        send_back = "Key does not exist";
    }
    pthread_mutex_unlock(&keyArray_lock[key]);
    return send_back;
}


string concat_key(int key, int key2)
{
    string send_back;
    pthread_mutex_lock(&keyArray_lock[key]);
    pthread_mutex_lock(&keyArray_lock[key2]);

    if (keyArray[key] == "" || keyArray[key2] == "")
    {
        send_back = "Concat failed as at least one of the keys does not exist";
    }
    else
    {
        string temp = keyArray[key] + keyArray[key2];
        string temp2 = keyArray[key2] + keyArray[key];
        keyArray[key] = temp;
        keyArray[key2] = temp2;
        send_back = keyArray[key2];
    }
    pthread_mutex_unlock(&keyArray_lock[key2]);
    pthread_mutex_unlock(&keyArray_lock[key]);
    return send_back;
}


string fetch_key(int key)
{
    string send_back;
    pthread_mutex_lock(&keyArray_lock[key]);
    if (keyArray[key] != "")
    {
        send_back = keyArray[key];
    }
    else
    {
        send_back = "Key does not exist";
    }
    pthread_mutex_unlock(&keyArray_lock[key]);
    return send_back;
}
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void* handle_connection(void *)
{
    while (true)
    {
        struct input instruc;
        sem_wait(&thread_sem);
        int received_num, sent_num;
        int ret_val = 1;
        pthread_mutex_lock(&qu_lock);
        if(qu.empty())
        {
            pthread_mutex_unlock(&qu_lock);
            continue;
        }
        int client_socket_fd = qu.front();
        qu.pop();
        pthread_mutex_unlock(&qu_lock);
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        if (ret_val <= 0)
        {
            printf("Server could not read msg sent from client\n");
            close(client_socket_fd);
            return NULL;
        }

        int check;
        tie(instruc, check) = removeDupWord(cmd);
        if(check == 0) 
        { 
            cout<<"Client sent : Wrong input\n"; 
            string msg_to_send_back = "Wrong input" ; 
            int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back); 
            close(client_socket_fd);
            return NULL;
        }


        cout << "Client sent : " << cmd << endl;
        string msg_to_send_back;
        uint a = pthread_self();
        string thread_name = to_string(a);
        msg_to_send_back = thread_name + ":";
        if (instruc.command == "insert")
            msg_to_send_back = msg_to_send_back + insert_key(instruc.key, instruc.msg);

        else if (instruc.command == "delete")
            msg_to_send_back = msg_to_send_back + delete_key(instruc.key);

        else if (instruc.command == "update")
            msg_to_send_back = msg_to_send_back + update_key(instruc.key, instruc.msg);

        else if (instruc.command == "concat")
            msg_to_send_back = msg_to_send_back + concat_key(instruc.key, instruc.key2);

        else if (instruc.command == "fetch")
            msg_to_send_back = msg_to_send_back + fetch_key(instruc.key);
        else
        {
            cout<<"Client sent : Wrong input\n";
            msg_to_send_back = "Wrong input";
        }

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        sleep(2);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
        }
    
        close(client_socket_fd);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    char fer[100];
    if(argc < 2)
    {
        sprintf(fer, "Insufficient arguments supplied\n");
        write(1, fer, strlen(fer));
        return 1;
    }
    if(argc > 2)
    {
        sprintf(fer, "Extra arguments supplied\n");
        write(1, fer, strlen(fer));
        return 1;
    }
    // int n = argv[2][0]-48;
    int len = strlen(argv[1]);
    int n = 0;
    int ten = 1;
    for(int i = 0; i<len; i++)
    {
        int x = argv[1][len-i-1]-48;
        n = x*ten+n;
        ten = ten*10;
    }
    // cout<<n;
    sem_init(&thread_sem, 0, 0);
    for(int i=0; i<=100; i++)
    {
        keyArray_lock[i] = PTHREAD_MUTEX_INITIALIZER; //Mutex for dictionary
    }
    for(int i=0;i<n;i++)
    {
        pthread_create(&server_threads[i], NULL, handle_connection, NULL);
    }
    sleep(2);

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);

        pthread_mutex_lock(&qu_lock);

        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        qu.push(client_socket_fd);
        sem_post(&thread_sem);
        pthread_mutex_unlock(&qu_lock);
        // handle_connection(client_socket_fd);
    }

    close(wel_socket_fd);
    return 0;
}