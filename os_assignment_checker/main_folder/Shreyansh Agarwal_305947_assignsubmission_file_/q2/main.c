#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

#define RED printf("\033[0;31m");
#define YELLOW printf("\033[0;33m");
#define GREEN printf("\033[0;32m");
#define WHITE printf("\033[0;37m");
#define BLACK printf("\033[0;34m");
#define PURPLE printf("\033[0;35m");

int Globaltime;
int capH, capA, capN, specTime, goalsH, goalsA;

pthread_mutex_t mutexTime, mutexZone, mutexGoals;
pthread_cond_t condSeat, condGoals;
time_t startTime;

struct person
{
    char name[40];
    char team;
    int reachTime;
    int patTime;
    int goals;
    int seatRecievedAt;
    char seatZone;
    pthread_mutex_t personMutex;
    pthread_t personThread;
};

struct goals
{
    int numChances;
    struct goal *goalsData;
};

struct goal
{
    int time;
    char team;
    float chance;
};

void *personFunction(void *person)
{
    struct person *p = (struct person *)person;
    sleep(p->reachTime);
    pthread_mutex_lock(&mutexTime);
    time_t t = time(NULL) - startTime;
    RED
        printf("t = %ld: %s has reached the stadium\n", t, p->name);
    pthread_mutex_unlock(&mutexTime);
    while (1)
    {
        if (p->team == 'H')
        {
            pthread_mutex_lock(&mutexZone);
            time_t t = time(NULL) - startTime;
            if (capH > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone H\n", time(NULL) - startTime, p->name);
                capH--;
                p->seatZone = 'H';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            if (capN > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone N\n", time(NULL) - startTime, p->name);
                capN--;
                p->seatZone = 'N';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            pthread_mutex_unlock(&mutexZone);
        }
        else if (p->team == 'A')
        {
            pthread_mutex_lock(&mutexZone);
            if (capA > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone A\n", time(NULL) - startTime, p->name);
                capA--;
                p->seatZone = 'A';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            pthread_mutex_unlock(&mutexZone);
        }
        else if (p->team == 'N')
        {
            pthread_mutex_lock(&mutexZone);
            time_t t = time(NULL) - startTime;
            if (capN > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone N\n", time(NULL) - startTime, p->name);
                capN--;
                p->seatZone = 'N';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            if (capH > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone H\n", time(NULL) - startTime, p->name);
                capH--;
                p->seatZone = 'H';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            if (capA > 0)
            {
                GREEN
                printf("t = %ld: %s has got a seat in zone A\n", time(NULL) - startTime, p->name);
                capA--;
                p->seatZone = 'A';
                p->seatRecievedAt = time(NULL) - startTime;
            }
            if (p->seatRecievedAt != -1)
            {
                pthread_mutex_unlock(&mutexZone);
                break;
            }
            pthread_mutex_unlock(&mutexZone);
        }
        if (p->seatRecievedAt == -1)
        {
            pthread_mutex_lock(&mutexZone);
            struct timeval tv;
            struct timespec ts;
            int timeInMs = (p->patTime + p->reachTime - (time(NULL) - startTime) + 1) * 1000;
            gettimeofday(&tv, NULL);
            ts.tv_sec = time(NULL) + timeInMs / 1000;
            ts.tv_nsec = tv.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
            ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
            ts.tv_nsec %= (1000 * 1000 * 1000);
            int ret = pthread_cond_timedwait(&condSeat, &mutexZone, &ts);
            if (ret == 0)
            {
                pthread_mutex_unlock(&mutexZone);
                continue;
            }
            else
            {
                pthread_mutex_unlock(&mutexZone);
                if (time(NULL) - startTime > p->reachTime + p->patTime)
                {
                    YELLOW
                    printf("t = %ld: %s couldn\'t get a seat\n", time(NULL) - startTime, p->name);
                    return;
                }
            }
        }
    }
    if (p->seatRecievedAt != -1)
    {
        if (p->team == 'H')
        {
            while (p->goals > goalsA)
            {
                pthread_mutex_lock(&mutexGoals);
                struct timeval tv;
                struct timespec ts;
                int timeInMs = (specTime + p->seatRecievedAt - (time(NULL) - startTime)) * 1000;
                gettimeofday(&tv, NULL);
                ts.tv_sec = time(NULL) + timeInMs / 1000;
                ts.tv_nsec = tv.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
                ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
                ts.tv_nsec %= (1000 * 1000 * 1000);
                int ret = pthread_cond_timedwait(&condGoals, &mutexGoals, &ts);
                if (ret == 0)
                {
                    pthread_mutex_unlock(&mutexGoals);
                    continue;
                }
                else
                {
                    pthread_mutex_unlock(&mutexGoals);
                    break;
                }
            }
            if (goalsA >= p->goals)
            {
                YELLOW
                printf("t = %ld: %s is leaving due to bad performance of his team\n", time(NULL) - startTime, p->name);
                return;
            }
        }
        if (p->team == 'A')
        {
            while (p->goals > goalsH)
            {
                pthread_mutex_lock(&mutexGoals);
                struct timeval tv;
                struct timespec ts;
                int timeInMs = (specTime + p->seatRecievedAt - (time(NULL) - startTime)) * 1000;
                gettimeofday(&tv, NULL);
                ts.tv_sec = time(NULL) + timeInMs / 1000;
                ts.tv_nsec = tv.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
                ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
                ts.tv_nsec %= (1000 * 1000 * 1000);
                int ret = pthread_cond_timedwait(&condGoals, &mutexGoals, &ts);
                if (ret == 0)
                {
                    pthread_mutex_unlock(&mutexGoals);
                    continue;
                }
                else
                {
                    pthread_mutex_unlock(&mutexGoals);
                    break;
                }
            }
            if (goalsH >= p->goals)
            {
                YELLOW
                printf("t = %ld: %s is leaving due to bad performance of his team\n", time(NULL) - startTime, p->name);
                return;
            }
        }
        if (p->team == 'N')
        {
            sleep(specTime);
        }
        // sleep(specTime);
        pthread_mutex_lock(&mutexZone);
        if (p->seatZone == 'H')
        {
            capH++;
            RED
                printf("t = %ld: %s has left zone H\n", time(NULL) - startTime, p->name);
        }
        else if (p->seatZone == 'N')
        {
            capN++;
            RED
                printf("t = %ld: %s has left zone N\n", time(NULL) - startTime, p->name);
        }
        else if (p->seatZone == 'A')
        {
            capA++;
            RED
                printf("t = %ld: %s has left zone A\n", time(NULL) - startTime, p->name);
        }
        pthread_cond_broadcast(&condSeat);
        pthread_mutex_unlock(&mutexZone);
    }
}

void *goalFunction(void *args)
{
    struct goals *Goals = (struct goals *)args;
    for (int i = 0; i < Goals->numChances; i++)
    {
        sleep(Goals->goalsData[i].time - Goals->goalsData[i - 1].time);
        int prob = Goals->goalsData[i].chance * 100;
        int num = rand() % 100;
        if (num < prob)
        {
            BLACK
            printf("t = %ld: Team %c has scored their %d goal\n", time(NULL) - startTime, Goals->goalsData[i].team,
                   Goals->goalsData[i].team == 'H' ? goalsH + 1: goalsA + 1);
            pthread_mutex_lock(&mutexGoals);
            if (Goals->goalsData[i].team == 'H')
                goalsH++;
            else
                goalsA++;
            pthread_cond_broadcast(&condGoals);
            pthread_mutex_unlock(&mutexGoals);
        }
        else
        {
            PURPLE
            printf("t = %ld: Team %c has missed a chance to score their %d goal\n", time(NULL) - startTime, Goals->goalsData[i].team, Goals->goalsData[i].team == 'H' ? goalsH + 1: goalsA + 1);
        }
    }
}
int main()
{
    srand(time(NULL));
    pthread_mutex_init(&mutexTime, NULL);
    pthread_mutex_init(&mutexZone, NULL);
    pthread_mutex_init(&mutexGoals, NULL);
    pthread_cond_init(&condSeat, NULL);
    pthread_cond_init(&condGoals, NULL);
    Globaltime = 0;
    pthread_t goalThread;
    int numGrp;
    scanf("%d %d %d", &capH, &capA, &capN);
    scanf("%d", &specTime);
    scanf("%d", &numGrp);
    int numPers[numGrp];
    struct person **persons = malloc(numGrp * sizeof(struct person *));
    for (int i = 0; i < numGrp; i++)
    {
        scanf("%d", &numPers[i]);
        persons[i] = malloc(numPers[i] * sizeof(struct person));
        for (int j = 0; j < numPers[i]; j++)
        {
            scanf("%s %c %d %d %d", persons[i][j].name, &persons[i][j].team, &persons[i][j].reachTime, &persons[i][j].patTime, &persons[i][j].goals);
            persons[i][j].seatRecievedAt = -1;
        }
    }
    struct goals *GOALS = malloc(sizeof(struct goals));
    scanf("%d", &GOALS->numChances);
    GOALS->goalsData = malloc(GOALS->numChances * sizeof(struct goal));
    for (int i = 0; i < GOALS->numChances; i++)
    {
        scanf("\n%c %d %f", &GOALS->goalsData[i].team, &GOALS->goalsData[i].time, &GOALS->goalsData[i].chance);
    }
    startTime = time(NULL);
    if (pthread_create(&goalThread, NULL, goalFunction, (void *)GOALS) != 0)
    {
        printf("Error creating thread\n");
        exit(1);
    }
    for (int i = 0; i < numGrp; i++)
    {
        for (int j = 0; j < numPers[i]; j++)
        {
            if (pthread_create(&persons[i][j].personThread, NULL, personFunction, (void *)&persons[i][j]) != 0)
            {
                printf("Error creating thread\n");
                exit(1);
            }
            pthread_mutex_init(&persons[i][j].personMutex, NULL);
        }
    }
    pthread_join(goalThread, NULL);
    for (int i = 0; i < numGrp; i++)
    {
        for (int j = 0; j < numPers[i]; j++)
        {
            pthread_join(persons[i][j].personThread, NULL);
            pthread_mutex_destroy(&persons[i][j].personMutex);
        }
        free(persons[i]);
    }
    free(persons);
    pthread_cond_destroy(&condSeat);
    pthread_cond_destroy(&condGoals);
    pthread_mutex_destroy(&mutexTime);
    pthread_mutex_destroy(&mutexGoals);
    pthread_mutex_destroy(&mutexZone);
    return 0;
}
/*
2 1 2
3
2
3
Vibhav N 3 2 -1
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4
*/



/*
2 1 2
8
1
1
Vibhav H 3 2 2
1
A 5 1
*/