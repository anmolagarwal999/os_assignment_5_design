compile using 
```
make
```

Run using 
```
./a.out
```

Each person is assigned a thread, it sleeps till the time he arrives to the stadium. Upon reaching he looks for seat in the assigned zones.
If there is a seat available, he takes it and waits for spec time. If not he waits till a seat is emptited before his patience runs out and either gets a seat or leaves the stadium. While watching the match the thread also waits on the goal signal to respond if the goals are more than he can see

There is a goal thread which randomly sees if there is a goal and broadcasts to all the waiting threads.
