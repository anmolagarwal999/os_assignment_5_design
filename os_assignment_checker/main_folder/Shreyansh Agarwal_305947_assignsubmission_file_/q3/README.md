compile by 
```
make
```

Run by
```
./server <No of worker threads>
./client
```

The response is sent by the server to the client for each query. All error handling is also handled by the server.
When the server program starts it creates n threads, all the threads wait for a task to be assigned to them. Whenever a new client connects, the main thread accepts the connection and pushes it to the task queue and broadcasts it to all threads. Whichever thread wakes first, gets the lock and pops the task from the queue and processes it and sends a response to the client. 
The data is stored in a vector of string and whenever the i-th entry is manipulated the corresponding lock is accquired
and the appropriate response is sent to the client

The client just makes m threads and each threads sleeps for the assigned time and then sends the request to the server and prints the response on the terminal 
