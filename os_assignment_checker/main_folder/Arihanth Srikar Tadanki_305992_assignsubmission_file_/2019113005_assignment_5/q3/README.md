# Multithreaded client and server

Compiling and running the code:

First compile and run the server followed by the client.

```bash
g++ server.cpp -o server -lpthread
./server <no_of_threads>
```

```bash
g++ client.cpp -o client -lpthread
./client < testcases.txt
```

Compiling for mac users:

```bash
clang++ -std=c++11 -stdlib=libc++ server.cpp -o server -lpthread
clang++ -std=c++11 -stdlib=libc++ client.cpp -o client -lpthread
```

## Note

Some modifications were made to run on mac and haven't tested on ubuntu. For example, in `server.cpp`, `bind` was changed to `::bind`.

## Variables

Created some data structures to pass as arguments or to compactly store data.

- `all_client_locks[MAX_CLIENTS]` is an array of mutex locks for each client thread.

- `client_sockets_lock` is a mutex lock for modifying the client sockets.

- `client_sockets` is a queue that contains all the clients that are then passed to the `handle_connection` function.

- `server_dict[MAX_CLIENTS]` is the container that stores mappings of unique keys to values.

- `client_sem` is a semaphore that stores the client requests that need to be handled.

## Logic

### Client

We provide input to the client. They send the commands to execute to the server as a request. Each request is a thread that sleeps for a specified amout of time before contacting the server. The output of the command sent is printed by the client.

### Server

The server does all the processing of the requests from the client. Each request is run as a thread. Initially, we declare `m` number of threads to be present in the server threadpool which is the maximum number of requests it can parallelly process. When the client connects with the server, ut allocates a socket file descriptor and pushes into the `client_sockets` queue and the `client_sem` semaphore is incremented. Every spawned thread waits for the semaphore. Once posted, the file descriptor is popped from the queue and processed by `handle_connection`.
