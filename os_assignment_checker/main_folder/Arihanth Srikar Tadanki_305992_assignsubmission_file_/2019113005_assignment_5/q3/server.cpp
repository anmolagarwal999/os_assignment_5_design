#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>
#include <semaphore.h>
#include <queue>
#include <sstream>
#include <thread>
using namespace std;
/////////////////////////////

// Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 101
#define PORT_ARG 8001
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

const int initial_msg_len = 256;

pthread_mutex_t all_client_locks[MAX_CLIENTS];
pthread_mutex_t client_sockets_lock;
pthread_t *client_threads;
string server_dict[MAX_CLIENTS];
sem_t client_sem;
queue<int> client_sockets;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    // debug(output);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // debug(s.length());
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void handle_connection(int client_socket_fd)
{
    // int client_socket_fd = *((int *)client_socket_fd_ptr);
    //####################################################

    int received_num, sent_num;

    /* read message from client */
    int ret_val = 1;

    while (true)
    {
        string cmd;
        tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
        ret_val = received_num;
        // debug(ret_val);
        printf("Read something\n");
        if (ret_val <= 0)
        {
            // perror("Error read()");
            printf("Server could not read msg sent from client\n");
            goto close_client_socket_ceremony;
        }
        cout << "Client sent : " << cmd << endl;
        if (cmd == "exit")
        {
            cout << "Exit pressed by client" << endl;
            goto close_client_socket_ceremony;
        }
        string msg_to_send_back = "Ack: " + cmd;

        string s;
        stringstream ss(cmd);
        vector<string> words; // space-separated words sent by client
        char delim = ' ';
        while (getline(ss, s, delim)) // stream from the string
            words.pb(s);

        int i1 = stoi(words[1]);
        pthread_mutex_lock(&all_client_locks[i1]);
        if (words[0] == "concat")
        {
            string string1 = server_dict[i1];
            int i2 = stoi(words[2]);
            pthread_mutex_lock(&all_client_locks[i2]);
            string string2 = server_dict[i2];

            if (!string1.empty() && !string2.empty())
            {
                server_dict[i2] = string2 + string1;
                server_dict[i1] = string1 + string2;
                msg_to_send_back = string2 + string1;
            }
            else
                msg_to_send_back = "Concat failed as at least one of the keys does not exist";
            pthread_mutex_unlock(&all_client_locks[i2]);
        }
        else if (words[0] == "fetch")
        {
            if (!server_dict[i1].empty())
                msg_to_send_back = server_dict[i1];
            else
                msg_to_send_back = "Key does not exist";
        }
        else if (words[0] == "update")
        {
            if (!server_dict[i1].empty())
            {
                server_dict[i1] = words[2];
                msg_to_send_back = server_dict[i1];
            }
            else
                msg_to_send_back = "Key does not exist";
        }
        else if (words[0] == "delete")
        {
            if (server_dict[i1].empty())
                msg_to_send_back = "No such key exists";
            else
            {
                server_dict[i1].clear();
                msg_to_send_back = "Deletion successful";
            }
        }
        else if (words[0] == "insert")
        {
            if (!server_dict[i1].empty())
                msg_to_send_back = "Key already exists";
            else
            {
                msg_to_send_back = "Insertion successful";
                server_dict[i1] = words[2];
            }
        }
        pthread_mutex_unlock(&all_client_locks[i1]);

        sleep(2);

        stringstream st;
        auto myid = std::this_thread::get_id();
        st << myid;
        msg_to_send_back = " : " + st.str() + " : " + msg_to_send_back;
        cout << msg_to_send_back << endl;

        ////////////////////////////////////////
        // "If the server write a message on the socket and then close it before the client's read. Will the client be able to read the message?"
        // Yes. The client will get the data that was sent before the FIN packet that closes the socket.

        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        // debug(sent_to_client);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
            break;
        }
    }

close_client_socket_ceremony:
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    // return NULL;
}

void *worker_function(void *args)
{
    int client_socket_fd = -1;
    for (;;)
    {
        sem_wait(&client_sem); // wait for client_sockets queue elements

        pthread_mutex_lock(&client_sockets_lock);
        if (client_sockets.empty() == false)
        {
            client_socket_fd = client_sockets.front();
            client_sockets.pop();
        }
        pthread_mutex_unlock(&client_sockets_lock);

        if (client_socket_fd == -1)
            continue;

        handle_connection(client_socket_fd);
        client_socket_fd = -1;
    }

    return NULL;
}

int main(int argc, char *argv[])
{
    int num_threads = atoi(argv[1]);
    client_threads = (pthread_t *)malloc(sizeof(pthread_t) * num_threads);
    for (int i = 0; i < MAX_CLIENTS; i++)
    {
        server_dict[i] = "";
        pthread_mutex_init(&all_client_locks[i], NULL);
    }

    int i, j, k, t, n;

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    pthread_mutex_init(&client_sockets_lock, NULL); // lock before operating on the queue
    sem_init(&client_sem, 0, 0);                    // init to 0 client requests
    for (int i = 0; i < num_threads; i++)
        pthread_create(&client_threads[i], NULL, worker_function, NULL);

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact
    from a client process running on an arbitrary host
    */
    // get welcoming socket
    // get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    // CHECK WHY THE CASTING IS REQUIRED
    if (::bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */

    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client.
        */
        // accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        pthread_mutex_lock(&client_sockets_lock);
        client_sockets.push(client_socket_fd);
        pthread_mutex_unlock(&client_sockets_lock);
        sem_post(&client_sem);
    }

    free(client_threads);
    close(wel_socket_fd);
    return 0;
}