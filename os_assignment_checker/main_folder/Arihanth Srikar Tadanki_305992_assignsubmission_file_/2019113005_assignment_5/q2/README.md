# The Clasico Experience

Compiling and running the code - contains a makefile so run:

```bash
make
./a.out < testcases.txt
```

## Variables

- `person` contains all information corresponding to an individual.

- `team` contains all information corresponding to a team.

- `arg_struct` is a struct that contains all the parameters and is passed as a void pointer.

- We have 3 semaphores, one for each zone - `a_zone`, `h_zone`, `n_zone`.

- `goals_lock` is a mutex lock and `goals_changed` is the consitional variable that signals that a goal was scored. The goals of each team is stored in `goals`.

- `person_zone_lock` is the mutex lock and `person_moved` is the conditional varibale that signals a change in zone. `person_in_zone` keeps track of the zone of each individual.

## Logic

We take input and store it in our people and team structures. We create 2 types of threads - one for each person and one for each team. The persons thread manages the entry and exit whereas the team thread manages the number of goals scored. We dont lazy wait on any of the threads and have conditions to wake them up.

### Simulate the entry and exit

- This is handled by the `person_motion` function.
- The person thread calls this function first which sleeps until the entry time of the person and then calls the `allocate_seat` functions.
- It waits for the thread to signal `person_moved`.
- If the person did not receive this signal within the patience period then he did not get a seat in any of the zones and proceeds to exit the simulation.
- We then update the respective semaphore.

### Simulate seat allocation

- This is hanlded by `seat_allocation`.
- Based on the type of fan or the teams the person supports, we look for free spots in the respective zones.
- We create new threads, call the respective functions `search_in_A`, `search_in_H`, and `search_in_N`, and wait for them to complete.

### Simulate people watching the game

- This is handled by `watching_game`.
- Here we look at the type of fans they are and the teams they support and accordingly check if they are enraged by the opponent team's goals.
- We basically wait unitl they are either enraged or till the game ends. When either happens, the person leaves the simulation.

### Simulating goals scored by teams

- This is handled by `score_goals`.
- These threads are only woken up after the time that they can try to score a goal.
- We randomly score a goal based on the probablity that the team can score.
- Once a team scores a goal we much update the other threads that this event took place. We do this by broadcasting to whatever thread was waiting on `goals_changed`.
