#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#define NUM_OF_SEMAPHORES 3
#define PERSON_NAME_SIZE 15
#define LAB_NAME_SIZE 15
#define PTHREAD_COND_SIZE sizeof(pthread_cond_t)
#define PTHREAD_MUTEX_SIZE sizeof(pthread_mutex_t)
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct person
{
    int id;
    int reach_time;
    int group_no;
    int no_of_sems;
    int no_of_goals;
    int patience;
    char name[PERSON_NAME_SIZE];
    char sem_types[NUM_OF_SEMAPHORES];
    char fan_type;
} person;
person *people;

typedef struct arg_struct
{
    person arg_person;
    char zone;
} args;
args *team_args;

typedef struct team
{
    int id;
    int no_of_chances;
    int *prev_chance;
    float *goal_prob;
    char team_type;
} team;
team *teams;

pthread_t *people_thread, *teams_thread;
sem_t h_zone, a_zone, n_zone;
int h_capacity, a_capacity, n_capacity, spectating_time, num_people = 0;

// keeps track of goals of every team
int goals[2];
pthread_cond_t goals_changed[2];
pthread_mutex_t goals_lock[2];

// keeps track of the zones a person is in
char *person_in_zone;
pthread_mutex_t *person_zone_lock;
pthread_cond_t *person_moved;

int char_cmp(char *input_char, char check_char[])
{
    char temp[2];
    temp[0] = *(char *)input_char;
    temp[1] = '\0';
    return strcmp(temp, check_char);
}

void *search_in_N(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&n_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'N'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone N\n" RESET, cur_person.name);
        }
        else
            sem_post(&n_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_A(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&a_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'A'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone A\n" RESET, cur_person.name);
        }
        // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
        else
            sem_post(&a_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_H(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&h_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT) // patience runs out
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'H'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone H\n" RESET, cur_person.name);
        }
        else
            sem_post(&h_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *simulate_person_in_game(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int type_of_fan = -1;
    if (char_cmp(&cur_person.fan_type, "A") == 0) // A fan => follow goals of the Home team
        type_of_fan = 0;
    else if (char_cmp(&cur_person.fan_type, "H") == 0) // H fan => follow goals of the Away team
        type_of_fan = 1;
    if (type_of_fan == -1) // neutral => doesn't get enraged
        return NULL;

    pthread_mutex_lock(&goals_lock[type_of_fan]);
    while (goals[type_of_fan] < cur_person.no_of_goals) // wait till opposing team goals doesn't enrage
    {
        if (goals[type_of_fan] >= 0)
            pthread_cond_wait(&goals_changed[type_of_fan], &goals_lock[type_of_fan]);
        else
            break;
    }
    if (goals[type_of_fan] >= cur_person.no_of_goals)
        printf(RED "%s is leaving due to bad performance of his team\n" RESET, cur_person.name);
    pthread_mutex_unlock(&goals_lock[type_of_fan]);
    pthread_cond_broadcast(&person_moved[cur_person.id]);

    return NULL;
}

void *sear(void *input_person)
{
    person cur_person = *(person *)input_person;
    int id = cur_person.id;

    pthread_t threads[NUM_OF_SEMAPHORES]; // n, a, h
    if (char_cmp(&cur_person.fan_type, "N") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "A") == 0)
    {
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "H") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }

    return NULL;
}

void *person_motion(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;
    int id = cur_person.id, rt = cur_person.reach_time;

    sleep(rt); // enters at reach_time
    printf(BLU "%s has reached the stadium\n" RESET, cur_person.name);

    // simulate seat allocation
    pthread_t thread;
    pthread_create(&thread, NULL, sear, &arguments);
    pthread_join(thread, NULL);

    // did not find any seat
    int isFound_seat = 0;
    pthread_mutex_lock(&person_zone_lock[id]);
    // D = Zoneless, G = Left
    if (char_cmp(&person_in_zone[id], "D") != 0 && char_cmp(&person_in_zone[id], "G") != 0)
        isFound_seat = 1;
    pthread_mutex_unlock(&person_zone_lock[id]);

    if (isFound_seat)
    {
        struct timespec ts; // spectating time
        clock_gettime(CLOCK_REALTIME, &ts);

        // found a seat and watching the game
        pthread_create(&thread, NULL, simulate_person_in_game, &arguments);

        pthread_mutex_lock(&person_zone_lock[id]);
        ts.tv_sec += spectating_time;
        // signal conditional variable when person wants to leave
        if (pthread_cond_timedwait(&person_moved[id], &person_zone_lock[id], &ts) == ETIMEDOUT)
            printf(RED "%s watched the match for %d seconds and is leaving\n" RESET, cur_person.name, spectating_time);
        pthread_mutex_unlock(&person_zone_lock[id]);

        // person left, increment semaphore
        pthread_mutex_lock(&person_zone_lock[id]);
        if (char_cmp(&person_in_zone[id], "H") == 0)
            sem_post(&h_zone);
        else if (char_cmp(&person_in_zone[id], "A") == 0)
            sem_post(&a_zone);
        else if (char_cmp(&person_in_zone[id], "N") == 0)
            sem_post(&n_zone);
        person_in_zone[id] = 'G';
        pthread_mutex_unlock(&person_zone_lock[id]);

        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }
    else
    {
        printf(BLU "%s did not find a seat in any of the zones\n" RESET, cur_person.name);
        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }

    return NULL;
}

void *simulate_team(void *input_team)
{
    team cur_team = *(team *)input_team;

    int i;
    for (i = 0; i < cur_team.no_of_chances; i++)
    {
        // probability of team scoring
        float x = cur_team.goal_prob[i] - ((float)rand() / (float)(RAND_MAX / 1.0));
        int id = cur_team.id;
        sleep(cur_team.prev_chance[i]); // sleep till next chance to score goal

        if (x >= 0) // gloal scored
        {
            pthread_mutex_lock(&goals_lock[id]);
            goals[id]++;
            printf(CYN "Team %c has scored thier goal %d\n" RESET, cur_team.team_type, goals[id]);
            pthread_mutex_unlock(&goals_lock[id]);
        }
        else // gloal missed
            printf(CYN "Team %c missed their chance to score their goal %d\n" RESET, cur_team.team_type, goals[id] + 1);
        pthread_cond_broadcast(&goals_changed[id]); // broadcast after every chance
    }
    return NULL;
}

void allocate_memory(int isDatastruct)
{
    if (isDatastruct == 1) // init people, teams, args
    {
        people = malloc(sizeof(person) * 1);
        team_args = malloc(sizeof(args) * 1);
        teams = malloc(sizeof(team) * 2);
        int i;
        for (i = 0; i < 2; i++)
        {
            teams[i].goal_prob = malloc(sizeof(float) * 1);
            teams[i].prev_chance = malloc(sizeof(int) * 1);
        }
    }
    else if (isDatastruct == 0) // init rest
    {
        person_moved = malloc(sizeof(pthread_cond_t) * num_people);
        person_in_zone = malloc(sizeof(char) * num_people);
        person_zone_lock = malloc(sizeof(pthread_mutex_t) * num_people);
        people_thread = malloc(sizeof(pthread_t) * num_people);
        teams_thread = malloc(sizeof(pthread_t) * 2);
    }
}

void take_input()
{
    int i, ii;
    int num_groups, num_in_group;

    scanf("%d %d %d", &h_capacity, &a_capacity, &n_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    for (i = 1; i < num_groups + 1; i++)
    {
        scanf("%d", &num_in_group);

        for (ii = 0; ii < num_in_group; ii++)
        {
            char name[PERSON_NAME_SIZE], support_teams;
            int reach_time, patience, no_of_goals;
            people = realloc(people, sizeof(person) * (num_people + 1));
            team_args = realloc(team_args, sizeof(args) * (num_people + 1));

            scanf("%s %c %d %d %d", name, &support_teams, &reach_time, &patience, &no_of_goals);
            strcpy(people[num_people].name, name);
            people[num_people].fan_type = support_teams;
            people[num_people].reach_time = reach_time;
            people[num_people].patience = patience;
            people[num_people].no_of_goals = no_of_goals;
            people[num_people].id = num_people;
            people[num_people].group_no = i;
            team_args[num_people].arg_person = people[num_people];

            num_people++;
        }
    }

    int goal_scoring_chances;
    scanf("%d\n", &goal_scoring_chances);

    for (i = 0; i < goal_scoring_chances; i++)
    {
        int prev_time[2] = {0, 0}, inp_time, j = -1;
        char inp_team;
        scanf("%c", &inp_team);
        if (char_cmp(&inp_team, "H") == 0)
            j = 0;
        else if (char_cmp(&inp_team, "A") == 0)
            j = 1;

        if (j == -1)
        {
            printf("Incorrect Team Name Entered\n");
            exit(1);
        }

        teams[j].goal_prob = realloc(teams[j].goal_prob, sizeof(float) * (teams[j].no_of_chances + 1));
        teams[j].prev_chance = realloc(teams[j].prev_chance, sizeof(int) * (teams[j].no_of_chances + 1));

        int chances = teams[j].no_of_chances;
        teams[j].no_of_chances++;

        float goal_prob;
        scanf("%d %f\n", &inp_time, &goal_prob);
        teams[j].goal_prob[chances] = goal_prob;

        // time from the previous chance to goal
        teams[j].prev_chance[chances] = inp_time - prev_time[j];
        prev_time[j] = inp_time;
    }
}

int main(int argc, char *argv[])
{
    allocate_memory(1);
    teams[0].team_type = 'H';
    teams[1].team_type = 'A';
    int i;
    for (i = 0; i < 2; i++)
    {
        teams[i].id = i;
        teams[i].no_of_chances = 0;
    }
    take_input();
    allocate_memory(0);

    pthread_mutex_init(&goals_lock[0], NULL);
    pthread_mutex_init(&goals_lock[1], NULL);
    pthread_cond_init(&goals_changed[0], NULL);
    pthread_cond_init(&goals_changed[1], NULL);
    goals[0] = 0;
    goals[1] = 0;

    // initialize the semaphores corresponding to the number of seats in each zone
    sem_init(&h_zone, 0, h_capacity);
    sem_init(&a_zone, 0, a_capacity);
    sem_init(&n_zone, 0, n_capacity);

    for (i = 0; i < num_people; i++)
    {
        person_in_zone[i] = 'E';
        pthread_mutex_init(&person_zone_lock[i], NULL);
        pthread_cond_init(&person_moved[i], NULL);
    }

    // create threads
    pthread_create(&teams_thread[0], NULL, simulate_team, &teams[0]);
    pthread_create(&teams_thread[1], NULL, simulate_team, &teams[1]);
    for (i = 0; i < num_people; i++)
        pthread_create(&people_thread[i], NULL, person_motion, &team_args[i]);

    // wait for the threads to finish
    pthread_join(teams_thread[0], NULL);
    pthread_join(teams_thread[1], NULL);
    for (i = 0; i < num_people; i++)
        pthread_join(people_thread[i], NULL);

    goals[0] = -1;
    pthread_cond_broadcast(&goals_changed[0]);
    pthread_cond_destroy(&goals_changed[0]);
    pthread_mutex_destroy(&goals_lock[0]);
    pthread_cancel(teams_thread[0]);
    goals[1] = -1;
    pthread_cond_broadcast(&goals_changed[1]);
    pthread_cond_destroy(&goals_changed[1]);
    pthread_mutex_destroy(&goals_lock[1]);
    pthread_cancel(teams_thread[1]);

    sem_destroy(&n_zone);
    sem_destroy(&a_zone);
    sem_destroy(&h_zone);

    return 0;
}
