# An alternate course allocation Portal

Compiling and running the code - contains a makefile so run:

```bash
make
./a.out < testcases.txt
```

## Variables

- There are data structure to represent a student - `student`, a course - `course`, and a lab `lab`.
- We create an array of students - `all_students`, courses `all_courses`, labs - `all_labs`.
- `student_lock` is the mutex lock, `student_isAvailable` is the conditional variable that signals a change in the student state `student_sim`.
- `course_lock` is the mutex lock, `course_spots_isAvailable` is the conditional variable that signals a change free slots `course_spots_left`.
- `ta_lock` is the mutex lock, `ta_isAvailable` is the conditional variable that signals a TA state `ta`.

## Logic

Input is taken and stored in the structures. We create different threads for each student and each course. We run the simpulation, allocate TAs, conduct tutorials, drop courses, and end when the student and course complete. `init_courses`, `init_labTAs`, `init_students` mallocs memory for every data structure and assigns their values.

### Simulate the students looking for courses

- This is handled by `look_for_courses`.
- The thread (represents each student) goes to sleep until it is time for them to apply.
- The students provide a preference order. We go through them until they are either allocated a course or the course is dropped.
- When the student starts looking for a course we update their status.
- If there are free slots and TAs available then the student takes up the course and we update their status.
- We then wait till the tutorial gets over to wake the thread from sleep and check if the student picks the course based on random generator by checking probability.
- We then update the status based on course allocation.

### Simulate the course allocation

- This is handled by `course_allocation`.
- For a course to be taken we must find a TA.
- We loop through all the TAs and shortlist the ones who can TA the course based on the number of times they have TA'd.
- We wait if the TA is busy or withdraw if no TAs are available to take this course.
- Once we find a viable TA, we open the course to a fixed number of students.
- The tutorial is on hold until we have at least one student who has applied to that course and gotten the tutorial.
- We can also start the course with no students in case no one has applied and the TAs tutorial sessions will be updated.
- Considering there were students in the tutorial, we wake up their threads at the end of the session.
