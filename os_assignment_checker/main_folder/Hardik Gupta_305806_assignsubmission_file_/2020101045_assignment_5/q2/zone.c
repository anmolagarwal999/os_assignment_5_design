#include "headers.h"

void *handleZone(void *args)
{
    zone *zone1 = (zone *)args;
    int zone_no;
    if (zone1->id == 'H')
        zone_no = 0;
    else if (zone1->id == 'A')
        zone_no = 1;
    else
        zone_no = 2;

    // loop infinitely checking if any student has entered for this zone but has not been seated
    while (1)
    {
        sem_wait(&semaphore);
        for (int i = 0; i < no_of_spectator; i++)
        {
            pthread_mutex_lock(&mutexSpectator[i]);
            if ((Spectator[i].entered == 1) && (Spectator[i].is_seated == -1) && (Spectator[i].left == -1))
            {
                // printf("Name: %s Zone: %c\n" , Spectator[i].name , zone1->id);
                if (Spectator[i].team_code == 'N')
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else if (Spectator[i].team_code == 'A' && (zone1->id == 'A'))
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else if ( (Spectator[i].team_code == 'H') && ((zone1->id == 'N') || (zone1->id == 'H')))
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else
                {
                    pthread_mutex_unlock(&mutexSpectator[i]);
                }
            }
            else
            {
                pthread_mutex_unlock(&mutexSpectator[i]);   
            }
        }
    }
}