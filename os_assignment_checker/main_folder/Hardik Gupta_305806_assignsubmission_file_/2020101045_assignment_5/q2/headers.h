#ifndef _headers
#define _headers
#define _OPEN_THREADS

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>

#define MAX_NO_OF_SPECTATOR 200
#define MAX_GOALS 100

typedef struct zone
{
    char id; // id = [Zone H , Zone A , Zone N]
    int capacity;
    int seat_left;
} zone;

typedef struct spectator
{
    int spectator_id;
    char name[100];
    int entry_time;
    int group_no;
    char team_code; // H(home) , A (Away) or N (neutral)
    int patience_level;
    int enraged_no; // stores the number of goals opponent need to have to enrage this person
    int entered;
    int left;
    int is_seated;
    char zone_seated;
    int zone_no;
} spectator;

typedef struct goal
{
    int time;
    char team_code;
    float probability;
} goal;

int Team_Goal[2]; // stores which team has stored how many goal
                  // goal[0] -> home goal[1]-> away team

int spectating_time;
int num_groups;
int no_of_goal_scoring_chances;

int no_of_spectator;

spectator Spectator[MAX_NO_OF_SPECTATOR];
pthread_t threadSpectator[MAX_NO_OF_SPECTATOR];
pthread_cond_t condSpectator[MAX_NO_OF_SPECTATOR];
pthread_mutex_t mutexSpectator[MAX_NO_OF_SPECTATOR];

zone Zone[3];                 // Zone[0]-> H , Zone[1]->A , Zone[2]->N
pthread_mutex_t mutexZone[3]; // 0->H , 1->A , 2->N
pthread_t threadZone[3];      // 0->H , 1-> A , 2-> N

goal Goals[MAX_GOALS];
pthread_t threadGoal[MAX_GOALS];

sem_t semaphore;

void *handleZone(void *);
void *handleSpectator(void *);
void *handleGoal(void *);

// functions to print in diff color
void red () ;
void yellow () ;
void reset ();
void green ();
void blue ();
void purple();

#endif