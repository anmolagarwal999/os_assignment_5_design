# Question 2

To run Program :
-> make
->./a.out

## main.c
Takes input , initialize variables and makes thread for zone , spectator and goals. 
Join thread has been used on spectator and goals thread to complete terminaton only after goal and spectator thread has completed running.

## Spectator threads
Spectator.c is associated file. 
The thread first sleep for spec->entry_time of the spectator , and then the the spectator is made available to the zone.c (file responsible to allocate seat to the spectator) by making spec->entered = 1. 
We use the pthread_cond_timedwait for the patience level (in sec) of the spectator. 
If no seat is allocated during this time , then the pthread_cond_timedwait returns ETIMEDOUT value , in that case we exit the spectator.
Else , it returns 0 indicating that a signal was sent to it (after it was allocated the seat). Then we have an again condition timedwait till spectating time , if no signal is received during this time, then that means spectator has finished spectating else it has to exit due to bad performance of the team. 

## Zone threads
The main function of the zone thread is to allocate the seats to the spectator. It checks if any spectator has entered but not have been allocated any fees, if yes , and a seat is available in a zone in which it can be seated, then allocate the seat to him.
To avoid busy waiting , I have used semaphore. Whenever any spectator becomes availabe , it post to semaphore (increasing its value by 1) and every time the zone thread runs a loop through students , a wait request is made on semaphore (decreasing its value by 1). In this way , for each spectator , we runs a loop only once.

## Goal threads
The goal thread go to sleep till the time of the chance has come , then use random number to decide if the goal was successful or not. 
If goal was successful , then the fxn loop through all the player to check if it has become equal to enraged_value of any spectator.