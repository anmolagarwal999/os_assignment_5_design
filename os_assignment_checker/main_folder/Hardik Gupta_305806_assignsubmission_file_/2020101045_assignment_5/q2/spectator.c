#include "headers.h"

void *handleSpectator(void *args)
{
    spectator *spectator1 = (spectator *)args;

    int t = spectator1->entry_time;
    sleep(t);

    yellow();
    printf("%s has reached the stadium\n", spectator1->name);
    reset();

    int patience_time = spectator1->patience_level;
    int spectator_id = spectator1->spectator_id;

    // getting time
    struct timespec timeToWait;
    clock_gettime(CLOCK_REALTIME, &timeToWait);

    timeToWait.tv_sec += patience_time;
    pthread_mutex_lock(&mutexSpectator[spectator_id]);

    spectator1->entered = 1;

    sem_post(&semaphore);
    sem_post(&semaphore);

    int ret1 = pthread_cond_timedwait(&condSpectator[spectator_id], &mutexSpectator[spectator_id], &timeToWait);
    pthread_mutex_unlock(&mutexSpectator[spectator_id]);

    // printf("hello %s", spectator1->name);

    if (ret1 == 0) // person has been allocated a seat
    {
         // if no of goals has already exceeded the enraged value then quit
        if((spectator1->team_code == 'H' && Team_Goal[1] >= spectator1->enraged_no) || (spectator1->team_code == 'A' && Team_Goal[0]>=spectator1->enraged_no))
        {
            goto checkpoint1;
        }

        // update time
        clock_gettime(CLOCK_REALTIME, &timeToWait);
        timeToWait.tv_sec += spectating_time;

        pthread_mutex_lock(&mutexSpectator[spectator_id]);
        int ret2 = pthread_cond_timedwait(&condSpectator[spectator_id], &mutexSpectator[spectator_id], &timeToWait);

        if (ret2 == 0)
        {
            checkpoint1: // label
            spectator1->left = 1;
            // signal was sent and that means other team has exceeded the no of goals
            red();
            printf("%s is leaving due to bad performance of his team\n", spectator1->name);
            reset();

            pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
            Zone[spectator1->zone_no].seat_left++;
            pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }

        else if (ret2 == ETIMEDOUT)
        {
            // No signal was sent and thus person completed its spectating time
            spectator1->left = 1;
            red();
            printf("%s has watched the match for %d time is now leaving\n", spectator1->name, spectating_time);
            reset();

            pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
            Zone[spectator1->zone_no].seat_left++;
            pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }
        else
        {
            perror("Error in cond timedwait: ");
            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }
    }
    else if (ret1 == ETIMEDOUT)
    {
        // patience level exceeded
        // spectator leaves the simulation
        spectator1->left = 1;
        red();
        printf("%s could not get a seat\n", spectator1->name);
        reset();

        pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
        Zone[spectator1->zone_no].seat_left++;
        pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

        return NULL;
    }
    else
    {
        perror("Error in cond timedwait: ");
        return NULL;
    }
}
