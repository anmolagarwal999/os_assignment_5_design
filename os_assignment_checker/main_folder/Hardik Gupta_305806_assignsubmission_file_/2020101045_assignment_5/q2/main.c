#include "headers.h"

// color code
void red()
{
    printf("\033[1;31m");
}
void yellow()
{
    printf("\033[1;33m");
}
void reset()
{
    printf("\033[0m");
}
void green()
{
    printf("\033[1;32m");
}
void blue()
{
    printf("\033[1;34m");
}
void purple()
{
    printf("\033[1;35m");
}

void handle_input()
{
    for (int i = 0; i < 3; i++)
    {
        scanf("%d", &Zone[i].capacity);
        Zone[i].seat_left = Zone[i].capacity;
    }

    scanf("%d %d", &spectating_time, &num_groups);

    no_of_spectator = 0;

    for (int i = 1; i <= num_groups; i++)
    {
        int no_of_person;
        scanf("%d", &no_of_person);

        // printf("Value of no_of_person: %d\n" , no_of_person);

        for (int j = 0; j < no_of_person; j++)
        {
            scanf("%s %c %d %d %d", Spectator[no_of_spectator].name, &Spectator[no_of_spectator].team_code, &Spectator[no_of_spectator].entry_time, &Spectator[no_of_spectator].patience_level, &Spectator[no_of_spectator].enraged_no);
            Spectator[no_of_spectator].entered = -1;
            Spectator[no_of_spectator].left = -1;
            Spectator[no_of_spectator].is_seated = -1;
            Spectator[no_of_spectator].spectator_id = no_of_spectator;

            // for neutral fans
            if (Spectator[no_of_spectator].enraged_no == -1)
                Spectator[no_of_spectator].enraged_no = 9999999;

            no_of_spectator++;
        }
    }

    scanf("%d", &no_of_goal_scoring_chances);

    for (int i = 0; i < no_of_goal_scoring_chances; i++)
    {
        getchar();
        scanf("%c %d %f", &Goals[i].team_code, &Goals[i].time, &Goals[i].probability);
    }

    // initialize other variables
    Zone[0].id = 'H';
    Zone[1].id = 'A';
    Zone[2].id = 'N';

    Team_Goal[0] = 0;
    Team_Goal[1] = 0;
}

void check_input()
{
    for(int i=0; i <3 ; i++)
    {
        printf("Zone capacity: %d\n" , Zone[i].capacity);
    }

    printf("Spectating time: %d no_of_group: %d\n" , spectating_time , num_groups);

    printf("total no of spectator: %d\n" , no_of_spectator);

    for(int i=0; i < no_of_spectator; i++)
    {
        printf("name: %s zone_id: %c entry_time: %d patience_time: %d num_goals: %d\n" , Spectator[i].name , Spectator[i].team_code , Spectator[i].entry_time , Spectator[i].patience_level , Spectator[i].enraged_no);
    }

    printf("No of goals: %d\n" , no_of_goal_scoring_chances);

    for(int i=0; i < no_of_goal_scoring_chances; i++)
    {
        printf("Team code: %c time: %d prob: %f\n" , Goals[i].team_code , Goals[i].time  , Goals[i].probability );
    }
}



int main()
{
    srand(time(NULL));
    handle_input();
    // check_input();

    sem_init(&semaphore , 0 , 0);

    // init zone lock
    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_init(&mutexZone[i], NULL);
        pthread_create(&threadZone[i], NULL, handleZone, &Zone[i]);
    }

    // init mutex lock
    for (int i = 0; i < no_of_spectator; i++)
    {
        pthread_mutex_init(&mutexSpectator[i], NULL);
        pthread_cond_init(&condSpectator[i], NULL);
        pthread_create(&threadSpectator[i], NULL, handleSpectator, &Spectator[i]);
    }

    for (int i = 0; i < no_of_goal_scoring_chances; i++)
    {
        pthread_create(&threadGoal[i], NULL, handleGoal, &Goals[i]);
    }

    // join thread
    for (int i = 0; i < no_of_spectator; i++)
    {
        pthread_join(threadSpectator[i], NULL);
    }

    for(int i=0; i< no_of_goal_scoring_chances; i++)
    pthread_join(threadGoal[i] , NULL);

    printf("Simulation Ended\n");
}
