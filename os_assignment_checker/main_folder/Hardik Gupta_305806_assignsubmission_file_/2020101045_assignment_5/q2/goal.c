#include "headers.h"

void *handleGoal(void *args)
{
    goal *goal1 = (goal *)args;
    int t = goal1->time;

    sleep(t);

    float random_value =  (float)rand() / RAND_MAX;
    // printf("random value is %f\n" , random_value);

    if (random_value <= goal1->probability)
    {
        // update the goal of the team
        int x;
        if (goal1->team_code == 'H')
            x = 0;
        else
            x = 1;

        Team_Goal[x]++;

        char Team_id = goal1->team_code;

        green();
        printf("Team %c has scored their %dth goal\n", Team_id, Team_Goal[x]);
        reset();

        // check which players have exceeded their enraged value
        for (int i = 0; i < no_of_spectator; i++)
        {
            spectator *spectator1 = &Spectator[i];

            if ( (spectator1->entered==1) && (spectator1->is_seated==1) && (spectator1->left == -1) && (spectator1->team_code != Team_id) && (Team_Goal[x] >= spectator1->enraged_no))
            {
                // signal the player that he must leave now
                pthread_cond_signal(&condSpectator[i]);
            }
        }

        return NULL;
    }
    else
    {
        red();
        if (goal1->team_code == 'A')
        {
            printf("Team A has missed its chance to score its %d goal\n", Team_Goal[1] + 1);
        }
        else
        {
            printf("Team H has missed its chance to score its %d goal\n", Team_Goal[0] + 1);
        }
        reset();
        return NULL;
    }
}