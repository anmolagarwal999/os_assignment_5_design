#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#define SA struct sockaddr
struct dict
{
    int key;
    char value[150];
    pthread_mutex_t lock;
};

struct what
{
    struct dict dictionary[100];
    int num;
};

pthread_mutex_t numlock;

pthread_mutex_t lock;

struct what dir;

void thread_implementation(int sockfd)
{
    char buf[150];
    bzero(buf, 150);
    pthread_mutex_lock(&lock);
    read(sockfd, buf, sizeof(buf));
    pthread_mutex_unlock(&lock);
    int i = 0;
    char buf2[150];
    bzero(buf2, 150);
    while (buf[i] != ' ')
    {
        buf2[i] = buf[i];
        i++;
    }
    i++;
    pid_t tid;
    tid = gettid();
    if (strcmp(buf2, "insert") == 0)
    {
        bzero(buf2, 150);
        int j = 0;
        while (buf[i] != ' ')
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        i++;
        int key = atoi(buf2);
        bzero(buf2, 150);
        int k = 0;
        while (buf[i] != NULL)
        {
            buf2[k] = buf[i];
            i++;
            k++;
        }
        int l = 0;
        while (l < dir.num && dir.dictionary[l].key != key)
        {
            l++;
        }
        if (l < dir.num)
        {
            bzero(buf, 150);
            sprintf(buf, "%d:Key already exists", gettid());
            write(sockfd, buf, 150);
        }
        else
        {
            pthread_mutex_lock(&dir.dictionary[dir.num].lock);
            dir.dictionary[dir.num].key = key;
            strcpy(dir.dictionary[dir.num].value, buf2);
            pthread_mutex_unlock(&dir.dictionary[dir.num].lock);
            pthread_mutex_lock(&numlock);
            dir.num++;
            pthread_mutex_unlock(&numlock);
            bzero(buf, 150);
            sprintf(buf, "%d:Insertion successful", gettid());
            write(sockfd, buf, 150);
        }
    }
    else if (strcmp(buf2, "delete") == 0)
    {
        bzero(buf2, 150);
        int j = 0;
        while (buf[i] != NULL)
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        i++;
        int key = atoi(buf2);
        bzero(buf2, 150);
        int l = 0;
        while (l < dir.num && dir.dictionary[l].key != key)
        {
            l++;
        }
        if (l < dir.num)
        {
            pthread_mutex_lock(&dir.dictionary[l].lock);
            dir.dictionary[l].key = -1;
            pthread_mutex_unlock(&dir.dictionary[l].lock);
            bzero(buf, 150);
            sprintf(buf, "%d:Deletion successful", gettid());
            write(sockfd, buf, 150);
        }
        else
        {
            bzero(buf, 150);
            sprintf(buf, "%d:No such key exist", gettid());
            write(sockfd, buf, 150);
        }
    }
    else if (strcmp(buf2, "update") == 0)
    {
        bzero(buf2, 150);
        int j = 0;
        while (buf[i] != ' ')
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        i++;
        int key = atoi(buf2);
        bzero(buf2, 150);
        j = 0;
        while (buf[i] != NULL)
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        int l = 0;
        while (l < dir.num && dir.dictionary[l].key != key)
        {
            l++;
        }
        if (l < dir.num)
        {
            pthread_mutex_lock(&dir.dictionary[l].lock);
            strcpy(dir.dictionary[l].value, buf2);
            bzero(buf, 150);
            sprintf(buf, "%d:", gettid());
            strcat(buf, dir.dictionary[l].value);
            write(sockfd, buf, 150);
            pthread_mutex_unlock(&dir.dictionary[l].lock);
        }
        else
        {
            bzero(buf, 150);
            sprintf(buf, "%d:Key does not exist", gettid());
            write(sockfd, buf, 150);
        }
    }
    else if (strcmp(buf2, "fetch") == 0)
    {
        bzero(buf2, 150);
        int j = 0;
        while (buf[i] != NULL)
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        i++;
        int key = atoi(buf2);
        bzero(buf2, 150);
        int l = 0;
        while (l < dir.num && dir.dictionary[l].key != key)
        {
            l++;
        }
        if (l < dir.num)
        {
            pthread_mutex_lock(&dir.dictionary[l].lock);
            bzero(buf, 150);
            sprintf(buf, "%d:", gettid());
            strcat(buf, dir.dictionary[l].value);
            write(sockfd, buf, 150);
            pthread_mutex_unlock(&dir.dictionary[l].lock);
        }
        else
        {
            bzero(buf, 150);
            sprintf(buf, "%d:Key does not exist", gettid());
            write(sockfd, buf, 150);
        }
    }
    else if (strcmp(buf2, "concat") == 0)
    {
        bzero(buf2, 150);
        int j = 0;
        while (buf[i] != ' ')
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        i++;
        int key1 = atoi(buf2);
        bzero(buf2, 150);
        j = 0;
        while (buf[i] != NULL)
        {
            buf2[j] = buf[i];
            i++;
            j++;
        }
        int key2 = atoi(buf2);
        int l = 0;
        while (l < dir.num && dir.dictionary[l].key != key1)
        {
            l++;
        }
        int m = 0;
        while (m < dir.num && dir.dictionary[m].key != key2)
        {
            m++;
        }
        if (l < dir.num && m < dir.num)
        {
            char value[150];
            bzero(value, 150);
            pthread_mutex_lock(&dir.dictionary[l].lock);
            pthread_mutex_lock(&dir.dictionary[m].lock);
            strcpy(value, dir.dictionary[l].value);
            strcat(dir.dictionary[l].value, dir.dictionary[m].value);
            strcat(dir.dictionary[m].value, value);
            bzero(buf, 150);
            sprintf(buf, "%d:", gettid());
            strcat(buf, dir.dictionary[m].value);
            write(sockfd, buf, 150);
            pthread_mutex_unlock(&dir.dictionary[m].lock);
            pthread_mutex_unlock(&dir.dictionary[l].lock);
        }
        else
        {
            bzero(buf, 150);
            sprintf(buf, "%d:Concat failed as at least one of the keys does not exist", gettid());
            write(sockfd, buf, 150);
        }
    }
    else
    {
        bzero(buf, 150);
        write(sockfd, "No such operation", 18);
    }
}

void thread_start(int sock)
{
    while (1)
    {
        struct sockaddr_in client;
        int len = sizeof(client);
        fflush(stdout);
        int fd = accept(sock, (SA *)&client, &len);
        if (fd < 0)
        {
            printf("server accept failed...\n");
            exit(0);
        }
        thread_implementation(fd);
    }
}

int main(int argc, char **argv)
{
    int sock;
    struct sockaddr_in server, client;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        perror("Error: ");
        exit(0);
    }
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(8080);
    if ((bind(sock, (SA *)&server, sizeof(server))) != 0)
    {
        perror("Error: ");
        exit(0);
    }
    if ((listen(sock, 100)) != 0)
    {
        perror("Error: ");
        exit(0);
    }
    if (argc < 2)
    {
        exit(0);
    }
    int num = atoi(argv[1]);
    pthread_t thread[num];
    for (int i = 0; i < num; i++)
    {
        pthread_create(&thread[i], NULL, &thread_start, sock);
    }

    for (int p = 0; p < num; p++)
    {
        pthread_join(thread[p], NULL);
    }
    close(sock);
}
