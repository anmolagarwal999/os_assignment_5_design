#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <pthread.h>
#define PORT 8080
#define SA struct sockaddr

int time1;

struct client
{
    int time;
    char name[100];
    int num1;
    int num2;
    char value[100];
};

struct cli
{
    pthread_t thread[100];
    struct client client[100];
    int num;
};

struct cli clion;

void func(int sockfd, int i)
{
    char buf[150];
    char num[4];
    char num2[4];
    bzero(num, 4);
    bzero(num2, 4);
    bzero(buf, 150);
    int num1 = atoi(buf);
    int n;
    strcat(buf, clion.client[i].name);
    strcat(buf, " ");
    if (strcmp(clion.client[i].name, "insert") == 0)
    {
        int x = clion.client[i].num1;
        sprintf(num, "%d", x);
        strcat(buf, num);
        strcat(buf, " ");
        char* value = clion.client[i].value;
        strcat(buf, value);
    }
    else if (strcmp(clion.client[i].name, "update") == 0)
    {
        sprintf(num,"%d", clion.client[i].num1);
        strcat(buf, num);
        strcat(buf, " ");
        strcat(buf, clion.client[i].value);
    }
    else if (strcmp(clion.client[i].name, "delete") == 0)
    {
        sprintf(num,"%d", clion.client[i].num1);
        strcat(buf, num);
    }
    else if (strcmp(clion.client[i].name, "fetch") == 0)
    {
        sprintf(num,"%d", clion.client[i].num1);
        strcat(buf, num);
    }
    else if (strcmp(clion.client[i].name, "concat") == 0)
    {
        sprintf(num,"%d", clion.client[i].num1);
        strcat(buf, num);
        strcat(buf, " ");
        sprintf(num2,"%d", clion.client[i].num2);
        strcat(buf, num2);
    }
    write(sockfd, buf, sizeof(buf));
    bzero(buf, 150);
    read(sockfd, buf, 150);
    printf("%d:",i);
    printf("%s\n",buf);
}

void create_threads(int i)
{
    int sockfd, connfd;
    struct sockaddr_in server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("Socket couldn't be created\n");
        exit(0);
    }
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(PORT);
    sleep(clion.client[i].time);
    if (connect(sockfd, (SA *)&server, sizeof(server)) != 0)
    {
        printf("Connection failed\n");
        exit(0);
    }

    func(sockfd, i);
    close(sockfd);
}

int main()
{
    scanf("%d", &clion.num);
    for (int i = 0; i < clion.num; i++)
    {
        scanf("%d %s", &clion.client[i].time, &clion.client[i].name);
        if (strcmp(clion.client[i].name, "insert") == 0)
        {
            scanf("%d %s", &clion.client[i].num1, &clion.client[i].value);
        }
        else if (strcmp(clion.client[i].name, "update") == 0)
        {
            scanf("%d %s", &clion.client[i].num1, &clion.client[i].value);
        }
        else if (strcmp(clion.client[i].name, "delete") == 0)
        {
            scanf("%d", &clion.client[i].num1);
        }
        else if (strcmp(clion.client[i].name, "fetch") == 0)
        {
            scanf("%d", &clion.client[i].num1);
        }
        else if (strcmp(clion.client[i].name, "concat") == 0)
        {
            scanf("%d %d", &clion.client[i].num1, &clion.client[i].num2);
        }
    }

    for(int i = 0; i< clion.num; i++)
    {
        //time1 = clock()/CLOCKS_PER_SEC;
        pthread_create(&clion.thread[i], NULL, &create_threads, (void *)i);
    }
    for (int p = 0; p < clion.num; p++)
    {
        pthread_join(clion.thread[p], NULL);
    }
}
