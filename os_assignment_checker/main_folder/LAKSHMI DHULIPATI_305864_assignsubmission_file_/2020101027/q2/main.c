#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#define MAX_PPL 32;

char HAN;
float prob;
int hgoal;
int agoal;
struct person
{
    char *name;
    long long unsigned int time;
    long long unsigned int patience;
    long long unsigned int acquire_time;
    char zone_type;
    int enraged_num;
    bool secured_seat;
    bool exit_status;
    char zone;
};

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

int hnum;
int anum;
int lnum;

pthread_t wait[100];
pthread_t exiting[100];

struct group
{
    int num_ppl;
    struct person *people[32];
};

struct grp_chance
{
    char HA;
    float prob;
    int time_elapsed;
};

pthread_mutex_t grp;

int spectating_time;
int num_people;
int h, a, n;
int num_grp;
int num_grp_ppl;

pthread_mutex_t hlock;
pthread_mutex_t alock;
pthread_mutex_t nlock;
pthread_mutex_t dlock;
pthread_mutex_t block;
pthread_cond_t cond1;
pthread_cond_t cond2;
pthread_cond_t cond3;

pthread_cond_t condn[100];

pthread_mutex_t lock1;
pthread_mutex_t lock2;
pthread_mutex_t lock3;

pthread_t goal[100];

pthread_t exit_goal[100];

void stadium(struct person *people)
{
    char *name = people->name;
    sleep(people->time + people->patience);
    int m = 0;
    if (people->secured_seat == 0)
    {
        printf(RED "%s could not get a seat\n", &name);
        if (people->zone == 'H')
        {
            pthread_mutex_lock(&lock1);
            hnum--;
            pthread_mutex_unlock(&lock1);
        }
        if (people->zone == 'A')
        {
            pthread_mutex_lock(&lock2);
            anum--;
            pthread_mutex_unlock(&lock2);
        }
        if (people->zone == 'N')
        {
            pthread_mutex_lock(&lock3);
            lnum--;
            pthread_mutex_unlock(&lock3);
        }
        people->exit_status = 1;
        printf(RED "%s is exiting the stadium\n", &name);
        fflush(stdout);
    }
    else
    {
        if (people->zone_type == 'H')
        {
            sleep(spectating_time - people->patience - ((clock() - people->acquire_time) / CLOCKS_PER_SEC));
            if (people->exit_status == 0)
            {
                m = 1;
                people->exit_status = 1;
                pthread_mutex_lock(&hlock);
                h++;
                pthread_cond_signal(&cond1);
                pthread_mutex_unlock(&hlock);
            }
        }

        if (people->zone_type == 'A')
        {
            sleep(spectating_time - ((clock() - people->acquire_time) / CLOCKS_PER_SEC));
            if (people->exit_status == 0)
            {
                m = 1;
                people->exit_status = 1;
                pthread_mutex_lock(&alock);
                a++;
                pthread_cond_signal(&cond2);
                pthread_mutex_unlock(&alock);
            }
        }

        if (people->zone_type == 'N')
        {
            sleep(spectating_time - ((clock() - people->acquire_time) / CLOCKS_PER_SEC));
            if (people->exit_status == 0)
            {
                m = 1;
                people->exit_status = 1;
                pthread_mutex_lock(&nlock);
                n++;
                pthread_cond_signal(&cond3);
                pthread_mutex_unlock(&nlock);
            }
        }
        if (m == 1)
            printf(RED "%s watched the match for %d seconds and is leaving\n", &name, spectating_time);
    }
}

pthread_cond_t condwina;
pthread_mutex_t locka;

void exit_goals(struct person *people)
{
    pthread_mutex_lock(&locka);
L8:
    pthread_cond_wait(&condwina, &locka);
    if (people->exit_status == 0)
    {
        if (people->zone_type == 'H')
        {
            if (agoal - hgoal >= people->enraged_num)
            {
                if (people->zone == 'H')
                {
                    pthread_mutex_unlock(&locka);
                    people->exit_status = 1;
                    pthread_mutex_lock(&hlock);
                    h++;
                    pthread_cond_signal(&cond1);
                    pthread_mutex_unlock(&hlock);
                }

                if (people->zone == 'N')
                {
                    pthread_mutex_unlock(&locka);
                    people->exit_status = 1;
                    pthread_mutex_lock(&nlock);
                    n++;
                    pthread_cond_signal(&cond3);
                    pthread_mutex_unlock(&nlock);
                }
                printf(RED "%s is leaving due to bad performance of his team\n", &people->name);
            }
            else
            {
                goto L8;
            }
        }
        if (people->zone_type == 'A')
        {
            if (hgoal - agoal >= people->enraged_num)
            {
                if (people->zone == 'A')
                {
                    pthread_mutex_unlock(&locka);
                    people->exit_status = 1;
                    pthread_mutex_lock(&alock);
                    a++;
                    pthread_cond_signal(&cond2);
                    pthread_mutex_unlock(&alock);
                }

                printf(RED "%s is leaving due to bad performance of his team\n", &people->name);
            }
            else
            {
                goto L8;
            }
        }
    }

    pthread_mutex_unlock(&locka);
    pthread_exit(NULL);
}

void goals(struct grp_chance *goal_chance)
{
    sleep(goal_chance->time_elapsed);
    if (goal_chance->HA == 'H')
    {
        if (goal_chance->prob > 0.5)
        {
            pthread_mutex_lock(&block);
            hgoal++;
            pthread_mutex_unlock(&block);
            printf(YEL "The team H scored a goal\n");
            pthread_mutex_lock(&locka);
            pthread_cond_broadcast(&condwina);
            pthread_mutex_unlock(&locka);
        }
        else
        {
            printf(YEL "The team H has missed a goal\n");
        }
    }
    else if (goal_chance->HA == 'A')
    {
        if (goal_chance->prob > 0.5)
        {
            pthread_mutex_lock(&dlock);
            agoal++;
            pthread_mutex_unlock(&dlock);
            printf(YEL "The team A scored a goal\n");
            pthread_mutex_lock(&locka);
            pthread_cond_broadcast(&condwina);
            pthread_mutex_unlock(&locka);
        }
        else
        {
            printf(YEL "The team A has missed a goal\n");
        }
    }
}

void allocate(struct person *people)
{

    sleep(people->time);
    printf(GRN "%s reached the stadium\n", &people->name);
    if (people->zone_type == 'H')
    {

        pthread_mutex_lock(&hlock);
        pthread_mutex_lock(&nlock);
        if (h == 0 && n == 0)
        {
            pthread_mutex_lock(&lock1);
            pthread_mutex_lock(&lock3);
            if (hnum < lnum)
            {
                people->zone = 'H';
                hnum++;
                pthread_mutex_unlock(&lock1);
                pthread_mutex_unlock(&lock3);

                pthread_mutex_unlock(&nlock);
                int ret = pthread_cond_wait(&cond1, &hlock);
                if (people->secured_seat == 0 && ret == 0 && people->exit_status == 0)
                {
                    people->secured_seat = 1;
                    printf(WHT "%s secured a seat in zone H\n", &people->name);
                    people->acquire_time = clock();
                    h--;
                    pthread_mutex_unlock(&hlock);
                    pthread_mutex_lock(&lock1);
                    hnum--;
                    pthread_mutex_unlock(&lock1);
                    //return;
                }
            }
            else
            {
                people->zone = 'N';
                
                pthread_mutex_unlock(&hlock);

                lnum++;
                pthread_mutex_unlock(&lock3);
                pthread_mutex_unlock(&lock1);
                int ret2 = pthread_cond_wait(&cond2, &nlock);
                if (people->secured_seat == 0 && ret2 == 0 && people->exit_status == 0)
                {
                    people->secured_seat = 1;
                    printf(CYN "%s secured a seat in zone N\n", &people->name);
                    people->acquire_time = clock();
                    n--;
                    pthread_mutex_unlock(&nlock);
                    pthread_mutex_lock(&lock3);
                    lnum--;
                    pthread_mutex_unlock(&lock3);
                    //return;
                }
            }
        }
        else if (n > 0)
        {
            
            pthread_mutex_unlock(&hlock);
            n--;
            people->zone = 'N';
            pthread_mutex_unlock(&nlock);
            people->secured_seat = 1;
            printf(CYN "%s secured a seat in zone N\n", &people->name);
            people->acquire_time = clock();
        }
        else if (h > 0)
        {
            people->zone = 'H';
            pthread_mutex_unlock(&nlock);
            h--;
            pthread_mutex_unlock(&hlock);

            people->secured_seat = 1;
            printf(WHT "%s secured a seat in zone H\n", &people->name);
            people->acquire_time = clock();
        }
    }

    else if (people->zone_type == 'A')
    {
        pthread_mutex_lock(&alock);
        if (a == 0)
        {
            pthread_mutex_lock(&lock2);
            anum++;
            pthread_mutex_unlock(&lock2);
            people->zone = 'A';
            int ret = pthread_cond_wait(&cond3, &alock);
            if (people->secured_seat == 0 && ret == 0 && people->exit_status == 0)
            {
                people->secured_seat = 1;
                printf(MAG "%s secured a seat in zone A\n", &people->name);
                people->acquire_time = clock();
                a--;
                pthread_mutex_lock(&lock2);
                anum--;
                pthread_mutex_unlock(&lock2);
                //return;
            }
        }
        else if (a > 0)
        {
            a--;
            people->zone = 'A';
            people->secured_seat = 1;
            printf(MAG "%s secured a seat in zone A\n", &people->name);
            people->acquire_time = clock();
        }
        pthread_mutex_unlock(&alock);
    }

    else if (people->zone_type == 'N')
    {
        pthread_mutex_lock(&hlock);
        pthread_mutex_lock(&nlock);
        pthread_mutex_lock(&alock);
        if (h == 0 && n == 0 && a == 0)
        {
            pthread_mutex_lock(&lock1);
            pthread_mutex_lock(&lock3);
            pthread_mutex_lock(&lock2);
            if (hnum < anum && hnum < lnum)
            {
                pthread_mutex_unlock(&lock3);
                pthread_mutex_unlock(&lock2);
                hnum++;
                pthread_mutex_unlock(&lock1);
                pthread_mutex_unlock(&nlock);
                pthread_mutex_unlock(&alock);
                people->zone = 'H';
                int ret = pthread_cond_wait(&cond1, &hlock);
                if (people->secured_seat == 0 && ret == 0 && people->exit_status == 0)
                {
                    people->secured_seat = 1;
                    people->acquire_time = clock();
                    h--;
                    pthread_mutex_unlock(&hlock);
                    printf(WHT "%s secured a seat in zone H\n", &people->name);
                    pthread_mutex_lock(&lock1);
                    hnum--;
                    pthread_mutex_unlock(&lock1);
                    //return;
                }
            }
            if (lnum <= hnum && lnum < anum)
            {
                pthread_mutex_unlock(&lock1);
                pthread_mutex_unlock(&lock2);

                lnum++;
                pthread_mutex_unlock(&alock);
                pthread_mutex_unlock(&hlock);
                pthread_mutex_unlock(&lock3);
                people->zone = 'N';
                int ret2 = pthread_cond_wait(&cond2, &nlock);
                if (people->secured_seat == 0 && ret2 == 0 && people->exit_status == 0)
                {
                    people->secured_seat = 1;
                    people->acquire_time = clock();
                    n--;
                    pthread_mutex_unlock(&nlock);
                    printf(CYN "%s secured a seat in zone N\n", &people->name);
                    pthread_mutex_lock(&lock3);
                    lnum--;
                    pthread_mutex_unlock(&lock3);
                    //return;
                }
            }
            if (anum <= hnum && anum <= lnum)
            {
                pthread_mutex_unlock(&lock1);
                pthread_mutex_unlock(&lock3);
                anum++;
                pthread_mutex_unlock(&lock2);
                pthread_mutex_unlock(&nlock);
                pthread_mutex_unlock(&hlock);
                people->zone = 'A';
                int ret3 = pthread_cond_wait(&cond3, &alock);
                if (people->secured_seat == 0 && ret3 == 0 && people->exit_status == 0)
                {
                    people->secured_seat = 1;
                    people->acquire_time = clock();
                    a--;
                    pthread_mutex_unlock(&alock);
                    printf(MAG "%s secured a seat in zone A\n", &people->name);
                    pthread_mutex_lock(&lock2);
                    anum--;
                    pthread_mutex_unlock(&lock2);
                    //return;
                }
            }
        }
        else if (h > 0)
        {
            pthread_mutex_unlock(&nlock);
            pthread_mutex_unlock(&alock);
            h--;
            pthread_mutex_unlock(&hlock);
            people->zone = 'H';
            people->secured_seat = 1;
            printf(WHT "%s secured a seat in zone H\n", &people->name);
            people->acquire_time = clock();
        }
        else if (n > 0)
        {
            pthread_mutex_unlock(&alock);
            pthread_mutex_unlock(&hlock);
            n--;
            pthread_mutex_unlock(&nlock);
            people->zone = 'N';
            people->secured_seat = 1;
            printf(CYN "%s secured a seat in zone N\n", &people->name);
            people->acquire_time = clock();
        }
        else if (a > 0)
        {
            pthread_mutex_unlock(&nlock);
            pthread_mutex_unlock(&hlock);
            a--;
            people->zone = 'A';
            pthread_mutex_unlock(&alock);
            people->secured_seat = 1;
            people->acquire_time = clock();
            printf(MAG "%s secured a seat in zone A\n", &people->name);
        }
    }
    return;
}

int main()
{
    char space;
    scanf("%d%d%d", &h, &a, &n);
    scanf("%c", &space);
    scanf("%d", &spectating_time);
    scanf("%d%c", &num_grp, &space);
    struct group grp[num_grp];
    for (int i = 0; i < num_grp; i++)
    {
        scanf("%d%c", &num_grp_ppl, &space);
        grp[i].num_ppl = num_grp_ppl;
        for (int j = 0; j < num_grp_ppl; j++)
        {
            grp[i].people[j] = malloc(sizeof(struct person));
            char *name;
            char init;
            int time;
            int patience;
            int goals;
            scanf("%s%c%c%d%d%d", &name, &space, &init, &time, &patience, &goals);
            grp[i].people[j]->name = name;
            grp[i].people[j]->patience = patience;
            grp[i].people[j]->time = time;
            grp[i].people[j]->zone_type = init;
            grp[i].people[j]->enraged_num = goals;
            grp[i].people[j]->secured_seat = 0;
            grp[i].people[j]->acquire_time = 0;
            grp[i].people[j]->exit_status = 0;
        }
    }
    int chances;
    char HA;
    float timeelapsed;
    float probability;
    scanf("%d", &chances);
    struct grp_chance goal_chance[chances];
    for (int i = 0; i < chances; i++)
    {
        scanf("%c%c%f%f", &space, &HA, &timeelapsed, &probability);
        goal_chance[i].HA = HA;
        goal_chance[i].prob = probability;
        goal_chance[i].time_elapsed = timeelapsed;
    }
    time_t timein = clock();
    int k = 0;
    for (int i = 0; i < num_grp; i++)
    {
        for (int j = 0; j < grp[i].num_ppl; j++)
        {
            pthread_create(&wait[k], NULL, &allocate, grp[i].people[j]);
            pthread_create(&exiting[k], NULL, &stadium, grp[i].people[j]);
            pthread_create(&exit_goal[i], NULL, &exit_goals, grp[i].people[j]);
            k++;
        }
    }
    int l = 0;
    for (int i = 0; i < chances; i++)
    {
        pthread_create(&goal[i], NULL, &goals, &goal_chance[i]);
        l++;
    }

    for (int p = 0; p < k; p++)
    {
        pthread_join(wait[p], NULL);
    }
    for (int p = 0; p < k; p++)
    {
        pthread_join(exiting[p], NULL);
    }
    /*for (int p = 0; p < k; p++)
    {
        pthread_join(exit_goal, NULL);
    }*/
    for (int p = 0; p < l; p++)
    {
        pthread_join(goal[p], NULL);
    }
}