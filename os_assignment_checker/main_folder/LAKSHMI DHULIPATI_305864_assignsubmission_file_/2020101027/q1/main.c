#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

struct ta
{
    int ta_time;
    int ta_num;
    bool ta_curr;
};

struct lab
{
    char name[50];
    int limit;
    int num_ta;
    int num;
    struct ta ta[50];
    bool exists;
    pthread_mutex_t lock1;
    pthread_mutex_t lock2;
};

struct course
{
    char name[50];
    float interest;
    int lab[50];
    int course_max_slot;
    int num_labs;
    int num;
    bool available;
    pthread_cond_t check;
    pthread_mutex_t lock;
    bool exists;
    int num_wait;
    pthread_mutex_t lock2;
};

struct student
{
    float calibre;
    int pref1;
    int pref2;
    int pref3;
    float time;
    int cur_pref;
};

struct courses
{
    int num;
    struct course course[50];
};

int num_students;
pthread_mutex_t stud;
pthread_t hello;
pthread_cond_t exiti;

struct courses all_course;

struct students
{
    struct student student[50];
    int num;
};

struct students students;

struct labs
{
    int num_labs;
    struct lab lab[50];
};

struct labs labss;
pthread_t registration[150];
pthread_t course_off[150];

void student(int i)
{
    sleep(students.student[i].time);
    printf(BLU "Student %d has filled in preferences for course registration\n", i);
    students.student[i].cur_pref = students.student[i].pref1;
S1:
    pthread_mutex_lock(&all_course.course[students.student[i].cur_pref].lock2);
    if (all_course.course[students.student[i].cur_pref].exists == 1)
    {
        pthread_mutex_unlock(&all_course.course[students.student[i].cur_pref].lock2);
        pthread_mutex_lock(&all_course.course[students.student[i].cur_pref].lock);
        all_course.course[students.student[i].cur_pref].num_wait++;
        pthread_cond_wait(&all_course.course[students.student[i].cur_pref].check, &all_course.course[students.student[i].cur_pref].lock);
        //all_course.course[students.student[i].cur_pref].available = all_course.course[students.student[i].cur_pref].available - 1;
        pthread_mutex_unlock(&all_course.course[students.student[i].cur_pref].lock);
    }
    pthread_mutex_unlock(&all_course.course[students.student[i].cur_pref].lock2);
    if (all_course.course[students.student[i].cur_pref].exists == 0 && students.student[i].cur_pref == students.student[i].pref1)
    {
        //printf("Student %d has withdrawn from course %s", i, all_course.course[students.student[i].cur_pref].name);
        students.student[i].cur_pref = students.student[i].pref2;
        printf(YEL "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n", i, all_course.course[students.student[i].pref1].name, all_course.course[students.student[i].pref2].name);
        goto S1;
    }
    else if (all_course.course[students.student[i].cur_pref].exists == 0 && students.student[i].cur_pref == students.student[i].pref2)
    {
        //printf("Student %d has withdrawn from course %s", i, all_course.course[students.student[i].cur_pref].name);
        students.student[i].cur_pref = students.student[i].pref3;
        printf(YEL "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", i, all_course.course[students.student[i].pref2].name, all_course.course[students.student[i].pref3].name);
        goto S1;
    }
    else if (all_course.course[students.student[i].cur_pref].exists == 1)
    {
        printf(MAG "Student %d has been allocated a seat in course %s\n", i, all_course.course[students.student[i].cur_pref].name);
        //tutorial check
        //wait tutorial time
        //then start the conditions...
        sleep(6);
        if (students.student[i].calibre * all_course.course[students.student[i].cur_pref].interest > 0.5)
        {
            printf(CYN "Student %d has selected course %s permanently\n", i, all_course.course[students.student[i].cur_pref].name);
            pthread_mutex_lock(&stud);
            num_students--;
            pthread_cond_signal(&exiti);
            pthread_mutex_unlock(&stud);
            return;
        }
        else
        {
            printf(CYN "Student %d has withdrawn from course %s\n", i, all_course.course[students.student[i].cur_pref].name);
            if (students.student[i].cur_pref == students.student[i].pref1)
            {
                printf(YEL "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n", i, all_course.course[students.student[i].pref1].name, all_course.course[students.student[i].pref2].name);
                students.student[i].cur_pref = students.student[i].pref2;
                goto S1;
            }
            else if (students.student[i].cur_pref == students.student[i].pref2)
            {
                printf(YEL "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", i, all_course.course[students.student[i].pref2].name, all_course.course[students.student[i].pref3].name);
                students.student[i].cur_pref = students.student[i].pref3;
                goto S1;
            }
            else
            {
                pthread_mutex_lock(&stud);
                num_students--;
                printf("Student %d couldn’t get any of his preferred courses\n", i);
                pthread_cond_signal(&exiti);
                pthread_mutex_unlock(&stud);
                return;
            }
        }
    }
    else
    {
        pthread_mutex_lock(&stud);
        num_students--;
        pthread_cond_signal(&exiti);
        pthread_mutex_unlock(&stud);
        printf("Student %d couldn’t get any of his preferred courses\n", i);
        return;
    }
}

void exiting()
{
    pthread_mutex_lock(&stud);
E1:
    pthread_cond_wait(&exiti, &stud);
    if (num_students > 0)
    {
        goto E1;
    }
    else
    {
        printf(RED "EXITED");
        exit(0);
    }
    pthread_mutex_unlock(&stud);
}

//pthread_mutex_t lock1;
//pthread_mutex_t lock2;

void course_offered(int i)
{
    //printf("%s %d\n", all_course.course[i].name, all_course.course[i].num);
    while (all_course.course[i].exists == 1 && num_students > 0)
    {
        
        //printf("num : %d\n", all_course.course[i].exists);
        int z = 0;
        int r,s;
        char name[50];
        bzero(name, 50);
        int l = 0;
        int m = 0;
        int p, q;
        //while(m == 0){
        for (int x = 0; x < all_course.course[i].num_labs; x++)
        {
            if (labss.lab[all_course.course[i].lab[x]].exists == 1)
            {
                for (int y = 0; y < labss.lab[all_course.course[i].lab[x]].num_ta; y++)
                {

                    pthread_mutex_lock(&labss.lab[all_course.course[i].lab[x]].lock1);
                    if (labss.lab[all_course.course[i].lab[x]].ta[y].ta_curr == 0 && (labss.lab[all_course.course[i].lab[x]].ta[y].ta_time < labss.lab[all_course.course[i].lab[x]].limit) && labss.lab[all_course.course[i].lab[x]].exists == 1)
                    {
                        r = x;
                        z = 1;
                        strcpy(name, labss.lab[all_course.course[i].lab[x]].name);
                        labss.lab[all_course.course[i].lab[x]].ta[y].ta_time++;
                        labss.lab[all_course.course[i].lab[x]].ta[y].ta_curr = 1;
                        q = labss.lab[all_course.course[i].lab[x]].ta[y].ta_time - 1;
                        p = y;
                        pthread_mutex_unlock(&labss.lab[all_course.course[i].lab[x]].lock1);
                        goto C1;
                    }
                    pthread_mutex_unlock(&labss.lab[all_course.course[i].lab[x]].lock1);
                }
                for (int y = 0; y < labss.lab[all_course.course[i].lab[x]].num_ta; y++)
                {
                    pthread_mutex_lock(&labss.lab[all_course.course[i].lab[x]].lock2);
                    if (labss.lab[all_course.course[i].lab[x]].ta[y].ta_time == labss.lab[all_course.course[i].lab[x]].limit)
                    {
                        l = 1;
                        //p = y;
                        //labss.lab[all_course.course[i].lab[x]].ta[y].ta_time++;
                        //q = labss.lab[all_course.course[i].lab[x]].ta[y].ta_time;
                        //labss.lab[all_course.course[i].lab[x]].ta[y].ta_curr = 1;
                    }
                    pthread_mutex_unlock(&labss.lab[all_course.course[i].lab[x]].lock2);
                }
                if (l == 0)
                {
                    labss.lab[all_course.course[i].lab[x]].exists = 0;
                    printf(GRN "Lab %s no longer has students available for TA ship\n", labss.lab[all_course.course[i].lab[x]].name);
                }
            }

            if (labss.lab[all_course.course[i].lab[x]].exists == 1)
            {
                m = 1;
            }
        } //}
        //TA t_i from lab l_j has been allocated to course c_k for nth TA ship
        if (m == 0)
        {
            for(int i = 0;i< num_students;i++)
            {
                pthread_mutex_lock(&all_course.course[i].lock);
                pthread_cond_signal(&all_course.course[i].check);
                pthread_mutex_unlock(&all_course.course[i].lock);
            }
            all_course.course[i].exists = 0;
            printf(GRN "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", all_course.course[i].name);
            break;
        }
    C1:
        if (z == 1)
        {
            printf(WHT "TA %d from lab %s has been allocated to course %s for %dth TA ship\n", p, name, all_course.course[i].name, q + 1);
            srand(time(NULL));
            int j = rand() % all_course.course[i].course_max_slot + 1;
            printf(RED "Course %s has been allocated %d seats\n", all_course.course[i].name, j);
            sleep(5);
            if (all_course.course[i].num_wait > j)
            {
                for (int l = 0; l < j; l++)
                {
                    pthread_mutex_lock(&all_course.course[i].lock);
                    pthread_cond_signal(&all_course.course[i].check);
                    pthread_mutex_unlock(&all_course.course[i].lock);
                }
                printf(CYN "Tutorial has started for Course %s with %d seats filled out of %d\n", all_course.course[i].name, j, j);
                //sleep(3);
                pthread_mutex_lock(&all_course.course[i].lock);
                all_course.course[i].num_wait = all_course.course[i].num_wait - j;
                pthread_mutex_unlock(&all_course.course[i].lock);
            }
            else
            {
                for (int l = 0; l < all_course.course[i].num_wait; l++)
                {
                    pthread_mutex_lock(&all_course.course[i].lock);
                    pthread_cond_signal(&all_course.course[i].check);
                    pthread_mutex_unlock(&all_course.course[i].lock);
                }
                printf(CYN "Tutorial has started for Course %s with %d seats filled out of %d\n", all_course.course[i].name, all_course.course[i].num_wait, j);
                pthread_mutex_lock(&all_course.course[i].lock);
                all_course.course[i].num_wait = 0;
                pthread_mutex_unlock(&all_course.course[i].lock);
            }
            labss.lab[all_course.course[i].lab[r]].ta[p].ta_curr = 0;
            printf(RED "TA %d from lab %s has completed the tutorial and left the course %s\n", p, name, all_course.course[i].name);
        }
    }
    //printf("NO");
}

int main()
{
    char space;
    scanf("%d%d%d", &students.num, &labss.num_labs, &all_course.num);
    scanf("%c", &space);
    for (int i = 0; i < all_course.num; i++)
    {
        scanf("%s%f%d%d", &all_course.course[i].name, &all_course.course[i].interest, &all_course.course[i].course_max_slot, &all_course.course[i].num_labs);
        for (int j = 0; j < all_course.course[i].num_labs; j++)
        {
            scanf("%d", &all_course.course[i].lab[j]);
        }
        all_course.course[i].num = i;
        all_course.course[i].available = 0;
        all_course.course[i].exists = 1;
        scanf("%c", &space);
    }

    for (int i = 0; i < students.num; i++)
    {
        scanf("%f%d%d%d%f", &students.student[i].calibre, &students.student[i].pref1, &students.student[i].pref2, &students.student[i].pref3, &students.student[i].time);
        num_students = students.num;
    }

    for (int i = 0; i < labss.num_labs; i++)
    {
        scanf("%c%s%d%d", &space, &labss.lab[i].name, &labss.lab[i].num_ta, &labss.lab[i].limit);
        labss.lab[i].num = i;
        for (int j = 0; j < labss.lab[i].num_ta; j++)
        {
            labss.lab[i].ta[j].ta_time = 0;
            labss.lab[i].ta[j].ta_num = j;
        }
        labss.lab[i].exists = 1;
    }
    for (int i = 0; i < students.num; i++)
    {
        pthread_create(&registration[i], NULL, &student, i);
    }
    for (int i = 0; i < all_course.num; i++)
    {
        pthread_create(&course_off[i], NULL, &course_offered, i);
    }

    pthread_create(&hello, NULL, exiting, NULL);

    for (int i = 0; i < students.num; i++)
    {
        pthread_join(registration[i], NULL);
    }
    for (int i = 0; i < all_course.num; i++)
    {
        pthread_join(course_off[i], NULL);
    }
    pthread_join(hello, NULL);
}

/* 10 3 4
SMAI 0.8 3 2 0 2
NLP 0.95 4 1 0
CV 0.9 2 2 1 2
DSA 0.75 5 3 0 1 2
0.8 0 3 1 1
0.6 3 1 2 3
0.85 2 1 0 1
0.5 1 2 3 2
0.75 0 2 1 3
0.95 1 0 2 2
0.4 3 0 2 3
0.1 0 3 1 2
0.85 1 0 3 1
0.3 0 1 2 1
PRECOG 3 1
CVIT 4 2
RRC 1 3 */