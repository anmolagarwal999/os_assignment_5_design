#ifndef COLORS_H
#define COLORS_H
/******** COLORS ********/

#define RED "\033[1;31m"
#define GREEN "\033[1;32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#endif
