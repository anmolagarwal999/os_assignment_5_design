#include "functions.h"
#include <stdbool.h>
#include "colors.h"

// #defines
#define fo(x, y) for(int x = 0; x < y; ++x)

// Global variables
int nCourses, nStudents, nLabs, nCoursesLeft, timeNow;

// Structs variables
ptr_course_struct *course;
ptr_student_struct *student;
ptr_lab_struct *lab;
pthread_t time_thread;

// mutex and condition locks
pthread_mutex_t lockStudent, lockCheckTA, *lockLab;
pthread_cond_t checkTime = PTHREAD_COND_INITIALIZER, checkTA = PTHREAD_COND_INITIALIZER;

int main()
{
    // Input variables
    scanf("%d%d%d", &nStudents, &nLabs, &nCourses);
    nCoursesLeft = nCourses;
    course = (ptr_course_struct *)(malloc(sizeof(ptr_course_struct) * nCourses));

    // Initialize course structs
    fo(i, nCourses)
    {
        course[i] = malloc(sizeof(CourseStruct));
        float interest;
        int maxSlot, nLabs;
        scanf("%s %f %d %d", course[i]->name, &interest, &maxSlot, &nLabs);

        course[i]->interest = interest;
        course[i]->course_max_slot = maxSlot;
        course[i]->status = WAIT_TA;
        course[i]->no_of_labs = nLabs;
        course[i]->lab_list = malloc(sizeof(int) * nLabs);
        course[i]->course_id = i;

        if (!course[i]->lab_list)
            return 1;
        fo (j, course[i]->no_of_labs)
            scanf("%d", &course[i]->lab_list[j]);
    }

    // Initialize student structs
    student = (ptr_student_struct *)(malloc(sizeof(ptr_student_struct) * nStudents));
    fo(i, nStudents)
        student[i] = (ptr_student_struct)malloc(sizeof(StudentStruct)), student[i]->student_id = i, student[i]->status = NOT_REG,
        scanf("%f%d%d%d%d", &student[i]->calibre, &student[i]->pref1, &student[i]->pref2, &student[i]->pref3, &student[i]->time);

    // Initialize lab structs
    lab = (ptr_lab_struct *)(malloc(sizeof(ptr_lab_struct) * nLabs));

    fo(i, nLabs)
    {
        lab[i] = (ptr_lab_struct)malloc(sizeof(LabStruct)), lab[i]->lab_id = i;

        int taNum, maxTime;
        scanf("%s%d%d", lab[i]->name, &taNum, &maxTime);

        lab[i]->TA_num = taNum;
        lab[i]->max_times = maxTime;
        lab[i]->TA = malloc(sizeof(int *) * lab[i]->TA_num);

        fo(j, lab[i]->TA_num)
            lab[i]->TA[j] = (int *)malloc(sizeof(int) * 2), lab[i]->TA[j][0] = NT_ALTD, lab[i]->TA[j][1] = 0;

        lab[i]->status = lab[i]->max_times && lab[i]->TA_num ? NT_EXHSTD : EXHAUSTED;
    }

    // Initialize mutex and condition locks
    pthread_t thread_students[nStudents];
    pthread_t thread_courses[nCourses];
    srand(time(0));
    pthread_mutex_init(&lockStudent, NULL), pthread_mutex_init(&lockCheckTA, NULL);

    lockLab = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * nLabs);
    if (lockLab == NULL)
        return 6;

    // Initialize lab mutex locks
    fo(labs, nLabs)
        pthread_mutex_init(&lockLab[labs], NULL);

    fo(i, nStudents)
    {
        int *id = malloc(sizeof(int)); *id = i;
        if (pthread_create(&thread_students[i], NULL, &studentFunc, id))
            return 3;
    }

    // Initialize course mutex locks
    fo(i, nCourses)
    {
        int *id = malloc(sizeof(int));
        *id = i;
        if (pthread_create(&thread_courses[i], NULL, &coursesFunc, id))
            return 4;
    }

    // Initialize time thread
    if (pthread_create(&time_thread, NULL, &timerFunc, NULL))
        return 5;

    // Wait for all threads to finish
    fo(i, nStudents)
        pthread_join(thread_students[i], NULL);

    // Wait for all threads to finish
    pthread_join(time_thread, NULL);

    fo(i, nCourses)
        pthread_join(thread_courses[i], NULL);

    // Destroy mutex and condition locks
    pthread_mutex_destroy(&lockStudent);
    pthread_mutex_destroy(&lockCheckTA);

    // Destroy lab mutex locks
    fo(labs, nLabs)
        pthread_mutex_destroy(&lockLab[labs]);

    pthread_cond_destroy(&checkTime);
    pthread_cond_destroy(&checkTA);

    // Free memory
    fo(i, nCourses)
        free(course[i]);

    free(course);

    fo(i, nStudents)
        free(student[i]);

    free(student);

    fo(i, nLabs)
    {
        fo(j, lab[i]->TA_num)
            free(lab[i]->TA[j]);
        free(lab[i]->TA);
        free(lab[i]);
    }
    free(lab);
    free(lockLab);

    return 0;
}
