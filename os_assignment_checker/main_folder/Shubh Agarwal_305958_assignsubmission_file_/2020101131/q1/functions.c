#include "functions.h"
#include "colors.h"
#include <stdbool.h>

#define fo(x, y) for(int x = 0; x < y; ++x)

// Print to console
void log0(int a, char* b)
{
    printf(CYAN "Student %d has been allocated a seat in course %s\n" RESET, a, b);
}

void log1(int i)
{
    printf(GREEN "Student %d has filled in preferences for course registration\n" RESET, i);
}

void log3(char* name)
{
    printf(RED "Course %s doesnt have any TAs eligible and is removed from the course offerings\n" RESET, name);
}

void log4(int a, char* b, char* c, int d)
{
    printf(MAGENTA "TA %d from lab %s has been allocated to course %s for his %d TA ship\n" RESET, a, b, c, d);
}

void log5(char* a, int b)
{
    printf(BLUE "Course %s has been alloted %d seats\n" RESET, a, b);
}

void log6(char* a, int b, int c)
{
    printf(YELLOW "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, a, b, c);
}

void log7(int a, char* b)
{
    printf(BLUE "Student %d has selected course %s permanently\n" RESET, a, b);
}

void log8(int a, char* b)
{
    printf(RED "Student %d has withdrawn from course %s\n" RESET, a, b);
}

void log9(int a)
{
    printf("Student %d couldnt get any of his preferred courses\n", a);
}

void log11(int a, char* b, char* c)
{
    printf("Student %d has changed current preference from %s to %s\n", a, b, c);
}

void log12(int a, char* b, char* c)
{
    printf("Student %d has changed current preference from %s to %s\n", a, b, c);
}

void log13(int a)
{
    printf("Student %d couldnt get any of his preferred courses\n", a);
}

void log14(int a, char* b, char* c)
{
    printf("TA %d from lab %s has completed the tutorial and left the course %s\n", a, b, c);
}

void log15(char* a)
{
    printf("Lab %s no longer has students available for TA ship\n", a);
}

// Returns true if all courses are over or all students have been permanently allocated or have exited
// Returns false otherwise
int isExit(void)
{
    int s, c;
    s = c = 0;
    int i = -1;

    int studentLeftID[nStudents];

    while (++i < nCourses) if (WTDRN != course[i]->status) c++; // Counts the number of courses that have not been filled
    i = -1;
    while (++i < nStudents ) if (PERM_C1 > student[i]->status) studentLeftID[s] = i, s++; // Counts the number of students that have not been allocated a course

#define st student[studentLeftID[i]]->status
#define pr student[studentLeftID[i]]->pref1

    if (c && s) // both of them are true
    {
        fo(i, s)
            if (st < WAIT_C1)
                return 0;
            else if (course[pr]->status < WTDRN)
                if (st == WAIT_C1 || st == WAIT_C2 || st == WAIT_C3)
                    return 0;
        return 1;
    }
    else
        return 1;
#undef st
#undef pr
}

void *timerFunc()
{
    while(true) // Infinite loop to keep timer running
    {
        sleep(1);
        ++timeNow;

        if (isExit() && timeNow % 5 == 0) // condition to exit
            return NULL;

        pthread_cond_broadcast(&checkTime);
    }
    return NULL;
}

int TAAvailable(int courseNum, int *currentTA, int *currentLab)
{
    int isTALeft = 0;
    fo(l, course[courseNum]->no_of_labs) // loop through all labs
    {
        int allowedLab = course[courseNum]->lab_list[l];
        if (lab[allowedLab]->status == EXHAUSTED)
            continue;
        fo(i, lab[allowedLab]->TA_num) // loop through all TAs
        {
            if (NT_ALTD == lab[allowedLab]->TA[i][0] && lab[allowedLab]->max_times > lab[allowedLab]->TA[i][1])
            {
                pthread_mutex_lock(&lockLab[allowedLab]);
                *currentTA = i, *currentLab = allowedLab;
                lab[*currentLab]->TA[*currentTA][0] = TA_BUSY, lab[*currentLab]->TA[*currentTA][1]++;
                pthread_mutex_unlock(&lockLab[allowedLab]);
                return 0;
            }
            isTALeft = (isTALeft == 0 && lab[allowedLab]->TA[i][1] < lab[allowedLab]->max_times)? 1 : isTALeft; // if any TA is left
        }
    }
    *currentTA = *currentLab = -1;
    return isTALeft? -1 : -2; // Returns -1 if any TA is left, -2 if no TA is left
}

void *studentFunc(void *index)
{
    int i = *(int*)index;
    pthread_mutex_lock(&lockStudent);

    while (student[i]->time > timeNow) // Wait for the time to be right
        pthread_cond_wait(&checkTime, &lockStudent);

    student[i]->status = WAIT_C1; // Change status to WAIT_C1
    pthread_mutex_unlock(&lockStudent); // Unlock the mutex
    log1(i);

    return false;
}

void *coursesFunc(void *index)
{
    int courseID = *(int *)index, currentTA, currentLab; // Declaring variables
    sleep(2); // Wait for the student to be allocated a course

    while (true)
    {
        int eligible = TAAvailable(courseID, &currentTA, &currentLab); // Check if TA is available

        if (eligible == -2)
        {
            log3(course[courseID]->name);
            --nCoursesLeft, course[courseID]->status = WTDRN;
            return 0;
        }

        while (1 < nCoursesLeft && 0 > currentTA)
        {
            int run = true;
            pthread_mutex_lock(&lockCheckTA);

            while (run)
                pthread_cond_wait(&checkTA, &lockCheckTA), run = false;

            pthread_mutex_unlock(&lockCheckTA), eligible = TAAvailable(courseID, &currentTA, &currentLab);
        }

        if (eligible == -2)
        {
            log3(course[courseID]->name);
            --nCoursesLeft, course[courseID]->status = WTDRN;
            return 0;
        }

        log4(currentTA, lab[currentLab]->name, course[courseID]->name, lab[currentLab]->TA[currentTA][1]);
        updateLabStatus();
        course[courseID]->status = WAIT_SLOT;
        int slots = (rand() % (course[courseID]->course_max_slot)) + 1;
        int W = 0, studsInterested[nStudents];
        log5(course[courseID]->name, slots);

#define st studsInterested[W]
#define sts studsInterested[s]
#define st1 student[s]->status

        for (int s = 0; slots > W && nStudents > s; ++s)
            if (st1 == WAIT_C1 && student[s]->pref1 == courseID)
                st = s, W++;
            else if (st1 == WAIT_C2 && student[s]->pref2 == courseID)
                st = s, W++;
            else if (st1 == WAIT_C3 && student[s]->pref3 == courseID)
                st = s, W++;
            else
                continue;

        course[courseID]->status = TUT_ON;
        fo(s, W)
            log0(sts, course[courseID]->name), student[sts]->status -= 3;

        log6(course[courseID]->name, W, slots);
        sleep(5);
        int chosen = 0;
        double probability;
        fo(s, W)
        {
            probability = (course[courseID]->interest) * (student[sts]->calibre);
            if (probability > 0.5)
                ++chosen,
                log7(sts, course[courseID]->name), student[sts]->status += 6;
            else
            {
                log8(sts, course[courseID]->name);

                if (ATD_TUT_C3 == student[sts]->status)
                {
                    log9(sts), student[sts]->status = STUD_EXIT;
                    continue;
                }

                (ATD_TUT_C1 == student[sts]->status)?
                    log11(sts, course[student[sts]->pref1]->name, course[student[sts]->pref2]->name):
                    log11(sts, course[student[sts]->pref2]->name, course[student[sts]->pref3]->name);

                student[sts]->status += 4;

                if (course[student[sts]->pref2]->status == WTDRN || course[student[sts]->pref3]->status == WTDRN && student[sts]->status)
                    if (student[sts]->status == WAIT_C2)
                        log12(sts, course[student[sts]->pref2]->name, course[student[sts]->pref3]->name), student[sts]->status++;
                    else if (student[sts]->status == WAIT_C3)
                        log13(sts), student[sts]->status = STUD_EXIT;
                    else;
                else;
            }
        }

        log14(currentTA, lab[currentLab]->name, course[courseID]->name);
        lab[currentLab]->TA[currentTA][0] = NT_ALTD;
        course[courseID]->status = WAIT_TA, pthread_cond_broadcast(&checkTA);
    }
    return false;
}

int updateLabStatus()
{
    int notExhausted = 0;
    fo(labs, nLabs)
    {
        if (lab[labs]->status == EXHAUSTED)
            continue;
        notExhausted = 0;
        fo(tas, lab[labs]->TA_num)
            if (lab[labs]->max_times > lab[labs]->TA[tas][1])
            {
                notExhausted = true;
                break;
            }

        if (notExhausted == false)
        {
            pthread_mutex_lock(&lockLab[labs]);
            lab[labs]->status = EXHAUSTED;
            pthread_mutex_unlock(&lockLab[labs]);
            log15(lab[labs]->name);
        }
    }
    return 0;
}
