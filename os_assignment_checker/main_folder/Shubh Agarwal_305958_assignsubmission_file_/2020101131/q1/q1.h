#ifndef Q1_H
#define Q1_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define NOT_REG false
#define ATD_TUT_C1 true
#define ATD_TUT_C2 2
#define ATD_TUT_C3 3
#define STUD_EXIT 10

#define WAIT_TA false
#define WAIT_SLOT true
#define TUT_ON 2
#define WTDRN 3

#define EXHAUSTED true
#define NT_EXHSTD false

#define WAIT_C1 4
#define WAIT_C2 5
#define WAIT_C3 6
#define PERM_C1 7
#define PERM_C2 8
#define PERM_C3 9
#define NT_ALTD false
#define TA_BUSY true

typedef struct CourseStruct CourseStruct;
typedef struct StudentStruct StudentStruct;
typedef struct LabStruct LabStruct;

typedef CourseStruct *ptr_course_struct;
typedef StudentStruct *ptr_student_struct;
typedef LabStruct *ptr_lab_struct;

struct CourseStruct
{
    int course_id;
    char name[50];
    float interest;
    int course_max_slot;
    int no_of_labs;
    int *lab_list;
    int status;
};

struct StudentStruct
{
    float calibre;
    int student_id;
    int time;
    int pref1,pref2,pref3;
    int status;
};

struct LabStruct
{
    int lab_id;
    int status;
    int TA_num;
    char name[50];
    int max_times;
    int **TA;
};

#endif
