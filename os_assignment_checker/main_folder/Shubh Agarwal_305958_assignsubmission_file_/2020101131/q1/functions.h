#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "q1.h"

int isExit(void);
int updateLabStatus();
void *timerFunc();
void *studentFunc(void *index);
int TAAvailable(int courseNum, int *currentTA, int *currentLab);
void *coursesFunc(void *index);

extern int nCourses, nStudents, nLabs, nCoursesLeft, timeNow;
extern ptr_course_struct *course;
extern ptr_student_struct *student;
extern ptr_lab_struct *lab;
extern pthread_t time_thread;

extern pthread_mutex_t lockStudent, lockCheckTA, *lockLab;
extern pthread_cond_t checkTime, checkTA;

#endif
