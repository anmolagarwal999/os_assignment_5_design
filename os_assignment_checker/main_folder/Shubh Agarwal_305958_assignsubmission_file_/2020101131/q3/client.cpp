#include <cstdio>
#include <netdb.h>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

using namespace std;

constexpr int MAXBUFFERSIZE = 1024;
constexpr int PORT = 8080;

class Data
{
public:
    Data(int client_id, string command)
        : client_id(client_id), command(command) {}

    Data() = default;

	int client_id;
	string command;
};

void func(int sockfd, Data& data)
{
	write(sockfd, data.command.c_str(), data.command.length());
	char buff[MAXBUFFERSIZE] = {0};
	read(sockfd, buff, MAXBUFFERSIZE);

    cout << data.client_id << ": " << buff << endl;
}

void* client(void* args)
{
    // Tokenize string and store in vector
    vector<string> tokens;

    Data data = *(Data*)args;

    stringstream temp(data.command);
    string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(temp, intermediate, ' '))
        tokens.push_back(intermediate);

    // Printing the token vector
	if(tokens.size() < 2)
	{
        cout << "Command doesn't exist\n";
		return NULL;
	}

    // Convert string to integer
    sleep(stoi(tokens[0]));

	int sockfd, connfd;
	sockaddr_in servaddr, cli;

	// socket create and varification
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		exit(0);

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	// connect the client socket to server socket
	if (connect(sockfd, (sockaddr *)&servaddr, sizeof(servaddr)) != 0)
		exit(0);

	func(sockfd, data);
	close(sockfd);

    return NULL;
}

int main()
{
    // Input n clients
	int nClients;
    cin >> nClients;
    getchar();

    // Create a thread for each client
    vector<pthread_t> client_threads(nClients);
    vector<Data> datas(nClients);

	for(int i = 0, j = 0; i < nClients; i++, j = 0)
    {
        getline(cin, datas[i].command);
        datas[i].client_id = i;
        pthread_create(&client_threads[i], NULL, client, &datas[i]);
    }

    // wait for all threads to finish
	for(int i = 0; i < nClients; i++)
        pthread_join(client_threads[i], NULL);

	return 0;
}
