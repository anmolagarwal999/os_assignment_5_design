#ifndef __ENTITIES_H__
#define __ENTITIES_H__

#include <pthread.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_BLACK   "\x1b[30m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Structure representing the entity spectator
typedef struct spectator{
    int id;                                         // Spectator ID of group
    char name[30];                                  // Name of person
    char fanbase;                                   // Home, Away or Neutral fan
    int arrival_time;                               // Time of arrival at the stadium
    int patience_time;                              // Patience value of person
    int humiliation_threshold;                      // Max no of opposition goals a spectator can bear watching
    int grpid;                                      // Group ID to which person belongs
    int wait_index;                                 // Index at which person is waiting for a seat in the stadium
    int cur_time;                                   // Number of seconds elapsed since start of simulation
    int is_watching;                                // State of spectator
    
    sem_t wait_seat;                                // Semaphore used to wait for vacant seat in stadium
    sem_t spectate;                                 // Semaphore used to spectate match
    sem_t wait_gate;                                // Semaphore used to wait at the exit gate
}spectator,*Spectator;

typedef struct group
{
    int num_people;                                 // Number of people in the group
    int num_people_exited;                          // Number of people waiting at the exit gate
    Spectator *people;                              // Array of persons/spectators
    pthread_t *spectator_thread_ids;                // Array of person/spectator thread IDs
    pthread_mutex_t exit_lock;                      // Mutex lock to ensure thread safety of group objects
}group,*Group;

typedef struct chance
{
    char team;                                      // Home or Away team
    int time;                                       // Time in seconds after which a goal scoring chance arises
    float conversion_prob;                          // Probability of conversion into goal
}chance,*Chance;

typedef struct zone{
    char name;                                      // Name of the zone (H,N,A)
    int num_seats;                                  // Max no of seats available in the zone
    int num_seats_left;                             // No of vacant seats left
}zone,*Zone;

int spectating_time;                                
int num_groups;
int num_chances;
int scoreline[2];                                   // Scoreline in the format {Home goals,Away goals}

Group *Groups;
Chance *Chances;
Zone H,N,A;                                         // Home, Away and Neutral zones
Spectator Waiting_list[30];                         // List of people waiting for a vacant seat in the stadium
int num_waiting;                                    // Number of waiting people

pthread_mutex_t Spect_lock;                         // Mutex lock to ensure thread safety of global variables

void *spectatorSim(void *spect);
void *chanceSim(void *arg);

#endif