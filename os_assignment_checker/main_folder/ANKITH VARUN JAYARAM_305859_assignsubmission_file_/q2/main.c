#include "entities.h"
#include <stdio.h>
#include <stdlib.h>

// Run simulation
void runSim()
{
    // Create spectator threads
    for(int i = 0 ; i < num_groups ; i++)
    {
        Groups[i]->spectator_thread_ids = (pthread_t *) malloc(Groups[i]->num_people*sizeof(pthread_t));        
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
        {
            pthread_t curr_td;
            pthread_create(&curr_td,NULL,spectatorSim,(void *)Groups[i]->people[j]);
            Groups[i]->spectator_thread_ids[j] = curr_td;
        }
    }

    // Create chance thread
    pthread_t chances_thread_id;
    pthread_create(&chances_thread_id,NULL,chanceSim,NULL);

    // Join spectator threads
    for(int i = 0 ; i < num_groups ; i++)
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
            pthread_join(Groups[i]->spectator_thread_ids[j],NULL);

    // Join chance thread
    pthread_join(chances_thread_id,NULL);
}

// Take input and initialise all entities of the simulation 
void input()
{
    H = (Zone) malloc(sizeof(zone));
    A = (Zone) malloc(sizeof(zone));
    N = (Zone) malloc(sizeof(zone));
    
    scanf("%d %d %d",&H->num_seats,&A->num_seats,&N->num_seats);
    scanf("%d",&spectating_time);
    
    scanf("%d",&num_groups);
    Groups = (Group *) malloc(num_groups*sizeof(group));
    for(int i = 0 ; i < num_groups ; i++)
    {
        Groups[i] = (Group) malloc(sizeof(group));
        scanf("%d",&Groups[i]->num_people);
        Groups[i]->people = (Spectator *) malloc(Groups[i]->num_people*sizeof(spectator));
        Groups[i]->num_people_exited = 0;
        pthread_mutex_init(&Groups[i]->exit_lock,NULL);
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
        {
            Groups[i]->people[j] = (Spectator) malloc(sizeof(spectator));
            scanf("%s %c %d %d %d",Groups[i]->people[j]->name,&Groups[i]->people[j]->fanbase,&Groups[i]->people[j]->arrival_time,&Groups[i]->people[j]->patience_time,&Groups[i]->people[j]->humiliation_threshold);
            Groups[i]->people[j]->grpid = i;
            Groups[i]->people[j]->is_watching = 0;
            Groups[i]->people[j]->id = j;
            sem_init(&Groups[i]->people[j]->wait_seat,0,0);
            sem_init(&Groups[i]->people[j]->spectate,0,0);
            sem_init(&Groups[i]->people[j]->wait_gate,0,0);
        }
    }

    scanf("%d",&num_chances);
    Chances = (Chance *) malloc(num_chances*sizeof(chance));
    for(int i = 0 ; i < num_chances ; i++)
    {
        Chances[i] = (Chance) malloc(sizeof(chance));
        scanf("\n%c %d %f",&Chances[i]->team,&Chances[i]->time,&Chances[i]->conversion_prob);
    }

    H->name = 'H';
    H->num_seats_left = H->num_seats;

    A->name = 'A';
    A->num_seats_left = A->num_seats;

    N->name = 'N';
    N->num_seats_left = N->num_seats;

    num_waiting = 0;
    pthread_mutex_init(&Spect_lock,NULL);

    scoreline[0] = 0;
    scoreline[1] = 0;
}

int main()
{
    input();
    runSim();
}