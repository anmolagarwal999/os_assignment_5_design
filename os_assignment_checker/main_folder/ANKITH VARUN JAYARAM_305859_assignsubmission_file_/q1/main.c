#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "entities.h"

void runSim()
{
    pthread_t student_thread_ids[num_students], course_thread_ids[num_courses], lab_thread_ids[num_labs];
    for(int i = 0 ; i < num_students ; i++)
    {
        pthread_t curr_td;
        pthread_create(&curr_td,NULL,studentSim,(void *)(Students[i]));
        student_thread_ids[i] = curr_td;
    }

    for(int i = 0 ; i < num_courses ; i++)
    {
        pthread_t curr_td;
        pthread_create(&curr_td,NULL,courseSim,(void *)(Courses[i]));
        course_thread_ids[i] = curr_td;
    }

    for(int i = 0 ; i < num_students ; i++)
        pthread_join(student_thread_ids[i],NULL);
    
    for(int i = 0 ; i < num_courses ; i++)
        pthread_join(course_thread_ids[i],NULL);
}

void input()
{
    scanf("%d %d %d",&num_students,&num_labs,&num_courses);     // Get number of courses, labs and students

    init_courses();
    init_students();
    init_labs();
}

int main()
{
    input();
    runSim();
}