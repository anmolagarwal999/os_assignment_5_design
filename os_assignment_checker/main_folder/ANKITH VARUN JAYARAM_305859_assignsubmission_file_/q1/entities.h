#ifndef __ENTITIES_H__
#define __ENTITIES_H__

#include <semaphore.h>
#include <pthread.h>

typedef struct student{
    int id;
    float caliber;
    int prefs[3];
    int cur_pref;
    int time;
    sem_t wait_course;
    sem_t take_tut;
    sem_t wait_stud;
    sem_t wait_tut;
}student,*Student;

typedef struct course{
    char name[30];
    int id;
    float interest;
    int course_max_slots;
    int num_accepted_labs;
    int *accepted_labs;
    int allocated_TA;
    int allocated_lab;
    int num_seats_occupied;
    int* students_enrolled;
    int is_available;
    int wait;

    sem_t Student_wait;
    sem_t TA_wait;    

    pthread_mutex_t Student_lock;
}course,*Course;

typedef struct TA{
    int id;
    int is_free;
    int num_TAship;
    pthread_mutex_t TA_lock;
}ta,*TA;

typedef struct lab{
    char name[30];
    int id;
    int num_TAs;
    int mentor_max_times;
    int num_TAs_left;
    TA *TAs;
    int is_TA_available;
    sem_t start_allocation;
    int num_courses_allotted;
    int* courses_allotted;
}lab,*Lab;

#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_RESET   "\x1b[0m"

const int num_students, num_labs, num_courses;
Student *Students;
Course *Courses;
Lab *Labs;
int students_available;

void init_courses();
void init_students();
void init_labs();

void *courseSim(void *crs);
void *studentSim(void *stud);

#endif