#include "entities.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_courses()
{
    Courses = (Course *) malloc(num_courses*sizeof(course));
    for(int i = 0 ; i < num_courses ; i++)
    {
        Courses[i] = (Course) malloc(sizeof(course));
        scanf("%s %f %d %d",Courses[i]->name,&Courses[i]->interest,&Courses[i]->course_max_slots,&Courses[i]->num_accepted_labs);
        Courses[i]->id = i;
        Courses[i]->accepted_labs = (int *) malloc(Courses[i]->num_accepted_labs*sizeof(int));
        for(int j = 0 ; j < Courses[i]->num_accepted_labs ; j++)
            scanf("%d ",&Courses[i]->accepted_labs[j]);
        
        Courses[i]->is_available = 1;
        Courses[i]->allocated_TA = -1;
        Courses[i]->allocated_lab = -1;
        Courses[i]->num_seats_occupied = 0;
        Courses[i]->students_enrolled = (int *) malloc(Courses[i]->course_max_slots*sizeof(int));
        Courses[i]->wait = 0;

        sem_init(&Courses[i]->Student_wait,0,0);
        sem_init(&Courses[i]->TA_wait,0,0);

        pthread_mutex_init(&Courses[i]->Student_lock,NULL);
    }
}

void init_students()
{
    Students = (Student *) malloc(num_students*sizeof(student));
    for(int i = 0 ; i < num_students ; i++)
    {
        Students[i] = (Student) malloc(sizeof(student));
        Students[i]->id = i;
        Students[i]->cur_pref = 0;
        sem_init(&Students[i]->take_tut,0,0);
        sem_init(&Students[i]->wait_course,0,0);
        sem_init(&Students[i]->wait_stud,0,0);
        sem_init(&Students[i]->wait_tut,0,0);
        scanf("%f %d %d %d %d",&Students[i]->caliber,&Students[i]->prefs[0],&Students[i]->prefs[1],&Students[i]->prefs[2],&Students[i]->time);
    }
}

void init_labs()
{
    Labs = (Lab *) malloc(num_labs*sizeof(lab));
    for(int i = 0 ; i < num_labs ; i++)
    {
        Labs[i] = (Lab) malloc(sizeof(lab));
        scanf("%s %d %d",Labs[i]->name,&Labs[i]->num_TAs,&Labs[i]->mentor_max_times);
        Labs[i]->id = i;
        Labs[i]->num_TAs_left = Labs[i]->num_TAs;
        Labs[i]->TAs = (TA *) malloc(Labs[i]->num_TAs*sizeof(ta));
        
        for(int j = 0 ; j < Labs[i]->num_TAs ; j++)
        {
            Labs[i]->TAs[j] = (TA) malloc(sizeof(ta));
            Labs[i]->TAs[j]->id = i;
            Labs[i]->TAs[j]->is_free = 1;
            Labs[i]->TAs[j]->num_TAship = 0;
            pthread_mutex_init(&Labs[i]->TAs[j]->TA_lock,NULL);
        }

        Labs[i]->courses_allotted = (int *) malloc(num_courses*sizeof(int));
        Labs[i]->num_courses_allotted = 0;
        
        for(int j = 0 ; j < num_courses ; j++)
        {
            for(int k = 0 ; k < Courses[j]->num_accepted_labs ; k++)
                if(i == Courses[j]->accepted_labs[k])
                    Labs[i]->courses_allotted[Labs[i]->num_courses_allotted++] = j;
        }
        
        Labs[i]->is_TA_available = 1;
        sem_init(&Labs[i]->start_allocation,0,0);
    }
}

int* chooseTA(Course Crs)
{
    int *allocatedTA = (int *) malloc(2*sizeof(int));
    allocatedTA[0] = -1;
    allocatedTA[1] = -1;

    for(int i = 0 ; i < Crs->num_accepted_labs && allocatedTA[1] == -1 ; i++)
    {
        for(int j = 0 ; j < Labs[Crs->accepted_labs[i]]->num_TAs && allocatedTA[1]  == -1 && Labs[Crs->accepted_labs[i]]->is_TA_available == 1 ; j++)
        {
            pthread_mutex_lock(&Labs[Crs->accepted_labs[i]]->TAs[j]->TA_lock);
            if(Labs[Crs->accepted_labs[i]]->TAs[j]->is_free == 1 && Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship <= Labs[Crs->accepted_labs[i]]->mentor_max_times)
            { 
                allocatedTA[0] = i;
                allocatedTA[1] = j;
                Labs[Crs->accepted_labs[i]]->TAs[j]->is_free = 0;
                Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship++;
                printf("TA %d from lab %s has been allocated to course %s " ANSI_COLOR_MAGENTA "for " ANSI_COLOR_RESET "his ",j,Labs[Crs->accepted_labs[i]]->name,Crs->name);
                printf((Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship == 1) ? "1st TA ship\n" : ((Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship == 2) ? "2nd TA ship\n" : ((Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship == 3) ? "3rd TA ship\n" : "%dth TA ship\n")),Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship);
                sleep(1);

                if(Labs[Crs->accepted_labs[i]]->TAs[j]->num_TAship == Labs[Crs->accepted_labs[i]]->mentor_max_times)
                    Labs[Crs->accepted_labs[i]]->num_TAs_left--;

                if(Labs[Crs->accepted_labs[i]]->num_TAs_left == 0 && Labs[Crs->accepted_labs[i]]->is_TA_available == 1)
                {
                    printf("Lab %s no longer has students available " ANSI_COLOR_MAGENTA "for " ANSI_COLOR_RESET "TA ship\n",Labs[Crs->accepted_labs[i]]->name);
                    Labs[Crs->accepted_labs[i]]->is_TA_available = 0;
                }
            }
            pthread_mutex_unlock(&Labs[Crs->accepted_labs[i]]->TAs[j]->TA_lock);
        }
    }

    return allocatedTA;
}

int chooseSeats(Course Crs)
{
    srand(time(NULL));
    return (random() % Crs->course_max_slots) + 1;
}

int checkStudent(Course Crs)
{
    int num_students_available = 0;
    for(int i = 0 ; i < num_students ; i++)
    {
        if(Students[i]->cur_pref <= 3)
            for(int j = Students[i]->cur_pref - 1 ; j < 3 ; j++)
                if(Students[i]->prefs[j] == Crs->id)
                {
                    num_students_available++;
                    break;
                }
    }
    return num_students_available;
}

void *courseSim(void *crs)
{
    Course Crs = (Course) crs;
    sleep(1);
    while(1)
    {        
        // Allocate a free TA to the course
        while(Crs->allocated_TA == -1)
        {
            int labs_available = Crs->num_accepted_labs;
            for(int i = 0 ; i < Crs->num_accepted_labs ; i++)
                if(Labs[Crs->accepted_labs[i]]->is_TA_available == 0)
                    labs_available--;

            if(labs_available == 0)
            {
                printf("Course %s does not have any TA mentors eligible and is removed from course offerings\n",Crs->name);
                Crs->is_available = 0;
                for(int i = 0 ; i < num_students ; i++)
                    if(Crs->id == Students[i]->prefs[Students[i]->cur_pref - 1])
                    {
                        int val;
                        sem_getvalue(&Students[i]->wait_course,&val);
                        if(val == 0)
                            sem_post(&Students[i]->wait_course);
                    }
                return NULL;
            }
        
            int *Allocation = chooseTA(Crs);
            Crs->allocated_lab = Allocation[0];
            Crs->allocated_TA = Allocation[1];

            if(Crs->allocated_TA == -1)
            {
                Crs->wait == 1;
                sem_wait(&Crs->TA_wait);
            }
        }

        int num_seats = chooseSeats(Crs);
        printf("Course %s has been allocated %d seats\n",Crs->name,num_seats);
        sleep(1);

        while(Crs->num_seats_occupied == 0){
        int num_students_available = 0;
        for(int i = 0, j = 1 ; i < num_students && j <= num_seats ; i++)
        {
            if(Students[i]->cur_pref <= 3 && Students[i]->cur_pref > 0)
            {
                if(Students[i]->prefs[Students[i]->cur_pref - 1] == Crs->id)
                {    
                    sem_wait(&Students[i]->wait_stud);
                    Crs->students_enrolled[Crs->num_seats_occupied++] = i;
                    j++;
                    sem_post(&Students[i]->wait_course);
                }
    
                for(int j = Students[i]->cur_pref - 1 ; j < 3 ; j++)
                    if(Students[i]->prefs[j] == Crs->id)
                    {
                        num_students_available++;
                        break;
                    }
            }
        }

        if(num_students_available == 0)
        {
            Crs->is_available = 0;
            printf("Course %s is removed from course offerings due to non-availability of students\n",Crs->name);
            return NULL;
        }

        if(Crs->num_seats_occupied == 0)
            sem_wait(&Crs->Student_wait);    
        }

        // Start tut
        for(int i = 0 ; i < Crs->num_seats_occupied ; i++)
        {
            sem_wait(&Students[Crs->students_enrolled[i]]->wait_tut);
        }
        printf("Tutorial has started" ANSI_COLOR_MAGENTA " for " ANSI_COLOR_RESET "course %s with %d seat(s) filled out of %d\n",Crs->name,Crs->num_seats_occupied,num_seats);
        sleep(2);

        // End tut
        printf("TA %d from lab %s has completed the tutorial " ANSI_COLOR_MAGENTA "for " ANSI_COLOR_RESET "course %s\n",Crs->allocated_TA,Labs[Crs->accepted_labs[Crs->allocated_lab]]->name,Crs->name);
        sleep(1);

        for(int i = 0 ; i < Crs->num_seats_occupied ; i++)
            sem_post(&Students[Crs->students_enrolled[i]]->take_tut);

        Labs[Crs->accepted_labs[Crs->allocated_lab]]->TAs[Crs->allocated_TA]->is_free = 1;

        for(int i = 0 ; i < Labs[Crs->accepted_labs[Crs->allocated_lab]]->num_courses_allotted ; i++)
        {
            if(Courses[Labs[Crs->accepted_labs[Crs->allocated_lab]]->courses_allotted[i]]->wait == 1)
            {
                Courses[Labs[Crs->accepted_labs[Crs->allocated_lab]]->courses_allotted[i]]->wait = 0;
                sem_post(&Courses[Labs[Crs->accepted_labs[Crs->allocated_lab]]->courses_allotted[i]]->TA_wait);
            }
        }

        Crs->allocated_lab = -1;
        Crs->allocated_TA = -1;
        Crs->num_seats_occupied = 0;
    }
}

void *studentSim(void *stud)
{
    Student Stud = (Student) stud;
    
    sleep(Stud->time);
    printf("Student %d has filled" ANSI_COLOR_MAGENTA " in" ANSI_COLOR_RESET " preferences" ANSI_COLOR_MAGENTA " for" ANSI_COLOR_RESET " course registration\n",Stud->id);
    Stud->cur_pref = 1;
    sleep(1);

    while(Stud->cur_pref < 4)
    {
        sem_post(&Stud->wait_stud);
        sem_wait(&Stud->wait_course);

        if(Courses[Stud->prefs[Stud->cur_pref - 1]]->is_available == 1)
        {
            printf("Student %d has been allocated a seat " ANSI_COLOR_MAGENTA "in " ANSI_COLOR_RESET "course %s\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name);
            
            int val;
            pthread_mutex_lock(&Courses[Stud->prefs[Stud->cur_pref - 1]]->Student_lock);
            sem_getvalue(&Courses[Stud->prefs[Stud->cur_pref - 1]]->Student_wait,&val);
            if(val == 0)
                sem_post(&Courses[Stud->prefs[Stud->cur_pref - 1]]->Student_wait);
            pthread_mutex_unlock(&Courses[Stud->prefs[Stud->cur_pref - 1]]->Student_lock);
            sleep(1);
            
            sem_post(&Stud->wait_tut);
            sem_wait(&Stud->take_tut);
            

            if(Stud->caliber*Courses[Stud->prefs[Stud->cur_pref - 1]]->interest > 0.5)
            {
                printf("Student %d has selected the course %s permanently\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name);
                Stud->cur_pref = 4;
                return NULL;
            }
            else
            {
                printf("Student %d has withdrawn from the course %s\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name);
                sleep(1);
            }
        }

        if(Stud->cur_pref == 1 && Courses[Stud->prefs[Stud->cur_pref]]->is_available == 1)
        {
            printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name,Courses[Stud->prefs[Stud->cur_pref]]->name);
            Stud->cur_pref++;
        }
        else if(Stud->cur_pref == 1 && Courses[Stud->prefs[Stud->cur_pref]]->is_available == 0 && Courses[Stud->prefs[Stud->cur_pref + 1]]->is_available == 1)
        {
            printf("Student %d has changed current preference from %s (priority 1) to %s (priority 3)\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name,Courses[Stud->prefs[Stud->cur_pref + 1]]->name);
            Stud->cur_pref += 2;
        }
        else if(Stud->cur_pref == 2 && Courses[Stud->prefs[Stud->cur_pref]]->is_available == 1)
        {
            printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n",Stud->id,Courses[Stud->prefs[Stud->cur_pref - 1]]->name,Courses[Stud->prefs[Stud->cur_pref]]->name);
            Stud->cur_pref++;
        }
        else
        {
            printf("Student %d could not get any of his preferred courses\n",Stud->id);
            Stud->cur_pref += 2;
        }
        sleep(1);
    }
}