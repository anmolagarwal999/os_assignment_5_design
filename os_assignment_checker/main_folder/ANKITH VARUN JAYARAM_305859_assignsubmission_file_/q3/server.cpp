#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <cstring>
#include <queue>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <assert.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

// Structure representing a client request
typedef struct client{                                             
    int client_socket_fd;                           // File descriptor of the dedicated socket used for communication between server and client
    struct sockaddr_in client_addr_obj;             // Dedicated socket details
}client,*Client;

// Strcuture representing a worker thread
typedef struct worker_td{
    bool is_free;                                   // Represents the state of worker thread
    sem_t wait_req;                                 // Semaphore used to wait if no client request is available
    Client cl;                                      // Client request to be handled
}worker_td, *Worker_td;

int num_worker_tds;                                 // Number of worker threads
Worker_td *tds;                                     // Array representing pool of worker threads
sem_t wait_worker_tds;                              // Semaphore used to wait till a worker thread is free
sem_t start;                                        // Semaphore used to start assigning thread

queue <Client> client_reqs;                         // Queue of accepted client requests
pthread_mutex_t queue_lock;                         // Mutex lock to ensure thread safety of queue

map <int,string> server_dict;                       // Dictionary mantained by server
pthread_mutex_t dict_val_locks[101];                // Array of mutex locks to ensure thread safety of key-value pairs of dictionary

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define MAX_CLIENTS 100                             // Maximum number of clients a server can listen to
#define PORT_ARG 8001                               // Port on which the server is situated

const int initial_msg_len = 256;
const LL buff_sz = 1048576;

// Read incoming string from client
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

// Send outgoing string to client
int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

// Tokenise a string based on a delimiter
vector <string> split(string str, char ch)
{
    vector <string> tokens;
    for(int i = 0, j = 0 ; i < str.length() ; i++)
    {
        if(i == str.length() - 1)
            tokens.push_back(str.substr(j));
        else if(str[i] == ch && i != 0)
        {
            tokens.push_back(str.substr(j,i - j));
            j = i + 1;
        }
    }
    return tokens;
}

// Concatenate multiple strings using white spaces
string combine(vector <string> tokens, int i)
{
    string str = tokens[i];
    for(int j = i + 1 ; j < tokens.size() ; j++)
        str += " " + tokens[j];
    return str;
}

// Execute read/write operations on server dictionary
string executeCmd(string cmd)
{
    vector <string> tokens = split(cmd,' ');                                    // Parse the command
    string outMsg;

    if(tokens[0] == "insert")                                                   // Insert key-value pair into dictionary
    {
        int key = stoi(tokens[1]);
        if(server_dict.count(key))
            outMsg = "Key already exists";
        else
        {
            pthread_mutex_lock(&dict_val_locks[key]);
            server_dict.insert(pair<int,string>(key,combine(tokens,2)));
            outMsg = "Insertion successful";
            pthread_mutex_unlock(&dict_val_locks[key]);
        }
    }
    else if(tokens[0] == "delete")                                              // Delete a key-value pair from dictionary
    {
        int key = stoi(tokens[1]);
        if(server_dict.count(key))
        {
            pthread_mutex_lock(&dict_val_locks[key]);
            server_dict.erase(key);
            outMsg = "Deletion successful";
            pthread_mutex_unlock(&dict_val_locks[key]);
        }
        else
            outMsg = "No such key exists";
    }
    else if(tokens[0] == "update")                                              // Update a key-value pair in dictionary
    {
        int key = stoi(tokens[1]);
        auto itr = server_dict.find(key);
        if(itr != server_dict.end())
        {
            pthread_mutex_lock(&dict_val_locks[key]);
            itr->second = combine(tokens,2);
            outMsg = itr->second;
            pthread_mutex_unlock(&dict_val_locks[key]);
        }
        else
            outMsg = "Key does not exist";
    }
    else if(tokens[0] == "concat")                                              // Concatenate a pair of values based on two input keys in dictionary
    {
        int key1 = stoi(tokens[1]), key2 = stoi(tokens[2]);
        auto itr1 = server_dict.find(key1), itr2 = server_dict.find(key2);

        if(itr1 != server_dict.end() && itr2 != server_dict.end())
        {
            pthread_mutex_lock(&dict_val_locks[key1]);
            pthread_mutex_lock(&dict_val_locks[key2]);

            string temp = itr1->second;
            itr1->second += itr2->second;
            itr2->second += temp;
            outMsg = itr2->second;

            pthread_mutex_unlock(&dict_val_locks[key1]);
            pthread_mutex_unlock(&dict_val_locks[key2]);
        }
        else
            outMsg = "Concat failed as at least one of the keys does not exist";
    }
    else if(tokens[0] == "fetch")                                               // Fetch the value pointed by a key in dictionary
    {
        int key = stoi(tokens[1]);

        if(server_dict.count(key))
        {
            pthread_mutex_lock(&dict_val_locks[key]);
            outMsg = server_dict.at(key);
            pthread_mutex_unlock(&dict_val_locks[key]);
        }
        else
            outMsg = "Key does not exist";
    }
    else
        outMsg = "Invalid command";

    return outMsg;                                                              // Return the response to be sent back to the client
}

// Handle client request - receive, process and transmit
void handle_connection(Client cl)
{
    int received_num, sent_num;
    int ret_val = 1;

    // Read command string from socket
    string cmd;
    tie(cmd, received_num) = read_string_from_socket(cl->client_socket_fd, buff_sz);
    ret_val = received_num;
    if (ret_val <= 0)
    {
        printf("Server could not read msg sent from client\n");
    }
        
    // Execute the command followed by an artifical sleep of 2 secs    
    string outMsg = executeCmd(cmd);
    sleep(2); 

    // Write response string into socket
    int sent_to_client = send_string_on_socket(cl->client_socket_fd, outMsg);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
    }
    sleep(1);                                                                   // Wait for the response to reach the client
    close(cl->client_socket_fd);                                                // Close client request
}

// Function routine to simulate a worker thread
void *workerTdSim(void* wrk)
{
    Worker_td Wrk = (Worker_td) wrk;
    
    while(1)
    {
        sem_post(&wait_worker_tds);                     // Unlock assigning thread
        sem_wait(&Wrk->wait_req);                       // Wait till a client request is assigned to the worker thread

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(Wrk->cl->client_addr_obj.sin_port), inet_ntoa(Wrk->cl->client_addr_obj.sin_addr));
        handle_connection(Wrk->cl);
        printf(BRED "Disconnected from client on port number %d and IP %s" ANSI_RESET "\n",ntohs(Wrk->cl->client_addr_obj.sin_port), inet_ntoa(Wrk->cl->client_addr_obj.sin_addr));
        Wrk->is_free = true;                            // Free the worker thread
    }
}

// Function routine to simulate assigning thread
void *assignWorkerTd(void* arg)
{
    while(1)
    {
        sem_wait(&start);                               // Wait till a user request is accepted into waiting queue
        sem_wait(&wait_worker_tds);                     // Wait till at least one worker thread becomes free
        
        for(int i = 0 ; i < num_worker_tds ; i++)       // Iterate through each worker thread
            if(tds[i]->is_free)                         // Check if worker thread is free
            {
                pthread_mutex_lock(&queue_lock);
                tds[i]->cl = client_reqs.front();       // Assign client request to worker thread
                client_reqs.pop();                      // Remove client request from waiting queue
                pthread_mutex_unlock(&queue_lock);
                tds[i]->is_free = false;
                sem_post(&tds[i]->wait_req);            // Unlock worker thread
                break;
            }
    }
}

int main(int argc, char *argv[])
{
    // Initialise worker threads and spawn them
    num_worker_tds = atoi(argv[1]);
    tds = (Worker_td *) malloc(num_worker_tds*sizeof(worker_td));
    for(int i = 0 ; i < num_worker_tds ; i++)
    {
        tds[i] = (Worker_td) malloc(sizeof(worker_td));
        tds[i]->is_free = true;
        sem_init(&tds[i]->wait_req,0,0);

        pthread_t cur_td;
        pthread_create(&cur_td,NULL,workerTdSim,(void *)tds[i]);
    }

    // Initialise semaphores
    sem_init(&start,0,0);
    sem_init(&wait_worker_tds,0,0);

    // Initialise assigning thread and spawn it
    pthread_t assign_td;
    pthread_create(&assign_td,NULL,assignWorkerTd,NULL);

    // Initialise mutex locks
    pthread_mutex_init(&queue_lock,NULL);
    for(int i = 0 ; i <= 100 ; i++)
        pthread_mutex_init(&dict_val_locks[i],NULL);

    // Initialise and create welcome socket for server
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    // Initialise port and IP for welcome socket
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); 

    // Bind welcome socket to the above intitialised port and IP
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    // Listen for incoming connection requests 
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << BBLU "Server has started listening on the LISTEN PORT" << endl;
    clilen = sizeof(client_addr_obj);

    int i = 0;
    while (1)
    {
        // Accept a client request, create a dedicated socket and push it onto the waiting queue
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BYEL "Accepted client request and designated port number %d and IP %s to it\n" ANSI_RESET,ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        // Initialise client request object
        Client cl = (Client) malloc(sizeof(client));            
        cl->client_socket_fd = client_socket_fd;
        cl->client_addr_obj = client_addr_obj;

        // Push client request onto waiting queue
        pthread_mutex_lock(&queue_lock);
        client_reqs.push(cl);                                   
        pthread_mutex_unlock(&queue_lock);

        // Unlock assigning thread
        sem_post(&start);                                       
    }

    close(wel_socket_fd);
    return 0;
}