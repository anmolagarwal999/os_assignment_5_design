#include <iostream>
#include <string>
#include <cstring>
#include <tuple>
#include <vector>

#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

// Structure representing client request
typedef struct request{
    int time;                                                                       // Delay time                                                                  
    int id;                                                                         // Request ID
    string command;                                                                 // Command to be issued
}request,*Request;

typedef long long LL;
const LL MOD = 1000000007;

#define SERVER_PORT 8001
const LL buff_sz = 1048576;

// Create a TCP socket to communicate with the server and establish connection
int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }

    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj));                                             // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);                                                  // Convert to big-endian order

    // Connect to server
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    return socket_fd;
}

// Read incoming string from server
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

// Send outgoing string to server
int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }

    return bytes_sent;
}

// Handles user request
void begin_process(string to_send, int req_id)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);                                             // Create a TCP socket and establish connection to communicate with server

    send_string_on_socket(socket_fd, to_send);                                              // Send user request to server
    int num_bytes_read;
    string msg,output_msg;
    tie(msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);                 // Get server response 
    output_msg = to_string(req_id) + ":" + to_string(pthread_self()) + ":" + msg;
    cout << output_msg << endl;                                                             // Output appropriate message
}

// Function routine to simulate user request thread 
void *userReqSim(void *req)
{
    Request Req = (Request) req;
    sleep(Req->time);
    begin_process(Req->command,Req->id);
    return NULL;
}

// Scrape the first word from a string by splitting it based on a delimiter
pair <int,string> split(string s, char ch)
{
    int n;
    string str;
    for(int i = 0 ; i < s.length() ; i++)
    {
        if(s[i] == ch && i != 0)
        {
            n = stoi(s.substr(0,i));
            str = s.substr(i + 1);
            return {n,str};
        }
    }
    return{-1,"error"};
}

int main()
{
    int num_reqs;                                                                   // Number of user requests
    cin>>num_reqs;
    cin.ignore();

    Request *reqs = (Request *) malloc(num_reqs*sizeof(request));                   // Initialise array of user requests
    pthread_t *reqs_td_ids = (pthread_t *) malloc(num_reqs*sizeof(pthread_t));      // Initialise array of user request thread IDs

    for(int i = 0 ; i < num_reqs ; i++)
    {
        string input,command;
        int time;
        reqs[i] = (Request) malloc(sizeof(request));
        
        // Get user request
        getline(cin,input);
        tie(time,command) = split(input, ' ');
        reqs[i]->time = time;
        reqs[i]->id = i;
        reqs[i]->command = command;

        // Spawn user request thread
        pthread_t cur_td;
        pthread_create(&cur_td,NULL,userReqSim,(void *)reqs[i]);
        reqs_td_ids[i] = cur_td;
    }

    for(int i = 0 ; i < num_reqs ; i++)
        pthread_join(reqs_td_ids[i],NULL);
}