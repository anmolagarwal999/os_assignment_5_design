#include <stdio.h>
#include <stdlib.h>
typedef struct course
{
    int id;
    char name[1000];
    float interest;
    int max_student_slot; //per TA
    int num_of_labs;
    int* labs;// must be selected one from given labs
}course;
typedef struct student
{
    int id;
    float ability;
    int pref1;
    int pref2;
    int pref3;
    int reg_time_delay;
    int curr_pref;
}student;
typedef struct lab
{
    char name[1000];
    int num_TA;
    int max_time;
}lab;
int main()
{
    int num_students, num_labs, num_courses;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course c[num_courses];
    for(int i=0;i<num_courses;i++)
    {
        c[i].id=i;
        scanf("%s %f %d %d", c[i].name, c[i].interest, c[i].max_student_slot, c[i].num_of_labs);
        c[i].labs=(int*)malloc(sizeof(int)*c[i].num_of_labs);
        for(int j=0;j< c[i].num_of_labs; j++)
        {
            scanf("%d", c[i].labs[j]);
        }
    }
    student s[num_students];
    for(int i=0;i< num_students;i++)
    {
        s[i].id=i;
        scanf("%f %d %d %d %d", &s[i].ability, &s[i].pref1, &s[i].pref2, &s[i].pref3, &s[i].reg_time_delay);
        s[i].curr_pref=s[i].pref1;
    }
    lab l[num_labs];
    for(int i=0;i<num_labs;i++)
    {
        scanf("%s %d %d", l[i].name, &l[i].num_TA, &l[i].num_TA);
    }


}