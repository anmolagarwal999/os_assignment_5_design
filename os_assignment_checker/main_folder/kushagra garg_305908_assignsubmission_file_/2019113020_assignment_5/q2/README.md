# Extending xv6

[@kushagra](https://www.github.com/kushargs) (2019113020)


  
## Part 1
### Deployment

To compile and run 

```bash
$  gcc -o q2 main.c people.h goal_chance.h 
$ ./q1

```


  
### Code flow
* get input 
* initialize locks (person status, goal_scored), conditional variables and semaphores (3 for each zone)
* people_thread 
   - start of people_thread (people_fun)
   - find seat in the respective zone by creating different threads (zone_n_fun)  
   - if seat not found return in pateince time, return
   - else, start watching game (people_in_game_fun)
   - wait for the goal to get scored and people to get enraged or watch time to end
   - people leave 

* goal_scores
   - calculate time between goal
   - in for loop, sleep btw goal time 

   - calculate probablity of goal
   - if goal scored, brodcast it for people to get enraged
   - repeat till no chance left

### clarification
* people and goal_chance structues defined in people.h, goal_chance.h,respectivly.




    
