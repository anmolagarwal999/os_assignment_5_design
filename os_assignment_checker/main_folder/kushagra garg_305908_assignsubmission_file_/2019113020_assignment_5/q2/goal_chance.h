typedef struct goal_chance {
    int no;
    int team;
    char team_name;
    int time;
    int wait_time;
    double prob;
} goal_chance;