typedef struct people {
    char name[20];
    int id;
    int enter_time;
    char team_fan;
    int patience_val;    
    int goal_limit;
    int group_no;
    int group_cardinality;
    char current_zone;
} people;