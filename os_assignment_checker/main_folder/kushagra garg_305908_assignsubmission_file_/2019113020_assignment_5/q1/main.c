#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>

#include "student.h"
#include "course.h"
#include "lab.h"

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

// variables
int no_students;
int no_courses;
int no_labs;
int student_filled_pref;

course *course_array;
student *student_array;
lab *labs_array;

// threads
pthread_t *course_thread;
pthread_t *student_thread;

// locks and conditions

pthread_mutex_t *lock_course_spot;
pthread_mutex_t *lock_student_status;
pthread_mutex_t **lock_lab_ta;

pthread_cond_t *cond_aval_course_spot;
pthread_cond_t *cond_aval_student_status;
pthread_cond_t **cond_aval_lab_tas;
pthread_cond_t cond_stud_filled_pref;

void change_student_status_course(int id, int status, int course)
{
    pthread_mutex_lock(&lock_student_status[id]);
    student_array[id].status = status;
    student_array[id].course = course;
    pthread_mutex_unlock(&lock_student_status[id]);
}

void *student_fun(void *args)
{

    student curr_student = *(student *)args;
    //wait for atleast one student to fill prefrence
    sleep(curr_student.time_to_fill);
    student_filled_pref = 1;
    printf(ANSI_COLOR_RED "Student %d has filled in preferences for course registration\n" ANSI_COLOR_RESET, curr_student.id);
    pthread_cond_broadcast(&cond_stud_filled_pref);

    int current_pref_course_id;
    int pref_no;

    for (pref_no = 0; pref_no < 3; pref_no++)
    {
        if (pref_no == 1 || pref_no == 2)
        {
            printf(ANSI_COLOR_RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" ANSI_COLOR_RESET, curr_student.id, course_array[curr_student.pref[pref_no - 1]].name, pref_no, course_array[curr_student.pref[pref_no]].name, pref_no + 1);
        }

        current_pref_course_id = curr_student.pref[pref_no];
        //change status
        change_student_status_course(curr_student.id, 0, current_pref_course_id);

        // wait for spots
        pthread_mutex_lock(&lock_course_spot[current_pref_course_id]);
        while (course_array[current_pref_course_id].no_spots == 0)
            pthread_cond_wait(&cond_aval_course_spot[current_pref_course_id], &lock_course_spot[current_pref_course_id]);

        //if no spot
        if (course_array[current_pref_course_id].no_spots == -1)
        {
            pthread_mutex_unlock(&lock_course_spot[current_pref_course_id]);
            continue;
        }
        else if (course_array[current_pref_course_id].no_spots > 0)
        {
            //fill the spot
            course_array[current_pref_course_id].no_spots -= 1;
            pthread_mutex_unlock(&lock_course_spot[current_pref_course_id]);
            printf(ANSI_COLOR_RED "Student %d has been allocated a seat in course %s\n" ANSI_COLOR_RESET, curr_student.id, course_array[current_pref_course_id].name);

            change_student_status_course(curr_student.id, 1, current_pref_course_id);

            pthread_mutex_lock(&lock_student_status[curr_student.id]);
                //wait for tute
                pthread_cond_wait(&cond_aval_student_status[curr_student.id], &lock_student_status[curr_student.id]);
        
            pthread_mutex_unlock(&lock_student_status[curr_student.id]);
    
    
            //attending tute
            // generate prob
            double prob = (double)rand() / (double)((unsigned)RAND_MAX + 1);

            if (course_array[current_pref_course_id].interest * curr_student.calibre >= prob)
            {
                printf(ANSI_COLOR_RED "Student %d has selected course %s permanently\n" ANSI_COLOR_RESET, curr_student.id, course_array[current_pref_course_id].name);
                change_student_status_course(curr_student.id, 2, current_pref_course_id);
                return NULL;
            }
            else
            {
                printf(ANSI_COLOR_RED "Student %d has withdrawn from course %s\n" ANSI_COLOR_RESET, curr_student.id, course_array[current_pref_course_id].name);
                change_student_status_course(curr_student.id, 0, -1);
                continue;
            }
        }
    }

    if (pref_no >= 3)
        printf(ANSI_COLOR_RED "Student %d did not get any of their preferred courses\n" ANSI_COLOR_RESET, curr_student.id);

    return NULL;
}

void *course_fun(void *args)
{
    int counter = 0;
    course current_course = *(course *)args;
    while (1)
    {
        counter++;
        int got_ta = 0;
        int LId;
        int t;
        int min_course_taken = __INT_MAX__;

        // loop lab(l) -> ta(t)

        for (int l = 0; l < current_course.no_aval_labs; l++)
        {
            LId = current_course.aval_labs[l];

            min_course_taken = __INT_MAX__;

            for (int z = 0; z < labs_array[LId].no_ta; z++)
            {
                //searcing for the ta with min_course_taken
                pthread_mutex_lock(&lock_lab_ta[LId][z]);
                    if (labs_array[LId].no_ta_course[z] < min_course_taken)
                        min_course_taken = labs_array[LId].no_ta_course[z];
                pthread_mutex_unlock(&lock_lab_ta[LId][z]);
            }
            // printf("######### course: %d lab: %d, min_course_taken %d counter: %d\n", current_course.id,  LId, min_course_taken, counter);
            for (t = 0; t < labs_array[LId].no_ta; t++)
            {
                pthread_mutex_lock(&lock_lab_ta[LId][t]);
                if (labs_array[LId].no_ta_course[t] <= min_course_taken)
                {
                    //wait if ta can take course but is busy
                    while (labs_array[LId].ta_status[t] == 0 && labs_array[LId].no_ta_course[t] < labs_array[LId].course_max)
                        pthread_cond_wait(&cond_aval_lab_tas[LId][t], &lock_lab_ta[LId][t]);

                    //if ta available
                    if (labs_array[LId].ta_status[t] == 1 && labs_array[LId].no_ta_course[t] < labs_array[LId].course_max)
                    {
                        labs_array[LId].no_ta_course[t]++;
                        labs_array[LId].ta_status[t] = 0;
                        printf(ANSI_COLOR_YELLOW "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" ANSI_COLOR_RESET, t, labs_array[LId].name, current_course.name, labs_array[LId].no_ta_course[t]);
                        got_ta = 1;
                        pthread_mutex_unlock(&lock_lab_ta[LId][t]);
                        break;
                    }
                    else
                    {
                        pthread_mutex_unlock(&lock_lab_ta[LId][t]);
                        continue;
                    }
                }
                else
                {
                    pthread_mutex_unlock(&lock_lab_ta[LId][t]);
                    continue;
                }
            }
            if (got_ta == 1)
                break;
        }

        // no ta left
        if (got_ta == 0)
        {
            printf(ANSI_COLOR_BLUE "Course %s does not have any TA's eligible and is removed from course offerings\n" ANSI_COLOR_RESET, current_course.name);
            pthread_mutex_lock(&lock_course_spot[current_course.id]);
            course_array[current_course.id].no_spots = -1;
            pthread_mutex_unlock(&lock_course_spot[current_course.id]);
            pthread_cond_broadcast(&cond_aval_course_spot[current_course.id]);
            return NULL;
        }
        else
        {
            // open tut_slots slots for the tutorial
            int tut_slots = (rand() % current_course.slots_max) + 1;
            printf(ANSI_COLOR_BLUE "Course %s has been allocated %d seats\n" ANSI_COLOR_RESET, current_course.name, tut_slots);

            pthread_mutex_lock(&lock_course_spot[current_course.id]);
            course_array[current_course.id].no_spots = tut_slots;
            pthread_mutex_unlock(&lock_course_spot[current_course.id]);

            //wait for student to fill pref
            pthread_mutex_lock(&lock_course_spot[current_course.id]);
            while (student_filled_pref == 0)
            {
                pthread_cond_wait(&cond_stud_filled_pref, &lock_course_spot[current_course.id]);
            }
            pthread_mutex_unlock(&lock_course_spot[current_course.id]);

            int spots_filled;
            int W;
            do
            {
                spots_filled = 0;
                W = 0;
                
                pthread_cond_broadcast(&cond_aval_course_spot[current_course.id]);
                //locking all student sattus to fill the tute
                for (int i = 0; i < no_students; i++)
                    pthread_mutex_lock(&lock_student_status[i]);

                for (int i = 0; i < no_students; i++)
                {
                    if (student_array[i].course == current_course.id && student_array[i].status == 0)
                        W += 1;

                    if (student_array[i].course == current_course.id && student_array[i].status == 1)
                        spots_filled += 1;
                }

                for (int i = 0; i < no_students; i++)
                    pthread_mutex_unlock(&lock_student_status[i]);

                if (W == 0)
                    break;

            } while ((tut_slots > spots_filled && W != 0) || spots_filled == 0);

            pthread_mutex_lock(&lock_course_spot[current_course.id]);
            course_array[current_course.id].no_spots = 0;
            pthread_mutex_unlock(&lock_course_spot[current_course.id]);

            printf(ANSI_COLOR_YELLOW "TA %d has started tutorial for Course %s with %d seats filled out of %d\n" ANSI_COLOR_RESET, t, current_course.name, spots_filled, tut_slots);

            //tute happning
            sleep(5);
            printf(ANSI_COLOR_YELLOW "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_COLOR_RESET, t, labs_array[LId].name, current_course.name);

            for (int i = 0; i < no_students; i++)
            {
                pthread_mutex_lock(&lock_student_status[i]);
                if (student_array[i].status == 1 && student_array[i].course == current_course.id)
                    pthread_cond_broadcast(&cond_aval_student_status[i]);
                pthread_mutex_unlock(&lock_student_status[i]);
            }
        }

        pthread_mutex_lock(&lock_lab_ta[LId][t]);
        labs_array[LId].ta_status[t] = 1;
        pthread_cond_broadcast(&cond_aval_lab_tas[LId][t]);
        pthread_mutex_unlock(&lock_lab_ta[LId][t]);
    }
    return NULL;
}

void input()
{

    for (int i = 0; i < no_courses; i++)
    {
        int no_aval_labs = 0;
        double interest;
        int slots_max;
        
        scanf("%s%lf%d%d", course_array[i].name, &interest, &slots_max, &no_aval_labs);

        course_array[i].interest = interest;
        course_array[i].slots_max = slots_max;
        course_array[i].id = i;
        course_array[i].no_aval_labs = no_aval_labs;
        course_array[i].aval_labs = malloc(sizeof(int) * no_aval_labs);

        for (int j = 0; j < no_aval_labs; j++)
        {
            scanf("%d", &course_array[i].aval_labs[j]);
        }

        course_array[i].no_spots = 0;
    }

    for (int i = 0; i < no_students; i++)
    {
        int pref[3];
        double calibre;
        int time_to_fill;
        scanf("%lf%d%d%d%d", &calibre, pref, pref+1, pref+2, &time_to_fill);


        student_array[i].id = i;
        student_array[i].calibre = calibre;
        student_array[i].time_to_fill = time_to_fill;
        student_array[i].pref[0] = pref[0];
        student_array[i].pref[1] = pref[1];
        student_array[i].pref[2] = pref[2];
        student_array[i].status = -1;
        student_array[i].course = -1;
    }

    for (int i = 0; i < no_labs; i++)
    {
        scanf("%s%d%d", labs_array[i].name, &labs_array[i].no_ta, &labs_array[i].course_max);
        labs_array[i].id = i;
        labs_array[i].ta_status = malloc(sizeof(int) * labs_array[i].no_ta);

        labs_array[i].no_ta_course = malloc(sizeof(int) * labs_array[i].no_ta);

        for (int j = 0; j < labs_array[i].no_ta; j++)
        {
            labs_array[i].ta_status[j] = 1;
            labs_array[i].no_ta_course[j] = 0;
        }
    }
}

int main()
{
    scanf("%d %d %d", &no_students, &no_labs, &no_courses);

    course_array = malloc(sizeof(course) * no_courses);
    student_array = malloc(sizeof(student) * no_students);
    labs_array = malloc(sizeof(lab) * no_labs);

    input();

    course_thread = malloc(sizeof(pthread_t) * no_courses);
    student_thread = malloc(sizeof(pthread_t) * no_students);

    student_filled_pref = 0;

    //initialize locks
    lock_course_spot = malloc(sizeof(pthread_mutex_t) * no_courses);
    lock_student_status = malloc(sizeof(pthread_mutex_t) * no_students);
    lock_lab_ta = malloc(sizeof(pthread_mutex_t *) * no_labs);

    //initialize cond
    cond_aval_course_spot = malloc(sizeof(pthread_cond_t) * no_courses);
    cond_aval_student_status = malloc(sizeof(pthread_cond_t) * no_students);
    cond_aval_lab_tas = malloc(sizeof(pthread_cond_t *) * no_labs);

    for (int i = 0; i < no_courses; i++)
    {
        pthread_mutex_init(&lock_course_spot[i], NULL);
        pthread_cond_init(&cond_aval_course_spot[i], NULL);
        course_array[i].no_spots = 0;
    }

    for (int i = 0; i < no_students; i++)
    {
        pthread_mutex_init(&lock_student_status[i], NULL);
        pthread_cond_init(&cond_aval_student_status[i], NULL);
    }

    for (int i = 0; i < no_labs; i++)
    {
        lock_lab_ta[i] = malloc(sizeof(pthread_mutex_t) * labs_array[i].no_ta);

        cond_aval_lab_tas[i] = malloc(sizeof(pthread_cond_t) * labs_array[i].no_ta);

        for (int j = 0; j < labs_array[i].no_ta; j++)
        {
            pthread_cond_init(&cond_aval_lab_tas[i][j], NULL);

            pthread_mutex_init(&lock_lab_ta[i][j], NULL);
        }
    }

    for (int i = 0; i < no_courses; i++)
        pthread_create(&course_thread[i], NULL, course_fun, &course_array[i]);
    for (int i = 0; i < no_students; i++)
        pthread_create(&student_thread[i], NULL, student_fun, &student_array[i]);

    for (int i = 0; i < no_courses; i++)
        pthread_join(course_thread[i], NULL);
    for (int i = 0; i < no_students; i++)
        pthread_join(student_thread[i], NULL);
}