# Assignment 5

[@kushagra](https://www.github.com/kushargs) (2019113020)


  
## Part 1
### Deployment

To compile and run 

```bash
$  gcc -o q1 main.c student.h course.h lab.h -lpthread
$ ./q1

```


### Code flow
- get input 

- initialize locks and conditional variables

course thread


1. start a course thread
2. find ta from the available labs  
3. if ta there but busy wait for it
4. make slots for the course
5. call students for course
6. start the tute
7. end tute and ta leaves the course
8. repeat
9. if no ta's left, end the thread

student thread


1. fill preference after wait time
2. check if course available
3. wait for the slot
4. register for the course
5. wait for the tute
6. attend tute
7. decide to enroll or withdraw
8. if withdrawn repeat
9. else return the thread
10. if pref_no > 3, end the thread

### clarification
* The tute can start with 0 slots filled
* course, student, lab structues defined in course.h, student.h, lab.h respectivly
* waiting for atleast one student to fill the preference before starting the tute.

### bonus

implementation


1. find the minimum no of ta'ship done by the ta
2. find the first ta with minimum ta'ship 
3. make him take tute


    

  