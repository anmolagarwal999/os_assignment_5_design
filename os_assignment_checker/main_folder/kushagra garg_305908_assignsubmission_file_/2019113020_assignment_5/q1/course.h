typedef struct course
{
    int id;
    char name[99];
    int slots_max;
    double interest;
    int no_aval_labs;
    int *aval_labs;
    int no_spots;
} course;
