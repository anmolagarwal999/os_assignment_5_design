typedef struct student
{
    int id;
    int pref[3];
    double calibre;
    int time_to_fill;
    int status;  // 0: waiting for course, 1: waiting for tute, 2: attending course -1: not_filled
    int course; 
} student;
