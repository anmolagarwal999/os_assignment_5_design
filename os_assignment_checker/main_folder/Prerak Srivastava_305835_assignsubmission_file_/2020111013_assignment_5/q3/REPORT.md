# Multithreaded Client and Server

## To run the program
To run server:
```
$ gcc server.cpp -g -o server -pthread
$ ./server <number of worker threads in the thread pool>
```
To run client:
```
$ gcc client.cpp -g -o client -pthread
$ ./client < input.txt
```
## Basic Idea
- Each client sends a request to the server after a particular delay. 
- This request is simple modifictation to a dictionary which consists of key value pairs. 
- After this request is processed by the server, it sends the output back to the client which then outputs it.
## Implementation
1. **Client**:
    The client thread is simulated using the `client_routine` function which does the following
    - The command that is given as input is written to the socket
    - Once it is written, and the server has processed it, it recieves the output of the request using the `read_string_from_socket` function
    - This output is then printed.
2. **Server**: The server is simulated using the `server_routiune` function which does the following
    - The dictionary is maintained the `dict` vector. 
    - The queue of requests is maintained using the `queue_struct` struct.
    - The worker threads that process the requests sent by the client are simulated using the `worker_routine` function.
    - The function first waits for the queue to be non-empty using a conditional variable.
    - Once the queue is non-empty, the string is separated into `command` and `args`, where depending on the `command` the `args` are processed. These are following commands that have been implemented
        - `insert_function` : inserts a key-value pair into the dictionary
        - `concat_function` : concatonates the values of two keys and stores them back in the dictionary
        - `fetch_function` : fetches the value of a key from the dictionary
        - `update_function` : updates the value for a given key in the dictionary
        - `delete_function` : delete a key-value pair from the dictionary
    - Each of the functions takes in the string of arguments and processes them accordingly, depending on the function, and then returns a string output back to the client using the `send_string_on_socket` function.