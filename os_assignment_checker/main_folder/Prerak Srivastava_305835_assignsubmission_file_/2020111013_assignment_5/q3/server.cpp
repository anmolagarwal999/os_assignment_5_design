#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>

using namespace std;

#define PORT_ARG 8001
#define MAX 100
const long long buff_sz = 1048576;

int num_worker_threads;

struct node //for storing all the values in the dictionary
{
    string value;
    int available; //to check if the key is present in the dictionary or not
    pthread_mutex_t lock;
};

typedef struct node node;
vector<node> dict(MAX);

struct queue_struct //sturct for queue
{
    queue<int *> q;
    pthread_mutex_t lock;
    pthread_cond_t cv;
} queue_struct_var;

typedef queue_struct queue_struct;

int send_string_on_socket(int fd, const string &s) 
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }

    return bytes_sent;
}

void insert_function(string args, int client_socket_fd) // inserts a key value pair into the dictionary
{
    int key;
    string value;
    stringstream ss(args);
    string token;
    int j = 0;
    while (getline(ss, token, ' ')) //separating the key and value
    {
        if (j == 0)
        {
            key = stoi(token);
        }
        else
        {
            value = token;
        }
        j++;
    }
    pthread_mutex_lock(&dict[key].lock);
    if (dict[key].available == 0) //checking if the key is already present 
    {
        dict[key].value = value;
        dict[key].available = 1; // key is now present in dict
        printf("Insertion Succesful %s at %d", value.c_str(), key);
        send_string_on_socket(client_socket_fd, "Insertion Succesful");
    }
    else
    {
        printf("Key already present"); //error handling
        send_string_on_socket(client_socket_fd, "Key already present");
    }
    pthread_mutex_unlock(&dict[key].lock);
}

void concat_function(string args, int client_socket_fd) //concatonates 2 values for 2 keys
{
    int first_key, second_key;
    string first_value, second_value;
    stringstream ss(args);
    string token;
    int j = 0;
    while (getline(ss, token, ' '))
    {
        if (j == 0)
        {
            first_key = stoi(token);
        }
        else if (j == 1)
        {
            second_key = stoi(token);
        }
        j++;
    }
    pthread_mutex_lock(&dict[min(first_key,second_key)].lock);
    pthread_mutex_lock(&dict[max(first_key,second_key)].lock);
    if (dict[first_key].available == 0 || dict[second_key].available == 0)
    {
        printf("Either one of the keys is not present");
        send_string_on_socket(client_socket_fd, "Conact Failed as Either one of the keys is not present");
    }
    else
    {   
        // copies of the values are made and then added
        string first_key_copy = dict[first_key].value;
        string second_key_copy = dict[second_key].value;
        dict[first_key].value = first_key_copy + second_key_copy;
        dict[second_key].value = second_key_copy + first_key_copy;
        printf("Concatenation Succesful\n");
        send_string_on_socket(client_socket_fd, dict[second_key].value);
    }
    pthread_mutex_unlock(&dict[max(first_key,second_key)].lock);
    pthread_mutex_unlock(&dict[min(first_key,second_key)].lock);
}

void fetch_function(string args, int client_socket_fd) //gets the value for given key
{
    int key;
    stringstream ss(args);
    string token;
    int j = 0;
    while (getline(ss, token, ' '))
    {
        if (j == 0)
        {
            key = stoi(token);
        }
        j++;
    }
    pthread_mutex_lock(&dict[key].lock);
    if (dict[key].available == 0)
    {
        printf("Key doesn't exist\n");
        send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
    }
    else
    {
        printf("Value of key %d is %s\n", key, dict[key].value.c_str());
        send_string_on_socket(client_socket_fd, dict[key].value);
    }
    pthread_mutex_unlock(&dict[key].lock);
}

void update_function(string args, int client_socket_fd) //updates value for a key
{
    int key;
    string value;
    stringstream ss(args);
    string token;
    int j = 0;
    while (getline(ss, token, ' '))
    {
        if (j == 0)
        {
            key = stoi(token);
        }
        else
        {
            value = token;
        }
        j++;
    }
    pthread_mutex_lock(&dict[key].lock);
    if (dict[key].available == 1)
    {
        dict[key].value = value;
        printf("Update Succesful\n");
        send_string_on_socket(client_socket_fd, dict[key].value);
    }
    else
    {
        printf("Update Failed as Key doesn't exist\n");
        send_string_on_socket(client_socket_fd, "Key Doesn't Exist");
    }
    pthread_mutex_unlock(&dict[key].lock);
}

void delete_function(string args, int client_socket_fd) //deletes a key value pair from dict
{
    int key;
    stringstream ss(args);
    string token;
    int j = 0;
    while (getline(ss, token, ' '))
    {
        if (j == 0)
        {
            key = stoi(token);
        }
        j++;
    }
    pthread_mutex_lock(&dict[key].lock);
    if (dict[key].available == 0)
    {
        printf("Delete Failed as Key doesn't exist\n");
        send_string_on_socket(client_socket_fd, "Key doesn't exist");
    }
    else
    {
        dict[key].available = 0;
        dict[key].value = "";
        printf("Delete Succesful\n");
        send_string_on_socket(client_socket_fd, "Deletion Succesful");
    }
    pthread_mutex_unlock(&dict[key].lock);
}

void *worker_routine(void *arg)
{
    char client_output[buff_sz];
    int size;
    while (1)
    {
        string client_output = "";
        pthread_mutex_lock(&queue_struct_var.lock);
        while (queue_struct_var.q.empty())
        {
            pthread_cond_wait(&queue_struct_var.cv, &queue_struct_var.lock); // waits for a signal for queue to be non empty
        }
        int *client_socket_fd = queue_struct_var.q.front();
        queue_struct_var.q.pop(); // gets top element
        pthread_mutex_unlock(&queue_struct_var.lock);
        client_output.resize(buff_sz);
        size = read(*client_socket_fd, &client_output[0], buff_sz - 1); // reads from client
        client_output[size] = '\0';
        client_output.resize(size);
        string args = "";
        stringstream ss(client_output);
        string command = "";
        string token;
        int j = 0;
        while (getline(ss, token, ' ')) // gets the command 
        {
            if (j == 0)
            {
                command = token;
            }
            else
            {
                args += token;
                args += " ";
            }
            j++;
        } // accordingly function is called to execute command
        if(strcmp(command.c_str(), "insert") == 0)
        {
            insert_function(args, *client_socket_fd);
        }
        else if(strcmp(command.c_str(), "concat") == 0)
        {
            concat_function(args, *client_socket_fd);
        }
        else if(strcmp(command.c_str(), "fetch") == 0)
        {
            fetch_function(args, *client_socket_fd);
        }
        else if(strcmp(command.c_str(), "update") == 0)
        {
            update_function(args, *client_socket_fd);
        }
        else if(strcmp(command.c_str(), "delete") == 0)
        {
            delete_function(args, *client_socket_fd);
        }
        else
        {
            printf("Invalid Command\n");
            send_string_on_socket(*client_socket_fd, "Invalid Command");
        }
    }
}

int main(int argc, char *argv[])
{
    num_worker_threads = stoi(argv[1]);
    pthread_mutex_init(&queue_struct_var.lock, NULL);
    pthread_cond_init(&queue_struct_var.cv, NULL);
    pthread_t worker_threads[num_worker_threads];
    for (int i = 0; i < MAX; i++)
    {
        dict[i].available = 0;
        pthread_mutex_init(&dict[i].lock, NULL);
    }
    for (int i = 0; i < num_worker_threads; i++)
    {
        pthread_create(&worker_threads[i], NULL, worker_routine, NULL);
    }
    socklen_t clilen;
    int wel_socket_fd, port_number;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    /////////////////////////////////////////////////////////////////////////
    /* create socket */
    /*
    The server program must have a special door—more precisely,
    a special socket—that welcomes some initial contact 
    from a client process running on an arbitrary host
    */
    //get welcoming socket
    //get ip,port
    /////////////////////////
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////
    /* IP address can be anything (INADDR_ANY) */
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET; 
    // On the server side I understand that INADDR_ANY will bind the port to all available interfaces,
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(PORT_ARG); //process specifies port
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* bind socket to this port number on this machine */
    /*When a socket is created with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns
       the address specified by addr to the socket referred to by the file
       descriptor wel_sock_fd.  addrlen specifies the size, in bytes, of the
       address structure pointed to by addr.  */

    //CHECK WHY THE CASTING IS REQUIRED
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /* listen for incoming connection requests */
    listen(wel_socket_fd, MAX);
    printf("Server has started listening on the LISTEN PORT\n");
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        /* accept a new request, create a client_socket_fd */
        /*
        During the three-way handshake, the client process knocks on the welcoming door
of the server process. When the server “hears” the knocking, it creates a new door—
more precisely, a new socket that is dedicated to that particular client. 
        */
        //accept is a blocking call
        printf("Waiting for a new client to request for a connection\n");
        int client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        cout << "ok" << endl;
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        int *temp = (int *)malloc(sizeof(int));
        *temp = client_socket_fd;
        pthread_mutex_lock(&queue_struct_var.lock);
        queue_struct_var.q.push(temp);
        pthread_mutex_unlock(&queue_struct_var.lock);
        pthread_cond_signal(&queue_struct_var.cv);
        printf("New client connected from port number %d and IP %s \n", ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
    }
    close(wel_socket_fd);
}