#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <bits/stdc++.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <pthread.h>

#define SERVER_PORT 8001
const long long buff_sz = 1048576;
using namespace std;

struct request_struct
{
    int index;
    int client_fd;
    int wtime;
    string command;
    pthread_mutex_t lock;
};

typedef struct request_struct request_struct;

request_struct* clients;

int get_socket_fd()
{
    struct sockaddr_in server_obj;

    // socket() creates an endpoint for communication and returns a file
    //        descriptor that refers to that endpoint.  The file descriptor
    //        returned by a successful call will be the lowest-numbered file
    //        descriptor not currently open for the process.
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // Converts an IP address in numbers-and-dots notation into either a
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    //https://stackoverflow.com/a/20778887/6427607

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(BGRN "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        // return "
        exit(-1);
    }

    // debug(output);
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    // cout << "We are sending " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    // debug(bytes_sent);
    // debug(s);
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        // return "
        exit(-1);
    }

    return bytes_sent;
}

void *client_routine(void *(arg))
{
    int client_id = *(int *)arg;
    clients[client_id].client_fd = get_socket_fd(); // getting socket
    sleep(clients[client_id].wtime); // sleeping for specified time
    pthread_mutex_lock(&clients[client_id].lock);
    send_string_on_socket(clients[client_id].client_fd, clients[client_id].command); // sending command
    pthread_mutex_unlock(&clients[client_id].lock);

    pthread_mutex_lock(&clients[client_id].lock);
    pair<string, int> buffer = read_string_from_socket(clients[client_id].client_fd, buff_sz); // reading response
    string response = buffer.first; // getting response
    int bytes_received = buffer.second; // getting bytes received
    response.resize(bytes_received); // resizing response
    close(clients[client_id].client_fd); // closing socket
    cout << clients[client_id].index << " : " << gettid() << " : " << response << endl; // printing response
    pthread_mutex_unlock(&clients[client_id].lock);
    return NULL;
}

int main()
{
    int num_clients;
    scanf("%d", &num_clients);
    clients = new request_struct[num_clients];
    pthread_t client_threads[num_clients];
    string temp;
    getline(cin, temp);
    for (int i = 0; i < num_clients; i++) // reading each line tokenizing and storing in struct 
    {
        string input = ""; 
        string wtime = "";
        getline(cin, input);
        // tokenizing input
        stringstream ss(input);
        string token;
        int j = 0;
        while (getline(ss, token, ' '))
        {
            if (j == 0)
            {
                clients[i].wtime = stoi(token);
            }
            else
            {
                // concatonate the rest
                clients[i].command += token;
                clients[i].command += " ";
            }
            j++;
        }
    }
    for (int i = 0; i < num_clients; i++)
    {
        clients[i].index = i;
        // initializing lock
        pthread_mutex_init(&clients[i].lock, NULL);
    }
    for (int i = 0; i < num_clients; i++)
    {
        int *tmp = new int;
        *tmp = i;
        pthread_create(&client_threads[i], NULL, client_routine, (void *)tmp);
    }
    for (int i = 0; i < num_clients; i++)
    {
        pthread_join(client_threads[i], NULL);
    }
    return 0;
}