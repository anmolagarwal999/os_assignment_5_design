#ifndef DEFS
#define DEFS

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>    
#include <unistd.h>
#include "colors.h"

#define MAXCHAR 20



typedef struct courses Courses;
typedef struct labs LAB;
typedef struct ta TA;
typedef struct students Students;

struct labs
{
    int lab_id;
    int no_of_TAs;
    int max_times_TA;
    char name[MAXCHAR];
    bool unavailable;
    TA* list_of_TAs;
};

struct courses
{
    int course_id;
    float interest;
    char name[MAXCHAR];
    int num_labs;
    int max_slots;
    int* lab_list;
    bool unavailable;    
};

struct ta
{
    int TA_id;
    int TA_lab;
    int tot_TAs; 
    int is_avail;
    int num_of_times_TAd;
    pthread_mutex_t lock;
};


struct students
{
    int student_id;
    float calibre;
    int course_pref[3];
    int current_course;
    int preference_time;
    int isFree;
    int course_allocated;
    pthread_mutex_t lock;
    pthread_cond_t cv;
};

int num_of_studs, num_of_labs, num_of_courses;
Courses* courses_list;
Students* students_list;
LAB* labs_list;

#endif