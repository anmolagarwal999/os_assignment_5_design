# Course Allocation Portal

## To compile the program

```
$ gcc main.c -g -pthread -o main 
$ ./main < input.txt
```

## The Basic Idea
- There simulation consists of multiple students who wish to be allocated into their preferred courses, one by one. 
- The course allocation portal's job is to search for the students, allocate them, and then depending on whether the student decides to choose the course permanently or not, the course portal has to allocate the student again, until the students preferences are exhausted, after which the student exists the simulation.
- Each course has a set of labs and each lab has a set of TAs, who conduct tutorials. A course is withdrawn if it no longer has TAs available in the alloted labs who can take tutorials

## Implementation
There are two main functions that perform the simulation namely: 

1. `student_routine` : Simulates the student. Each student has a calibre, time at which he/she fills the preference and the preference order of the courses in decreasing order. The simulation is as follows
    1. First the thread will sleep for the time that has the student takes to fill his/her preferences.
    2. Then for each of the students preferences, starting with the most preferred, the student first waits for the course to start allocating students if a tut is already going on.
    3. After this it randomly chosen if the student decides to permanently choose the course or withdraw from it and move on to their next preference. 
    4. If the student has withdrawn from all courses then the student exits the simulation. This is done using the `isStudentExit` function.

2. `course_routine` : Simulates the course allocation process. Each course has an interest level and list of labs which are used. The simulation is as follows:
    1. First it is checks whether the course has been withdrawn or not. This done by `withdraw_course` function.
    2. After this the function looks through the list of TAs in each of its labs to find a TA who is available to take a tut.
    3. After checking if the TA is available, he/she is allocated to the course, and a random number of seats are chosen for the tut.
    4. Then the ta takes the tutorial, which is represented by the function sleeping for 3 seconds. The tut simulation is done by the `take_tut` function, which allocates the seats, conducts the tut, and then releases the students after the tut.
    5. If the lab has no TA that can take tut, then the lab is withdrawn and appropriate message is displayed.