#include "defs.h"

bool isStudentExit(int student_id) // function that executes when the student has to exit simulation without getting any course
{
    Students *student = &students_list[student_id];
    if(student->current_course == 2)
    {
        student->isFree = 0;
        printf(BLU "Student %d" RED" could not get any of his preferred courses and has exited the simulation.\n", student->student_id);
        return 1;
    }
    return 0;
}

void *student_routine(void *arg)
{
    int student_id = *(int *)arg;
    Students *student = &students_list[student_id];
    sleep(student->preference_time); // sleeping until the student fills in preferences
    pthread_mutex_lock(&student->lock);
    printf(BLU "Student %d" GRN" has filled in preferences for course registration form.\n", student->student_id);
    student->isFree = 1; 
    pthread_mutex_unlock(&student->lock); 
    for(; student->current_course < 3; student->current_course++) 
    {
        pthread_mutex_lock(&student->lock);
        if (courses_list[student->course_pref[student->current_course]].unavailable == 0){pthread_cond_wait(&student->cv, &student->lock);} // waits for the course to be available
        int course_index = student->course_pref[student->current_course]; // gets the index of the course that the student wants to register
        if (student->course_allocated != student->course_pref[student->current_course] && courses_list[course_index].unavailable == 1) 
        {
            if(isStudentExit(student_id)){return NULL;}
            printf(BLU "Student %d" YEL" has changed his current preference from %s to %s\n", student->student_id, courses_list[student->course_pref[student->current_course]].name, courses_list[student->course_pref[student->current_course + 1]].name);
            student->isFree = 1;
            pthread_mutex_unlock(&student->lock);
        }
        else
        {
            if ((1 + (rand()%(100))) < 100 * (student->calibre * courses_list[course_index].interest)) // randomly choosing if student accepts the course or withdraws
            {
                printf(BLU "Student %d" RED" has selected the course %s permanently and has exited the simulation\n", student->student_id, courses_list[course_index].name);
                break;
            }
            else
            {
                printf(BLU "Student %d has withdrawn from course %s\n", student->student_id, courses_list[course_index].name);
                if(isStudentExit(student_id)){return NULL;}
                printf(BLU "Student %d has changed his current preference from %s to %s\n", student->student_id, courses_list[student->course_pref[student->current_course]].name, courses_list[student->course_pref[student->current_course + 1]].name);
                student->isFree = 1;
                student->course_allocated = -1;
            }
            pthread_mutex_unlock(&student->lock);
        }
    }
    student->isFree = 0; // student is no longer free
}

bool check_ta_avail(int course_id) //checks if any tas from the course are available
{
    Courses *course = &courses_list[course_id];
    for (int i = 0; i < course->num_labs; i++)
    {
        for (int j = 0; j < labs_list[course->lab_list[i]].no_of_TAs; j++)
        {
            if (labs_list[course->lab_list[i]].list_of_TAs[j].num_of_times_TAd < labs_list[course->lab_list[i]].max_times_TA) // checks if TA has TAd lesser times than allowed
            {
                return true;
            }
        }
    }
    return false;
}

bool withdraw_course(int course_id) // executed when the course has been withdrawn
{
    Courses *course = &courses_list[course_id];
    if (!check_ta_avail(course_id))
    {
        printf(MAG "Course %s" RED" does not have any TA mentors eligible and is removed from course offerings\n", course->name);
        course->unavailable = 1;
        for (int i = 0; i < num_of_studs; i++)
        {
            if (students_list[i].course_pref[students_list[i].current_course] == course->course_id)
            {
                pthread_cond_signal(&students_list[i].cv);
            }
        }
        return 1;
    }
    return 0;
}

bool is_lab_avail(int labno) //checks if lab has any TAs available
{
    for (int j = 0; j < labs_list[labno].no_of_TAs; j++) //going through list of TAs in the lab
    {
        if (labs_list[labno].list_of_TAs[j].num_of_times_TAd < labs_list[labno].max_times_TA)
        {
            return true;
        }
    }
    return false;
}

void take_tut(int course_id, int D, int labno, int j) //function which simulates tutorial
{
    Courses *course = &courses_list[course_id];
    int num_students_in_tut = 0;
    int tut_student_list[num_of_studs];
    while (!num_students_in_tut) // Looks for students who have not been allocated yet
    {
        for (int i = 0; i < num_of_studs; i++)
        {
            if (students_list[i].isFree == 1 && students_list[i].course_allocated == -1 && num_students_in_tut < D && students_list[i].course_pref[students_list[i].current_course] == course->course_id) // checking if student interested if free and can join or not
            {
                pthread_mutex_lock(&students_list[i].lock); 
                printf(BLU "Student %d" GRN" has been allocated a seat in course %s\n", i, course->name);
                students_list[i].course_allocated = course->course_id;
                tut_student_list[num_students_in_tut++] = i;
            }
        }
        sleep(1);
    }
    // taking the tut
    printf(GRN "Tutorial has started for course %s with %d seats filled out of %d\n", course->name, num_students_in_tut, D);
    sleep(3);
    printf(CYN"TA %d from" YEL" lab %s" GRN" has completed the tutorial for course %s\n", j, labs_list[labno].name, course->name);
    // releasing the students after the tutorial
    for (int i = 0; i < num_students_in_tut; i++)
    {
        students_list[tut_student_list[i]].isFree = 0;
        pthread_mutex_unlock(&students_list[tut_student_list[i]].lock);
        pthread_cond_signal(&students_list[tut_student_list[i]].cv);
    }
}

void *course_routine(void *arg) // simulates allocation of courses for students
{
    sleep(1);
    while (1)
    {
        int course_id = *(int *)arg;
        Courses *course = &courses_list[course_id];

        if(withdraw_course(course_id)){return NULL;} // checks if course has been withdrawn and removes it from the list of courses
        for (int i = 0; i < course->num_labs; i++)
        {
            int labno = course->lab_list[i];
            for (int j = 0; j < labs_list[labno].no_of_TAs; j++) // finding ta for the given course
            {
                if (labs_list[labno].list_of_TAs[j].is_avail == 0 && labs_list[labno].list_of_TAs[j].num_of_times_TAd < labs_list[labno].max_times_TA)
                {
                    pthread_mutex_lock(&labs_list[labno].list_of_TAs[j].lock);
                    labs_list[labno].list_of_TAs[j].is_avail = 1;
                    labs_list[labno].list_of_TAs[j].num_of_times_TAd++;
                    printf(CYN "TA %d from" YEL" lab %s" GRN" has been allocated to course %s for his %dth TA ship.\n", j, labs_list[labno].name, course->name, labs_list[labno].list_of_TAs[j].num_of_times_TAd);
                    int D = 1 + (rand()%(course->max_slots));
                    printf(MAG "Course %s" GRN" has been allocated %d seats\n", course->name, D); // randomly allocating seats for the tut
                    take_tut(course_id, D, labno, j);
                    labs_list[labno].list_of_TAs[j].is_avail = 0;

                    if (!is_lab_avail(labno) && labs_list[labno].unavailable == 0) // checks if lab is unavailable 
                    {
                        printf(YEL"Lab %s" RED" no longer has students available for TA ship.\n", labs_list[labno].name);
                        labs_list[labno].unavailable = 1;
                    }
                    pthread_mutex_unlock(&labs_list[labno].list_of_TAs[j].lock);
                }
            }
        }
    }
}

void freemem() // frees all memory that was allocated 
{
    for(int i = 0; i < num_of_courses; i++)
    {
        free(courses_list[i].lab_list);
    }
    for(int i = 0; i < num_of_labs; i++)
    {
        free(labs_list[i].list_of_TAs);
    }
    free(courses_list);
    free(labs_list);
    free(students_list);
}

int main()
{
    scanf("%d %d %d", &num_of_studs, &num_of_labs, &num_of_courses);
    courses_list = (Courses*)malloc(num_of_courses * sizeof(Courses));
    labs_list = (LAB*)malloc(num_of_labs * sizeof(LAB));
    students_list = (Students*)malloc(num_of_studs * sizeof(Students));

    pthread_t coursethreads[num_of_courses];
    pthread_t studentsthreads[num_of_studs];
    pthread_t labsthreads[num_of_labs];

    for (int i = 0; i < num_of_courses; i++)
    {
        courses_list[i].course_id = i;
        courses_list[i].unavailable = 0;
        scanf("%s %f %d %d", courses_list[i].name, &courses_list[i].interest, &courses_list[i].max_slots, &courses_list[i].num_labs);
        courses_list[i].lab_list = (int *)malloc(sizeof(int) * courses_list[i].num_labs);
        for (int j = 0; j < courses_list[i].num_labs; j++)
        {
            scanf("%d", &courses_list[i].lab_list[j]);
        }
    }

    for (int i = 0; i < num_of_studs; i++)
    {
        students_list[i].student_id = i;
        students_list[i].isFree = 0;
        students_list[i].course_allocated = -1;
        students_list[i].current_course = 0;
        scanf("%f %d %d %d %d", &students_list[i].calibre, &students_list[i].course_pref[0], &students_list[i].course_pref[1], &students_list[i].course_pref[2], &students_list[i].preference_time);

        // initializing cv
        pthread_cond_init(&students_list[i].cv, NULL);
    }

    for (int i = 0; i < num_of_labs; i++)
    {
        labs_list[i].lab_id = i;
        labs_list[i].unavailable = 0;
        scanf("%s %d %d", labs_list[i].name, &labs_list[i].no_of_TAs, &labs_list[i].max_times_TA);
        labs_list[i].list_of_TAs = (TA *)malloc(sizeof(TA) * labs_list[i].no_of_TAs);
        for (int j = 0; j < labs_list[i].no_of_TAs; j++)
        {
            labs_list[i].list_of_TAs[j].TA_id = j;
            labs_list[i].list_of_TAs[j].TA_lab = i;
            labs_list[i].list_of_TAs[j].is_avail = 0;
            labs_list[i].list_of_TAs[j].num_of_times_TAd = 0;
            pthread_mutex_init(&labs_list[i].list_of_TAs->lock, NULL);
        }
    }

    for (int i = 0; i < num_of_studs; i++)
    {
        int* arg = (int *)malloc(sizeof(int));
        *arg = i;
        pthread_create(&studentsthreads[i], NULL, student_routine, arg);
        // initializing the lock
        pthread_mutex_init(&students_list[i].lock, NULL);
    }

    for (int i = 0; i < num_of_courses; i++)
    {
        int* arg = (int *)malloc(sizeof(int));
        *arg = i;
        pthread_create(&coursethreads[i], NULL, course_routine, arg);
    }

    for (int i = 0; i < num_of_studs; i++)
    {
        pthread_join(studentsthreads[i], NULL);
    }

    for (int i = 0; i < num_of_studs; i++)
    {
        pthread_cond_destroy(&students_list[i].cv);
        pthread_mutex_destroy(&students_list[i].lock);
    }
    freemem();
}