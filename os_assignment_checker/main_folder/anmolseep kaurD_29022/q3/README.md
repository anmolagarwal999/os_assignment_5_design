FILES :
-`server.cpp`
-`client.cpp`

`server.cpp`: 
    - We use an array of structs to maintain the dictionary. 
    - We create as many threads as there are workers and also maintain queue to store the commands.
    - In the `main` function we create the socket and invoke listen.
    - Now we have an infinite for loop, if we get a request , we lock the queue , push the commond into it and again unlock. We call `push_into_queue` function for this.
    - In the `server_worker_threads` function,we have an infite for loop , and check  if queue has any entry , if it has we select it and then pop it else we wait for signal from the init server function.
    - In server `execute_commands` function, we read the data requests sent by client and handle the requests.
    - Break the command into individual words and handle all the commands
    - Call send_string_on_socket to send back the output to client.

`client.cpp`:
    - Read all of the inputs and divided it into as many threads as there were clients.
    - Construct the socket and then sleep for the amount of time specified in input.
    - After that, we lock this client, preventing anyone else from sending commands to the server.
    - We unlock and then lock it after delivering the instruction to read the output from the server.
    - Now print any output we received from the server and close the thread.
    - After all of the clients have been dealt with, the programme will depart.

Compilation Instructions :
Run `g++ -std=c++17 client.cpp -lpthread`  for client file.
and `g++ -std=c++17 server.cpp -lpthread` for server file.
 