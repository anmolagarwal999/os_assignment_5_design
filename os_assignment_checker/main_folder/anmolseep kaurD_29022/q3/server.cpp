#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <vector>
#include <queue>
using namespace std;
/////////////////////////////

// Regular bold output.c_str()
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
//#define debug(x) printf(#x << " : " << \n")
#define part printf("-----------------------------------\n");

///////////////////////////////
#define MAX_CLIENTS 100
#define PORT_ARG 8001
#define MAX_DICT_SIZE 100

const int initial_msg_len = 256;
int no_of_threads;
////////////////////////////////////

const LL buff_sz = 1048576;

///////////////////////////////////////////////////
queue<int *> q;
pthread_mutex_t queue_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;

bool check_w = true;

struct dictionary_node
{
    string str;
    int deleted;
    pthread_mutex_t mutex;
};

struct dictionary_node dictionary_arr[101];

int send_string_on_socket(int fd, const string &s)
{
    string thr = to_string(gettid()) + " : ";
    thr += s;
    // debug(s.length());
    int bytes_sent = write(fd, thr.c_str(), thr.length());
    if (bytes_sent < 0)
    {
        perror("Failed to SEND DATA via socket.\n");
    }
    return bytes_sent;
}

void execute_commands(int fd)
{
    std::string output;
    output.resize(buff_sz);
    int bytes_received = read(fd, &output[0], buff_sz - 1);
    // debug(bytes_received);
    if (bytes_received <= 0)
    {
        perror("Failed to read data from socket. \n");
        return;
    }
    output[bytes_received] = '\0';
    output.resize(bytes_received);

    istringstream ss(output);
    string token;
    vector<string> argu;
    while (ss >> token)
    {
        argu.push_back(token);
    }

    if (argu.size() <= 0)
    {
        return;
    }

    char msg[256] = "";
    int deter = -1;
    if (argu[0] == "insert")
    {
        deter = 1;
    }
    else if (argu[0] == "delete")
    {
        deter = 2;
    }
    else if (argu[0] == "update")
    {
        deter = 3;
    }
    else if (argu[0] == "concat")
    {
        deter = 4;
    }
    else if (argu[0] == "fetch")
    {
        deter = 5;
    }
    int num_arg = argu.size();
    switch (deter)
    {
    case 1:
    {
        if (num_arg != 3)
        {
            strcpy(msg, "ERROR :Insert: Incorrect number of arguments");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
            return;
        }

        stringstream num_key(argu[1]);
        int key = 0;
        num_key >> key;
        pthread_mutex_lock(&dictionary_arr[key].mutex);
        if (dictionary_arr[key].deleted == 0)
        {
            strcpy(msg, "Key already exists");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }

        else if (dictionary_arr[key].deleted == 1)
        {
            dictionary_arr[key].deleted = 0;
            dictionary_arr[key].str = argu[2];
            strcpy(msg, "Insertion successful");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }
        pthread_mutex_unlock(&dictionary_arr[key].mutex);
        break;
    }

    case 2:
    {
        if (num_arg != 2)
        {
            strcpy(msg, "ERROR :Delete: Incorrect number of arguments");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
            return;
        }

        stringstream num_key(argu[1]);
        int key = 0;
        num_key >> key;
        pthread_mutex_lock(&dictionary_arr[key].mutex);
        if (dictionary_arr[key].deleted == 1)
        {
            strcpy(msg, "No such key exists");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }
        else if (dictionary_arr[key].deleted == 0)
        {
            dictionary_arr[key].deleted = 1;
            strcpy(msg, "Deletion successful");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }
        pthread_mutex_unlock(&dictionary_arr[key].mutex);
        break;
    }

    case 3:
    {
        if (num_arg != 3)
        {
            strcpy(msg, "ERROR :update: Incorrect number of arguments");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
            return;
        }
        stringstream num_key(argu[1]);
        int key = 0;
        num_key >> key;
        pthread_mutex_lock(&dictionary_arr[key].mutex);
        if (dictionary_arr[key].deleted == 1)
        {
            strcpy(msg, "Key does not exist");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }
        else if (dictionary_arr[key].deleted == 0)
        {
            dictionary_arr[key].str = argu[2];
            printf("Update Successful\n");
            send_string_on_socket(fd, dictionary_arr[key].str);
        }
        pthread_mutex_unlock(&dictionary_arr[key].mutex);
        break;
    }

    case 4:
    {
        if (num_arg != 3)
        {
            strcpy(msg, "ERROR : concat: Incorrect number of arguments");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
            return;
        }

        stringstream num_key1(argu[1]);
        int key1 = 0;
        num_key1 >> key1;
        stringstream num_key2(argu[2]);
        int key2 = 0;
        num_key2 >> key2;
        pthread_mutex_lock(&dictionary_arr[key1].mutex);
        pthread_mutex_lock(&dictionary_arr[key2].mutex);

        if (dictionary_arr[key1].deleted == 1 || dictionary_arr[key2].deleted == 1)
        {
            strcpy(msg, "Concat failed as at least one of the keys does not exist");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
        }
        else
        {
            string temp = dictionary_arr[key1].str;
            dictionary_arr[key1].str = dictionary_arr[key1].str + dictionary_arr[key2].str;
            dictionary_arr[key2].str += temp;
            send_string_on_socket(fd, dictionary_arr[key2].str);
            printf("Concat Successful\n");
        }
        pthread_mutex_unlock(&dictionary_arr[key1].mutex);
        pthread_mutex_unlock(&dictionary_arr[key2].mutex);
    }
    break;

    case 5:
    {
        if (num_arg != 2)
        {
            strcpy(msg, "ERROR : fetch : Incorrect number of arguments");
            printf("%s\n", msg);
            send_string_on_socket(fd, msg);
            return;
        }
        stringstream num_key(argu[1]);
        int key = 0;
        num_key >> key;
        pthread_mutex_lock(&dictionary_arr[key].mutex);
        if (dictionary_arr[key].deleted == 1)
        {
            printf("Key does not exist\n");
            send_string_on_socket(fd, "Key does not exist");
        }
        else
        {
            printf("Fetch successful\n");
            send_string_on_socket(fd, dictionary_arr[key].str);
        }
        pthread_mutex_unlock(&dictionary_arr[key].mutex);
    }
    break;

    default:
    {
        printf("Incorrect operation\n");
        send_string_on_socket(fd, "Incorrect operation");
    }
    break;
    }
}
void *server_worker_threads(void *(arg))
{
    while (true)
    {
        pthread_mutex_lock(&queue_lock);

        while (q.empty())
        {
            pthread_cond_wait(&cond_var, &queue_lock);
        }
        
        int *client_sockfd = q.front();
        q.pop();
        pthread_mutex_unlock(&queue_lock);
        execute_commands(*client_sockfd);
    }
}

void push_into_queue(int *index)
{
    pthread_mutex_lock(&queue_lock);
    q.push(index);
    pthread_mutex_unlock(&queue_lock);
    return;
}

int main(int argc, char *argv[])
{
    no_of_threads = stoi(argv[1]);
    pthread_t thread_arr[no_of_threads];

    for (int i = 0; i < 101; i++)
    {
        dictionary_arr[i].deleted = 1;
        pthread_mutex_init(&dictionary_arr[i].mutex, NULL);
    }

    for (int i = 0; i < no_of_threads; i++)
    {
        pthread_create(&thread_arr[i], NULL, server_worker_threads, NULL);
    }

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;

    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); // process specifies port

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(wel_socket_fd, MAX_CLIENTS);
    printf("Server has started listening on the LISTEN PORT\n");
    clilen = sizeof(client_addr_obj);
    while (true)
    {
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        int *index = new int;
        *index = client_socket_fd;

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
        push_into_queue(index);
        pthread_cond_signal(&cond_var);
    }

    close(wel_socket_fd);
    return 0;
}
