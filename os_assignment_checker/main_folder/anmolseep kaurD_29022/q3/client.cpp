#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>


#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;


//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl

///////////////////////////////
#define SERVER_PORT 8001
////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////

int no_of_req;
struct client_struct
{
    pthread_t TID;
    int client_fd;
    int time_delay;
    string cmd;
    pthread_mutex_t mutex;
};

struct client_struct *ptr_to_client;
pthread_mutex_t terminal = PTHREAD_MUTEX_INITIALIZER;

int send_command(int index)
{
    int bytes_sent = write(ptr_to_client[index].client_fd, ptr_to_client[index].cmd.c_str(), ptr_to_client[index].cmd.length());
    if (bytes_sent < 0)
    {
        perror("Failed To communicate with the server");

        return -1;
    }
    return 1;
}

int receive_output(int index)
{
    string buffer;
    buffer.resize(buff_sz);
    int byte_read = read(ptr_to_client[index].client_fd, &buffer[0], buff_sz - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);

    if (byte_read <= 0)
    {
        perror("Failed To communicate with the server");
        return -1;
    }

    pthread_mutex_lock(&terminal);
    cout << index << " : " << buffer << endl; 
    pthread_mutex_unlock(&terminal);
    return 1;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    int port_num = SERVER_PORT;
    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order


    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);

    }
    return socket_fd;
}
////////////////////////////////////////////////////////

void *begin_process(void *(arg))
{
    int index = *(int *)arg;
    struct sockaddr_in server_obj;
    ptr_to_client[index].client_fd = get_socket_fd(&server_obj);

    sleep(ptr_to_client[index].time_delay);

    pthread_mutex_lock(&ptr_to_client[index].mutex);
    if (send_command(index) < 0)
    {
        return NULL;
    }
    pthread_mutex_unlock(&ptr_to_client[index].mutex);

    pthread_mutex_lock(&ptr_to_client[index].mutex);
    if (receive_output(index) < 0)
    {
        return NULL;
    }
    pthread_mutex_unlock(&ptr_to_client[index].mutex);
    return NULL;
}

int main(int argc, char *argv[])
{

    scanf("%d", &no_of_req);

    ptr_to_client = (struct client_struct *)malloc(no_of_req * sizeof(struct client_struct));

    for (int i = 0; i < no_of_req; i++)
    {
        int time_delay;
        char strg[100];
        scanf("%d", &time_delay);
        scanf("%[^\n]%*c", strg);
        ptr_to_client[i].time_delay = time_delay;
        ptr_to_client[i].cmd = strg;
        ptr_to_client[i].mutex = PTHREAD_MUTEX_INITIALIZER;
    }

    for (int i = 0; i < no_of_req; i++)
    {
        int *index = new int;
        *index = i;
        pthread_create(&ptr_to_client[i].TID, NULL, begin_process, (void *)index);
    }

    for (int i = 0; i < no_of_req; i++)
    {
        pthread_join(ptr_to_client[i].TID, NULL);
    }

    free(ptr_to_client);

    return 0;
}
