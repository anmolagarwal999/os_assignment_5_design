
Files:
-q1.c

# Compilation instructions:
`gcc q1.c -lpthread`

# Assumptions:
1. Tutorials can be started with 0 students.


The question is implemented through structs for each entity:
-Labs
-Students
-TA
-courses

- In the main function , first of all input is taken and initializtion of variables is done wherever required. Input_Handler is called for this purpose.

- Then CoursesThread and StudentThread function is called.

- CourseThread : 
    - We complete the loop by performing the following:
    - Compare the current tutored of the TA's of the Labs allocated to this course to the maximum times of the TA's of the Labs assigned to this course until we obtain a TA or the TAs are done, and handle the case if the course is to be discontinued.
    - Handle the cases of students who are enrolled in that course and send them a signal to increment their priority.
    - Iterate through all TAs of alloted Labs, and for each TA, we check if he is available and his number of times is less than the maximum, and then we lock that TA for this course. 
    - Iterate through all students; If they are free(course == -1) and whether they have the course as the one we are iterating through and then in that lock them and allot them a seat in the tutorial. 
    - Conduct the tutorial and then unlock TA and student(and send signal) . 
    - Repeat.

- StudentThread :
    - First, wait for students to come in.
     - We complete the loop by doing the following:
     - Lock the student and see if the course is still available; if not, up the priority.
     - If the priority exceeds 3, handle the case and exit the loop.
     - Wait for a signal from the course thread if the course is available.
     - Now one of two things can happen: the student will either attend the tutorial or the course will be withdrawn.
     - We handle both cases using the if condition of whether the student's course is equal to what it should be.
     -If a student attends a tutorial, we calculate probability and use it to determine whether the student wishes to study the    course permanently or not. If the probability is lower, we raise the priority and unlock the student.
     - Similarly, if a course is removed, we increase the priority.
    - Everywhere we increase priority handle the case if priority becomes more than 3 and exit the loop.


- Wait for the StudentThread to finish execution.