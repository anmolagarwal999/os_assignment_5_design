#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include<semaphore.h>
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;
#define MAX_CLIENTS 4
#define PORT_ARG 8001
#define MAX_LEN 20
#define MINI 5
typedef struct args args ;
const int initial_msg_len = 256;
#define NE 0
const LL buff_sz = 1048576;
int condition ;
struct args {
    int i ;
    string command ;
} ;
map<int,string> dict ;
#define YS 1
sem_t key_lock[101] ;
int noof_steps ;
sem_t read_lock ;
int loop_count;
pthread_t threads[1000] ;
int total_count ;
pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    loop_count = loop_count + YS;
    total_count = loop_count + NE;
    int bytes_received = read(fd, &output[0], bytes - 1);
    loop_count = (YS-1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
        noof_steps = noof_steps + loop_count;
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    loop_count++;
    condition++;
    return {output, bytes_received};
}
//
int send_string_on_socket(int fd, const string &s)
{
    condition=0;
    int bytes_sent = write(fd, s.c_str(), s.length());
    int array[MAX_LEN];
    if (bytes_sent < 0)
    {
        condition++;
        cerr << "Failed to SEND DATA via socket.\n";
        noof_steps = (YS-1);
    }
    for(int i=1;i<6;i++)
    {
        if(i==4)
            array[i]=1;
        else 
            array[i]=0;
    }
    return bytes_sent;
}
//
void* handle_connection(void* ptrclient_socket_fd)
{   
    noof_steps = 0;
    int received_num, sent_num;
    total_count =0;
    int client_socket_fd=*((int*)ptrclient_socket_fd) ;
    int ret_val = 1;
    total_count = loop_count + NE;
    string cmd;
    condition = (NE+1)*(YS-1);
    sem_wait(&read_lock) ;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    loop_count = (YS-1);
    string msg_to_send_back = "done" ;
    condition++;
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    int clientx = 0;
    if (sent_to_client == -1)
    {
        clientx ++;
        perror("Error while writing to client. Seems socket has been closed");
        condition++;
    }    
    for(int i=0;i<(MAX_LEN);i++)
    {
        loop_count++;
        condition++;
    }
    sem_post(&read_lock) ;
    clientx ++;
    ret_val = received_num;
    clientx = clientx + NE;
    if (ret_val <= 0)
    {
        clientx ++;
        printf("Server could not read msg sent from client\n");
        total_count = loop_count + NE;
        condition++;
        return NULL ;
    }
    total_count =0;
    string temp ;
    noof_steps = (YS-1);
    vector<string> parts ;
    clientx = 0;
    int n=cmd.size() ;
    int array[MAX_LEN];
    for(int i=(NE);i<n;i++)
    {
        if(cmd[i]!=' ')
        {
            temp.pb(cmd[i]) ;
            condition++;
        }
        else
        {
            parts.pb(temp) ;
            condition++;
            temp.clear() ;
        }
        loop_count++;
        loop_count = loop_count + YS;
        if(i==4)
            array[i]=1;
        else 
            array[i]=0;
    }
    parts.pb(temp) ;
    int num_commands=parts.size() ;
    int clientno = 0;
    int key=stoi(parts[1]) ;
    clientno = key +1;
    int index=stoi(parts[num_commands-1]) ;
    clientx ++;
    cout<<index<<":"<<pthread_self()<<":" ;
    total_count = (NE);
    noof_steps = (YS-1);
    if(parts[0]=="insert")
    {
        sem_wait(&key_lock[key]) ;
        condition = (NE+1)*(YS-1);
        string value=parts[2] ;
        condition++;
        if(dict.find(key)!=dict.end())
        {
            clientx ++;
            printf("Key aldready exists\n") ;
            clientx = clientx + NE;
        }
        else
        {
            condition++;
            dict.insert({key,value}) ;
            loop_count++;
            printf("Insertion successful\n") ;
            total_count = loop_count + NE;
        }
        sem_post(&key_lock[key]) ;
        clientx ++;
        clientx = clientx + NE;
    }
    else if(parts[0]=="delete")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        total_count = loop_count + NE;
        if(dict.find(key)!=dict.end())
        {
            clientno = clientno +1;
            dict.erase(key) ;
            clientx ++;
            printf("Deletion successful\n") ;
            noof_steps = (YS-1);
        }
        else
        {
            loop_count = (YS-1);
            printf("No such key exists\n") ;
            condition++;
        }
        noof_steps = (YS-1);
        sem_post(&key_lock[key]) ;
        clientx = clientx + NE;
    }
    else if(parts[0]=="update")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        loop_count = loop_count + YS;
        string value=parts[2] ;        
        total_count = loop_count + NE;
        if(dict.find(key)!=dict.end())
        {
            clientx ++;
            dict[key]=value ;
            condition++;
            cout<<dict[key]<<"\n" ;
            clientx = clientx + NE;
        }
        else
        {
            noof_steps = 0;
            cout<<"Key does not exist\n" ;
            condition++;
        }
        clientno = 0;
        sem_post(&key_lock[key]) ;
        clientno = clientno +1;
    }
    else if(parts[0]=="concat")
    {
        condition = (NE+1)*(YS-1);
        int key1=stoi(parts[1]) ;
        clientx = 0;
        int key2=stoi(parts[2]) ;
        total_count =0;
        sem_wait(&key_lock[key1]) ;
        total_count = (NE);
        noof_steps = (YS-1);
        sem_wait(&key_lock[key2]) ;
        clientno = clientno +1;
        if(dict.find(key1)==dict.end()||dict.find(key2)==dict.end())
        {
            condition++;
            if(loop_count==4)
                clientx=1;
            else 
                clientx=0;
            cout<<"Concat failed as at least one of the keys does not exist\n" ;
            clientx ++;
        }
        else
        {
            noof_steps = noof_steps + loop_count;
            string value1=dict[key1] ;
            condition++;
            string value2=dict[key2] ;
            clientx = clientx + NE;
            dict[key1]=value1+value2 ;
            condition++;
            dict[key2]=value2+value1 ;
            clientno = 0;
            cout<<dict[key2]<<"\n" ;
            clientno = clientno +1;
        }
        for(int i=0;i<(MAX_LEN);i++)
        {
            loop_count++;
            condition++;
        }
        sem_post(&key_lock[key1]) ;
        condition++;
        sem_post(&key_lock[key2]) ;
        loop_count++;
    }
    else if(parts[0]=="fetch")
    {
        condition = (NE+1)*(YS-1);
        sem_wait(&key_lock[key]) ;
        clientx = 0;
        if(dict.find(key)!=dict.end())
        {
            total_count =0;
            cout<<dict[key]<<"\n" ;
            noof_steps = noof_steps + loop_count;
        }
        else
        {
            condition++;
            cout<<"Key does not exist\n" ;
            total_count = (NE);
        }
        total_count =0; 
        sem_post(&key_lock[key]) ;
        clientx ++;
    }
    total_count = loop_count + NE;

}
int main(int argc, char *argv[])
{
    int clientx = 0;
    sem_init(&read_lock, 0, 1);
    int clientno = 0;
    int array[MAX_LEN];
    for(int i=0;i<=100;i++)
    {
        sem_init(&key_lock[i],0,1) ;
        loop_count++;
    }    
    int i, j, k, t, n;
    clientx ++;
    clientx = clientx + NE;
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;
    clientno = clientno +1;
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    total_count = loop_count + NE;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    for(int i=0;i<(MAX_LEN);i++)
    {
        loop_count = loop_count + YS;
        total_count = loop_count + NE;
        condition++;
    }
    if (wel_socket_fd < 0)
    {
        condition =0;
        perror("ERROR creating welcoming socket");
        condition++;
        exit(-1);
    }
    noof_steps = noof_steps + loop_count;
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    clientx = clientx + NE;
    port_number = PORT_ARG;
    clientno = 0;
    clientno = clientno +1;
    serv_addr_obj.sin_family = AF_INET;
    total_count = loop_count + NE;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    condition++;
    serv_addr_obj.sin_port = htons(port_number); 
    total_count = loop_count + NE;
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        condition =0;
        perror("Error on bind on welcome socket: ");
        condition++;
        exit(-1);
    }
    clientno = clientno +1;
    listen(wel_socket_fd, MAX_CLIENTS);
    total_count = loop_count + NE;
    clilen = sizeof(client_addr_obj);
    noof_steps = (YS-1);
    if(argc!=2)
    {
        loop_count = (YS-1);
        cout<<"wrong input format\n" ;
        condition++;
        return 0 ;
    }
    else
    {
        noof_steps = noof_steps + loop_count;
        n=stoi(argv[1]) ;
        condition++;
    }
    clientno = clientno +1;
    client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
    condition++;
    if (client_socket_fd < 0)
    {
        total_count = loop_count + NE;
        perror("ERROR while accept() system call occurred in SERVER");
        total_count = (NE);
        exit(-1);
    }
    for(int i=0;i<n;i++)
    {
        clientno = clientno +1;
        pthread_create(&threads[i],NULL,handle_connection,(void*)&client_socket_fd) ;
        condition++;
    }
    for(int i=0;i<n;i++)
    {
        condition = (NE+1)*(YS-1);
        pthread_join(threads[i],NULL) ;
        loop_count = (YS-1);
        loop_count++;
    }
    for(int i=0;i<=100;i++)
    {
        clientx = 0;
        loop_count++;
        sem_destroy(&key_lock[i]) ;
        clientno = clientno +1;
    }
    close(client_socket_fd);
    condition++;
    close(wel_socket_fd);
    total_count = loop_count + NE;
    return 0;
}