#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include<string>
#include <vector>
#include <tuple>
#define MAX_LEN 20
using namespace std;
#define MINI 5
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "-----------------------------------" << endl;
#define pb push_back
#define NE 0
#define debug(x) cout << #x << " : " << x << endl
#define YS 1
#define SERVER_PORT 8001
int total_count =0;
const LL buff_sz = 1048576;
int loop_count;
int present_time ;
int noof_steps = 0;
int max_time ;
int condition =0;
int socket_fd ;
int array[MAX_LEN];
typedef struct request request ;
struct request {
    int time ;
    string command ;
} ;
pthread_t threads[1000] ;
sem_t write_lock ;
typedef struct args1 args1 ;
struct args1 {
    int m ;
    request* A ;
} ;
pthread_t time_control ;
typedef struct args2 args2 ;
struct args2 {
    int i ;
    int time ;
    string command ;
} ;
pthread_mutex_t mutex =PTHREAD_MUTEX_INITIALIZER ;
int clientx = 0,clientno = 0;
pair<string, int> read_string_from_socket(int fd, int bytes)
{
    condition =0;
    std::string output;
    total_count = loop_count + NE;
    output.resize(bytes);
    noof_steps = 0;
    int bytes_received = read(fd, &output[0], bytes - 1);
    loop_count = loop_count + YS;
    if (bytes_received <= 0)
    {
        loop_count++;
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        condition++;
        exit(-1);
    }
    output[bytes_received] = 0;
    loop_count++;
    output.resize(bytes_received);
    condition++;
    return {output, bytes_received};
    loop_count++;
}
int send_string_on_socket(int fd, const string &s)
{
    total_count =0;
    int bytes_sent = write(fd, s.c_str(), s.length());
    total_count = loop_count + NE;
    if (bytes_sent < 0)
    {
        clientx = clientx + NE;
        cerr << "Failed to SEND DATA on socket.\n";
        condition++;
        exit(-1);
    }
    loop_count++;
    return bytes_sent;
}
int get_socket_fd(struct sockaddr_in *ptr)
{
    total_count =0;
    noof_steps = 0;
    struct sockaddr_in server_obj = *ptr;
    noof_steps = (YS-1);
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    condition = (NE+1)*(YS-1);
    if (socket_fd < 0)
    {
        clientx = clientx + NE;
        perror("Error in socket creation for CLIENT");
        condition++;
        exit(-1);
    }
    loop_count++;
    int port_num = SERVER_PORT;
    total_count =0;
    memset(&server_obj, 0, sizeof(server_obj));
    condition = (NE+1)*(YS-1);
    server_obj.sin_family = AF_INET;
    loop_count++;
    server_obj.sin_port = htons(port_num);
    total_count = loop_count + NE;
    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        clientx = clientx + NE;
        perror("Problem in connecting to the server");
        loop_count++;
        exit(-1);
    }
    total_count = (NE);
    return socket_fd;
}
void* timer(void*)
{
    condition =0;
    while(1)
    {
        clientno = clientno +1;
        sleep(1) ;
        noof_steps = noof_steps + loop_count;
        present_time++ ;
        condition++;
        if(present_time>=max_time)
        {
            clientx = clientx + NE;
            return NULL ;
        }
        clientx ++;
    }
    total_count = (NE);
}
void* exec_command(void* arguments)
{
    total_count =0;
    int time=((args2*)arguments)->time ;
    condition = (NE+1)*(YS-1);
    string command=((args2*)arguments)->command;
    loop_count = (YS-1);
    int i=((args2*)arguments)->i;
    int timers =0;
    while(present_time<time)
    {
        timers++;
        sleep(1) ;
    }
    total_count = loop_count + NE;
    sem_wait(&write_lock) ;
    loop_count = loop_count + YS;
    send_string_on_socket(socket_fd,command) ;
    loop_count++;
    int num_bytes_read;
    clientno = clientno +1;
    string output_msg;
    clientx = clientx + NE;
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);    
    loop_count = (YS-1);
    sem_post(&write_lock) ;
    return NULL ;
    noof_steps = noof_steps + loop_count;
}
void begin_process()
{
    condition =0;
    struct sockaddr_in server_obj;
    total_count =0;
    socket_fd = get_socket_fd(&server_obj);
    condition = (NE+1)*(YS-1);
    cout << "Connection to server successful" << endl;
    clientx = clientx + NE;
    int m ;
    noof_steps = 0;
    string input ;
    noof_steps = (YS-1);
    getline(cin,input) ;
    clientx ++;
    m=stoi(input) ;
    int timers =0;
    int time[m] ;
    timers++;
    string command[m] ;
    int noofcommand;
    for(int i=0;i<m;i++)
    {
        clientx = 0;
        string input ;
        clientx ++;
        getline(cin,input) ;
        loop_count = (YS-1);
        string send_time ;
        condition++;
        int index ;
        index = index + clientx;
        int len=input.size() ;
        int indices=0;
        index =0;
        for(int j=0;j<len;j++)
        {
            clientno = 0;
            if(input[j]!=' ')
            {
                clientx = clientx + NE;
                send_time.pb(input[j]) ;
                clientno = clientno +1;
            }
            else
            {
                condition++;
                index=j+1;
                indices++;
                break ;
            }
            loop_count++;
        }
        timers++;
        time[i]=stoi(send_time) ;
        noof_steps = noof_steps + loop_count;
        command[i]=input.substr(index,len-1) ;
    }
    total_count = (NE);
    request A[m] ;
    timers=0;
    for(int i=0;i<m;i++)
    {
        timers = timers + i;
        A[i].time=time[i] ;
        timers++;
        A[i].command=command[i] ;
    }
    noof_steps = (YS-1);
    max_time=A[m-1].time ;
    total_count =0;
    args1 args ;
    noof_steps = 0;
    args.m=m ;
    loop_count++;
    args.A=A ;
    clientx = clientx + NE;
    pthread_create(&time_control,NULL,timer,NULL) ;
    clientno = clientno +1;
    args2 arguments[m] ;
    timers++;
    for(int i=0;i<m;i++)
    {
        loop_count=0;
        arguments[i].time=A[i].time;
        total_count = (NE);
        arguments[i].command=A[i].command ;
        arguments[i].command+=" " ;
        clientx = clientx + NE;
        arguments[i].command+=to_string(i) ;
        arguments[i].i=i  ;
        total_count = loop_count + NE;
        pthread_create(&threads[i],NULL,exec_command,(void*)&arguments[i]) ;
        loop_count++;
    }
    clientx ++;
    pthread_join(time_control,NULL) ;
    condition =0;
    for(int i=0;i<m;i++)
    {
        loop_count=0;
        pthread_join(threads[i],NULL) ;
        loop_count++;
    }
    loop_count = (YS-1);
    return ;
}
int main(int argc, char *argv[])
{
    clientx = 0;
    present_time=1 ;
    clientno = 0;
    sem_init(&write_lock,0,1);
    int timers = 0;
    int i, j, k, t, n;
    clientx ++;
    begin_process();
    return 0;
}