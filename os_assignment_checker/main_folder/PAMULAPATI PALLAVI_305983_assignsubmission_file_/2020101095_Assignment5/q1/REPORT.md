# Implemetation 

* In main()
    * After reading the input we create threads for each student and course given as input.
    * Then the main function waits till all the student threads join. 
    * The student threads execute `student_func()`.

* In student_func()
    * The thread sleeps for the specified time and then seat allocation for the student starts.
    
    * Then the student waits till his preference becomes preference 3 and preference 3 get removed if he was not allocated seat in any of this preferred courses.This is implemented using a while loop.
    
    * The while loop is broken when he is allocated a seat and did'nt withdraw the course after listenning to the tutorial.
    
    * The student changes his current preference to the next, either if he withdraws after listening to the tut or the course is removed.
    
    * while accessing the array `available_seat[]` we accuire mutex lock so that other thread do not change the value of seats available in the course.
    
    * After the while loop is executed, the student thread returns back to the main function.

### Not implemented   

* `course_func()` - here we check whether ta is available in the labs under the course to conduct the tut and if the ta is available we conduct the tut and allocate seats. if all the ta's of the labs are left then the course is deleted.