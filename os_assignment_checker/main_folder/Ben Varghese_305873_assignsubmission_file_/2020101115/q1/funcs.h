#ifndef FUNCS_H
#define FUNCS_H

#include "user.h"
#include <unistd.h>
#include <time.h>

void *pref_fill(void *arg);
void *time_count(void *arg);
void *course_working(void *arg);
void allot_TA(course *Course, lab *Labs, int num_labs);
void allot_tut(course *Course, student Students[], int num_students);
void student_choice(course *Courses, int num_courses, student *Student);

#endif
