#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdio.h>
#include <pthread.h>

#define COLOR_PINK     "\x1b[95m"
#define COLOR_RESET    "\x1b[0m"

typedef struct courses {
    int course_id;              // course ID
    char course_name[200];      // course name
    double interest;            // course interest (b/w 0 and 1)
    int *lab_ids;               // set of labs for getting TAs
    int num_course_labs;        // number of labs
    int curr_TA_id;             // the ID of current TA
    char curr_TA_lab[100];      // the lab of current TA (may be NULL)
    int course_max_slot;        // max number of slots
    int status;                 // 0 => waiting for TA
                                // 1 => waiting for slots to be filled
                                // 2 => tutorial being conducted
                                // 3 => withdrawn
    pthread_cond_t course_cond;
} course;

typedef struct students {
    int student_id;             // student ID
    int pref1;                  // first preference (highest priority)
    int pref2;                  // second preference
    int pref3;                  // third preference (lowest priority)
    int curr_pref;              // current preference
    double calibre;             // student's calibre quotient
    int time;                   // preference submission time (in seconds)
    int status;                 // indicates what the student is doing -
                                    // 0 => waiting for 1st pref
                                    // 1 => attending 1st pref
                                    // 2 => waiting for 2nd pref
                                    // 3 => attending 2nd pref
                                    // 4 => waiting for 3rd pref
                                    // 5 => attending 3rd pref
                                    // 6 => selected course / didn't take any
} student;

typedef struct ta {
    int id;
    int times;                  // the number of times the TA was a TA
    int status;                 // indicates what the TA is doing -
                                    // 0 => available to TA
                                    // 1 => currently busy being TA
} TA;

typedef struct labs {
    int lab_id;                 // lab ID
    char lab_name[200];         // lab name
    int n_i;                    // number of TAs
    int limit;                  // lab limit
    TA *ta_arr;                 // array of TAs
} lab;

typedef struct CSL {
    course *Course;
    student *Student;
    lab *Lab;
    int num_courses;
    int num_students;
    int num_labs;
} csl;

typedef struct Course_student {
    course *Course;
    student *Student;
    int num_students;
    int num_courses;
    int time;
    TA *ta_mentor;
} course_student;

#endif
