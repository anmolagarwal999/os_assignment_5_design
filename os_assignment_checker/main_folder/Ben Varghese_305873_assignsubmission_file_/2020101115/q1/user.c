#include "user.h"

void init_course(course Courses[], int num_courses) {
    int num_course_labs;
    for(int i=0; i<num_courses; i++) {
        Courses[i].course_id = i;
        scanf("%s", Courses[i].course_name);
        scanf("%lf", &Courses[i].interest);
        scanf("%d", &Courses[i].course_max_slot);
        scanf("%d", &Courses[i].num_course_labs);
        num_course_labs = Courses[i].num_course_labs;
        Courses[i].curr_TA_id = -1;
        Courses[i].status = 0;
        strcpy(Courses[i].curr_TA_lab, "None");
        Courses[i].lab_ids = (int *) malloc(num_course_labs*sizeof(int));
        for(int j=0; j<num_course_labs; j++) {
            scanf("%d", &Courses[i].lab_ids[j]);
        }
        pthread_cond_init(&Courses[i].course_cond, NULL);
    }
}

void print_courses(course Courses[], int num_courses) {
    for(int i=0; i<num_courses; i++) {
        printf("Course ID: %d\n", Courses[i].course_id);
        printf("Course name: %s\n", Courses[i].course_name);
        printf("Course interest: %lf\n", Courses[i].interest);
        printf("Course max slots: %d\n", Courses[i].course_max_slot);
        printf("Course number of labs: %d\n", Courses[i].num_course_labs);
        printf("Lab IDs - \n");
        for(int j=0; j<Courses[i].num_course_labs; j++) {
            printf("\tLab %d ID: %d\n", j+1, Courses[i].lab_ids[j]);
        }
        printf("\n");
    }
}

void init_student(student Students[], int num_students) {
    for(int i=0; i<num_students; i++) {
        Students[i].student_id = i;
        scanf("%lf", &Students[i].calibre);
        scanf("%d", &Students[i].pref1);
        scanf("%d", &Students[i].pref2);
        scanf("%d", &Students[i].pref3);
        scanf("%d", &Students[i].time);
        Students[i].status = -1;
        Students[i].curr_pref = Students[i].pref1;
    }
}

void print_students(student Students[], int num_students) {
    for(int i=0; i<num_students; i++) {
        printf("Student ID: %d\n", Students[i].student_id);
        printf("Student calibre: %lf\n", Students[i].calibre);
        printf("Student first preference: %d\n", Students[i].pref1);
        printf("Student second preference: %d\n", Students[i].pref2);
        printf("Student third preference: %d\n", Students[i].pref3);
        printf("Student filling time: %d\n", Students[i].time);
        printf("Status: ");
        if(Students[i].status==0) {
            printf("Waiting for first preference\n");
        }
        else if(Students[i].status==1) {
            printf("Attending first preference\n");
        }
        else if(Students[i].status==2) {
            printf("Waiting for second preference\n");
        }
        else if(Students[i].status==3) {
            printf("Attending second preference\n");
        }
        else if(Students[i].status==4) {
            printf("Waiting for  third preference\n");
        }
        else if(Students[i].status==5) {
            printf("Attending third preference)\n");
        }
        else if(Students[i].status==6) {
            printf("Withdrawn\n");
        }
        else printf("Unknown");
        printf("\n");
    }
}

void init_lab(lab Labs[], int num_labs) {
    for(int i=0; i<num_labs; i++) {
        Labs[i].lab_id = i;
        scanf("%s", Labs[i].lab_name);
        scanf("%d", &Labs[i].n_i);
        scanf("%d", &Labs[i].limit);
        Labs[i].ta_arr = (TA *) malloc(Labs[i].n_i*sizeof(TA));
        for(int j=0; j<Labs[i].n_i; j++) {
            Labs[i].ta_arr[j].id = j;
            Labs[i].ta_arr[j].times = 0;
            Labs[i].ta_arr[j].status = 0;
        }
    }
}

void print_labs(lab Labs[], int num_labs) {
    for(int i=0; i<num_labs; i++) {
        printf("Lab ID: %d\n", Labs[i].lab_id);
        printf("Lab name: %s\n", Labs[i].lab_name);
        printf("Lab number of TAs: %d\n", Labs[i].n_i);
        printf("Lab limit for TAs: %d\n", Labs[i].limit);
        printf("TAs - \n");
        for(int j=0; j<Labs[i].n_i; j++) {
            printf("TA ID: %d\n", Labs[i].ta_arr[j].id);
            printf("TA times: %d\n", Labs[i].ta_arr[j].times);
            printf("TA status: %d\n", Labs[i].ta_arr[j].status);
        }
        printf("\n");
    }
}

int find_current_pref(student Student) {
    if(Student.status==0) {
        return Student.pref1;
    }
    else if(Student.status==2) {
        return Student.pref2;
    }
    else if(Student.status==4) {
        return Student.pref3;
    }
    else return -1;
}
