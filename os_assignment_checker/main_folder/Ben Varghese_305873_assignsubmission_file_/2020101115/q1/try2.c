#include "funcs.h"

pthread_cond_t student_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t courses_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t sc_cond = PTHREAD_COND_INITIALIZER;

void *pref_fill(void *arg) {
    course_student *cs = (course_student *) arg;
    student Student = *cs->Student;
    course *Courses = cs->Course;
    int num_courses = cs->num_courses;
    sleep(Student.time);
    pthread_mutex_lock(&student_lock);
    printf("Student %d has filled" COLOR_PINK " in " COLOR_RESET "preferences" COLOR_PINK " for " COLOR_RESET "course registration\n", Student.student_id);
    cs->Student->status = 0;
    Student = *cs->Student;
    pthread_cond_broadcast(&sc_cond);
    while(cs->Student->status%2==0) {
        pthread_cond_wait(&student_cond, &student_lock);
        Student = *cs->Student;
    }
    if(cs->Student->status%2!=0) {
        double interest;
        char cname[200];
        for(int i=0; i<num_courses; i++) {
            if(cs->Student->status==1 && Student.pref1==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
            else if(cs->Student->status==3 && Student.pref2==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
            else if(cs->Student->status==5 && Student.pref3==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
        }
        double probability = interest*Student.calibre;
        probability *= 100;
        int prob = (int) probability;
        int choice = rand()%100;
        if(choice<prob) {
            cs->Student->status = 6;
            printf("Student %d has selected the course %s permanently\n", Student.student_id, cname);
        }
        else {
            printf("Student %d has withdrawn from course %s\n", Student.student_id, cname);
            int flag = 0;
            char ncname[200];
            if(cs->Student->status==1) {
                for(int i=0; i<num_courses; i++) {
                    if(Student.pref2==Courses[i].course_id) {
                        if(Courses[i].status==3) {
                            cs->Student->status = 3;
                            flag++;
                            break;
                        }
                        strcpy(ncname, Courses[i].course_name);
                    }
                }
                if(flag==0) {
                    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n", Student.student_id, cname, ncname);
                }
            }
            if(cs->Student->status==3) {
                for(int i=0; i<num_courses; i++) {
                    if(Student.pref3==Courses[i].course_id) {
                        if(Courses[i].status==3) {
                            cs->Student->status = 5;
                            flag++;
                            break;
                        }
                        strcpy(ncname, Courses[i].course_name);
                    }
                }
                if(flag==0) {
                    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
                else if(flag==1 && cs->Student->status==3) {
                    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
                else if(flag==1 && cs->Student->status==5) {
                    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
            }
            cs->Student->status += 1;

            if(cs->Student->status==6) {
                printf("Student %d could not get any of his preferred courses\n", Student.student_id);
            }
        }
    }

    pthread_mutex_unlock(&student_lock);
}

void *course_working(void *arg) {
    csl *c = (csl *) arg;
    course *Course = c->Course;
    student *Students = c->Student;
    lab *Labs = c->Lab;
    //int num_courses = c->num_courses;
    int num_students = c->num_students;
    int num_labs = c->num_labs;
    
    while(1) {
        int flag = 0;           // will be set to 1 if a TA is found
        int count = 0;          // keeps track of the number of TAs who have already reached their limit
        pthread_mutex_lock(&course_lock);
        while(1) {
            for(int i=0; i<Course->num_course_labs; i++) {
                for(int j=0; j<num_labs; j++) {
                    if(Labs[j].lab_id==Course->lab_ids[i]) {
                        for(int k=0; k<Labs[j].n_i; k++) {
                            if(Labs[j].ta_arr[k].status==0 && Labs[j].ta_arr[k].times<Labs[j].limit) {
                                printf("TA %d from lab %s has been allocated to course %s. Past experience count: %d\n", Labs[j].ta_arr[k].id, Labs[j].lab_name, Course->course_name, Labs[j].ta_arr[k].times);
                                // Past experience count gives the number of times the TA has been a TA
                                flag = 1;
                                Labs[j].ta_arr[k].status = 1;
                                Labs[j].ta_arr[k].times++;
                                Course->status = 1;
                                Course->curr_TA_id = Labs[j].ta_arr[k].id;
                                strcpy(Course->curr_TA_lab, Labs[j].lab_name);
                                break;
                            }
                            else if(Labs[j].ta_arr[k].times==Labs[j].limit) {
                                count++;
                            }
                        }
                    }
                    if(flag==1) {
                        break;
                    }
                }
                if(flag==1) {
                    break;
                }
            }
            if(flag==0) {                               // no TA was found
                int limit = 0;
                for(int i=0; i<num_labs; i++) {         // checking if any TA will be available or the course should be withdrawn
                    limit += Labs[i].n_i;
                }
                if(count==limit) {
                    printf("Course %s has been withdrawn\n", Course->course_name);
                    Course->status = 3;
                }
                else {
                    pthread_cond_wait(&courses_cond, &course_lock);
                    continue;
                }
            }
            break;
        }

        int d = rand()%Course->course_max_slot;
        d++;
        printf("Course %s has been allocated %d seats\n", Course->course_name, d);
        int w=0;
        
        for(int i=0; i<num_students; i++) {
            if(Students[i].curr_pref==Course->course_id && (Students[i].status==0 || Students[i].status==2 || Students[i].status==4)) {
                w++;
                pthread_mutex_lock(&student_lock);
                Students[i].status++;
                pthread_cond_broadcast(&student_cond);
                pthread_mutex_unlock(&student_lock);
                printf("Student %d has been allocated a seat" COLOR_PINK " in " COLOR_RESET "course %s\n", Students[i].student_id, Course->course_name);
                if(w==d) {
                    break;
                }
            }
        }

        Course->status = 2;

        int flag2 = 0;
        for(int j=0; j<num_labs; j++) {
            if(strcmp(Labs[j].lab_name, Course->curr_TA_lab)==0) {
                for(int k=0; k<Labs[j].n_i; k++) {
                    if(Labs[j].ta_arr[k].id == Course->curr_TA_id) {
                        Labs[j].ta_arr[k].status = 0;
                        flag2 = 1;
                        break;
                    }
                }
            }
            if(flag2==1) {
                break;
            }
        }
        
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", Course->course_name, w, d);
        printf("TA %d from lab %s has completed the tutorial" COLOR_PINK " for " COLOR_RESET "course %s\n", Course->curr_TA_id, Course->curr_TA_lab, Course->course_name);
        Course->curr_TA_id = -1;
        strcpy(Course->curr_TA_lab, "None");
        pthread_mutex_unlock(&course_lock);
        sleep(5);
        
        pthread_cond_broadcast(&courses_cond);
        int sc_flag = 0;
        for(int i=0; i<num_students; i++) {
            if(Students[i].status!=6) {
                sc_flag = 1;
                break;
            }
        }
        if(sc_flag==1) {
            continue;
        }

        break;
    }
}
