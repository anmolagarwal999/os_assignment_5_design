#ifndef USER_H
#define USER_H

#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <string.h>
#include "structs.h"

pthread_mutex_t course_lock;
pthread_mutex_t student_lock;
pthread_mutex_t sc_lock;

void init_course(course Courses[], int num_courses);
void init_student(student Students[], int num_students);
void init_lab(lab Labs[], int num_labs);

void print_courses(course Courses[], int num_courses);
void print_students(student Students[], int num_students);
void print_labs(lab Labs[], int num_labs);

int find_current_pref(student Student);

#endif
