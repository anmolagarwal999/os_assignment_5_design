#include "funcs.h"

int main() {
    srand(time(0));
    int num_students, num_labs, num_courses;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course *Courses = (course *) malloc(num_courses*sizeof(course));
    student *Students = (student *) malloc(num_students*sizeof(student));
    lab *Labs = (lab *) malloc(num_labs*sizeof(lab));

    init_course(Courses, num_courses);
    init_student(Students, num_students);
    init_lab(Labs, num_labs);

    printf("\n");
    
    pthread_mutex_init(&course_lock, NULL);
    pthread_mutex_init(&student_lock, NULL);
    pthread_mutex_init(&sc_lock, NULL);

    pthread_t course_threads[num_courses];
    pthread_t student_threads[num_students];

    course_student cs[num_students];
    for(int i=0; i<num_students; i++) {
        cs[i].Student = &Students[i];
        cs[i].Course = Courses;
        cs[i].num_courses = num_courses;
    }
    
    for(int i=0; i<num_students; i++) {
        pthread_create(&student_threads[i], NULL, pref_fill, (void *)&cs[i]);
    }
    sleep(1);
    csl c[num_courses];
    for(int i=0; i<num_courses; i++) {
        c[i].Course = &Courses[i];
        c[i].Student = Students;
        c[i].Lab = Labs;
        c[i].num_courses = num_courses;
        c[i].num_students = num_students;
        c[i].num_labs = num_labs;
    }
    for(int i=0; i<num_courses; i++) {
        pthread_create(&course_threads[i], NULL, course_working, (void *)&c[i]);
    }

    
    for(int i=0; i<num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }
    for(int i=0; i<num_students; i++) {
        pthread_join(student_threads[i], NULL);
    }

    pthread_mutex_destroy(&course_lock);
    pthread_mutex_destroy(&student_lock);
}
