The program consists of the following files - 
    1. struct.h
        Has all the structs for student, course, lab, and some other structs necessary to pass arguments to the thread functions.
    2. user.h
        Has the prototypes for functions for initializing the student, course, and lab structs.
    3. user.c
        Has the functions for initializing the student, course, and lab structs.
    4. funcs.h
        Has the prototypes for all the main functions of the program
    5. try2.c
        Has the main functions of the program, including -
            a. pref_fill
                Works with the student threads.
                First, it waits for the student to fill their preferences (sleep until student's time is reached)
                Then, using conditional variables, move to the course_working function.
                Once it returns from that function, the courses would have been allocated seats and tutorials would have been held.
                Then, this function updates the students, including whether they choose that course or withdraw.
            b. course_working
                Works with the course threads.
                First, it allots a TA to the course.
                If no TA is available, it'll conditionally wait for a TA to be available.
                If all TAs have reached their limit, course is withdrawn.
                Then, seats are allocated to the course and students are allocated these seats.
                Then, the tutorial is conducted.
    6. try.c (this is essentially main.c)
        Declares the student and course threads and calls functions appropriately.