#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include<semaphore.h>

/////////////////////////////
#include <bits/stdc++.h>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

typedef long long LL;

#define pb push_back
#define debug(x) cout << #x << " : " << x << endl
#define part cout << "-----------------------------------" << endl;

///////////////////////////////
#define MAX_CLIENTS 4
#define PORT_ARG 8001

const int initial_msg_len = 256;

////////////////////////////////////

const LL buff_sz = 1048576;
///////////////////////////////////////////////////


map<int,string> dict ;
sem_t key_lock[101] ;
sem_t read_lock ;
pthread_t threads[1000] ;

typedef struct args args ;
struct args {
    int i ;
    string command ;
} ;

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }

    return bytes_sent;
}

///////////////////////////////

void* handle_connection(void* ptrclient_socket_fd)
{    
    int received_num, sent_num;
    int client_socket_fd=*((int*)ptrclient_socket_fd) ;
    int ret_val = 1;
    string cmd;
    sem_wait(&read_lock) ;
    tie(cmd, received_num) = read_string_from_socket(client_socket_fd, buff_sz);
    string msg_to_send_back = "done" ;
    int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
    if (sent_to_client == -1)
    {
        perror("Error while writing to client. Seems socket has been closed");
    }    
    sem_post(&read_lock) ;
    ret_val = received_num;
    if (ret_val <= 0)
    {
        printf("Server could not read msg sent from client\n");
        return NULL ;
    }
    string temp ;
    vector<string> parts ;
    int n=cmd.size() ;
    for(int i=0;i<n;i++)
    {
        if(cmd[i]!=' ')
        {
            temp.pb(cmd[i]) ;
        }
        else
        {
            parts.pb(temp) ;
            temp.clear() ;
        }
    }
    parts.pb(temp) ;
    int num_commands=parts.size() ;
    int key=stoi(parts[1]) ;
    int index=stoi(parts[num_commands-1]) ;
    cout<<index<<":"<<pthread_self()<<":" ;
    if(parts[0]=="insert")
    {
        sem_wait(&key_lock[key]) ;
        string value=parts[2] ;
        if(dict.find(key)!=dict.end())
        {
            printf("Key already exists\n") ;
        }
        else
        {
            dict.insert({key,value}) ;
            printf("Insertion successful\n") ;
        }
        sem_post(&key_lock[key]) ;
    }
    else if(parts[0]=="delete")
    {
        sem_wait(&key_lock[key]) ;
        if(dict.find(key)!=dict.end())
        {
            dict.erase(key) ;
            printf("Deletion successful\n") ;
        }
        else
        {
            printf("No such key exists\n") ;
        }
        sem_post(&key_lock[key]) ;
    }
    else if(parts[0]=="update")
    {
        sem_wait(&key_lock[key]) ;
        string value=parts[2] ;        
        if(dict.find(key)!=dict.end())
        {
            dict[key]=value ;
            cout<<dict[key]<<"\n" ;
        }
        else
        {
            cout<<"Key does not exist\n" ;
        }
        sem_post(&key_lock[key]) ;
    }
    else if(parts[0]=="concat")
    {
        int key1=stoi(parts[1]) ;
        int key2=stoi(parts[2]) ;
        sem_wait(&key_lock[key1]) ;
        sem_wait(&key_lock[key2]) ;
        if(dict.find(key1)==dict.end()||dict.find(key2)==dict.end())
        {
            cout<<"Concat failed as at least one of the keys does not exist\n" ;
        }
        else
        {
            string value1=dict[key1] ;
            string value2=dict[key2] ;
            dict[key1]=value1+value2 ;
            dict[key2]=value2+value1 ;
            cout<<dict[key2]<<"\n" ;
        }
        sem_post(&key_lock[key1]) ;
        sem_post(&key_lock[key2]) ;
    }
    else if(parts[0]=="fetch")
    {
        sem_wait(&key_lock[key]) ;
        if(dict.find(key)!=dict.end())
        {
            cout<<dict[key]<<"\n" ;
        }
        else
        {
            cout<<"Key does not exist\n" ;
        }
        sem_post(&key_lock[key]) ;
    }

}

int main(int argc, char *argv[])
{
    sem_init(&read_lock, 0, 1);
    for(int i=0;i<=100;i++)
    {
        sem_init(&key_lock[i],0,1) ;
    }    
    int i, j, k, t, n;
    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;
    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); 
    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    listen(wel_socket_fd, MAX_CLIENTS);
    clilen = sizeof(client_addr_obj);
    if(argc!=2)
    {
        cout<<"wrong input format\n" ;
        return 0 ;
    }
    else
    {
        n=stoi(argv[1]) ;
    }
    client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
    if (client_socket_fd < 0)
    {
        perror("ERROR while accept() system call occurred in SERVER");
        exit(-1);
    }
    for(int i=0;i<n;i++)
    {
        pthread_create(&threads[i],NULL,handle_connection,(void*)&client_socket_fd) ;
    }
    for(int i=0;i<n;i++)
    {
        pthread_join(threads[i],NULL) ;
    }
    for(int i=0;i<=100;i++)
    {
        sem_destroy(&key_lock[i]) ;
    }
    close(client_socket_fd);
    close(wel_socket_fd);
    return 0;
}