                                                OPERATING SYSTEMS AND NETWORKS ASSIGNMENT-5

Name: P.Praneeth Varma
Roll no:2020101040

3rd question:
-> I used c++ to code this question hence during compilation g++ should be used along with -pthread flag.
-> There are 2 codes for this question client_sim.cpp and server_prog.cpp
-> Note that first we need to run server code before running client code and I have used map data structure in C++ to execute the commands. First the input should be given to client then the output should be seen through server code and the description of how the functions work is mentioned in the report of this question
