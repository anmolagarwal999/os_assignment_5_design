#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <iostream>
#include <assert.h>
#include <tuple>
#include <queue>
#include<map>
#include<string>
#include<sstream>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"info
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

///////////////////////////////
#define PORT_ARG 8001
#define BUFFER_SIZE  1048576
#define DEFAULT_WORKER_THREADS 5
#define MAX_CLIENTS 4
///////////////////////////////////////////////////

queue<pair<int, string>> incoming_requests;    // maps the client socket to the request   
pthread_mutex_t incoming_requests_mutex;       // to lock the process of pushing by a worker thread in a routine
pthread_cond_t incoming_requests_cond;         // to broadcast when a new request is pushed

// map
map<int, string> client_query_map;

// mutex for commandExecute
pthread_mutex_t commandExecute_mutex;

string commandExecute(string cmd){
    vector<string> split_command; 
    string status = "Some error occured.....";

    istringstream ss(cmd);
    string temp;
    while(getline(ss, temp, ' ')){
        split_command.push_back(temp);
    }   

    if(split_command.size() > 4){
        status = "Invalid number of arguments.....";
    }

    else if(split_command.size() == 4){
        if(split_command[1] == "insert"){

            int key = stoi(split_command[2]);
            string value = split_command[3];
            
            //lock mutex
            pthread_mutex_lock(&commandExecute_mutex);

            if(client_query_map[key] != ""){
                status = "Key already exists";
            }
            else{
                client_query_map[key] = value;
                status = "Insertion successful";
            }

            //unlock mutex
            pthread_mutex_unlock(&commandExecute_mutex);

        }
        else if(split_command[1] == "update"){
            int key = stoi(split_command[2]);
            string value = split_command[3];

            //lock mutex
            pthread_mutex_lock(&commandExecute_mutex);

            if(client_query_map[key] != ""){
                client_query_map[key] = value;
                status = value;
            }
            else {
                status = "Key does not exist";
            }

            //unlock mutex
            pthread_mutex_unlock(&commandExecute_mutex);
        }

        else if(split_command[1] == "concat"){
            int key_1 = stoi(split_command[2]);
            int key_2 = stoi(split_command[3]);

            //lock mutex
            pthread_mutex_lock(&commandExecute_mutex);

            if(client_query_map[key_1] != "" && client_query_map[key_2] != ""){
                string temp = client_query_map[key_1];
                client_query_map[key_1] += client_query_map[key_2];
                client_query_map[key_2] += temp;
                status = client_query_map[key_2];
            }
            else {
                status = "Concat failed as at least one of the keys does not exist";
            }

            //unlock mutex
            pthread_mutex_unlock(&commandExecute_mutex);
        }

        else{
            status = "Invalid command.....";
        }
    }
    else if(split_command.size() == 3){
        if(split_command[1] == "delete"){
            int key = stoi(split_command[2]);
            //lock mutex
            pthread_mutex_lock(&commandExecute_mutex);
            if(client_query_map[key] != ""){
                client_query_map.erase(key);
                status = "Deletion successful";
            }
            else {
                status = "No such key exists";
            }
            //unlock mutex
            pthread_mutex_unlock(&commandExecute_mutex);
        }
        else if(split_command[1] == "fetch"){
            int key = stoi(split_command[2]);
            //lock mutex
            pthread_mutex_lock(&commandExecute_mutex);
            if(client_query_map[key] != ""){
                status = client_query_map[key];
            }
            else {
                status = "Key does not exist";
            }
            //unlock mutex
            pthread_mutex_unlock(&commandExecute_mutex);
        }
    }

    else {
        status = "Invalid command.....";
    }

    return status;
}


int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
    return bytes_sent;
}


 void handle_connection(int fd){
    char buffer[BUFFER_SIZE];
    // clear buffer
    memset(buffer, 0, BUFFER_SIZE);
    int bytes_read = read(fd, buffer, BUFFER_SIZE);
    if (bytes_read < 0)
    {
        cerr << "Failed to READ DATA on socket.\n";
        exit(-1);
    }
    string s(buffer);
    cout << "Received: " << s << endl;
    //send_string_on_socket(fd, s);
    
    // add the request s to the queue
    // lock the queue
    pthread_mutex_lock(&incoming_requests_mutex);
    incoming_requests.push(make_pair(fd, s));
    // unlock the queue
    pthread_mutex_unlock(&incoming_requests_mutex);
    // broadcast the worker thread
    pthread_cond_broadcast(&incoming_requests_cond);
    //close(fd);
 }


void *workerRoutine(void *args){
    while(1){
        int client_socket_fd;
        string client_command; 

        // check if command can be executed by the worker thread
        pthread_mutex_lock(&incoming_requests_mutex);
        while(incoming_requests.empty()){
            pthread_cond_wait(&incoming_requests_cond, &incoming_requests_mutex);
        }
       
        // store incoming requests queue 
        tie(client_socket_fd, client_command) = incoming_requests.front();
        // pop
        incoming_requests.pop();
        pthread_mutex_unlock(&incoming_requests_mutex);

        // executed thread_id
        string status = to_string(pthread_self()) + ":";
        // execute client_command
        
        status += commandExecute(client_command);

        // delay
        sleep(2);

        // send response
        int bytes = send_string_on_socket(client_socket_fd, status);

        //finish connection
        close(client_socket_fd);

    }
}




int main(int argc, char *argv[]){

    // initialize mutex and conds
    pthread_mutex_init(&incoming_requests_mutex, NULL);
    pthread_cond_init(&incoming_requests_cond, NULL);

    // initialize all worker threads

    int W_THREADS = (argc > 1) ? atoi(argv[1]) : DEFAULT_WORKER_THREADS;
    pthread_t wt_pool[W_THREADS];
    for(int i = 0; i < W_THREADS; i++){
        if(pthread_create(&wt_pool[i], NULL, &workerRoutine, NULL)){
            cerr << "Failed to create worker thread.\n";
            exit(-1);
        }
    }

    // not required to join

    int wel_socket_fd, client_socket_fd, port_number;
    socklen_t clilen;

    struct sockaddr_in serv_addr_obj, client_addr_obj;

    wel_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (wel_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }

    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number); //process specifies port

    if (bind(wel_socket_fd, (struct sockaddr *)&serv_addr_obj, sizeof(serv_addr_obj)) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    listen(wel_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT with " << W_THREADS << " worker threads" << endl;
    clilen = sizeof(client_addr_obj);

    while (1)
    {
        printf("Waiting for a new client to request for a connection\n");
        client_socket_fd = accept(wel_socket_fd, (struct sockaddr *)&client_addr_obj, &clilen);
        if (client_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }

        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));
       
       // push to the queue first
        handle_connection(client_socket_fd);
        
    }

    // destroy mutex and conds
    pthread_mutex_destroy(&incoming_requests_mutex);
    pthread_cond_destroy(&incoming_requests_cond);

    close(wel_socket_fd);
 
    return 0;
}