#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

/////////////////////////////
#include <pthread.h>
#include <iostream>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;
/////////////////////////////

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

///////////////////////////////
#define SERVER_PORT 8001
#define BUFFER_SIZE 1048576
///////////////////////////////

using namespace std; 

typedef struct {
    int req_idx; 
    int delay; 
    string command_with_args;
    string response = "";       // to be acknowledged from server
} Message;


int send_string_on_socket(int fd, const string &s)
{
    //cout << "send_string_on_socket: " << s << endl;
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
    return bytes_sent;
}

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }

    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}

void *clientRoutine(void *args){
    Message msg = *(Message*)args;

    struct sockaddr_in server_obj;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }

    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order

    // sleep before connecting to server
    sleep(msg.delay);

    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //cout << "Connection to server successful" << endl;

    // for a given user thread, get the input command and send it to the server
    int bytes = send_string_on_socket(socket_fd, msg.command_with_args);  // error taken care of in source function

    // get back ack from the server, and store response in msg.response
    int bytes_out;
    tie(msg.response, bytes_out) = read_string_from_socket(socket_fd, BUFFER_SIZE);

    // print required info
    cout << msg.req_idx << ":" << msg.response << endl; 

    // end connection
    close(socket_fd);
}

int main(int argc, char *argv[]){

    int NUM_USER_THREADS; cin >> NUM_USER_THREADS;

    // pack the input into array of struct message that's defined

    Message messages[NUM_USER_THREADS];
    for(int i=0; i<NUM_USER_THREADS; i++){
        int t; cin >> t;
        messages[i].delay = t;
        messages[i].req_idx = i;
        string command_with_args; getline(cin, command_with_args);
        messages[i].command_with_args = command_with_args;
    }

    // creating threads corresponding to every single input command.

    pthread_t user_threads[NUM_USER_THREADS];
    for(int i=0; i<NUM_USER_THREADS; i++){
        if(pthread_create(&user_threads[i], NULL, &clientRoutine, &messages[i])){
            cout << "Some error while creating the user threads....." << endl;
        }
    }
    for(int i=0; i<NUM_USER_THREADS; i++){
        if(pthread_join(user_threads[i], NULL)){
            cout << "Some error while joining the user threads....." << endl; 
        }
    }
    
    return 0;
}