# Assignment - 5
> Name : Abhijnan Vegi
> Roll No : 2020101004

Running the `make` command will compile the code for all the questions. 

Executables are

q1 - `cp`

q2 - `std`

q3 - `srvr` for server and `clnt` for client.

You can remove the binaries by running `make clean`.  