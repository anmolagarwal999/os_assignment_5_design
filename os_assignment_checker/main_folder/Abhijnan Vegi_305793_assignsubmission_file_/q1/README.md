# Course Portal

I implement course portal using two threads. One thread for the course, that is acquiring a TA, and conducting a tutorial. The other thread is for students, that is apply for a course, attend a tutorial and change preference.

All the threads are started as the input is being read.

## Student thread
1. Student sleeps till his registration time. `sleep(registration_time)`
2. Student applies for a course by acquiring the course lock and incrementing the students waiting for the course.
3. It also checks if course has been withdrawn, if yes it goes to change priority.
4. Student now sleeps on the condition variable which will be signalled once the seats are allocated.
5. Students wakes up, decrements students waiting for the course and releases the course lock.
6. Student now acquires the tut lock, increments students waiting for the tutorial and sleeps on the condition variable.
7. Student wakes up, decides if it wants to change priority or not, if it does it repreats all steps from step 2.

## Course thread
1. Waits until there's atleast one student waiting for the course.
2. Iterates through the list of labs, each lab has a list of TAs. The course tries acquiring the lock of each TA and if it fails it goes to the next. It doesn't busy wait since it use trylock.
3. While iterating it also checks if it can get TA after some periuod of waiting, if it doesn't, the course is withdrawn and all the sleeping students are woken up with a broadcast.
4. If it gets a TA, it stores the available students in a variable and signals the condition variable the number of seats allocated.
5. It then waits for all the students woken up to join the tutorial.
6. Sleeps for some time and then broadcasts on the condition variable to wake up all the students.
7. TA is now released and the course thread goes back to step 1.
