# The clasico experience

I implemented this simulation with 3 threads, The match simulation which updates the score board, spectator threads and a group thread.

## Spectator thread
1. Sleeps till he arrives at the stadium.
2. Spawns threads which acquire a seat for him in any of the preferred zones, and waits on a condition variable until the seat is available. Working of these threads is described later.
3. If the condition variable times out he leaves the simulation.
4. If he acquires a seat, he sleeps on a condition variable in queue for the opponent teams scoreboard. This is signalled whenever the opponent team scores a goal, allowing spectator to wake up and decide if he has to leave the simulation.
5. If the condition variable times out he leaves to the exit after signalling the semaphore for his seat.

## Thread for acquiring seat
1. Do a timed wait on the semaphore for a seat of particular type.
2. If not acquired, exit thread.
3. If acquired, acquire a the lock whose pointer is passed as an argument.
4. If seat is empty, update seat_type with the type of seat acquired and signal the condition variable on which the spectator is sleeping.
6. If seat is not empty, post the semaphore for the seat
7. Release the lock acquired and exit.

## Group thread
1. pthread_join on all the spectator threads that are a part of the group.
2. Print "group is leaving for dinner" and exit.

## Match thread
1. Read input and wait till time of next goal chance
2. Decide if team score the goal
3. If team scored, iterate through the queue of condition variables for that team and signal them.
4. Repeat from step 1.
