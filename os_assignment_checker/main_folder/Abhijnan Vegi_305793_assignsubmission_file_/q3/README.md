# Multithreaded client and server

## Server
1. Spawn threads which wait on a condition varaible which will be signalled when a request arrives.
2. When a request arrives, add its socket fd to a queue, and signal the condition variable.
3. Repeat until server is stopped.

### Server thread
1. Read data from socket.
2. Parse type of operation and keys.
3. Acquire lock on that particular key, this is done by creating a map of type `map<int,struct{value,lock}>`.
4. In case of acquiring multiple locks it tries acquiring both the lock using trylock, but if any one fails it releases all locks and tries again.
5. Carry out the operation and release the keys.
6. Back to step 1

# Client
1. Read input and spawn threads.
2. Each thread waits till the time for sending request comes, sends it and waits for response.
3. When response is received, print it.