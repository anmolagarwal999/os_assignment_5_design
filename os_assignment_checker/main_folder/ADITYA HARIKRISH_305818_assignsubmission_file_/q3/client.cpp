#include "utils.h"

#define SERVER_PORT 8001
#define MAX_BUFFER_SIZE 10000

struct thread_details {
    int id;
};

struct Request {
    pthread_t tid;
    int64_t timeOfRequest;
    int i, client_fd;
    std::string command;
    pthread_mutex_t mutex;
};

std::vector<Request> client;
pthread_mutex_t stdoutLock;

int get_client_socket_fd() {
    sockaddr_in server_obj;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        perror("Error in socket creation for CLIENT");
        exit(EXIT_FAILURE);
    }
    int port_num = SERVER_PORT;
    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);
    if (connect(socket_fd, (sockaddr *)&server_obj, sizeof(server_obj)) < 0) {
        perror("Problem in connecting to the server");
        exit(EXIT_FAILURE);
    }
    return socket_fd;
}

void *clientThread(void *(arg)) {
    int id = ((thread_details *)arg)->id;
    client[id].client_fd = get_client_socket_fd();
    std::string command = client[id].command;
    sleep(client[id].timeOfRequest);
    pthread_mutex_lock(&client[id].mutex);
    ssize_t writeSize = write(client[id].client_fd, command.c_str(), command.length());

    if (writeSize < 0) {
        std::cerr << "Failed To communicate with the server\n";
        pthread_mutex_unlock(&client[id].mutex);
        return NULL;
    }
    pthread_mutex_unlock(&client[id].mutex);

    std::string buffer(MAX_BUFFER_SIZE, '.');
    int byte_read = read(client[id].client_fd, &buffer[0], MAX_BUFFER_SIZE - 1);
    buffer[byte_read] = '\0';
    buffer.resize(byte_read);

    pthread_mutex_lock(&client[id].mutex);
    if (byte_read <= 0) {
        std::cerr << "Failed To communicate with the server\n";
        pthread_mutex_unlock(&client[id].mutex);
        return NULL;
    }
    pthread_mutex_lock(&stdoutLock);
    std::cout << client[id].i << " : " << gettid() << " : " << buffer << std::endl;
    pthread_mutex_unlock(&stdoutLock);

    pthread_mutex_unlock(&client[id].mutex);

    return NULL;
}

int main() {
    pthread_mutex_init(&stdoutLock, NULL);

    int num_clients;
    std::cin >> num_clients;
    std::string inputLine;
    getline(std::cin, inputLine);
    for (int i = 0; i < num_clients; i++) {
        std::string str, time_req;
        str.clear();
        time_req.clear();
        getline(std::cin, str);
        std::string::iterator itr = str.begin();

        for (itr = str.begin(); *itr != ' '; ++itr) {
            time_req += *itr;
        }

        itr++;
        Request temp;
        temp.timeOfRequest = stoi(time_req);
        std::string command(itr, str.end());
        temp.command = command;
        client.push_back(temp);
    }
    for (int i = 0; i < num_clients; i++) {
        client[i].i = i;
        pthread_mutex_init(&(client[i].mutex), NULL);
    }
    for (int i = 0; i < num_clients; i++) {
        thread_details *temp = new thread_details;
        temp->id = i;
        pthread_create(&client[i].tid, NULL, clientThread, (void *)temp);
    }

    for (int i = 0; i < num_clients; i++) {
        pthread_join(client[i].tid, NULL);
    }

    return 0;
}