#ifndef UTILS_H
#define UTILS_H

#include "header.h"

#ifndef DEBUG
#define DEBUG 0
#endif

// Defining colours
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BRIGHT_GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define BRIGHT_BLUE "\x1b[94m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

// Some macros
#define PthreadCreate(newthread, attr, funcPtr, arg)          \
    if (pthread_create(newthread, attr, funcPtr, arg) != 0) { \
        perror("pthread_create() error");                     \
        exit(1);                                              \
    }

#define PthreadCondInit(cond, attr)            \
    if (pthread_cond_init(&cond, NULL) != 0) { \
        perror("pthread_cond_init() error");   \
        exit(1);                               \
    }

#define after_malloc_check(x)                  \
    if (x == NULL) {                           \
        perror("Failed to allocate memory\n"); \
        exit(EXIT_FAILURE);                    \
    }

#endif