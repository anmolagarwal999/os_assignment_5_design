#ifndef HEADER_H
#define HEADER_H

#include <arpa/inet.h>
#include <assert.h>
#include <bits/stdc++.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <queue>
#include <tuple>
#include <vector>

#ifndef DEBUG
#define DEBUG 0
#endif

#endif