#include "utils.h"

void printOrdinalNumber(unsigned n) {
    if (n % 10 == 1 && n % 100 != 11)
        printf("st");
    else if (n % 10 == 2 && n % 100 != 12)
        printf("nd");
    else if (n % 10 == 3 && n % 100 != 13)
        printf("rd");
    else
        printf("th");
}