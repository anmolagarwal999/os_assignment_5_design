#include "header.h"
#include "input.h"
#include "utils.h"

pthread_t *course_thread = NULL, *stu_thread = NULL, *lab_thread = NULL;
// pthread_t course_thread[10000];
// pthread_t stu_thread[10000];
// pthread_t lab_thread[10000];

Course* course = NULL;
Student* student = NULL;
Lab* lab = NULL;
unsigned num_students, num_labs, num_courses;
// pthread_mutex_t *studentLock, *courseLock, *labLock;
pthread_mutex_t numLock;

int main() {
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

#if DEBUG >= 1
    printf("num_students = %d\nnum_labs = %d\nnum_courses = %d\n", num_students, num_labs, num_courses);
#endif

    course = (Course*)malloc(sizeof(Course) * num_courses);
    after_malloc_check(course);
    student = (Student*)malloc(sizeof(Student) * num_students);
    after_malloc_check(student);
    lab = (Lab*)malloc(sizeof(Lab) * num_labs);
    after_malloc_check(lab);

    // Taking input
    input();
    // inputCourses();
    // inputStudents();
    // inputLabs();

    // Allocating space to the thread arrays
    course_thread = (pthread_t*)malloc(sizeof(pthread_t) * num_courses);
    after_malloc_check(course_thread);
    stu_thread = (pthread_t*)malloc(sizeof(pthread_t) * (num_students + 1));
    after_malloc_check(stu_thread);
    lab_thread = (pthread_t*)malloc(sizeof(pthread_t) * num_labs);
    // after_malloc_check(lab_thread);

    // Allocating space to lock arrays
    // courseLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_courses);
    // after_malloc_check(courseLock);
    // studentLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_students);
    // after_malloc_check(studentLock);
    // labLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_labs);
    // after_malloc_check(labLock);

    // for (unsigned i = 0; i < num_courses; ++i) {
    //     pthread_mutex_init(&courseLock[i], NULL);
    // }
    // for (unsigned i = 0; i < num_students; ++i) {
    //     pthread_mutex_init(&studentLock[i], NULL);
    // }
    // for (unsigned i = 0; i < num_labs; ++i) {
    //     pthread_mutex_init(&labLock[i], NULL);
    // }
    pthread_mutex_init(&numLock, NULL);

    for (unsigned i = 0; i < num_courses; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&course_thread[i], NULL, courseFunc, (void*)temp);
        // pthread_create(&course_thread[i], NULL, courseFunc, &i);
    }
    for (unsigned i = 0; i < num_students; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&(stu_thread[i]), NULL, studentFunc, (void*)temp);
        // pthread_create(&stu_thread[i], NULL, studentFunc, &i);
    }
    for (unsigned i = 0; i < num_labs; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&lab_thread[i], NULL, labFunc, (void*)temp);
        // pthread_create(&lab_thread[i], NULL, labFunc, &i);
    }

    // // Waiting for the threads to finish execution
    for (unsigned i = 0; i < num_labs; ++i) {
        pthread_join(lab_thread[i], NULL);
    }
    for (unsigned i = 0; i < num_students; ++i) {
        pthread_join(stu_thread[i], NULL);
    }
    for (unsigned i = 0; i < num_courses; ++i) {
        pthread_join(course_thread[i], NULL);
    }

    // Delete locks
    // free(labLock);
    // free(courseLock);
    // free(studentLock);

    // Delete threads
    free(lab_thread);
    free(stu_thread);
    free(course_thread);

    // Deleting labs
    for (unsigned i = 0; i < num_labs; ++i) {
        free(lab[i].ta);
    }
    free(lab);

    // Deleting students
    free(student);

    // Deleting courses
    for (unsigned i = 0; i < num_courses; ++i) {
        free(course[i].L);
    }
    free(course);
    return 0;
}