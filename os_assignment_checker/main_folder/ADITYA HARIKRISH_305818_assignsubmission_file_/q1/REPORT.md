Each course, student and lab has a corresponding thread.

Each course thread does the following:

1. Iterate through the list of possible labs that TAs can be chosen from.
2. For each such lab, iterate through its list of TAs till we encounter a free TA.
3. This TA randomly generates a class size.
4. Iterate through all the students and find the ones who are free and whose top priority is this course. Add them to the tutorial until the class size is full or all the students have been iterated through.
5. Start the class and change the current preference of the students attending.
6. After the class is done, increment the number of classes taught by this TA and move on to the next TA.
7. If all TAs that were eligible to take classes have fully filled up their quota of teaching, then this course exits the simulation.

Each student thread does the following:

1. Fill the form after `T` seconds.
2. After attending a tutorial, calculate the probability of the student choosing the course.
3. According to this probability, the student either chooses the course and is allotted a seat, or withdraws from a course and updates the current preference (in which case if all 3 preferences are not chosen from, they quit the simulation).