#ifndef INPUT_H
#define INPUT_H

#include "header.h"
#include "utils.h"

// Master function
void input();

void inputCourses();
void inputStudents();
void inputLabs();

#endif