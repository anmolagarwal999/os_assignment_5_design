#ifndef UTILS_H
#define UTILS_H

#include "header.h"

#ifndef DEBUG
#define DEBUG 0
#endif

typedef struct Course {
    // CourseID = index in the original course arary (assumption)
    unsigned CourseID;

    char name[100];             // name of the course
    double interest;            // between 0 and 1
    unsigned maxNumberOfSlots;  // max number of slots which can be allotted by a TA, aka course_max_slot

    unsigned numberOfLabs;  // number of labs from which TAs can be chosen (i.e. size of L)
    unsigned *L;            // Lab IDs of the set of labs to choose TAs from

    pthread_mutex_t lock;

    // currentTA;

} Course;

typedef struct Student {
    // StudentID = index in the original student array
    unsigned StudentID;

    double calibre;

    unsigned preference[3];       // CourseIDs of preferences. preference[0] > preference[1] > preference[2], i.e., 0 is most preferred
    unsigned *currentPreference;  // not yet chosen

    uint64_t timeOfFilling;  // time of filling preferences, in seconds
    bool hasFilledForm;
    bool currentlyOccupied;

    pthread_mutex_t lock;
} Student;

typedef struct TA {
    bool currentlyOccupied;
    unsigned numSessionsTaken;

    // pthread_mutex_t lock;
} TA;

typedef struct Lab {
    // LabID = index in the original lab array
    unsigned LabID;

    char name[100];              // name of the lab
    unsigned numberOfTAs;        // number of TAs in the lab
    unsigned maxNumTACanMentor;  // max number of times a member of the lab can TA a course

    TA *ta;  // list of TAs in this lab
    pthread_mutex_t lock;
} Lab;

typedef struct thread_details {
    unsigned id;
} thread_details;

// arrays of threads
extern pthread_t *course_thread;
extern pthread_t *stu_thread;
extern pthread_t *lab_thread;

// extern pthread_t course_thread[10000];
// extern pthread_t stu_thread[10000];
// extern pthread_t lab_thread[10000];

extern unsigned num_students, num_labs, num_courses;

// arrays of Course, Student, Lab
extern Course *course;    // size: num_courses
extern Student *student;  // size: num_students
extern Lab *lab;          // size: num_labs

// Functions for the threads
void *courseFunc(void *arg);
void *studentFunc(void *arg);
void *labFunc(void *arg);

void printOrdinalNumber(unsigned n);  // Prints 'th' or 'nd' or 'st' depending on n. Eg: if n==1 then st because 1st

// Defining colours
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BRIGHT_GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define BRIGHT_BLUE "\x1b[94m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

// Mutex locks
// extern pthread_mutex_t *studentLock, *courseLock, *labLock;
// extern pthread_mutex_t *studentLock, *courseLock, *labLock;

extern pthread_mutex_t numLock;  // for num_courses, num_labs and num_students

// Some macros
#define PthreadCreate(newthread, attr, funcPtr, arg)          \
    if (pthread_create(newthread, attr, funcPtr, arg) != 0) { \
        perror("pthread_create() error");                     \
        exit(1);                                              \
    }

#define PthreadCondInit(cond, attr)            \
    if (pthread_cond_init(&cond, NULL) != 0) { \
        perror("pthread_cond_init() error");   \
        exit(1);                               \
    }

#define after_malloc_check(x)                  \
    if (x == NULL) {                           \
        printf("Failed to allocate memory\n"); \
        exit(EXIT_FAILURE);                    \
    }

#endif