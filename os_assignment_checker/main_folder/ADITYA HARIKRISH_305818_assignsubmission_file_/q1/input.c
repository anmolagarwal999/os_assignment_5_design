#include "input.h"

void input() {
    inputCourses();
    inputStudents();
    inputLabs();
}

void inputCourses() {
    for (unsigned i = 0; i < num_courses; ++i) {
        course[i].CourseID = i;
        pthread_mutex_init(&(course[i].lock), NULL);

        scanf("%s %lf %u %u", course[i].name, &(course[i].interest), &(course[i].maxNumberOfSlots), &(course[i].numberOfLabs));

        assert(course[i].interest >= 0 && course[i].interest <= 1);
        assert(course[i].maxNumberOfSlots > 0);
        assert(course[i].numberOfLabs > 0);

        course[i].L = (unsigned*)malloc(sizeof(int) * course[i].numberOfLabs);

        for (unsigned j = 0; j < course[i].numberOfLabs; ++j) {
            scanf("%u", &(course[i].L[j]));
        }
    }

#if DEBUG == 2
    printf(GREEN "**COURSES**\n" RESET);
    for (unsigned i = 0; i < num_courses; ++i) {
        printf("Name: %s\n", course[i].name);
        printf("Interest: %lf\n", course[i].interest);
        printf("maxNumberOfSlots: %u\n", course[i].maxNumberOfSlots);
        printf("numberOfLabs: %u\n", course[i].numberOfLabs);
        printf("L: ");
        for (unsigned j = 0; j < course[i].numberOfLabs; ++j) {
            printf("%u ", course[i].L[j]);
        }
        printf("\n\n");
    }
#endif
}

void inputStudents() {
    for (unsigned i = 0; i < num_students; ++i) {
        student[i].currentPreference = &(student[i].preference[0]);
        student[i].StudentID = i;
        student[i].hasFilledForm = false;
        student[i].currentlyOccupied = false;

        pthread_mutex_init(&(student[i].lock), NULL);

        scanf("%lf", &(student[i].calibre));
        for (unsigned j = 0; j < 3; ++j) {
            scanf("%u", &(student[i].preference[j]));
        }
        scanf("%" PRIu64, &(student[i].timeOfFilling));
    }

#if DEBUG == 2
    printf(GREEN "**STUDENTS**\n" RESET);
    for (unsigned i = 0; i < num_students; ++i) {
        printf("calibre: %lf\n", student[i].calibre);
        printf("Preferences: ");
        for (unsigned j = 0; j < 3; ++j)
            printf("%u ", student[i].preference[j]);
        printf("\ntimeOfFilling: %" PRIu64 "\n\n", student[i].timeOfFilling);
    }
#endif
}

void inputLabs() {
    for (unsigned i = 0; i < num_labs; ++i) {
        lab[i].LabID = i;
        pthread_mutex_init(&(lab[i].lock), NULL);

        scanf("%s %u %u",
              lab[i].name,
              &(lab[i].numberOfTAs),
              &(lab[i].maxNumTACanMentor));

        lab[i].ta = (TA*)malloc(sizeof(TA) * lab[i].numberOfTAs);
        after_malloc_check(lab[i].ta);

        for (unsigned j = 0; j < lab[i].numberOfTAs; ++j) {
            lab[i].ta[j].currentlyOccupied = false;
            lab[i].ta[j].numSessionsTaken = 0;
        }
    }
#if DEBUG == 2
    printf(GREEN "**LABS**\n" RESET);
    for (unsigned i = 0; i < num_labs; ++i) {
        printf("name: %s\n", lab[i].name);
        printf("numberOfTAs: %u\n", lab[i].numberOfTAs);
        printf("maxNumTACanMentor: %u\n\n", lab[i].maxNumTACanMentor);
    }
#endif
}
