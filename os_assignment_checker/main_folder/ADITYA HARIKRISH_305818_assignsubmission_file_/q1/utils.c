#include "utils.h"

#include "vector.h"

void *courseFunc(void *arg) {
    unsigned courseID = ((thread_details *)arg)->id;
#if DEBUG >= 1
    printf(CYAN "CourseID in courseFunc: %u\n" RESET, courseID);
#endif
    assert(courseID < num_courses);
    assert(course != NULL);

    // iterate through L
    pthread_mutex_lock(&(course[courseID].lock));

    for (unsigned i = 0; i < course[courseID].numberOfLabs; ++i) {
        unsigned labID = course[courseID].L[i];

        // iterate through the available TAs to find a TA who's free
        pthread_mutex_lock(&(lab[labID].lock));

        for (unsigned j = 0; j < lab[labID].numberOfTAs; ++j) {
            if (!(lab[labID].ta[j].currentlyOccupied)) {
                lab[labID].ta[j].currentlyOccupied = true;
                vector studentsInClass;
                init_vector(&studentsInClass);

                printf(BRIGHT_BLUE
                       "TA %u from lab %s "
                       "has been allocated to course %s for his %u" RESET,
                       j, lab[labID].name,
                       course[courseID].name,
                       lab[labID].ta[j].numSessionsTaken);

                printOrdinalNumber(lab[labID].ta[j].numSessionsTaken);

                printf(BRIGHT_BLUE " TA ship\n" RESET);

                unsigned slotSize = ((unsigned)rand()) % course[courseID].maxNumberOfSlots + 1;  // number between 0 and max num of slots that the course permits

                unsigned slotsFilled = 0;
                for (unsigned k = 0;
                     k < num_students && slotsFilled <= slotSize;
                     ++k) {
                    if (pthread_mutex_trylock(&(student[k].lock)) != 0) {
                        continue;
                    }
                    if (student[k].hasFilledForm == false || student[k].currentlyOccupied == true || (student[k].currentPreference - student[k].preference > 3)) {
                        pthread_mutex_unlock(&(student[k].lock));
                        continue;
                    }

                    if (*(student[k].currentPreference) == courseID) {
                        pushback(&studentsInClass, student[k].StudentID);
                        printf(MAGENTA
                               "Student %u "
                               "has been allocated a seat in course %s\n" RESET,
                               student[k].StudentID,
                               course[courseID].name);
                        student[k].currentlyOccupied = true;
                        (student[k].currentPreference)++;
                    }

                    pthread_mutex_unlock(&(student[k].lock));
                }

                (lab[labID].ta[j].numSessionsTaken)++;
                lab[labID].ta[j].currentlyOccupied = false;

                delete_vector(&studentsInClass);
            }
        }

        pthread_mutex_unlock(&(lab[labID].lock));
    }

    pthread_mutex_unlock(&(course[courseID].lock));
    return NULL;
}

void *studentFunc(void *arg) {
    unsigned studentID = ((thread_details *)arg)->id;

#if DEBUG >= 1
    printf(CYAN "StudentID in studentFunc: %u\n" RESET, studentID);
#endif
    assert(studentID < num_students);

    pthread_mutex_lock(&(student[studentID].lock));

    // student fills form after waiting for t seconds
    sleep((unsigned)(student[studentID].timeOfFilling));
    student[studentID].hasFilledForm = true;

    printf("Student %u has filled in preferences for course registration\n", studentID);
    pthread_mutex_unlock(&(student[studentID].lock));

    return NULL;
}

void *labFunc(void *arg) {
    unsigned labID = ((thread_details *)arg)->id;
    assert(labID < num_labs);
    return NULL;
}

void printOrdinalNumber(unsigned n) {
    if (n % 10 == 1 && n % 100 != 11)
        printf(BRIGHT_BLUE "st" RESET);
    else if (n % 10 == 2 && n % 100 != 12)
        printf(BRIGHT_BLUE "nd" RESET);
    else if (n % 10 == 3 && n % 100 != 13)
        printf(BRIGHT_BLUE "rd" RESET);
    else
        printf(BRIGHT_BLUE "th" RESET);
}
