#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <bits/stdc++.h>
#include <queue>
using namespace std;
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
#define pb push_back
const LL MOD = 1000000007;
#define ll long long
#define PI 3.14159265
#define br printf("\n")
#define fo(i, n) for (int i = 0; i < n; i++)
#define MAX_LEN 101
#define MAX_KEY 100
#define MAX_CLIENTS 1000
#define PORT_ARG 8001
#define max_number_args 100
#define max_size_args 1000
const int initial_msg_len = 256;
const LL buff_sz = 1048576;
int num_threads_pool;
queue<int *> client_queue;
pthread_mutex_t client_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t client_queue_cond = PTHREAD_COND_INITIALIZER;
string map_array[MAX_LEN] = {""};
pthread_mutex_t map_mutex[101] = {PTHREAD_MUTEX_INITIALIZER};
// pthread_cond_t map_array_cond[101] = {PTHREAD_COND_INITIALIZER};

void *handle_connection(void *ptr_client_socket_fd, int index);
void *thread_function(void *arg);
pair<string, int> read_string_from_socket(const int &fd, int bytes);
int send_string_on_socket(int fd, const string &s);
int tokenizer2(string str, vector<string> &tokens);

int insert_func(vector<string> tokens);
int delete_func(vector<string> tokens);
int update_func(vector<string> tokens);
int concat_func(vector<string> tokens);
int fetch_func(vector<string> tokens);

int main(int argc, char *argv[])
{
    fo(i, MAX_LEN)
    {
        map_mutex[i] = PTHREAD_MUTEX_INITIALIZER;
    }
    num_threads_pool = atoi(argv[1]);
    pthread_t thread_pool[num_threads_pool];
    int array_index[num_threads_pool];
    for (int i = 0; i < num_threads_pool; i++)
    {
        array_index[i] = i;
        pthread_create(&thread_pool[i], NULL, thread_function, &array_index[i]);
    }

    int welcome_socket_fd, final_socket_fd, port_number;
    struct sockaddr_in serv_addr_obj, client_addr_obj;

    welcome_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (welcome_socket_fd < 0)
    {
        perror("ERROR creating welcoming socket");
        exit(-1);
    }
    bzero((char *)&serv_addr_obj, sizeof(serv_addr_obj));
    port_number = PORT_ARG;
    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(port_number);
    socklen_t server_size = sizeof(serv_addr_obj);
    if (bind(welcome_socket_fd, (struct sockaddr *)&serv_addr_obj, server_size) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }
    listen(welcome_socket_fd, MAX_CLIENTS);
    cout << "Server has started listening on the LISTEN PORT" << endl;

    while (true)
    {
        cout << "Waiting for connection requests\n";
        socklen_t client_size = sizeof(client_addr_obj);
        final_socket_fd = accept(welcome_socket_fd, (struct sockaddr *)&client_addr_obj, &client_size);
        if (final_socket_fd < 0)
        {
            perror("ERROR while accept() system call occurred in SERVER");
            exit(-1);
        }
        printf(BGRN "New client connected from port number %d and IP %s \n" ANSI_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr));

        int *pointer = (int *)malloc(sizeof(int));
        *pointer = final_socket_fd;
        pthread_mutex_lock(&client_queue_mutex);
        client_queue.push(pointer);
        pthread_cond_signal(&client_queue_cond);
        pthread_mutex_unlock(&client_queue_mutex);
    }

    close(welcome_socket_fd);
    return 0;
}

void *handle_connection(void *ptr_client_socket_fd, int index)
{
    int client_socket_fd = *((int *)ptr_client_socket_fd);
    //free(ptr_client_socket_fd);
    int recieved_num;
    int ret_val = -1;
    string cmd;
    tie(cmd, recieved_num) = read_string_from_socket(client_socket_fd, buff_sz);
    ret_val = recieved_num;
    if (ret_val <= 0)
    {
        cout << "Server could not read msg sent from client\n";
    }
    else
    {
        vector<string> tokens;
        int num_tokens = tokenizer2(cmd, tokens);
        // cout << num_tokens << endl;
        // fo(i, num_tokens)
        // {
        //     cout << tokens[i];
        //     br;
        // }
        string msg_to_send_back;
        if (tokens[0] == "insert" && num_tokens == 3)
        {
            int ret_val = insert_func(tokens);
            if (ret_val == -1)
            {
                msg_to_send_back = "Key already exists";
            }
            else if (ret_val == -2)
            {
                msg_to_send_back = "Invalid Key";
            }
            else
            {
                msg_to_send_back = "Insertion Successfull";
            }
        }
        else if (tokens[0] == "delete" && num_tokens == 2)
        {
            int ret_val = delete_func(tokens);
            if (ret_val == -1)
            {
                msg_to_send_back = "No such Key exists";
            }
            else if (ret_val == -2)
            {
                msg_to_send_back = "Invalid Key";
            }
            else
            {
                msg_to_send_back = "Deletion Successfull";
            }
        }
        else if (tokens[0] == "update" && num_tokens == 3)
        {
            int ret_val = update_func(tokens);
            if (ret_val == -1)
            {
                msg_to_send_back = "Key does not exist";
            }
            else if (ret_val == -2)
            {
                msg_to_send_back = "Invalid Key";
            }
            else
            {
                msg_to_send_back = tokens[2];
            }
        }
        else if (tokens[0] == "concat" && num_tokens == 3)
        {
            int ret_val = concat_func(tokens);
            if (ret_val == -1)
            {
                msg_to_send_back = "Concat failed as atleast one of the keys does not exist";
            }
            else if (ret_val == -2)
            {
                msg_to_send_back = "Invalid Key";
            }
            else
            {
                int key = stoi(tokens[2]);
                msg_to_send_back = map_array[key];
            }
        }
        else if (tokens[0] == "fetch" && num_tokens == 2)
        {
            int ret_val = fetch_func(tokens);
            if (ret_val == -1)
            {
                msg_to_send_back = "Key does not exist";
            }
            else if (ret_val == -2)
            {
                msg_to_send_back = "Invalid Key";
            }
            else
            {
                int key = stoi(tokens[1]);
                msg_to_send_back = map_array[key];
            }
        }
        else
        {
            msg_to_send_back = "Invalid Command";
        }
        msg_to_send_back = to_string(index) + ":" + msg_to_send_back;
        //sleep(2);
        int sent_to_client = send_string_on_socket(client_socket_fd, msg_to_send_back);
        if (sent_to_client == -1)
        {
            perror("Error while writing to client. Seems socket has been closed");
        }
    }
    close(client_socket_fd);
    printf(BRED "Disconnected from client" ANSI_RESET "\n");
    return NULL;
}

void *thread_function(void *arg)
{
    int *index = (int *)arg;
    while (true)
    {
        int *ptr_client_socket_fd;
        pthread_mutex_lock(&client_queue_mutex);
        if (client_queue.empty())
        {
            pthread_cond_wait(&client_queue_cond, &client_queue_mutex);
        }
        ptr_client_socket_fd = client_queue.front();
        client_queue.pop();
        pthread_mutex_unlock(&client_queue_mutex);
        if (ptr_client_socket_fd != NULL)
        {
            handle_connection(ptr_client_socket_fd, *index);
        }
    }
}

pair<string, int> read_string_from_socket(const int &fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. \n";
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA via socket.\n";
    }
    return bytes_sent;
}

int insert_func(vector<string> tokens)
{
    int key = stoi(tokens[1]);
    string value = tokens[2];
    // cout << "Key: " << key << " Value: " << value << endl;
    if (key < 0 || key > MAX_KEY)
    {
        return -2;
    }
    if (map_array[key] != "")
    {
        return -1;
    }
    pthread_mutex_lock(&map_mutex[key]);
    map_array[key] = value;
    pthread_mutex_unlock(&map_mutex[key]);
    return 0;
}

int delete_func(vector<string> tokens)
{
    int key = stoi(tokens[1]);
    if (key < 0 || key > MAX_KEY)
    {
        return -2;
    }
    if (map_array[key] == "")
    {
        return -1;
    }
    pthread_mutex_lock(&map_mutex[key]);
    map_array[key] = "";
    pthread_mutex_unlock(&map_mutex[key]);
    return 0;
}

int update_func(vector<string> tokens)
{
    int key = stoi(tokens[1]);
    string value = tokens[2];
    if (key < 0 || key > MAX_KEY)
    {
        return -2;
    }
    if (map_array[key] == "")
    {
        return -1;
    }
    pthread_mutex_lock(&map_mutex[key]);
    map_array[key] = value;
    pthread_mutex_unlock(&map_mutex[key]);
    return 0;
}

int concat_func(vector<string> tokens)
{
    int key = stoi(tokens[1]);
    int key2 = stoi(tokens[2]);
    if (key < 0 || key > MAX_KEY)
    {
        return -2;
    }
    if (key2 < 0 || key2 > MAX_KEY)
    {
        return -2;
    }
    if (map_array[key] == "" || map_array[key2] == "")
    {
        return -1;
    }
    pthread_mutex_lock(&map_mutex[key]);
    pthread_mutex_lock(&map_mutex[key2]);
    string temp = map_array[key];
    map_array[key] += map_array[key2];
    map_array[key2] += temp;
    pthread_mutex_unlock(&map_mutex[key2]);
    pthread_mutex_unlock(&map_mutex[key]);
    return 0;
}

int fetch_func(vector<string> tokens)
{
    int key = stoi(tokens[1]);
    if (key < 0 || key > MAX_KEY)
    {
        return -2;
    }
    if (map_array[key] == "")
    {
        return -1;
    }
    return 0;
}

int tokenizer2(string str, vector<string> &tokens)
{
    stringstream check1(str);
    string intermediate;
    while (getline(check1, intermediate, ' '))
    {
        tokens.push_back(intermediate);
    }
    return tokens.size();
}