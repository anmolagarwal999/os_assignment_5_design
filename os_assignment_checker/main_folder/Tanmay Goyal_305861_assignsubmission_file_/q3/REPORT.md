# Q3 Report

## How to execute

- Server side
  - First compile using the command: g++ q3server.cpp -o server -lpthread
  - Now execute using: ./server 'n' where n is any arbitrary number of threads to be used
- Client side
  - First compile using the command: g++ q3client.cpp -o client -lpthread
  - Now execute using: ./client

## Client side logic

- For every query, a new thread gets created.
- The thread function gets called which gets the client socket file descriptor using the get_socket_fd function (which connects the client socket to server socket (which is bounded to a port on the server side)).
- The get_scoket_fd function creates a valid socket for the client and initiates the connection.
- Every thread sleeps for a certain amount of time depending on the amount of time after which the wuery needs to be adressed (given in input).
- On establishing connection to the server side, the thread from client side sends the operation in the form of a string to the server which is taken up by one of the worker threads on the server side. The client thread then waits for the server to execute the operation and return the output.
- The client thread prints the output after recieving it from the server and the client thread returns null. the client program waits for all threads to return null using the pthread join command after which the program stops.

## Server side logic

- I have created thread pools which take queries one by one from a queue untill the queue is empty after which it waits for any new query to arrive in the queue.
- The thread function of these pool threads does nothing but removes query from the queue and hands it to the handle connection function. To avoid busy waiting, we use a conditional variable here as the cpu shouldnt keep looking for queries in queue when the queue is empty.
- The main function binds welcome socket to port and listens for active connections. If it finds a valid client, it establishes a connection and pushes pointer to the new created socket file descriptor on the queue. Also, it singals to wait condition in thread function after every element is pushed in the queue. Otherwise, the program will get stuck as it wont no when the queue is no longer empty.
- The handle connection function tokenizes the input string and decides which operation to perform out of the following:
  - insert <key> <value> : This command is supposed to create a new “key” on the server’s dictionary and set its value as <value>. Also, in case the “key” already exists on the server, then an appropriate error stating “Key already exists” should be displayed. If successful, “Insertion successful” should be displayed.
  - update <key> <value> : This command is supposed to update the value corresponding to <key> on the server’s dictionary and set its value as <value>. Also, in case the “key” does not exist on the server, then an appropriate error stating “Key does not exist” should be displayed. If successful, the updated value of the key should be displayed.
  - concat <key1> <key2> : Let values corresponding to the keys before execution of this command be {key1: value_1, key_2:value_2}. Then, the corresponding values after this command's execution should be {key1: value_1+value_2, key_2: value_2+value_1}. If either of the keys do not exist in the dictionary, an error message “Concat failed as at least one of the keys does not exist” should be displayed. Else, the final value of key_2 should be displayed. No input will be provided where <key_1> = <key_2>.
  - delete <key> : This command is supposed to remove the <key> from the dictionary. If no key with the name <key> exists, then an error stating “No such key exists” should be displayed. If successful, display the message “Deletion successful”.
  - fetch <key> : must display the value corresponding to the key if it exists at the connected server, and an error “Key does not exist” otherwise.
- These operations are performed on an array of strings where the index represents the key. A null value represents that there is no value stored at that key currently.
