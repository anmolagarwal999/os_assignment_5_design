#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <assert.h>
#include <tuple>
#include <vector>
using namespace std;
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
#define pb push_back
#define MAX_CLIENTS 1000
#define PORT_ARG 8001
const int initial_msg_len = 256;
const LL buff_sz = 1048576;
#define ll long long
#define PI 3.14159265
#define br printf("\n")
#define fo(i, n) for (int i = 0; i < n; i++)

struct queries
{
    int time;
    int id;
    string query;
};

typedef struct queries queries;

int get_socket_fd(struct sockaddr_in *ptr);
void *init_request(void *arg);
int send_string_on_socket(int fd, const string &s);
pair<string, int> read_string_from_socket(int fd, int bytes);
pthread_mutex_t lock;

queries request_list[MAX_CLIENTS];
int main()
{
    int total_requests;
    scanf("%d", &total_requests);
    fo(i, total_requests)
    {
        int time;
        scanf("%d", &time);
        string str;
        getline(cin, str);
        request_list[i].time = time;
        request_list[i].query = str;
        request_list[i].id = i;
    }
    pthread_t threads[total_requests]; // array of thread IDs
    fo(i, total_requests)
    {
        pthread_create(&threads[i], NULL, init_request, &request_list[i]);
    }
    fo(i, total_requests)
    {
        pthread_join(threads[i], NULL);
    }
    return 0;
}

int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;
    int client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = PORT_ARG;
    memset(&server_obj, 0, sizeof(server_obj));
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num);
    if (connect(client_socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }
    return client_socket_fd;
}

void *init_request(void *arg)
{
    struct sockaddr_in server_obj;
    int socket_fd = get_socket_fd(&server_obj);
    queries *q = (queries *)arg;
    int time = q->time;
    string str = q->query.substr(1, q->query.length() - 1);
    sleep(time);
    pthread_mutex_lock(&lock);
    send_string_on_socket(socket_fd, str);
    pthread_mutex_unlock(&lock);
    int num_bytes_read;
    string output_msg;
    pthread_mutex_lock(&lock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << to_string(q->id) <<":" << output_msg << endl;
    pthread_mutex_unlock(&lock);
    return NULL;
}

int send_string_on_socket(int fd, const string &s)
{
    int bytes_sent = write(fd, s.c_str(), s.length());
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
        exit(-1);
    }
    return bytes_sent;
}

pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);
    int bytes_received = read(fd, &output[0], bytes - 1);
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);
    return {output, bytes_received};
}
