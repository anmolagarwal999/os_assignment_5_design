# Q1 Report

## How to execute

- First compile using the command: gcc q1.c -o q1 -lpthread
- Now execute using: ./q1

## Structs

- A struct for each course storing all the details.
- A struct for all the students.
- A struct for storing information of all the labs.
- A struct for storing all the lab TAs.

## Threads

- Multiple threads for simulating all the courses and its tutorials.
- Threads for each student taking up the courses.

## Course thread

- If the course has not yet been deleted, we enter the while loop to assign a TA. 
- If a TA is accessible, that is, if none of the TAs are busy or tired, we perform a tutorial for the course after waiting 1 second for the student threads to register for the session. If everyone in the lab has reached the limit on how many times they can accept a TAship, the lab will no longer send out TAs.
- For the length of the tutorial, the course thread sleeps for 3 seconds.
- When the tutorial is over, a signal is broadcast to wake up all the threads participating in this tutorial that were waiting on a conditional variable. 
- When all of the TAs have been used, the course is withdrawn and the main while loop is exited.

## Student thread

- The student thread is dormant until registration time.
- If the present course option is not changed, the student must wait for a tutorial to take place.
- The student then gets a seat from the alloted slots in the tutorial.
- When the student attends the tutorial, conditional wait is used to remove busy waiting. The student then wakes up on getting a signal from the respective course.
- The student is then assigned a seat in the tutorial from the available slots. Conditional wait is used to eliminate busy waiting when the student attends the tutorial.
- When the student receives a signal from the appropriate course, he or she awakens.
- If the student's likelihood is larger than a randomly generated float between 0 and 1, he or she picks a course permanently. If a course is discontinued or the probability is reduced, the student does not receive it.
- The student then exits the simulation.
