# Q2 Report

## How to execute

- First compile using the command: gcc q2.c -o q2 -lpthread
- Now execute using: ./q2

## Structs

- I have created a struct for every zone having the currently filled seats number and the total capacity.
- There is a 'people' struct for every spectator and a group struct because every spectator belongs to a group.
- There is a chace struct storing the data of every goal scoring chance.

## Threads

- There is a single thread evaluating all goal scoring chances because they happen sequentially and not synchronously.
- Every spectator has a thread of his own which simulates allocation of seat, departure of spectator, etc.

## Event wise logic

- Event 1: Person has reached the stadium
  - Basically every thread will sleep for a certain amount of time given in input because every spectator reaches the stadium after this given time.
- Event 2: Person has got a seat in a zone
  - First the program checks if there is a seat avaiable for the spectator depending on his type. If there is no seat currently available in valid zones, then the conditional variable 'check' makes it wait untill the time a person leaves the stadium after which it checks for availability of seats again. If still no seat, then it waits again unless a person leaves the stadium. This repeats. However busy waiting does not happen, as it is conditioned to check only after someone leaves the stadium.
- Event 3: Person couldn’t get a seat
  - After the above conditional variable gets the signal, everytime a while condition (that of do while) checks if the patience time of the Person has been crossed or not. If the patience time has exceeded, then the do while loop gets exited and seat is not allocated.
- Event 4: Person watched the match for 'X' time and is leaving
  - The exit of every spectator is simulated using conditional timed wait. Again a do while loop is factored inside of which this conditional timed wait is implemented to avoid busy waiting. So, every specatator who has got a seat goes in this loop and gets his enraged value checked. If the number of goals by opposite side have exceeded the enraged value of the spectator, then, the spectator exits the loop. Everytime a goal is scored by any side, the conditional wait is signalled, and the event 5 (person leaving due to bad defensive performance) is checked. Since this is a conditional timed wait, incase the current time exceeds the spectating time of the spectator, then also the wait condition exits and I break from the do while loop, while printing the appropriate event.
- Event 5: Person is leaving due to the bad performance of his team
  - Its covered in the above point (event 4)
- Event 6: Team converted the goal scoring chance to a goal
  - Sleeping times are kept to simulate goals (since they happen after some time in the match).
  - Every goal probability is checked with a random double number from 0 to 1 and this decides whether the goal has happened or has the goal been missed.
- Event 7: Team failed to convert the goal scoring chance to a goal
  - Logic is same as event 6.
