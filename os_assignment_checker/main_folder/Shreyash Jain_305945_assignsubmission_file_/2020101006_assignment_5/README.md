# OSN Assignment : Concurrency, Networking and Parallelism

Solutions to Operating Systems and Networking Assignment

## Execution instructions

### Question 1

- Compile using `gcc q1.c -lpthread -o q1`
- then run using `./q1`

### Question 2

- Compile using `gcc q2.c -lpthread -o q2`
- then run using `./q2`

### Question 3

**Server**

- Compile using `g++ server.cpp -lpthread -o server`
- then run using `./server <number of working threads>` eg : `./server 7`

**Client**

- Compile using `g++ client.cpp -lpthread -o client`
- then run using `./client`
