#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <string>
#include <semaphore.h>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>
using namespace std;

//colour coding
#define black "\e[1;30m"
#define red "\e[1;31m"
#define green "\e[1;32m"
#define ANSI_RESET "\x1b[0m"
typedef long long LL;
const LL MOD = 1000000007;
#define part cout << "--------" << endl;
#define debug(x) cout << #x << " : " << x << endl

#define SERVER_PORT 8001
typedef struct packet {
    int time;
    string command;
} packet;

const LL buff_sz = 1039999;
int socket_fd;
vector<packet> requests;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


pair<string, int> read_string_from_socket(int fd, int bytes)
{
    std::string output;
    output.resize(bytes);

    int bytes_received = read(fd, &output[0], bytes - 1);
    // debugging
    if (bytes_received <= 0)
    {
        cerr << "Failed to read data from socket. Seems server has closed socket\n";
        exit(-1);
    }
    output[bytes_received] = 0;
    output.resize(bytes_received);

    return {output, bytes_received};
}



int send_string_on_socket(int fd, const string &s)
{
  
    int bytes_sent = write(fd, s.c_str(), s.length());
  
    if (bytes_sent < 0)
    {
        cerr << "Failed to SEND DATA on socket.\n";
  
        exit(-1);
    }

    return bytes_sent;
}
int get_socket_fd(struct sockaddr_in *ptr)
{
    struct sockaddr_in server_obj = *ptr;
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        perror("Error in socket creation for CLIENT");
        exit(-1);
    }
    int port_num = SERVER_PORT;

    memset(&server_obj, 0, sizeof(server_obj)); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(port_num); //convert to big-endian order
// converts ip-address in dots and numbers

    /////////////////////////////////////////////////////////////////////////////////////////
    /* connect to server */

    if (connect(socket_fd, (struct sockaddr *)&server_obj, sizeof(server_obj)) < 0)
    {
        perror("Problem in connecting to the server");
        exit(-1);
    }

    //part;
    // printf(green "Connected to server\n" ANSI_RESET);
    // part;
    return socket_fd;
}

////////////////////////////////////////////////////////

void *begin_process(void *args)
{
    int *thread_idx_addr = (int *)args;
    int thread_idx = *thread_idx_addr;

    // Delay sending request
    sleep(requests[thread_idx].time);
    string cmd = requests[thread_idx].command;

    pthread_mutex_lock(&lock);
    send_string_on_socket(socket_fd, cmd);
    pthread_mutex_unlock(&lock);

    int num_bytes_read;
    string output_msg;

    pthread_mutex_lock(&lock);
    tie(output_msg, num_bytes_read) = read_string_from_socket(socket_fd, buff_sz);
    cout << thread_idx << ":" << gettid() << ":" << output_msg << endl;
    pthread_mutex_unlock(&lock);

    return NULL;
}

////////////////////////////////////////////////////////

int main(int argc, char *argv[]) 
{
    int m, i;
    string m_str;
    getline(cin, m_str);
    m = stoi(m_str);            // Number of clients/requests

    // Get input line by line as a single string and parse to separate the send-time
    for (i = 0; i < m; i++) {
        string input, time_str = "";
        getline(cin, input);
        auto it = input.begin();
        while (*it != ' ') {
            time_str += *it;
            it++;
        }
        it++;
        packet data;
        data.time = stoi(time_str);
        string cmd(it, input.end());
        data.command = cmd;
        requests.push_back(data);
    }

    struct sockaddr_in server_obj;
    socket_fd = get_socket_fd(&server_obj);
    // cout << "Connection to server successful" << endl;

    // Multithreading
    pthread_t clients[m];
    int thread_idx[m];

    for (i = 0; i < m; i++) {
        thread_idx[i] = i;
        pthread_create(&clients[i], NULL, begin_process, (void *)&thread_idx[i]);
    }

    for (i = 0; i < m; i++) {
        pthread_join(clients[i], NULL);
    }

    return 0;
}