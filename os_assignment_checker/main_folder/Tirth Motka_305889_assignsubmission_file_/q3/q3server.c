#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
sem_t x, y;
pthread_t tid;
pthread_t writerthreads[100];
pthread_t readerthreads[100];
int readercount = 0;
void* reader(void* param){
    int i = *(int*)param;
    sem_wait(&y);
    sem_wait(&x);
    printf("Reader %d is reading\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);

}
void* writer(void* param){
    int i = *(int*)param;
    sem_wait(&x);
    sem_wait(&y);
    printf("Writer %d is writing\n", i);
    sem_post(&x);
    sem_post(&y);
    pthread_exit(NULL);
}
int main()
{
    int i, j, k, n, m, portNumber, addr_size, sock, newSocket, *newSocketPtr;
    struct sockaddr_in serverStorage, clientStorage;
    portNumber = atoi("8080");
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        puts("Socket failed");
        exit(1);
    }
    serverStorage.sin_family = AF_INET;
    serverStorage.sin_port = htons(portNumber);
    serverStorage.sin_addr.s_addr = INADDR_ANY;
    memset(serverStorage.sin_zero, '\0', sizeof(serverStorage.sin_zero));
    addr_size = sizeof(struct sockaddr_in);
    if (bind(sock, (struct sockaddr *)&serverStorage, sizeof(serverStorage)) == -1)
    {
        puts("Bind failed");
        exit(1);
    }
    if (listen(sock, 5) == -1)
    {
        puts("Listen failed");
        exit(1);
    }
    while (1)
    {
        newSocket = accept(sock, (struct sockaddr *)&clientStorage, &addr_size);
        if (newSocket == -1)
        {
            puts("Accept failed");
            exit(1);
        }
        newSocketPtr = malloc(sizeof(int));
        *newSocketPtr = newSocket;
        pthread_create(&tid, NULL, reader, newSocketPtr);
        pthread_join(tid, NULL);
        pthread_create(&tid, NULL, writer, newSocketPtr);
        pthread_join(tid, NULL);
    }
    close(sock);
    return 0;
	
}
