
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

void *clienthread(void *args)
{
    int sock = *(int *)args;
    char buffer[1024];
    int read_size;
    while (1)
    {
        read_size = recv(sock, buffer, 1024, 0);
        if (read_size == 0)
        {
            puts("Client disconnected");
            fflush(stdout);
            break;
        }
        else if (read_size == -1)
        {
            perror("recv failed");
            break;
        }
        printf("%s\n", buffer);
        fflush(stdout);
    }
    close(sock);
    return 0;
}
int main(int argc, char *argv[])
{
    int serverSocket, newSocket, portNumber, addr_size, i;
    struct sockaddr_in serverStorage, clientStorage;
    portNumber = atoi(argv[1]);
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
    {
        puts("Socket failed");
        exit(1);
    }
    memset(&serverStorage, 0, sizeof(serverStorage));
    serverStorage.sin_family = AF_INET;
    serverStorage.sin_port = htons(portNumber);
    serverStorage.sin_addr.s_addr = INADDR_ANY;
    if (bind(serverSocket, (struct sockaddr *)&serverStorage, sizeof(serverStorage)) == -1)
    {
        puts("Bind failed");
        exit(1);
    }
    if (listen(serverSocket, 3) == -1)
    {
        puts("Listen failed");
        exit(1);
    }
    addr_size = sizeof(clientStorage);
    while (1)
    {
        newSocket = accept(serverSocket, (struct sockaddr *)&clientStorage, &addr_size);
        if (newSocket == -1)
        {
            puts("Accept failed");
            exit(1);
        }
        puts("Connection accepted");
        pthread_t tid;
        pthread_create(&tid, NULL, clienthread, &newSocket);
        pthread_join(tid, NULL);

    }
    return 0;
    
}