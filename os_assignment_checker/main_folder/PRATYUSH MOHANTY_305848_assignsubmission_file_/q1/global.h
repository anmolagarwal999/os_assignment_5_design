#ifndef _GLOGAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "course.h"
#include "student.h"
#include "lab.h"

#define true 1
#define false 0

typedef int bool;

#endif