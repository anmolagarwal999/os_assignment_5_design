#include "global.h"
#include "student.h"

void assign_course(struct student *s, struct course *c)
{
    int i;
    for (i = 0; i < MAX_COURSES; i++)
    {
        if (s->courses[i] == NULL)
        {
            s->courses[i] = c;
            return;
        }
    }
}
void *student_thread_func(void *arg)
{
    student_ADT *student = (student_ADT *)arg;

    sleep(student->reg_time);
    printf("Student %d has filled in preferences for course registration\n", student->student_ID);

    bool joined_course_ID1 = false;
    bool joined_course_ID2 = false;
    bool joined_course_ID3 = false;

    while (1)
    {
        if (course_list[student->course_ID1].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID1].name);
            course_list[student->course_ID1].course_max_slots--;
            break;
        }
    }
    double prob = course_list[student->course_ID1].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID1 = true;
        assign_course(student, student->course_ID1);
    }
    while (1 && !joined_course_ID1)
    {
        if (course_list[student->course_ID2].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID2].name);
            course_list[student->course_ID2].course_max_slots--;
            break;
        }
    }
    prob = course_list[student->course_ID2].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID2 = true;
        assign_course(student, student->course_ID2);
    }
    while (1 && !joined_course_ID2)
    {
        if (course_list[student->course_ID3].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID3].name);
            course_list[student->course_ID3].course_max_slots--;
            break;
        }
    }
    prob = course_list[student->course_ID3].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID3 = true;
        assign_course(student, student->course_ID3);
    }
    if (!joined_course_ID1 && !joined_course_ID2 && !joined_course_ID3)
    {
        printf("Student %d couldn’t get any of his preferred courses\n", student->student_ID);
    }
}