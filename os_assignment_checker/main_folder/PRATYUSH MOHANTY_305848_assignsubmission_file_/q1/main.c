#include "global.h"
#include "course.h"
#include "student.h"
#include "lab.h"

int main(int argc, char const *argv[])
{
    scanf("%d", &no_of_students);
    scanf("%d", &no_of_labs);
    scanf("%d", &no_of_courses);

    course_list = (course_ADT *)malloc(sizeof(course_ADT) * no_of_courses);
    for (int i = 0; i < no_of_courses; i++)
    {
        course_list[i].course_ID = i;
        scanf("%s", course_list[i].name);
        scanf("%d", &course_list[i].interest_quotient);
        scanf("%d", &course_list[i].course_max_slots);
        scanf("%d", &course_list[i].no_of_labs_by_TAs);

        course_list[i].lab_IDs = (int *)malloc(sizeof(int) * course_list[i].no_of_labs_by_TAs);
        for (int j = 0; j < course_list[i].no_of_labs_by_TAs; j++)
        {
            scanf("%d", &course_list[i].lab_IDs[j]);
        }
    }

    student_list = (student_ADT *)malloc(sizeof(student_ADT) * no_of_students);
    for (int i = 0; i < no_of_students; i++)
    {
        student_list[i].student_ID = i;
        scanf("%d", &student_list[i].calibre_quotient);
        scanf("%d", &student_list[i].course_ID1);
        scanf("%d", &student_list[i].course_ID2);
        scanf("%d", &student_list[i].course_ID3);
        scanf("%d", &student_list[i].reg_time);
    }

    lab_list = (lab_ADT *)malloc(sizeof(lab_ADT) * no_of_labs);
    for (int i = 0; i < no_of_labs; i++)
    {
        lab_list[i].lab_ID = i;
        scanf("%d", &lab_list[i].name);
        scanf("%d", &lab_list[i].no_of_TAs);
        scanf("%d", &lab_list[i].TA_limit);
    }

    pthread_t *student_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_students);
    for (int i = 0; i < no_of_students; i++)
    {
        if (pthread_create(&student_threads[i], NULL, student_thread_func, &student_list[i]) != 0)
        {
            printf("Error creating student_thread\n");
            exit(1);
        }
    }
    pthread_t *course_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_courses);
    for (int i = 0; i < no_of_courses; i++)
    {
        if (pthread_create(&course_threads[i], NULL, course_thread_func, &course_list[i]) != 0)
        {
            printf("Error creating course_thread\n");
            exit(1);
        }
    }
    pthread_t *lab_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_labs);
    for (int i = 0; i < no_of_labs; i++)
    {
        if (pthread_create(&lab_threads[i], NULL, lab_thread_func, &lab_list[i]) != 0)
        {
            printf("Error creating lab_thread\n");
            exit(1);
        }
    }

    for (int i = 0; i < no_of_students; i++)
    {
        pthread_join(student_threads[i], NULL);
    }
    for (int i = 0; i < no_of_courses; i++)
    {
        pthread_join(course_threads[i], NULL);
    }
    for (int i = 0; i < no_of_labs; i++)
    {
        pthread_join(lab_threads[i], NULL);
    }
    return 0;
}
