#ifndef _LAB_H_
#define _LAB_H_

int no_of_labs;
typedef struct
{
    int lab_ID;
    char name[20];
    int no_of_TAs;
    int TA_limit;
} lab_ADT;

lab_ADT *lab_list = NULL;

void *lab_thread_func(void *arg);
#endif