#ifndef _STUDENT_H_
#define _STUDENT_H_

int no_of_students;
typedef struct
{
    int student_ID;
    int calibre_quotient;
    int course_ID1;
    int course_ID2;
    int course_ID3;
    int reg_time;
} student_ADT;

student_ADT *student_list = NULL;

void *student_thread_func(void *arg);
#endif