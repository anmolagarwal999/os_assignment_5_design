#ifndef _COURSE_H_
#define _COURSE_H_

int no_of_courses;
typedef struct
{
    int course_ID;
    char name[20];
    int interest_quotient;
    int course_max_slots;
    int no_of_labs_by_TAs;
    int *lab_IDs;
} course_ADT;

course_ADT *course_list = NULL;

void *course_thread_func(void *arg);
#endif