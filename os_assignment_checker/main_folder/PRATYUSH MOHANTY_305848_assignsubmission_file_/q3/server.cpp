#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <bits/stdc++.h>
using namespace std;

#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_RED "\x1B[31m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_GREEN "\x1B[32m"
#define COLOR_RESET "\x1B[0m"

#define INVALID -1
#define SERVER_PORT 8080
#define SERVER_BACKLOG 100

#define MAX_CMD_LEN 10
#define MAX_VAL_LEN 1000
#define MAX_REP_LEN 1000

queue<int *> client_queue;
vector<pair<int, string>> dictionary;

pthread_mutex_t mutex_lock_dict;
pthread_mutex_t mutex_lock_queue;

typedef struct
{
    int request_id;
    int time;
    char string_cmd[MAX_CMD_LEN];
    int key;
    char string_value[MAX_VAL_LEN];
    int key_1, key_2;
} input_cmd_type;
typedef struct
{
    int request_id;
    pid_t thread_worker_id;
    char string_reply_msg[MAX_REP_LEN];
} server_response_type;
typedef struct
{
    int arg1;
    input_cmd_type arg2;
} arg_struct;
void *handle_insert(void *arg)
{
    input_cmd_type *input_cmd = &((arg_struct *)arg)->arg2;
    int client_fd = ((arg_struct *)arg)->arg1;

    int key = input_cmd->key;
    string value(input_cmd->string_value);

    server_response_type server_response;
    server_response.request_id = input_cmd->request_id;
    server_response.thread_worker_id = gettid();

    pthread_mutex_lock(&mutex_lock_dict);

    bool key_found = false;
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key)
        {
            key_found = true;
            break;
        }
    }
    if (key_found == false)
    {
        dictionary.push_back(make_pair(key, value));
        strcpy(server_response.string_reply_msg, "Insertion Successful");
    }
    else
    {
        strcpy(server_response.string_reply_msg, "Key already exists");
    }
    pthread_mutex_unlock(&mutex_lock_dict);

    sleep(2);   // Artificial delay
    send(client_fd, &server_response, sizeof(server_response_type), 0);

    return NULL;
}
void *handle_delete(void *arg)
{
    input_cmd_type *input_cmd = &((arg_struct *)arg)->arg2;
    int client_fd = ((arg_struct *)arg)->arg1;

    int key = input_cmd->key;

    server_response_type server_response;
    server_response.request_id = input_cmd->request_id;
    server_response.thread_worker_id = gettid();

    pthread_mutex_lock(&mutex_lock_dict);

    bool key_found = false;
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key)
        {
            key_found = true;
            dictionary.erase(dictionary.begin() + i);
            break;
        }
    }
    if (key_found == true)
    {
        strcpy(server_response.string_reply_msg, "Deletion successful");
    }
    else
    {
        strcpy(server_response.string_reply_msg, "No such key exists");
    }
    pthread_mutex_unlock(&mutex_lock_dict);

    sleep(2);   // Artificial delay
    send(client_fd, &server_response, sizeof(server_response_type), 0);

    return NULL;
}
void *handle_update(void *arg)
{
    input_cmd_type *input_cmd = &((arg_struct *)arg)->arg2;
    int client_fd = ((arg_struct *)arg)->arg1;

    int key = input_cmd->key;
    string value(input_cmd->string_value);

    server_response_type server_response;
    server_response.request_id = input_cmd->request_id;
    server_response.thread_worker_id = gettid();

    pthread_mutex_lock(&mutex_lock_dict);

    bool key_found = false;
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key)
        {
            key_found = true;
            dictionary[i].second = value;
            break;
        }
    }
    if (key_found == true)
    {
        strcpy(server_response.string_reply_msg, input_cmd->string_value);
    }
    else
    {
        strcpy(server_response.string_reply_msg, "Key does not exist");
    }
    pthread_mutex_unlock(&mutex_lock_dict);

    sleep(2);   // Artificial delay
    send(client_fd, &server_response, sizeof(server_response_type), 0);

    return NULL;
}
void *handle_concat(void *arg)
{
    input_cmd_type *input_cmd = &((arg_struct *)arg)->arg2;
    int client_fd = ((arg_struct *)arg)->arg1;

    int key_1 = input_cmd->key_1;
    int key_2 = input_cmd->key_2;

    server_response_type server_response;
    server_response.request_id = input_cmd->request_id;
    server_response.thread_worker_id = gettid();

    pthread_mutex_lock(&mutex_lock_dict);

    bool key_1_found = false;
    bool key_2_found = false;
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key_1)
        {
            key_1_found = true;
            break;
        }
    }
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key_2)
        {
            key_2_found = true;
            break;
        }
    }
    if (key_1_found == true && key_2_found == true)
    {
        for (int i = 0; i < dictionary.size(); i++)
        {
            if (dictionary[i].first == key_1)
            {
                for (int j = 0; j < dictionary.size(); j++)
                {
                    if (dictionary[j].first == key_2)
                    {
                        string value1 = dictionary[i].second;
                        string value2 = dictionary[j].second;

                        dictionary[i].second = value1 + value2;
                        dictionary[j].second = value2 + value1;

                        strcpy(server_response.string_reply_msg, dictionary[j].second.c_str());
                        break;
                    }
                }
                break;
            }
        }
    }
    else
    {
        strcpy(server_response.string_reply_msg, "Concat failed as at least one of the keys does not exist");
    }
    pthread_mutex_unlock(&mutex_lock_dict);

    sleep(2);   // Artificial delay
    send(client_fd, &server_response, sizeof(server_response_type), 0);

    return NULL;
}
void *handle_fetch(void *arg)
{
    input_cmd_type *input_cmd = &((arg_struct *)arg)->arg2;
    int client_fd = ((arg_struct *)arg)->arg1;

    int key = input_cmd->key;

    server_response_type server_response;
    server_response.request_id = input_cmd->request_id;
    server_response.thread_worker_id = gettid();

    pthread_mutex_lock(&mutex_lock_dict);

    bool key_found = false;
    for (int i = 0; i < dictionary.size(); i++)
    {
        if (dictionary[i].first == key)
        {
            key_found = true;
            strcpy(server_response.string_reply_msg, dictionary[i].second.c_str());
            break;
        }
    }
    if (key_found == false)
    {
        strcpy(server_response.string_reply_msg, "Key does not exist");
    }
    pthread_mutex_unlock(&mutex_lock_dict);

    sleep(2);   // Artificial delay
    send(client_fd, &server_response, sizeof(server_response_type), 0);

    return NULL;
}
void *handle_connection(void *arg)
{
    // cout << "Handling Connection " << endl;

    int client_fd = *((int *)arg);

    input_cmd_type input_cmd;
    int receive_status = recv(client_fd, &input_cmd, sizeof(input_cmd_type), 0);
    if (receive_status == 0)
    {
        cout << "Client disconnected" << endl;
        close(client_fd);
    }
    else if (receive_status < 0)
    {
        perror("recv");
        return NULL;
    }

    arg_struct args;
    args.arg1 = client_fd;
    args.arg2 = input_cmd;

    if (strcmp(input_cmd.string_cmd, "insert") == 0)
    {
        handle_insert(&args);
    }
    else if (strcmp(input_cmd.string_cmd, "delete") == 0)
    {
        handle_delete(&args);
    }
    else if (strcmp(input_cmd.string_cmd, "update") == 0)
    {
        handle_update(&args);
    }
    else if (strcmp(input_cmd.string_cmd, "concat") == 0)
    {
        handle_concat(&args);
    }
    else if (strcmp(input_cmd.string_cmd, "fetch") == 0)
    {
        handle_fetch(&args);
    }
    else
    {
        cout << "Invalid command" << endl;
        exit(1);
    }
    return NULL;
}
void *thread_init_function(void *arg)
{
    while (true)
    {
        pthread_mutex_lock(&mutex_lock_queue);
        if (client_queue.size() > 0)
        {
            int *client_fd_ptr = client_queue.front();
            client_queue.pop();
            handle_connection(client_fd_ptr);
        }
        pthread_mutex_unlock(&mutex_lock_queue);
    }
    return NULL;
}
int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        cout << COLOR_RED << "Usage: ./server <number of worker threads in the thread pool>" << COLOR_RESET << endl;
        return 1;
    }

    int thread_pool_size = atoi(argv[1]);
    pthread_t thread_pool[thread_pool_size];
    for (int i = 0; i < thread_pool_size; i++)
    {
        if (pthread_create(&thread_pool[i], NULL, thread_init_function, NULL) != 0)
        {
            cout << "Error creating thread" << endl;
            return 1;
        }
    }

    int server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd == INVALID)
    {
        perror("socket");
        return 1;
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("bind");
        return 1;
    }
    if (listen(server_fd, SERVER_BACKLOG) < 0)
    {
        perror("listen");
        return 1;
    }
    cout << COLOR_GREEN << "Server is listening on port " << SERVER_PORT << COLOR_RESET << endl;

    while (true)
    {
        cout << "Waiting for client to connect..." << endl;

        struct sockaddr_in client_addr;
        socklen_t client_addr_len = sizeof(client_addr);

        int client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &client_addr_len);
        if (client_fd == INVALID)
        {
            perror("accept");
            return 1;
        }
        cout << COLOR_GREEN << "Client connected" << COLOR_RESET << endl;

        int *client_fd_ptr = new int;
        *client_fd_ptr = client_fd;

        pthread_mutex_lock(&mutex_lock_queue);
        client_queue.push(client_fd_ptr);
        pthread_mutex_unlock(&mutex_lock_queue);
    }
    return 0;
}