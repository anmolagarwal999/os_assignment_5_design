#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <bits/stdc++.h>
using namespace std;

#define COLOR_RED "\x1B[31m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_GREEN "\x1B[32m"
#define COLOR_RESET "\x1B[0m"

#define INVALID -1
#define SERVER_PORT 8080

#define MAX_CMD_LEN 10
#define MAX_VAL_LEN 1000
#define MAX_REP_LEN 1000

typedef struct
{
    int request_id;
    int time;
    char string_cmd[MAX_CMD_LEN];
    int key;
    char string_value[MAX_VAL_LEN];
    int key_1, key_2;
} input_cmd_type;
typedef struct
{
    int request_id;
    pid_t thread_worker_id;
    char string_reply_msg[MAX_REP_LEN];
} server_response_type;

void *client_thread_func(void *arg)
{
    input_cmd_type *input_cmd = (input_cmd_type *)arg;

    int time = input_cmd->time;
    int request_idx = input_cmd->request_id;
    sleep(time);

    int server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0)
    {
        perror("socket");
        exit(1);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    int connection_status = connect(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (connection_status < 0)
    {
        perror("connect");
        exit(1);
    }

    send(server_fd, input_cmd, sizeof(input_cmd_type), 0);

    server_response_type server_response;
    int recv_status = recv(server_fd, &server_response, sizeof(server_response), 0);
    if (recv_status < 0)
    {
        perror("recv");
        exit(1);
    }
    cout << request_idx << ":" << server_response.thread_worker_id << ":" << server_response.string_reply_msg << endl;

    close(server_fd);
    pthread_exit(NULL);

    return NULL;
}
int main(int argc, char const *argv[])
{
    if (argc != 1)
    {
        cout << "Usage: ./client" << endl;
        return 1;
    }

    int n_threads;
    cin >> n_threads;

    input_cmd_type input_cmd[n_threads];
    for (int i = 0; i < n_threads; i++)
    {
        int t;
        string cmd;
        cin >> t >> cmd;

        if (cmd == "insert")
        {
            int key;
            string value;
            cin >> key >> value;

            input_cmd[i].request_id = i;

            input_cmd[i].time = t;
            strcpy(input_cmd[i].string_cmd, cmd.c_str());
            input_cmd[i].key = key;
            strcpy(input_cmd[i].string_value, value.c_str());

            input_cmd[i].key_1 = INVALID;
            input_cmd[i].key_2 = INVALID;
        }
        else if (cmd == "delete")
        {
            int key;
            cin >> key;

            input_cmd[i].request_id = i;

            input_cmd[i].time = t;
            strcpy(input_cmd[i].string_cmd, cmd.c_str());
            input_cmd[i].key = key;

            strcpy(input_cmd[i].string_value, "");
            input_cmd[i].key_1 = INVALID;
            input_cmd[i].key_2 = INVALID;
        }
        else if (cmd == "update")
        {
            int key;
            string value;
            cin >> key >> value;

            input_cmd[i].request_id = i;

            input_cmd[i].time = t;
            strcpy(input_cmd[i].string_cmd, cmd.c_str());
            input_cmd[i].key = key;
            strcpy(input_cmd[i].string_value, value.c_str());

            input_cmd[i].key_1 = INVALID;
            input_cmd[i].key_2 = INVALID;
        }
        else if (cmd == "concat")
        {
            int key_1, key_2;
            cin >> key_1 >> key_2;

            input_cmd[i].request_id = i;

            input_cmd[i].time = t;
            strcpy(input_cmd[i].string_cmd, cmd.c_str());
            input_cmd[i].key_1 = key_1;
            input_cmd[i].key_2 = key_2;

            input_cmd[i].key = INVALID;
            strcpy(input_cmd[i].string_value, "");
        }
        else if (cmd == "fetch")
        {
            int key;
            cin >> key;

            input_cmd[i].request_id = i;

            input_cmd[i].time = t;
            strcpy(input_cmd[i].string_cmd, cmd.c_str());
            input_cmd[i].key = key;

            strcpy(input_cmd[i].string_value, "");
            input_cmd[i].key_1 = INVALID;
            input_cmd[i].key_2 = INVALID;
        }
        else
        {
            perror("Invalid command");
            return 1;
        }
    }
    // cout << endl;

    pthread_t threads[n_threads];
    for (int i = 0; i < n_threads; i++)
    {
        if (pthread_create(&threads[i], NULL, client_thread_func, &input_cmd[i]) != 0)
        {
            perror("pthread_create");
            return 1;
        }
    }

    for (int i = 0; i < n_threads; i++)
    {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
