# Multithreaded client and server

### Multiple clients making requests to a single server program

## Client Program

### Runninng the client program
```
./client

1. The first line of input would be the total number of user requests throughout the simulation (m).
2. The next m lines would be the user requests.
    <Time in sec after which the request to connect to the server is to be made> <cmd with appropriate arguments>
```


### Input Commands

```
1. insert <key> <value> : This command is supposed to create a new “key” "vaue" pair on the server’s
    dictionary if not already exists
2. delete <key> : This command is supposed to remove the <key> from the dictionary.
3. update <key> <value> : This command is supposed to update the value corresponding to <key> on the
    server’s dictionary and set its value as <value>.
4. concat <key1> <key2> : Let values corresponding to the keys before execution of this command be 
    {key1:value_1, key_2:value_2}. 
Then, the corresponding values after this command's execution should be 
    key1:value_1+value_2, key_2: value_2+value_1}
5. fetch <key> : must display the value corresponding to the key if it exists at the connected server.

```
### Output

```
For each concluded user request, the user thread must print the request index ; followed by a colon ;
 followed by the thread worker ID on the server end which catered to the request; followed by the 
 verdict/error/key value.
```

The client program creates a number of threads equal to the number of user requests. Each thread is responsible for executing onr of the user request.

Inside the thread function, the connection is established with the server and the user request is sent to the server in form of a struct.
Also the response from the server is received in form of a struct and the response is printed to the client. 


## Server Program

### Runninng the client program

```
./server <number of worker threads in the thread pool>
```

The server maintains a dictionary. A dictionary is a container that stores mappings of unique keys to values. It is basically vector<pair<int, string>> dictionary.

The dictionary values are common across all clients.

As soon as the server program starts it creates ‘n’ worker threads in the thread pool. These ‘n’ threads are the ones which are supposed to deal with the client requests. As a result, atmax ‘n’ client requests will be handled by the server at any given instant.

The connection is established using the following function calls:
* int server_fd = socket(AF_INET, SOCK_STREAM, 0);
* bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr))
* listen(server_fd, SERVER_BACKLOG)

Whenever a new client’s connection request is accepted (using the accept() system call), the server pushes the client request to a client_queue. The server threads picks up one of the client request from the client_queue and processes it using pthread_mutex_lock(&mutex_lock_queue); and pthread_mutex_unlock(&mutex_lock_queue);

While performing the request, the server thread checks if the request is valid or not. If the request is valid, the server thread calls the appropriate function to perform the request.

In appropriate function, the given operations are performed using the  pthread_mutex_lock(&mutex_lock_dict); and pthread_mutex_unlock(&mutex_lock_dict); and then a response is sent back to the client.

The response contains the request index, the thread worker ID on the server end which catered to the request, and the verdict/error/key value.