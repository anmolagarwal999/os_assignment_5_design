#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "Person.h"
#include "Goals.h"

#define RED printf("\033[0;31m");
#define GREEN printf("\033[0;32m");
#define PURPLE printf("\033[0;35m");
#define YELLOW printf("\033[0;33m");
#define WHITE printf("\033[0;37m");
#define MAGENTA printf("\033[0;35m");
#define CYAN printf("\033[0;36m");
#define RESET printf("\033[0m");

int zone_H_capacity;
int zone_A_capacity;
int zone_N_capacity;

time_t simulation_startTime;

pthread_mutex_t group_mutex;
pthread_mutex_t zone_mutex;
pthread_mutex_t scoreboard_mutex;

pthread_cond_t group_cond;
pthread_cond_t zone_cond;
pthread_cond_t scoreboard_cond;

int num_groups;
int spectating_time;
int num_of_goalscoring_chances;

group_ADT *groups;
scoreBoard_ADT *scoreBoard;

#define VALID 1
#define INVALID -1

#define true 1
#define false 0

#endif