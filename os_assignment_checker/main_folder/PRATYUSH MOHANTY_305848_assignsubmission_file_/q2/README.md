# The Clasico Experience

### Compiling the Code
```
1. Using makefile:
    make

2. Compile using gcc
    gcc -pthread Person.c Goals.c
```
### Running the Code
```
./a.out
```

### main.c

* Initializes the environment, and then runs the simulation.
```
* Initializes all global variables and notes the simultaion start time.

* Calls for the functions for taking all the inputs about different entities:
    1) Person
    2) Goals / ScoreBoard

* Calls functions for creating threads for different entities:
    1) Person : Each person has a individual thread
    2) Goals / ScoreBoard : This thread is used for noting down which goals are achieved by which team.
```

### Goals.c

Contains the code for following functions:
```
* take_scoreGoalInput(): For taking user input about a goal scoring chance({team, time_elapsed, probability of scoring goal}).

* create_scoreGoal(): For creating a thread for a goal scoring chance.

* scoreGoal_thread_func(): For running the thread for a goal scoring chance.
    - For each of the goal scoring chances, 
        it radomly decides whether the goal is scored or not by a team based on the probability of scoring goal.
    - While doing so it keeps updating the score of the teams:
         -  num of goals scored by A
         -  num of goals scored by B
    using mutexes 
```
### Person.c

Contains code for following functions:

```
* take_personInput(): For taking user input about a person. 

* create_personThread(): This function is used for creating a thread for each person.

* person_thread_func(): This function is used for running the thread function for each person.
    This function calls for 3 functions:
        1) allocate_seat(person);
        2) check_if_seatAllocated(person);
        3) leave_dueTo_badPerformance(person);
        4) free_seat(person);

* allocate_seat(): This function is used for allocating a seat to a person based on the team he supports i.e. {H/N/A}. 
    Also it updates the number of seats left in each zone.

* check_if_seatAllocated(): This function checks if time_now > person_leave_time.
    If yes, then print person couldnot get a seat.

* leave_dueTo_badPerformance(): This function is used check for leaving the stadium due to bad performance.
    If person->enrage_value > num_of_goalsBy_Opponent team
        then person leaves due to bad performance of his team and vacants a seat.

* free_seat(): This function is used for freeing a seat in a given zone allocated to a person.
    This also keeps a track of the number of perople of a particular group that have left and are waiting for their friends. 
    When all the persons of a group have left the stadium, then the group leaves for dinner.
```