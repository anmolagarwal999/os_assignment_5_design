#ifndef _SCORE_BOARD_H_
#define _SCORE_BOARD_H_

typedef struct
{
    char team;
    int time_elapsed;
    float probability;
} goal_ADT;
typedef struct
{
    goal_ADT *goals_scored;
} scoreBoard_ADT;

pthread_t scoreGoalThread;
int num_of_goalsBy_A;
int num_of_goalsBy_H;

void take_scoreGoalInput();

void create_scoreGoalThread();
void join_scoreGoalThread();

void *scoreGoal_thread_func(void *arg);

#endif