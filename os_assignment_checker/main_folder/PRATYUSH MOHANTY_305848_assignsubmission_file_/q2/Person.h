#ifndef _PERSON_H_
#define _PERSON_H_

typedef struct
{
    int groupID;
    char name[50];
    char fanof_H_A_N;

    int reach_time;
    int patience_time;
    
    int enrage_value;
    int is_enraged;

    int seat_receive_time;
    char seat_receive_zone;

    int is_at_exit;
    pthread_t personThread;
    pthread_mutex_t person_mutex;
} person_ADT;
typedef struct
{
    int groupID;
    int no_of_people;
    int no_of_people_at_exit;
    person_ADT *person_in_grp;
} group_ADT;

void take_personInput();

void create_personThread();
void join_personThread();

void *person_thread_func(void *arg);

#endif