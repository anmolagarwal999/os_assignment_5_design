#include "global.h"
#include "Goals.h"

void take_scoreGoalInput()
{
    for (int i = 0; i < num_of_goalscoring_chances; i++)
    {
        char team;
        int time_elapsed;
        float probability;
        scanf("\n%c %d %f", &team, &time_elapsed, &probability);

        (scoreBoard->goals_scored[i]).team = team;
        (scoreBoard->goals_scored[i]).time_elapsed = time_elapsed;
        (scoreBoard->goals_scored[i]).probability = probability;
        // printf("%c %d %f\n", (scoreBoard->goals_scored[i]).team, (scoreBoard->goals_scored[i]).time_elapsed, (scoreBoard->goals_scored[i]).probability);
    }
}
void create_scoreGoalThread()
{
    if (pthread_create(&scoreGoalThread, NULL, scoreGoal_thread_func, &scoreBoard) != 0)
    {
        printf("Error creating goalThread\n");
        exit(1);
    }
}
void join_scoreGoalThread()
{
    pthread_join(scoreGoalThread, NULL);
}
void *scoreGoal_thread_func(void *arg)
{
    scoreBoard_ADT *scoreBoardtemp = (scoreBoard_ADT *)arg;
    for (int i = 0; i < num_of_goalscoring_chances; i++)
    {
        int prev_time_elapsed = 0;
        if (i > 0)
        {
            prev_time_elapsed = scoreBoard->goals_scored[i - 1].time_elapsed;
        }
        int curr_time_elapsed = scoreBoard->goals_scored[i].time_elapsed;
        sleep(curr_time_elapsed - prev_time_elapsed);

        if (scoreBoard->goals_scored[i].probability * 100 > rand() % 100)
        {
            pthread_mutex_lock(&scoreboard_mutex);
            if (scoreBoard->goals_scored[i].team == 'H')
            {
                YELLOW
                printf("t = %ld: Team %c has scored their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_H + 1);
                RESET
                num_of_goalsBy_H++;
            }
            else
            {
                YELLOW
                printf("t = %ld: Team %c has scored their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_A + 1);
                RESET
                num_of_goalsBy_A++;
            }
            pthread_cond_broadcast(&scoreboard_cond);
            pthread_mutex_unlock(&scoreboard_mutex);
        }
        else
        {
            RED if (scoreBoard->goals_scored[i].team == 'H')
                printf("t = %ld: Team %c missed the chance to score their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_H + 1);
            else printf("t = %ld: Team %c missed the chance to score their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_A + 1);
            RESET
        }
    }
}