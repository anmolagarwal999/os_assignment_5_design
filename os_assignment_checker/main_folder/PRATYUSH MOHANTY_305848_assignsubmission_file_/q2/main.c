#include "global.h"

void init_simulation()
{
    pthread_mutex_init(&zone_mutex, NULL);
    pthread_mutex_init(&scoreboard_mutex, NULL);
    pthread_mutex_init(&group_mutex, NULL);
    
    pthread_cond_init(&zone_cond, NULL);
    pthread_cond_init(&scoreboard_cond, NULL);
    pthread_cond_init(&group_cond, NULL);

    simulation_startTime = time(NULL);
    num_of_goalsBy_A = 0;
    num_of_goalsBy_H = 0;
}
int main(int argc, char const *argv[])
{
    srand(time(NULL));
    groups = NULL;
    scoreBoard = NULL;

    scanf("%d %d %d", &zone_H_capacity, &zone_A_capacity, &zone_N_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    groups = (group_ADT *)malloc(sizeof(group_ADT) * num_groups);
    take_personInput();

    scanf("%d", &num_of_goalscoring_chances);

    scoreBoard = (scoreBoard_ADT *)malloc(sizeof(scoreBoard_ADT));
    scoreBoard->goals_scored = (goal_ADT *)malloc(sizeof(goal_ADT) * num_of_goalscoring_chances);

    take_scoreGoalInput();
    // for (int i = 0; i < num_of_goalscoring_chances; i++)
    // {
    //     printf("%c %d %f\n", (scoreBoard->goals_scored[i]).team, (scoreBoard->goals_scored[i]).time_elapsed, (scoreBoard->goals_scored[i]).probability);
    // }
    init_simulation();

    create_personThread();
    create_scoreGoalThread();

    join_scoreGoalThread();
    join_personThread();
}
