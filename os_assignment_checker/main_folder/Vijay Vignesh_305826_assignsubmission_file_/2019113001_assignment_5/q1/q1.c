#include<stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <pthread.h>
#include <semaphore.h>

// To make print statements color coded ---
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
// ---

struct Student
{
    int id;
    float calibre;
    int prefs[3];
    int t;
};

struct Course
{
    int id;
    char* name;
    float interest;
    int max_slots;
    int num_labs;
    int* tlabs;
    int tut_status;
    int available_slots;

    pthread_mutex_t course_mutex;
    pthread_cond_t tut_cond;
};

struct TA
{
    int id;
    int active;
    int tuts_completed;

    pthread_mutex_t TA_mutex;
};

struct Lab
{
    int id;
    char* name;
    int num_TAs;
    struct TA* TAs;
    int max_TAships;

    pthread_mutex_t lab_mutex;
};

// global variables ---
int ttl_students, ttl_courses, ttl_labs, student_queue_length;
struct Student* students;
struct Course*  courses;
struct Lab*     tlabs;
// --- end of global variables.

// global mutex locks & cond. variables ---
pthread_mutex_t student_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
// ---

// probabilistically determine whether a student finalizes a course
int courseFinalized(struct Course course, struct Student student) {
    double probability = course.interest * student.calibre * (double)RAND_MAX;
    return ((double)rand() <= probability ? 1 : 0);
}

// start of student routine ---
void* studentRoutine(void* args)
{
    struct Student* student = (struct Student*)args;

    // wait till registration time
    sleep(student->t);
    printf(ANSI_COLOR_BLUE"Student %d has filled in preferences for course registration\n"ANSI_COLOR_RESET,
           student->id);

    for(int i=0; i<3; i++)
    {
        struct Course* course = &courses[student->prefs[i]];
        
        // wait for ta to be assigned and for tut to be allocated, while course is active.
        int tut_allocated = 0;
        pthread_mutex_lock(&course->course_mutex);
        while(!tut_allocated && course->id != -1)
        {
            // waiting for ta
            while(course->tut_status != 1 && course->id != -1)
                pthread_cond_wait(&course->tut_cond, &course->course_mutex);
            
            // waiting for tut allotment
            while(course->tut_status != 2 && course->id != -1 && !tut_allocated)
            {
                if(course->available_slots > 0)
                {
                    course->available_slots--;
                    printf(ANSI_COLOR_CYAN"Student %d has been allocated a seat in course %s\n"ANSI_COLOR_RESET,
                            student->id, course->name);
                    tut_allocated = 1;
                }
                pthread_cond_wait(&course->tut_cond, &course->course_mutex);
            }
        }
        int course_active = course->id;
        pthread_mutex_unlock(&course->course_mutex);

        // if course is active, continue to tutorial
        if(course_active != -1)
        {
            // attend tutorial
            pthread_mutex_lock(&course->course_mutex);
            while(course->tut_status != 0)
                pthread_cond_wait(&course->tut_cond, &course->course_mutex);
            pthread_mutex_unlock(&course->course_mutex);

            // finalize or withdraw current course and move on to next pref
            if (courseFinalized(*course, *student)) {
                printf(ANSI_COLOR_GREEN"Student %d has selected the course %s permanently\n"ANSI_COLOR_RESET,
                        student->id, course->name);

                pthread_mutex_lock(&student_queue_mutex);
                student_queue_length--;
                pthread_mutex_unlock(&student_queue_mutex);
                break;
            }
        }

        if(i<2)
        {
            printf(ANSI_COLOR_MAGENTA"Student %d has changed preference from the course %s (priority %d) to %s (priority %d)\n"ANSI_COLOR_RESET,
                    student->id,course->name,i,courses[student->prefs[i+1]].name, i+1);
        }
        else if(i==2)
        {
            printf(ANSI_COLOR_RED"Student %d could not get any of their preferred courses\n"ANSI_COLOR_RESET, student->id);
            pthread_mutex_lock(&student_queue_mutex);
            student_queue_length--;
            pthread_mutex_unlock(&student_queue_mutex);
        }

    }
    // pthread_mutex_lock(&student_queue_mutex);
    // printf("Student %d LEFT\n",student_queue_length);
    // pthread_mutex_unlock(&student_queue_mutex);
    return NULL;
}
// --- end of student routine 

// start of course routine ---
void* courseRoutine(void* args)
{
    struct Course* course = (struct Course*)args;
    sleep(course->id);

    while(1)
    {
        // check if all students have exited
        pthread_mutex_lock(&student_queue_mutex);
        int students_remaining = student_queue_length;
        pthread_mutex_unlock(&student_queue_mutex);

        // check if course has been withdrawn
        pthread_mutex_lock(&course->course_mutex);
        int active = course->id;
        pthread_mutex_unlock(&course->course_mutex);
        
        // exit thread if no students in queue or course withdrawn
        if (!students_remaining || active == -1) {
            break;
        }

        int TA_assigned=-1,tlab_assigned=-1, lab_ct=0;

        // // assign a TA
        // while(TA_assigned == -1 && lab_ct < course->num_labs)
        // {
        //     lab_ct=0;

            // assign a TA
            for(int i=0; i < course->num_labs && tlab_assigned ==-1; i++)
            {
                struct Lab* lab = &tlabs[course->tlabs[i]];
                pthread_mutex_lock(&lab->lab_mutex);
                int lab_active = lab->id;
                pthread_mutex_unlock(&lab->lab_mutex);

                // lab inactive
                if(lab_active == -1) {lab_ct++;continue;}            
                
                int ct=0;
                for(int j=0; j < lab->num_TAs && TA_assigned == -1; j++)
                {
                    pthread_mutex_lock(&lab->TAs[j].TA_mutex);
                    int TA_active = lab->TAs[j].active;
                    int tuts_completed = lab->TAs[j].tuts_completed;
                    pthread_mutex_unlock(&lab->TAs[j].TA_mutex);
                    //printf("\n %s: %s lab_active %d -- TA no:%d  active:%d  completed:%d\n",course->name,lab->name,lab_active,j,TA_active, tuts_completed);
                    // ta already assigned
                    if(TA_active) continue;

                    // ta already completed allowed tuts
                    if(tuts_completed >= lab->max_TAships) {ct++;continue;}

                    TA_assigned   = j;
                    tlab_assigned = i;

                    pthread_mutex_lock(&lab->TAs[j].TA_mutex);
                    lab->TAs[j].active = 1;
                    pthread_mutex_unlock(&lab->TAs[j].TA_mutex);

                    printf(ANSI_COLOR_GREEN"TA %d from lab %s has been allocated to course %s for their %d%s TAship\n"ANSI_COLOR_RESET,
                                TA_assigned, lab->name, course->name, lab->TAs[j].tuts_completed+1,
                                (lab->TAs[j].tuts_completed+1 % 10 == 1   ? "st"
                                : lab->TAs[j].tuts_completed+1 % 10 == 2  ? "nd"
                                : lab->TAs[j].tuts_completed+1 % 10 == 3  ? "rd"
                                                                          : "th"));
                }
                
                // making the lab inactive
                if(ct == lab->num_TAs) {printf("Lab %s has no more available TAs\n",lab->name);lab->id = -1;}
            }
        // }

        // remove course if no more tas available.
        if(TA_assigned == -1)
        {
            pthread_mutex_lock(&course->course_mutex);
            course->id = -1;
            pthread_mutex_unlock(&course->course_mutex);
            pthread_cond_broadcast(&course->tut_cond);

            printf(ANSI_COLOR_RED"Course %s does not have any TA mentors eligible and is removed from course "
                    "offerings\n"ANSI_COLOR_RESET, course->name);

            continue;
        }

        // allocate seats for tutorial
        int D = (rand() % course->max_slots) + 1;
        printf(ANSI_COLOR_CYAN"Course %s has been allocated %d seats\n"ANSI_COLOR_RESET, course->name, D);

        // change status to 1 to show that seat alocatation has begun.
        pthread_mutex_lock(&course->course_mutex);
        course->available_slots = D;
        course->tut_status = 1;

        printf(">>>> waiting for students to enrol into tut...\n");

        pthread_mutex_unlock(&course->course_mutex);
        pthread_cond_broadcast(&course->tut_cond);

        // wait for students to enrol into tutorial
        sleep(3);

        // tutorial begins
        pthread_mutex_lock(&course->course_mutex);
        int a_s = course->available_slots;
        course->tut_status = 2;
        printf(ANSI_COLOR_MAGENTA"Tutorial has started for Course %s with %d seats filled out of %d\n"ANSI_COLOR_RESET,
               course->name, (D - a_s), D);
        pthread_mutex_unlock(&course->course_mutex);
        pthread_cond_broadcast(&course->tut_cond);
        sleep(2);

        // tutorial ends
        pthread_mutex_lock(&course->course_mutex);
        course->tut_status = 0;
        pthread_mutex_unlock(&course->course_mutex);
        pthread_cond_broadcast(&course->tut_cond);
        printf(ANSI_COLOR_YELLOW"TA %d from lab %s has completed the tutorial for the course %s\n"ANSI_COLOR_RESET,
               TA_assigned, tlabs[course->tlabs[tlab_assigned]].name, course->name);

        // deallocate TA
        pthread_mutex_lock(&tlabs[tlab_assigned].TAs[TA_assigned].TA_mutex);
        tlabs[tlab_assigned].TAs[TA_assigned].tuts_completed++;
        tlabs[tlab_assigned].TAs[TA_assigned].active=0;
        pthread_mutex_unlock(&tlabs[tlab_assigned].TAs[TA_assigned].TA_mutex);
        sleep(1);
    }

    // printf(ANSI_COLOR_RED"---- EXITING COURSE THREAD: %s ----\n"ANSI_COLOR_RESET, course->name);
    return NULL;
}
// --- end of course routine 

int main()
{
    srand(time(NULL));

    // Take input ---
    scanf("%d %d %d", &ttl_students, &ttl_labs, &ttl_courses);
    students = calloc(ttl_students, sizeof(struct Student));
    courses  = calloc(ttl_courses,  sizeof(struct Course ));
    tlabs    = calloc(ttl_labs,     sizeof(struct Lab    ));
    student_queue_length = ttl_students;

    for(int i=0; i<ttl_courses; i++)
    {
        struct Course course;
        course.tut_status=0; course.available_slots = 0;
        course.id = i; course.name = calloc(1000, sizeof(char));
        scanf("%s %f %d %d", course.name, &course.interest, &course.max_slots, &course.num_labs);
        course.tlabs = calloc(course.num_labs, sizeof(int));
        for (int j = 0; j < course.num_labs; j++) scanf("%d", &course.tlabs[j]);

        pthread_mutex_init(&course.course_mutex, NULL);
        pthread_cond_init(&course.tut_cond, NULL);

        courses[i] = course;
    }
    for(int i=0; i<ttl_students; i++)
    {
        struct Student student;
        student.id = i;
        scanf("%f %d %d %d %d", &student.calibre, &student.prefs[0], &student.prefs[1], &student.prefs[2], &student.t);

        students[i] = student;
    }
    for(int i=0; i<ttl_labs; i++)
    {
        struct Lab lab;
        lab.id = i; lab.name = calloc(1000, sizeof(char));
        scanf("%s %d %d", lab.name, &lab.num_TAs, &lab.max_TAships);
        pthread_mutex_init(&lab.lab_mutex, NULL);
        lab.TAs = calloc(lab.num_TAs, sizeof(struct TA));
        for(int j=0; j<lab.num_TAs; j++) 
        {
            lab.TAs[j].id=j;
            lab.TAs[j].active=0; 
            lab.TAs[j].tuts_completed=0;
            pthread_mutex_init(&lab.TAs[j].TA_mutex, NULL);
        }

        tlabs[i] = lab;
    }
    // --- end of taking input.

    // initialize student threads ---
    pthread_t student_threads[ttl_students];
    for(int i=0; i<ttl_students; i++)
    {
        struct Student* target = &students[i];
        if (pthread_create(&student_threads[i], NULL, &studentRoutine, target))
            perror("Failed to create student threads");
    }
    // --- end of initializing student thread.

    // initialize course threads ---
    pthread_t course_threads[ttl_courses];
    for(int i=0; i<ttl_courses; i++)
    {
        struct Course* target = &courses[i];
        if (pthread_create(&course_threads[i], NULL, &courseRoutine, target))
            perror("Failed to create course threads");
    }
    // --- end of initializing course thread.

    // join created threads ---
    for(int i=0; i<ttl_students; i++)
        if(pthread_join(student_threads[i], NULL))
            perror("Failed to join student thread");
    for(int i=0; i<ttl_courses; i++)
        if(pthread_join(course_threads[i], NULL))
            perror("Failed to join course thread");

    printf("\nSimulation Complete\n");
    return 0;
}