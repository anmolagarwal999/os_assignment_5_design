#include <iostream>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <string>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <strings.h>

sem_t curr_sem;
const int MSG_LEN = 256;
const int MAX_CLIENTS = 4;
const int PORT_ARG = 8001;


void *thread (void *arg) {
    sem_wait(&curr_sem);
    printf("\nEnter Thread ID: %ld\n", pthread_self());
    sleep(5);
    printf("Exit Thread ID: %ld ===\n", pthread_self());
    sem_post(&curr_sem);
    return (void*)(NULL);
}

// g++ -pthread -Ofast -static -static server.cpp -o server
int main(int argc, const char *argv[]) {
    // if (argc != 2) {
    //     std::cout << "Invalid number of arguments\n";
    //     std::cout << "Usage: ./server <number of worker threads in the thread pool>\n";
    //     return 0;
    // }
    int welcome_socket_fd;
    int client_scoket_fd;
    int port_no = 8000;
    socklen_t cli_len;
    struct socket ;
    welcome_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    bzero((char*));
    sem_init(&curr_sem, 0, 2);
    pthread_t t[4];
    for (int i = 0; i < 4; ++i)
        pthread_create(&t[i], NULL, thread, NULL), sleep(1);
    for (int i = 0; i < 4; ++i) {
        pthread_join(t[i], NULL);
    }
    sem_destroy(&curr_sem);
    return 0;
}