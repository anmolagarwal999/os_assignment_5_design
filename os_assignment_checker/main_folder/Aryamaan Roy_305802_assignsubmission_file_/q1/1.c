#include "1.h"
int time_now = 0;
//returns 1 if all courses are withdrawn or all students have exited/permanentely chosen
//, else 0
int is_exit(void)
{
    int s = 0, c = 0;
    int student_left_id[num_students];
    //int courses_left_id[num_courses];
    for (int i = 0; i < num_courses; i++)
    {
        if (course[i]->status != WTDRN)
            c++;
    }
    for (int i = 0; i < num_students; i++)
    {
        if (student[i]->status < PERM_C1)
        {
            student_left_id[s] = i;
            s++;
        }
    }
    if (s && c)
    {
        for (int i = 0; i < s; ++i)
        {
            if (student[student_left_id[i]]->status < WAIT_C1 || (student[student_left_id[i]]->status == WAIT_C1 && course[student[student_left_id[i]]->pref1]->status != WTDRN) || (student[student_left_id[i]]->status == WAIT_C2 && course[student[student_left_id[i]]->pref2]->status != WTDRN) || (student[student_left_id[i]]->status == WAIT_C3 && course[student[student_left_id[i]]->pref3]->status != WTDRN))
                return 0;
        }
        printf(A);
        printf("Deadlock was handled when students where left but their course preferences were withdrawn\n");
        printf(RESET);
        return 1;
    }
    else
        return 1;
}

int update_lab_status()
{
    int not_exhausted = 0;
    for (int labs = 0; labs < num_labs; ++labs)
    {
        if (lab[labs]->status == EXHAUSTED)
            continue;
        not_exhausted = 0;
        for (int tas = 0; tas < lab[labs]->TA_num; ++tas)
        {
            if (lab[labs]->TA[tas][1] < lab[labs]->max_times)
            {
                not_exhausted = 1;
                break;
            }
        }
        if (not_exhausted == 0)
        {
            pthread_mutex_lock(&lock_lab[labs]);
            lab[labs]->status = EXHAUSTED;
            pthread_mutex_unlock(&lock_lab[labs]);
            printf(B);
            printf("Lab %s no longer has students available for TA ship\n", lab[labs]->name);
            printf(RESET);
        }
    }
    return 0;
}

void *timer_func()
{
    while (1)
    {
        sleep(1);
        time_now++;
        if (time_now % 5 == 0 && is_exit())
        {
            printf("Time at exit : %d\n", time_now);
            return NULL;
        }
        pthread_cond_broadcast(&check_time);
    }
    return NULL;
}
void *student_func(void *index)
{
    int i = *(int *)index;
    pthread_mutex_lock(&lock_student);
    while (time_now < student[i]->time)
    {
        pthread_cond_wait(&check_time, &lock_student);
    }
    student[i]->status = WAIT_C1;
    pthread_mutex_unlock(&lock_student);
    printf(C);
    printf("Student %d has filled in preferences for course registration\n", i);
    printf(RESET);
    return NULL;
}
int TA_available(int course_num, int *current_TA, int *current_lab)
{
    int is_TA_left = 0;
    for (int l = 0; l < course[course_num]->no_of_labs; ++l) //looping over allowed labs in course
    {
        int allowed_lab = course[course_num]->lab_list[l];
        if (lab[allowed_lab]->status == EXHAUSTED)
            continue;
        for (int i = 0; i < lab[allowed_lab]->TA_num; ++i)
        {
            if (lab[allowed_lab]->TA[i][0] == NT_ALTD && lab[allowed_lab]->TA[i][1] < lab[allowed_lab]->max_times)
            {
                //lock lab here
                pthread_mutex_lock(&lock_lab[allowed_lab]);
                if (lab[allowed_lab]->TA[i][0] == NT_ALTD && lab[allowed_lab]->TA[i][1] < lab[allowed_lab]->max_times)
                {
                    *current_TA = i;
                    *current_lab = allowed_lab;
                    lab[*current_lab]->TA[*current_TA][0] = TA_BUSY;
                    lab[*current_lab]->TA[*current_TA][1]++;
                    //unlock lab here
                    pthread_mutex_unlock(&lock_lab[allowed_lab]);
                    return 0;
                }
                else
                {
                    pthread_mutex_unlock(&lock_lab[allowed_lab]);
                    continue;
                }
            }
            if (is_TA_left == 0 && lab[allowed_lab]->TA[i][1] < lab[allowed_lab]->max_times)
            {
                is_TA_left = 1;
            }
        }
    }
    *current_lab = -1;
    *current_TA = -1;
    if (!is_TA_left)
    {
        return -2;
    }
    else
        return -1;
}
void *courses_func(void *index)
{
    int course_id = *(int *)index;
    int current_TA, current_lab;
    sleep(2);
    while (1)
    {
        //check if TA is available
        int eligible = TA_available(course_id, &current_TA, &current_lab);
        if (eligible == -2)
        {
            printf(D);
            printf("Course %s doesnt have any TAs eligible and is removed from the course offerings\n", course[course_id]->name);
            printf(RESET);
            courses_left--;
            course[course_id]->status = WTDRN;
            return NULL;
        }
        //wait for TA if not
        //handle deadlock if only course left
        while (current_TA < 0 && courses_left > 1)
        {
            int run = 1;
            pthread_mutex_lock(&lock_checkTA);
            while (run)
            {
                pthread_cond_wait(&check_TA, &lock_checkTA);
                run = 0;
            }
            pthread_mutex_unlock(&lock_checkTA);
            eligible = TA_available(course_id, &current_TA, &current_lab);
        }
        if (eligible == -2)
        {
            printf(D);
            printf("Course %s doesnt have any TAs eligible and is removed from the course offerings\n", course[course_id]->name);
            printf(RESET);
            courses_left--;
            course[course_id]->status = WTDRN;
            return NULL;
        }
        printf(E);
        printf("TA %d from lab %s has been allocated to course %s for his %dst TA ship\n", current_TA, lab[current_lab]->name, course[course_id]->name, lab[current_lab]->TA[current_TA][1]);
        printf(RESET);
        //update whether any lab has been exhausted or not
        update_lab_status();
        course[course_id]->status = WAIT_SLOT;
        //deciding number of slots
        int slots = (rand() % (course[course_id]->course_max_slot)) + 1;
        printf(F);
        printf("Course %s has been alloted %d seats\n", course[course_id]->name, slots);
        printf(RESET);
        int W = 0;
        int student_interested[num_students];
        for (int s = 0; s < num_students && W < slots; ++s)
        {
            if (student[s]->status == WAIT_C1 && student[s]->pref1 == course_id)
            {
                student_interested[W] = s;
                W++;
            }
            else if (student[s]->status == WAIT_C2 && student[s]->pref2 == course_id)
            {
                student_interested[W] = s;
                W++;
            }
            else if (student[s]->status == WAIT_C3 && student[s]->pref3 == course_id)
            {
                student_interested[W] = s;
                W++;
            }
            else
                continue;
        }

        course[course_id]->status = TUT_ON;
        for (int s = 0; s < W; ++s)
        {
            printf(G);
            printf("Student %d has been allocated a seat in course %s\n", student_interested[s], course[course_id]->name);
            printf(RESET);
            student[student_interested[s]]->status -= 3;
        }
        printf(H);
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", course[course_id]->name, W, slots);
        printf(RESET);
        sleep(5);
        int chosen = 0;
        double probability;
        for (int s = 0; s < W; ++s)
        {
            probability = (course[course_id]->interest) * (student[student_interested[s]]->calibre);
            int x=rand()%10000 + 1;
            probability*=10000;
            if (probability > x)
            {
                chosen++;
                printf(I);
                printf("Student %d has selected course %s permanently\n", student_interested[s], course[course_id]->name);
                printf(RESET);
                student[student_interested[s]]->status += 6; //permanently choose course
            }
            else
            {
                printf(J);
                printf("Student %d has withdrawn from course %s\n", student_interested[s], course[course_id]->name);
                printf(RESET);
                if (student[student_interested[s]]->status == ATD_TUT_C3)
                {
                    printf("Student %d couldnt get any of his preferred courses\n", student_interested[s]);
                    student[student_interested[s]]->status = STUD_EXIT;
                    continue;
                }
                printf(K);
                if (student[student_interested[s]]->status == ATD_TUT_C1)
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->pref1]->name, course[student[student_interested[s]]->pref2]->name);
                else
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->pref2]->name, course[student[student_interested[s]]->pref3]->name);
                student[student_interested[s]]->status += 4; //move to next preference
                //if next preference course is withddrawn
                //2 cases : C1 to C3 or C2 to stud_exit
                if (student[student_interested[s]]->status == WAIT_C2 && course[student[student_interested[s]]->pref2]->status == WTDRN)
                {
                    printf("Student %d has changed current preference from %s to %s\n", student_interested[s], course[student[student_interested[s]]->pref2]->name, course[student[student_interested[s]]->pref3]->name);
                    student[student_interested[s]]->status++;
                }
                printf(RESET);
                if (student[student_interested[s]]->status == WAIT_C3 && course[student[student_interested[s]]->pref3]->status == WTDRN)
                {
                    printf("Student %d couldnt get any of his preferred courses\n", student_interested[s]);
                    student[student_interested[s]]->status = STUD_EXIT;
                }
            }
        }
        printf(L);
        printf("TA %d from lab %s has completed the tutorial and left the course %s\n", current_TA, lab[current_lab]->name, course[course_id]->name);
        printf(RESET);
        lab[current_lab]->TA[current_TA][0] = NT_ALTD;
        course[course_id]->status = WAIT_TA;
        pthread_cond_broadcast(&check_TA);
    }
    return NULL;
}

int main()
{
    //input
    scanf("%d%d%d", &num_students, &num_labs, &num_courses);
    courses_left = num_courses;
    course = (ptr_course_struct *)(malloc(sizeof(ptr_course_struct) * num_courses));
    for (int i = 0; i < num_courses; ++i)
    {
        course[i] = (ptr_course_struct)malloc(sizeof(course_struct));
        course[i]->course_id = i;
        course[i]->status = WAIT_TA;
        scanf("%s %f %d %d", course[i]->name, &course[i]->interest, &course[i]->course_max_slot, &course[i]->no_of_labs);
        course[i]->lab_list = (int *)malloc(sizeof(int) * course[i]->no_of_labs);
        if (course[i]->lab_list == NULL)
            return 1;
        for (int j = 0; j < course[i]->no_of_labs; ++j)
        {
            scanf("%d", &course[i]->lab_list[j]);
        }
    }

    student = (ptr_student_struct *)(malloc(sizeof(ptr_student_struct) * num_students));
    for (int i = 0; i < num_students; ++i)
    {
        student[i] = (ptr_student_struct)malloc(sizeof(student_struct));
        student[i]->student_id = i;
        student[i]->status = NOT_REG;
        scanf("%f%d%d%d%d", &student[i]->calibre, &student[i]->pref1, &student[i]->pref2, &student[i]->pref3, &student[i]->time);
    }

    lab = (ptr_lab_struct *)(malloc(sizeof(ptr_lab_struct) * num_labs));

    for (int i = 0; i < num_labs; ++i)
    {
        lab[i] = (ptr_lab_struct)malloc(sizeof(lab_struct));
        lab[i]->lab_id = i;
        lab[i]->current_level=0;
        scanf("%s%d%d", lab[i]->name, &lab[i]->TA_num, &lab[i]->max_times);
        lab[i]->TA = (int **)malloc(sizeof(int *) * lab[i]->TA_num);
        for (int j = 0; j < lab[i]->TA_num; ++j)
        {
            lab[i]->TA[j] = (int *)malloc(sizeof(int) * 2);
            lab[i]->TA[j][0] = NT_ALTD;
            lab[i]->TA[j][1] = 0;
        }
        if (lab[i]->max_times && lab[i]->TA_num)
            lab[i]->status = NT_EXHSTD;
        else
        {
            lab[i]->status = EXHAUSTED;
        }
    }
    //initializations
    pthread_t thread_students[num_students];
    pthread_t thread_courses[num_courses];
    srand(time(0));
    pthread_mutex_init(&lock_student, NULL);
    pthread_mutex_init(&lock_checkTA, NULL);
    lock_lab = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * num_labs);
    if (lock_lab == NULL)
    {
        return 6;
    }
    for (int labs = 0; labs < num_labs; ++labs)
    {
        pthread_mutex_init(&lock_lab[labs], NULL);
    }
    for (int i = 0; i < num_students; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_students[i], NULL, &student_func, a) != 0)
        {
            return 3;
        }
    }
    for (int i = 0; i < num_courses; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_courses[i], NULL, &courses_func, a) != 0)
        {
            return 4;
        }
    }
    if (pthread_create(&time_thread, NULL, &timer_func, NULL) != 0)
    {
        return 5;
    }
    for (int i = 0; i < num_students; i++)
    {
        pthread_join(thread_students[i], NULL);
    }
    pthread_join(time_thread, NULL);
    for (int i = 0; i < num_courses; i++)
    {
        pthread_join(thread_courses[i], NULL);
    }
    pthread_mutex_destroy(&lock_student);
    pthread_mutex_destroy(&lock_checkTA);
    for (int labs = 0; labs < num_labs; ++labs)
    {
        pthread_mutex_destroy(&lock_lab[labs]);
    }
    pthread_cond_destroy(&check_time);
    pthread_cond_destroy(&check_TA);

    //free memory
    for (int i = 0; i < num_courses; i++)
    {
        free(course[i]);
    }
    free(course);
    for (int i = 0; i < num_students; i++)
    {
        free(student[i]);
    }
    free(student);
    for (int i = 0; i < num_labs; ++i)
    {
        for (int j = 0; j < lab[i]->TA_num; ++j)
        {
            free(lab[i]->TA[j]);
        }
        free(lab[i]->TA);
        free(lab[i]);
    }
    free(lab);
    free(lock_lab);

    return 0;
}