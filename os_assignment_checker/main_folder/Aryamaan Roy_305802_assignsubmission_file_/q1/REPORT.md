Q1
# Assumption : 
Course or lab name should be < 50
# Structure
> I have created the structures of student, courses and labs in the 1.h file

# My line if thinking 

> I have implemented a thread for each course and student
> To keep Track of the time after all threads are created, I call a thread time_thread that executs a fucntion that mainly keeps track of time brementing a variable time_now 
> The functions call by each student and goal course are described below:

# Time thread
> Broadcasts signal every second to keep track of time
> This is done by incrementing a variable after sleeping a second
> It also checks the exit condition every 5 seconds to terminate the program accordingly

# Student Threads
> Each student waits using a conditional variable that is triggered every second from the broadcast from Time thread.
> The student then changes its status to "WAIT_C1" which means waiting for course 1

# Course threads
> The course thread first seacrhes for a TA to condcut the tutorial useing the TA_available() function that updates the current_TA and current_lab (its call by reference arguments) if a TA is found or else returns -1.
It returns -2 in case all TAs from all the labs that this course can take TAs from have reached their maximum tutorial limit. 
> In the above case the course thread ends, otherwise if TA not not found and current_TA is not updated, then the thread waits using conditional variable.
> Once TA is found, a random slot size is generated and students whose current preference is that course are selected.
> The tutorial is then conducted, even in the case of 0 students , it sleeps for 5 seconds and then decides for each student whether he takes or moves to next preference.
> The TA is then released and a signal is broadcasted indicating the above, so that other threads waiting for a TA can check.
> The TA selection along with rest of processes happen simultaneously for each course.
