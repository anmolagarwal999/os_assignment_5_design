#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define A "\033[1m"
#define I "\033[22m"
#define B "\033[38;5;1m"              
#define C "\033[38;5;121m"  
#define D "\033[38;5;209m" 
#define E "\033[38;5;205m"  
#define F "\033[38;5;225m" 
#define G "\033[38;5;213m"  
#define H "\033[38;5;223m"  
#define RESET "\033[0m"
#define J "\033[0;34m"   
#define K "\x1b[35m"  
#define L "\x1b[36m"

////STUDENTS STATUS////////////////////
#define NOT_REG       0
#define ATD_TUT_C1    1
#define ATD_TUT_C2    2
#define ATD_TUT_C3    3  
#define WAIT_C1       4
#define WAIT_C2       5
#define WAIT_C3       6  
#define PERM_C1       7
#define PERM_C2       8
#define PERM_C3       9
#define STUD_EXIT     10

////COURSE STATUS////////////////////
#define WAIT_TA       0
#define WAIT_SLOT     1
#define TUT_ON        2
#define WTDRN         3

////LAB STATUS////////////////////
#define EXHAUSTED     1
#define NT_EXHSTD     0

////TA STATUS////////////////////////
#define NT_ALTD       0
#define TA_BUSY       1




typedef struct{
    int course_id;
    char name[50];
    float interest;
    int course_max_slot;
    int no_of_labs;
    int *lab_list;
    int status;
}course_struct;
typedef course_struct *ptr_course_struct;


typedef struct{
int student_id;
float calibre;
int pref1,pref2,pref3;
int time;
int status;
}student_struct;
typedef student_struct *ptr_student_struct;

typedef struct {
    int lab_id;
    char name[50];
    int **TA;  //status,limit
    int TA_num;
    int max_times;
    int status;
    int current_level;
}lab_struct;
typedef lab_struct *ptr_lab_struct;

int num_courses;
int num_students;
int num_labs;
int courses_left;

ptr_course_struct *course;
ptr_student_struct *student;
ptr_lab_struct *lab;
pthread_t time_thread;
pthread_mutex_t lock_student;
pthread_mutex_t lock_checkTA;
pthread_mutex_t *lock_lab;
pthread_cond_t check_time = PTHREAD_COND_INITIALIZER;
pthread_cond_t check_TA=PTHREAD_COND_INITIALIZER;