#include "2.h"

int time_now = 0;
int TEAM_GOAL[2];

int is_no_goal()
{
    for (int g = 0; g < num_goals; ++g)
    {
        if (goal[g]->status == YET_TO_HAPPEN)
            return 0;
    }
    return 1;
}

int is_group_leave(int group_id)
{
    for(int i=0;i<num_people;++i)
    {
        if(spectator[i]->status!=SPECTATOR_EXIT && spectator[i]->group==group_id)
        {
            return 0;
        }

    }
    printf(RED);
    printf("t=%d : Group %d is leaving for dinner\n",time_now,group_id);
    printf(RESET);
    return 1;

}

int Seat_available(int id)
{
    printf(GREEN"t=%d : Seats filled : H:%d A:%d N:%d\n"RESET, time_now, stadium_zone[0]->seats_filled, stadium_zone[1]->seats_filled, stadium_zone[2]->seats_filled);
    if (spectator[id]->fan == 'H')
    {
        if (stadium_zone[0]->seats_filled < stadium_zone[0]->capacity)
        {
            pthread_mutex_lock(&lock_zone[0]);
            if (stadium_zone[0]->seats_filled < stadium_zone[0]->capacity)
            {
                stadium_zone[0]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 0;
                printf(BLUE);
                printf("t=%d : %s has got a seat in zone H\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[0]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[0]);
        }
        else if (stadium_zone[2]->seats_filled < stadium_zone[2]->capacity)
        {
            pthread_mutex_lock(&lock_zone[2]);
            if (stadium_zone[2]->seats_filled < stadium_zone[2]->capacity)
            {
                stadium_zone[2]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 2;
                printf(ORANGE);
                printf("t=%d : %s has got a seat in zone N\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[2]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[2]);
        }
        else
            return 0;
    }
    if (spectator[id]->fan == 'A')
    {
        if (stadium_zone[1]->seats_filled < stadium_zone[1]->capacity)
        {
            pthread_mutex_lock(&lock_zone[1]);
            if (stadium_zone[1]->seats_filled < stadium_zone[1]->capacity)
            {
                stadium_zone[1]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 1;
                printf(MAGENTA);
                printf("t=%d : %s has got a seat in zone A\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[1]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[1]);
        }
        else
            return 0;
    }
    else
    {
        if (stadium_zone[0]->seats_filled < stadium_zone[0]->capacity)
        {
            pthread_mutex_lock(&lock_zone[0]);
            if (stadium_zone[0]->seats_filled < stadium_zone[0]->capacity)
            {
                stadium_zone[0]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 0;
                printf(BLUE);
                printf("t=%d : %s has got a seat in zone H\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[0]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[0]);
        }
        else if (stadium_zone[1]->seats_filled < stadium_zone[1]->capacity)
        {
            pthread_mutex_lock(&lock_zone[1]);
            if (stadium_zone[1]->seats_filled < stadium_zone[1]->capacity)
            {
                stadium_zone[1]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 1;
                printf(MAGENTA);
                printf("t=%d : %s has got a seat in zone A\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[1]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[1]);
        }
        else if (stadium_zone[2]->seats_filled < stadium_zone[2]->capacity)
        {
            pthread_mutex_lock(&lock_zone[2]);
            if (stadium_zone[2]->seats_filled < stadium_zone[2]->capacity)
            {
                stadium_zone[2]->seats_filled++;
                spectator[id]->status = WATCHING;
                spectator[id]->time_of_seat = time_now;
                spectator[id]->zone = 2;
                printf(ORANGE);
                printf("t=%d : %s has got a seat in zone N\n", time_now, spectator[id]->name);
                printf(RESET);
                pthread_mutex_unlock(&lock_zone[2]);
                return 1;
            }
            else
                pthread_mutex_unlock(&lock_zone[2]);
        }
        return 0;
    }
}

int is_exit()
{
    for (int g = 0; g < num_goals; ++g)
    {
        if (goal[g]->status == YET_TO_HAPPEN)
            return 0;
    }
    for (int s = 0; s < num_people; ++s)
    {
        if (spectator[s]->status != SPECTATOR_EXIT)
            return 0;
    }
    return 1;
}
void *timer_func()
{
    while (1)
    {
        sleep(1);
        if (time_now % 5 == 0 && is_exit())
            return NULL;
        //checks all spectators who are waiting for seat or watching match
        // to see if time has exceeded
        for (int i = 0; i < num_people; ++i)
        {
            if (spectator[i]->status == WAITING_FOR_SEAT)
            {
                if (spectator[i]->arrival_time + spectator[i]->Patience_time < time_now)
                {
                    pthread_mutex_lock(&lock_spectator[i]);
                    if (spectator[i]->status == WAITING_FOR_SEAT)
                    {
                        spectator[i]->status = SPECTATOR_EXIT;
                        spectator[i]->time_of_exit = time_now;
                        pthread_mutex_unlock(&lock_spectator[i]);
                        printf(YELLOW);
                        printf("t=%d : %s couldn't get a seat\n", time_now, spectator[i]->name);
                        printf("t=%d : %s is waiting for their friends at exit\n", time_now, spectator[i]->name);
                        printf(RESET);
                        is_group_leave(spectator[i]->group);
                    }
                    else
                    {
                        pthread_mutex_unlock(&lock_spectator[i]);
                        continue;
                    }
                }
            }
            if (spectator[i]->status == WATCHING)
            {
                if (spectator[i]->time_of_seat + max_watching_time < time_now)
                {
                    pthread_mutex_lock(&lock_spectator[i]);
                    if (spectator[i]->status == WATCHING)
                    {
                        spectator[i]->status = SPECTATOR_EXIT;
                        spectator[i]->time_of_exit = time_now;
                        pthread_mutex_unlock(&lock_spectator[i]);
                        printf(CYAN);
                        printf("t=%d : %s watched the match for %d seconds and is leaving\n", time_now, spectator[i]->name, max_watching_time);
                        printf("t=%d : %s is waiting for their friends at exit\n", time_now, spectator[i]->name);
                        printf(RESET);
                        is_group_leave(spectator[i]->group);
                        stadium_zone[spectator[i]->zone]->seats_filled--;
                        pthread_cond_broadcast(&check_seat);
                    }
                    else
                    {
                        pthread_mutex_unlock(&lock_spectator[i]);
                        continue;
                    }
                }
            }
        }

        time_now++;
        pthread_cond_broadcast(&check_time);
        //printf("timer\n");
    }
    return NULL;
}

void *spectator_func(void *index)
{
    int spectator_id = *(int *)index;
    pthread_mutex_lock(&lock_to_wait_spectator);
    while (time_now < spectator[spectator_id]->arrival_time)
    {
        pthread_cond_wait(&check_time, &lock_to_wait_spectator);
    }
    //CHECK IF LOCK NEEDED TO CHANGE STATUS
    spectator[spectator_id]->status = WAITING_FOR_SEAT;
    pthread_mutex_unlock(&lock_to_wait_spectator);
    printf(LIGHT_PINK);
    printf("t=%d : %s has reached the stadium\n", time_now, spectator[spectator_id]->name);
    printf(RESET);
    int is_seat = Seat_available(spectator_id);
    while (is_seat < 0)
    {
        if (spectator[spectator_id]->status == SPECTATOR_EXIT)
        {
            return NULL;
        }
        int run = 1;
        pthread_mutex_lock(&lock_to_seat_spectator);
        while (run)
        {
            pthread_cond_wait(&check_seat, &lock_to_seat_spectator);
            run = 0;
        }
        pthread_mutex_unlock(&lock_to_seat_spectator);
        if (spectator[spectator_id]->status == SPECTATOR_EXIT)
        {
            return NULL;
        }
        is_seat = Seat_available(spectator_id);
    }

    int team_index;
    if (spectator[spectator_id]->fan == 'H')
        team_index = 1;
    else if (spectator[spectator_id]->fan == 'A')
        team_index = 0;
    else
        team_index = 2;
    if (team_index < 2)
    {
        pthread_mutex_lock(&lock_to_goal_spectator);
        while (spectator[spectator_id]->status < SPECTATOR_EXIT && !is_no_goal())
        {
            if (TEAM_GOAL[team_index] >= spectator[spectator_id]->max_tolerate_goals)
            {
                spectator[spectator_id]->status = SPECTATOR_EXIT;
                printf("t=%d : %s is leaving due to bad performance of his team\n", time_now, spectator[spectator_id]->name);
                stadium_zone[spectator[spectator_id]->zone]->seats_filled--;
                pthread_cond_broadcast(&check_seat);
                pthread_mutex_unlock(&lock_to_goal_spectator);
                printf("t=%d : %s is waiting for their friends at exit\n", time_now, spectator[spectator_id]->name);
                is_group_leave(spectator[spectator_id]->group);
                return NULL;
            }
            pthread_cond_wait(&check_goal, &lock_to_goal_spectator);
        }
        pthread_mutex_unlock(&lock_to_goal_spectator);
    }
    return NULL;
}

void *goal_func(void *index)
{
    int goal_id = *(int *)index;
    int team_index;
    if (goal[goal_id]->team == 'H')
        team_index = 0;
    else
        team_index = 1;

    pthread_mutex_lock(&lock_to_wait_goal);
    while (time_now < goal[goal_id]->score_chance_time)
    {
        pthread_cond_wait(&check_time, &lock_to_wait_goal);
    }
    pthread_mutex_unlock(&lock_to_wait_goal);

    int x=rand()%10000 +1;
    float probability=goal[goal_id]->prob*10000;
    if (probability > x)
    {
        goal[goal_id]->status = GOAL_SCORED;
        TEAM_GOAL[team_index]++;
        printf(PURPLE);
        if (team_index == 0)
        {
            printf("t=%d : Team H have scored their %d th goal\n", time_now, TEAM_GOAL[0]);
        }
        else
        {
            printf("t=%d : Team A have scored their %d th goal\n", time_now, TEAM_GOAL[1]);
        }
        printf(RESET);
    }
    else
    {
        goal[goal_id]->status = GOAL_MISSED;
        printf(LIGHT_PURPLE);
        if (team_index == 0)
        {
            printf("t=%d : Team H missed the chance to score their %d th goal\n", time_now, TEAM_GOAL[0]);
        }
        else
        {
            printf("t=%d : Team A missed the chance to score their %d th goal\n", time_now, TEAM_GOAL[1]);
        }
        printf(RESET);
    }
    pthread_cond_broadcast(&check_goal);

    return NULL;
}

int main()
{
    srand(time(0));
    
    TEAM_GOAL[0] = 0;
    TEAM_GOAL[1] = 0;
    stadium_zone = (ptr_zone_struct *)malloc(sizeof(ptr_zone_struct) * 3);
    if (stadium_zone == NULL)
        return 1;
    for (int i = 0; i < 3; ++i)
    {
        stadium_zone[i] = (ptr_zone_struct)malloc(sizeof(zone_struct));
        if (stadium_zone[i] == NULL)
            return 4;
    }
    stadium_zone[0]->Name = 'H';
    stadium_zone[1]->Name = 'A';
    stadium_zone[2]->Name = 'N';
    stadium_zone[0]->seats_filled = 0;
    stadium_zone[1]->seats_filled = 0;
    stadium_zone[2]->seats_filled = 0;

    //input
    scanf("%d %d %d", &stadium_zone[0]->capacity, &stadium_zone[1]->capacity, &stadium_zone[2]->capacity);
    scanf("%d", &max_watching_time);
    scanf("%d", &num_groups);
    spectator = (ptr_spectator_struct *)malloc(sizeof(ptr_spectator_struct) * 15 * num_groups);
    if (spectator == NULL)
        return 2;
    num_people = 0;
    for (int g = 0; g < num_groups; ++g)
    {
        scanf("%d", &k);
        for (int person = 0; person < k; ++person)
        {
            spectator[num_people] = (ptr_spectator_struct)malloc(sizeof(spectator_struct));
            if (spectator[num_people] == NULL)
                return 5;
            scanf("%s %c %d %d %d", spectator[num_people]->name, &spectator[num_people]->fan, &spectator[num_people]->arrival_time, &spectator[num_people]->Patience_time, &spectator[num_people]->max_tolerate_goals);
            spectator[num_people]->status = NOT_ARRIVED;
            spectator[num_people]->group=g;
            num_people++;
        }
    }
    scanf("%d", &num_goals);
    goal = (ptr_goal_struct *)malloc(sizeof(ptr_goal_struct) * num_goals);
    if (goal == NULL)
    {
        return 3;
    }
    for (int g = 0; g < num_goals; ++g)
    {
        goal[g] = (ptr_goal_struct)malloc(sizeof(goal_struct));
        if (goal[g] == NULL)
            return 6;
        scanf("%c", &goal[g]->team);
        while (!(goal[g]->team == 'H' || goal[g]->team == 'A'))
        {
            scanf("%c", &goal[g]->team);
        }
        scanf("%d %f", &goal[g]->score_chance_time, &goal[g]->prob);
        goal[g]->status = YET_TO_HAPPEN;
    }

    // printf("\nTest Output: \n");
    // printf("ZONES:\n");
    // for(int i=0;i<3;++i)
    // {
    //     printf("Zone %c, Capacity : %d\n",stadium_zone[i]->Name,stadium_zone[i]->capacity);
    // }
    // printf("\nSPECTATOR:\n");
    // for(int i=0;i<num_people;++i)
    // {
    //     printf("%s %c %d %d %d\n",spectator[i]->name, spectator[i]->fan, spectator[i]->arrival_time, spectator[i]->Patience_time, spectator[i]->max_tolerate_goals);
    // }
    // printf("\nGOALS\n");
    // for(int i=0;i<num_goals;++i)
    // {
    //     printf("%c %d %f\n",goal[i]->team, goal[i]->score_chance_time, goal[i]->prob);
    // }
    // printf("done\n\n");

    pthread_t thread_goals[num_goals];
    pthread_t thread_spectators[num_people];
    pthread_mutex_init(&lock_to_wait_spectator, NULL);
    pthread_mutex_init(&lock_to_wait_goal, NULL);
    pthread_mutex_init(&lock_to_seat_spectator, NULL);
    pthread_mutex_init(&lock_to_goal_spectator, NULL);

    lock_spectator = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * num_people);
    if (lock_spectator == NULL)
    {
        return 7;
    }
    for (int p = 0; p < num_people; ++p)
    {
        pthread_mutex_init(&lock_spectator[p], NULL);
    }

    lock_zone = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * 3);
    if (lock_spectator == NULL)
    {
        return 11;
    }
    for (int p = 0; p < 3; ++p)
    {
        pthread_mutex_init(&lock_zone[p], NULL);
    }

    for (int i = 0; i < num_people; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_spectators[i], NULL, &spectator_func, a) != 0)
        {
            return 8;
        }
    }
    for (int i = 0; i < num_goals; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;
        if (pthread_create(&thread_goals[i], NULL, &goal_func, a) != 0)
        {
            return 9;
        }
    }
    if (pthread_create(&time_thread, NULL, &timer_func, NULL) != 0)
    {
        return 10;
    }
    for (int i = 0; i < num_people; i++)
    {
        pthread_join(thread_spectators[i], NULL);
    }
    pthread_join(time_thread, NULL);
    for (int i = 0; i < num_goals; i++)
    {
        pthread_join(thread_goals[i], NULL);
    }
    for (int p = 0; p < num_people; ++p)
    {
        pthread_mutex_destroy(&lock_spectator[p]);
    }
    for (int p = 0; p < 3; ++p)
    {
        pthread_mutex_destroy(&lock_zone[p]);
    }
    pthread_mutex_destroy(&lock_to_wait_spectator);
    pthread_mutex_destroy(&lock_to_wait_goal);
    pthread_mutex_destroy(&lock_to_seat_spectator);
    pthread_mutex_destroy(&lock_to_goal_spectator);
    pthread_cond_destroy(&check_time);
    pthread_cond_destroy(&check_goal);
    pthread_cond_destroy(&check_seat);


    //free memory 
   
    for (int i = 0; i < 3; ++i)
    {
        free(stadium_zone[i]);
    }
    free(stadium_zone);
    return 0;
}