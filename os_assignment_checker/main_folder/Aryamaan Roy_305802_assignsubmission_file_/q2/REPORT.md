Q2

1) there should not be more than 15 people in each group
2) name of spectator should not be more than 25 characters

# My line of thinking :
1. I created threads for each goal scored, and each spectator
2. To keep Track of the time after all threads are created, I call a thread time_thread that executs a fucntion that mainly keeps track of time brementing a variable time_now 
3. The functions call by each spectator and goal thread are described below:

# Goal Thread:
> Triggered every second by timer to check whther it is time to score goal
> After time reached, it decides whether goal scored or not and broadcasts a signal for conditional variable to spectator called check_goal

# Timer thread
> Broadcasts signal every second to keep track of time
> Check for all spectators who are either waiting for seat or if match time exceeded and then changed status of student

# Spectator
> Each spectator waits by a conditional variable and changes its status when it is its time to enter stadium
> Then it runs a function Seat_available() that allots seat and changes spectator status 
> If not alloted, it waits for seat using a conditional variable check_seat
> After getting alloted it check and waits whether goals scored are below the limit the user can handle

# BONUS Implementation:
> I store the group_id of each spectator on input
> When spectator leaves seat or couldnt get seat, then call function:
is_group_leave(group_id) which checks and prints that if all spectators of that group has exited, then group goes for dinner
