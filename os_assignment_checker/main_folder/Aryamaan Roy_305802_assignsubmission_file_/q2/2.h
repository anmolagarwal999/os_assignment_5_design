#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"              //
#define GREEN "\033[38;5;121m"  //
#define ORANGE "\033[38;5;209m" //
#define PURPLE "\033[38;5;205m"  //
#define LIGHT_PINK "\033[38;5;225m" //
#define LIGHT_PURPLE "\033[38;5;213m"  //
#define YELLOW "\033[38;5;223m"  //
#define RESET "\033[0m"
#define BLUE "\033[0;34m"   //
#define MAGENTA "\x1b[35m"  //
#define CYAN "\x1b[36m"

////SPECTATOR STATUS
#define NOT_ARRIVED         0
#define WAITING_FOR_SEAT    1
#define WATCHING            2
#define SPECTATOR_EXIT      3

//GOAL STATUS
#define YET_TO_HAPPEN       0
#define GOAL_SCORED         1
#define GOAL_MISSED         2

typedef struct
{
    char Name;
    int capacity;
    int seats_filled;
} zone_struct;
typedef zone_struct *ptr_zone_struct;



typedef struct
{
    char name[25];
    int arrival_time;
    int time_of_seat;
    int time_of_exit;
    char fan;
    int status;
    int Patience_time;
    int max_tolerate_goals;
    int zone;
    int group;
} spectator_struct;
typedef spectator_struct *ptr_spectator_struct;


typedef struct
{
    char team;
    int score_chance_time;
    float prob;
    int status;
} goal_struct;
typedef goal_struct *ptr_goal_struct;


ptr_zone_struct *stadium_zone;
ptr_spectator_struct *spectator;
int max_watching_time; 
int num_groups;
int num_people;
int k; 
ptr_goal_struct *goal;
int num_goals;


pthread_mutex_t *lock_spectator;
pthread_mutex_t *lock_zone;
pthread_mutex_t lock_to_wait_spectator;
pthread_mutex_t lock_to_seat_spectator;
pthread_mutex_t lock_to_goal_spectator;
pthread_mutex_t lock_to_wait_goal;
pthread_t time_thread;
pthread_cond_t check_time = PTHREAD_COND_INITIALIZER;
pthread_cond_t check_goal=PTHREAD_COND_INITIALIZER;
pthread_cond_t check_seat=PTHREAD_COND_INITIALIZER;