# Question 2

## Running The Code
The steps to run the code are as follows:
1. Run `gcc -g main.c -lpthread` to compile the code.
2. Then run `./a.out` to run the code.
3. Input the test cases in the terminal.
4. You can store the test cases in a file and redirect them to the standard input of a.out as follows: `./a.out < input.txt`. Here, input.txt is the file containing the test cases.

## Logic

We have used threads in this assignment. This allows for different clients to connect to the same server at the same time. The _race_ condition can also be found as we can modify the dictionary because of the multiple threads. To prevent the _race_ condition, _mutex_ locks can be used which makes the modifying operations thread safe as they become atomic. Mutex locks have the ability to change the value only inside the critial section. There is no parallelism implemented, only concurrency. A queue data structure is used to store the client connections that cannot be processed at the moment due to the busy threads in the pool. To avoid busy waiting, conditional variables are used. 

## Files

- `src/main.c`: The main file which contains all the code.

## Assumptions

There should not be too many requests at the same time or the server might hang beacuse all the threads would be busy. A lock would then come into place because there are several more requests and still the server is busy.
