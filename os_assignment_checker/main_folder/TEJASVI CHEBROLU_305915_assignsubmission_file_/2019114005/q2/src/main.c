#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct person
{
    int id;
    int patience_time;
    char name[20];
    int reach_time;
    char fan_type;
    int group_no;
    int num_goals;
} person;

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

typedef struct team
{
    char team_type;
    float *probability_goal;
    int num_chances;
    int *time_from_previous_chance;
    int id;
} team;

person *people;
sem_t h_zone, a_zone, n_zone;
int num_people = 0;
int goals[2];
pthread_cond_t *person_moved;
pthread_t *t_people, *t_teams;
int h_capacity, a_capacity, n_capacity, spectating_time;
pthread_mutex_t goals_lock[2];
char *person_in_zone;
team *teams;
pthread_mutex_t *person_zone_lock;
pthread_cond_t goals_changed[2];

void *search_in_h_zone(void *args)
{
    struct timespec ts;
    person p_person = *(person *)args;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec = p_person.patience_time + ts.tv_sec;
    bool time_check = sem_timedwait(&h_zone, &ts) == -1 && errno == ETIMEDOUT;
    if (time_check)
    {
        int index = p_person.id;
        pthread_mutex_lock(&person_zone_lock[index]);
        if (person_in_zone[index] == 'E')
        {
            person_in_zone[index] = 'D';
        }
        pthread_mutex_unlock(&person_zone_lock[index]);
        return NULL;
    }
    int index = p_person.id;
    pthread_mutex_lock(&person_zone_lock[index]);
    if (person_in_zone[p_person.id] - 'E' == 0 || person_in_zone[p_person.id] - 'D' == 0)
    {
        int index = p_person.id;
        person_in_zone[index] = 'H';
        char t_fan_type = p_person.fan_type;
        printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone H" ANSI_COLOR_RESET "\n", p_person.name, t_fan_type);
    }
    else
    {
        sem_post(&h_zone);
    }
    pthread_mutex_unlock(&person_zone_lock[p_person.id]);

    return NULL;
}

void *search_in_a_zone(void *args)
{
    struct timespec ts;
    person p_person = *(person *)args;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec = p_person.patience_time + ts.tv_sec;
    bool time_check = sem_timedwait(&a_zone, &ts) == -1 && errno == ETIMEDOUT;

    if (time_check)
    {
        int index = p_person.id;
        pthread_mutex_lock(&person_zone_lock[index]);
        if (person_in_zone[index] == 'E')
        {
            person_in_zone[index] = 'D';
        }
        pthread_mutex_unlock(&person_zone_lock[index]);
        index = p_person.id;
        return NULL;
    }

    int index = p_person.id;
    pthread_mutex_lock(&person_zone_lock[index]);
    if (person_in_zone[p_person.id] == 'E' || person_in_zone[p_person.id] == 'D')
    {
        char t_fan_type = p_person.fan_type;
        person_in_zone[p_person.id] = 'A';
        printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone A" ANSI_COLOR_RESET "\n", p_person.name, t_fan_type);
    }
    else
    {
        sem_post(&a_zone);
    }
    pthread_mutex_unlock(&person_zone_lock[p_person.id]);

    return NULL;
}

void *search_in_n_zone(void *args)
{
    struct timespec ts;
    person p_person = *(person *)args;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec = p_person.patience_time + ts.tv_sec;
    bool time_check = sem_timedwait(&n_zone, &ts) == -1 && errno == ETIMEDOUT;

    if (time_check)
    {
        int index = p_person.id;
        pthread_mutex_lock(&person_zone_lock[index]);
        if (person_in_zone[index] == 'E')
        {
            person_in_zone[index] = 'D';
        }
        index = p_person.id;
        pthread_mutex_unlock(&person_zone_lock[index]);
        return NULL;
    }
    int index = p_person.id;
    pthread_mutex_lock(&person_zone_lock[index]);
    if (person_in_zone[p_person.id] - 'E' == 0 || person_in_zone[p_person.id] - 'D' == 0)
    {
        char t_fan_type = p_person.fan_type;
        person_in_zone[p_person.id] = 'N';
        printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone N" ANSI_COLOR_RESET "\n", p_person.name, t_fan_type);
    }
    else
    {
        sem_post(&n_zone);
    }
    index = p_person.id;
    pthread_mutex_unlock(&person_zone_lock[index]);

    return NULL;
}

void *simulate_person_seat(void *args)
{
    person p_person = *(person *)args;
    bool check_person_type = p_person.fan_type == 'H';
    if (check_person_type)
    {
        pthread_t h_thread;
        pthread_create(&h_thread, NULL, search_in_h_zone, &p_person);
        pthread_t n_thread;
        pthread_create(&n_thread, NULL, search_in_n_zone, &p_person);
        int thread_count = 0;
        pthread_join(h_thread, NULL);
        thread_count++;
        pthread_join(n_thread, NULL);
        thread_count++;
    }
    else if (p_person.fan_type == 'A')
    {
        pthread_t a_thread;
        pthread_create(&a_thread, NULL, search_in_a_zone, &p_person);
        pthread_join(a_thread, NULL);
    }
    else if (p_person.fan_type == 'N')
    {
        pthread_t h_thread;
        pthread_create(&h_thread, NULL, search_in_h_zone, &p_person);
        pthread_t a_thread;
        pthread_create(&a_thread, NULL, search_in_a_zone, &p_person);
        pthread_t n_thread;
        pthread_create(&n_thread, NULL, search_in_n_zone, &p_person);
        int thread_count = 0;
        pthread_join(h_thread, NULL);
        thread_count++;
        pthread_join(a_thread, NULL);
        thread_count++;
        pthread_join(n_thread, NULL);
        thread_count++;
    }

    return NULL;
}

void *simulate_person_in_game(void *args)
{
    person p_person = *(person *)args;

    bool check_person_type = p_person.fan_type == 'H';
    if (check_person_type)
    {
        pthread_mutex_lock(&goals_lock[1]);
        int index = 1;
        while (goals[index] < p_person.num_goals && goals[index] >= 0)
        {
            pthread_cond_wait(&goals_changed[index], &goals_lock[index]);
        }
        if (goals[index] >= p_person.num_goals)
        {
            printf(ANSI_COLOR_RED "%s got enraged and went to the exit" ANSI_COLOR_RESET "\n", p_person.name);
        }
        pthread_mutex_unlock(&goals_lock[index]);
        index = p_person.id;
        pthread_cond_broadcast(&person_moved[index]);
    }
    else if (p_person.fan_type == 'A')
    {
        pthread_mutex_lock(&goals_lock[0]);
        int index = 0;
        while (goals[index] < p_person.num_goals && goals[index] >= 0)
        {
            pthread_cond_wait(&goals_changed[index], &goals_lock[index]);
        }
        if (goals[index] >= p_person.num_goals)
        {
            printf(ANSI_COLOR_RED "%s got enraged and went to the exit" ANSI_COLOR_RESET "\n", p_person.name);
        }
        pthread_mutex_unlock(&goals_lock[index]);
        index = p_person.id;
        pthread_cond_broadcast(&person_moved[index]);
    }

    return NULL;
}

void *simulate_person_enter_exit(void *args)
{
    person p_person = *(person *)args;
    sleep(p_person.reach_time);
    pthread_t thread;
    printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n", p_person.name);
    pthread_create(&thread, NULL, simulate_person_seat, &p_person);
    pthread_join(thread, NULL);
    int index = p_person.id;
    pthread_mutex_lock(&person_zone_lock[index]);
    bool check_person_id = person_in_zone[p_person.id] - 'D' == 0 || person_in_zone[p_person.id] - 'G' == 0;
    if (check_person_id)
    {
        printf(ANSI_COLOR_BLUE "%s did not find a seat in any of the zones" ANSI_COLOR_RESET "\n", p_person.name);
        pthread_mutex_unlock(&person_zone_lock[index]);
        printf(ANSI_COLOR_MAGENTA "%s left the stadium" ANSI_COLOR_RESET "\n", p_person.name);
        return NULL;
    }
    pthread_mutex_unlock(&person_zone_lock[index]);
    struct timespec ts;
    pthread_create(&thread, NULL, simulate_person_in_game, &p_person);
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec = spectating_time + ts.tv_sec;
    pthread_mutex_lock(&person_zone_lock[index]);
    bool check_time_wait = pthread_cond_timedwait(&person_moved[p_person.id], &person_zone_lock[p_person.id], &ts) == ETIMEDOUT;
    if (check_time_wait)
    {
        int time_spent = spectating_time;
        printf(ANSI_COLOR_YELLOW "%s watched the match for %d seconds and is leaving" ANSI_COLOR_RESET "\n", p_person.name, time_spent);
    }
    pthread_mutex_unlock(&person_zone_lock[index]);
    pthread_mutex_lock(&person_zone_lock[index]);
    if (person_in_zone[p_person.id] == 'H')
    {
        sem_post(&h_zone);
    }
    if (person_in_zone[p_person.id] == 'A')
    {
        sem_post(&a_zone);
    }
    if (person_in_zone[p_person.id] == 'N')
    {
        sem_post(&n_zone);
    }
    person_in_zone[index] = 'G';
    pthread_mutex_unlock(&person_zone_lock[index]);
    int opt = 0;
    printf(ANSI_COLOR_MAGENTA "%s left the stadium" ANSI_COLOR_RESET "\n", p_person.name);
    opt = 1;
    return NULL;
}

void *simulate_team(void *args)
{
    team p_team = *(team *)args;

    for (int i = 0; i < p_team.num_chances; i++)
    {
        float x = (float)rand() / (float)(RAND_MAX / 1.0);
        sleep(p_team.time_from_previous_chance[i]);
        bool check_chance = x < p_team.probability_goal[i];
        int index = p_team.id;
        if (check_chance)
        {
            pthread_mutex_lock(&goals_lock[index]);
            goals[index]++;
            int goal_count = goals[index];
            printf(ANSI_COLOR_CYAN "Team %c has scored goal number %d" ANSI_COLOR_RESET "\n", p_team.team_type, goal_count);
            pthread_mutex_unlock(&goals_lock[index]);
        }
        else
        {
            printf(ANSI_COLOR_CYAN "Team %c missed their chance to score a goal" ANSI_COLOR_RESET "\n", p_team.team_type);
        }
        pthread_cond_broadcast(&goals_changed[index]);
    }

    return NULL;
}

int main()
{

    scanf("%d %d %d", &h_capacity, &a_capacity, &n_capacity);
    int p_size = sizeof(person) * 1;
    scanf("%d", &spectating_time);
    int num_groups, num_in_group;
    scanf("%d", &num_groups);
    people = malloc(p_size);

    for (int i = 1; i <= num_groups; i++)
    {
        int a_type = 0;
        scanf("%d", &num_in_group);
        for (int ii = 0; ii < num_in_group; ii++)
        {
            a_type += 2;
            int pe_size = sizeof(person) * (num_people + 1);
            people = realloc(people, pe_size);
            scanf("%s %c", people[num_people].name, &people[num_people].fan_type);
            scanf("%d %d %d", &people[num_people].reach_time, &people[num_people].patience_time, &people[num_people].num_goals);
            int ctr = i;
            people[num_people].group_no = ctr;
            ctr = num_people;
            people[num_people].id = ctr;
            num_people += 1;
        }
    }
    int int_size = sizeof(int) * 1;
    teams = malloc(sizeof(team) * 2);
    int float_size = sizeof(float) * 1;

    for (int i = 0; i < 2; i++)
    {
        teams[i].num_chances = 0;
        teams[i].id = i;
        teams[i].time_from_previous_chance = malloc(int_size);
        teams[i].probability_goal = malloc(float_size);
        if (i == '1')
        {
            teams[i].team_type = 'A';
        }
        else
        {
            teams[i].team_type = 'H';
        }
    }

    int goal_scoring_chances, prev_a_time = 0, prev_h_time = 0;

    for (int i = 0; i < 2; i++)
    {
        int p_c = 0;
        pthread_mutex_init(&goals_lock[i], NULL);
        p_c += i;
        pthread_cond_init(&goals_changed[i], NULL);
    }

    char inp_team;
    goals[0] = 0;
    scanf("%d\n", &goal_scoring_chances);
    int inp_time;
    goals[1] = 0;

    for (int i = 0; i < goal_scoring_chances; i++)
    {
        scanf("%c", &inp_team);
        if (inp_team == 'H')
        {
            int a_size = sizeof(int) * (teams[0].num_chances + 1);
            teams[0].time_from_previous_chance = realloc(teams[0].time_from_previous_chance, a_size);
            a_size = sizeof(float) * (teams[0].num_chances + 1);
            teams[0].probability_goal = realloc(teams[0].probability_goal, a_size);

            scanf("%d %f\n", &inp_time, &teams[0].probability_goal[teams[0].num_chances]);

            int add_time = inp_time - prev_h_time;
            teams[0].time_from_previous_chance[teams[0].num_chances] = add_time;
            teams[0].num_chances += 1;
            prev_h_time = inp_time;
        }
        else if (inp_team == 'A')
        {
            int a_size = sizeof(int) * (teams[1].num_chances + 1);
            teams[1].time_from_previous_chance = realloc(teams[1].time_from_previous_chance, a_size);
            a_size = sizeof(float) * (teams[1].num_chances + 1);
            teams[1].probability_goal = realloc(teams[1].probability_goal, a_size);

            scanf("%d %f\n", &inp_time, &teams[1].probability_goal[teams[1].num_chances]);

            int add_time = inp_time - prev_a_time;
            teams[1].time_from_previous_chance[teams[1].num_chances] = add_time;
            teams[1].num_chances += 1;
            prev_a_time = inp_time;
        }
    }

    int pp_size = sizeof(pthread_cond_t) * num_people;
    sem_init(&h_zone, 0, h_capacity);
    int c_size = sizeof(char) * num_people;
    sem_init(&a_zone, 0, a_capacity);
    int pm_size = sizeof(pthread_mutex_t) * num_people;
    sem_init(&n_zone, 0, n_capacity);

    person_moved = malloc(pp_size);
    person_in_zone = malloc(c_size);
    person_zone_lock = malloc(pm_size);

    for (int i = 0; i < num_people; i++)
    {
        person_in_zone[i] = 'E';
        pthread_cond_init(&person_moved[i], NULL);
        pthread_mutex_init(&person_zone_lock[i], NULL);
    }

    t_teams = malloc(sizeof(pthread_t) * 2);
    int s_destroy = 0;
    t_people = malloc(sizeof(pthread_t) * num_people);

    for (int i = 0; i < num_people; i++)
    {
        int cnt = i;
        pthread_create(&t_people[i], NULL, simulate_person_enter_exit, &people[i]);
        cnt += pp_size;
    }
    for (int i = 0; i < 2; i++)
    {
        int cnt = i;
        pthread_create(&t_teams[i], NULL, simulate_team, &teams[i]);
        cnt += pp_size;
    }

    for (int i = 0; i < num_people; i++)
    {
        int cnt = i;
        pthread_join(t_people[i], NULL);
        cnt += pp_size;
    }
    for (int i = 0; i < 2; i++)
    {
        int cnt = i;
        pthread_join(t_teams[i], NULL);
        cnt += pp_size;
    }

    goals[0] = -1;
    goals[1] = -1;

    for (int i = 0; i < 2; i++)
    {
        s_destroy += 1;
        pthread_cond_broadcast(&goals_changed[i]);
        s_destroy += 1;
        pthread_mutex_destroy(&goals_lock[i]);
        s_destroy += 1;
        pthread_cond_destroy(&goals_changed[i]);
        s_destroy += 1;
        pthread_cancel(t_teams[i]);
    }
    s_destroy = 0;
    sem_destroy(&h_zone);
    s_destroy += 1;
    sem_destroy(&a_zone);
    s_destroy += 1;
    sem_destroy(&n_zone);
    s_destroy += 1;

    free(person_in_zone);
    s_destroy += 1;
    free(person_zone_lock);
    free(person_moved);
    free(people);

    for (int i = 0; i < 2; i++)
    {
        free(teams[i].time_from_previous_chance);
        free(teams[i].probability_goal);
    }
    free(teams);

    return 0;
}
