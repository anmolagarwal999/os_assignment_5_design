# Question 1

## Running The Code
The steps to run the code are as follows:
1. Run `gcc -g main.c -lpthread` to compile the code.
2. Then run `./a.out` to run the code.
3. Input the test cases in the terminal.
4. You can store the test cases in a file and redirect them to the standard input of a.out as follows: `./a.out < input.txt`. Here, input.txt is the file containing the test cases.

## Logic

We have used threads in this assignment. The input is taken by the ```take_input()``` function and the data is stored in the students, courses and labs structures. Then we create threads for the courses and students and run them. Each course thread then searches for available TAs from the labs of the course and then if it finds the free TAs by looping over the complete set of TAs in a particular lab. It then signals the students waiting for the confirmation that the TA is available and then they try getting a seat for the tutorial. The courses thread starts a tutorial and the students who are enrolled wait for a conditional variable (to prevent deadlocks) to signal the end of the tutorial. The students then choose whether they want to finalise the course or not by a random probability. Once chosen, the students move on to their next preference. The course thread then checks if the courses have more TAs that it can avail, if not we delete the course, then we also check if any of the student have this course as a preference and is yet to take tutorial of the course. If there is not such student, then also we delete the course, just by changing the variable in the struct. The course thread checks if the number of enrolled students is not 0, if 0 the thread is set to wait for a student to avail finish other tutorial and avail the course. If it is not 0, the tutorial is conducted and then the signalling is done the students attending the tutorial that the tutorial is done.

## Files

- `src/main.c`: The main file which contains all the code.

## Assumptions

There are no assumptions made in this assignment (Not clarified on Moodle).
