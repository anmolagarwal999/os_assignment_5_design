# Question 3

## Running The Code
The steps to run the code are as follows:
1. Run `clear && make server && ./server __n__`. This starts a server with __n__ threads.
2. In another terminal, run `clear && make client && ./client` to start the client.
3. Input the test cases in the terminal of the client.

## Logic

We have used threads in this assignment. This allows for different clients to connect to the same server at the same time. The _race_ condition can also be found as we can modify the dictionary because of the multiple threads. To prevent the _race_ condition, _mutex_ locks can be used which makes the modifying operations thread safe as they become atomic. Mutex locks have the ability to change the value only inside the critial section. There is no parallelism implemented, only concurrency. A queue data structure is used to store the client connections that cannot be processed at the moment due to the busy threads in the pool. To avoid busy waiting, conditional variables are used. 

## Files

- `src/all.h` - Header file for all the files.
- `src/client.cpp` - Client file.
- `src/commands.cpp` - Command file.
- `src/commands.h` - Header file for the command file.
- `src/server.cpp` - Server file.
- `Makefile` - Makefile.

## Assumptions

There should not be too many requests at the same time or the server might hang beacuse all the threads would be busy. A lock would then come into place because there are several more requests and still the server is busy.
