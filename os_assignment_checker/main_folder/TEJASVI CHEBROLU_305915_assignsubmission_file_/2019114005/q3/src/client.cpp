#include "all.h"

void check_code(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(EXIT_ONE);
    }
    return;
}

void *perform_connections(void *input_ptr)
{
    int socket_fd;
    string input = *((string *)input_ptr);
    char rbuff[USE_BUFFER_SIZE];
    struct sockaddr_in server_addr;

    check_code((socket_fd = socket(AF_INET, SOCK_STREAM, EXIT_ZERO)), "cant create sock\n");
    int server_address_size = sizeof(server_addr);
    bzero((char *)&server_addr, server_address_size);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(USE_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    check_code(connect(socket_fd, (struct sockaddr *)&server_addr, server_address_size), "cant connect to server\n");
    int c_len = strlen(input.c_str());
    send(socket_fd, input.c_str(), c_len, 0);
    check_code(recv(socket_fd, rbuff, USE_BUFFER_SIZE, 0) - 1, "can't recieve\n");
    int b_size = sizeof(rbuff);
    fputs(rbuff, stdout);
    bzero((char *)&rbuff, b_size);

    close(socket_fd);

    return NULL;
}

int main()
{
    string temp;
    int last_time = 0;
    getline(cin, temp);
    vector<vector<string>> inputs(USE_REQUESTS);
    int m = stoi(temp);
    for (int i = 0; i < m; i++)
    {
        string temp, time;
        getline(cin, temp);
        int t_l = temp.length() - EXIT_ONE;
        time = temp.substr(0, temp.find(' '));
        string remaining = temp.substr(temp.find(' ') + EXIT_ONE, t_l);
        int time_int = stoi(time);
        inputs[time_int].push_back(remaining);
        last_time = max(last_time, stoi(time));
    }

    for (int i = 0; i <= last_time; i++)
    {
        int i_len = inputs[i].size();
        for (int j = 0; j < i_len; j++)
        {
            pthread_t thread;
            pthread_create(&thread, NULL, perform_connections, &inputs[i][j]);
        }
        sleep(EXIT_ONE);
    }

    return EXIT_ZERO;
}
