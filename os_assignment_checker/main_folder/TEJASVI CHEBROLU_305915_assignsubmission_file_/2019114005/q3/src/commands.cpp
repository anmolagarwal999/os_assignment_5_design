#include "commands.h"
#define INF 1e9
#include "all.h"

string perform_insert(char *args[USE_ARGS])
{
    bool arg_nums = !(args[0] && args[1] && args[2]);
    if (arg_nums)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    int dic_len = dict[key].length();
    string value = args[2];

    if (dic_len)
        return "Key already exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return "Insertion Successful\n";
}

string test()
{
    return "testing 123!\n";
}

string perform_fetch(char *args[USE_ARGS])
{
    bool arg_nums = !(args[0] && args[1]);
    if (arg_nums)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool k_len_check = !dict[key].length();
    if (k_len_check)
        return "Key does not exist\n";
    else
        return dict[key] + "\n";
}

string perform_update(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1] && args[2]);
    if (arg_len)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool n_key_len = !dict[key].length();
    string value = args[2];

    if (n_key_len)
        return "Key dones not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    key_len = !key_len;
    dict[key] = value;
    pthread_mutex_unlock(&my_dict_mutex[key]);

    return value + "\n";
}

string perform_concat(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1] && args[2]);
    if (arg_len)
        return "Invalid #args\n";

    int key1 = atoi(args[1]);
    bool key1_len = !(0 <= key1 && key1 <= 100);
    if (key1_len)
        return "Invalid key1\n";
    int key2 = atoi(args[2]);
    bool key2_len = !(0 <= key2 && key2 <= 100);
    if (key2_len)
        return "Invalid key2\n";

    string temp1 = dict[key1];
    string temp2 = dict[key2];

    bool t_lens = !(temp1.length() and temp2.length());
    if (t_lens)
        return "Concat failed as at least one of the keys does not exist\n";

    pthread_mutex_lock(&my_dict_mutex[key1]);
    int index = key1;
    pthread_mutex_lock(&my_dict_mutex[key2]);
    dict[index] = temp1 + temp2;
    index = key2;
    dict[index] = temp2 + temp1;
    pthread_mutex_unlock(&my_dict_mutex[key2]);
    key1_len = !key1_len;
    pthread_mutex_unlock(&my_dict_mutex[key1]);

    return dict[key2] + "\n";
}

string perform_delete(char *args[USE_ARGS])
{
    bool arg_len = !(args[0] && args[1]);
    if (arg_len)
        return "Invalid #args\n";

    int key = atoi(args[1]);
    bool key_len = !(0 <= key && key <= 100);
    if (key_len)
        return "Invalid key\n";

    bool n_key_len = !dict[key].length();
    if (n_key_len)
        return "No such key exists\n";

    pthread_mutex_lock(&my_dict_mutex[key]);
    key_len = !key_len;
    dict[key] = "";
    pthread_mutex_unlock(&my_dict_mutex[key]);
    return "Deletion successful\n";
}

string perform_commands(char *user_input)
{

    char **args = (char **)malloc(USE_ARGS * sizeof(char *));
    int i = 0;

    args[i] = strtok(user_input, " \t");
    string response = "\n";
    while (args[i])
        args[++i] = strtok(NULL, " \t");

    if (!strncmp(args[0], "test", 4))
        return test();
    else if (!strncmp(args[0], "insert", 7))
        return perform_insert(args);
    else if (!strncmp(args[0], "fetch", 6))
        return perform_fetch(args);
    else if (!strncmp(args[0], "delete", 6))
        return perform_delete(args);
    else if (!strncmp(args[0], "update", 6))
        return perform_update(args);
    else if (!strncmp(args[0], "concat", 6))
        return perform_concat(args);
    return response;
}
