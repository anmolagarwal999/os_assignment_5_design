#ifndef _ALL_
#define _ALL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define USE_PORT 1337
#define USE_BUFFER_SIZE 256
#define USE_BACKLOG 5
#define EXIT_ONE 1
#define EXIT_ZERO 0
#define USE_REQUESTS 256
#define USE_ARGS 256

using namespace std;
#define USE_DICT_SIZE 101
extern pthread_mutex_t my_dict_mutex[USE_DICT_SIZE];
extern vector<string> dict;

#endif
