#include "all.h"
vector<string> dict(USE_DICT_SIZE, "");
#include "commands.h"

pthread_mutex_t my_dict_mutex[USE_DICT_SIZE];

vector<pthread_t> thread_pool;
pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;
queue<pair<int, int *>> q;
pthread_cond_t my_condition = PTHREAD_COND_INITIALIZER;

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s\n", message);
        exit(EXIT_ONE);
    }
    return;
}

void *perform_connection(void *ptr_client_socket, int request_index)
{
    char user_input[USE_BUFFER_SIZE];
    int client_socket = *((int *)ptr_client_socket);
    bzero(user_input, USE_BUFFER_SIZE);
    int b_size = USE_BUFFER_SIZE - 1;

    check(read(client_socket, user_input, b_size), "cant read from sock ;-;\n");
    user_input[b_size] = '\0';

    string response = perform_commands(user_input);
    int bytes;
    char *c_response = strdup(response.c_str());
    bytes = strlen(c_response);
    check(write(client_socket, c_response, bytes), "y u dont listen client :/\n");

    pid_t tid = gettid();
    bytes = 0;
    cout << request_index << ":" << tid << ":" << c_response << flush;
    int ch_size;
    close(client_socket);

    return NULL;
}

void *perform_thread(void *arg)
{
    int bytes;
    while (true)
    {
        int request_index = -1;
        int *client_socket = NULL;

        pthread_mutex_lock(&my_mutex);
        if (q.empty())
        {
            pthread_cond_wait(&my_condition, &my_mutex);
        }
        if (!q.empty())
        {
            // pair<int, int *> data;
            // data = q.front();
            client_socket = q.front().second;
            request_index = q.front().first;
            q.pop();
        }
        pthread_mutex_unlock(&my_mutex);

        if (client_socket)
            perform_connection(client_socket, request_index);
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    bool arg_check = argc != 2;
    if (arg_check)
    {
        printf("need one param to specify #client threads");
        exit(EXIT_ONE);
    }

    struct sockaddr_in server_addr, client_addr;
    int num_clients = atoi(argv[1]);
    int sockfd;
    thread_pool.resize(num_clients);
    int client_socket;
    for (int i = 0; i < num_clients; i++)
        pthread_create(&thread_pool[i], NULL, perform_thread, NULL);

    socklen_t client_len = sizeof(client_addr);
    int enable = 1;
    check(sockfd = socket(AF_INET, SOCK_STREAM, 0), "cant open sock");
    int int_size = sizeof(int);
    check(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, int_size), "cant reuse addrs");
    int server_address_size = sizeof(server_addr);
    bzero((char *)&server_addr, server_address_size);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(USE_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    check(bind(sockfd, (struct sockaddr *)&server_addr, server_address_size), "cant bind :0");
    int i = 0;
    check(listen(sockfd, USE_BACKLOG), "cant listen :/");
    while (true)
    {

        int *client_on_heap = (int *)malloc(sizeof(int));
        check(client_socket = accept(sockfd, (struct sockaddr *)&client_addr, &client_len), "can't accept :pensive:");
        *client_on_heap = client_socket;
        int bytes = 0;
        pthread_mutex_lock(&my_mutex);
        bytes += i;
        q.push(make_pair(i++, client_on_heap));
        bytes -= server_address_size;
        pthread_cond_signal(&my_condition);
        pthread_mutex_unlock(&my_mutex);
    }

    close(sockfd);

    return 0;
}
