#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define MAX 50

int num_courses;
struct courses
{
    char name[100];    // course name
    int course_id;      // course id
    float interest;     // interest quotient
    int max_seats;      // maximum number of seats that can be allocated for a tutorial
    int no_of_labs;     // number of labs from which the course can select a TA
    int lab_list[MAX]; // list of labs from which the course can select a TA
    int available;      // 0 implies course is allocating, 1 implies it is not
    int exited;
};
typedef struct courses Courses;

int num_students;
struct students
{
    int student_id;        // student id
    int registration_time; // time at which the student registered
    float calibre;         // student calibre
    int priority[3];       // the courses of the student in priority order
    int curr_priority;     // current priority of student
    int available;
    int assigned_course;
    int exited;
    int in_tutorial;
    int waiting; 
};
typedef struct students Students;

int num_labs;
struct labs
{
    char name_lab[100];         // lab name
    int lab_id;                  // lab id
    int num_TAs;                 // number of TAs in the lab
    int max_num_times_allocated; // number of times TA can be allocated
    int exited;                  // 0 means lab is in simulation, 1 means it has exited
    int exit_flag;              
};
typedef struct labs Labs;

struct TAs
{
    int TA_id;
    int lab_id;
    int num_labs_done;
    int current_course;
    int available;
};
typedef struct TAs TA;

// global variables for each entity
Courses courses[MAX];
Students students_array[MAX];
Labs labs_array[MAX];
TA TAs_array[MAX][MAX];

// functions
void *start_student(void *arg);
void *start_course(void *arg);

// Locks and cv's
pthread_mutex_t student_mutex[MAX];
pthread_mutex_t TA_mutex[MAX][MAX];
pthread_cond_t cond_var[MAX];

#define RED     "\x1B[31m"
#define GREEN   "\x1B[32m"
#define YELLOW  "\x1B[33m"
#define BLUE    "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN    "\x1B[36m"
#define RESET   "\x1B[0m"

// Student thread
void *start_student(void *arg)
{
    Students *student = (Students *)arg;
    sleep(student->registration_time); // sleep until student fills form
    printf(CYAN "Student %d has filled in preferences for course registration\n", student->student_id);

    pthread_mutex_lock(student_mutex + student->student_id);
    // setting student as available for allocation now
    student->available = 1;
    pthread_mutex_unlock(student_mutex + student->student_id);

    while (student->curr_priority < 3)
    {
        pthread_mutex_lock(student_mutex + student->student_id);
        if (!courses[student->priority[student->curr_priority]].available)
        {
            // wait until course thread signals that it has finished tutorial or withdrawn
            pthread_cond_wait(cond_var + student->student_id, student_mutex + student->student_id);
        }
        if (courses[student->priority[student->curr_priority]].available && student->assigned_course != student->priority[student->curr_priority])
        {
            if (student->curr_priority < 2)
            {
                
                student->available = 1;
                student->curr_priority++;
                printf(CYAN "Student %d has changed his current preference from %s (priority %d) to %s (priority %d)\n", student->student_id, courses[student->priority[student->curr_priority-1]].name, student->curr_priority, courses[student->priority[student->curr_priority]].name, student->curr_priority + 1);
                pthread_mutex_unlock(&student_mutex[student->student_id]);
            }
            else if(student->curr_priority == 2)
            {
                student->available = 0;
                student->exited = 1;
                printf(RED "Student %d couldn’t get any of his preferred courses\n", student->student_id);
                pthread_mutex_unlock(&student_mutex[student->student_id]);
                return NULL;
            }
        }
        else
        {
            float prob = student->calibre * courses[student->priority[student->curr_priority]].interest;
            // student takes the course permanantly
            if (((float)(rand() % 10000) / 10000.0) < prob)
            {
                printf(RED "Student %d has selected the course %s permanently\n", student->student_id, courses[student->priority[student->curr_priority]].name);
                pthread_mutex_unlock(student_mutex + student->student_id);
                break;
            }
            else
            // student withdraws from the course
            {
                printf(CYAN "Student %d has withdrawn from course %s\n", student->student_id, courses[student->priority[student->curr_priority]].name);
                if (student->curr_priority < 2)
                {
                    student->available = 1;
                    student->curr_priority++;
                    student->assigned_course = -1;
                    printf(CYAN "Student %d has changed his current preference from %s (priority %d) to %s (priority %d)\n", student->student_id, courses[student->priority[student->curr_priority-1]].name, student->curr_priority, courses[student->priority[student->curr_priority]].name, student->curr_priority + 1);
                    pthread_mutex_unlock(&student_mutex[student->student_id]);
                }
                else if(student->curr_priority == 2)
                {
                    student->available = 0;
                    student->exited = 1;
                    printf(RED "Student %d couldn’t get any of his preferred courses\n", student->student_id);
                    pthread_mutex_unlock(&student_mutex[student->student_id]);
                    return NULL;
                }
            }
        }
    }
    student->exited = 1;
    student->available = 0;
    return NULL;
}

void *start_course(void *arg)
{
    Courses *course = (Courses *)arg;
    sleep(1); // because I want student threads to start first
    while (!course->exited)
    {
        // iterating through all the labs that the course can choose a TA from
        for (int i = 0; i < course->no_of_labs; i++)
        {
            // iterating through all the TA's that each lab has
            for (int j = 0; j < labs_array[course->lab_list[i]].num_TAs; j++)
            {
                if (TAs_array[course->lab_list[i]][j].available)
                {
                    if(TAs_array[course->lab_list[i]][j].num_labs_done < labs_array[course->lab_list[i]].max_num_times_allocated)
                    {
                        // Now, this TA is with this course and should not be taken by anyother course until he finishes tutorial
                        pthread_mutex_lock(&TA_mutex[course->lab_list[i]][j]);
                        TAs_array[course->lab_list[i]][j].available = 0;
                        TAs_array[course->lab_list[i]][j].num_labs_done++;
                        printf(GREEN "TA %d from lab %s has been allocated to course %s for his %dth TA ship.\n", j, labs_array[course->lab_list[i]].name_lab, course->name, TAs_array[course->lab_list[i]][j].num_labs_done);
                        int num_seats = rand() % course->max_seats + 1;
                        printf(YELLOW "Course %s has been allocated %d seats\n", course->name, num_seats);

                        int student_queue[MAX];
                        int num_students_allocated = 0;
                        int seats = num_seats;
                        int done = 0;
                        int beg = 0;

                        // Wait until atleast one student is ready to attend lab
                        while (!num_students_allocated)
                        {
                            for (int i = 0; i < num_students; i++)
                            {
                                if (students_array[i].available && !students_array[i].exited && students_array[i].assigned_course == -1)
                                {
                                    if(num_students_allocated < num_seats && students_array[i].priority[students_array[i].curr_priority] == course->course_id)
                                    {
                                        printf(CYAN "Student %d has been allocated a seat in course %s\n" , i, course->name);
                                        pthread_mutex_lock(student_mutex + i);
                                        seats--;
                                        num_students_allocated++;
                                        student_queue[beg++] = i;
                                        students_array[i].assigned_course = course->course_id;
                                    }
                                }
                            }
                            done = 1;
                            sleep(1);
                        }

                        printf(YELLOW "Tutorial has started for course %s with %d seats filled out of %d\n", course->name, num_students_allocated, num_seats);
                        sleep(1); // sleep for tutorial time
                        printf(YELLOW "TA %d from lab %s has completed the tutorial for course %s\n", j, labs_array[course->lab_list[i]].name_lab, course->name);

                        for (int i = 0; i < beg; i++)
                        {
                            students_array[student_queue[i]].available = 0;
                            pthread_mutex_unlock(&student_mutex[student_queue[i]]);
                            pthread_cond_signal(cond_var + student_queue[i]);
                        }
                        labs_array[course->lab_list[i]].exit_flag = 1;
                        for (int j = 0; j < labs_array[course->lab_list[i]].num_TAs; j++)
                        {
                            if (TAs_array[course->lab_list[i]][j].num_labs_done < labs_array[course->lab_list[i]].max_num_times_allocated)
                            {
                                labs_array[course->lab_list[i]].exit_flag = 0;
                                break;
                            }
                        }
                        TAs_array[course->lab_list[i]][j].available = 1;
                        pthread_mutex_unlock(&TA_mutex[course->lab_list[i]][j]);
                        if (labs_array[course->lab_list[i]].exit_flag && !labs_array[course->lab_list[i]].exited)
                        {
                            printf(GREEN "Lab %s no longer has students available for TA ship.\n", labs_array[course->lab_list[i]].name_lab);
                            labs_array[course->lab_list[i]].exited = 1;
                        }
                    }
                }
            }
        }
        course->exited = 1;
        for (int i = 0; i < course->no_of_labs; i++)
        {
            for (int j = 0; j < labs_array[course->lab_list[i]].num_TAs; j++)
            {
                if (TAs_array[course->lab_list[i]][j].num_labs_done < labs_array[course->lab_list[i]].max_num_times_allocated)
                {
                    course->exited = 0;
                    break;
                }
            }
        }
    }
    course->available = 1; // coz I use this to wait for students, I cant use exited coz it does not have a lock around it
    printf(RED "Course %s does not have any TA mentors eligible and is removed from course offerings\n", course->name);
    for (int i = 0; i < num_students; i++)
    {
        // signal to students who are currently waiting for this course
        if (students_array[i].priority[students_array[i].curr_priority] == course->course_id)
        {
            pthread_cond_signal(cond_var + i);
        }
    }
    return NULL;
}

void mutex_init()
{
    for (int i = 0; i < num_labs; i++)
    {
        for (int j = 0; j < labs_array[i].num_TAs; j++)
        {
            if (pthread_mutex_init(&TA_mutex[i][j], NULL) != 0)
            {
                perror("mutex init failed");
            }
        }
    }
    for (int i = 0; i < num_students; i++)
    {
        if (pthread_mutex_init(&student_mutex[i], NULL) != 0)
        {
            perror("mutex init failed");
        }
    }
    for (int i = 0; i < num_students; i++)
    {
        if (pthread_cond_init(cond_var + i, NULL) != 0)
        {
            perror("Could not initialise conditional variable");
            return;
        }
    }
}

int main()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    // initialising mutexes and condition variables
    mutex_init();

    // Taking input
    for (int i = 0; i < num_courses; i++)
    {
        courses[i].course_id = i;
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].max_seats, &courses[i].no_of_labs);
        for (int j = 0; j < courses[i].no_of_labs; j++)
        {
            scanf("%d", &courses[i].lab_list[j]);
            courses[i].exited = 0;
        }
        courses[i].available = 0;
    }

    for (int i = 0; i < num_students; i++)
    {
        students_array[i].student_id = i;
        students_array[i].curr_priority = 0;
        students_array[i].assigned_course = -1;
        students_array[i].in_tutorial = 0;
        students_array[i].waiting = 0;
        students_array[i].exited = 0;
        students_array[i].available = 0;
        scanf("%f %d %d %d %d", &students_array[i].calibre, &students_array[i].priority[0], &students_array[i].priority[1], &students_array[i].priority[2], &students_array[i].registration_time);
    }

    for (int i = 0; i < num_labs; i++)
    {
        labs_array[i].lab_id = i;
        labs_array[i].exited = 0;
        scanf("%s %d %d", labs_array[i].name_lab, &labs_array[i].num_TAs, &labs_array[i].max_num_times_allocated);
        for (int j = 0; j < labs_array[i].num_TAs; j++)
        {
            TAs_array[i][j].lab_id = i;
            TAs_array[i][j].TA_id = j;
            TAs_array[i][j].current_course = -1;
            TAs_array[i][j].num_labs_done = 0;
            TAs_array[i][j].available = 1;
        }
    }

    // Creating threads
    pthread_t course_threads[MAX];
    pthread_t student_threads[MAX];
    for (int i = 0; i < num_students; i++)
    {
        pthread_create(&student_threads[i], NULL, start_student, students_array + i);
    }
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&course_threads[i], NULL, start_course, courses + i);
    }

    // Joining threads
    for (int i = 0; i < num_students; i++)
    {
        pthread_join(student_threads[i], NULL);
    }

    // Destroying mutexes and condition variables
    for (int i = 0; i < num_labs; i++)
    {
        for (int j = 0; j < labs_array[i].num_TAs; j++)
        {
            if (pthread_mutex_destroy(&TA_mutex[i][j]) != 0)
            {
                ; // because TA mutex locks might still be locked after student threads exit simulation
            }
        }
    }
    for (int i = 0; i < num_students; i++)
    {
        if (pthread_mutex_destroy(&student_mutex[i]) != 0)
        {
            perror("student mutex destroy failed");
        }
    }
    for (int i = 0; i < num_students; i++)
    {
        if (pthread_cond_destroy(cond_var + i) != 0)
        {
            perror("Could not destroy conditional variable");
        }
    }
}
