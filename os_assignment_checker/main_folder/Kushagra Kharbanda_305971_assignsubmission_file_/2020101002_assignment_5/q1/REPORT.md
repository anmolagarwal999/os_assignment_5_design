# Report for q1

### How to run the code
- To compile the code use the following command `gcc q1.c -lpthread -o q1`
- To run the code use the following command `./q1`

I have used the following threads in my implementation.

## Course Thread
1. Select a TA from one of the labs.
2. Allocate a random number of seats b/w 1 and course_max_slots
3. Take the tutorial. (basically sleep for some time)
4. Broadcast to all the students attending the tutorial that the tutorial is over so that they can carry on.
5. Repeat until TA's are unavailable in which case course will be withdrawn.
6. In case the course is withdrawn, broadcast to all waiting students to move on to nexxt preference.

## Student Thread
1. Sleep until they begin registration.
2. Apply for a course according to priority order.
3. They will wait until a course thread signals the the tutorial has ended for them
4. The course thread will allocate students by going through a list of waiting students. then counduct tutorial and then signal to the student threads to move on
5. After tutorial either the student takes the course permanantly or moves on to his next preference or exits permanantly.

## Labs
I have not implemented labs with threads as I am just treating them as a common resource which the courses will use to appoint TA's.

## Concurrency handling
### Locks
I have used a lock for the following things

1. For each TA so that at one time only one course can have a TA and change his attributes.
2. For each student to avoid multiple courses changing the attributes of a student at the same time.

### Conditional variables

I have used a conditional variable for each student to signal to him that his tutorial is over.

I am implementing case 2 according to moodle post, where I don't start the tutorial until I have at least 1 student to attend it. I do that by making the student wait on a conditional variable.