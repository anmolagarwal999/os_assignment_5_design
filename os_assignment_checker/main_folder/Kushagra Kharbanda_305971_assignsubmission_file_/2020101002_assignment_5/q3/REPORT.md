# Report for q3
- To compile the code use the following commands `g++ server.cpp -lpthread -o server` and `g++ client.cpp -lpthread -o client` for server and client respectively.
- To run the server use `./server <number of worker threads in the thread pool>`
- To run the client use `./client`

## Implementation
### Server
1. I create n worker threads at the start of the program, where n is passed by command line argument.
2. I initialise the server using the appropriate system calls taught in the tutorial.
3. Whenever the server accepts a request I push the client file descriptor into a queue. I do this inside a mutex lock for the queue.
4. Each worker thread waits for some client file descriptor to be pushed onto the queue and then pops it inside of the queue mutex lock and then begins working on it.
5. It reads the string of command sent by the client and does the appropriate work and sends back the date to be printed by the client after a delay of 2 seconds.
6. this process continues on and on.

### Client
1. The client takes input first.
2. It then spawns m threads for each command.
3. Each thread establishes a connection with the server
4. Each thread then sends the command to the server.
5. After the server processes the request, it sends the output back to the client thread.
6. The client thread prints the output and exits.
7. Once all threads finish execution, the client program exits.

## Concurrency handling
### Locks
1. I have used a lock for the queue which stores client file descriptors to ensure that only one worker thread is popping off from it at one time.
2. I have used a lock for each key in the dictionary to ensure that only one worker thread is accessing that key at one time.

## Conditional variables
1. I have used a conditional variable to signal to the waiting worker threads whenever I push a new client file descriptor onto the queue.