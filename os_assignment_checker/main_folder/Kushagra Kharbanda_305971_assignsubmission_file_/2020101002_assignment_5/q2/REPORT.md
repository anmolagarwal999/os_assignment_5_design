# Report for q2

### How to run the code
- To compile the code use the following command `gcc q2.c -lpthread -o q2`
- To run the code use the following command `./q2`

I have used the following threads in my implementation.

### Spectator thread
1. The spectator thread sleeps till time of arrival.
2. Then I use `sem_timedwait` which is a semaphore with an extra functionality that it returns after a specific period of time. So, I use this to exit if the waiting time of spectator exceeds the patience value.
3. If he gets a seat, I randomly allocate him a seat in one of the available zones.
4. Then the spectator starts to watch the match. Now, I need to stop his watching on two conditions - end of spectating time and if he gets enraged on watching his team's poor perfomance.
5. So, I use `pthread_cond_timed_wait` to accomplish both these things
6. After he is done watching, the spectator goes to the gate and waits for his group.

### Goals thread
1. The goals thread sleeps for the time till the next chance.
2. Then it randomly decides to give goal or not on basis of a random number.
3. After any team scores a goal I broadcast to all spectators watching the match that a goal was scored, so that they can check whether it is time for spectator to get enraged and leave.
4. Keep doing this till all goals are done.

### Group thread
This thread just waits for all spectators of the group to exit and come to the gate.

## Concurrency handling
### Locks
1. One lock for each zone to ensure that only one spectator thread is changing attributes of the zone at one time.
2. A lock for Away team's goals and one for Home team's goals to allow for `pthread_cond_timedwait`, so that the spectators watching the match can wait on goals.
3. One lock for each group so that only one spectator changes attributes of the group at one time.

## Cnditional variables
1. A conditional variable for each team's goals to allow goal thread to signal to spectators watching that a goal has been scored.
2. A conditional variable for each group so that group can wait for all spectators to exit and wait at the gate.

### Semaphores
Since H can access seats of H and N zones, N from H,N,A A from only A, I made three semaphores:
1. sem_H_N for Home fans
2. sem_H_N_A for Neutral fans
3. sem_A for Away fans
