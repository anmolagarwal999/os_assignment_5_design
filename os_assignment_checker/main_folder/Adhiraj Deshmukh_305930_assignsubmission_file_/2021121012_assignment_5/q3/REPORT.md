# Question 3: Multithreaded client and server

## How to run the program :

- To compile the program, execute command : `gcc -pthread -o server server_prog.cpp -o client client_sim.cpp`

- To run the program, execute command : `./server [value of number of server threads]` and then `./client`
  
- To run the program with given test case input, execute command : `./client < input.txt`

## Report

- In file `server_prog.cpp` :

  We create and handle server threads. The goal of this program is to create a pool of threads and listen to client requests and do the appropriate job

  To assign request to the pool of server threads we have used a queue, and then we push the requests in the back of the queue and one of the threads and take the request from the top of the queue.

  The dictionary is an array of strings and every id is mutex locked for synchronization.

  The listening and creation of sockets is taken from base code.

- In file `client_sim.cpp` :

  We just create threads corresponding to client requests and simultaneously send request to server port `8001`, and send message as given in the input.