// Normal headers
#include <stdio.h> // fflush(), perror(), sprintf()
#include <stdlib.h> // calloc(), malloc()
#include <string.h> //  strtok(), strstr(), strlen()
#include <time.h>

#include <unistd.h> // system calls: open(), read(), write(), lseek(), gethostname()
#include <fcntl.h> // use of flags O_CREAT, SEEK_SET, etc.

#include <sys/stat.h> // mkdir(), stat(), _exit()
#include <sys/types.h>
#include <sys/wait.h> // wait()
#include <sys/utsname.h> // uname()

#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <dirent.h>
#include <signal.h>
#include <limits.h>

#include <pthread.h>
#include <semaphore.h>

// C++ headers
// #include<bits/stdc++.h>
// using namespace std;

// #define debug(x) cout << #x << " : " << x << endl

// colors
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"