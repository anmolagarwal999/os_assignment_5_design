# Question 1: An alternate course allocation Portal

## How to run the program :

- To compile the program, execute command : `gcc -pthread -o q1 q1.c`

- To run the program, execute command : `./q1`
  
- To run the program with given testcase input, execute command : `./q1 < input.txt`

## Report

- To store the data of all courses, students and labs we have created corresponding structures with their attribute data types to help with the coding.

- We have assumed that max number of entities could be ***CMAX*** which is initialized as 30.

- After that we have created arrays of all types of entities globally having the size ***CMAX***.

- Then we have initialized globally the required semaphores and thread arrays of size ***CMAX***

- In the main function first we have taken the input and then initialized our data corresponding to courses, students and labs.

- After initializing the structures and semaphores, threads corresponding to students is created using `pthread_create()` and called into function `init_student`. 
  
  Threads corresponding to courses is also created the same way but called into function `init_courses`. 
  
  There are no threads corresponding to labs since we don't require them.

- Here the threads students and courses interact with each other in tutorials, and labs act as a resource (semaphores) for conducting the tutorials.

- First we start with execution of student thread in `init_students` :
  
  Here student threads start after some _time_ according to input. The student threads go over their courses according to preference and wait for their seats.

  The resource for course seats is kept as `sem_course_seats[i]` semaphores which correspond to seats available for the thread. The student thread will wait for the seats until it receives a signal for available seat or the signal is broadcasted and flag for the following course `arr_courses[i].removed` is set to true, in which case the thread goes over next preference.

  After a seat is acquired it signals the course to start the tutorial, and then the thread wait for the tutorial to be over, after which the thread selects a random double using `random_double()` function and calculates the probability that the thread will select the current course permanently according to course interest and student's caliber.

  If the student selects the course then execution of thread is over or else it will go to the next course according to preference.

- Now we look at execution of course threads in `init_course` :
  
  Here the course thread is in an infinite while loop, and iterates until it is unable to conduct any tutorials and is removed from course offerings.

  In the loop, first it checks for an available TA resource, the resource `sem_ta[i][j]` has to be mutex locked since many threads can simultaneous look for a TA in the same lab, and thus for synchronization purposes we have to hold the lock until we check TA's availability or use it. If a TA is found we use it for our current course's tutorial, or we finish the execution of current course since no TA is available.

  After that we allocate seats to the students that have current preference as this course, wait for them for the tutorial, conduct the tutorial using semaphore `sem_tutorials_start[i]` and `sem_tutorials_end[i]`, then release them.
  This continues until all TA resources are finished, and the course is removed from the offering by making flag `arr_courses[i].removed` true.
