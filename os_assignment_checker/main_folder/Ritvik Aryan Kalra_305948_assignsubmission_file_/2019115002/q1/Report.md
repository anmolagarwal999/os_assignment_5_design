# Q1 Report

By: Ritvik Aryan Kalra

Roll no. - 2019115002

### Course Func

- Until all of the students have departed the simulation, each course seeks for a TA.
- A *students_finished* variable is maintained to check if all the students have exited
- All the eligible TAs are traversed by the course, if a TA is found then it allocates seats for the tutorial and then finds available students.
- If no TA is available then the course waits for the eligible TAs to get free.
- If all the eligible TAs of a course are not available anymore then the course exits the simulation after removing all the waiting students.

### Student Func

- A student fills his/her preference after the set time
- course is changed if the present preferred code is not available
- If the present course of the person is available then the students waits for to get a slot in the tutorial of the course.
- We move to next preference if while waiting the course is removed
- If the course is not removed then the student chooses the course based on a probability.
- If he/she chooses the course then the student thread is ended, else the students moves to the next preference.
- If till end the student doesnt find their preference then the student exits the simulation
