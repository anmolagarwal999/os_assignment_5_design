# Operating Systems and Networks

### Keshav Bajaj - 2019115010

## To RUN

- `gcc q1.c -o q1 -lpthread && ./q1`
- Enter valid inputs

## Explanation

### General

- Each student and course has a different thread.
- Mutex locks are used to remove race conditions between threads.
- Conditional signals are used to pass information between threads.

### Course Func

- Each course looks for a TA until all the students have exited the simulation
- To check if all the students have exited the a **students_finished** variable is maintained
- Course then traverses through all the eligible TAs, if a TA is found then it allocates seats for the tutorial and then finds available students.
- If no TA is available then the course waits for the eligible TAs to get free.
- If all the eligible TAs of a course are not available anymore then the course exits the simulation after removing all the waiting students.

### Student Func

- A student fills his/her preference after the set time
- If the present course of the person is not available then the student changes his preference
- If the present course of the person is available then the students waits for to get a slot in the tutorial of the course.
- If while waiting the course is removed then the students again moves to his next preference
- If the course is not removed then the student chooses the course based on a probability.
- If he/she chooses the course then the student thread is ended, else the students moves to the next preference.
- If till end the student doesnt find their preference then the student exits the simulation
