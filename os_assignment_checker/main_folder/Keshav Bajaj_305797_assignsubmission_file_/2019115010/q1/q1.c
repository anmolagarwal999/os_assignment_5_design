#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

typedef struct lab
{
    int id, n_tas, max_times;
    char name[20];
    int *taught_courses, *is_free;
    pthread_mutex_t *ta_lock;
    pthread_cond_t *ta_cond;
} lab;

typedef struct course
{
    int id, course_max_slots, n_labs, ta, withdrawn;
    char name[25];
    float interest;
    int *labs;
    pthread_mutex_t course_lock;
} course;

typedef struct student
{
    int id, pref[3], p_pref, is_waiting;
    float reg_time, calibre;
    pthread_mutex_t student_lock;
    pthread_cond_t student_cond;

} student;

struct return_struct
{
    int p_lab;
    int p_ta;
};

struct input
{
    int num;
};

int n_students, students_finished;
student *students;
pthread_t *student_threads;
pthread_mutex_t students_lock;

int n_courses;
course *courses;
pthread_t *course_threads;

int n_labs;
lab *ta_labs;

// aux variables

struct return_struct set_ta(int p_lab, int p_ta, int l, int ta, char *name)
{
    struct return_struct rs;
    rs.p_lab = p_lab;
    rs.p_ta = p_ta;

    if (ta_labs[l].is_free[ta] && ta_labs[l].taught_courses[ta] - ta_labs[l].max_times < 0)
    {
        rs.p_lab = l;
        rs.p_ta = ta;
        ta_labs[l].is_free[ta] = 0;
        ta_labs[l].taught_courses[ta]++;

        printf(MAGENTA "TA %d from lab %s has been allocated to course %s for %d TA ship\n" RESET, ta, ta_labs[l].name, name, ta_labs[l].taught_courses[ta]);
    }

    return rs;
}

void *course_func(void *args)
{
    int id = ((struct input *)args)->num;
    course p_course = courses[id];

    while (1)
    {
        pthread_mutex_lock(&students_lock);
        if (students_finished == n_students)
        {
            pthread_mutex_unlock(&students_lock);
            printf(RED "Course %s stopped as all students exited the simulation.\n" RESET, p_course.name);
            return NULL;
        }
        pthread_mutex_unlock(&students_lock);

        int p_lab, p_ta;
        p_lab = -1;
        p_ta = -1;

        int l, ta;
        for (l = 0; l < p_course.n_labs; l++)
        {
            for (ta = 0; ta < ta_labs[l].n_tas; ta++)
            {
                pthread_mutex_lock(&ta_labs[l].ta_lock[ta]);
                struct return_struct rs = set_ta(p_lab, p_ta, l, ta, p_course.name);
                pthread_mutex_unlock(&ta_labs[l].ta_lock[ta]);
                p_lab = rs.p_lab;
                p_ta = rs.p_ta;
                if (rs.p_ta != -1)
                    break;
            }
            if (p_ta != -1)
                break;
        }

        // if TA not free then wait
        if (p_ta == -1)
        {
            int l, ta;
            for (l = 0; l < p_course.n_labs; l++)
            {
                for (ta = 0; ta < ta_labs[l].n_tas; ta++)
                {
                    int flag = 0;
                    pthread_mutex_lock(&ta_labs[l].ta_lock[ta]);

                    while (ta_labs[l].is_free[ta] == 0 && ta_labs[l].taught_courses[ta] - ta_labs[l].max_times < 0)
                    {
                        pthread_cond_wait(&ta_labs[l].ta_cond[ta], &ta_labs[l].ta_lock[ta]);
                    }

                    struct return_struct rs = set_ta(p_lab, p_ta, l, ta, p_course.name);

                    pthread_mutex_unlock(&ta_labs[l].ta_lock[ta]);

                    p_lab = rs.p_lab;
                    p_ta = rs.p_ta;
                    if (rs.p_ta != -1)
                    {
                        break;
                    }
                }
                if (p_ta != -1)
                {
                    break;
                }
                else
                {
                    // lab 1 has no TAs now
                    printf(GREEN "Lab %s no longer has students available for TA ship\n" RESET, ta_labs[l].name);
                }
            }
        }

        // if still not able to find any TA then end course
        if (p_ta == -1)
        {
            int p_id = p_course.id;
            pthread_mutex_lock(&courses[p_id].course_lock);

            courses[p_id].withdrawn = 1;

            for (int s = 0; s < n_students; s++)
            {
                int flag = 0;
                pthread_mutex_lock(&students[s].student_lock);
                if (students[s].p_pref == p_id)
                {
                    students[s].is_waiting = 0;
                    flag = 1;
                    pthread_cond_signal(&students[s].student_cond);
                }
                pthread_mutex_unlock(&students[s].student_lock);
            }
            printf(RED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" RESET, p_course.name);
            pthread_mutex_unlock(&courses[p_id].course_lock);

            return NULL;
        }

        // TA is now selected
        int total_slots = (rand() % p_course.course_max_slots) + 1;
        // event 7
        printf(BLUE "Course %s has been allocated %d seats\n" RESET, p_course.name, total_slots);
        int *p_students = malloc(sizeof(int) * total_slots);
        int filled_slots = 0, s;

        for (s = 0; s < n_students; s++)
        {
            pthread_mutex_lock(&students[s].student_lock);

            if (students[s].is_waiting)
                if (students[s].p_pref == p_course.id)
                {
                    p_students[filled_slots++] = s;
                    printf(MAGENTA "Student %d has been allocated a seat in course %s\n" RESET, s, p_course.name);
                }

            pthread_mutex_unlock(&students[s].student_lock);

            if (filled_slots >= total_slots)
            {
                break;
            }
        }

        // students have been selected now lets start tut
        printf(CYAN "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, p_course.name, filled_slots, total_slots);

        sleep(3);

        // tut done. now free the TA and all the students
        pthread_mutex_lock(&ta_labs[p_lab].ta_lock[p_ta]);
        ta_labs[p_lab].is_free[p_ta] = 1;
        printf(MAGENTA "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, p_ta, ta_labs[p_lab].name, p_course.name);
        pthread_cond_broadcast(&ta_labs[p_lab].ta_cond[p_ta]);
        pthread_mutex_unlock(&ta_labs[p_lab].ta_lock[p_ta]);

        for (int i = 0; i < filled_slots; i++)
        {
            int id = p_students[i];
            pthread_mutex_lock(&students[id].student_lock);
            students[id].is_waiting = 0;
            pthread_cond_signal(&students[id].student_cond);
            pthread_mutex_unlock(&students[id].student_lock);
        }

        // freeing the allocated temp array
        free(p_students);
    }
}

void *student_func(void *args)
{
    int id = ((struct input *)args)->num;
    student p_student = students[id];

    // wait until registration time
    sleep(p_student.reg_time);
    // event 1
    printf(RESET "Student %d has filled in preferences for course registration\n" RESET, p_student.id);

    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_lock(&students[id].student_lock);
        students[id].p_pref = students[id].pref[i];
        pthread_mutex_unlock(&students[id].student_lock);

        int p_preff = students[id].p_pref;
        pthread_mutex_lock(&courses[p_preff].course_lock);
        int course_withdrawn = courses[p_preff].withdrawn;
        pthread_mutex_unlock(&courses[p_preff].course_lock);

        pthread_mutex_lock(&students[id].student_lock);
        if (!course_withdrawn)
            students[id].is_waiting = 1;
        else
            students[id].is_waiting = 0;

        while (students[id].is_waiting)
        {
            pthread_cond_wait(&students[id].student_cond, &students[id].student_lock);
        }
        pthread_mutex_unlock(&students[id].student_lock);

        pthread_mutex_lock(&courses[students[id].p_pref].course_lock);
        course_withdrawn = courses[students[id].p_pref].withdrawn;
        pthread_mutex_unlock(&courses[students[id].p_pref].course_lock);

        if (!course_withdrawn)
        {
            // decide wether to continue
            float finalising_prob = p_student.calibre * courses[students[id].p_pref].interest;
            float outcome = (float)rand() / (float)(RAND_MAX);

            if (outcome <= finalising_prob)
            {
                // end student thread
                printf(CYAN "Student %d has selected course %s permanently\n" RESET, id, courses[students[id].p_pref].name);
                pthread_mutex_lock(&students[id].student_lock);
                students[id].p_pref = -1;
                pthread_mutex_unlock(&students[id].student_lock);

                pthread_mutex_lock(&students_lock);
                students_finished++;
                pthread_mutex_unlock(&students_lock);

                return NULL;
            }
            // did not want to continue course so move on to next course
            printf("Student %d has withdrawn from course %s\n", id, courses[students[id].p_pref].name);
        }

        // change course
        if (i < 2)
        {
            printf(YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET, id, courses[students[id].pref[i]].name, i, courses[students[id].pref[i + 1]].name, i + 1);
        }
    }

    pthread_mutex_unlock(&students[id].student_lock);
    // event 6
    printf(RED "Student %d couldn’t get any of his preferred courses\n", id);

    pthread_mutex_lock(&students_lock);
    students_finished++;
    pthread_mutex_unlock(&students_lock);

    return NULL;
}

int main()
{
    // take input and initialise stuff
    scanf("%d %d %d", &n_students, &n_labs, &n_courses);

    courses = malloc(sizeof(course) * n_courses);

    // course info
    for (int i = 0; i < n_courses; i++)
    {
        courses[i].id = i;
        int nlabs;
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].course_max_slots, &nlabs);
        courses[i].n_labs = nlabs;
        courses[i].labs = malloc(sizeof(int) * n_labs);
        for (int j = 0; j < n_labs; j++)
            scanf("%d", &courses[i].labs[j]);

        pthread_mutex_init(&courses[i].course_lock, NULL);
        courses[i].ta = -1;
        courses[i].withdrawn = 0;
    }

    students = malloc(sizeof(student) * n_students);
    // student info
    for (int i = 0; i < n_students; i++)
    {
        students[i].id = i;
        int pref1, pref2, pref3;
        scanf("%f %d %d %d %f", &students[i].calibre, &pref1, &pref2, &pref3, &students[i].reg_time);
        students[i].pref[0] = pref1;
        students[i].pref[1] = pref2;
        students[i].pref[2] = pref3;

        pthread_mutex_init(&students[i].student_lock, NULL);
        pthread_cond_init(&students[i].student_cond, NULL);
        students[i].p_pref = -1;
        students[i].is_waiting = 0;
    }

    ta_labs = malloc(sizeof(lab) * n_labs);
    // lab info
    for (int i = 0; i < n_labs; ++i)
    {
        ta_labs[i].id = i;
        scanf("%s %d %d", ta_labs[i].name, &ta_labs[i].n_tas, &ta_labs[i].max_times);

        int n_tas = ta_labs[i].n_tas;
        ta_labs[i].ta_lock = malloc(sizeof(pthread_mutex_t) * n_tas);
        ta_labs[i].taught_courses = malloc(sizeof(int) * n_tas);
        ta_labs[i].ta_cond = malloc(sizeof(pthread_cond_t) * n_tas);
        ta_labs[i].is_free = malloc(sizeof(int) * n_tas);

        // initialising tas
        for (int j = 0; j < n_tas; j++)
        {
            pthread_mutex_init(&ta_labs[i].ta_lock[j], NULL);
            ta_labs[i].taught_courses[j] = 0;
            pthread_cond_init(&ta_labs[i].ta_cond[j], NULL);
            ta_labs[i].is_free[j] = 1;
        }
    }

    pthread_mutex_init(&students_lock, NULL);
    students_finished = 0;

    // allocating space for threads array
    student_threads = malloc(sizeof(pthread_t) * n_students);
    for (int s = 0; s < n_students; s++)
    {
        struct input *temp = (struct input *)malloc(sizeof(struct input));
        temp->num = s;
        pthread_create(&student_threads[s], NULL, student_func, (void *)temp);
    }

    course_threads = malloc(sizeof(pthread_t) * n_courses);
    for (int c = 0; c < n_courses; c++)
    {
        struct input *temp = (struct input *)malloc(sizeof(struct input));
        temp->num = c;
        pthread_create(&course_threads[c], NULL, course_func, (void *)temp);
    }
    for (int s = 0; s < n_students; s++)
    {
        pthread_join(student_threads[s], NULL);
        printf(RED "Student %d exited from simulation\n" RESET, s);
    }
    for (int c = 0; c < n_courses; c++)
    {
        pthread_join(course_threads[c], NULL);
    }

    return 0;
}