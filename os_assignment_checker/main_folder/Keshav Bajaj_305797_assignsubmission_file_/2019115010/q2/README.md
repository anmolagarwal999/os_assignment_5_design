# Operating Systems and Networks

### Keshav Bajaj - 2019115010

## To RUN

- `gcc q2.c -o q2 -lpthread && ./q2`
- Enter valid inputs

## Explanation

### General

- Each student and course has a different thread.
- Mutex locks are used to remove race conditions between threads.
- Conditional signals are used to pass information between threads.

### Person_enter_exit Func

- A person enters the stadium after the set time
- Find a seat for the person in their zones is created
- If there is no seat for the person then cancel the thread and return
- If seat is found then start a thread for the person to watch the match
- Also run the current thread till the viewing time of the person
- Once the person leaves, reduce the semaphore value of the zone the person was in before

### Watch Func

- Watch the match and leave if the opponent team scores more than the person can bear to watch

### Team Func

- Run the thread till the scoring chances of the teams

### Vacancy x Func

- Calculate the time the person has been waiting for the seat
- If the waiting time is more than the time the person can wait then make him leave
- If the person was at the entrance or didn't get any seat from the other zone threads, allocate this zone
- If the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
