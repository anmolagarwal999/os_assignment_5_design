#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <bits/stdc++.h>
#include <sys/types.h>

#define PORT 1337
#define BUFFER_SIZE 256
#define MAX_REQUESTS 256
using namespace std;

void check(int code, const char *message)
{
    if (code < 0)
    {
        printf("%s", message);
        exit(1);
    }
    return;
}

void *handle_connect(void *input_ptr)
{
    int sockfd;
    string input = *((string *)input_ptr);
    char rbuff[BUFFER_SIZE];

    struct sockaddr_in server_addr;

    int check_code = (sockfd = socket(AF_INET, SOCK_STREAM, 0));
    check(check_code, "cant create sock\n");

    bzero((char *)&server_addr, sizeof(server_addr));

    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    check_code = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    check(check_code, "cant connect to server\n");
    send(sockfd, input.c_str(), strlen(input.c_str()), 0);

    check_code = recv(sockfd, rbuff, BUFFER_SIZE, 0) - 1;
    check(check_code, "can't recevie\n");
    fputs(rbuff, stdout);
    bzero((char *)&rbuff, sizeof(rbuff));

    close(sockfd);

    return NULL;
}

int main()
{
    string temp;
    int m;
    getline(cin, temp);
    m = stoi(temp);
    int last_time = 0, i = 0;
    vector<vector<string>> inputs(MAX_REQUESTS);
    for (i = 0; i < m; i++)
    {
        string temp;
        string time;
        string remaining;
        getline(cin, temp);
        time = temp.substr(0, temp.find(' '));
        remaining = temp.substr(temp.find(' ') + 1, temp.length() - 1);

        int time_int = stoi(time);
        inputs[time_int].push_back(remaining);
        last_time = max(last_time, stoi(time));
    }

    for (i = 0; i <= last_time; i++)
    {
        vector<pthread_t> thread_pool;
        thread_pool.resize(inputs[i].size());

        for (int j = 0; j < inputs[i].size(); j++)
            pthread_create(&thread_pool[j], NULL, handle_connect, &inputs[i][j]);
        sleep(1);
    }

    return 0;
}
