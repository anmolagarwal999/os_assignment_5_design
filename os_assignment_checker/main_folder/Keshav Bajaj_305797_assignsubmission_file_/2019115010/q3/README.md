# Operating Systems and Networks

### Keshav Bajaj - 2019115010

## To RUN

- `gcc server.cpp -o server -lpthread && ./server <NUM_OF_THREADS>` to start the server
- `gcc client.cpp -o client -lpthread && ./client` to start the client

## Explanation

- Threads are used to connect multiple clients at the same time.
- Mutex locks are used to remove race conditions between threads aka different clients.
- If all the threads are busy then the client requests are stored in a queue.
