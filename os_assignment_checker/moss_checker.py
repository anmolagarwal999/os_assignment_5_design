import os
import zipfile
import tarfile
import csv
from functools import reduce

def error_copy(filepath, OUTPUT_FOLDER = "error_files"):
	if not os.path.exists(OUTPUT_FOLDER):
		os.makedirs(OUTPUT_FOLDER)

	worked = os.system("cp -r '"+filepath+"' "+OUTPUT_FOLDER+"/")
	if worked!=0:
		print("ERROR COPYING FOR "+filepath)

def moss_check(dir, EVAL_FILETYPE = '.c', OUTPUT_CSV = 'results.csv', OUTPUT_FOLDER = "concat_files_2"):

	results = []

	## CHANGE ACCORDING TO CSV FILE HEADERS NEEDED AND NUMBER OF QUESTIONS
	result = { 'NAME': 'NAME', 'ROLL NO': 'ROLL NO'}

	csv_f = open(OUTPUT_CSV,'a')
	csv_w = csv.DictWriter(csv_f,result.keys())
	csv_w.writerow(result)

	student_dirs = os.listdir(dir)
	for student_dir in student_dirs:
		result = {}

		student_name = student_dir.partition('_')[0]
		result['NAME'] = student_name

		dir_path = os.path.join(dir,student_dir)
		submitted_files = os.listdir(dir_path)

		FILE_EXT = ""
		# Checks extension for submitted file viz zip
		for file in submitted_files:

			file_components = file.split('.', 1)
			if len(file_components) < 2:
				continue
			comp_file_name, comp_file_extension = file_components

			if comp_file_extension == "zip":
				FILE_EXT = "zip"
			elif comp_file_extension == "tar.gz" or comp_file_extension == "tar.xz" or comp_file_extension == "tar" or comp_file_extension == "gz":
				FILE_EXT = "tar"
			
			if FILE_EXT != "":
				comp_file = file
				break

		if FILE_EXT == "":
			print(student_name, comp_file_extension ,"SUBMISSION NOT IN DESIRED COMPRESSION FORMAT")
			error_copy(dir_path)
			continue

		comp_file_path = os.path.join(dir_path, comp_file)
		result['ROLL NO'] = comp_file_name

		# try:
		# 	if FILE_EXT == 'zip':
		# 		with zipfile.ZipFile(comp_file_path,"r") as zip_ref:
		# 			zip_ref.extractall(dir_path)
		# 	elif FILE_EXT == 'tar':
		# 		with tarfile.open(comp_file_path) as tar_ref:
		# 			tar_ref.extractall(dir_path)
		# except:
		# 	print(student_name, comp_file_extension, "ERROR IN EXTRACTION2")
		# 	error_copy(dir_path)
		# 	continue

		extracted_content = os.listdir(dir_path)
		extracted_content.remove(comp_file)

		# Checks if single folder is present
		if len(extracted_content) == 1:
			assign_folder = extracted_content[0]
			assign_folder_path = os.path.join(dir_path,assign_folder)
			assign_files = os.listdir(assign_folder_path)
		elif comp_file_name in extracted_content:
			assign_folder = comp_file_name
			assign_folder_path = os.path.join(dir_path,assign_folder)
			assign_files = os.listdir(assign_folder_path)
		else:
			assign_folder = student_dir
			assign_folder_path = dir_path
			assign_files = extracted_content

		all_files = []
		for root, dirs, files in os.walk(assign_folder_path):
			for file in files:
				if file.endswith(".c"):
					all_files.append(os.path.join(root, file))

		if not os.path.exists(OUTPUT_FOLDER):
			os.makedirs(OUTPUT_FOLDER)

		with open(f"{OUTPUT_FOLDER}/{student_name}_{assign_folder}.c", "w") as outfile:
			for filename in all_files:
				try:
					with open(filename) as infile:
						contents = infile.read()
						outfile.write(contents)
				except:
					print(student_name, "ERROR ASSEMBLING FILE ", filename)
					error_copy(dir_path)
					continue

		results.append(result)
		csv_w.writerow(result)

	csv_f.close()
	return results


if __name__ == '__main__':
	# dir = 'M21CS3.301-Assignment-3 Extending the shell-29989'
	# results = moss_check(dir)
	results = moss_check("mailed-a3")
	# print(results)