
//###########FILE CHANGE ./main_folder/PAMULAPATI PALLAVI_305983_assignsubmission_file_/2020101095_Assignment5/q1/q1.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>

#define RED "\033[1;91m"
#define GREEN "\033[1;92m"
#define YELLOW "\033[1;93m"
#define BLUE "\033[1;94m"
#define MAGENTA "\033[1;95m"
#define CYAN "\033[1;96m"
#define NORMAL "\033[0m"

#define TA_NUM_MAX 50
#define LAB_NUM_MAX 50


typedef struct student{
    int stud_id;
    int prf1;
    int prf2;
    int prf3;
    int curr_prf;
    int form_filling_time;
    char prf1_name[20];
    char prf2_name[20];
    char prf3_name[20];
    char curr_name[20];
    double calibre;
    double curr_intrst;
    double prf1_intrst;
    double prf2_intrst;
    double prf3_intrst;
}Student;

typedef struct course{
    char Name[20];      // name of the course 
    double intrest_q;    // intrest in the course 
    int max_slots;      // max slots a ta can allocate
    int num_labs;       // no.of labs for the course
    int labs_list[50];  // indices of the possible labs
    pthread_mutex_t c_lock; 
} Course;

typedef struct lab{
    int lab_idx;        // lab index 
    char Name[20];      // name of the lab
    int TA_no;      // no.of ta's in the lab
    int max_times;      // maximum no.of times a ta can mentor
    int count[TA_NUM_MAX];
    pthread_mutex_t lab_lock;
}Lab;

int available_seats[50];        // if there are 50 courses this array maintains the number of seats available in that course.
// available seats = -1 => course is deleted
pthread_mutex_t seats_lock = PTHREAD_MUTEX_INITIALIZER;

Lab Labs[LAB_NUM_MAX];

void* course_func(void* arg)
{
    // here we check whether ta is available in the labs under the course to conduct the tut 
    // and if the ta is available we conduct the tut and allocate seats 
    // if all the ta's of the labs are left then the course is deleted.   
}



void* student_func(void* arg)
{
    sleep(((Student *)arg)->form_filling_time);
    printf(CYAN"Student %d has filled in preferences for course registration\n"NORMAL,((Student *)arg)->stud_id);
    if((((Student *)arg)->curr_prf == ((Student *)arg)->prf3) && available_seats[((Student *)arg)->prf3] == -1)
    {
        printf(RED"Student %d couldn’t get any of his preferred courses\n", ((Student *)arg)->stud_id);
        return NULL;
    }

    while((((Student *)arg)->curr_prf != ((Student *)arg)->prf3) || available_seats[((Student *)arg)->prf3] != -1)
    {
        int check = 0;
        // printf("Reached1\n");
        pthread_mutex_lock(&seats_lock);
        if(available_seats[((Student *)arg)->curr_prf] == -1)   // if the course is removed
        {
            if(((Student *)arg)->curr_prf == ((Student *)arg)->prf1)
            {
                ((Student *)arg)->curr_prf = ((Student *)arg)->prf2;
                printf(BLUE"Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n",((Student *)arg)->stud_id,((Student *)arg)->prf1_name,((Student *)arg)->prf2_name);   
            }
            else if(((Student *)arg)->curr_prf == ((Student *)arg)->prf2)
            {
                ((Student *)arg)->curr_prf = ((Student *)arg)->prf3;
                printf(BLUE"Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n",((Student *)arg)->stud_id,((Student *)arg)->prf2_name,((Student *)arg)->prf3_name);
            }
            else 
            {
                printf(RED"Student %d couldn’t get any of his preferred courses\n", ((Student *)arg)->stud_id);
                    break;
            }
        }
        else if(available_seats[((Student *)arg)->curr_prf] > 0) 
        {
            available_seats[((Student *)arg)->curr_prf]--; 
            printf(BLUE"Student %d has been allocated a seat in course %s\n"NORMAL, ((Student *)arg)->stud_id, ((Student *)arg)->curr_name);
            check = 1;
        }
        pthread_mutex_unlock(&seats_lock);

        if(check == 1)
        {
            sleep(3); 
            double probability = ((Student *)arg)->calibre*((Student *)arg)->curr_intrst;
            probability *=100;

            double random_no = rand()%100;
            // printf("Reached rand = %lf and probability = %lf\n", random_no, probability);
            if(random_no > probability)
            {
                printf(YELLOW"Student %d has withdrawn from course %s\n"NORMAL, ((Student *)arg)->stud_id, ((Student *)arg)->curr_name);
                if(((Student *)arg)->curr_prf == ((Student *)arg)->prf1)
                {
                    ((Student *)arg)->curr_prf = ((Student *)arg)->prf2;
                    printf(BLUE"Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n",((Student *)arg)->stud_id,((Student *)arg)->prf1_name,((Student *)arg)->prf2_name);   
                }
                else if(((Student *)arg)->curr_prf == ((Student *)arg)->prf2)
                {
                    ((Student *)arg)->curr_prf = ((Student *)arg)->prf3;
                    printf(BLUE"Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n",((Student *)arg)->stud_id,((Student *)arg)->prf2_name,((Student *)arg)->prf3_name);
                }
                else 
                {
                    printf(RED"Student %d couldn’t get any of his preferred courses\n", ((Student *)arg)->stud_id);
                    break;
                }
            }
            else
            {
                printf(GREEN"Student %d has selected course %s permanently\n"NORMAL, ((Student *)arg)->stud_id, ((Student *)arg)->curr_name);
                break;
            }
        }
    }

    return NULL;
}

int main(void)
{
    srand(time(NULL));
    
    
    
    int num_students, num_labs, num_courses;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    for(int i = 0;i < num_courses;i++)
        available_seats[i] = 0;

    Course courses[num_courses];
    pthread_t course_thr[num_courses];
    for(int i = 0; i < num_courses; i++)
    {
        pthread_mutex_init(&(courses[i].c_lock),NULL);
        scanf("%s %lf %d %d", courses[i].Name, &courses[i].intrest_q, &courses[i].max_slots, &courses[i].num_labs);
        for(int j = 0;j < courses[i].num_labs; j++)
            scanf("%d",&courses[i].labs_list[j]);
    }
    Student students[num_students];
    pthread_t students_thr[num_students];

    for(int i = 0;i < num_students; i++)
    {
        students[i].stud_id = i;
        scanf("%lf %d %d %d %d", &students[i].calibre, &students[i].prf1, &students[i].prf2, &students[i].prf3, &students[i].form_filling_time);
        students[i].curr_prf = students[i].prf1;
        students[i].curr_intrst = courses[students[i].curr_prf].intrest_q;
        students[i].prf1_intrst = courses[students[i].prf1].intrest_q;
        students[i].prf2_intrst = courses[students[i].prf2].intrest_q;
        students[i].prf3_intrst = courses[students[i].prf3].intrest_q;
        strcpy(students[i].curr_name,courses[students[i].curr_prf].Name);
        strcpy(students[i].prf1_name,courses[students[i].prf1].Name);
        strcpy(students[i].prf2_name,courses[students[i].prf2].Name);
        strcpy(students[i].prf3_name,courses[students[i].prf3].Name);
    }

    for(int i = 0;i < num_labs; i++)
    {
        scanf("%s %d %d", Labs[i].Name, &Labs[i].TA_no, &Labs[i].max_times);
        Labs[i].lab_idx = i;
        pthread_mutex_init(&(Labs[i].lab_lock),NULL);
        for(int j = 0;j< Labs[i].TA_no;j++)
        {
            Labs[i].count[j] = 0; // initial no.of times he did ta ship = 0
        }
    }

    for(int i = 0;i < num_courses;i++)
        pthread_create(&course_thr[i], NULL, course_func, &(courses[i]));

    for(int i = 0;i < num_students; i++)
        pthread_create(&students_thr[i],NULL,student_func,&(students[i]));

    for(int i = 0;i < num_students;i++)
        pthread_join(students_thr[i],NULL);
    
    return 0;
}
