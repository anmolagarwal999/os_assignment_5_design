
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/labs.c ####################//

#include "global.h"

void labs_list_init(int n)
{
    // Setup the labs information with the input
    labs_list = (Labs *)shareMem(n * sizeof(Labs));
    for (int i = 0; i < n; i++)
    {
        labs_list[i].id = i;
        scanf("%s %d %d", labs_list[i].name, &labs_list[i].student_TA_number, &labs_list[i].number_of_TAships);
        labs_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(labs_list[i].mutex, NULL);
        labs_list[i].TA_available = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_available[j] = 1;
        }
        labs_list[i].TA_allotments = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_allotments[j] = 0;
        }
    }
    labs_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(labs_list->mutex, NULL);
}
void labs_init(Labs *l)
{
    ; // Do nothing here
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/student.c ####################//

#include "global.h"

void student_list_init(int n)
{
    // Initialize students details with the input and initialize locks
    student_list = (student *)shareMem(n * sizeof(student));
    float calibre;
    int pref1, pref2, pref3;
    int time_to_fill = 0;
    for (int i = 0; i < n; i++)
    {
        student_list[i].id = i;
        scanf("%f %d %d %d %d", &calibre, &pref1, &pref2, &pref3, &time_to_fill);
        student_list[i].calibre = calibre;
        student_list[i].pref_1 = pref1;
        student_list[i].pref_2 = pref2;
        student_list[i].pref_3 = pref3;
        student_list[i].time_to_fill = time_to_fill;
        student_list[i].tut_attendance = -1;
        student_list[i].permanent_selection = -1;
        student_list[i].curr_preference = -1;
        student_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].cond = (pthread_cond_t *)shareMem(sizeof(pthread_cond_t));
        student_list[i].tut_done = 0;
        pthread_mutex_init(student_list[i].mutex, NULL);
        pthread_mutex_init(student_list[i].lock, NULL);
        pthread_cond_init(student_list[i].cond, NULL);
    }
    student_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(student_list_lock, NULL);
}
void allot_preferences(student *s)
{
    // If there is no initial preference we simply give the first choice as the current one
    // Change to the next one is this function is called
    // Show that the student withdrwas the course and switches or leaves the list if the choices are done
    if (s->curr_preference == -1)
    {
        s->curr_preference = s->pref_1;
    }
    else if (s->curr_preference == s->pref_1)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_2;
        printf(BMAG "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_RESET, s->id, course_list[s->pref_1].name, course_list[s->pref_2].name);
    }
    else if (s->curr_preference == s->pref_2)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_3;
        printf(BMAG "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_RESET, s->id, course_list[s->pref_2].name, course_list[s->pref_3].name);
    }
    else
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        printf(BBLK "Student %d couldn’t get any of his preferred courses\n" ANSI_RESET, s->id);
        s->id = -1;
    }
    if (s->id != -1 && course_list[s->curr_preference].id == -1) // If the student is still active but the course he chooses isnt we call the funtion again to either switch or leave
    {
        allot_preferences(s);
    }
}
void student_init(student *s)
{
    //printf("Student %d\n", s->id);
    sleep(s->time_to_fill);
    printf(BGRN "Student %d has filled in preferences for course registration\n" ANSI_RESET, s->id);
    for(int i = 0; i < 4; i++)
    {
        // Allot the student preferences to change the students choice or make him choose
        pthread_mutex_lock(s->mutex);
        allot_preferences(s);
        pthread_mutex_unlock(s->mutex);
        if(s->id == -1)// Leave if the student is invalid
        {
            return;
        }
        pthread_mutex_lock(s->lock);
        while (s->tut_done == 0)
        {
            pthread_cond_wait(s->cond, s->lock);// Wait for the tut to finish or the course to be removed
        }
        pthread_mutex_unlock(s->lock);
        s->tut_done = 0;
        pthread_cond_init(s->cond, NULL);
        if(course_list[s->curr_preference].id == -1)// Continue if the course was invalid
        {
            pthread_mutex_unlock(s->mutex);
            continue;
        }
        // MAke the random choice based on the possibility that we can calculate
        float prob_permanent = s->calibre * course_list[s->curr_preference].interest * 100;
        int random_number = rand() % 100 + 1;
        if (prob_permanent >= (float)random_number)
        {
            printf(BGRN "Student %d has selected the course %s permanently\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
            s->id = -1;
            pthread_mutex_unlock(s->mutex);
            return;
        }
        pthread_mutex_unlock(s->mutex);
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/main.c ####################//

#include "global.h"

extern int num_students, num_courses, num_labs;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course_list_init(num_courses);
    student_list_init(num_students);
    labs_list_init(num_labs);
    pthread_t *student_threads = (pthread_t *)shareMem(num_students * sizeof(pthread_t));
    for (int i = 0; i < num_students; i++)
    {
        pthread_create(&student_threads[i], NULL, (void *)student_init, &student_list[i]);
    }
    pthread_t *course_threads = (pthread_t *)shareMem(num_courses * sizeof(pthread_t));
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&course_threads[i], NULL, (void *)course_init, &course_list[i]);
    }
    pthread_t *labs_threads = (pthread_t *)shareMem(num_labs * sizeof(pthread_t));
    for (int i = 0; i < num_labs; i++)
    {
        pthread_create(&labs_threads[i], NULL, (void *)labs_init, &labs_list[i]);
    }
    for (int i = 0; i < num_students; i++) {
        pthread_join(student_threads[i], NULL);
    }
    for (int i = 0; i < num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }
    for (int i = 0; i < num_labs; i++) {
        pthread_join(labs_threads[i], NULL);
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/student.h ####################//

#ifndef STUDENT_H
#define STUDENT_H

typedef struct
{
    int id;
    float calibre;
    int pref_1;
    int pref_2;
    int pref_3;
    int permanent_selection;
    int curr_preference;
    int tut_attendance;
    int time_to_fill;
    pthread_mutex_t *mutex;
    pthread_mutex_t *lock;
    pthread_cond_t *cond;
    int tut_done;
} student;

student *student_list;
pthread_mutex_t *student_list_lock;

void student_list_init(int n);
void student_init(student *s);
void allot_preferences(student *s);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/course.c ####################//

#include "global.h"

void course_list_init(int n)
{
    // We initialize the course struct here, we initialize the values of a course with the input and also initialize the locks
    course_list = (course *)shareMem(n * sizeof(course));
    for (int i = 0; i < n; i++)
    {
        course_list[i].id = i;
        // Get course details
        scanf("%s %f %d %d", course_list[i].name, &course_list[i].interest, &course_list[i].maximum_slots, &course_list[i].number_of_labs);
        course_list[i].labs_accepted = (int *)shareMem(course_list[i].number_of_labs * sizeof(int));
        for (int j = 0; j < course_list[i].number_of_labs; j++)
        {
            // Get the labs from which the course can take TAs
            scanf("%d", &course_list[i].labs_accepted[j]);
        }
        course_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(course_list[i].mutex, NULL);
    }
    course_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(course_list_lock, NULL);
}
int allot_TAs(course *c)
{
    bool remove_course = true;
    for (int i = 0; i < c->number_of_labs; i++)
    {
        // We first iterate over all the labs from which TAs can be chosen
        pthread_mutex_lock(labs_list[c->labs_accepted[i]].mutex);
        if (labs_list[c->labs_accepted[i]].id == -1)
        {
            pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
            continue;
        }
        bool lab_no_slots = true;
        for (int j = 0; j < labs_list[c->labs_accepted[i]].student_TA_number; j++)
        {
            if (labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                remove_course = false;
                lab_no_slots = false;
            }
            // Choose a TA if the TA is not exhausted and he is available
            if (labs_list[c->labs_accepted[i]].TA_available[j] == 1 && labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                // Update TA stats and print details
                labs_list[c->labs_accepted[i]].TA_available[j] = 0;
                labs_list[c->labs_accepted[i]].TA_allotments[j]++;
                c->TA_index = j;
                printf(BYEL "TA %d from lab %s allocated to course %s for his %d" ANSI_RESET, j, labs_list[c->labs_accepted[i]].name, c->name, labs_list[c->labs_accepted[i]].TA_allotments[j]);
                switch (labs_list[c->labs_accepted[i]].TA_allotments[j] % 10)
                {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                }
                printf(BYEL " TAship\n" ANSI_RESET);
                pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
                // Return the lab from which the TA is alloted
                return labs_list[c->labs_accepted[i]].id;
            }
        }
        // If there are no slots in the Lab it indicates that all the TAs are exhausted so it is no longer possible to use this lab
        // Indicate this lab is invalid and print an error
        if (lab_no_slots)
        {
            labs_list[c->labs_accepted[i]].id = -1;
            printf(BBLK "Lab %s no longer has students available for TA ship\n" ANSI_RESET, labs_list[c->labs_accepted[i]].name);
        }
        pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
    }
    // If nolabs were found to be available at all, it implies that we can no longer offer this course
    // return the correpsonding status
    if (remove_course)
    {
        return -2;
    }
    // Indicate no TA was found but is still there
    return -1;
}
void dealloc_TA(course *c, int lab_index, int TA_index)
{
    // Make a TA available again
    pthread_mutex_lock(labs_list[lab_index].mutex);
    labs_list[lab_index].TA_available[TA_index] = 1;
    pthread_mutex_unlock(labs_list[lab_index].mutex);
    return;
}
int dealloc_students(course *c, int max_slots)
{
    pthread_mutex_lock(student_list_lock);
    for (int i = 0; i < num_students; i++)
    {
        if (student_list[i].id == -1)
        {
            continue;
        }
        if (student_list[i].tut_attendance == c->id) // If the student is attending the current course remove himfrom the tut and signal that the tut is done
        {
            pthread_mutex_lock(student_list[i].lock);
            pthread_mutex_lock(student_list[i].mutex);
            student_list[i].tut_attendance = -1;
            student_list[i].curr_preference = c->id;
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            student_list[i].tut_done = 1;
            // printf("Tut Done\n");
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            pthread_cond_signal(student_list[i].cond);
            pthread_mutex_unlock(student_list[i].lock);
            pthread_mutex_unlock(student_list[i].mutex);
        }
    }
    pthread_mutex_unlock(student_list_lock);
}
int allot_students(course *c, int max_slots)
{
    int t = max_slots; // max slots that can be given
    while (t == max_slots) // Keep repeating is no slots are filled
    {
        bool all_done = true;
        for (int i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(student_list[i].mutex);
            if (student_list[i].id == -1) // If student is off the list skip
            {
                pthread_mutex_unlock(student_list[i].mutex);
                continue;
            }
            else
            {
                all_done = false;
            }
            if (t == 0)
            {
                pthread_mutex_unlock(student_list[i].mutex); // break once the slots are filled
                return t;
            }
            if (student_list[i].curr_preference == c->id) // Change the students preference to -1 and inidcate he is in a tut.
            {
                student_list[i].tut_attendance = c->id;
                student_list[i].curr_preference = -1;
                t--;
            }
            pthread_mutex_unlock(student_list[i].mutex);
        }
        if (all_done) // Exit the thread if no students remain
        {
            pthread_exit(0);
        }
    }
    return t;
}
void remove_course(course *c)
{
    // To remove a course we go over the students list and remove those whose current preference is the course
    // We then signal that the tut is done even if it wasnt to start the student thread again
    for (int i = 0; i < num_students; i++)
    {
        pthread_mutex_lock(student_list[i].mutex);
        if (student_list[i].curr_preference != c->id || student_list[i].id == -1)
        {
            pthread_mutex_unlock(student_list[i].mutex);
            continue;
        }
        pthread_mutex_lock(student_list[i].lock);
        student_list[i].tut_done = 1;
        pthread_cond_signal(student_list[i].cond);
        pthread_mutex_unlock(student_list[i].lock);
        pthread_mutex_unlock(student_list[i].mutex);
    }
}
void course_init(course *c)
{
    while (true)
    {
        // At every point the course prepares to start a new tut, we choose a random number for the number of slots the tut can have.
        int d = random_number(1, c->maximum_slots);
        printf(BGRN "Course %s has been allocated %d seats\n" ANSI_RESET, c->name, d);
        bool all_done = true;
        // We then allocate TAs to the course by choosing from the list of labs available to it.
        int result = allot_TAs(c);
        while (result < 0)
        {
            // If the return value is -2, it indicates the course can no longer be offered so we declare it invalid and print an error message
            if (result == -2)
            {
                printf(BRED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, c->name);
                remove_course(c);
                c->id = -1;
                return;
            }
            else if (result == -1)
            {
                // TAs have not been allocated but can be, so repeat the process.
                result = allot_TAs(c);
            }
        }
        if (result >= 0)
        {
            int slots_left;
            // We now allot students
            slots_left = allot_students(c, d);
            printf(BCYN "Tutorial has started for course %s with %d slots filled out of %d\n" ANSI_RESET, c->name, d - slots_left, d);
            for (int i = 0; i < 5; i++)
            {
                sleep(1);
            }
            printf(BMAG "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_RESET, c->TA_index, labs_list[result].name, c->name);
            fflush(stdout);
            // Deallocate TAs and then students
            dealloc_TA(c, result, c->TA_index);
            dealloc_students(c, d);
        }
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/labs.h ####################//

#ifndef LABS_H
#define LABS_H

typedef struct
{
    int id;
    char name[100];
    int student_TA_number;
    int *TA_available;
    int *TA_allotments;
    int number_of_TAships;
    pthread_mutex_t *mutex;
} Labs;

Labs *labs_list;
pthread_mutex_t *labs_list_lock;

void labs_list_init(int n);
void labs_init(Labs *l);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/course.h ####################//

#ifndef COURSE_H
#define COURSE_H

typedef struct
{
    int id;
    char name[100];
    float interest;
    int maximum_slots;
    int number_of_labs;
    int *labs_accepted;
    int TA_index;
    pthread_mutex_t *mutex;
} course;

course *course_list;
pthread_mutex_t *course_list_lock;

void course_list_init(int n);
int allot_TAs(course *c);
void dealloc_TA(course *c, int lab_index, int TA_index);
int dealloc_students(course *c, int max_slots);
int allot_students(course *c, int max_slots);
void course_init(course *c);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q1/global.h ####################//

#ifndef GLOBAL_H
#define GLOBAL_H

#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "course.h"
#include "labs.h"
#include "student.h"

void *shareMem(size_t size);
int random_number(int min, int max);
int num_students, num_courses, num_labs;

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/labs.c ####################//

#include "global.h"

void labs_list_init(int n)
{
    // Setup the labs information with the input
    labs_list = (Labs *)shareMem(n * sizeof(Labs));
    for (int i = 0; i < n; i++)
    {
        labs_list[i].id = i;
        scanf("%s %d %d", labs_list[i].name, &labs_list[i].student_TA_number, &labs_list[i].number_of_TAships);
        labs_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(labs_list[i].mutex, NULL);
        labs_list[i].TA_available = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_available[j] = 1;
        }
        labs_list[i].TA_allotments = (int *)shareMem(labs_list[i].student_TA_number * sizeof(int));
        for (int j = 0; j < labs_list[i].student_TA_number; j++)
        {
            labs_list[i].TA_allotments[j] = 0;
        }
    }
    labs_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(labs_list->mutex, NULL);
}
void labs_init(Labs *l)
{
    ; // Do nothing here
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/student.c ####################//

#include "global.h"

void student_list_init(int n)
{
    // Initialize students details with the input and initialize locks
    student_list = (student *)shareMem(n * sizeof(student));
    float calibre;
    int pref1, pref2, pref3;
    int time_to_fill = 0;
    for (int i = 0; i < n; i++)
    {
        student_list[i].id = i;
        scanf("%f %d %d %d %d", &calibre, &pref1, &pref2, &pref3, &time_to_fill);
        student_list[i].calibre = calibre;
        student_list[i].pref_1 = pref1;
        student_list[i].pref_2 = pref2;
        student_list[i].pref_3 = pref3;
        student_list[i].time_to_fill = time_to_fill;
        student_list[i].tut_attendance = -1;
        student_list[i].permanent_selection = -1;
        student_list[i].curr_preference = -1;
        student_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        student_list[i].cond = (pthread_cond_t *)shareMem(sizeof(pthread_cond_t));
        student_list[i].tut_done = 0;
        pthread_mutex_init(student_list[i].mutex, NULL);
        pthread_mutex_init(student_list[i].lock, NULL);
        pthread_cond_init(student_list[i].cond, NULL);
    }
    student_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(student_list_lock, NULL);
}
void allot_preferences(student *s)
{
    // If there is no initial preference we simply give the first choice as the current one
    // Change to the next one is this function is called
    // Show that the student withdrwas the course and switches or leaves the list if the choices are done
    if (s->curr_preference == -1)
    {
        s->curr_preference = s->pref_1;
    }
    else if (s->curr_preference == s->pref_1)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_2;
        printf(BMAG "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_RESET, s->id, course_list[s->pref_1].name, course_list[s->pref_2].name);
    }
    else if (s->curr_preference == s->pref_2)
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        s->curr_preference = s->pref_3;
        printf(BMAG "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_RESET, s->id, course_list[s->pref_2].name, course_list[s->pref_3].name);
    }
    else
    {
        printf(BRED "Student %d has withdrawn from course %s\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
        printf(BBLK "Student %d couldn’t get any of his preferred courses\n" ANSI_RESET, s->id);
        s->id = -1;
    }
    if (s->id != -1 && course_list[s->curr_preference].id == -1) // If the student is still active but the course he chooses isnt we call the funtion again to either switch or leave
    {
        allot_preferences(s);
    }
}
void student_init(student *s)
{
    //printf("Student %d\n", s->id);
    sleep(s->time_to_fill);
    printf(BGRN "Student %d has filled in preferences for course registration\n" ANSI_RESET, s->id);
    for(int i = 0; i < 4; i++)
    {
        // Allot the student preferences to change the students choice or make him choose
        pthread_mutex_lock(s->mutex);
        allot_preferences(s);
        pthread_mutex_unlock(s->mutex);
        if(s->id == -1)// Leave if the student is invalid
        {
            return;
        }
        pthread_mutex_lock(s->lock);
        while (s->tut_done == 0)
        {
            pthread_cond_wait(s->cond, s->lock);// Wait for the tut to finish or the course to be removed
        }
        pthread_mutex_unlock(s->lock);
        s->tut_done = 0;
        pthread_cond_init(s->cond, NULL);
        if(course_list[s->curr_preference].id == -1)// Continue if the course was invalid
        {
            pthread_mutex_unlock(s->mutex);
            continue;
        }
        // MAke the random choice based on the possibility that we can calculate
        float prob_permanent = s->calibre * course_list[s->curr_preference].interest * 100;
        int random_number = rand() % 100 + 1;
        if (prob_permanent >= (float)random_number)
        {
            printf(BGRN "Student %d has selected the course %s permanently\n" ANSI_RESET, s->id, course_list[s->curr_preference].name);
            s->id = -1;
            pthread_mutex_unlock(s->mutex);
            return;
        }
        pthread_mutex_unlock(s->mutex);
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/main.c ####################//

#include "global.h"

extern int num_students, num_courses, num_labs;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course_list_init(num_courses);
    student_list_init(num_students);
    labs_list_init(num_labs);
    pthread_t *student_threads = (pthread_t *)shareMem(num_students * sizeof(pthread_t));
    for (int i = 0; i < num_students; i++)
    {
        pthread_create(&student_threads[i], NULL, (void *)student_init, &student_list[i]);
    }
    pthread_t *course_threads = (pthread_t *)shareMem(num_courses * sizeof(pthread_t));
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&course_threads[i], NULL, (void *)course_init, &course_list[i]);
    }
    pthread_t *labs_threads = (pthread_t *)shareMem(num_labs * sizeof(pthread_t));
    for (int i = 0; i < num_labs; i++)
    {
        pthread_create(&labs_threads[i], NULL, (void *)labs_init, &labs_list[i]);
    }
    for (int i = 0; i < num_students; i++) {
        pthread_join(student_threads[i], NULL);
    }
    for (int i = 0; i < num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }
    for (int i = 0; i < num_labs; i++) {
        pthread_join(labs_threads[i], NULL);
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/student.h ####################//

#ifndef STUDENT_H
#define STUDENT_H

typedef struct
{
    int id;
    float calibre;
    int pref_1;
    int pref_2;
    int pref_3;
    int permanent_selection;
    int curr_preference;
    int tut_attendance;
    int time_to_fill;
    pthread_mutex_t *mutex;
    pthread_mutex_t *lock;
    pthread_cond_t *cond;
    int tut_done;
} student;

student *student_list;
pthread_mutex_t *student_list_lock;

void student_list_init(int n);
void student_init(student *s);
void allot_preferences(student *s);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/course.c ####################//

#include "global.h"

void course_list_init(int n)
{
    // We initialize the course struct here, we initialize the values of a course with the input and also initialize the locks
    course_list = (course *)shareMem(n * sizeof(course));
    for (int i = 0; i < n; i++)
    {
        course_list[i].id = i;
        // Get course details
        scanf("%s %f %d %d", course_list[i].name, &course_list[i].interest, &course_list[i].maximum_slots, &course_list[i].number_of_labs);
        course_list[i].labs_accepted = (int *)shareMem(course_list[i].number_of_labs * sizeof(int));
        for (int j = 0; j < course_list[i].number_of_labs; j++)
        {
            // Get the labs from which the course can take TAs
            scanf("%d", &course_list[i].labs_accepted[j]);
        }
        course_list[i].mutex = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
        pthread_mutex_init(course_list[i].mutex, NULL);
    }
    course_list_lock = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(course_list_lock, NULL);
}
int allot_TAs(course *c)
{
    bool remove_course = true;
    for (int i = 0; i < c->number_of_labs; i++)
    {
        // We first iterate over all the labs from which TAs can be chosen
        pthread_mutex_lock(labs_list[c->labs_accepted[i]].mutex);
        if (labs_list[c->labs_accepted[i]].id == -1)
        {
            pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
            continue;
        }
        bool lab_no_slots = true;
        for (int j = 0; j < labs_list[c->labs_accepted[i]].student_TA_number; j++)
        {
            if (labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                remove_course = false;
                lab_no_slots = false;
            }
            // Choose a TA if the TA is not exhausted and he is available
            if (labs_list[c->labs_accepted[i]].TA_available[j] == 1 && labs_list[c->labs_accepted[i]].TA_allotments[j] < labs_list[c->labs_accepted[i]].number_of_TAships)
            {
                // Update TA stats and print details
                labs_list[c->labs_accepted[i]].TA_available[j] = 0;
                labs_list[c->labs_accepted[i]].TA_allotments[j]++;
                c->TA_index = j;
                printf(BYEL "TA %d from lab %s allocated to course %s for his %d" ANSI_RESET, j, labs_list[c->labs_accepted[i]].name, c->name, labs_list[c->labs_accepted[i]].TA_allotments[j]);
                switch (labs_list[c->labs_accepted[i]].TA_allotments[j] % 10)
                {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                }
                printf(BYEL " TAship\n" ANSI_RESET);
                pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
                // Return the lab from which the TA is alloted
                return labs_list[c->labs_accepted[i]].id;
            }
        }
        // If there are no slots in the Lab it indicates that all the TAs are exhausted so it is no longer possible to use this lab
        // Indicate this lab is invalid and print an error
        if (lab_no_slots)
        {
            labs_list[c->labs_accepted[i]].id = -1;
            printf(BBLK "Lab %s no longer has students available for TA ship\n" ANSI_RESET, labs_list[c->labs_accepted[i]].name);
        }
        pthread_mutex_unlock(labs_list[c->labs_accepted[i]].mutex);
    }
    // If nolabs were found to be available at all, it implies that we can no longer offer this course
    // return the correpsonding status
    if (remove_course)
    {
        return -2;
    }
    // Indicate no TA was found but is still there
    return -1;
}
void dealloc_TA(course *c, int lab_index, int TA_index)
{
    // Make a TA available again
    pthread_mutex_lock(labs_list[lab_index].mutex);
    labs_list[lab_index].TA_available[TA_index] = 1;
    pthread_mutex_unlock(labs_list[lab_index].mutex);
    return;
}
int dealloc_students(course *c, int max_slots)
{
    pthread_mutex_lock(student_list_lock);
    for (int i = 0; i < num_students; i++)
    {
        if (student_list[i].id == -1)
        {
            continue;
        }
        if (student_list[i].tut_attendance == c->id) // If the student is attending the current course remove himfrom the tut and signal that the tut is done
        {
            pthread_mutex_lock(student_list[i].lock);
            pthread_mutex_lock(student_list[i].mutex);
            student_list[i].tut_attendance = -1;
            student_list[i].curr_preference = c->id;
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            student_list[i].tut_done = 1;
            // printf("Tut Done\n");
            // printf("student %d, course %s, curr_preference %d\n", student_list[i].id, c->name, student_list[i].curr_preference);
            pthread_cond_signal(student_list[i].cond);
            pthread_mutex_unlock(student_list[i].lock);
            pthread_mutex_unlock(student_list[i].mutex);
        }
    }
    pthread_mutex_unlock(student_list_lock);
}
int allot_students(course *c, int max_slots)
{
    int t = max_slots; // max slots that can be given
    while (t == max_slots) // Keep repeating is no slots are filled
    {
        bool all_done = true;
        for (int i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(student_list[i].mutex);
            if (student_list[i].id == -1) // If student is off the list skip
            {
                pthread_mutex_unlock(student_list[i].mutex);
                continue;
            }
            else
            {
                all_done = false;
            }
            if (t == 0)
            {
                pthread_mutex_unlock(student_list[i].mutex); // break once the slots are filled
                return t;
            }
            if (student_list[i].curr_preference == c->id) // Change the students preference to -1 and inidcate he is in a tut.
            {
                student_list[i].tut_attendance = c->id;
                student_list[i].curr_preference = -1;
                t--;
            }
            pthread_mutex_unlock(student_list[i].mutex);
        }
        if (all_done) // Exit the thread if no students remain
        {
            pthread_exit(0);
        }
    }
    return t;
}
void remove_course(course *c)
{
    // To remove a course we go over the students list and remove those whose current preference is the course
    // We then signal that the tut is done even if it wasnt to start the student thread again
    for (int i = 0; i < num_students; i++)
    {
        pthread_mutex_lock(student_list[i].mutex);
        if (student_list[i].curr_preference != c->id || student_list[i].id == -1)
        {
            pthread_mutex_unlock(student_list[i].mutex);
            continue;
        }
        pthread_mutex_lock(student_list[i].lock);
        student_list[i].tut_done = 1;
        pthread_cond_signal(student_list[i].cond);
        pthread_mutex_unlock(student_list[i].lock);
        pthread_mutex_unlock(student_list[i].mutex);
    }
}
void course_init(course *c)
{
    while (true)
    {
        // At every point the course prepares to start a new tut, we choose a random number for the number of slots the tut can have.
        int d = random_number(1, c->maximum_slots);
        printf(BGRN "Course %s has been allocated %d seats\n" ANSI_RESET, c->name, d);
        bool all_done = true;
        // We then allocate TAs to the course by choosing from the list of labs available to it.
        int result = allot_TAs(c);
        while (result < 0)
        {
            // If the return value is -2, it indicates the course can no longer be offered so we declare it invalid and print an error message
            if (result == -2)
            {
                printf(BRED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, c->name);
                remove_course(c);
                c->id = -1;
                return;
            }
            else if (result == -1)
            {
                // TAs have not been allocated but can be, so repeat the process.
                result = allot_TAs(c);
            }
        }
        if (result >= 0)
        {
            int slots_left;
            // We now allot students
            slots_left = allot_students(c, d);
            printf(BCYN "Tutorial has started for course %s with %d slots filled out of %d\n" ANSI_RESET, c->name, d - slots_left, d);
            for (int i = 0; i < 5; i++)
            {
                sleep(1);
            }
            printf(BMAG "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_RESET, c->TA_index, labs_list[result].name, c->name);
            fflush(stdout);
            // Deallocate TAs and then students
            dealloc_TA(c, result, c->TA_index);
            dealloc_students(c, d);
        }
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/labs.h ####################//

#ifndef LABS_H
#define LABS_H

typedef struct
{
    int id;
    char name[100];
    int student_TA_number;
    int *TA_available;
    int *TA_allotments;
    int number_of_TAships;
    pthread_mutex_t *mutex;
} Labs;

Labs *labs_list;
pthread_mutex_t *labs_list_lock;

void labs_list_init(int n);
void labs_init(Labs *l);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/course.h ####################//

#ifndef COURSE_H
#define COURSE_H

typedef struct
{
    int id;
    char name[100];
    float interest;
    int maximum_slots;
    int number_of_labs;
    int *labs_accepted;
    int TA_index;
    pthread_mutex_t *mutex;
} course;

course *course_list;
pthread_mutex_t *course_list_lock;

void course_list_init(int n);
int allot_TAs(course *c);
void dealloc_TA(course *c, int lab_index, int TA_index);
int dealloc_students(course *c, int max_slots);
int allot_students(course *c, int max_slots);
void course_init(course *c);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q1/global.h ####################//

#ifndef GLOBAL_H
#define GLOBAL_H

#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "course.h"
#include "labs.h"
#include "student.h"

void *shareMem(size_t size);
int random_number(int min, int max);
int num_students, num_courses, num_labs;

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

#endif