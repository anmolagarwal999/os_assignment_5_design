
//###########FILE CHANGE ./main_folder/Adhiraj Deshmukh_305930_assignsubmission_file_/2021121012_assignment_5/q1/q1.c ####################//

#include "headers.h"

#define CMAX 30

typedef struct course {
    int id;

    char name[20];
    double interest;
    int max_slots;
    int num_course_labs;
    int course_labs[CMAX];

    int removed;

} course;

typedef struct student {
    int id;

    double calibre;
    int course_preference[3];
    int time;

    int current_course;

} student;

typedef struct lab {
    int id;

    char name[20];
    int num_ta;
    int limit;

    int ta_na;
    int completed;

} lab;


int num_students, num_labs, num_courses;

course arr_courses[CMAX];
student arr_students[CMAX];
lab arr_labs[CMAX];

int ta_limit[CMAX][CMAX];

sem_t sem_course_seats[CMAX];
sem_t sem_tutorials_start[CMAX];
sem_t sem_tutorials_end[CMAX];

sem_t mutex_ta[CMAX][CMAX];

pthread_t th_courses[CMAX];
pthread_t th_students[CMAX];


double random_double() {
    return (double) rand() / (double) RAND_MAX;
}

void *init_courses(void *args) {

    course* curr_course = (course *) args;

    while(1) {
        int flag = 0;
        int x=-1, y=-1;

        for(int i=0; i<curr_course->num_course_labs; i++) {
            int lab_id = curr_course->course_labs[i];

            if(arr_labs[lab_id].completed) {
                continue;
            }

            for(int j=0; j<arr_labs[lab_id].num_ta; j++) {
                sem_wait(&mutex_ta[lab_id][j]);

                if(ta_limit[lab_id][j] < arr_labs[lab_id].limit) {
                    flag = 1;
                    x=lab_id, y=j;

                    ta_limit[lab_id][j]++;

                    arr_labs[lab_id].ta_na += (ta_limit[lab_id][j] == arr_labs[lab_id].num_ta); 
                    break;
                }

                sem_post(&mutex_ta[lab_id][j]);
            }

            if(flag) {
                break;
            }
        }

        if(flag == 0) {
            break;
        }

        printf("TA %d from lab %s has been allocated to course %s for %dth" ANSI_COLOR_CYAN " TA ship " ANSI_COLOR_RESET "\n", y, arr_labs[x].name, curr_course->name, ta_limit[x][y]);

        int d = rand() % (curr_course->max_slots) + 1;
        printf("Course %s has been" ANSI_COLOR_CYAN " allocated " ANSI_COLOR_RESET "%d seats\n", curr_course->name, d);

        int cnt_seats = 0;
        for(int i=0; i<num_students; i++) {
            cnt_seats += (arr_students[i].current_course == curr_course->id);
        }

        cnt_seats = (cnt_seats <= d) ? cnt_seats : d;

        for(int i=0; i<cnt_seats; i++) {
            sem_post(&sem_course_seats[curr_course->id]);
        }

        for(int i=0; i<cnt_seats; i++) {
            sem_wait(&sem_tutorials_start[curr_course->id]);
        }

        printf("Tutorial has" ANSI_COLOR_CYAN " started " ANSI_COLOR_RESET "for Course %s with %d seats filled out of %d\n", curr_course->name, cnt_seats, d);
        sleep(5);
        printf("TA %d from lab %s has" ANSI_COLOR_CYAN " completed " ANSI_COLOR_RESET "the tutorial and left the course %s\n", y, arr_labs[x].name, curr_course->name);

        if(arr_labs[x].completed == 0 && arr_labs[x].ta_na == arr_labs[x].num_ta) {
            printf("Lab %s" ANSI_COLOR_CYAN " no longer " ANSI_COLOR_RESET "has students available for TA ship\n", arr_labs[x].name);
            arr_labs[x].completed = 1;
        }

        for(int i=0; i<cnt_seats; i++) {
            sem_post(&sem_tutorials_end[curr_course->id]);
        }

        sem_post(&mutex_ta[x][y]);
    }

    printf("Course %s doesn’t have any TA’s eligible and is" ANSI_COLOR_CYAN " removed " ANSI_COLOR_RESET "from course offerings\n", curr_course->name);

    arr_courses[curr_course->id].removed = 1;
    for(int i=0; i<num_students; i++) {
        sem_post(&sem_course_seats[curr_course->id]);
    }

    return NULL;
}

void *init_student(void *args) {

    student* curr_student = (student *) args;

    sleep(curr_student->time);
    printf("Student %d has filled in preferences for course" ANSI_COLOR_MAGENTA " registration " ANSI_COLOR_RESET "\n", curr_student->id);

    for(int i=0; i<3; i++) {

        if(arr_courses[curr_student->course_preference[i]].removed) {
            if(i < 2) printf("Student %d has" ANSI_COLOR_MAGENTA " changed " ANSI_COLOR_RESET "current preference from %s (priority %d) to %s (priority %d)\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name, i+1, arr_courses[curr_student->course_preference[i+1]].name, i+2);
            continue;
        }

        curr_student->current_course = curr_student->course_preference[i];
        sem_wait(&sem_course_seats[curr_student->course_preference[i]]);
        curr_student->current_course = -1;

        if(arr_courses[curr_student->course_preference[i]].removed) {
            if(i < 2) printf("Student %d has" ANSI_COLOR_MAGENTA " changed " ANSI_COLOR_RESET "current preference from %s (priority %d) to %s (priority %d)\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name, i+1, arr_courses[curr_student->course_preference[i+1]].name, i+2);
            continue;
        }

        printf("Student %d has been" ANSI_COLOR_MAGENTA " allocated " ANSI_COLOR_RESET "a seat in course %s\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name);
        
        sem_post(&sem_tutorials_start[curr_student->course_preference[i]]);
        sem_wait(&sem_tutorials_end[curr_student->course_preference[i]]);

        double prob = arr_courses[curr_student->course_preference[i]].interest * curr_student->calibre;
        double rand = random_double();

        if(rand <= prob) {
            printf("Student %d has" ANSI_COLOR_MAGENTA " selected " ANSI_COLOR_RESET "course %s permanently\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name);
            return NULL;
        }

        printf("Student %d has withdrawn from course %s\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name);
        if(i < 2) printf("Student %d has" ANSI_COLOR_MAGENTA " changed " ANSI_COLOR_RESET "current preference from %s (priority %d) to %s (priority %d)\n", curr_student->id, arr_courses[curr_student->course_preference[i]].name, i+1, arr_courses[curr_student->course_preference[i+1]].name, i+2);
    }

    printf("Student %d" ANSI_COLOR_MAGENTA " couldn’t " ANSI_COLOR_RESET "get any of his preferred courses\n", curr_student->id);
    
    return NULL;
}


int main(int argc, char* argv[]) {

    srand(time(0));

    printf(ANSI_COLOR_RED " SIMULATION IS STARTING! " ANSI_COLOR_RESET "\n");

    scanf(" %d %d %d", &num_students, &num_labs, &num_courses);
    
    for(int i=0; i<num_courses; i++) {
        arr_courses[i].id = i;
        arr_courses[i].removed = 0;

        scanf(" %s", arr_courses[i].name);
        scanf(" %lf %d %d", &arr_courses[i].interest, &arr_courses[i].max_slots, &arr_courses[i].num_course_labs);
        for(int j=0; j<arr_courses[i].num_course_labs; j++) {
            scanf(" %d", &arr_courses[i].course_labs[j]);
        }

        sem_init(&sem_course_seats[i], 0, 0);
        sem_init(&sem_tutorials_start[i], 0, 0);
        sem_init(&sem_tutorials_end[i], 0, 0);
    }

    for(int i=0; i<num_students; i++) {
        arr_students[i].id = i;
        arr_students[i].current_course = -1;

        scanf(" %lf", &arr_students[i].calibre);
        for(int j=0; j<3; j++) {
            scanf(" %d", &arr_students[i].course_preference[j]);
        }
        scanf(" %d", &arr_students[i].time);
    }

    for(int i=0; i<num_labs; i++) {
        arr_labs[i].id = i;
        arr_labs[i].ta_na = 0;
        arr_labs[i].completed = 0;

        scanf(" %s", arr_labs[i].name);
        scanf(" %d %d", &arr_labs[i].num_ta, &arr_labs[i].limit);

        for(int j=0; j<arr_labs[i].num_ta; j++) {
            sem_init(&mutex_ta[i][j], 0, 1);
        }
    }

    for(int i=0; i<num_students; i++) {
        pthread_create(&th_students[i], NULL, init_student, &arr_students[i]);
    }

    for(int i=0; i<num_courses; i++) {
        pthread_create(&th_courses[i], NULL, init_courses, &arr_courses[i]);
    }

    for(int i=0; i<num_students; i++) {
        pthread_join(th_students[i], NULL);
    }

    for(int i=0; i<num_courses; i++) {
        pthread_join(th_courses[i], NULL);
    }

    printf(ANSI_COLOR_GREEN " SIMULATION IS OVER! " ANSI_COLOR_RESET "\n");

    for(int i=0; i<num_courses; i++) {
        sem_destroy(&sem_course_seats[i]);
        sem_destroy(&sem_tutorials_start[i]);
        sem_destroy(&sem_tutorials_end[i]);
    }

    for(int i=0; i<num_labs; i++) {
        for(int j=0; j<arr_labs->num_ta; j++) {
            sem_destroy(&mutex_ta[i][j]);
        }
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/Adhiraj Deshmukh_305930_assignsubmission_file_/2021121012_assignment_5/q1/headers.h ####################//

// Normal headers
#include <stdio.h> // fflush(), perror(), sprintf()
#include <stdlib.h> // calloc(), malloc()
#include <string.h> //  strtok(), strstr(), strlen()
#include <time.h>

#include <unistd.h> // system calls: open(), read(), write(), lseek(), gethostname()
#include <fcntl.h> // use of flags O_CREAT, SEEK_SET, etc.

#include <sys/stat.h> // mkdir(), stat(), _exit()
#include <sys/types.h>
#include <sys/wait.h> // wait()
#include <sys/utsname.h> // uname()

#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <dirent.h>
#include <signal.h>
#include <limits.h>

#include <pthread.h>
#include <semaphore.h>

// C++ headers
// #include<bits/stdc++.h>
// using namespace std;

// #define debug(x) cout << #x << " : " << x << endl

// colors
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"