
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/main.c ####################//

#include "headers.h"
int main()
{
    scanf("%d %d %d", &total_students, &total_labs, &total_courses);
    for (int i = 0; i < total_courses; i++)
    {
        Courses[i].course_id = i;
        Courses[i].course_removed = -1;
        scanf("%s %f %d %d", Courses[i].name, &Courses[i].interest_quo, &Courses[i].max_slots, &Courses[i].num_labs);

        for (int j = 0; j < Courses[i].num_labs; j++)
        {
            scanf("%d", &Courses[i].lab_list[j]);
        }
    }
    for (int i = 0; i < total_students; i++)
    {
        scanf("%f %d %d %d %d", &Students[i].calbre, &Students[i].pref[0], &Students[i].pref[1], &Students[i].pref[2], &Students[i].entry_time);
        Students[i].id = i;
        Students[i].finalise = 0;
        Students[i].current_pref = 0;
        Students[i].availability = -1;
        Students[i].attending_tut = 0;
        Students[i].course = -1;
        pthread_mutex_init(&Students[i].student_mutex , NULL);
        pthread_cond_init(&Students[i].student_cond , NULL);
    }
    for (int i = 0; i < total_labs; i++)
    {
        scanf("%s %d %d", Labs[i].name, &Labs[i].num_ta, &Labs[i].max_times_ta);

        for (int j = 0; j < Labs[i].num_ta; j++)
        {
            Labs[i].tas[j].id = j;
            Labs[i].tas[j].course_id = -1;
            Labs[i].tas[j].num_taship = 0;
            Labs[i].tas[j].availability = 0;
            Labs[i].availability = 1;
            Labs[i].active_tas = Labs[i].num_ta;
        }

        Labs[i].lab_id = i;
    }
    for (int i = 0; i < total_labs; i++)
    {
        for (int j = 0; j < 50; j++)
        {
            pthread_mutex_init(&Tas_mutex[i][j], NULL);
        }
    }
    for(int i=0 ; i<50 ; i++)
    {
        ta_id[i]=-1;
        lab_id[i]=-1;
    }
    for (int i = 0; i < total_students; i++)
        pthread_create(&Threads_student[i], NULL, Students_init, &Students[i]);

    for (int i = 0; i < total_courses; i++)
        pthread_create(&Threads_course[i], NULL, Course_init, &Courses[i]);

    for (int i = 0; i < total_students; i++)
        pthread_join(Threads_student[i], NULL);

    return 0;
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/students.h ####################//

#ifndef STUDENT_H
#define STUDENT_H
typedef struct student
{
    int id;
    float calbre;
    int entry_time;
    int pref[3];
    int finalise;      // if 0 , it has not finalized else yes , it has finalalized
    int availability;  // will tell if student has filled its choices or not?
    int current_pref;  // tell which priority is it at this moment
    int attending_tut; // will tell if a student is in a tut or not?
    int course;        // tells which course he is planning to take // -1 means no signal was not sent from any course
    pthread_mutex_t student_mutex;
    pthread_cond_t student_cond;
} student;

student Students[500];
pthread_t Threads_student[500];

#endif
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/functions.c ####################//

#include "headers.h"
void check_ta(int lab_no)
{
    bool ind = false;
    for (int i = 0; i < Labs[lab_no].num_ta; i++)
    {
        if (Labs[lab_no].tas[i].num_taship < Labs[lab_no].max_times_ta)
        {
            ind = true;
        }
    }
    if (ind == false && Labs[lab_no].availability != -1)
    {
        Labs[lab_no].availability = -1;
        printf(ANSI_COLOR_MAGENTA "Lab %s no longer has students available for TA ship\n" ANSI_COLOR_RESET , Labs[lab_no].name);
    }
}
int check_lab(course *c)
{
    cnt = 0;
    ind = 0;
    for (int i = 0; i < c->num_labs; i++)
    {
        for (int j = 0; j < Labs[c->lab_list[i]].num_ta; j++)
        {
            if (Labs[c->lab_list[i]].tas[j].num_taship < Labs[c->lab_list[i]].max_times_ta)
            {
                return 0;
            }
        }
    }

    c->course_removed = 1;
    printf(ANSI_COLOR_YELLOW "Course %s doesn't have any TA's eligible and is removed from course offerings" ANSI_COLOR_RESET "\n", c->name);

    for (int i = 0; i < total_students; i++)
    {
        if (Students[i].pref[Students[i].current_pref] == c - Courses)
        {
            pthread_mutex_lock(&Students[i].student_mutex);
            Students[i].course = c - Courses;
            pthread_cond_signal(&Students[i].student_cond);
            pthread_mutex_unlock(&Students[i].student_mutex);
        }
    }
    return 1;
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/labs.h ####################//

#ifndef TA_H
#define TA_H
typedef struct ta
{
    int id;
    int course_id;    
    int num_taship;   
    int availability; 
} ta;
typedef struct lab
{
    int lab_id;
    char name[256];
    int max_times_ta;
    int num_ta;       // stores how many ta lab has  // ids are 0 indexed
    ta tas[51];       // stores the info about the tas in a lab
    int availability; // if 1 -> means ta some ta is there who has not completed its quota else 0
    int active_tas;   // stores how many tas are there who has not completed their quota
    //pthread_mutex_t tas_mutex[51];
} lab;
lab Labs[50];
pthread_t labThread[50];
pthread_mutex_t Tas_mutex[50][50];
ta *ta_info[50];
#endif
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/courses.h ####################//

#ifndef COURSES_H
#define COURSES_H
typedef struct course
{
    int course_id;
    char name[256];
    float interest_quo;
    int max_slots;
    int num_labs;
    int lab_list[50];
    int course_removed;
} course;

course Courses[50];
pthread_t Threads_course[50];
int ta_id[50];
int lab_id[50] ;
#endif
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/students.c ####################//

#include "headers.h"

// one thread per student
void *Students_init(void *input)
{
    student *s = (student *)input;

    sleep(s->entry_time);

    printf(ANSI_COLOR_BLUE "Students %ld has filled in preferences for course registration" ANSI_COLOR_RESET "\n", s - Students);

    s->availability = 1;

    while (1)
    {
        pthread_mutex_lock(&s->student_mutex);
    begin:
        s->course = -1;
        s->attending_tut = 0;
        // wait for a signal from course to indicate that a tutorial has been completed
        int course_num = s->pref[s->current_pref];
        if (Courses[course_num].course_removed == -1)
        {
            pthread_cond_wait(&s->student_cond, &s->student_mutex);
        }

        s->attending_tut = 0;

        // check if signal was because course has been removed
        // course has been removed
        // move to the next course which is available
        if (Courses[s->course].course_removed == 1)
        {
            if (s->current_pref == 2)
            {
                s->availability = -1;
                printf(ANSI_COLOR_RED "Student %ld couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s - Students);
                pthread_mutex_unlock(&s->student_mutex);
                return NULL;
            }
            else if (s->current_pref == 1)
            {
                if (Courses[s->pref[s->current_pref + 1]].course_removed == -1)
                {
                    s->current_pref++;
                    printf(ANSI_COLOR_BLUE "Student %ld has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s - Students, Courses[s->pref[s->current_pref - 1]].name, s->current_pref - 1, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else
                {
                    s->availability = -1;
                    printf(ANSI_COLOR_RED "Student %ld couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s - Students);
                    pthread_mutex_unlock(&s->student_mutex);
                    return NULL;
                }
            }
            else if (s->current_pref == 0)
            {
                if (Courses[s->pref[s->current_pref + 1]].course_removed == -1)
                {
                    s->current_pref++;
                    printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s->id, Courses[s->pref[s->current_pref - 1]].name, s->current_pref - 1, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else if (Courses[s->pref[s->current_pref + 2]].course_removed == -1)
                {
                    s->current_pref = s->current_pref + 2;
                    printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s->id, Courses[s->pref[s->current_pref - 2]].name, s->current_pref - 2, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else
                {
                    s->availability = -1;
                    printf(ANSI_COLOR_RED "Student %d couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s->id);
                    pthread_mutex_unlock(&s->student_mutex);
                    return NULL;
                }
            }
        }

        // student has completed the tut of some course
        s->attending_tut = 0;
        float probability = (s->calbre) * (Courses[s->pref[s->current_pref]].interest_quo) * 1.0;
        float random_value = ((float)rand() * 1.0) / RAND_MAX;

        if (random_value <= probability) // select this course
        {
            printf(ANSI_COLOR_MAGENTA  "Student %ld has selected the %s permanently" ANSI_COLOR_RESET "\n", s-Students, Courses[s->pref[s->current_pref]].name);
            Students[s-Students].finalise = 1;
            s->availability = -1;
            pthread_mutex_unlock(&s->student_mutex);
            return NULL;
        }
        else // change priority if this is not the third course or else exit the simulation
        {

            printf(ANSI_COLOR_CYAN "Student %ld has withdrawn from the %s course" ANSI_COLOR_RESET "\n", s-Students, Courses[s->pref[s->current_pref]].name);

            if (s->current_pref == 2)
            {
                s->availability = -1;
                printf(ANSI_COLOR_RED "Student %ld couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s - Students);
                pthread_mutex_unlock(&s->student_mutex);
                return NULL;
            }
            else if (s->current_pref == 1)
            {
                if (Courses[s->pref[s->current_pref + 1]].course_removed == -1)
                {
                    s->current_pref++;
                    printf(ANSI_COLOR_BLUE "Student %ld has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s - Students, Courses[s->pref[s->current_pref - 1]].name, s->current_pref - 1, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else
                {
                    s->availability = -1;
                    printf(ANSI_COLOR_RED "Student %ld couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s - Students);
                    pthread_mutex_unlock(&s->student_mutex);
                    return NULL;
                }
            }
            else if (s->current_pref == 0)
            {
                if (Courses[s->pref[s->current_pref + 1]].course_removed == -1)
                {
                    s->current_pref++;
                    printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s->id, Courses[s->pref[s->current_pref - 1]].name, s->current_pref - 1, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else if (Courses[s->pref[s->current_pref + 2]].course_removed == -1)
                {
                    s->current_pref = s->current_pref + 2;
                    printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority %d) to %s (priority %d)" ANSI_COLOR_RESET "\n", s->id, Courses[s->pref[s->current_pref - 2]].name, s->current_pref - 2, Courses[s->pref[s->current_pref]].name, s->current_pref);
                    goto begin;
                }
                else
                {
                    s->availability = -1;
                    printf(ANSI_COLOR_RED "Student %d couldn't get any of his preferred courses" ANSI_COLOR_RESET "\n", s->id);
                    pthread_mutex_unlock(&s->student_mutex);
                    return NULL;
                }
            }
        }
    }
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/courses.c ####################//

#include "headers.h"

void *Course_init(void *input)
{
    course *c = (course *)input;
    int students_selected[100];
    int a = check_lab(c);
    if (a == 1)
    {
        return NULL;
    }
    while (1)
    {
        while (1)
        {
            cnt++;
            if (cnt > 15)
            {
                int x = check_lab(c);
                cnt = 0;
                if (x == 1)
                {
                    return 0;
                }
            }
            for (int i = 0; i < c->num_labs; i++)
            {
                for (int j = 0; j < Labs[c->lab_list[i]].num_ta; j++)
                {
                    if ((Labs[c->lab_list[i]].tas[j].availability == 0) && (Labs[c->lab_list[i]].tas[j].num_taship < Labs[c->lab_list[i]].max_times_ta))
                    {
                        if (pthread_mutex_trylock(&Tas_mutex[Labs[c->lab_list[i]].lab_id][j]) != 0)
                        {
                            continue;
                        }

                        Labs[c->lab_list[i]].tas[j].availability = 1;
                        Labs[c->lab_list[i]].tas[j].num_taship++;
                        Labs[c->lab_list[i]].tas[j].course_id = c->course_id;
                        ta_info[c-Courses] = &Labs[c->lab_list[i]].tas[j];
                        ta_id[c-Courses] = Labs[c->lab_list[i]].tas[j].id;
                        lab_id[c-Courses] = Labs[c->lab_list[i]].lab_id;
                        goto proceed;
                    }
                }
            }
        }
    proceed:

        printf(ANSI_COLOR_YELLOW "Course %s has been allocated TA %d from lab %s" ANSI_COLOR_RESET "\n", c->name, ta_id[c-Courses], Labs[lab_id[c-Courses]].name);

        printf(ANSI_COLOR_GREEN "TA %d from lab %s has been allocated to course %s for his %d taship" ANSI_COLOR_RESET "\n", ta_id[c-Courses], Labs[lab_id[c-Courses]].name, c->name, ta_info[c-Courses]->num_taship);

        int slots = (rand() % c->max_slots) + 1;

        printf("Course %s has been allocated %d seats\n", c->name, slots);

        int num_student_selected = 0;

        while (num_student_selected == 0)
        {
            for (int i = 0; i < total_students; i++)
            {

                if ((Students[i].availability != -1) && (Students[i].pref[Students[i].current_pref] == c->course_id) && (Students[i].attending_tut == 0))
                {
                    pthread_mutex_lock(&Students[i].student_mutex);
                    Students[i].attending_tut = 1;
                    students_selected[num_student_selected] = i;
                    Students[i].course = c->course_id;
                    num_student_selected++;

                    printf(ANSI_COLOR_GREEN "Student %d has been allocated a seat in course %s" ANSI_COLOR_RESET "\n", i, c->name);
                }

                if (num_student_selected >= slots)
                    break;
            }
        }

        printf("Tutorial has started for course %s with %d seats fulled out of %d\n", c->name, num_student_selected, slots);
        sleep(tut_time);



        printf("TA %d from lab %s has completed tut for course %s\n", ta_id[c-Courses], Labs[lab_id[c-Courses]].name, c->name);

        for (int i = 0; i < num_student_selected; i++)
        {
            pthread_mutex_unlock(&Students[students_selected[i]].student_mutex);
            pthread_cond_signal(&Students[students_selected[i]].student_cond);
        }

        ta_info[c-Courses]->availability = 0;

        check_ta(lab_id[c-Courses]);

        pthread_mutex_unlock(&Tas_mutex[lab_id[c-Courses]][ta_info[c-Courses]->id]);
    }
}

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/functions.h ####################//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
void check_ta(int lab_no);
int check_lab(course *c);
#endif
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q1/headers.h ####################//

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>
#include "students.h"
#include "courses.h"
#include "labs.h"
#include "functions.h"

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

#define tut_time 3

int total_students, total_courses, total_labs;
int students_selected[100];
int cnt;
int ind;

void *Students_init(void *);
void *Course_init(void *);
void *Lab_init(void *);

