
//###########FILE CHANGE ./main_folder/Anurag Gupta_305791_assignsubmission_file_/Final/q1/main.c ####################//

#include "headers.h"
void *courseSimulator(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    while (1)
    {
        course_TA_allocation(id);
        course_size_allocation(id);
        course_start(id);
        tut_over(id);
        if (course_Removed(id) == 1)
        {
            printf("Course %s Removed\n", CourseList[id].CourseName);
            break;
        }
        if (!(studentsOver - num_student))
        {
            printf("Ending Simulation\n");
            break;
        }
    }
    return NULL;
}

void position_giver(int pos)
{
    if (pos == 1)
    {
        position[0] = '1';
        position[1] = 's';
        position[2] = 't';
        position[3] = '\0';
        // strcpy(position, "1st");
    }
    if (pos == 2)
    {
        position[0] = '2';
        position[1] = 'n';
        position[2] = 'd';
        position[3] = '\0';
        // strcpy(position, "2nd");
    }
    if (pos == 3)
    {
        position[0] = '3';
        position[1] = 'r';
        position[2] = 'd';
        position[3] = '\0';
        // strcpy(position, "3rd");
    }
    if (pos >= 4)
    {
        char str[100];
        char random[100];
        strcpy(random, "th");
        sprintf(str, "%d", pos);
        strcat(str, random);
        strcpy(position, str);
    }
}

void course_size_allocation(int id)
{
    pthread_mutex_lock(&mutex_lock2[id]);
    CourseList[id].isOn = 0;
    int max_seats = CourseList[id].course_max_slots;
    int number_of_seats = rand()%max_seats;
    CourseList[id].D = number_of_seats;
    printf("Course %s has been allocated %d seats\n", CourseList[id].CourseName, number_of_seats);
    pthread_cond_broadcast(&c2[id]);
    pthread_mutex_unlock(&mutex_lock2[id]);
}

void course_TA_allocation(int id)
{
    int f = 0;
    while (f < CourseList[id].p)
    {
        int labID = CourseList[id].listp[f];
        int j = 0;
        while (j < LabList[labID].noOfTAs)
        {
            pthread_mutex_lock(&course_TA_allocation_lock);
            if (CourseList[id].TA_Allocated+1 == 0)
            {
                if (LabList[labID].courseDone[j] - LabList[labID].max  < 0)
                {
                    if (!LabList[labID].busy[j])
                    {
                        int yes = 1;
                        int no = 0;
                        LabList[labID].courseDone[j] = LabList[labID].courseDone[j] + yes;
                        LabList[labID].busy[j] = yes;
                        CourseList[id].labID_Allocated = labID;
                        CourseList[id].TA_Allocated = j;
                        position_giver(LabList[labID].courseDone[j]);
                        printf("TA %d from lab %s has been allocated to course %s for his %s TA ship\n", j, LabList[labID].labName, CourseList[id].CourseName, position);
                    }
                }
            }
            pthread_mutex_unlock(&course_TA_allocation_lock);
            j++;
        }
        f++;
    }
}

void course_start(int id)
{
    while (!(CourseList[id].currentStudentSize))
    {
        if (!(studentsOver - num_student))
        {
            break;
        }
    }
    CourseList[id].isOn = 1;
    printf("Tutorial has started for course %s with %d seats filled\n", CourseList[id].CourseName, CourseList[id].currentStudentSize);
}

void tut_over(int id)
{
    int yes = 1;
    int no = 0;
    pthread_mutex_lock(&mutex_lock[id]);
    int TA_number = CourseList[id].TA_Allocated;
    char course[100];
    strcpy(course,CourseList[id].CourseName);
    printf("TA %d fromm lab %s has completed the tutorial for course %s\n", TA_number, LabList[CourseList[id].labID_Allocated].labName, course);
    LabList[CourseList[id].labID_Allocated].busy[CourseList[id].TA_Allocated] = no;
    CourseList[id].TA_Allocated = no-1;
    CourseList[id].currentStudentSize = no;
    pthread_cond_broadcast(&c[id]);
    pthread_mutex_unlock(&mutex_lock[id]);
}
int course_Removed(int id)
{
    int TAsOver = 1;
    int l = 0;
    while (l < CourseList[id].p)
    {
        int labID = CourseList[id].listp[l];
        int f = 0;
        while (f < LabList[labID].noOfTAs)
        {
            if (LabList[labID].courseDone[f] < LabList[labID].max)
            {
                TAsOver = 0;
            }
            f++;
        }
        l++;
    }
    if (TAsOver)
    {
        CourseList[id].isExisting = 0;
        CourseList[id].isOn = 0;
        return 1;
    }
    return 0;
}

void *StudentSimulator(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    int time = StudentList[id].time;
    sleep(time);
    int valid = 1;
    student_PreferenceFilling(id);
    while(valid)
    {
        student_courseApplication(id);
        sleep(2);
        student_CourseSelection(id);
        if (student_Removed(id) == 1)
        {
            printf("Student %d Removed\n", id);
            break;
        }
    }
    return NULL;
}

int student_Removed(int id)
{
    if (StudentList[id].existing == 0)
    {
        return 1;
    }
    return 0;
}

void student_PreferenceFilling(int id)
{
    StudentList[id].existing = 1;
    printf("Student %d has filled in preference for course registration\n", id);
}

void student_courseApplication(int id)
{
    int yes = 1;
    int no = 0;
    int courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];
    int nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
    int pos = StudentList[id].prefPos;
    StudentList[id].isLearning = no;
L2:
    if (CourseList[courseWantedID].isExisting)
    {
        if (!CourseList[courseWantedID].isOn)
        {
            pthread_mutex_lock(&student_CourseApplication_lock);
            if (CourseList[courseWantedID].currentStudentSize - CourseList[courseWantedID].D < 0)
            {
                CourseList[courseWantedID].currentStudentSize = CourseList[courseWantedID].currentStudentSize+1;
                StudentList[id].isLearning = yes;
                char course[100];
                strcpy(course,CourseList[courseWantedID].CourseName);
                printf("Student %d has been allocated a seat in course %s\n", id, course);
            }
            pthread_mutex_unlock(&student_CourseApplication_lock);
        }
        else
        {
            pthread_mutex_lock(&mutex_lock2[courseWantedID]);
            while (CourseList[courseWantedID].isOn)
            {
                pthread_cond_wait(&c2[courseWantedID], &mutex_lock2[courseWantedID]);
            }

            //printf("GOTTT signal for course StudID : %d prefPos: %d\n", id, StudentList[id].prefPos);

            if (course_Removed(courseWantedID) == 1)
            {
                printf("Course Removed : %d for Student ID %d\n", courseWantedID, id);
                goto L1;
            }

            if (CourseList[courseWantedID].isExisting)
            {
                if (!CourseList[courseWantedID].isOn)
                {
                    pthread_mutex_lock(&student_CourseApplication_lock);
                    if (CourseList[courseWantedID].currentStudentSize - CourseList[courseWantedID].D<0)
                    {
                        CourseList[courseWantedID].currentStudentSize = CourseList[courseWantedID].currentStudentSize+1;
                        StudentList[id].isLearning = yes;
                        char course[100];
                        strcpy(course,CourseList[courseWantedID].CourseName);
                        printf("Student %d has been allocated a seat in course %s\n", id, course);
                    }
                    pthread_mutex_unlock(&student_CourseApplication_lock);
                }
            }
            pthread_mutex_unlock(&mutex_lock2[courseWantedID]);
        }
    }
    else
    {
    L1:
        if (StudentList[id].prefPos -2 == 0)
        {
            StudentList[id].isLearning = yes;
            StudentList[id].existing = no;
            studentsOver = studentsOver+1;
            printf("Student %d could not get any of his preferred courses\n", id);
        }
        else
        {
            char priority_one[100];
            strcpy(priority_one,CourseList[courseWantedID].CourseName);
            char priority_two[100];
            strcpy(priority_two,CourseList[nextCourseWantedID].CourseName);
            printf("Student %d has changed current prefernence from %s (priority %d) to %s (priority %d)\n", id,priority_one, pos, priority_two, pos+1);
            pos = pos+1;
            StudentList[id].prefPos = StudentList[id].prefPos+1;
            courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];
            nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
            StudentList[id].isLearning = no;
            goto L2;
        }
    }

    if (!StudentList[id].isLearning)
    {
        goto L2;
    }
}

void student_CourseSelection(int id)
{
    int yes = 1;
    int no = 0;
    int courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];

    pthread_mutex_lock(&mutex_lock[courseWantedID]);
    while (CourseList[courseWantedID].isOn)
    {
        pthread_cond_wait(&c[courseWantedID], &mutex_lock[courseWantedID]);
    }
    pthread_mutex_unlock(&mutex_lock[courseWantedID]);

    float prob;
    int nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
    int pos = StudentList[id].prefPos;
    //printf("StudID : %d prefPos : %d CourseWanted Id : %d next : %d\n", id, pos, courseWantedID, nextCourseWantedID);
    prob = StudentList[id].calibre * CourseList[courseWantedID].interestQ;
    //printf("prob :%f id: %d\n", prob, id);
    if (prob > 0.5)
    {
        studentsOver = studentsOver+1;
        StudentList[id].existing = no;
        char course[100];
        strcpy(course,CourseList[courseWantedID].CourseName);
        printf("Student %d has selected course %s permanently\n", id, course);
    }
    else
    {
        if (StudentList[id].existing)
        {
            if (StudentList[id].prefPos - 2 == 0)
            {
                StudentList[id].existing = no;
                studentsOver = studentsOver+1;
                printf("Student %d could not get any of his preferred courses\n", id);
            }
            else
            {
                char priority_one[100];
                strcpy(priority_one,CourseList[courseWantedID].CourseName);
                char priority_two[100];
                strcpy(priority_two,CourseList[nextCourseWantedID].CourseName);
                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", id, priority_one, pos, priority_two, pos+1);
                pos++;
                StudentList[id].prefPos++;
            }
        }
    }
}
void input()
{
    scanf("%lld", &num_student);
    scanf("%lld", &num_labs);
    scanf("%lld", &num_courses);
    int i = 0;
    while (i < num_courses)
    {
        scanf("%s", CourseList[i].CourseName);
        scanf("%f", &CourseList[i].interestQ);
        scanf("%lld", &CourseList[i].course_max_slots);
        scanf("%lld", &CourseList[i].p);

        int j = 0;
        while (j < CourseList[i].p)
        {
            scanf("%lld", &CourseList[i].listp[j]);
            j++;
        }
        CourseList[i].currentStudentSize = 0;
        CourseList[i].isOn = 0;
        CourseList[i].isExisting = 1;
        CourseList[i].TA_Allocated = -1;
        i++;
    }
    i = 0;
    while (i < num_student)
    {
        scanf("%f", &StudentList[i].calibre);
        scanf("%lld", &StudentList[i].courseIDs[0]);
        scanf("%lld", &StudentList[i].courseIDs[1]);
        scanf("%lld", &StudentList[i].courseIDs[2]);
        scanf("%f", &StudentList[i].time);
        StudentList[i].isLearning = 0;
        StudentList[i].prefPos = 0;
        StudentList[i].existing = 1;
        i++;
    }

    i = 0;
    while (i < num_labs)
    {
        scanf("%s", &LabList[i].labName);
        scanf("%lld", &LabList[i].noOfTAs);
        scanf("%lld", &LabList[i].max);
        memset(LabList[i].courseDone, 0, sizeof(LabList[i].courseDone));
        memset(LabList[i].busy, 0, sizeof(LabList[i].busy));
        LabList[i].busy[i] = 0;
        i++;
    }
}

int main()
{
    pthread_mutex_init(&course_TA_allocation_lock, NULL);
    pthread_mutex_init(&student_CourseApplication_lock, NULL);
    studentsOver = 0;
    srand(time(0));
    input();
    printf("\n");
    int l = 0;
    while (l < num_student)
    {
        pthread_t curr_tid;
        struct thread_details *thread_input = (struct thread_details *)malloc(sizeof(struct thread_details));
        thread_input->id = l;
        pthread_create(&curr_tid, NULL, StudentSimulator, (void *)(thread_input));
        studentThreadArr[l] = curr_tid;
        l++;
    }

    l = 0;
    while (l < num_courses)
    {
        pthread_t curr_tid;
        struct thread_details *thread_input = (struct thread_details *)malloc(sizeof(struct thread_details));
        thread_input->id = l;
        pthread_create(&curr_tid, NULL, courseSimulator, (void *)(thread_input));
        courseThreadArr[l] = curr_tid;
        l++;
    }
    l = 0;
    while (l < num_courses)
    {
        pthread_join(courseThreadArr[l], NULL);
        l++;
    }
    l = 0;
    while (l < num_student)
    {
        pthread_join(studentThreadArr[l], NULL);
        l++;
    }
}
//###########FILE CHANGE ./main_folder/Anurag Gupta_305791_assignsubmission_file_/Final/q1/headers.h ####################//

#include<stdio.h>
#include <stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include <string.h>
#include<time.h>

void *courseSimulator(void *arg);
void position_giver(int pos);
void course_size_allocation(int id);
void course_TA_allocation(intid);
void course_start(int id);
void tut_over(int id);
int course_Removed(int id);
void student_CourseSelection(int id);
void student_courseApplication(int id);
void student_PreferenceFilling(int id);
int student_Removed(int id);
void *StudentSimulator(void *arg);


time_t startTime;
int studentsOver;


pthread_t courseThreadArr[10000];
pthread_t studentThreadArr[10000];
pthread_mutex_t course_TA_allocation_lock;
pthread_mutex_t student_CourseApplication_lock;
pthread_mutex_t mutex_lock[100];
pthread_cond_t c[100];

pthread_mutex_t mutex_lock2[100];
pthread_cond_t c2[100];

int turns[100];
struct Student
{
    float calibre;
    int courseIDs[3];
    float time;
    int isLearning;
    int prefPos;
    int existing;
};

struct Course
{
    char CourseName[50];
    float interestQ;
    int course_max_slots;
    int p;
    int listp[100];

    int D;
    int currentStudentSize;

    int isOn;
    int isExisting;

    char courseTA[100];
    int labID_Allocated;
    int TA_Allocated;
};

struct Lab
{
    char labName[50];
    int noOfTAs;
    int courseDone[100];
    int busy[100];
    int max;
};

int num_labs;
int num_courses;
int num_student;
int  i;
int  j;
char position[50];
struct Course CourseList[100];
struct Lab LabList[100];
struct Student StudentList[100];

struct thread_details
{
    int id;
};
//////////////END/////////////////