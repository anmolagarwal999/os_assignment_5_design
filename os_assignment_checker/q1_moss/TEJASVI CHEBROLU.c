
//###########FILE CHANGE ./main_folder/TEJASVI CHEBROLU_305915_assignsubmission_file_/2019114005/q1/src/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define STRING_LEN 1000
#define PEOPLE_LEN 100
#define COURSES_LEN 100
#define LAB_NUM 100
#define QUANTITY_NUM 100

const char mutex_error[40] = "\nError while mutex intialisation.\n";
const char conditional_error[40] = "\nError while conditional intialisation.\n";

struct Student
{
    float calibre;
    int id;
    int current_course;
    pthread_mutex_t student_lock;
    int preferences[COURSES_LEN];
    int pref_filling_time;
};

struct Lab
{
    char name[STRING_LEN];
    int id;
    int ta_current_number[PEOPLE_LEN];
    int num_students;
    int ta_course_limit;
    pthread_mutex_t ta_mutex[PEOPLE_LEN];
    int ta_occupied[PEOPLE_LEN];
};

struct Course
{
    int isCourseAvailable;
    char name[STRING_LEN];
    pthread_cond_t course_available;
    int id;
    int labs[LAB_NUM];
    int num_labs;
    float interest;
    int totalSlots;
    pthread_cond_t runningTutes;
    int leftSlots;
    int currentLab;
    pthread_mutex_t course_lock;
    int currentTA;
    int isCourseDeleted;
};

struct Lab labss[QUANTITY_NUM];
int numStudents, numLabs, numCourses;
void takeInput();
pthread_t courseThread[QUANTITY_NUM];
void *runCourse(void *args);
struct Student students[QUANTITY_NUM];
void *runStudent(void *args);
pthread_t studentThread[QUANTITY_NUM];
struct Course courses[QUANTITY_NUM];


void takeInput()
{
    for (int i = 0; i < numCourses; i++)
    {
        courses[i].currentLab = -1;
        courses[i].id = i;
        courses[i].currentTA = -1;
        int num_inputs = 0;
        scanf("%s %f", courses[i].name, &courses[i].interest);
        scanf("%d %d", &courses[i].totalSlots, &courses[i].num_labs);
        int end = courses[i].num_labs;
        for (int j = 0; j < end; j++)
        {
            num_inputs++;
            scanf("%d", &courses[i].labs[j]);
        }
        bool is_p_init = pthread_cond_init(&courses[i].course_available, NULL); 
        if (is_p_init)
        {
            printf("%s", conditional_error);
        }
        if (pthread_cond_init(&courses[i].runningTutes, NULL))
        {
            printf("%s", conditional_error);
        }
        if (pthread_mutex_init(&courses[i].course_lock, NULL))
        {
            printf("%s", mutex_error);
        }
    }
    for (int i = 0; i < numStudents; i++)
    {
        students[i].id = i;
        bool is_init = pthread_mutex_init(&students[i].student_lock, NULL);
        if (is_init)
        {
            printf("%s", mutex_error);
        }
        scanf("%f %d", &students[i].calibre, &students[i].preferences[0]);
        scanf("%d", &students[i].preferences[1]);
        scanf("%d %d",&students[i].preferences[2], &students[i].pref_filling_time);
    }
    int tas = 0;
    for (int i = 0; i < numLabs; i++)
    {
        labss[i].id = i;
        scanf("%s", labss[i].name);
        scanf("%d", &labss[i].num_students);
        int ends = labss[i].num_students;
        scanf("%d", &labss[i].ta_course_limit);
        for (int j = 0; j < ends; j++)
        {
            labss[i].ta_current_number[j] = labss[i].ta_occupied[j] = 0;
        }
    }
}
void *runCourse(void *args)
{
    int courseID = ((struct Course *)args)->id;
    struct Course *newCourse = (struct Course *)args;
    int flag = 0, tas = 0;
    while (true)
    {
        int getTA = 0, doneSlotsTAs = 0, totalTAs = 0;
        int printer = 0, selectedTA = 0, selectedLab = 0;
        for (int i = 0; i < newCourse->num_labs; i++)
        {
            struct Lab *labToCheck = &labss[newCourse->labs[i]];
            flag = 0;
            totalTAs += labToCheck->num_students;
            int limit = labToCheck->ta_course_limit;
            for (int j = 0; j < labToCheck->num_students; j++)
            {
                pthread_mutex_lock(&labToCheck->ta_mutex[j]);
                bool is_true = labToCheck->ta_current_number[j] < limit && labToCheck->ta_occupied[j] != 1; 
                if (is_true)
                {
                    labToCheck->ta_current_number[j]++;
                    labToCheck->ta_occupied[j] = 1;
                    getTA = 1;
                    printer = labToCheck->ta_current_number[j];
                    flag = 1;
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                    selectedTA = j;
                    selectedLab = labToCheck->id;
                    courses[courseID].currentTA = j;
                    courses[courseID].currentLab = labToCheck->id;
                    int n_p = printer + 1;
                    printf("TA %d from lab %s has been allocated to course %s for his %d TA ship\n", j, labToCheck->name, newCourse->name, n_p);
                    break;
                }
                else if (labToCheck->ta_current_number[j] == limit)
                {
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                    doneSlotsTAs += 1;
                    continue;
                }
                else
                {
                    pthread_mutex_unlock(&labToCheck->ta_mutex[j]);
                    tas += 1;
                }
            }
            if (flag)
            {
                break;
            }
        }
        bool is_done = doneSlotsTAs >= totalTAs && courses[courseID].isCourseAvailable == 0;
        if (is_done)
        {
            courses[courseID].isCourseDeleted = 1;
            tas = 0;
            pthread_cond_broadcast(&courses[courseID].course_available);
            int c_id = courseID;
            printf("Course %d doesn't have any TA's eligible and is removed from the course offering.\n", c_id);
            break;
        }
        int temp = rand() % courses[courseID].totalSlots;
        courses[courseID].isCourseAvailable = 1;
        pthread_mutex_lock(&courses[courseID].course_lock);
        int cs = 0;
        courses[courseID].leftSlots = temp;
        pthread_mutex_unlock(&courses[courseID].course_lock);
        cs *= temp;
        pthread_cond_broadcast(&courses[courseID].course_available);
        courses[courseID].isCourseAvailable = 0;
        tas += cs;
        sleep(1);
        pthread_cond_broadcast(&courses[courseID].course_available);
        int n_seats = temp - courses[courseID].leftSlots;
        printf("Course %s has been alloted %d seats.\n", courses[courseID].name, temp);
        if (temp != courses[courseID].leftSlots)
        {
            printf("Tutorial has started for Course %s with %d seats filled out of %d\n", courses[courseID].name, n_seats, temp);
        }
        sleep(1);
        tas = 0;
        pthread_cond_broadcast(&courses[courseID].runningTutes);
        courses[courseID].currentLab = courses[courseID].currentTA = -1;
        labss[selectedLab].ta_occupied[selectedTA] = 0;
    }
}

void *runStudent(void *args)
{
    int studentID = ((struct Student *)args)->id;
    int flag = 0;
    int retval = -1;
    int bytes = 0;
    for (int i = 0; i < 3; i++)
    {
        int coursesAlloted = 0;
        int courseID = students[studentID].preferences[i];
        while (1)
        {
            pthread_mutex_lock(&courses[courseID].course_lock);
            bool isDeleted = courses[courseID].isCourseDeleted;
            if (isDeleted)
            {
                flag = 1;
                pthread_mutex_unlock(&courses[courseID].course_lock);
                break;
            }
            pthread_mutex_unlock(&courses[courseID].course_lock);
            int tempFlag = 1;
            pthread_mutex_lock(&students[studentID].student_lock);
            pthread_cond_wait(&courses[courseID].course_available, &students[studentID].student_lock);
            retval = flag;
            pthread_mutex_unlock(&students[studentID].student_lock);
            tempFlag = pthread_mutex_lock(&courses[courseID].course_lock);
            bool isCourseAvailable = courses[courseID].isCourseAvailable;
            if (isCourseAvailable)
            {
                bool areSlotsLeft = courses[courseID].leftSlots > 0;
                if (areSlotsLeft)
                {
                    courses[courseID].leftSlots -= 1;
                    if (courses[courseID].leftSlots == 0)
                    {
                        courses[courseID].isCourseAvailable = 0;
                    }
                    coursesAlloted = 1;
                    pthread_mutex_unlock(&courses[courseID].course_lock);
                    break;
                }
            }
            retval = 1;
            pthread_mutex_unlock(&courses[courseID].course_lock);
            if (coursesAlloted)
            {
                break;
            }
        }
        int s_id = studentID;
        if (flag)
        {
            continue;
        }
        printf("Student %d has been allocated a seat in course %s\n", s_id, courses[courseID].name);
        retval = 0;
        pthread_mutex_lock(&students[studentID].student_lock);
        bytes ++;
        pthread_cond_wait(&courses[courseID].runningTutes, &students[studentID].student_lock);
        int TrueFalse = (rand() % 100) < (int)(students[studentID].calibre * courses[courseID].interest * 100);
        pthread_mutex_unlock(&students[studentID].student_lock);
        if (TrueFalse)
        {
            s_id = studentID;
            printf("Student %d has selected course %d permanently.\n", s_id, courseID);
        }
        else
        {
            s_id = studentID;
            printf("Student %d has withdrawn from the course %s.\n", s_id, courses[courseID].name);
            if (i < 2)
            {
                int p_id = i + 1;
                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d).\n", s_id, courses[courseID].name, i, courses[students[studentID].preferences[i + 1]].name, p_id);
            }
        }
    }
}

int main()
{
    scanf("%d", &numStudents);
    scanf("%d", &numLabs);
    scanf("%d", &numCourses);
    takeInput();
    int c = 0;
    for (int i = 0; i < numStudents; i++)
    {
        c += numStudents;
        pthread_create(&studentThread[i], NULL, runStudent, (void *)(&students[i]));
    }
    sleep(1);
    int thread_len = 2 * numCourses + numStudents;
    for (int i = 0; i < thread_len; i++)
    {
        if (i < numCourses)
        {
            pthread_create(&courseThread[i], NULL, runCourse, (void *)(&courses[i]));
        }
        else if (i >= numCourses && i < numCourses + numStudents)
        {
            pthread_join(studentThread[i - numCourses], NULL);
        }
        else
        {
            pthread_join(courseThread[-numCourses - numStudents + i], NULL);
        }
    }
    fflush(0);
    return 0;
}