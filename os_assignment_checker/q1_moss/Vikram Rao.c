
//###########FILE CHANGE ./main_folder/Vikram Rao_305985_assignsubmission_file_/2020101123/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <pthread.h>

#define ANSI_COLOR_RED       "\x1b[31m"
#define ANSI_COLOR_GREEN     "\x1b[32m"
#define ANSI_COLOR_YELLOW    "\x1b[33m"
#define ANSI_COLOR_BLUE      "\x1b[34m"
#define ANSI_COLOR_MAGENTA   "\x1b[35m"
#define ANSI_COLOR_CYAN      "\x1b[36m"
#define ANSI_COLOR_RESET     "\x1b[0m"

// For errors
#define ANSI_COLOR_RED_BOLD  "\x1b[1;31m"

// Total number of labs
#define MAXLABS 5

typedef struct student_info {
    int idx;
    int p1;
    int accept1;
    int p2;
    int accept2;
    int p3;
    int accept3;
    int time;
    int status;
} student_info;

typedef struct course_info {
    char name[10];
    int num_seats;
} course_info;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *thread_func(void *arg)
{
    student_info *st = (student_info *)arg;
    sleep(st->time);

    pthread_mutex_lock(&lock);
    printf("Student %d has filled " ANSI_COLOR_MAGENTA "in" ANSI_COLOR_RESET " preferences " ANSI_COLOR_MAGENTA "for" ANSI_COLOR_RESET " course registration\n", st->idx);
    pthread_mutex_unlock(&lock);

    return NULL;
}

void *course_thread_func(void *inp)
{
    course_info *course = (course_info *)inp;

    pthread_mutex_lock(&lock);
    printf("Course %s has been allocated %d seats\n", course->name, course->num_seats);
    pthread_mutex_unlock(&lock);

    return NULL;
}

int main() 
{
    int i = 0, j = 0;
    int num_students, num_labs, num_courses;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    char courses[num_courses][10];
    float interest_quotients[num_courses];
    int max_slots[num_courses], num_TA_labs[num_courses], lab_ids[num_courses][MAXLABS];

    for (i = 0; i < num_courses; i++) {
        for (j = 0; j < MAXLABS; j++) {
            lab_ids[i][j] = -1;
        }
    }
    for (i = 0; i < num_courses; i++) {
        scanf("%s %f %d %d", courses[i], interest_quotients + i, \
                            max_slots + i, num_TA_labs + i);
        int n = num_TA_labs[i];
        if (n <= 0) {
            printf(ANSI_COLOR_RED_BOLD "Error: Labs must be at least 1" \
                    ANSI_COLOR_RESET "\n");
            return 1;
        }
        if (n > MAXLABS) {
            printf(ANSI_COLOR_RED_BOLD "Error: Too many labs selected" \
                    ANSI_COLOR_RESET "\n");
            return 1;
        }
        for (j = 0; j < n; j++) {
            scanf("%d", &lab_ids[i][j]);
        }
    }

    float calibre_quotients[num_students];
    int choices[num_students][3], regtime[num_students];

    for (i = 0; i < num_students; i++) {
        scanf("%f %d %d %d %d", calibre_quotients + i, \
                            &choices[i][0], &choices[i][1], &choices[i][2], regtime + i);
    }

    char labs[num_labs][10];
    int num_members[num_labs], max_TA[num_labs];

    for (i = 0; i < num_labs; i++) {
        scanf("%s %d %d", labs[i], num_members + i, max_TA + i);
    }

    pthread_t students[num_students];
    student_info arguments[num_students];

    for (i = 0; i < num_students; i++) {
        arguments[i].idx = i;
        arguments[i].p1 = choices[i][0];
        arguments[i].p2 = choices[i][1];
        arguments[i].p3 = choices[i][2];
        arguments[i].accept1 = 0;
        arguments[i].accept2 = 0;
        arguments[i].accept3 = 0;
        arguments[i].time = regtime[i];
        arguments[i].status = -1;
        pthread_create(&students[i], NULL, thread_func, (void *)&arguments[i]);
    }

    for (i = 0; i < num_students; i++) {
        pthread_join(students[i], NULL);
    }

    pthread_t course_threads[num_courses];
    course_info params[num_courses];

    for (i = 0; i < num_courses; i++) {
        strcpy(params[i].name, courses[i]);
        params[i].num_seats = max_slots[i];
        pthread_create(&course_threads[i], NULL, course_thread_func, (void *)&params[i]);
    }

    for (i = 0; i < num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }

    return 0;
}