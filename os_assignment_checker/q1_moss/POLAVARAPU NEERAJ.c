
//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/studentthread.c ####################//

#include "headers.h"

void *student(void *arg)
{
    int id = (long int)arg;
    sleep(timepref[id]);
    printf(COLOR_GREEN "Student %d has filled in preferences for course registration\n" COLOR_RESET, id);
    for (int prefid = 0; prefid < 3; prefid++)
    {
        pthread_mutex_lock(&mutex[pref[id][prefid]]);
        push(pref[id][prefid], id);
        pthread_mutex_unlock(&mutex[pref[id][prefid]]);

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }
        else
        {
            sem_wait(&stud[id]);
        }

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }

        sem_wait(&slot[id][pref[id][prefid]]);
        printf(COLOR_BLUE "Student %d has been allocated a seat in course %s\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);

        sem_wait(&tut[id][pref[id][prefid]]);

        float prob = interest[id] * calibreofstudent[pref[id][prefid]];
        float random;
        random = (rand() % 100 + 1) / (float)100;
        if (prob >= random)
        {
            printf(COLOR_CYAN "Student %d has selected course %s permanently\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);
            return NULL;
        }
        else
        {
            if (prefid < 2)
            {
                printf(COLOR_MAGENTA "Student %d has withdrawn from the course %s\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]]);
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
            }
            else
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
            }
        }
    l1:;
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/._queue.c ####################//

#include "headers.h"

void *student(void *arg)
{
    int id = (long int)arg;
    sleep(timepref[id]);
    printf(COLOR_GREEN "Student %d has filled in preferences for course registration\n" COLOR_RESET, id);
    for (int prefid = 0; prefid < 3; prefid++)
    {
        pthread_mutex_lock(&mutex[pref[id][prefid]]);
        push(pref[id][prefid], id);
        pthread_mutex_unlock(&mutex[pref[id][prefid]]);

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }
        else
        {
            sem_wait(&stud[id]);
        }

        if (coursedead[pref[id][prefid]] == 1)
        {
            if (prefid < 2)
            {
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
                goto l1;
            }
            if (prefid == 2)
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
                return NULL;
            }
        }

        sem_wait(&slot[id][pref[id][prefid]]);
        printf(COLOR_BLUE "Student %d has been allocated a seat in course %s\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);

        sem_wait(&tut[id][pref[id][prefid]]);

        float prob = interest[id] * calibreofstudent[pref[id][prefid]];
        float random;
        random = (rand() % 100 + 1) / (float)100;
        if (prob >= random)
        {
            printf(COLOR_CYAN "Student %d has selected course %s permanently\n" COLOR_WHITE, id, nameofcourse[pref[id][prefid]]);
            return NULL;
        }
        else
        {
            if (prefid < 2)
            {
                printf(COLOR_MAGENTA "Student %d has withdrawn from the course %s\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]]);
                printf(COLOR_YELLOW "Student %d has changed current preference from %s(priority %d) to %s(priority %d)\n" COLOR_RESET, id, nameofcourse[pref[id][prefid]], prefid + 1, nameofcourse[pref[id][prefid + 1]], prefid + 2);
            }
            else
            {
                printf(COLOR_WHITE "Student %d could not get any of his preferred courses\n" COLOR_RESET, id);
            }
        }
    l1:;
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/coursethread.c ####################//

#include "headers.h"

void *course(void *arg)
{
    int itlabs = 0;
    int id = (long int)arg;

    while (1)
    {
        if (queuesize[id] == 0)
        {
            arr[id] = 1;
            sem_wait(&courseslots[id]);
            arr[id] = 0;
        }
    l1:;

        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
        {
            itlabs++;
            if (itlabs < nooflabs[id])
                goto l1;
        }

        pthread_mutex_lock(&queuesizelock[id]);
        int filled;
        int slots = rand() % maxslots[id] + 1;
        if (slots <= queuesize[id])
            filled = slots;
        else
            filled = queuesize[id];
        pthread_mutex_unlock(&queuesizelock[id]);

        if (itlabs >= nooflabs[id])
        {
            coursedead[id] = 1;
            struct queue *temp = front[id];
            while (temp != NULL)
            {
                int y = temp->data;
                sem_post(&stud[y]);
                temp = temp->next;
            }
            printf(COLOR_BLUE "Course %s does not have any TA mentors eligible and is removed from course offerings\n" COLOR_RESET, nameofcourse[id]);
            return NULL;
        }

        struct queue *temp = front[id];
        for (int j = 0; j < filled; j++)
        {
            int y = temp->data;
            sem_post(&stud[y]);
            temp = temp->next;
        }

        pthread_mutex_lock(&mutex1[listoflabs[id][itlabs]]);
        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
            goto l1;
        sem_wait(&labcount[listoflabs[id][itlabs]]);
        if (talab[listoflabs[id][itlabs]] == labmax[listoflabs[id][itlabs]])
            goto l1;
        int x = labpop(listoflabs[id][itlabs]);
        tacount[listoflabs[id][itlabs]][x]++;
        talab[listoflabs[id][itlabs]]++;
        pthread_mutex_unlock(&mutex1[listoflabs[id][itlabs]]);

        printf(COLOR_RED "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" COLOR_RED, x, labname[listoflabs[id][itlabs]], nameofcourse[id], tacount[listoflabs[id][itlabs]][x]);

        printf(COLOR_MAGENTA "Course %s has been allocated %d seats\n" COLOR_RESET, nameofcourse[id], slots);

        int it = 0;
        int arrdone[10000];
        for (int j = 0; j < filled; j++)
        {
            pthread_mutex_lock(&mutex[id]);
            int x = pop(id);
            pthread_mutex_unlock(&mutex[id]);
            arrdone[it] = x;
            it++;
            sem_post(&slot[x][id]);
        }

        printf(COLOR_CYAN "Tutorial has started for Course %s with %d seats filled out of %d\n" COLOR_YELLOW, nameofcourse[id], filled, slots);

        sleep(2);

        for (int j = 0; j < filled; j++)
            sem_post(&tut[arrdone[j]][id]);

        printf(COLOR_YELLOW "TA %d from lab %s has completed the tutorial for course %s\n" COLOR_RESET, x, labname[listoflabs[id][itlabs]], nameofcourse[id]);
        if (tacount[listoflabs[id][itlabs]][x] < limit[listoflabs[id][itlabs]])
        {
            pthread_mutex_lock(&mutex1[listoflabs[id][itlabs]]);
            labpush(listoflabs[id][itlabs], x);
            sem_post(&labcount[listoflabs[id][itlabs]]);
            pthread_mutex_unlock(&mutex1[listoflabs[id][itlabs]]);
        }
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/main.c ####################//

#include "headers.h"

pthread_mutex_t mutex1[numberofcourses_max];
pthread_mutex_t mutex[numberofcourses_max];
pthread_mutex_t queuesizelock[numberofcourses_max];

sem_t *slot[numberofstudents_max];
sem_t *tut[numberofstudents_max];
sem_t labcount[numberoflabs_max];
sem_t stud[numberofstudents_max];
sem_t courseslots[numberofcourses_max];

int main()
{
    scaninput();

    // initiated locks and variables
    for (int i = 0; i < numberofstudents; i++)
    {
        sem_init(&stud[i], 0, 0);
        slot[i] = (sem_t *)malloc(numberofcourses * sizeof(sem_t));
        tut[i] = (sem_t *)malloc(numberofcourses * sizeof(sem_t));
        for (int j = 0; j < numberofcourses; j++)
        {
            sem_init(&slot[i][j], 0, 0);
            sem_init(&tut[i][j], 0, 0);
        }
    }

    for (int i = 0; i < numberoflabs; i++)
    {
        sem_init(&labcount[i], 0, numberoftas[i]);
        labqueuesize[i] = numberoftas[i];
        for (int j = 0; j < numberoftas[i]; j++)
        {
            tacount[i][j] = 0;
        }
        for (int j = 0; j < numberoftas[i]; j++)
        {
            labpush(i, j);
        }
        accesslab[i] = 1;
        talab[i] = 0;
        labmax[i] = numberoftas[i] * limit[i];
    }

    for (int i = 0; i < numberofcourses; i++)
    {
        sem_init(&courseslots[i], 0, 0);
        pthread_mutex_init(&mutex[i], NULL);
        pthread_mutex_init(&mutex1[i], NULL);
        pthread_mutex_init(&queuesizelock[i], NULL);
        queuesize[i] = 0;
        coursedead[i] = 0;
        arr[i] = 0;
    }
    // initiated locks and variables

    srand(time(0));
    srand(time(0));

    //initialised and created thread for each student, course, lab
    pthread_t students[numberofstudents];
    pthread_t courses[numberofcourses];
    pthread_t labs[numberoflabs];
    for (long int i = 0; i < numberofstudents; i++)
    {
        int x = pthread_create(&students[i], NULL, student, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    for (long int i = 0; i < numberofcourses; i++)
    {
        int x = pthread_create(&courses[i], NULL, &course, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    for (long int i = 0; i < numberoflabs; i++)
    {
        int x = pthread_create(&labs[i], NULL, &lab, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }
    //initialised and cread thread for each student, course, lab done.

    //making the main function thread to run until all other threads close.
    for (int i = 0; i < numberofstudents; i++)
        pthread_join(students[i], NULL);

    /// destroying locks
    for (int i = 0; i < numberofcourses; i++)
    {
        pthread_mutex_destroy(&queuesizelock[i]);
        pthread_mutex_destroy(&mutex[i]);
        pthread_mutex_destroy(&mutex1[i]);
        sem_destroy(&courseslots[i]);
    }
    for (int i = 0; i < numberofstudents; i++)
    {
        sem_destroy(&stud[i]);
        for (int j = 0; j < numberofcourses; j++)
        {
            sem_destroy(&slot[i][j]);
            sem_destroy(&tut[i][j]);
        }
    }
    for (int i = 0; i < numberoflabs; i++)
        sem_destroy(&labcount[i]);
    /// destroying locks
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/scaninput.c ####################//

#include "headers.h"

void scaninput()
{
    scanf("%d %d %d", &numberofstudents, &numberoflabs, &numberofcourses);
    for (int i = 0; i < numberofcourses; i++)
    {
        scanf("%s", nameofcourse[i]);
        scanf("%f", &interest[i]);
        scanf("%d", &maxslots[i]);
        scanf("%d", &nooflabs[i]);
        for (int j = 0; j < nooflabs[i]; j++)
        {
            scanf("%d", &listoflabs[i][j]);
        }
    }
    for (int i = 0; i < numberofstudents; i++)
    {
        scanf("%f", &calibreofstudent[i]);
        for (int j = 0; j < 3; j++)
            scanf("%d", &pref[i][j]);
        scanf("%d", &timepref[i]);
    }

    for (int i = 0; i < numberoflabs; i++)
    {
        scanf("%s", labname[i]);
        scanf("%d", &numberoftas[i]);
        scanf("%d", &limit[i]);
    }

    // numberofstudents = 10;
    // numberoflabs = 3;
    // numberofcourses = 4;
    // strcpy(nameofcourse[0], "SMAI");
    // strcpy(nameofcourse[1], "NLP");
    // strcpy(nameofcourse[2], "CV");
    // strcpy(nameofcourse[3], "DSA");
    // interest[0] = 0.8;
    // interest[1] = 0.95;
    // interest[2] = 0.9;
    // interest[3] = 0.75;
    // maxslots[0] = 3;
    // maxslots[1] = 4;
    // maxslots[2] = 2;
    // maxslots[3] = 5;
    // nooflabs[0] = 2;
    // nooflabs[1] = 1;
    // nooflabs[2] = 2;
    // nooflabs[3] = 3;
    // listoflabs[0][0] = 0;
    // listoflabs[0][1] = 2;
    // listoflabs[1][0] = 0;
    // listoflabs[2][0] = 1;
    // listoflabs[2][1] = 2;
    // listoflabs[3][0] = 0;
    // listoflabs[3][1] = 1;
    // listoflabs[3][2] = 2;
    // calibreofstudent[0] = 0.8;
    // calibreofstudent[1] = 0.6;
    // calibreofstudent[2] = 0.85;
    // calibreofstudent[3] = 0.5;
    // calibreofstudent[4] = 0.75;
    // calibreofstudent[5] = 0.95;
    // calibreofstudent[6] = 0.4;
    // calibreofstudent[7] = 0.1;
    // calibreofstudent[8] = 0.85;
    // calibreofstudent[9] = 0.3;
    // pref[0][0] = 0;
    // pref[1][0] = 3;
    // pref[2][0] = 2;
    // pref[3][0] = 1;
    // pref[4][0] = 0;
    // pref[5][0] = 1;
    // pref[6][0] = 3;
    // pref[7][0] = 0;
    // pref[8][0] = 1;
    // pref[9][0] = 0;

    // pref[0][1] = 3;
    // pref[1][1] = 1;
    // pref[2][1] = 1;
    // pref[3][1] = 2;
    // pref[4][1] = 2;
    // pref[5][1] = 0;
    // pref[6][1] = 0;
    // pref[7][1] = 3;
    // pref[8][1] = 0;
    // pref[9][1] = 1;

    // pref[0][2] = 1;
    // pref[1][2] = 2;
    // pref[2][2] = 0;
    // pref[3][2] = 3;
    // pref[4][2] = 1;
    // pref[5][2] = 2;
    // pref[6][2] = 2;
    // pref[7][2] = 1;
    // pref[8][2] = 3;
    // pref[9][2] = 2;

    // timepref[0] = 1;
    // timepref[1] = 3;
    // timepref[2] = 1;
    // timepref[3] = 2;
    // timepref[4] = 3;
    // timepref[5] = 2;
    // timepref[6] = 3;
    // timepref[7] = 2;
    // timepref[8] = 1;
    // timepref[9] = 1;

    // strcpy(labname[0], "PRECOG");
    // strcpy(labname[1], "CVIT");
    // strcpy(labname[2], "RRC");

    // numberoftas[0] = 3;
    // numberoftas[1] = 4;
    // numberoftas[2] = 1;

    // limit[0] = 1;
    // limit[1] = 2;
    // limit[2] = 3;
}
//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/labthread.c ####################//

#include "headers.h"

void *lab(void *arg)
{
    int id = (long int)arg;
    while (1)
    {
        for (int i = 0; i < numberoftas[id]; i++)
        {
            if (tacount[id][i] < limit[id])
            {
                goto l1;
            }
        }
        accesslab[id] = 0;
        printf(COLOR_GREEN "Lab %s no longer has students available for TA ship\n" COLOR_RESET, labname[id]);
        return NULL;
    l1:;
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/queue.c ####################//

#include "headers.h"

void push(int id, int value)
{
    struct queue *temp;
    temp = (struct queue *)malloc(sizeof(struct queue));

    if (temp != NULL)
    {
        temp->data = value;
        if (front[id] == NULL)
        {
            front[id] = temp;
            rear[id] = temp;
            front[id]->next = NULL;
            rear[id]->next = NULL;
        }
        else
        {
            rear[id]->next = temp;
            rear[id] = temp;
            rear[id]->next = NULL;
        }

        queuesize[id]++;
        if (queuesize[id] == 1 && arr[id] == 1)
        {
            sem_post(&courseslots[id]);
        }
    }
}

int pop(int id)
{
    if (front[id] != NULL)
    {
        int x = front[id]->data;
        front[id] = front[id]->next;
        if (front[id] == NULL)
        {
            rear[id] = NULL;
        }
        queuesize[id]--;
        return x;
    }
    else
        return -20;
}

void display(int id)
{
    struct queue *ptr;
    ptr = front[id];
    printf(COLOR_BLUE "ID-%d = " COLOR_RESET, id);
    if (front[id] == NULL)
    {
        printf(COLOR_BLUE "Empty queue\n" COLOR_RESET);
    }
    else
    {
        while (ptr != NULL)
        {
            printf(COLOR_BLUE "%d" COLOR_RESET, ptr->data);
            ptr = ptr->next;
            if (ptr != NULL)
                printf("-");
        }
        printf("\n");
    }
}

void labpush(int id, int value)
{
    struct queue *temp;
    temp = (struct queue *)malloc(sizeof(struct queue));

    if (temp != NULL)
    {
        temp->data = value;
        if (labfront[id] == NULL)
        {
            labfront[id] = temp;
            labrear[id] = temp;
            labfront[id]->next = NULL;
            labrear[id]->next = NULL;
        }
        else
        {
            labrear[id]->next = temp;
            labrear[id] = temp;
            labrear[id]->next = NULL;
        }
    }
}

int labpop(int id)
{
l1:;
    if (labfront[id] != NULL)
    {
        int x = labfront[id]->data;
        labfront[id] = labfront[id]->next;
        if (labfront[id] == NULL)
        {
            labrear[id] = NULL;
        }
        labqueuesize[id]--;
        return x;
    }
    return -20;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q1/headers.h ####################//

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_RESET "\x1b[0m"

#define numberofcourses_max 10000
#define numberofstudents_max 10000
#define numberoflabs_max 10000
#define numberoftas_max 10000

struct queue
{
    int data;
    struct queue *next;
};

struct queue *front[numberofcourses_max];
struct queue *rear[numberofcourses_max];

struct queue *labfront[numberofcourses_max];
struct queue *labrear[numberofcourses_max];

long int queuesize[numberofcourses_max];
int labqueuesize[numberoflabs_max];
int tacount[numberoflabs_max][numberoftas_max];
int numberofstudents;
int numberoflabs;
int numberofcourses;

char nameofcourse[numberofcourses_max][1000];
float interest[numberofcourses_max];
int maxslots[numberofcourses_max];
int nooflabs[numberofcourses_max];
int listoflabs[numberofcourses_max][1000];

float calibreofstudent[numberofstudents_max];
int pref[numberofstudents_max][3];
int timepref[numberofstudents_max];

char labname[numberoflabs_max][1000];
int numberoftas[numberoflabs_max];
int limit[numberoflabs_max];

void *student(void *arg);
void *course(void *arg);
void *lab(void *arg);
void scaninput();
int pop(int id);
void push(int id, int value);
void display(int id);

int statusoflab[numberoflabs_max];
int labqueue[numberoflabs_max];
int labpop(int id);
void labpush(int id, int value);

int accesslab[numberoflabs_max];
int coursedead[numberofcourses_max];

pthread_mutex_t mutex[numberofcourses_max];
pthread_mutex_t mutex1[numberofcourses_max];
pthread_mutex_t queuesizelock[numberofcourses_max];

sem_t *slot[numberofstudents_max];
sem_t *tut[numberofstudents_max];
sem_t labcount[numberoflabs_max];
sem_t stud[numberofstudents_max];
sem_t courseslots[numberofcourses_max];

int arr[numberofcourses_max];
int talab[numberoflabs_max];
int labmax[numberoflabs_max];