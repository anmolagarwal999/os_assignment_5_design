
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/student.c ####################//

#include "global.h"
#include "student.h"

void assign_course(struct student *s, struct course *c)
{
    int i;
    for (i = 0; i < MAX_COURSES; i++)
    {
        if (s->courses[i] == NULL)
        {
            s->courses[i] = c;
            return;
        }
    }
}
void *student_thread_func(void *arg)
{
    student_ADT *student = (student_ADT *)arg;

    sleep(student->reg_time);
    printf("Student %d has filled in preferences for course registration\n", student->student_ID);

    bool joined_course_ID1 = false;
    bool joined_course_ID2 = false;
    bool joined_course_ID3 = false;

    while (1)
    {
        if (course_list[student->course_ID1].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID1].name);
            course_list[student->course_ID1].course_max_slots--;
            break;
        }
    }
    double prob = course_list[student->course_ID1].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID1 = true;
        assign_course(student, student->course_ID1);
    }
    while (1 && !joined_course_ID1)
    {
        if (course_list[student->course_ID2].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID2].name);
            course_list[student->course_ID2].course_max_slots--;
            break;
        }
    }
    prob = course_list[student->course_ID2].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID2 = true;
        assign_course(student, student->course_ID2);
    }
    while (1 && !joined_course_ID2)
    {
        if (course_list[student->course_ID3].course_max_slots > 0)
        {
            printf("Student %d has been allocated a seat in course %s\n", student->student_ID, course_list[student->course_ID3].name);
            course_list[student->course_ID3].course_max_slots--;
            break;
        }
    }
    prob = course_list[student->course_ID3].interest_quotient * (double)student->calibre_quotient;
    if (prob > rand() % 100)
    {
        printf("Student %d has selected course %s permanently\n", student->student_ID, course_list[student->course_ID3].name);
        joined_course_ID3 = true;
        assign_course(student, student->course_ID3);
    }
    if (!joined_course_ID1 && !joined_course_ID2 && !joined_course_ID3)
    {
        printf("Student %d couldn’t get any of his preferred courses\n", student->student_ID);
    }
}
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/lab.h ####################//

#ifndef _LAB_H_
#define _LAB_H_

int no_of_labs;
typedef struct
{
    int lab_ID;
    char name[20];
    int no_of_TAs;
    int TA_limit;
} lab_ADT;

lab_ADT *lab_list = NULL;

void *lab_thread_func(void *arg);
#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/main.c ####################//

#include "global.h"
#include "course.h"
#include "student.h"
#include "lab.h"

int main(int argc, char const *argv[])
{
    scanf("%d", &no_of_students);
    scanf("%d", &no_of_labs);
    scanf("%d", &no_of_courses);

    course_list = (course_ADT *)malloc(sizeof(course_ADT) * no_of_courses);
    for (int i = 0; i < no_of_courses; i++)
    {
        course_list[i].course_ID = i;
        scanf("%s", course_list[i].name);
        scanf("%d", &course_list[i].interest_quotient);
        scanf("%d", &course_list[i].course_max_slots);
        scanf("%d", &course_list[i].no_of_labs_by_TAs);

        course_list[i].lab_IDs = (int *)malloc(sizeof(int) * course_list[i].no_of_labs_by_TAs);
        for (int j = 0; j < course_list[i].no_of_labs_by_TAs; j++)
        {
            scanf("%d", &course_list[i].lab_IDs[j]);
        }
    }

    student_list = (student_ADT *)malloc(sizeof(student_ADT) * no_of_students);
    for (int i = 0; i < no_of_students; i++)
    {
        student_list[i].student_ID = i;
        scanf("%d", &student_list[i].calibre_quotient);
        scanf("%d", &student_list[i].course_ID1);
        scanf("%d", &student_list[i].course_ID2);
        scanf("%d", &student_list[i].course_ID3);
        scanf("%d", &student_list[i].reg_time);
    }

    lab_list = (lab_ADT *)malloc(sizeof(lab_ADT) * no_of_labs);
    for (int i = 0; i < no_of_labs; i++)
    {
        lab_list[i].lab_ID = i;
        scanf("%d", &lab_list[i].name);
        scanf("%d", &lab_list[i].no_of_TAs);
        scanf("%d", &lab_list[i].TA_limit);
    }

    pthread_t *student_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_students);
    for (int i = 0; i < no_of_students; i++)
    {
        if (pthread_create(&student_threads[i], NULL, student_thread_func, &student_list[i]) != 0)
        {
            printf("Error creating student_thread\n");
            exit(1);
        }
    }
    pthread_t *course_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_courses);
    for (int i = 0; i < no_of_courses; i++)
    {
        if (pthread_create(&course_threads[i], NULL, course_thread_func, &course_list[i]) != 0)
        {
            printf("Error creating course_thread\n");
            exit(1);
        }
    }
    pthread_t *lab_threads = (pthread_t *)malloc(sizeof(pthread_t) * no_of_labs);
    for (int i = 0; i < no_of_labs; i++)
    {
        if (pthread_create(&lab_threads[i], NULL, lab_thread_func, &lab_list[i]) != 0)
        {
            printf("Error creating lab_thread\n");
            exit(1);
        }
    }

    for (int i = 0; i < no_of_students; i++)
    {
        pthread_join(student_threads[i], NULL);
    }
    for (int i = 0; i < no_of_courses; i++)
    {
        pthread_join(course_threads[i], NULL);
    }
    for (int i = 0; i < no_of_labs; i++)
    {
        pthread_join(lab_threads[i], NULL);
    }
    return 0;
}

//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/student.h ####################//

#ifndef _STUDENT_H_
#define _STUDENT_H_

int no_of_students;
typedef struct
{
    int student_ID;
    int calibre_quotient;
    int course_ID1;
    int course_ID2;
    int course_ID3;
    int reg_time;
} student_ADT;

student_ADT *student_list = NULL;

void *student_thread_func(void *arg);
#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/course.c ####################//

#include "global.h"
#include "course.h"

void *course_thread_func(void *arg)
{
    course_ADT *course = (course_ADT *)arg;

    printf("Course %s has been allocated %d seats\n", course->name, course->course_max_slots);
    // Event: 7
    // Event: 8
    // Event: 9
    // Event: 10
    if ()
    {
        printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", course->name);
    }
}

//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/course.h ####################//

#ifndef _COURSE_H_
#define _COURSE_H_

int no_of_courses;
typedef struct
{
    int course_ID;
    char name[20];
    int interest_quotient;
    int course_max_slots;
    int no_of_labs_by_TAs;
    int *lab_IDs;
} course_ADT;

course_ADT *course_list = NULL;

void *course_thread_func(void *arg);
#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/global.h ####################//

#ifndef _GLOGAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "course.h"
#include "student.h"
#include "lab.h"

#define true 1
#define false 0

typedef int bool;

#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q1/lab.c ####################//

#include "global.h"
#include "lab.h"

void *lab_thread_func(void *arg)
{
    lab_ADT *lab = (lab_ADT *)arg;

    // Event: 11
    if ()
    {
        printf("TA t_i from lab %d has been allocated to course c_k for nth TA ship\n", lab->lab_ID);
    }
    // Event: 12
    if ()
    {
        printf("Lab %d no longer has students available for TA ship\n", lab->lab_ID);
    }
}