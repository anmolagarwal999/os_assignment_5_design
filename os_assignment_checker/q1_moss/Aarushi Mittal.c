
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/student.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* studentFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int i = *(int*)arg;
    // printf("S%d: %d %d %d\n", Student[i].sid, Student[i].Pref[0], Student[i].Pref[1], Student[i].Pref[2]);
    // pthread_mutex_unlock(&lock);

    courseRegister(i);

    return NULL;
}

void courseRegister(int i)
{
    sleep(Student[i].FillTime);
    Student[i].Status = FILLED;
    printf( GREEN "Student %d has filled in preferences for course registration\n"   CLEAR, i);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/main.c ####################//

// Each person is a thread
// Each TA is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "student.c"
#include "course.c"
// #include "seat.c"
// #include "goal.c"
// #include "leave.c"

void main()
{
    printf( GREEN "Simulation has started!!\n" CLEAR);
    input();
    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");

    // Thread for each student
    thStudent = (pthread_t*)malloc(numStudents * sizeof(pthread_t));
    for(int i = 0; i < numStudents; i++)
    {
        pthread_create(&thStudent[i], NULL, studentFunction, &i);
        usleep(20);
    }

    sleep(1);

    // Thread for each course
    thCourse = (pthread_t*)malloc(numCourses * sizeof(pthread_t));
    for(int i = 0; i < numCourses; i++)
    {
        pthread_create(&thCourse[i], NULL, courseFunction, &i);
        usleep(20);
    }

    threadExit();
}
    
    // Person Threads
   per_Thrd();
}

void per_Thrd()
{
     tPersonID PersonID[num_people];
    int c = 0;
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
        }
    }

    // Goal Threads
    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

    // Join threads
    join();

    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");
}

void threadExit()
{
    // Exit student threads
    for (int i = 0; i < numStudents; i++)
    {
        pthread_exit(thStudent[i]);
    }
    // Exit course threads
    for (int i = 0; i < numCourses; i++)
    {
        pthread_exit(thCourse[i]);
    }
    // Exit student threads
    for (int i = 0; i < numLabs; i++)
    {
        pthread_exit(thLab[i]);
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/seat.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf( YELLOW "%s could not get a seat\n"   CLEAR, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    // Push
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;
    Zone[team].NumSpectators++;

    // Display
    printf( YELLOW "%s has got a seat in zone %c (%d/%d)\n"   CLEAR, 
            Group[i].Person[j].Name,
            getZoneAsChar(team),
            Zone[team].NumSpectators,
            Zone[team].Capacity);

    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    // Person already left
    if(Group[i].Person[j].status == WAITING)
        return;

    // Spectating time over
    printf( MAGENTA "%s watched the match for %d seconds and is leaving\n"   CLEAR, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    // Pop
    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&lock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&lock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/person.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf( CYAN "%s has reached the stadium\n"   CLEAR, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf( BLUE "Group %d is leaving for dinner\n"   CLEAR, i+1);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/course.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* courseFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int p= *(int*)arg;
    // pthread_mutex_unlock(&lock);

    allocateMentor(i);

    return NULL;
}

void allocateMentor(int C)
{
    // Course already occupied
    if(Course[C].Status == OCCUPIED)
        return;

    // Iterate through course's lab id's
    for(int i= 0; i< Course[C].NumLabs; i++)
    {
        tLab L = Lab[Course[C].lid[i]];
        
        // Iterate though the lab's mentors
        for(int  q = 0;  q < L.NumMentors;  q++)
        {
            tMentor M = L.Mentor[ q];

            // Mentor is taking sesh
            if(M.Status == OCCUPIED)
                continue;

            // Allocate M to C
            Course[C].Status = OCCUPIED;
            Lab[Course[C].lid[i]].Mentor[ q].Status = OCCUPIED;
            printf( CYAN "TA %d from lab %s has been allocated to course %s for his %d TAship\n"   CLEAR, q, L.Name, Course[C].Name, ++Lab[Course[C].lid[i]].Mentor[ q].Num);

            numSlots(C);
            allocateSeats(C);
            
            return;
        }
    }
}

void numSlots(int i)
{
    Course[i].Slots = randint(Course[i].MaxSlots);
    irintf( RED "Course %s has been allocated %d seats\n"   CLEAR, Course[i].Name, Course[i].Slots);
}

void allocateSeats(int C)
{
    for(int i= 0; i< numStudents; i++)
    {
        if(Student[i].Status == NOT_FILLED)
            continue;

        if(Course[C].SlotsFilled == Course[C].Slots)
            return;

        if(Student[i].Pref[Student[i].Current] == Course[C].cid)
        {
            printf(BLUE "Student %d has been allocated a seat in course %s\n"   CLEAR, i, Course[C].Name);
            // printf("pref = %s current = %d\n", Course[Student[P].Pref[Student[i].Current]].Name, Student[i].Current);
            Course[C].SlotsFilled++;
        }
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/utility.h ####################//

#include "libraries.h"
//empties buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
//check if probability n is less than random number between 0 and 1
int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}

// Random number between 1 and L 
int randint(int L)
{
    srand(time(0));
    return (rand() % L) + 1;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/leave.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(P.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf( RED "%s is leaving due to bad performance of his team\n"   CLEAR, Zone[Z].Seat[i].Person.Name);
        printf("%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;

        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        pthread_mutex_lock(&lock);
        pthread_cond_signal(&cond_seat_freed);
        pthread_mutex_unlock(&lock);
    }
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define CLEAR   "\x1b[0m"

// Course/Mentor status
#define FREE 0
#define OCCUPIED 1

// Student status
#define NOT_FILLED 0
#define FILLED 1

// Types time
typedef uint Time;

// Student 
struct stStudent
{
    
    int sid;
    float Calibre;
    int Current;
    int Pref[2];
    Time FillTime;
    int Status;
};
typedef struct stStudent student;


// Course 
struct stCourse
{
    int cid;
    char Name[10];
    float Interest;
    int MaxSlots;
    int Slots;
    int SlotsFilled;
    int NumLabs;
    int* lid;
    int Status;
};
typedef struct stCourse course;

// Mentor 
struct stMentor
{
    int ID;
    int Num;
    int Status;
    pthread_mutex_t MentorLock;
};
typedef struct stMentor mentor;

// Lab
struct stLab
{
    char Name[10];
    int NumMentors;
    tMentor* Mentor;
    int Max;
};
typedef struct stLab lab;


#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/input.c ####################//

#include "libraries.h"
#include "variables.h"
#include "utility.h"



void inputCourses()
{
    Course = (course*)malloc(numCourses * sizeof(course));
    for(int i = 0; i < numCourses; i++)
    {
        // Prefix
        printf( RED "C%d: "   CLEAR, i+1);

        // Set Course ID and status
        Course[i].cid = i;
        Course[i].Status = FREE;
        Course[i].SlotsFilled = 0;

        // Input course variables
        scanf("%s %f %d %d", &Course[i].Name,&Course[i].Interest,&Course[i].MaxSlots,&Course[i].NumLabs);

        // Input lab id's for this course
        Course[i].lid = (int*)malloc(Course[i].NumLabs * sizeof(int));
        for(int j = 0; j < Course[i].NumLabs; j++)
        {
            scanf("%d", &Course[i].lid[j]);
        }
    }
}

void inputStudents()
{
    Student = (student*)malloc(numStudents * sizeof(student));

    // Iterate through all the students
    for(int i = 0; i < numStudents; i++)
    {
        flushSTDIN();

        // Prefix
        printf( CYAN "S%d: "   CLEAR, i+1);

        // Set student ID
        Student[i].StudentID = i;
        Student[i].Current = 0;
        Student[i].Status = NOT_FILLED;

        // Input student variables
        scanf("%f %d %d %d %d", &Student[i].Calibre,&Student[i].Pref[0],&Student[i].Pref[1], &Student[i].Pref[2],&Student[i].FillTime);
    }
}

mentor* init_TA(int i)
{
    mentor *m =(mentor*)malloc((Lab[i].NumMentors + 1) * sizeof(mentor));
        for(int j = 0; j < Lab[i].NumMentors; j++)
        {
            m[j].ID = j;
            m[j].Num = 0;
            m[j].Status = FREE;
            pthread_mutex_init(&m[j].MentorLock, NULL);
        }

        return m;
}

void inputLabs()
{
    Lab = (lab*)malloc(numLabs * sizeof(lab));

    for(int i = 0; i < numLabs; i++)
    {
        flushSTDIN();

        // Prefix
        printf( GREEN "L%d: "   CLEAR, i+1);

        // Initialize all TA's
        Lab[i].Mentor = init_TA(i);

        // Input lab variables
        scanf("%s %d %f", &Lab[i].Name,&Lab[i].NumMentors,&Lab[i].Max);
    }   
    printf("\n");
}

void input()
{
    printf( BLUE "#Students | #Labs | #Courses\n"   CLEAR);
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);

    inputCourses();
    inputStudents();
    inputLabs();
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

clock_t getTime();
void delay(int ms);
clock_t start;

int main()
{
    start = clock();
    delay(5000);
    printf("Hello\n");
}

clock_t getTime()
{
    clock_t diff = clock() - start;
    return (diff * 1000)/CLOCKS_PER_SEC;
}

void delay(int ms)
{
    clock_t timeDelay = ms + clock();
    while(timeDelay > clock());
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/variables.h ####################//

#ifndef VAR_H
#define VAR_H

const int numStudents;    
const int numLabs;    
const int numCourses;    

course* Course;
student* Student;
lab* Lab;

pthread_t* thStudent;
pthread_t* thCourse;
pthread_t* thLab;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t* goal_thread;
pthread_cond_t cond_seat_freed = PTHREAD_COND_INITIALIZER;
// int seat_freed = 0;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputCourses();
    void inputStudents();
    mentor* init_TA(int i);
    void inputLabs();
    void printZone(int i);s
    void printGroup(int i);

// Main
void per_thrd();
void threadExit();

// Student
void* studentFunction(void* arg);
    void courseRegister(int i);

// Course
void* courseFunction(void* arg);
    void allocateMentor(int i);
    void numSlots(int i);
    void allocateSeats(int C);

// Lab
void* labFunction(void* arg);

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();
int randint(int L);

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q1/src/goal.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf( GREEN "Team H has scored their %d%s goal!\n"   CLEAR, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf( GREEN "Team A has scored their %d%s goal!\n"   CLEAR, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
        return NULL;
    }
    // Goal Missed
    printf( GREEN "Team %c has missed!\n"   CLEAR, Goal[i].Team);
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/student.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* studentFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int i = *(int*)arg;
    // printf("S%d: %d %d %d\n", Student[i].sid, Student[i].Pref[0], Student[i].Pref[1], Student[i].Pref[2]);
    // pthread_mutex_unlock(&lock);

    courseRegister(i);

    return NULL;
}

void courseRegister(int i)
{
    sleep(Student[i].FillTime);
    Student[i].Status = FILLED;
    printf( GREEN "Student %d has filled in preferences for course registration\n"   CLEAR, i);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/main.c ####################//

// Each person is a thread
// Each TA is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "student.c"
#include "course.c"
// #include "seat.c"
// #include "goal.c"
// #include "leave.c"

void main()
{
    printf( GREEN "Simulation has started!!\n" CLEAR);
    input();
    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");

    // Thread for each student
    thStudent = (pthread_t*)malloc(numStudents * sizeof(pthread_t));
    for(int i = 0; i < numStudents; i++)
    {
        pthread_create(&thStudent[i], NULL, studentFunction, &i);
        usleep(20);
    }

    sleep(1);

    // Thread for each course
    thCourse = (pthread_t*)malloc(numCourses * sizeof(pthread_t));
    for(int i = 0; i < numCourses; i++)
    {
        pthread_create(&thCourse[i], NULL, courseFunction, &i);
        usleep(20);
    }

    threadExit();
}
    
    // Person Threads
   per_Thrd();
}

void per_Thrd()
{
     tPersonID PersonID[num_people];
    int c = 0;
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
        }
    }

    // Goal Threads
    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

    // Join threads
    join();

    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");
}

void threadExit()
{
    // Exit student threads
    for (int i = 0; i < numStudents; i++)
    {
        pthread_exit(thStudent[i]);
    }
    // Exit course threads
    for (int i = 0; i < numCourses; i++)
    {
        pthread_exit(thCourse[i]);
    }
    // Exit student threads
    for (int i = 0; i < numLabs; i++)
    {
        pthread_exit(thLab[i]);
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/seat.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf( YELLOW "%s could not get a seat\n"   CLEAR, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    // Push
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;
    Zone[team].NumSpectators++;

    // Display
    printf( YELLOW "%s has got a seat in zone %c (%d/%d)\n"   CLEAR, 
            Group[i].Person[j].Name,
            getZoneAsChar(team),
            Zone[team].NumSpectators,
            Zone[team].Capacity);

    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    // Person already left
    if(Group[i].Person[j].status == WAITING)
        return;

    // Spectating time over
    printf( MAGENTA "%s watched the match for %d seconds and is leaving\n"   CLEAR, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    // Pop
    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&lock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&lock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/person.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf( CYAN "%s has reached the stadium\n"   CLEAR, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf( BLUE "Group %d is leaving for dinner\n"   CLEAR, i+1);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/course.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* courseFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int p= *(int*)arg;
    // pthread_mutex_unlock(&lock);

    allocateMentor(i);

    return NULL;
}

void allocateMentor(int C)
{
    // Course already occupied
    if(Course[C].Status == OCCUPIED)
        return;

    // Iterate through course's lab id's
    for(int i= 0; i< Course[C].NumLabs; i++)
    {
        tLab L = Lab[Course[C].lid[i]];
        
        // Iterate though the lab's mentors
        for(int  q = 0;  q < L.NumMentors;  q++)
        {
            tMentor M = L.Mentor[ q];

            // Mentor is taking sesh
            if(M.Status == OCCUPIED)
                continue;

            // Allocate M to C
            Course[C].Status = OCCUPIED;
            Lab[Course[C].lid[i]].Mentor[ q].Status = OCCUPIED;
            printf( CYAN "TA %d from lab %s has been allocated to course %s for his %d TAship\n"   CLEAR, q, L.Name, Course[C].Name, ++Lab[Course[C].lid[i]].Mentor[ q].Num);

            numSlots(C);
            allocateSeats(C);
            
            return;
        }
    }
}

void numSlots(int i)
{
    Course[i].Slots = randint(Course[i].MaxSlots);
    irintf( RED "Course %s has been allocated %d seats\n"   CLEAR, Course[i].Name, Course[i].Slots);
}

void allocateSeats(int C)
{
    for(int i= 0; i< numStudents; i++)
    {
        if(Student[i].Status == NOT_FILLED)
            continue;

        if(Course[C].SlotsFilled == Course[C].Slots)
            return;

        if(Student[i].Pref[Student[i].Current] == Course[C].cid)
        {
            printf(BLUE "Student %d has been allocated a seat in course %s\n"   CLEAR, i, Course[C].Name);
            // printf("pref = %s current = %d\n", Course[Student[P].Pref[Student[i].Current]].Name, Student[i].Current);
            Course[C].SlotsFilled++;
        }
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/utility.h ####################//

#include "libraries.h"
//empties buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
//check if probability n is less than random number between 0 and 1
int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}

// Random number between 1 and L 
int randint(int L)
{
    srand(time(0));
    return (rand() % L) + 1;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/leave.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(P.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf( RED "%s is leaving due to bad performance of his team\n"   CLEAR, Zone[Z].Seat[i].Person.Name);
        printf("%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;

        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        pthread_mutex_lock(&lock);
        pthread_cond_signal(&cond_seat_freed);
        pthread_mutex_unlock(&lock);
    }
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define CLEAR   "\x1b[0m"

// Course/Mentor status
#define FREE 0
#define OCCUPIED 1

// Student status
#define NOT_FILLED 0
#define FILLED 1

// Types time
typedef uint Time;

// Student 
struct stStudent
{
    
    int sid;
    float Calibre;
    int Current;
    int Pref[2];
    Time FillTime;
    int Status;
};
typedef struct stStudent student;


// Course 
struct stCourse
{
    int cid;
    char Name[10];
    float Interest;
    int MaxSlots;
    int Slots;
    int SlotsFilled;
    int NumLabs;
    int* lid;
    int Status;
};
typedef struct stCourse course;

// Mentor 
struct stMentor
{
    int ID;
    int Num;
    int Status;
    pthread_mutex_t MentorLock;
};
typedef struct stMentor mentor;

// Lab
struct stLab
{
    char Name[10];
    int NumMentors;
    tMentor* Mentor;
    int Max;
};
typedef struct stLab lab;


#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/input.c ####################//

#include "libraries.h"
#include "variables.h"
#include "utility.h"



void inputCourses()
{
    Course = (course*)malloc(numCourses * sizeof(course));
    for(int i = 0; i < numCourses; i++)
    {
        // Prefix
        printf( RED "C%d: "   CLEAR, i+1);

        // Set Course ID and status
        Course[i].cid = i;
        Course[i].Status = FREE;
        Course[i].SlotsFilled = 0;

        // Input course variables
        scanf("%s %f %d %d", &Course[i].Name,&Course[i].Interest,&Course[i].MaxSlots,&Course[i].NumLabs);

        // Input lab id's for this course
        Course[i].lid = (int*)malloc(Course[i].NumLabs * sizeof(int));
        for(int j = 0; j < Course[i].NumLabs; j++)
        {
            scanf("%d", &Course[i].lid[j]);
        }
    }
}

void inputStudents()
{
    Student = (student*)malloc(numStudents * sizeof(student));

    // Iterate through all the students
    for(int i = 0; i < numStudents; i++)
    {
        flushSTDIN();

        // Prefix
        printf( CYAN "S%d: "   CLEAR, i+1);

        // Set student ID
        Student[i].StudentID = i;
        Student[i].Current = 0;
        Student[i].Status = NOT_FILLED;

        // Input student variables
        scanf("%f %d %d %d %d", &Student[i].Calibre,&Student[i].Pref[0],&Student[i].Pref[1], &Student[i].Pref[2],&Student[i].FillTime);
    }
}

mentor* init_TA(int i)
{
    mentor *m =(mentor*)malloc((Lab[i].NumMentors + 1) * sizeof(mentor));
        for(int j = 0; j < Lab[i].NumMentors; j++)
        {
            m[j].ID = j;
            m[j].Num = 0;
            m[j].Status = FREE;
            pthread_mutex_init(&m[j].MentorLock, NULL);
        }

        return m;
}

void inputLabs()
{
    Lab = (lab*)malloc(numLabs * sizeof(lab));

    for(int i = 0; i < numLabs; i++)
    {
        flushSTDIN();

        // Prefix
        printf( GREEN "L%d: "   CLEAR, i+1);

        // Initialize all TA's
        Lab[i].Mentor = init_TA(i);

        // Input lab variables
        scanf("%s %d %f", &Lab[i].Name,&Lab[i].NumMentors,&Lab[i].Max);
    }   
    printf("\n");
}

void input()
{
    printf( BLUE "#Students | #Labs | #Courses\n"   CLEAR);
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);

    inputCourses();
    inputStudents();
    inputLabs();
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

clock_t getTime();
void delay(int ms);
clock_t start;

int main()
{
    start = clock();
    delay(5000);
    printf("Hello\n");
}

clock_t getTime()
{
    clock_t diff = clock() - start;
    return (diff * 1000)/CLOCKS_PER_SEC;
}

void delay(int ms)
{
    clock_t timeDelay = ms + clock();
    while(timeDelay > clock());
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/variables.h ####################//

#ifndef VAR_H
#define VAR_H

const int numStudents;    
const int numLabs;    
const int numCourses;    

course* Course;
student* Student;
lab* Lab;

pthread_t* thStudent;
pthread_t* thCourse;
pthread_t* thLab;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t* goal_thread;
pthread_cond_t cond_seat_freed = PTHREAD_COND_INITIALIZER;
// int seat_freed = 0;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputCourses();
    void inputStudents();
    mentor* init_TA(int i);
    void inputLabs();
    void printZone(int i);s
    void printGroup(int i);

// Main
void per_thrd();
void threadExit();

// Student
void* studentFunction(void* arg);
    void courseRegister(int i);

// Course
void* courseFunction(void* arg);
    void allocateMentor(int i);
    void numSlots(int i);
    void allocateSeats(int C);

// Lab
void* labFunction(void* arg);

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();
int randint(int L);

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q1/src/goal.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf( GREEN "Team H has scored their %d%s goal!\n"   CLEAR, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf( GREEN "Team A has scored their %d%s goal!\n"   CLEAR, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
        return NULL;
    }
    // Goal Missed
    printf( GREEN "Team %c has missed!\n"   CLEAR, Goal[i].Team);
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}