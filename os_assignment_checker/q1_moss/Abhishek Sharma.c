
//###########FILE CHANGE ./main_folder/Abhishek Sharma_305785_assignsubmission_file_/q1/q1.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>
#include <stdbool.h>
/*for loop*/
#define rep(i, a, b) for (int i = a; i < (b); ++i)
#define NOT_FOUND -1
//colors for good interface

/*Stimulation*/
#define RED "\033[1;31m"
/*Event1,Event7*/
#define GRN "\033[1;32m"
/*Event2,Event8*/
#define YEL "\033[1;33m"
/*Event3,Event9*/
#define BLU "\033[1;34m"
/*Event4,Event10*/
#define MAG "\033[1;35m"
/*Event5,Event11*/
#define CYN "\033[1;36m"
/*Event6*/
#define WHT "\033[1;37m"
/*Event12*/
#define PUR "\033[1;38m"
#define RESET "\033[0m"
/*constrains*/
#define STUDENTS_MX 512
#define COURSE_MX 52
#define LABS_MX 52
#define TA_IN_LAB_MX 52
#define LAB_IN_COURSE_MX 52
#define TA_IN_COURSE_MX 10

/*struct for student*/
typedef struct student
{ 
    int id;                   //student id
    double caliber;           //student caliber (interest)
    int enter_time;           //students enters in the sequence
    int course_pref[3];       //3 courses prefernce
    int current_pref_no;      //current course prefernce number
    int in_tut;               //present in a tutorial or not
    int final_course;         //present course
    bool has_finalized_course; //if student has finalized course or not
    bool is_available;        //if student is available or not

} student;

/*struct for courses*/
typedef struct course
{
    int course_id;                  //course id
    char name[256];                 //course name
    double interest;                //course interest
    int max_slots;                  //max slots for a course
    int no_of_labs;                 //number of labs assiciated with a course
    int lab_list[100];              // assuming course do not take tas from more than 10 lasb
    bool course_removed;            //course has removed or not from the plan
 
} course;

typedef struct Ta
{
    int id;                     //Ta id
    int course_id;              //Ta teaching in course
    int kitna_Ta_bnchuka;       //how many times Ta has been assigned
    bool taship_now;             //present Ta in which course

} Ta;
typedef struct lab
{
 
    int lab_id;               //lab id
    char name[200];           //lab name
    int limit_ta;             //Ta limit in a lab
    int no_of_ta;             //number of Ta in a lab present
    Ta Tas[52]; 
    int no_of_active_ta;      //number of active Ta in a lab
    bool is_available;         

} lab;

/*global variables*/
int total_students;   //TOTAL NUMBER OF STUDENTS
int total_courses;    //TOTAL NUMBER OF COURSES
int total_labs;       //TOTAL NUMBER OF LABS

/*global array */
student students[STUDENTS_MX];
course courses[COURSE_MX];
lab Labs[LABS_MX];

/*threads*/
pthread_t students_threads[STUDENTS_MX];
pthread_t courses_threads[COURSE_MX];
pthread_t lab_threads[LABS_MX];

/*mutexts*/
pthread_mutex_t students_mutex[STUDENTS_MX];
pthread_mutex_t Ta_mutex[LABS_MX][52];

/*CONDITION VARIABLES*/
pthread_cond_t students_cond[STUDENTS_MX];

void *studentfunction(void *);
void *coursefunction(void *);

int randNum(long int lower, long int upper);   //for random generation  of numbers


/*functions*/

int randNum(long int lower, long int upper)
{
    return (int)(random() % (upper - lower + 1) + lower);
}

/*pthread_create*/
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg)
{
    if (pthread_create(thread, attr, start_routine, arg) != 0)
    {
        perror("Thread was not created");
    }
}
/*pthread_join*/
void pthreadJoin(pthread_t thread, void **retval)
{
    if (pthread_join(thread, retval) != 0)
    {
        perror("Thread could not join");
    }
}
/*mutexunlock*/
void mutexUnlock(pthread_mutex_t *mutex)
{
    fflush(stdout);
    if (pthread_mutex_unlock(mutex) != 0)
    {
        perror("Cant lock mutex");
    }
}
/*condwait*/
void condWait(pthread_cond_t *cv, pthread_mutex_t *mutex)
{
    if (pthread_cond_wait(cv, mutex) != 0)
    {
        perror("Cant cond wait on cv");
    }
}
/*mutexlock*/
void mutexLock(pthread_mutex_t *mutex)
{
    fflush(stdout);
    if (pthread_mutex_lock(mutex) != 0)
    {
        perror("Cant lock mutex");
    }
}
/*condBroadcase*/
void condBroadcast(pthread_cond_t *cv)
{
    if (pthread_cond_broadcast(cv) != 0)
    {
        perror("Cant broadcast");
    }
}
//for number bases suffixes
char* getpos(int n){
    char *pp;
    if (n%10 ==1){
        pp = "st";
    }
    else if(n%10 ==2){
        pp = "nd";
    }
    else if(n%10 ==3){
        pp = "rd";
    }
    else{
        pp = "th";
    }

    return pp;
}
/*students fill for prefernce
 *students interest is checked and their permanent selction is to be decided
 *if withrawn of course is there then preference is changed
 *if course still exist then student takes the course permanently
 *if he gets nothing in his pocket then he has to leave the stimulation
*/
void *studentfunction(void *args)
{

    student *studdmunda = (student *)args;
    int id = studdmunda->id;
    //enter time for student so that registation filling is in order
    int time = studdmunda->enter_time;
    sleep(time);
    printf(GRN "Students %d has filled  preferences for course registration\n" RESET, id);

    studdmunda->is_available = true;

    while (1)
    {
        mutexLock(&students_mutex[id]);
    again:
        studdmunda->final_course = NOT_FOUND;
        studdmunda->in_tut = 0;
        // wait for a signal from course to indicate that a tutorial has been completed
        if (!courses[studdmunda->course_pref[studdmunda->current_pref_no]].course_removed)
            condWait(&students_cond[id], &students_mutex[id]);
        

        studdmunda->in_tut = 0;

        // check if signal was because course has been removed
        // course has been removed
        // move to the next course which is available
        if (courses[studdmunda->final_course].course_removed == true)
        {
            int pref = studdmunda->current_pref_no;

            // check which one is possible
            if ((pref - 2 <0) && (courses[studdmunda->course_pref[pref + 1]].course_removed == false))
            {
                studdmunda->current_pref_no++;
                pref = studdmunda->current_pref_no;
               
                printf(MAG "Student %d has changed its preference from %s (priority %d) to %s (priority %d)\n" RESET, studdmunda->id, courses[studdmunda->final_course].name, pref - 1, courses[studdmunda->course_pref[pref]].name, pref);

                // pthread_mutex_unlock(&students_mutex[id]);
                goto again;
            }
            else if ((pref-1 < 0) && (courses[studdmunda->course_pref[pref + 2]].course_removed == false))
            {
                studdmunda->current_pref_no += 2;
                pref = studdmunda->current_pref_no;

                printf(MAG "Student %d has changed its preference from %s (priority %d) to %s (priority %d)\n" RESET, studdmunda->id, courses[studdmunda->final_course].name, pref - 2, courses[studdmunda->course_pref[pref]].name, pref);
                goto again;
            }
            else
            {

                printf(RED "Student %d did not like any of the courses and has exited the simulation\n" RESET, studdmunda->id);

                studdmunda->is_available = false;
                mutexUnlock(&students_mutex[id]);
                return NULL;
            }
        }

        // student has completed the tut of some course
        studdmunda->in_tut = 0;

        // Deciding whether student selects or not
        double value = ((double)rand() * 1.0) / RAND_MAX;

        int pref_no = studdmunda->current_pref_no;

        int course_no = studdmunda->course_pref[pref_no];
        course *course1 = &courses[course_no];

        double probability = (studdmunda->caliber) * (course1->interest) * 1.0;  //according to the problem the student course intrst is equal to caibler*interst

        if (value <= probability) // select this course
        { 
            //if the value is grater then or equal to the probability then the student will select the course
            printf(CYN "Student %d has selected the %s permanently\n" RESET, id, course1->name);
            students[id].has_finalized_course = true;
            studdmunda->is_available = false;
            mutexUnlock(&students_mutex[id]);
            return NULL;
        }
        else // change priority if this is not the third course or else exit the simulation
        { 
            //else the course is changed to the next course
            //if nothing remaing with this criteria means 3 of 3 courses are consumed then he/she leaves
            printf(BLU "Student %d has withdrawn from the %s course\n" RESET, id, course1->name);

            int pref = studdmunda->current_pref_no;

            if ((pref-2<0) && (courses[studdmunda->course_pref[pref + 1]].course_removed == false))
            {
                studdmunda->current_pref_no++;
                pref = studdmunda->current_pref_no;

                printf(MAG "Student %d has changed its preference from %s (priority %d) to %s (priority %d)\n" RESET, studdmunda->id, courses[studdmunda->final_course].name, pref - 1, courses[studdmunda->course_pref[pref]].name, pref);
               
                goto again;
            }
            else if ((pref-1<0) && (courses[studdmunda->course_pref[pref + 2]].course_removed == false))
            {
                studdmunda->current_pref_no += 2;
                pref = studdmunda->current_pref_no;

                printf(MAG "Student %d has changed its preference from %s (priority %d) to %s (priority %d)\n" RESET, studdmunda->id, courses[studdmunda->final_course].name, pref - 2, courses[studdmunda->course_pref[pref]].name, pref);

                // pthread_mutex_unlock(&students_mutex[id]);
                goto again;
            }
            else
            {
                studdmunda->is_available = false;
                printf(RED "Student %d did not like any of the courses and has exited the simulation\n" RESET, studdmunda->id);
                mutexUnlock(&students_mutex[id]);
                return NULL;
            }
        }
    }
}

//courses

/*will START the course
 *search for Ta to assigned to the lab
 *allocate seats to students
 *get student for lab
 *tutorial begins in
 *tut ends or TA leavs and left the course
 *TA free now
 */
void *coursefunction(void *args)
{
    course *course1 = (course *)args;

    // will store the id's of the student selected for a tutorial
    int students_selected[100];

    

    int iter_count = 0;
    bool flag = false;

    for (int jo = 1;;jo++)
    {

   LAB_REMOVAL:
        iter_count = 0; // check for lab removal after every 10 iterations
        flag = false;

        // check if course is available
      rep(i,0,course1->no_of_labs)
        {
            lab *lab1 = &Labs[course1->lab_list[i]];
            int max_taship = lab1->limit_ta;

            rep(j,0,lab1->no_of_ta)
            {
                if (lab1->Tas[j].kitna_Ta_bnchuka < max_taship)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == true)
                break;
        }

        if (flag == false)
        {
            // course as been removed

            printf(MAG "Course %s doesn't have any TA's eligible and is removed from course offerings.\n" RESET, course1->name);

            course1->course_removed = true;

            rep(i,0,total_students)
            {
              
                if (students[i].course_pref[students[i].current_pref_no] == course1->course_id)
                {
                    students[i].final_course = course1->course_id;
                    pthread_cond_signal(&students_cond[students[i].id]);
                }
            }
            return NULL;
        }

        // find a ta for this course from the ta list
        int ta_no = NOT_FOUND;
        int lab_no = NOT_FOUND;
        bool course_ta_found = false;
        Ta *ta_selected;

        while (course_ta_found != true)
        {
            iter_count++;
            if (iter_count - TA_IN_COURSE_MX > 0)
                goto LAB_REMOVAL;
            

            // iterate through the list of tas
            rep(i,0,course1->no_of_labs)
            {
                lab *lab1 = &Labs[course1->lab_list[i]];
                {
                   rep(j,0,lab1->no_of_ta)
                    {
                        Ta *ta1 = &lab1->Tas[j];
                        if ((ta1->taship_now == false) && (ta1->kitna_Ta_bnchuka < lab1->limit_ta))
                        {
                            
                            if (pthread_mutex_trylock(&Ta_mutex[lab1->lab_id][j]) != 0)
                            {
                                continue;
                            }
                            //assign this Ta to the lab 
                            course_ta_found = true;
                            ta1->taship_now = true;
                            ta1->kitna_Ta_bnchuka++;
                            ta1->course_id = course1->course_id;
                            ta_selected = ta1;
                            ta_no = ta1->id;
                            lab_no = lab1->lab_id;
                            break;
                        }
                    }
                }

                if (course_ta_found == true)
                    break;
            }
        }

        if (course_ta_found == true)
        {
            // assign the ta to this course

            // printf(BLU "Course %s has been allocated TA %d from lab %s\n", course1->name, ta_no, Labs[lab_no].name);

            printf(CYN "TA %d from lab %s has been allocated to course %s for his %d%s TA ship\n" RESET, ta_no, Labs[lab_no].name, course1->name, ta_selected->kitna_Ta_bnchuka,getpos(ta_selected->kitna_Ta_bnchuka));

            // allocate random number of seats between 1 and max_slot for this course
            int slots = randNum(1, course1->max_slots);

            printf(GRN "Course %s has been allocated %d seats\n" RESET, course1->name, slots);

            int num_student_selected = 0;

            while (num_student_selected == 0)
            {

                // select students
                rep(i,0,total_students)
                {

                    if ((students[i].is_available != false) && (students[i].course_pref[students[i].current_pref_no] == course1->course_id) && (students[i].in_tut == 0))
                    {
                        mutexLock(&students_mutex[i]);
                        students[i].in_tut = 1;
                        students_selected[num_student_selected] = i;
                        students[i].final_course = course1->course_id;
                        num_student_selected++;

                        printf(YEL "Student %d has been allocated a seat in course %s\n" RESET, i, course1->name);
                        sleep(1);
                    }

                    if (num_student_selected >= slots)
                        break;
                }

                if (num_student_selected == 0)
                {
                    // make this thread go for sleep for one sec before checking again
                    sleep(1);
                }
            }

            // start the tutorial // go to sleep for 2 sec

            printf(YEL "Tutorial has started for course %s with %d seats filled out of %d\n" RESET, course1->name, num_student_selected, slots);
            sleep(2);

            // tutorial has ended

            printf(BLU "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, ta_no, Labs[lab_no].name, course1->name);

            
           rep(i,0,num_student_selected)
            {
               // signal all the selected students.that tut has ended and they can update their choices
                mutexUnlock(&students_mutex[students_selected[i]]);
                pthread_cond_signal(&students_cond[students_selected[i]]);
            }

            ta_selected->taship_now = false;

            // checking for ta removal
            int max_taship = Labs[lab_no].limit_ta;
            bool lab_flag = false;

            rep(i,0,Labs[lab_no].no_of_ta)
            {
                if (Labs[lab_no].Tas[i].kitna_Ta_bnchuka < max_taship)
                {
                    lab_flag = true;
                }
            }

            if (lab_flag == false && Labs[lab_no].is_available != false)
            {

                printf(RED "Lab %s no longer has students available for TAship.\n" RESET, Labs[lab_no].name);

                Labs[lab_no].is_available = false;
            }

            mutexUnlock(&Ta_mutex[lab_no][ta_selected->id]);
        }
    }
}

int main()
{
    srand(time(NULL));
    /*input*/
    scanf("%d", &total_students);
    scanf("%d", &total_labs);
    scanf("%d", &total_courses);
    //courses input//

    rep(i, 0, total_courses)
    {
        scanf("%s", courses[i].name);
        scanf("%lf", &courses[i].interest);
        scanf("%d", &courses[i].max_slots);
        scanf("%d", &courses[i].no_of_labs);
        courses[i].course_removed = false;
        courses[i].course_id = i;

        rep(j,0,courses[i].no_of_labs)
             scanf("%d", &courses[i].lab_list[j]);
    }

    //STUDENTS input//

    rep(i, 0, total_students)
    {
        scanf("%lf", &students[i].caliber);
        for (int j = 0; j < 3; j++)
        {
            scanf("%d", &students[i].course_pref[j]);
        }
        scanf("%d", &students[i].enter_time);
        students[i].id = i;
        students[i].has_finalized_course = false;
        students[i].current_pref_no = 0;
        students[i].is_available = false;
        students[i].in_tut = 0;
        students[i].final_course = -1;
    }

    //labs//

    rep(i,0,total_labs)
    {
        scanf("%s", Labs[i].name);
        scanf("%d", &Labs[i].no_of_ta);
        scanf("%d", &Labs[i].limit_ta);

        Labs[i].lab_id = i;
        rep(j, 0, Labs[i].no_of_ta)
        {
            Labs[i].Tas[j].id = j;
            Labs[i].Tas[j].course_id = NOT_FOUND;
            Labs[i].Tas[j].taship_now = false;
            Labs[i].is_available = true;
            Labs[i].Tas[j].kitna_Ta_bnchuka = 0;
            Labs[i].no_of_active_ta = Labs[i].no_of_ta;
        }
       
    }

    /*input finised*/

    rep(i, 0, total_students)
    {
        pthread_cond_init(&students_cond[i], NULL);
        pthread_mutex_init(&students_mutex[i], NULL);
    }

    rep(i, 0, total_labs)
    {
        rep(j, 0, 52)
        {
            pthread_mutex_init(&Ta_mutex[i][j], NULL);
        }
    }

    rep(i, 0, total_students)
    {
        pthreadCreate(&students_threads[i], NULL, studentfunction, &students[i]);
    }

    rep(i, 0, total_courses)
    {
        pthreadCreate(&courses_threads[i], NULL, coursefunction, &courses[i]);
    }

    rep(i, 0, total_students)
    {
        pthreadJoin(students_threads[i], NULL);
    }

    printf(RED "Simulation Over\n" RESET);

    /*End*/
}