
//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q1/student.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>

/* Event 1: Student filled in preferences for course registration
“Student i has filled in preferences for course registration”
● Event 2 : Student has been allocated a seat in a particular course
“Student i has been allocated a seat in course c_j”
● Event 3 : Student has withdrawn from a course
“Student i has withdrawn from course c_j”
● Event 4 : Student has changed their preference
“Student i has changed current preference from course_x (priority k) to course_y (priority k+1)”
● Event 5 : Student has selected a course permanently
“Student i has selected course c_j permanently”
● Event 6 : Student either didn’t get any of their preferred courses or has withdrawn from them and has exited
the simulation
“Student i couldn’t get any of his preferred courses” */

void *student_function(void *inp)
{
    char *courseAllotted;
    char *nextcourse;
    int coursepreferd;
    struct timespec to;
    int id = ((struct thread_details *)inp)->idx;

    //----------------wait to fill the preference---------------------

    pthreadMutexLock(&preferenceLock); //FOR CONDITION

    to.tv_sec = start.tv_sec + students_attr[id]->t; //time till student waitted to fill the preference
    pthread_cond_timedwait(&timec, &preferenceLock, &to);
    printf(GREEN "Student %d has filled in preferences for course registration"RESET "\n", id);
    pthreadMutexUnlock(&preferenceLock);

    //-------------STUDENT HAS FILLED THE PREFERENCE -------------------

    while (1)
    {
        //-------------tell prefered course this student is available and  waiting-----------------------------
        //make student available
        coursepreferd = students_attr[id]->preference[students_attr[id]->current_pref];
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //pthreadMutexLock(&studentLock);
        students_attr[id]->available = 1; //student is available now
        // pthreadMutexUnlock(&studentLock);
        //increment prefered course NumStudentCurrentP,add student id to course studentcurrent list
        pthreadMutexLock(&courseLocks[coursepreferd]);
        courses_attr[coursepreferd]->studentcurrent[courses_attr[coursepreferd]->NumStudentCurrentP] = id;
        courses_attr[coursepreferd]->NumStudentCurrentP++;
        pthreadMutexUnlock(&courseLocks[coursepreferd]);

        pthreadCondBroadcast(&checkPref); //broadcast to courses waiting on students with that couse as preference

        //---------------WAIT TILL STUDENT GETS ALLOTED TO A COURSE-------------
        pthreadMutexLock(&studentLock);
        while (students_attr[id]->available == 1)
        {


            courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;

            //-----------------------------------CHECK IF COURSE IS WITHDRAWN---------------------------------------------------
            while (students_attr[id]->current_pref <= 2 && !courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->active)
            {
                courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
                students_attr[id]->current_pref++;
                nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
                coursepreferd = students_attr[id]->preference[students_attr[id]->current_pref];
                //--------------------tell course-----------------------------------
                pthreadMutexLock(&courseLocks[coursepreferd]);
                courses_attr[coursepreferd]->studentcurrent[courses_attr[coursepreferd]->NumStudentCurrentP] = id;
                courses_attr[coursepreferd]->NumStudentCurrentP++;
                pthreadMutexUnlock(&courseLocks[coursepreferd]);

                printf( YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
                pthreadCondBroadcast(&checkPref); //broadcast to courses waiting on students with that couse as preference
            }
            if (students_attr[id]->current_pref > 2)
            {
                printf(BRED "Student %d couldn’t get any of his preferred courses"RESET"\n", id);
                pthreadMutexLock(&mutex);
                studentsExited++;
                pthreadMutexUnlock(&mutex);
                pthreadMutexUnlock(&studentLock);
                pthreadCondBroadcast(&checkPref);
                pthread_exit(0);
            }
             //-----------------------------WAIT FOR SEAT ---------------------------------------------------------------------------
            pthreadCondWait(&checkSeat, &studentLock);
            
            //printf("{%d %d}\n",students_attr[id]->available,id);
        }

        courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
        printf("Student %d has been allocated a seat in course %s\n", id, courseAllotted);

        pthreadMutexUnlock(&studentLock);
        //==================================================================================================

        //---------------STUDENT GETS ALLOTED TO A COURSE----------------------------

        //-----------student attending tutorial-------------------
        sleep(8);
        //-----------TUTORIAL OVER---------------------------------------

        //-------------CHECK IF STUDENT WIDRAWED OR CHOOSED COURSE PERMANENTLY-----------------------

        int probability = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->interest * students_attr[id]->calibre * 100;
        int decision = rand() % 100 <= probability;
        if (decision == 1)
        {
            printf(BRED "Student %d has selected course %s permanently" RESET "\n", id, courseAllotted);
            pthreadMutexLock(&mutex); //GUARDS studentsExited
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref); //NOTIFY course waiting for students to check if all student have exited
            pthread_exit(0);                  //EXITED
        }

        printf(YELLOW "Student %d has withdrawn from course %s"RESET"\n", id, courseAllotted);
        //--------------------------------------------------------------------------------------------

        //-------------Student withdrawn will go for next preference --------------------------------

        // change preference to next priority
        students_attr[id]->current_pref++;

        // there is no next preference
        if (students_attr[id]->current_pref > 2)
        {
            printf(BRED "Student %d couldn’t get any of his preferred courses" RESET "\n", id);
            pthreadMutexLock(&mutex);
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref);
            pthread_exit(0);
        }

        nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;

        //cehck if next preference couse is withdrawn if so change to next one
        while (students_attr[id]->current_pref <= 2 && !courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->active)
        {
            printf(YELLOW "Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
            courseAllotted = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
            students_attr[id]->current_pref++;
            nextcourse = courses_attr[students_attr[id]->preference[students_attr[id]->current_pref]]->name;
        }
        // there is no next active course preference so exit
        if (students_attr[id]->current_pref > 2)
        {
            printf(BRED "Student %d couldn’t get any of his preferred courses"RESET"\n", id);
            pthreadMutexLock(&mutex);
            studentsExited++;
            pthreadMutexUnlock(&mutex);
            pthreadCondBroadcast(&checkPref);
            pthread_exit(0);
        }
        // next active  course preference
        else
        {

            printf(GREEN"Student %d has changed current preference from %s (priority %d) to %s (priority %d)"RESET"\n", id, courseAllotted, students_attr[id]->current_pref, nextcourse, students_attr[id]->current_pref + 1);
        }
        //---------------------------------loop-----------------------------------------------
    }
}

//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q1/main.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>
pthread_cond_t checkSeat = PTHREAD_COND_INITIALIZER;
pthread_cond_t waitForTA = PTHREAD_COND_INITIALIZER;
pthread_cond_t checkPref = PTHREAD_COND_INITIALIZER;
pthread_cond_t timec = PTHREAD_COND_INITIALIZER;
int studentsExited = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;                   //syncronises the acces of studentsExited
pthread_mutex_t studentLock= PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t preferenceLock = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t TALock = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t labLocks[lab_max];       // each lab has a lock,multiple courses can look for TA in different lab at the same time
pthread_mutex_t courseLocks[course_max]; //each course has alock, multiple students can update studentcurrent(student ids currrently

int main()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    if (num_students <= 0)
    {
        printf("invalid students\n");
        return 0;
    }
    else if (num_labs <= 0)
    {
        printf("invalid labs\n");
        return 0;
    }
    else if (num_courses <= 0)
    {
        printf("Invalid courses\n");
        return 0;
    }

    //Declaring the pthreads
    pthread_t students_thread[num_students];
    pthread_t courses_thread[num_courses];

    //printf("%d %d %d",num_students,num_labs,num_courses);

    //course inputs
    for (int i = 0; i < num_courses; i++)
    {
        courses_attr[i] = (struct course *)(malloc(sizeof(struct course)));
        scanf("%s %f %d %d", courses_attr[i]->name, &courses_attr[i]->interest, &courses_attr[i]->course_max_slot, &courses_attr[i]->lab_ids[0]);
        //printf("%d %s %f %d %d",i,courses[i].name,courses[i].interest,courses[i].course_max_slot,courses[i].lab_ids[0]);
        //printf("%d",courses[i].lab_ids[0]);
        for (int j = 1; j <= courses_attr[i]->lab_ids[0]; j++)
        {
            scanf("%d", &courses_attr[i]->lab_ids[j]);
        }
        courses_attr[i]->TA_mentor = -1;
        courses_attr[i]->active = 1;
        courses_attr[i]->NumStudentCurrentP = 0;
        pthreadMutexInit(&courseLocks[i]);
    }
    //student inputs
    for (int i = 0; i < num_students; i++)
    {
        students_attr[i] = (struct student *)(malloc(sizeof(struct student)));
        scanf("%f %d %d %d %d", &students_attr[i]->calibre, &students_attr[i]->preference[0], &students_attr[i]->preference[1], &students_attr[i]->preference[2], &students_attr[i]->t);
        students_attr[i]->current_pref = 0;
        students_attr[i]->available = 0;
    
    }
    //labs inputs and initialization
    for (int i = 0; i < num_labs; i++)
    {
        labs_attr[i] = (struct lab *)(malloc(sizeof(struct lab)));
        scanf("%s %d %d", labs_attr[i]->name, &labs_attr[i]->n_i, &labs_attr[i]->limit);
        for (int r = 0; r < labs_attr[i]->n_i; r++)
        {
            labs_attr[i]->mentors[r].tut_left = labs_attr[i]->limit;
            labs_attr[i]->mentors[r].status = 0;
        }
        labs_attr[i]->nextTA = 0;
       pthreadMutexInit(&labLocks[i]);
    }

    //start
    clock_gettime(CLOCK_REALTIME, &start);

    //create threads
    for (int i = 0; i < num_courses; i++)
    {
        pthread_t curr_tid;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->idx = i;
        pthreadCreate(&curr_tid, NULL, course_function, (void *)(thread_input));
        courses_thread[i] = curr_tid;
    }
    for (int i = 0; i < num_students; i++)
    {
        pthread_t curr_tid;
        td *thread_input = (td *)(malloc(sizeof(td)));
        thread_input->idx = i;
        pthreadCreate(&curr_tid, NULL, student_function, (void *)(thread_input));
        students_thread[i] = curr_tid;
         
    }

    //pthreadJoin
    for (int i = 0; i < num_students; i++)
    {
        pthreadJoin(students_thread[i], NULL);
    }

    for (int i = 0; i < num_courses; i++)
    {
        pthread_cancel(courses_thread[i]);
    }

    //Destry

    pthreadCondDestroy(&checkSeat);
    pthreadCondDestroy(&waitForTA);
    pthreadCondDestroy(&checkPref);
    pthreadCondDestroy(&timec);
    pthreadMutexDestroy(&mutex);
     for (int i = 0; i < num_courses; i++)
    {
       pthreadMutexDestroy(&courseLocks[i]);
       free(courses_attr[i]);
    }

    for (int i = 0; i <  num_labs; i++)
    {
         pthreadMutexDestroy(&labLocks[i]);
         free(labs_attr[i]);
    }



}
//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q1/course.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"
#include <string.h>

#include <sys/types.h>

/* 
Event 7 : Seats have been allocated for the course
“Course c_i has been allocated j seats”
● Event 8 : TA has started the tutorial with k seats, where k <= number of seats allocated for the course (j from
the previous step)
“Tutorial has started for Course c_i with k seats filled out of j”
● Event 9 : TA has completed the tutorial and left the course
“TA t_j from lab l_k has completed the tutorial and left the course c_i”
● Event 10 : The course doesn’t have any eligible students for TA ship available and is removed from the course
offerings.
“Course c_i doesn’t have any TA’s eligible and is removed from course offerings”
 */

//function to allocate seats to students
int Allocate_seats(int id, int rand_seats) //called inside pthreadMutexLock(&courseLocks[id]);
{
    int count = 0;
    int student_id;
    while (courses_attr[id]->NumStudentCurrentP) //run till some student has current preference as this course
    {
        student_id = courses_attr[id]->studentcurrent[0];
       // pthreadMutexLock(&studentLock); //guards students_attr to change available

        students_attr[student_id]->available = 0;

        //pthreadMutexUnlock(&studentLock);

        for (int i = 1; i < courses_attr[id]->NumStudentCurrentP; i++)
            courses_attr[id]->studentcurrent[i - 1] = courses_attr[id]->studentcurrent[i];
        courses_attr[id]->NumStudentCurrentP--;
        count++;

        
        if (count == rand_seats) //if all tutorial seats are filled break
        {
            break;
        }
    }
    return count;
}

int AllocateNextTA(int id) //used inside pthreadMutexLock(&labLocks[id]);
{
    int flag = 0;

    for (int i = 0; i < labs_attr[id]->n_i; i++)
    {
        if (labs_attr[id]->mentors[i].status == 0 && labs_attr[id]->mentors[i].tut_left) //free TA with TAship not crossed limit
        {
            flag = 2;
            labs_attr[id]->nextTA = i;
            break;
        }
        if (labs_attr[id]->mentors[i].tut_left) //there is TA ship left but every one is busy rightnow
        {
            flag = 1;
        }
    }

    return flag;
}

int AllocateTA(int id, int *n, char *c, int *TAlab)
{ //printf("[id]");
    int flag = 0;
    for (int t = 1; t <= courses_attr[id]->lab_ids[0]; t++) //diffrent threads of courses checks each lab prefered by them for TA
    {
        int i = courses_attr[id]->lab_ids[t];
        pthreadMutexLock(&labLocks[i]);
        if (labs_attr[i]->nextTA == -1) //there is TA ship left but every one is busy rightnow
        {
            flag = AllocateNextTA(i); // updates nextTA
        }
        if (labs_attr[i]->nextTA >= 0)
        {
            flag = 2;
            courses_attr[id]->TA_mentor = labs_attr[i]->nextTA;                                  //Assign TA to course
            labs_attr[i]->mentors[labs_attr[i]->nextTA].status = 1;                              //change status of TA
            *n = labs_attr[i]->limit - labs_attr[i]->mentors[labs_attr[i]->nextTA].tut_left + 1; // set which TA ship to print by course funtion
            labs_attr[i]->mentors[labs_attr[i]->nextTA].tut_left -= 1;                           //reduce the TA ship left
            strcpy(c, labs_attr[i]->name);
            labs_attr[i]->nextTA = -1;
            *TAlab = i;
            AllocateNextTA(i); // updates nextTA
        }
        pthreadMutexUnlock(&labLocks[i]);
        if (flag == 2)
        {
            break;
        }
    }

    return flag;
}

void *course_function(void *inp)
{
    int TAlab = 0;
    int n = 0;
    char c[64];
    int TAstatus = 0;
    int id = ((struct thread_details *)inp)->idx;
    sleep(1); //students are filling
    while (1)
    {
        // -------------------wait on checkPref untill someone has this course as preferce or all students exited------------------------
        pthreadMutexLock(&preferenceLock);

        while (courses_attr[id]->NumStudentCurrentP == 0)
        {
            pthreadCondWait(&checkPref, &preferenceLock); //wait till student preference changes to this course or all student exited
            //printf("[%d]", studentsExited);
            //checks if all student exited
            if (studentsExited == num_students)
            {
                pthreadMutexUnlock(&preferenceLock);
                printf(RED "Course %s  is removed from course offering"RESET"\n", courses_attr[id]->name);
                pthread_exit(0);
            }
        }
        pthreadMutexUnlock(&preferenceLock);

        //--------------//Allocate TA//--------------------------//
        TAstatus = AllocateTA(id, &n, c, &TAlab);

        //wait for TA to be free
        pthreadMutexLock(&TALock);
        while (TAstatus == 1)
        {
            //printf("ohno%d\n",id);
            pthreadCondWait(&waitForTA, &TALock);
            TAstatus = AllocateTA(id, &n, c, &TAlab);
        }
        pthreadMutexUnlock(&TALock);
        
        if (TAstatus == 0) //All TAs from prefered labs has exhausted his TA ship
        {
            printf(RED "Course %s doesn’t have any TA’s eligible and is removed from course offering"RESET"\n", courses_attr[id]->name);
            courses_attr[id]->active = 0;
            pthreadCondBroadcast(&checkSeat);
            pthread_exit(0);
        }

        printf(BLUE "TA %d from lab %s has been allocated to course %s for his %dth TA ship" RESET"\n", courses_attr[id]->TA_mentor, c, courses_attr[id]->name, n);

        //--------------//TA is allocated or course is withdrawn//----------//

        int rand_seats = rand() % courses_attr[id]->course_max_slot + 1; //seats alotted for tutorial
        printf(MAGENTA "Course %s has been allocated %d seats"RESET"\n", courses_attr[id]->name, rand_seats);
        sleep(1);
        //------------Allocate seats to students----------------------------//
        pthreadMutexLock(&courseLocks[id]);
        int seats = Allocate_seats(id, rand_seats);
        pthreadMutexUnlock(&courseLocks[id]);
        pthreadCondBroadcast(&checkSeat); // updates if student got a seat
        sleep(1);
        printf(BLUE "Tutorial has started for Course %s with %d seats filled out of %d" RESET  "\n", courses_attr[id]->name, seats, rand_seats);

        //------------------tutorial-----------------------------------------//
        sleep(5);


        printf( BLUE "TA %d from lab %s has completed the tutorial and left the course %s" RESET "\n", courses_attr[id]->TA_mentor, c, courses_attr[id]->name);

         //change status of TA
        pthreadMutexLock(&labLocks[TAlab]);
        labs_attr[TAlab]->mentors[courses_attr[id]->TA_mentor].status = 0;
        pthreadMutexUnlock(&labLocks[TAlab]);

        //-------------------tutorial over-----------------------------------------//

        pthreadCondBroadcast(&waitForTA); //notify threads who are waiting for TA
    }
}

//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q1/entities.h ####################//

#ifndef ENTITIES_H /* This is an "include guard" */
#define ENTITIES_H
#define Max 100

#define TA_max 100
#define lab_max 1000
#define student_max 1000
#define course_max 1000

#define RED "\x1b[31m"
#define BRED "\x1b[1;31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int num_students, num_labs, num_courses;

typedef struct thread_details
{
    int idx;
} td;

struct TA
{
    int tut_left; //initialised to limit of course and then decremented as TA is alocatted to course
    int status;   //set when TA is allocated to course and 0 when it gets free
};
struct lab
{
    char name[64]; //name of lab
    int n_i;       //number of tAs
    int limit;     //limit number of times a member of the lab can be TA mentor in the semester
    int nextTA;    //next free TA of lab
    struct TA mentors[Max];
};
struct course
{
    char name[64];
    float interest;                  //general student interest in the course over the past year. This value is a number between 0 and 1.
    int lab_ids[Max];                // number of labs at 0th index
    int TA_mentor;                   // TA allocated
    int course_max_slot;             //max slots given to course
    int NumStudentCurrentP;          // students currently interested in course
    int studentcurrent[student_max]; // student ids currrently interested in course
    int active;                      // 0 if no more TA can be allocated
};
struct student
{

    int preference[3]; //input preferences
    int current_pref;  //current course preference
    int t;             //time when preference whas filled
    float calibre;
    int available; // set if student is available
};
struct timespec start; //time when simulation started
struct lab *labs_attr[lab_max];
struct course *courses_attr[course_max];
struct student *students_attr[student_max];




extern pthread_mutex_t preferenceLock;
extern pthread_mutex_t TALock;
extern pthread_mutex_t studentLock;
extern pthread_mutex_t mutex;                   //syncronises the acces of studentsExited
extern pthread_mutex_t labLocks[lab_max];       // each lab has a lock,multiple courses can look for TA in different lab at the same time
extern pthread_mutex_t courseLocks[course_max]; //each course has alock, multiple students can update studentcurrent(student ids currrently
                                         //interested in course) in different courses athe same time


extern pthread_cond_t timec;                    //used with  pthread_cond_timedwait for student to wait till they fill the preference
extern pthread_cond_t checkSeat; //used to wait for course to allocate student a seat in tutorial
extern pthread_cond_t waitForTA; //used to wait for one of the TA from prefered labs to be free and has TA ship left
extern pthread_cond_t checkPref; //used to wait for student to have the course as priority

extern int studentsExited; //used to keep track of students who exited the simulation

//functions
void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void pthreadJoin(pthread_t thread, void **retval);
void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *mutex);
void pthreadMutexLock(pthread_mutex_t *mutex);
void pthreadMutexUnlock(pthread_mutex_t *mutex);
void pthreadCondBroadcast(pthread_cond_t *cv);
void pthreadMutexDestroy(pthread_mutex_t *m);
void pthreadCondDestroy(pthread_cond_t *cv);
void pthreadMutexInit(pthread_mutex_t *m);
void *student_function(void *inp);
void *course_function(void *inp);

#endif

//###########FILE CHANGE ./main_folder/Aparna Agrawal_305831_assignsubmission_file_/2021121007_assignment_5/q1/errorHandle.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "entities.h"

void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg)
{
    if (pthread_create(thread, attr, start_routine, arg) != 0)
    {
        perror("Thread was not created");
    }
}

void pthreadJoin(pthread_t thread, void **retval)
{
    if (pthread_join(thread, retval) != 0)
    {
        perror("Thread could not join");
    }
}

void pthreadCondWait(pthread_cond_t *cv, pthread_mutex_t *m)
{
    if (pthread_cond_wait(cv, m) != 0)
    {
        perror("Cannot cond wait on cv");
    }
}

void pthreadMutexLock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_lock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadMutexUnlock(pthread_mutex_t *m)
{
    fflush(stdout);
    if (pthread_mutex_unlock(m) != 0)
    {
        perror("Cannot lock mutex");
    }
}

void pthreadCondBroadcast(pthread_cond_t *cv)
{
    if (pthread_cond_broadcast(cv) != 0)
    {
        perror("Cannot broadcast");
    }
}

void pthreadMutexInit(pthread_mutex_t *m)
{
    if (pthread_mutex_init(m,0) != 0)
    {
        perror("Cannot initialize mutex");
    }
}


void pthreadMutexDestroy(pthread_mutex_t *m)
{
    fflush(stdout);
    int f = pthread_mutex_destroy(m);
    if ( f!= 0)
    {   printf("%d",f);
        perror("Cannot destroy mutex");
    }
}

void pthreadCondDestroy(pthread_cond_t *cv)
{
    fflush(stdout);
    if ( pthread_cond_destroy(cv)!= 0)
    {
        perror("Cannot destroy cv");
    }
}

