
//###########FILE CHANGE ./main_folder/anmolseep kaurD_29022/q1/q1.c ####################//

#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define BBLK printf("\033[1;30m");
#define BRED printf("\033[1;31m");
#define BGRN printf("\033[1;32m");
#define BYEL printf("\033[1;33m");
#define BBLU printf("\033[1;34m");
#define BMAG printf("\033[1;35m");
#define BCYN printf("\033[1;36m");
#define RESET printf("\033[0m");

struct TA
{
    int TA_id;
    int cur_TA_times;
    pthread_mutex_t TA_mutex;
};

struct Lab
{
    int Lab_id;
    char name[20];
    int n_i;
    int max_TA_times;
    struct TA *current_TA;
};

struct Course
{
    int C_index;
    pthread_t tid;
    char course_name[20];
    float interest;
    int total_lab_arr;
    int course_max_slot;
    int *assorted_lab_arr;
};

struct Student
{
    int student_index;
    pthread_t tid;
    int one;
    int two;
    int three;
    int course;
    int time;
    float calibre;
    int cur_priority;

    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

struct Lab *lab_arr = NULL;
struct Course *course_arr = NULL;
struct Student *student_arr = NULL;
int *is_course_avail = NULL;
int num_student_arr;
int num_lab_arr;
int num_course_arr;

void Input_handler()
{
    scanf("%d %d %d", &num_student_arr, &num_lab_arr, &num_course_arr);

    lab_arr = (struct Lab *)malloc(num_lab_arr * sizeof(struct Lab));
    course_arr = (struct Course *)malloc(num_course_arr * sizeof(struct Course));
    is_course_avail = (int *)malloc(num_course_arr * sizeof(int));
    student_arr = (struct Student *)malloc(num_student_arr * sizeof(struct Student));

    for (int i = 0; i < num_course_arr; i++)
    {
        float interest;
        int course_max_slot;
        int total_lab_arr;
        scanf("%s %f %d %d", course_arr[i].course_name, &interest, &course_max_slot, &total_lab_arr);
        course_arr[i].total_lab_arr = total_lab_arr;
        course_arr[i].assorted_lab_arr = (int *)malloc(course_arr[i].total_lab_arr * sizeof(int));
        for (int ii = 0; ii < course_arr[i].total_lab_arr; ii++)
        {
            scanf("%d", &course_arr[i].assorted_lab_arr[ii]);
        }
        course_arr[i].C_index = i;
        course_arr[i].interest = interest;
        is_course_avail[i] = 1;
        course_arr[i].course_max_slot = course_max_slot;
    }

    for (int i = 0; i < num_student_arr; i++)
    {
        int one;
        int two;
        int three;
        int my_time;
        scanf("%f %d %d %d %d", &student_arr[i].calibre, &one, &two, &three, &my_time);
        student_arr[i].time = my_time;
        student_arr[i].cur_priority = 0;
        student_arr[i].one = one;
        student_arr[i].two = two;
        student_arr[i].three = three;
        student_arr[i].course = -1;
        student_arr[i].student_index = i;
    }

    for (int i = 0; i < num_student_arr; i++)
    {
        pthread_mutex_init(&student_arr[i].mutex, NULL);
        pthread_cond_init(&student_arr[i].cond, NULL);
    }

    for (int i = 0; i < num_lab_arr; i++)
    {
        scanf("%s %d %d", lab_arr[i].name, &lab_arr[i].n_i, &lab_arr[i].max_TA_times);
        lab_arr[i].current_TA = (struct TA *)malloc(lab_arr[i].n_i * sizeof(struct TA));
        struct TA *ptr = lab_arr[i].current_TA;
        for (int ii = 0; ii < lab_arr[i].n_i; ii++)
        {
            ptr[ii].TA_id = ii;
            ptr[ii].cur_TA_times = 0;
            pthread_mutex_init(&ptr[ii].TA_mutex, NULL);
        }
    }
}

void *Student_Thd(void *arg)
{
    struct Student *S = (struct Student *)arg;
    sleep(S->time);
    BBLU
        printf("Student %d has filled in preferences for course registration\n", S->student_index);
    RESET

    S->cur_priority = 1;
    while (true)
    {
        pthread_mutex_lock(&S->mutex);
        if (S->cur_priority == 1 && is_course_avail[S->one] == 1)
        {
            pthread_cond_wait(&S->cond, &S->mutex);
        }
        else if (S->cur_priority == 2 && is_course_avail[S->two] == 1)
        {
            pthread_cond_wait(&S->cond, &S->mutex);
        }
        else if (S->cur_priority == 3 && is_course_avail[S->three] == 1)
        {
            pthread_cond_wait(&S->cond, &S->mutex);
        }

        if ((S->cur_priority == 1 && S->course == S->one) || (S->cur_priority == 2 && S->course == S->two) || (S->cur_priority == 3 && S->course == S->three))
        {
            if (S->calibre * course_arr[S->course].interest >= (rand() % 101) / (100.00))
            {
                BGRN
                    printf("Student %d has selected the course %s permanently\n", S->student_index, course_arr[S->course].course_name);
                RESET
                S->course = -2;
                pthread_mutex_unlock(&S->mutex);

                break;
            }
            else
            {
                S->cur_priority++;
                if (S->cur_priority < 4)
                {
                    BMAG
                        printf("Student %d has withdrawn from course %s \n", S->student_index, course_arr[S->course].course_name);
                    RESET

                    int c1 = S->one;
                    int c2 = S->two;
                    int c3 = S->three;
                    S->course = -1;
                    if (S->cur_priority == 2)
                    {
                        BYEL
                            printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, course_arr[c1].course_name, S->cur_priority - 1, course_arr[c2].course_name, S->cur_priority);
                        RESET
                    }
                    else if (S->cur_priority == 3)
                    {
                        BYEL
                            printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, course_arr[c2].course_name, S->cur_priority - 1, course_arr[c3].course_name, S->cur_priority);
                        RESET
                    }
                }

                else
                {
                    BYEL
                        printf("Student %d has withdrawn from course %s\n", S->student_index, course_arr[S->course].course_name);
                    RESET
                    BGRN
                        printf("Student %d couldn’t get any of his preferBRED courses\n", S->student_index);
                    RESET
                    S->course = -2;
                    pthread_mutex_unlock(&S->mutex);
                    break;
                }
            }
        }
        else
        {
            S->cur_priority++;
            if (S->cur_priority < 4)
            {
                int c1 = S->one;
                int c2 = S->two;
                int c3 = S->three;

                if (S->cur_priority == 2)
                {
                    BGRN
                        printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, course_arr[c1].course_name, S->cur_priority - 1, course_arr[c2].course_name, S->cur_priority);
                    RESET
                }
                else if (S->cur_priority == 3)
                {
                    BGRN
                        printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", S->student_index, course_arr[c2].course_name, S->cur_priority - 1, course_arr[c3].course_name, S->cur_priority);
                    RESET
                }
                S->course = -1;
            }
            else
            {
                BRED
                    printf("Student %d couldn’t get any of his preferred courses\n", S->student_index);
                RESET
                S->course = -2;
                pthread_mutex_unlock(&S->mutex);
                break;
            }
        }
        pthread_mutex_unlock(&S->mutex);
    }
}

void *Course_Thd(void *arg)
{
    struct Course *C = (struct Course *)arg;
    while (1)
    {
        bool course_av = false;
        int cur_lab_num = 0;
        while (cur_lab_num < C->total_lab_arr && course_av == false)
        {
            for (int j = 0; j < lab_arr[C->assorted_lab_arr[cur_lab_num]].n_i && course_av == false; j++)
            {
                if (lab_arr[C->assorted_lab_arr[cur_lab_num]].current_TA[j].cur_TA_times < lab_arr[C->assorted_lab_arr[cur_lab_num]].max_TA_times)
                {
                    course_av = true;
                }
            }
            cur_lab_num++;
        }
        if (course_av == false)
        {
            BMAG
                printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", C->course_name);
            RESET
            is_course_avail[C->C_index] = false;

            for (int i = 0; i < num_student_arr; i++)
            {
                if (student_arr[i].course == -1)
                {
                    if (student_arr[i].cur_priority == 1 && student_arr[i].one == C->C_index)
                    {
                        pthread_cond_signal(&student_arr[i].cond);
                    }
                    else if (student_arr[i].cur_priority == 3 && student_arr[i].three == C->C_index)
                    {
                        pthread_cond_signal(&student_arr[i].cond);
                    }
                    else if (student_arr[i].cur_priority == 2 && student_arr[i].two == C->C_index)
                    {
                        pthread_cond_signal(&student_arr[i].cond);
                    }
                }
            }
            break;
        }

        for (int k = 0; k < C->total_lab_arr; k++)
        {
            int which_taken[200];
            int how_many = 0;
            for (int kk = 0; kk < lab_arr[C->assorted_lab_arr[k]].n_i; kk++)
            {
                pthread_mutex_lock(&lab_arr[C->assorted_lab_arr[k]].current_TA[kk].TA_mutex);
                if (lab_arr[C->assorted_lab_arr[k]].current_TA[kk].cur_TA_times >= lab_arr[C->assorted_lab_arr[k]].max_TA_times)
                {
                    pthread_mutex_unlock(&lab_arr[C->assorted_lab_arr[k]].current_TA[kk].TA_mutex);
                }
                else
                {
                    int total_seats = (rand() % C->course_max_slot) + 1;
                    int seats_filled = 0;
                    for (int i = 0; i < num_student_arr; i++)
                    {
                        if (seats_filled < total_seats && student_arr[i].course == -1 && (student_arr[i].cur_priority == 1 && student_arr[i].one == C->C_index) || (student_arr[i].cur_priority == 3 && student_arr[i].three == C->C_index) || (student_arr[i].cur_priority == 2 && student_arr[i].two == C->C_index))
                        {
                            pthread_mutex_lock(&student_arr[i].mutex);
                            student_arr[i].course = C->C_index;
                            which_taken[how_many] = i;
                            seats_filled++;
                            how_many++;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    BMAG
                        printf("TA %d from the lab %d has been allocated to course %s for his %d", kk, C->assorted_lab_arr[k], C->course_name, lab_arr[C->assorted_lab_arr[k]].current_TA[kk].cur_TA_times + 1);
                    RESET
                    BMAG switch (lab_arr[C->assorted_lab_arr[k]].current_TA[kk].cur_TA_times + 1)
                    {
                    case (1):
                    {
                        printf("st");
                        break;
                    }
                    case (2):
                    {
                        printf("nd");
                        break;
                    }
                    case (3):
                    {
                        printf("rd");
                        break;
                    }
                    default:
                    {
                        printf("th");
                    }
                    }
                    BMAG
                        printf(" TA ship\n");
                    RESET

                    BBLU
                        printf("Course %s has been allocated %d seats\n", C->course_name, total_seats);
                    RESET

                    for (int i = 0; i < how_many; i++)
                    {
                        BYEL
                            printf("Student %d has been allocated a seat in course %s\n", which_taken[i], C->course_name);
                        RESET
                    }

                    printf("\033[1;34m"); // blue
                    printf("Tutorial has started for Course %s with %d seats filled out of %d\n", C->course_name, seats_filled, total_seats);
                    printf("\033[1;0m");

                    sleep(2);
                    lab_arr[C->assorted_lab_arr[k]].current_TA[kk].cur_TA_times++;
                    for (int i = 0; i < how_many; i++)
                    {
                        pthread_mutex_unlock(&student_arr[which_taken[i]].mutex);
                        pthread_cond_signal(&student_arr[which_taken[i]].cond);
                    }

                    pthread_mutex_unlock(&(lab_arr[C->assorted_lab_arr[k]].current_TA[kk].TA_mutex));

                    printf("\033[1;33m");
                    printf("TA %d from lab %s has completed the tutorial and left the course %s\n", kk, lab_arr[C->assorted_lab_arr[k]].name, C->course_name);
                    printf("\033[1;0m");

                    how_many = 0;
                    course_av = false;
                    for (int f = 0; f < lab_arr[C->assorted_lab_arr[k]].n_i && course_av == false; f++)
                    {
                        if ((lab_arr[C->assorted_lab_arr[k]].current_TA[f].cur_TA_times < lab_arr[C->assorted_lab_arr[k]].max_TA_times) && course_av == false)
                        {
                            course_av = true;
                        }
                    }

                    if (course_av == false)
                    {
                        BMAG
                            printf("Lab %s no longer has TA available for TA'ship\n", lab_arr[C->assorted_lab_arr[k]].name);
                        RESET
                    }
                }
            }
        }
    }
}

int main(void)
{
    Input_handler();
    srand(time(0));
    for (int i = 0; i < num_course_arr; i++)
    {
        pthread_create(&course_arr[i].tid, NULL, Course_Thd, &course_arr[i]);
    }
    for (int i = 0; i < num_student_arr; i++)
    {
        pthread_create(&student_arr[i].tid, NULL, Student_Thd, &student_arr[i]);
    }
    for (int i = 0; i < num_student_arr; i++)
    {
        pthread_join(student_arr[i].tid, NULL);
    }

    free(lab_arr);
    free(course_arr);
    free(student_arr);
    free(is_course_avail);

    return 0;
}