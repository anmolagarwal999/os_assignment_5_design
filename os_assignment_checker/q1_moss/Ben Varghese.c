
//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/funcs.h ####################//

#ifndef FUNCS_H
#define FUNCS_H

#include "user.h"
#include <unistd.h>
#include <time.h>

void *pref_fill(void *arg);
void *time_count(void *arg);
void *course_working(void *arg);
void allot_TA(course *Course, lab *Labs, int num_labs);
void allot_tut(course *Course, student Students[], int num_students);
void student_choice(course *Courses, int num_courses, student *Student);

#endif

//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/try.c ####################//

#include "funcs.h"

int main() {
    srand(time(0));
    int num_students, num_labs, num_courses;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    course *Courses = (course *) malloc(num_courses*sizeof(course));
    student *Students = (student *) malloc(num_students*sizeof(student));
    lab *Labs = (lab *) malloc(num_labs*sizeof(lab));

    init_course(Courses, num_courses);
    init_student(Students, num_students);
    init_lab(Labs, num_labs);

    printf("\n");
    
    pthread_mutex_init(&course_lock, NULL);
    pthread_mutex_init(&student_lock, NULL);
    pthread_mutex_init(&sc_lock, NULL);

    pthread_t course_threads[num_courses];
    pthread_t student_threads[num_students];

    course_student cs[num_students];
    for(int i=0; i<num_students; i++) {
        cs[i].Student = &Students[i];
        cs[i].Course = Courses;
        cs[i].num_courses = num_courses;
    }
    
    for(int i=0; i<num_students; i++) {
        pthread_create(&student_threads[i], NULL, pref_fill, (void *)&cs[i]);
    }
    sleep(1);
    csl c[num_courses];
    for(int i=0; i<num_courses; i++) {
        c[i].Course = &Courses[i];
        c[i].Student = Students;
        c[i].Lab = Labs;
        c[i].num_courses = num_courses;
        c[i].num_students = num_students;
        c[i].num_labs = num_labs;
    }
    for(int i=0; i<num_courses; i++) {
        pthread_create(&course_threads[i], NULL, course_working, (void *)&c[i]);
    }

    
    for(int i=0; i<num_courses; i++) {
        pthread_join(course_threads[i], NULL);
    }
    for(int i=0; i<num_students; i++) {
        pthread_join(student_threads[i], NULL);
    }

    pthread_mutex_destroy(&course_lock);
    pthread_mutex_destroy(&student_lock);
}

//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/structs.h ####################//

#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdio.h>
#include <pthread.h>

#define COLOR_PINK     "\x1b[95m"
#define COLOR_RESET    "\x1b[0m"

typedef struct courses {
    int course_id;              // course ID
    char course_name[200];      // course name
    double interest;            // course interest (b/w 0 and 1)
    int *lab_ids;               // set of labs for getting TAs
    int num_course_labs;        // number of labs
    int curr_TA_id;             // the ID of current TA
    char curr_TA_lab[100];      // the lab of current TA (may be NULL)
    int course_max_slot;        // max number of slots
    int status;                 // 0 => waiting for TA
                                // 1 => waiting for slots to be filled
                                // 2 => tutorial being conducted
                                // 3 => withdrawn
    pthread_cond_t course_cond;
} course;

typedef struct students {
    int student_id;             // student ID
    int pref1;                  // first preference (highest priority)
    int pref2;                  // second preference
    int pref3;                  // third preference (lowest priority)
    int curr_pref;              // current preference
    double calibre;             // student's calibre quotient
    int time;                   // preference submission time (in seconds)
    int status;                 // indicates what the student is doing -
                                    // 0 => waiting for 1st pref
                                    // 1 => attending 1st pref
                                    // 2 => waiting for 2nd pref
                                    // 3 => attending 2nd pref
                                    // 4 => waiting for 3rd pref
                                    // 5 => attending 3rd pref
                                    // 6 => selected course / didn't take any
} student;

typedef struct ta {
    int id;
    int times;                  // the number of times the TA was a TA
    int status;                 // indicates what the TA is doing -
                                    // 0 => available to TA
                                    // 1 => currently busy being TA
} TA;

typedef struct labs {
    int lab_id;                 // lab ID
    char lab_name[200];         // lab name
    int n_i;                    // number of TAs
    int limit;                  // lab limit
    TA *ta_arr;                 // array of TAs
} lab;

typedef struct CSL {
    course *Course;
    student *Student;
    lab *Lab;
    int num_courses;
    int num_students;
    int num_labs;
} csl;

typedef struct Course_student {
    course *Course;
    student *Student;
    int num_students;
    int num_courses;
    int time;
    TA *ta_mentor;
} course_student;

#endif

//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/user.c ####################//

#include "user.h"

void init_course(course Courses[], int num_courses) {
    int num_course_labs;
    for(int i=0; i<num_courses; i++) {
        Courses[i].course_id = i;
        scanf("%s", Courses[i].course_name);
        scanf("%lf", &Courses[i].interest);
        scanf("%d", &Courses[i].course_max_slot);
        scanf("%d", &Courses[i].num_course_labs);
        num_course_labs = Courses[i].num_course_labs;
        Courses[i].curr_TA_id = -1;
        Courses[i].status = 0;
        strcpy(Courses[i].curr_TA_lab, "None");
        Courses[i].lab_ids = (int *) malloc(num_course_labs*sizeof(int));
        for(int j=0; j<num_course_labs; j++) {
            scanf("%d", &Courses[i].lab_ids[j]);
        }
        pthread_cond_init(&Courses[i].course_cond, NULL);
    }
}

void print_courses(course Courses[], int num_courses) {
    for(int i=0; i<num_courses; i++) {
        printf("Course ID: %d\n", Courses[i].course_id);
        printf("Course name: %s\n", Courses[i].course_name);
        printf("Course interest: %lf\n", Courses[i].interest);
        printf("Course max slots: %d\n", Courses[i].course_max_slot);
        printf("Course number of labs: %d\n", Courses[i].num_course_labs);
        printf("Lab IDs - \n");
        for(int j=0; j<Courses[i].num_course_labs; j++) {
            printf("\tLab %d ID: %d\n", j+1, Courses[i].lab_ids[j]);
        }
        printf("\n");
    }
}

void init_student(student Students[], int num_students) {
    for(int i=0; i<num_students; i++) {
        Students[i].student_id = i;
        scanf("%lf", &Students[i].calibre);
        scanf("%d", &Students[i].pref1);
        scanf("%d", &Students[i].pref2);
        scanf("%d", &Students[i].pref3);
        scanf("%d", &Students[i].time);
        Students[i].status = -1;
        Students[i].curr_pref = Students[i].pref1;
    }
}

void print_students(student Students[], int num_students) {
    for(int i=0; i<num_students; i++) {
        printf("Student ID: %d\n", Students[i].student_id);
        printf("Student calibre: %lf\n", Students[i].calibre);
        printf("Student first preference: %d\n", Students[i].pref1);
        printf("Student second preference: %d\n", Students[i].pref2);
        printf("Student third preference: %d\n", Students[i].pref3);
        printf("Student filling time: %d\n", Students[i].time);
        printf("Status: ");
        if(Students[i].status==0) {
            printf("Waiting for first preference\n");
        }
        else if(Students[i].status==1) {
            printf("Attending first preference\n");
        }
        else if(Students[i].status==2) {
            printf("Waiting for second preference\n");
        }
        else if(Students[i].status==3) {
            printf("Attending second preference\n");
        }
        else if(Students[i].status==4) {
            printf("Waiting for  third preference\n");
        }
        else if(Students[i].status==5) {
            printf("Attending third preference)\n");
        }
        else if(Students[i].status==6) {
            printf("Withdrawn\n");
        }
        else printf("Unknown");
        printf("\n");
    }
}

void init_lab(lab Labs[], int num_labs) {
    for(int i=0; i<num_labs; i++) {
        Labs[i].lab_id = i;
        scanf("%s", Labs[i].lab_name);
        scanf("%d", &Labs[i].n_i);
        scanf("%d", &Labs[i].limit);
        Labs[i].ta_arr = (TA *) malloc(Labs[i].n_i*sizeof(TA));
        for(int j=0; j<Labs[i].n_i; j++) {
            Labs[i].ta_arr[j].id = j;
            Labs[i].ta_arr[j].times = 0;
            Labs[i].ta_arr[j].status = 0;
        }
    }
}

void print_labs(lab Labs[], int num_labs) {
    for(int i=0; i<num_labs; i++) {
        printf("Lab ID: %d\n", Labs[i].lab_id);
        printf("Lab name: %s\n", Labs[i].lab_name);
        printf("Lab number of TAs: %d\n", Labs[i].n_i);
        printf("Lab limit for TAs: %d\n", Labs[i].limit);
        printf("TAs - \n");
        for(int j=0; j<Labs[i].n_i; j++) {
            printf("TA ID: %d\n", Labs[i].ta_arr[j].id);
            printf("TA times: %d\n", Labs[i].ta_arr[j].times);
            printf("TA status: %d\n", Labs[i].ta_arr[j].status);
        }
        printf("\n");
    }
}

int find_current_pref(student Student) {
    if(Student.status==0) {
        return Student.pref1;
    }
    else if(Student.status==2) {
        return Student.pref2;
    }
    else if(Student.status==4) {
        return Student.pref3;
    }
    else return -1;
}

//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/try2.c ####################//

#include "funcs.h"

pthread_cond_t student_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t courses_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t sc_cond = PTHREAD_COND_INITIALIZER;

void *pref_fill(void *arg) {
    course_student *cs = (course_student *) arg;
    student Student = *cs->Student;
    course *Courses = cs->Course;
    int num_courses = cs->num_courses;
    sleep(Student.time);
    pthread_mutex_lock(&student_lock);
    printf("Student %d has filled" COLOR_PINK " in " COLOR_RESET "preferences" COLOR_PINK " for " COLOR_RESET "course registration\n", Student.student_id);
    cs->Student->status = 0;
    Student = *cs->Student;
    pthread_cond_broadcast(&sc_cond);
    while(cs->Student->status%2==0) {
        pthread_cond_wait(&student_cond, &student_lock);
        Student = *cs->Student;
    }
    if(cs->Student->status%2!=0) {
        double interest;
        char cname[200];
        for(int i=0; i<num_courses; i++) {
            if(cs->Student->status==1 && Student.pref1==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
            else if(cs->Student->status==3 && Student.pref2==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
            else if(cs->Student->status==5 && Student.pref3==Courses[i].course_id) {
                interest = Courses[i].interest;
                strcpy(cname, Courses[i].course_name);
                break;
            }
        }
        double probability = interest*Student.calibre;
        probability *= 100;
        int prob = (int) probability;
        int choice = rand()%100;
        if(choice<prob) {
            cs->Student->status = 6;
            printf("Student %d has selected the course %s permanently\n", Student.student_id, cname);
        }
        else {
            printf("Student %d has withdrawn from course %s\n", Student.student_id, cname);
            int flag = 0;
            char ncname[200];
            if(cs->Student->status==1) {
                for(int i=0; i<num_courses; i++) {
                    if(Student.pref2==Courses[i].course_id) {
                        if(Courses[i].status==3) {
                            cs->Student->status = 3;
                            flag++;
                            break;
                        }
                        strcpy(ncname, Courses[i].course_name);
                    }
                }
                if(flag==0) {
                    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n", Student.student_id, cname, ncname);
                }
            }
            if(cs->Student->status==3) {
                for(int i=0; i<num_courses; i++) {
                    if(Student.pref3==Courses[i].course_id) {
                        if(Courses[i].status==3) {
                            cs->Student->status = 5;
                            flag++;
                            break;
                        }
                        strcpy(ncname, Courses[i].course_name);
                    }
                }
                if(flag==0) {
                    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
                else if(flag==1 && cs->Student->status==3) {
                    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
                else if(flag==1 && cs->Student->status==5) {
                    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", Student.student_id, cname, ncname);
                }
            }
            cs->Student->status += 1;

            if(cs->Student->status==6) {
                printf("Student %d could not get any of his preferred courses\n", Student.student_id);
            }
        }
    }

    pthread_mutex_unlock(&student_lock);
}

void *course_working(void *arg) {
    csl *c = (csl *) arg;
    course *Course = c->Course;
    student *Students = c->Student;
    lab *Labs = c->Lab;
    //int num_courses = c->num_courses;
    int num_students = c->num_students;
    int num_labs = c->num_labs;
    
    while(1) {
        int flag = 0;           // will be set to 1 if a TA is found
        int count = 0;          // keeps track of the number of TAs who have already reached their limit
        pthread_mutex_lock(&course_lock);
        while(1) {
            for(int i=0; i<Course->num_course_labs; i++) {
                for(int j=0; j<num_labs; j++) {
                    if(Labs[j].lab_id==Course->lab_ids[i]) {
                        for(int k=0; k<Labs[j].n_i; k++) {
                            if(Labs[j].ta_arr[k].status==0 && Labs[j].ta_arr[k].times<Labs[j].limit) {
                                printf("TA %d from lab %s has been allocated to course %s. Past experience count: %d\n", Labs[j].ta_arr[k].id, Labs[j].lab_name, Course->course_name, Labs[j].ta_arr[k].times);
                                // Past experience count gives the number of times the TA has been a TA
                                flag = 1;
                                Labs[j].ta_arr[k].status = 1;
                                Labs[j].ta_arr[k].times++;
                                Course->status = 1;
                                Course->curr_TA_id = Labs[j].ta_arr[k].id;
                                strcpy(Course->curr_TA_lab, Labs[j].lab_name);
                                break;
                            }
                            else if(Labs[j].ta_arr[k].times==Labs[j].limit) {
                                count++;
                            }
                        }
                    }
                    if(flag==1) {
                        break;
                    }
                }
                if(flag==1) {
                    break;
                }
            }
            if(flag==0) {                               // no TA was found
                int limit = 0;
                for(int i=0; i<num_labs; i++) {         // checking if any TA will be available or the course should be withdrawn
                    limit += Labs[i].n_i;
                }
                if(count==limit) {
                    printf("Course %s has been withdrawn\n", Course->course_name);
                    Course->status = 3;
                }
                else {
                    pthread_cond_wait(&courses_cond, &course_lock);
                    continue;
                }
            }
            break;
        }

        int d = rand()%Course->course_max_slot;
        d++;
        printf("Course %s has been allocated %d seats\n", Course->course_name, d);
        int w=0;
        
        for(int i=0; i<num_students; i++) {
            if(Students[i].curr_pref==Course->course_id && (Students[i].status==0 || Students[i].status==2 || Students[i].status==4)) {
                w++;
                pthread_mutex_lock(&student_lock);
                Students[i].status++;
                pthread_cond_broadcast(&student_cond);
                pthread_mutex_unlock(&student_lock);
                printf("Student %d has been allocated a seat" COLOR_PINK " in " COLOR_RESET "course %s\n", Students[i].student_id, Course->course_name);
                if(w==d) {
                    break;
                }
            }
        }

        Course->status = 2;

        int flag2 = 0;
        for(int j=0; j<num_labs; j++) {
            if(strcmp(Labs[j].lab_name, Course->curr_TA_lab)==0) {
                for(int k=0; k<Labs[j].n_i; k++) {
                    if(Labs[j].ta_arr[k].id == Course->curr_TA_id) {
                        Labs[j].ta_arr[k].status = 0;
                        flag2 = 1;
                        break;
                    }
                }
            }
            if(flag2==1) {
                break;
            }
        }
        
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", Course->course_name, w, d);
        printf("TA %d from lab %s has completed the tutorial" COLOR_PINK " for " COLOR_RESET "course %s\n", Course->curr_TA_id, Course->curr_TA_lab, Course->course_name);
        Course->curr_TA_id = -1;
        strcpy(Course->curr_TA_lab, "None");
        pthread_mutex_unlock(&course_lock);
        sleep(5);
        
        pthread_cond_broadcast(&courses_cond);
        int sc_flag = 0;
        for(int i=0; i<num_students; i++) {
            if(Students[i].status!=6) {
                sc_flag = 1;
                break;
            }
        }
        if(sc_flag==1) {
            continue;
        }

        break;
    }
}

//###########FILE CHANGE ./main_folder/Ben Varghese_305873_assignsubmission_file_/2020101115/q1/user.h ####################//

#ifndef USER_H
#define USER_H

#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <string.h>
#include "structs.h"

pthread_mutex_t course_lock;
pthread_mutex_t student_lock;
pthread_mutex_t sc_lock;

void init_course(course Courses[], int num_courses);
void init_student(student Students[], int num_students);
void init_lab(lab Labs[], int num_labs);

void print_courses(course Courses[], int num_courses);
void print_students(student Students[], int num_students);
void print_labs(lab Labs[], int num_labs);

int find_current_pref(student Student);

#endif
