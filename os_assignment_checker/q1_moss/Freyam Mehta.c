
//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q1/student.c ####################//

#include "headers.h"

void *student_handler(void *arg) {
    student *stud = (student *)arg;

    clock_t diff;
    do {
        diff = clock() - time_0;
        diff /= 1000000;         // CLOCKS_PER_SEC = 1000000
    } while (diff < stud->time); // wait for the time to arrive (sleeeeeeeeeep)

    stud->registered = true;
    printf(RED "Student %d has filled in preferences for course registration\n" RESET, stud->ID);

    while (1) {
        pthread_mutex_lock(student_mutex + stud->ID); // lock student mutex

        bool flag_f = courses[stud->pref[stud->curr_pref]].is_removed; // check if the course is removed
        while (stud->attended == false) {                              // check if the student has attended the course
            if (flag_f == true)
                break;

            pthread_cond_wait(allocated + stud->ID, student_mutex + stud->ID); // wait for the course to be allocated
        }

        if (stud->curr_pref > 2) {
            stud->done = true;
            printf(CYAN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->ID); // if the student couldn’t get any of his preferred courses

            pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
            break;
        }

        while (flag_f) {
            // magic happens here
        }

        pthread_mutex_unlock(student_mutex + stud->ID); // unlock student mutex
    }

    return NULL;
}

//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q1/main.c ####################//

#include "headers.h"

int N_STUDENTS; // number of students
int N_LABS;     // number of labs
int N_COURSES;  // number of courses

void courses_input(void) {
    int length = N_COURSES;
    for (int i = 0; i < length; ++i) {
        courses[i].ID = i;

        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].max_slots, &courses[i].N_labs);
        for (int j = 0; j < courses[i].N_labs; j++)
            scanf("%d", &courses[i].LAB_IDS[j]);

        courses[i].is_removed = false;
        courses[i].LAB_IDS = (int *)malloc(sizeof(int) * (courses[i].N_labs));

        pthread_mutex_init(course_mutex + i, NULL);
    }
}

void students_input() {
    int length = N_STUDENTS;
    for (int i = 0; i < length; ++i) {
        students[i].ID = i;
        scanf("%f", &students[i].calibre);

        students[i].attended = false;
        students[i].registered = false;

        scanf("%d %d %d", &students[i].pref[0], &students[i].pref[1], &students[i].pref[2]);

        students[i].course_allocated = -1;

        scanf("%d", &students[i].time);

        pthread_cond_init(allocated + i, NULL);
        pthread_mutex_init(student_mutex + i, NULL);
    }
}

void labs_input() {
    int length = N_LABS;
    for (int i = 0; i < length; ++i) {
        labs[i].ID = i;
        labs[i].lk_TAs = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * labs[i].CAP);
        labs[i].flag = true;
        labs[i].lab_TAs = (int *)malloc(sizeof(int) * labs[i].CAP);

        scanf("%s %d %d", labs[i].name, &labs[i].CAP, &labs[i].max_limit);
        for (int j = 0; j < labs[i].CAP; j++) {
            pthread_mutex_init(labs[i].lk_TAs + j, NULL);
            labs[i].lab_TAs[j] = 0;
        }
    }
}

void simulation() {
    srand(time(0));

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_create(&studentT[i], NULL, student_handler, students + i); // student thread

    for (int i = 0; i < N_COURSES; ++i)
        pthread_create(&courseT[i], NULL, course_handler, courses + i); // course thread

    for (int i = 0; i < N_STUDENTS; ++i)
        pthread_join(studentT[i], NULL); // wait for student thread to finish

    for (int i = 0; i < N_COURSES; ++i)
        pthread_join(courseT[i], NULL); // wait for course thread to finish

    for (int i = 0; i < N_STUDENTS; ++i) {
        pthread_mutex_destroy(student_mutex + i); // destroy student mutex
        pthread_cond_destroy(allocated + i);      // destroy allocated condition
    }

    for (int i = 0; i < N_COURSES; ++i)
        pthread_mutex_destroy(course_mutex + i); // destroy course mutex
}

int main() {
    scanf("%d %d %d", &N_STUDENTS, &N_LABS, &N_COURSES);

    courses_input();
    students_input();
    labs_input();

    simulation();

    return 0;
}

//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q1/course.c ####################//

#include "headers.h"

void *course_handler(void *arg) {
    course *c = (course *)arg;
    while (1) {
        // magic happens here
    }
}

//###########FILE CHANGE ./main_folder/Freyam Mehta_305800_assignsubmission_file_/2020101114_assignment_5/q1/headers.h ####################//

#ifndef __MAIN__H__
#define __MAIN__H__

#define BLUE "\x1B[34m"
#define CYAN "\x1B[36m"
#define GREEN "\x1B[32m"
#define RED "\x1B[31m"
#define RESET "\x1B[0m"
#define YELLOW "\x1B[33m"

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

typedef struct student {
    bool attended;        // true if student attended a lab
    bool done;            // true if student is done
    bool registered;      // true if registered for a course
    float calibre;        // student's calibre
    int course_allocated; // course allocated to student
    int curr_pref;        // current preference
    int ID;               // student ID
    int pref[3];          // student's preferences
    int time;             // time student attended lab
} student;

typedef struct course {
    bool is_removed; // true if course is is_removed
    char name[75];   // course name
    float interest;  // course interest
    int *LAB_IDS;    // labs for course
    int ID;          // course ID
    int max_slots;   // max slots for course
    int N_labs;      // total labs for course
} course;

typedef struct lab {
    bool flag;               // true if lab is full
    char name[75];           // lab name
    int *lab_TAs;            // lab TAs
    int CAP;                 // lab CAP
    int ID;                  // lab ID
    int max_limit;           // max limit for lab
    pthread_mutex_t *lk_TAs; // lock for lab TAs
} lab;

#define MX_COURSES 500 // max courses
#define MX_LABS 500    // max labs
#define MX_STUDS 500   // max students

// global variables

clock_t time_0;                     // time time_0 lab starts
pthread_cond_t allocated[MX_STUDS]; // condition for student to attend lab

course courses[MX_COURSES];               // course list
pthread_t courseT[MX_COURSES];            // course thread
pthread_mutex_t course_mutex[MX_COURSES]; // course mutex

lab labs[MX_LABS]; // lab list
// pthread_t labT[MX_LABS];            // lab thread
// pthread_mutex_t lab_mutex[MX_LABS]; // lab mutex

student students[MX_STUDS];              // student list
pthread_t studentT[MX_STUDS];            // student thread
pthread_mutex_t student_mutex[MX_STUDS]; // student mutex

void students_input(); // get students details
void courses_input();  // get courses details
void labs_input();     // get labs details

void *course_handler(void *arg);  // course handler
void *handle_labs(void *arg);     // lab handler
void *student_handler(void *arg); // student handler

#endif