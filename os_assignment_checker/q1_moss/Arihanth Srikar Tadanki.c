
//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/2019113005_assignment_5/q1/q1.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#define NUM_COURSES_PER_STUDENT 3
#define COURSE_NAME_SIZE 15
#define LAB_NAME_SIZE 15
#define PTHREAD_COND_SIZE sizeof(pthread_cond_t)
#define PTHREAD_MUTEX_SIZE sizeof(pthread_mutex_t)
#define OCCUPIED 0
#define FREE 1
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

pthread_t *courses_thread = NULL, *students_thread = NULL;

// >= 0 => attending a tutorial or is registered
// <0 => waiting for course
int *student_sim;
pthread_cond_t *student_isAvailable;
pthread_mutex_t *student_lock;

// -1 if course has been withdrawn
int *course_spots_left;
pthread_cond_t *course_spots_isAvailable;
pthread_mutex_t *course_lock;

// remaining all_courses of a TA, TA status
int ***ta;
pthread_cond_t **ta_isAvailable;
pthread_mutex_t **ta_lock;

typedef struct student
{
    int id;
    int preferences[NUM_COURSES_PER_STUDENT];
    float calibre;
    int fill_time;
} student;

typedef struct course
{
    int id;
    int *labs;
    float interest;
    int num_labs;
    int course_max_slots;
    char name[LAB_NAME_SIZE];
} course;

typedef struct lab
{
    int id;
    int num_tas;
    int max_courses;
    char name[COURSE_NAME_SIZE];
} lab;

int num_students, num_labs, num_courses;
student *all_students;
course *all_courses;
lab *all_labs;

// conditional varirable to wait for TA of that course
void student_waitfor_ta(int lab_id, int ta_num)
{
    while (ta[lab_id][ta_num][1] < FREE)
    {
        if (ta[lab_id][ta_num][0] < 1)
            break;
        else
            pthread_cond_wait(&ta_isAvailable[lab_id][ta_num], &ta_lock[lab_id][ta_num]);
    }
}

void *course_allocation(void *args)
{
    int chosen_ta_lab_id, chosen_ta_id;
    course p_course = *(course *)args;

    for (;;)
    {
        chosen_ta_lab_id = -1;
        chosen_ta_id = -1;
        int lab_no, num_labs = p_course.num_labs;
        int i;
        for (lab_no = 0; lab_no < num_labs; lab_no++) // check each lab
        {
            int lab_id = p_course.labs[lab_no];
            int ta_num;
            for (ta_num = 0; ta_num < all_labs[lab_id].num_tas; ta_num++) // look for TAs in the lab
            {
                int isFoundTA;
                pthread_mutex_lock(&ta_lock[lab_id][ta_num]);
                student_waitfor_ta(lab_id, ta_num);
                isFoundTA = 0; // TA is picked for the course
                if (ta[lab_id][ta_num][1] > OCCUPIED && ta[lab_id][ta_num][0] > 0)
                {
                    chosen_ta_lab_id = lab_id;
                    chosen_ta_id = ta_num;
                    ta[lab_id][ta_num][1] = OCCUPIED;
                    ta[lab_id][ta_num][0]--;
                    isFoundTA = 1;
                }
                pthread_mutex_unlock(&ta_lock[lab_id][ta_num]);

                // allocated TA so quit this simulation
                if (isFoundTA)
                    break;
            }
            // allocated TA so quit this simulation
            if (chosen_ta_id > -1)
                break;
        }

        int id = p_course.id;
        if (chosen_ta_id < 0) // course is now removed
        {
            pthread_mutex_lock(&course_lock[id]);
            course_spots_left[id] = -1;
            pthread_mutex_unlock(&course_lock[id]);
            pthread_cond_broadcast(&course_spots_isAvailable[id]);
            printf(GRN "Course %s does not have any TA's eligible and is removed from course offerings\n" RESET, p_course.name);
            printf(GRN "Course thread %s has ended\n" RESET, p_course.name);
            return NULL;
        }
        // TA found
        char name[LAB_NAME_SIZE];
        strcpy(name, all_labs[chosen_ta_lab_id].name);
        pthread_mutex_lock(&ta_lock[chosen_ta_lab_id][chosen_ta_id]);
        int taship_num = all_labs[chosen_ta_lab_id].max_courses - ta[chosen_ta_lab_id][chosen_ta_id][0];
        printf(RED "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" RESET, chosen_ta_id, name, p_course.name, taship_num);
        pthread_mutex_unlock(&ta_lock[chosen_ta_lab_id][chosen_ta_id]);

        int tut_slots = (rand() % p_course.course_max_slots) + 1;
        int left_spots_in_lab = 0;
        for (i = 0; i < all_labs[chosen_ta_lab_id].num_tas; i++)
        {
            pthread_mutex_lock(&ta_lock[chosen_ta_lab_id][i]);
            left_spots_in_lab = left_spots_in_lab + ta[chosen_ta_lab_id][i][0];
            pthread_mutex_unlock(&ta_lock[chosen_ta_lab_id][i]);
        }
        if (left_spots_in_lab == 0)
            printf(RED "Lab %s no longer has mentors available for TAship\n" RESET, name);

        // update free tutorial slots
        pthread_mutex_lock(&course_lock[id]);
        course_spots_left[id] = tut_slots;
        pthread_mutex_unlock(&course_lock[id]);

        printf(GRN "Course %s has been allocated %d seats\n" RESET, p_course.name, tut_slots);

        int filled_seats = 0;
        int waiting_students = tut_slots;
        while ((filled_seats < tut_slots && waiting_students != 0) || (filled_seats < 1))
        {
            pthread_cond_broadcast(&course_spots_isAvailable[id]);

            int oneindex_id = id + 1;
            waiting_students = 0;
            filled_seats = 0;
            int i;
            for (i = 0; i < num_students; i++)
            {
                pthread_mutex_lock(&student_lock[i]);
                if (student_sim[i] == oneindex_id)
                    filled_seats++;
                if (student_sim[i] == -1 * oneindex_id)
                    waiting_students++;
                pthread_mutex_unlock(&student_lock[i]);
            }

            if (waiting_students == 0)
                break;
        }

        filled_seats = 0;
        int oneindex_id = id + 1;
        for (i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(&student_lock[i]);
            if (student_sim[i] == oneindex_id)
                filled_seats++;
            pthread_mutex_unlock(&student_lock[i]);
        }

        printf(RED "TA %d has started tutorial for Course %s with %d seats filled out of %d\n" RESET, chosen_ta_id, p_course.name, filled_seats, tut_slots);

        pthread_mutex_lock(&course_lock[id]);
        course_spots_left[id] = 0;
        pthread_mutex_unlock(&course_lock[id]);

        sleep(3); // conducting a tutorial

        for (i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(&student_lock[i]);
            if (student_sim[i] == oneindex_id)
            {
                student_sim[i] = 0;
                pthread_cond_broadcast(&student_isAvailable[i]);
            }
            pthread_mutex_unlock(&student_lock[i]);
        }

        printf(RED "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, chosen_ta_id, name, p_course.name);

        pthread_mutex_lock(&ta_lock[chosen_ta_lab_id][chosen_ta_id]);
        ta[chosen_ta_lab_id][chosen_ta_id][1] = FREE; // Ta is free
        pthread_cond_broadcast(&ta_isAvailable[chosen_ta_lab_id][chosen_ta_id]);
        pthread_mutex_unlock(&ta_lock[chosen_ta_lab_id][chosen_ta_id]);
    }

    printf(GRN "Course thread %s has ended\n" RESET, p_course.name);

    return NULL;
}

void *look_for_courses(void *args)
{
    student p_student = *(student *)args;

    sleep(p_student.fill_time);
    printf(BLU "Student %d has filled in preferences for course registration\n" RESET, p_student.id);

    int isFoundCourse = 0;
    int current_pref_course = -1;
    int current_pref = -1; // has 3 prefs
    int i;
    for (i = 0; i < 3; i++)
    {
        if (isFoundCourse)
            break;
        pthread_mutex_lock(&course_lock[p_student.preferences[i]]);
        if (course_spots_left[p_student.preferences[i]] != -1)
        {
            current_pref_course = course_spots_left[p_student.preferences[i]];
            current_pref = i;
            isFoundCourse = 1;
        }
        pthread_mutex_unlock(&course_lock[p_student.preferences[i]]);
    }

    while (current_pref >= 0) // student has a course to take up
    {
        int one_indexed_pref = current_pref_course + 1, id = p_student.id;
        pthread_mutex_lock(&student_lock[id]);
        student_sim[id] = -1 * one_indexed_pref; // student is waiting
        pthread_mutex_unlock(&student_lock[id]);

        int course_withdrawn = 0, isWaiting = 0;
        pthread_mutex_lock(&course_lock[current_pref_course]);
        isWaiting = 1;
        while (course_spots_left[current_pref_course] == 0)
            pthread_cond_wait(&course_spots_isAvailable[current_pref_course], &course_lock[current_pref_course]);
        isWaiting = 0;
        if (course_spots_left[current_pref_course] <= 0)
            course_withdrawn = 1;
        else
            course_spots_left[current_pref_course]--;
        pthread_mutex_unlock(&course_lock[current_pref_course]);

        if (!course_withdrawn)
        {
            printf(BLU "Student %d has been allocated a seat in course %s\n" RESET, id, all_courses[current_pref_course].name);

            one_indexed_pref = current_pref_course + 1;
            pthread_mutex_lock(&student_lock[id]);
            student_sim[id] = one_indexed_pref;
            pthread_mutex_unlock(&student_lock[id]);

            srand((unsigned int)time(NULL));
            float a = 1.0;
            float x = ((float)rand() / (float)(RAND_MAX)) * a;
            float withdraw_probability = all_courses[current_pref_course].interest * p_student.calibre;

            pthread_mutex_lock(&student_lock[id]);
            pthread_cond_wait(&student_isAvailable[id], &student_lock[id]);
            pthread_mutex_unlock(&student_lock[id]);

            if (x >= withdraw_probability) // check for course withdrawal
            {
                printf(BLU "Student %d has withdrawn from course %s\n" RESET, id, all_courses[current_pref_course].name);
                course_withdrawn = 1;
            }
            else
            {
                printf(BLU "Student %d has selected course %s permanently\n" RESET, id, all_courses[current_pref_course].name);
                pthread_mutex_lock(&student_lock[id]);
                student_sim[id] = 0;
                pthread_mutex_unlock(&student_lock[id]);
                printf(BLU "Student thread %d has ended\n" RESET, id);
                return NULL;
            }
        }
        if (course_withdrawn)
        {
            one_indexed_pref = current_pref_course + 1;
            int previous_pref_course = current_pref_course;
            for (i = one_indexed_pref; i < 3; i++) // check for other prefs
            {
                int pref = p_student.preferences[i];
                pthread_mutex_lock(&course_lock[pref]);
                if (course_spots_left[pref] != -1)
                {
                    current_pref_course = course_spots_left[pref];
                    current_pref = i;
                }
                pthread_mutex_unlock(&course_lock[pref]);
            }
            if (previous_pref_course != current_pref_course)
                printf(BLU "Student %d has changed current preference from %s to %s\n" RESET, id, all_courses[previous_pref_course].name, all_courses[current_pref_course].name);
            else
            {
                isFoundCourse = 0;
                current_pref = -1;
            }
        }
    }

    if (!isFoundCourse)
        printf(BLU "Student %d did not get any of their preferred all_courses\n" RESET, p_student.id);

    printf(BLU "Student thread %d has ended\n" RESET, p_student.id);

    return NULL;
}

void take_input()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    all_students = malloc(sizeof(student) * num_students);
    all_labs = malloc(sizeof(lab) * num_labs);
    all_courses = malloc(sizeof(course) * num_courses);

    int i;
    for (i = 0; i < num_courses; i++) // all_courses
    {
        char name[COURSE_NAME_SIZE];
        float interest;
        int max_slots, no_of_labs;
        scanf("%s %f %d %d", name, &interest, &max_slots, &no_of_labs);
        strcpy(all_courses[i].name, name);
        all_courses[i].interest = interest;
        all_courses[i].course_max_slots = max_slots;
        all_courses[i].num_labs = no_of_labs;
        all_courses[i].id = i;
        all_courses[i].labs = malloc(sizeof(int) * all_courses[i].num_labs);
        int ii;
        for (ii = 0; ii < all_courses[i].num_labs; ii++)
            scanf("%d", &all_courses[i].labs[ii]);
    }

    for (i = 0; i < num_students; i++) // all_students
    {
        float calibre;
        int pref1, pref2, pref3, start_time;
        scanf("%f %d %d %d %d", &calibre, &pref1, &pref2, &pref3, &start_time);
        all_students[i].calibre = calibre;
        all_students[i].preferences[0] = pref1;
        all_students[i].preferences[1] = pref2;
        all_students[i].preferences[2] = pref3;
        all_students[i].fill_time = start_time;
        all_students[i].id = i;
    }

    for (i = 0; i < num_labs; i++) // labs
    {
        char name[LAB_NAME_SIZE];
        int no_of_tas, max_courses;
        scanf("%s %d %d", name, &no_of_tas, &max_courses);
        strcpy(all_labs[i].name, name);
        all_labs[i].num_tas = no_of_tas;
        all_labs[i].max_courses = max_courses;
        all_labs[i].id = i;
    }
}

void init_courses()
{
    int i;
    course_spots_left = malloc(sizeof(int) * num_courses);
    course_spots_isAvailable = malloc(PTHREAD_COND_SIZE * num_courses);
    course_lock = malloc(PTHREAD_MUTEX_SIZE * num_courses);
    for (i = 0; i < num_courses; i++)
    {
        course_spots_left[i] = 0;
        pthread_cond_init(&course_spots_isAvailable[i], NULL);
        pthread_mutex_init(&course_lock[i], NULL);
    }
}

void init_labTAs()
{
    int i;
    ta = malloc(sizeof(int **) * num_labs);
    ta_isAvailable = malloc(sizeof(pthread_cond_t *) * num_labs);
    ta_lock = malloc(sizeof(pthread_mutex_t *) * num_labs);
    for (i = 0; i < num_labs; i++)
    {
        int ii;
        ta[i] = malloc(sizeof(int *) * all_labs[i].num_tas);
        ta_isAvailable[i] = malloc(PTHREAD_COND_SIZE * all_labs[i].num_tas);
        ta_lock[i] = malloc(PTHREAD_MUTEX_SIZE * all_labs[i].num_tas);
        int alloc_size = 2 * sizeof(int);
        for (ii = 0; ii < all_labs[i].num_tas; ii++)
        {
            ta[i][ii] = malloc(alloc_size);
            ta[i][ii][1] = FREE;
            ta[i][ii][0] = all_labs[i].max_courses;
            pthread_cond_init(&ta_isAvailable[i][ii], NULL);
            pthread_mutex_init(&ta_lock[i][ii], NULL);
        }
    }
}

void init_students()
{
    int i;
    student_sim = malloc(sizeof(int) * num_students);
    student_isAvailable = malloc(PTHREAD_COND_SIZE * num_students);
    student_lock = malloc(PTHREAD_MUTEX_SIZE * num_students);
    for (i = 0; i < num_students; i++)
    {
        pthread_cond_init(&student_isAvailable[i], NULL);
        pthread_mutex_init(&student_lock[i], NULL);
        student_sim[i] = -1 * (all_students[i].preferences[0] + 1);
    }
}

int main(int argc, char *argv[])
{
    int i;
    take_input();

    init_courses();
    init_labTAs();
    init_students();

    // allocate space and create threads
    students_thread = malloc(sizeof(pthread_t) * num_students);
    for (i = 0; i < num_students; i++)
        pthread_create(&students_thread[i], NULL, look_for_courses, &all_students[i]);
    courses_thread = malloc(sizeof(pthread_t) * num_courses);
    for (i = 0; i < num_courses; i++)
        pthread_create(&courses_thread[i], NULL, course_allocation, &all_courses[i]);

    // wait for completion of threads
    for (i = 0; i < num_students; i++)
        pthread_join(students_thread[i], NULL);
    for (i = 0; i < num_courses; i++)
        pthread_join(courses_thread[i], NULL);

    return 0;
}
//###########FILE CHANGE ./main_folder/Arihanth Srikar Tadanki_305992_assignsubmission_file_/__MACOSX/2019113005_assignment_5/q1/._q1.c ####################//

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#define NUM_OF_SEMAPHORES 3
#define PERSON_NAME_SIZE 15
#define LAB_NAME_SIZE 15
#define PTHREAD_COND_SIZE sizeof(pthread_cond_t)
#define PTHREAD_MUTEX_SIZE sizeof(pthread_mutex_t)
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct person
{
    int id;
    int reach_time;
    int group_no;
    int no_of_sems;
    int no_of_goals;
    int patience;
    char name[PERSON_NAME_SIZE];
    char sem_types[NUM_OF_SEMAPHORES];
    char fan_type;
} person;
person *people;

typedef struct arg_struct
{
    person arg_person;
    char zone;
} args;
args *team_args;

typedef struct team
{
    int id;
    int no_of_chances;
    int *prev_chance;
    float *goal_prob;
    char team_type;
} team;
team *teams;

pthread_t *people_thread, *teams_thread;
sem_t h_zone, a_zone, n_zone;
int h_capacity, a_capacity, n_capacity, spectating_time, num_people = 0;

// keeps track of goals of every team
int goals[2];
pthread_cond_t goals_changed[2];
pthread_mutex_t goals_lock[2];

// keeps track of the zones a person is in
char *person_in_zone;
pthread_mutex_t *person_zone_lock;
pthread_cond_t *person_moved;

int char_cmp(char *input_char, char check_char[])
{
    char temp[2];
    temp[0] = *(char *)input_char;
    temp[1] = '\0';
    return strcmp(temp, check_char);
}

void *search_in_N(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&n_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'N'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone N\n" RESET, cur_person.name);
        }
        else
            sem_post(&n_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_A(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&a_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT)
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'A'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone A\n" RESET, cur_person.name);
        }
        // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
        else
            sem_post(&a_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *search_in_H(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int id = cur_person.id, patience = cur_person.patience;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += patience;

    // wait till patience runs out
    int isPatience = sem_timedwait(&h_zone, &ts);
    pthread_mutex_lock(&person_zone_lock[id]);
    if (isPatience == -1 && errno == ETIMEDOUT) // patience runs out
    {
        if (char_cmp(&person_in_zone[id], "E") == 0) // E = Entrance
            person_in_zone[id] = 'D';                // D = Zoneless
    }
    else
    {
        if (char_cmp(&person_in_zone[id], "E") == 0 || char_cmp(&person_in_zone[id], "D") == 0)
        {
            person_in_zone[id] = 'H'; // person got a seat in a zone
            printf(GRN "%s got a seat in zone H\n" RESET, cur_person.name);
        }
        else
            sem_post(&h_zone); // seat was unused => increment semaphore
    }
    pthread_mutex_unlock(&person_zone_lock[id]);

    return NULL;
}

void *simulate_person_in_game(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;

    int type_of_fan = -1;
    if (char_cmp(&cur_person.fan_type, "A") == 0) // A fan => follow goals of the Home team
        type_of_fan = 0;
    else if (char_cmp(&cur_person.fan_type, "H") == 0) // H fan => follow goals of the Away team
        type_of_fan = 1;
    if (type_of_fan == -1) // neutral => doesn't get enraged
        return NULL;

    pthread_mutex_lock(&goals_lock[type_of_fan]);
    while (goals[type_of_fan] < cur_person.no_of_goals) // wait till opposing team goals doesn't enrage
    {
        if (goals[type_of_fan] >= 0)
            pthread_cond_wait(&goals_changed[type_of_fan], &goals_lock[type_of_fan]);
        else
            break;
    }
    if (goals[type_of_fan] >= cur_person.no_of_goals)
        printf(RED "%s is leaving due to bad performance of his team\n" RESET, cur_person.name);
    pthread_mutex_unlock(&goals_lock[type_of_fan]);
    pthread_cond_broadcast(&person_moved[cur_person.id]);

    return NULL;
}

void *sear(void *input_person)
{
    person cur_person = *(person *)input_person;
    int id = cur_person.id;

    pthread_t threads[NUM_OF_SEMAPHORES]; // n, a, h
    if (char_cmp(&cur_person.fan_type, "N") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "A") == 0)
    {
        pthread_create(&threads[1], NULL, search_in_A, &team_args[id]);
        pthread_join(threads[1], NULL);
    }
    else if (char_cmp(&cur_person.fan_type, "H") == 0)
    {
        pthread_create(&threads[0], NULL, search_in_N, &team_args[id]);
        pthread_join(threads[0], NULL);
        pthread_create(&threads[2], NULL, search_in_H, &team_args[id]);
        pthread_join(threads[2], NULL);
    }

    return NULL;
}

void *person_motion(void *input_person)
{
    args arguments = *(args *)input_person;
    person cur_person = arguments.arg_person;
    int id = cur_person.id, rt = cur_person.reach_time;

    sleep(rt); // enters at reach_time
    printf(BLU "%s has reached the stadium\n" RESET, cur_person.name);

    // simulate seat allocation
    pthread_t thread;
    pthread_create(&thread, NULL, sear, &arguments);
    pthread_join(thread, NULL);

    // did not find any seat
    int isFound_seat = 0;
    pthread_mutex_lock(&person_zone_lock[id]);
    // D = Zoneless, G = Left
    if (char_cmp(&person_in_zone[id], "D") != 0 && char_cmp(&person_in_zone[id], "G") != 0)
        isFound_seat = 1;
    pthread_mutex_unlock(&person_zone_lock[id]);

    if (isFound_seat)
    {
        struct timespec ts; // spectating time
        clock_gettime(CLOCK_REALTIME, &ts);

        // found a seat and watching the game
        pthread_create(&thread, NULL, simulate_person_in_game, &arguments);

        pthread_mutex_lock(&person_zone_lock[id]);
        ts.tv_sec += spectating_time;
        // signal conditional variable when person wants to leave
        if (pthread_cond_timedwait(&person_moved[id], &person_zone_lock[id], &ts) == ETIMEDOUT)
            printf(RED "%s watched the match for %d seconds and is leaving\n" RESET, cur_person.name, spectating_time);
        pthread_mutex_unlock(&person_zone_lock[id]);

        // person left, increment semaphore
        pthread_mutex_lock(&person_zone_lock[id]);
        if (char_cmp(&person_in_zone[id], "H") == 0)
            sem_post(&h_zone);
        else if (char_cmp(&person_in_zone[id], "A") == 0)
            sem_post(&a_zone);
        else if (char_cmp(&person_in_zone[id], "N") == 0)
            sem_post(&n_zone);
        person_in_zone[id] = 'G';
        pthread_mutex_unlock(&person_zone_lock[id]);

        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }
    else
    {
        printf(BLU "%s did not find a seat in any of the zones\n" RESET, cur_person.name);
        printf(MAG "%s left the stadium\n" RESET, cur_person.name);
    }

    return NULL;
}

void *simulate_team(void *input_team)
{
    team cur_team = *(team *)input_team;

    int i;
    for (i = 0; i < cur_team.no_of_chances; i++)
    {
        // probability of team scoring
        float x = cur_team.goal_prob[i] - ((float)rand() / (float)(RAND_MAX / 1.0));
        int id = cur_team.id;
        sleep(cur_team.prev_chance[i]); // sleep till next chance to score goal

        if (x >= 0) // gloal scored
        {
            pthread_mutex_lock(&goals_lock[id]);
            goals[id]++;
            printf(CYN "Team %c has scored thier goal %d\n" RESET, cur_team.team_type, goals[id]);
            pthread_mutex_unlock(&goals_lock[id]);
        }
        else // gloal missed
            printf(CYN "Team %c missed their chance to score their goal %d\n" RESET, cur_team.team_type, goals[id] + 1);
        pthread_cond_broadcast(&goals_changed[id]); // broadcast after every chance
    }
    return NULL;
}

void allocate_memory(int isDatastruct)
{
    if (isDatastruct == 1) // init people, teams, args
    {
        people = malloc(sizeof(person) * 1);
        team_args = malloc(sizeof(args) * 1);
        teams = malloc(sizeof(team) * 2);
        int i;
        for (i = 0; i < 2; i++)
        {
            teams[i].goal_prob = malloc(sizeof(float) * 1);
            teams[i].prev_chance = malloc(sizeof(int) * 1);
        }
    }
    else if (isDatastruct == 0) // init rest
    {
        person_moved = malloc(sizeof(pthread_cond_t) * num_people);
        person_in_zone = malloc(sizeof(char) * num_people);
        person_zone_lock = malloc(sizeof(pthread_mutex_t) * num_people);
        people_thread = malloc(sizeof(pthread_t) * num_people);
        teams_thread = malloc(sizeof(pthread_t) * 2);
    }
}

void take_input()
{
    int i, ii;
    int num_groups, num_in_group;

    scanf("%d %d %d", &h_capacity, &a_capacity, &n_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    for (i = 1; i < num_groups + 1; i++)
    {
        scanf("%d", &num_in_group);

        for (ii = 0; ii < num_in_group; ii++)
        {
            char name[PERSON_NAME_SIZE], support_teams;
            int reach_time, patience, no_of_goals;
            people = realloc(people, sizeof(person) * (num_people + 1));
            team_args = realloc(team_args, sizeof(args) * (num_people + 1));

            scanf("%s %c %d %d %d", name, &support_teams, &reach_time, &patience, &no_of_goals);
            strcpy(people[num_people].name, name);
            people[num_people].fan_type = support_teams;
            people[num_people].reach_time = reach_time;
            people[num_people].patience = patience;
            people[num_people].no_of_goals = no_of_goals;
            people[num_people].id = num_people;
            people[num_people].group_no = i;
            team_args[num_people].arg_person = people[num_people];

            num_people++;
        }
    }

    int goal_scoring_chances;
    scanf("%d\n", &goal_scoring_chances);

    for (i = 0; i < goal_scoring_chances; i++)
    {
        int prev_time[2] = {0, 0}, inp_time, j = -1;
        char inp_team;
        scanf("%c", &inp_team);
        if (char_cmp(&inp_team, "H") == 0)
            j = 0;
        else if (char_cmp(&inp_team, "A") == 0)
            j = 1;

        if (j == -1)
        {
            printf("Incorrect Team Name Entered\n");
            exit(1);
        }

        teams[j].goal_prob = realloc(teams[j].goal_prob, sizeof(float) * (teams[j].no_of_chances + 1));
        teams[j].prev_chance = realloc(teams[j].prev_chance, sizeof(int) * (teams[j].no_of_chances + 1));

        int chances = teams[j].no_of_chances;
        teams[j].no_of_chances++;

        float goal_prob;
        scanf("%d %f\n", &inp_time, &goal_prob);
        teams[j].goal_prob[chances] = goal_prob;

        // time from the previous chance to goal
        teams[j].prev_chance[chances] = inp_time - prev_time[j];
        prev_time[j] = inp_time;
    }
}

int main(int argc, char *argv[])
{
    allocate_memory(1);
    teams[0].team_type = 'H';
    teams[1].team_type = 'A';
    int i;
    for (i = 0; i < 2; i++)
    {
        teams[i].id = i;
        teams[i].no_of_chances = 0;
    }
    take_input();
    allocate_memory(0);

    pthread_mutex_init(&goals_lock[0], NULL);
    pthread_mutex_init(&goals_lock[1], NULL);
    pthread_cond_init(&goals_changed[0], NULL);
    pthread_cond_init(&goals_changed[1], NULL);
    goals[0] = 0;
    goals[1] = 0;

    // initialize the semaphores corresponding to the number of seats in each zone
    sem_init(&h_zone, 0, h_capacity);
    sem_init(&a_zone, 0, a_capacity);
    sem_init(&n_zone, 0, n_capacity);

    for (i = 0; i < num_people; i++)
    {
        person_in_zone[i] = 'E';
        pthread_mutex_init(&person_zone_lock[i], NULL);
        pthread_cond_init(&person_moved[i], NULL);
    }

    // create threads
    pthread_create(&teams_thread[0], NULL, simulate_team, &teams[0]);
    pthread_create(&teams_thread[1], NULL, simulate_team, &teams[1]);
    for (i = 0; i < num_people; i++)
        pthread_create(&people_thread[i], NULL, person_motion, &team_args[i]);

    // wait for the threads to finish
    pthread_join(teams_thread[0], NULL);
    pthread_join(teams_thread[1], NULL);
    for (i = 0; i < num_people; i++)
        pthread_join(people_thread[i], NULL);

    goals[0] = -1;
    pthread_cond_broadcast(&goals_changed[0]);
    pthread_cond_destroy(&goals_changed[0]);
    pthread_mutex_destroy(&goals_lock[0]);
    pthread_cancel(teams_thread[0]);
    goals[1] = -1;
    pthread_cond_broadcast(&goals_changed[1]);
    pthread_cond_destroy(&goals_changed[1]);
    pthread_mutex_destroy(&goals_lock[1]);
    pthread_cancel(teams_thread[1]);

    sem_destroy(&n_zone);
    sem_destroy(&a_zone);
    sem_destroy(&h_zone);

    return 0;
}
