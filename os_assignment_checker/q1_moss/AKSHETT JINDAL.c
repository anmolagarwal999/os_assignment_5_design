
//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/utils.c ####################//

#include "common.h"
#include "utils.h"

void err_n_die(const char *fmt, ...)
{
    int errno_save;
    va_list ap;

    errno_save = errno;

    fprintf(stderr, COLOR_RED);
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    fflush(stderr);

    if (errno_save != 0)
    {
        fprintf(stderr, "(errno = %d) : %s\n", errno_save, strerror(errno_save));
        fprintf(stderr, "\n");
        fprintf(stderr, COLOR_RESET);
        fflush(stderr);
    }

    va_end(ap);
    exit(1);
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/main.c ####################//

#include "common.h"
#include "utils.h"

#include "classes/Lab.h"
#include "classes/Course.h"
#include "classes/Mentor.h"
#include "classes/Student.h"

Course **course_nodes;
Student **student_nodes;
Lab **lab_nodes;

pthread_t *course_threads;
pthread_t *student_threads;
pthread_t *lab_threads;

/* Course  *course_nodes[MAX_COURSES];
Student *student_nodes[MAX_STUDENTS];
Lab     *lab_nodes[MAX_LABS]; */

/* pthread_t course_threads[MAX_COURSES];
pthread_t student_threads[MAX_STUDENTS];
pthread_t lab_threads[MAX_LABS]; */

void* course_thread_function(void *arg)
{
#if DEBUG >= 2
    llint thread_id = pthread_self();
    printf("%s:%d:%s(): thread_id = %lld\n", __FILE__, __LINE__, __func__, thread_id);
#endif

    Course *course = (Course*)arg;
#if DEBUG >= 2
    printf("%s%lld: Thread for course %s (id:%lld) started%s\n", COLOR_YELLOW, thread_id, course->name, course->id, COLOR_RESET);
#endif

    while (course->is_open)
    {
#if INFO_LEVEL >= 3
        printf(\
                "%s%sCourse %s going to look for another TA for a new tutorial%s\n",\
                COLOR_RED_BOLD,\
                TEXT_UNDERLINE,\
                course->name,\
                COLOR_RESET\
              );
#endif
        sleep(GAP_BETWEEN_TUTORIAL_SESSIONS);

        course->ta = NULL;
        course->ta_lab = NULL;
        bool can_find_ta = false;

        // TODO: Add logic for selecting TA
        for (llint lab_num=0; lab_num<course->num_labs; lab_num++)
        {
            if (course->ta != NULL) break;

            llint lab_id = course->lab_ids[lab_num];
            Lab *lab = lab_nodes[lab_id];

#if INFO_LEVEL >= 2
            printf(\
                    TEXT_UNDERLINE\
                    COLOR_RED_BOLD\
                    "Course %s is looking for TA in %s lab"\
                    COLOR_RESET"\n",\
                    course->name,\
                    lab->name\
                  );
#endif

            pthread_mutex_lock(&lab->lock);
            if (!lab->lab_mentors_available)
            {
                pthread_mutex_unlock(&lab->lock);
                continue;
            }
            pthread_mutex_unlock(&lab->lock);
            for (llint mentor_num=0; mentor_num<lab->num_mentors; mentor_num++)
            {
                Mentor *mentor = lab->mentors[mentor_num];
                if (pthread_mutex_trylock(&mentor->lock) != 0) {
                    /* Some other Course is using this Mentor */
                    continue;
                }
                /* Mentor is locked */
                if (mentor->taships_done != lab->curr_max_taship) {
                    /* Cannot use this mentor */
                    pthread_mutex_unlock(&mentor->lock);
                    continue;
                }

                course->ta = mentor;
                course->ta_lab = lab;
                mentor->taships_done += 1;
                printf(\
                        "%sTA %lld from lab %s has been allocated to course %s for his %lldth TA ship%s\n",\
                        COLOR_BLUE,\
                        course->ta->id,\
                        course->ta_lab->name,\
                        course->name,\
                        course->ta->taships_done,\
                        COLOR_RESET\
                      );

                /* Mentor can be used */
                pthread_mutex_lock(&lab->lock);
                lab->num_mentors_wo_max_taship -= 1;
                if (lab->num_mentors_wo_max_taship == 0)
                {
                    lab->curr_max_taship += 1;
                    lab->num_mentors_wo_max_taship = lab->num_mentors;
                    if (lab->curr_max_taship == lab->taship_limit)
                    {
                        lab->lab_mentors_available = false;
                        printf(\
                                COLOR_BLUE\
                                "Lab %s no longer has students available for TAship"\
                                COLOR_RESET"\n",\
                                lab->name\
                              );
                    }
                }
                pthread_mutex_unlock(&lab->lock);
                break;
            }
        }

        pthread_mutex_lock(&course->lock);

        if (course->ta == NULL)
        {
            if (!can_find_ta)
            {
                course->is_open = false;
                printf(\
                        "%sCourse %s does not have any TA mentors eligible and is removed from course offerings%s\n",\
                        COLOR_CYAN,\
                        course->name,\
                        COLOR_RESET\
                      );
                pthread_cond_broadcast(&course->tut_slots_cond);
                pthread_cond_broadcast(&course->tut_session_cond);
            }
            pthread_mutex_unlock(&course->lock);
            continue;
        }

        llint num_tut_slots = 1 + (rand() % course->tut_slots_limit);
        course->tut_slots = num_tut_slots;

        printf(\
                "Course %s has been allocated %lld seats\n",\
                course->name,\
                course->tut_slots\
              );

        pthread_cond_broadcast(&course->tut_slots_cond);
        pthread_mutex_unlock(&course->lock);

        sleep(TUTORIAL_SEAT_ALLOTMENT_DELAY);

        pthread_mutex_lock(&course->lock);

        printf(\
                "Tutorial has started for course %s with %lld seats filled out of %lld\n",\
                course->name,\
                num_tut_slots - course->tut_slots,\
                num_tut_slots\
              );
        course->tut_slots = 0;

        sleep(TUTORIAL_DURATION);

        pthread_cond_broadcast(&course->tut_session_cond);
        pthread_mutex_unlock(&course->lock);

        printf(\
                COLOR_BLUE\
                "TA %lld from lab %s has completed the tutorial and left the course %s"\
                COLOR_RESET "\n",\
                course->ta->id,\
                course->ta_lab->name,\
                course->name\
              );

        pthread_mutex_unlock(&course->ta->lock);
    }

#if DEBUG >= 2
    printf("%s%lld: Thread for course %s (id:%lld) ending%s\n", COLOR_YELLOW, thread_id, course->name, course->id, COLOR_RESET);
#endif
    return NULL;
}

void* student_thread_function(void *arg)
{
#if DEBUG >= 2
    llint thread_id = pthread_self();
    printf("%s:%d:%s(): thread_id = %lld\n", __FILE__, __LINE__, __func__, thread_id);
#endif

    Student *student = (Student*)arg;
#if DEBUG >= 2
    printf("%s%lld: Thread for student %lld started%s\n", COLOR_YELLOW, thread_id, student->id, COLOR_RESET);
#endif

    // Fill the preferences
    sleep(student->preferences_filling_time);
    printf(\
            "%sStudent %lld has filled in preferences for course registeration%s\n",\
            COLOR_YELLOW,\
            student->id,\
            COLOR_RESET\
          );

    for (llint pref_num=0; pref_num<STUDENT_NUM_PREFERENCES; pref_num++)
    {
        llint course_id = student->preferences[pref_num];
        Course *course = course_nodes[course_id];

        if (pref_num > 0)
        {
            llint prev_course_id = student->preferences[pref_num-1];
            Course *prev_course = course_nodes[prev_course_id];
            printf(\
                    "%sStudent %lld has changed current preference from %s (priority %lld) to %s (priority %lld)%s\n",\
                    COLOR_YELLOW,\
                    student->id,\
                    prev_course->name,\
                    pref_num,\
                    course->name,\
                    pref_num+1,\
                    COLOR_RESET\
                  );
        }

        pthread_mutex_lock(&course->lock);

#if INFO_LEVEL >= 2
        printf(\
                "%s%sStudent %lld is trying to register for course %s%s\n",\
                COLOR_YELLOW,\
                TEXT_UNDERLINE,\
                student->id,\
                course->name,\
                COLOR_RESET\
              );
#endif

        while (course->is_open && course->tut_slots == 0)
        {
            pthread_cond_wait(&course->tut_slots_cond, &course->lock);
        }

        if (!course->is_open)
        {
            pthread_mutex_unlock(&course->lock);
            continue;
        }

        course->tut_slots -= 1;
        printf(\
                "%sStudent %lld has been allocated a seat in course %s%s\n",\
                COLOR_CYAN,\
                student->id,\
                course->name,\
                COLOR_RESET\
              );

        pthread_cond_wait(&course->tut_session_cond, &course->lock);
        pthread_mutex_unlock(&course->lock);

        bool withdraw = student->withdraw_from_course(student, course);
        if (withdraw)
        {
            printf(\
                    "%sStudent %lld has withdrawn from course %s%s\n",\
                    COLOR_MAGENTA,\
                    student->id,\
                    course->name,\
                    COLOR_RESET\
                  );
        }
        else
        {
            printf(\
                    "%s%sStudent %lld has selected course %s permanently%s\n",\
                    COLOR_GREEN_BOLD,\
                    TEXT_UNDERLINE,\
                    student->id,\
                    course->name,\
                    COLOR_RESET\
                  );
            return NULL;
        }

    }

    printf(\
            "%s%sStudent %lld couldn't get any of his preferred courses%s\n",\
            COLOR_RED_BOLD,\
            TEXT_UNDERLINE,\
            student->id,\
            COLOR_RESET\
          );

#if DEBUG >= 2
    printf("%s%lld: Thread for student %lld ending%s\n", COLOR_YELLOW, thread_id, student->id, COLOR_RESET);
#endif
    return NULL;
}

/* void* lab_thread_function(void *arg)
{
#if DEBUG >= 2
    llint thread_id = pthread_self();
    printf("%s:%d:%s(): thread_id = %lld\n", __FILE__, __LINE__, __func__, thread_id);
#endif

    Lab *lab = (Lab*)arg;
#if DEBUG >= 2
    printf("%s%lld: Thread for lab %s (id:%lld) started%s\n", COLOR_YELLOW, thread_id, lab->name, lab->id, COLOR_RESET);
#endif

    // sleep(10);

#if DEBUG >= 2
    printf("%s%lld: Thread for lab %s (id:%lld) ending%s\n", COLOR_YELLOW, thread_id, lab->name, lab->id, COLOR_RESET);
#endif
    return NULL;
} */

int main(int argc, char **argv)
{
    llint num_students = 0;
    llint num_labs = 0;
    llint num_courses = 0;

    scanf(\
            "%lld %lld %lld",\
            &num_students,\
            &num_labs,\
            &num_courses\
         );

    course_nodes = (Course**)malloc(num_courses * sizeof(Course*));
    student_nodes = (Student**)malloc(num_students * sizeof(Student*));
    lab_nodes = (Lab**)malloc(num_labs * sizeof(Lab*));

    course_threads = malloc(num_courses * sizeof(pthread_t));
    student_threads = malloc(num_students * sizeof(pthread_t));
    lab_threads = malloc(num_labs * sizeof(pthread_t));

    if (course_nodes == NULL || student_nodes == NULL || lab_nodes == NULL)
        err_n_die("Failed to store all the entities");

    if (course_threads == NULL || student_threads == NULL || lab_threads == NULL)
        err_n_die("Failed to store all the threads");

    for (llint course_num=0; course_num<num_courses; course_num++)
    {
        course_nodes[course_num] = new_course_from_input(course_num);
#if DEBUG >= 2
        print_course(course_nodes[course_num]);
#endif
    }

    for (llint student_num=0; student_num<num_students; student_num++)
    {
        student_nodes[student_num] = new_student_from_input(student_num);
#if DEBUG >= 2
        print_student(student_nodes[student_num]);
#endif
    }

    for (llint lab_num=0; lab_num<num_labs; lab_num++)
    {
        lab_nodes[lab_num] = new_lab_from_input(lab_num);
#if DEBUG >= 2
        print_lab(lab_nodes[lab_num]);
#endif
    }

    for (llint student_num=0; student_num<num_students; student_num++)
    {
        Student *student = student_nodes[student_num];
        if (pthread_create(&student_threads[student_num], NULL, student_thread_function, (void*)student) != 0)
            err_n_die("Failed to create thread for student %lld", student->id);
    }

    for (llint course_num=0; course_num<num_courses; course_num++)
    {
        Course *course = course_nodes[course_num];
        if (pthread_create(&course_threads[course_num], NULL, course_thread_function, (void*)course) != 0)
            err_n_die("Failed to create thread for course %s (id:%lld)", course->name, course->id);
    }

    /* for (llint lab_num=0; lab_num<num_labs; lab_num++)
    {
        Lab *lab = lab_nodes[lab_num];
        if (pthread_create(&lab_threads[lab_num], NULL, lab_thread_function, (void*)lab) != 0)
            err_n_die("Failed to create thread for lab %s (id:%lld)", lab->name, lab->id);
    } */

#if DEBUG >= 2
    printf("%sAll threads started%s\n", COLOR_YELLOW, COLOR_RESET);
#endif

    for (llint student_num=0; student_num<num_students; student_num++)
    {
#if INFO_LEVEL >= 1
        printf("Waiting for thread of student %lld to join back...\n", student_num);
#endif
        if (pthread_join(student_threads[student_num], NULL) != 0)
            err_n_die("Failed to join thread for student %lld", student_nodes[student_num]->id);
    }

    /* for (llint course_num=0; course_num<num_courses; course_num++)
    {
        if (pthread_join(course_threads[course_num], NULL) != 0)
            err_n_die("Failed to join thread for course %s (id:%lld)", course_nodes[course_num]->name, course_nodes[course_num]->id);
    }

    for (llint lab_num=0; lab_num<num_labs; lab_num++)
    {
        if (pthread_join(lab_threads[lab_num], NULL) != 0)
            err_n_die("Failed to join thread for lab %s (id:%lld)", lab_nodes[lab_num]->name, lab_nodes[lab_num]->id);
    } */

    return 0;
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/common.h ####################//

#ifndef __Q1_COMMON_H
#define __Q1_COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#define MAX_COURSES 1000
#define MAX_STUDENTS 1000
#define MAX_LABS 1000

#define COURSE_NAME_LEN 49
#define LAB_NAME_LEN 49

#define STUDENT_NUM_PREFERENCES 3

#define TUTORIAL_DURATION 5
#define TUTORIAL_SEAT_ALLOTMENT_DELAY 2
#define GAP_BETWEEN_TUTORIAL_SESSIONS 2

#define DEBUG 0
#define INFO_LEVEL 0

// Ascii codes for 256-bit colors
#define COLOR_BLACK   "\033[0;30m"
#define COLOR_RED     "\033[0;31m"
#define COLOR_GREEN   "\033[0;32m"
#define COLOR_YELLOW  "\033[0;33m"
#define COLOR_BLUE    "\033[0;34m"
#define COLOR_MAGENTA "\033[0;35m"
#define COLOR_CYAN    "\033[0;36m"
#define COLOR_WHITE   "\033[0;37m"
#define COLOR_RESET   "\033[0m"

#define COLOR_GREEN_BOLD   "\033[1;32m"
#define COLOR_RED_BOLD     "\033[1;31m"
#define TEXT_UNDERLINE     "\033[4m"
#define TEXT_BOLD          "\033[1m"

typedef long long int llint;

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/utils.h ####################//

#ifndef __Q1_UTILS_H
#define __Q1_UTILS_H

void err_n_die(const char *fmt, ...);

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Course.c ####################//

#include "Course.h"
#include "../utils.h"

Course* new_course_from_input(llint id)
{
    Course* course = malloc(sizeof(Course));

    if (course == NULL)
        err_n_die("Failed to create new course");

    course->id = id;

    scanf("%s", course->name);
    scanf("%lf", &course->interest_quotient);
    scanf("%lld", &course->tut_slots_limit);
    scanf("%lld", &course->num_labs);

    course->lab_ids = malloc(course->num_labs * sizeof(llint));
    for (llint lab_num=0; lab_num<course->num_labs; lab_num++)
        scanf("%lld", &course->lab_ids[lab_num]);

    pthread_mutex_init(&course->lock, NULL);
    pthread_cond_init(&course->tut_slots_cond, NULL);
    pthread_cond_init(&course->tut_session_cond, NULL);
    course->tut_slots = 0;
    course->ta = NULL;
    course->ta_lab = NULL;

    if (course->num_labs == 0 || course->tut_slots_limit < 1) {
        course->is_open = false;
        printf(\
                "%sCourse %s does not have any TA mentors eligible and is removed from course offerings%s\n",\
                COLOR_CYAN,\
                course->name,\
                COLOR_RESET\
              );
    }
    else
        course->is_open = true;

    return course;
};

void print_course(Course* course)
{
    printf("\n");

    printf(\
            "%sCoure ID: %s%lld%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            course->id,\
            COLOR_RESET\
          );

    printf(\
            "%sCoure Name: %s%s%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            course->name,\
            COLOR_RESET\
          );

    printf(\
            "%sCoure Interest: %s%lf%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            course->interest_quotient,\
            COLOR_RESET\
          );

    printf(\
            "%sCoure Labs: %s",\
            COLOR_BLUE,\
            COLOR_GREEN\
          );
    for (llint lab_num=0; lab_num<course->num_labs; lab_num++)
        printf("%lld ", course->lab_ids[lab_num]);
    printf("%s\n", COLOR_RESET);

    printf("\n");
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Lab.c ####################//

#include "Lab.h"
#include "Mentor.h"
#include "../utils.h"

Lab* new_lab_from_input(llint id)
{
    Lab *lab = malloc(sizeof(Lab));

    if (lab == NULL)
        err_n_die("Failed to create a new lab");

    lab->id = id;

    scanf("%s", lab->name);
    scanf("%lld", &lab->num_mentors);
    scanf("%lld", &lab->taship_limit);

/* #if DEBUG >= 1
    printf("Making a new queue for lab %s\n", lab->name);
#endif */

    lab->mentors = malloc(lab->num_mentors * sizeof(Mentor*));
    for (llint mentor_num=0; mentor_num<lab->num_mentors; mentor_num++)
    {
        lab->mentors[mentor_num] = new_mentor(mentor_num, id, lab->taship_limit);
    }
    pthread_mutex_init(&lab->lock, NULL);
    lab->curr_max_taship = 0;
    lab->num_mentors_wo_max_taship = lab->num_mentors;

    if (lab->num_mentors == 0 || lab->taship_limit < 1) {
        lab->lab_mentors_available = false;
        printf(\
                COLOR_BLUE\
                "Lab %s no longer has students available for TAship"\
                COLOR_RESET"\n",\
                lab->name\
              );
    }
    else
        lab->lab_mentors_available = true;

    return lab;
}

void print_lab(Lab *lab)
{
    printf("\n");

    printf(\
            "%sLab ID: %s%lld%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            lab->id,\
            COLOR_RESET\
          );

    printf(\
            "%sLab Name: %s%s%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            lab->name,\
            COLOR_RESET\
          );

    printf(\
            "%sLab Mentors: %s",\
            COLOR_BLUE,\
            COLOR_GREEN\
          );
    printf("%s\n", COLOR_RESET);

    printf("\n");
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Student.c ####################//

#include "Student.h"
#include "../utils.h"

bool widthraw_from_course(Student *student, Course *course)
{
    // Pobability of accepting the course
    double prob = student->calibre_quotient * course->interest_quotient;
    double random_prob = (double)rand() / (double)RAND_MAX;

    return (random_prob >= prob ? true : false);
}

Student* new_student_from_input(llint id)
{
    Student *student = malloc(sizeof(Student));

    if (student == NULL)
        err_n_die("Failed to create a new student");

    student->id = id;

    scanf("%lf", &student->calibre_quotient);
    for (int pref_num=0; pref_num<STUDENT_NUM_PREFERENCES; pref_num++)
        scanf("%lld", &student->preferences[pref_num]);
    scanf("%lld", &student->preferences_filling_time);

    student->withdraw_from_course = widthraw_from_course;

    return student;
}

void print_student(Student *student)
{
    printf("\n");

    printf(\
            "%sStudent ID: %s%lld%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            student->id,\
            COLOR_RESET\
        );

    printf(\
            "%sStudent Calibre: %s%lf%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            student->calibre_quotient,\
            COLOR_RESET\
        );

    printf(\
            "%sStudent Preferences: %s",\
            COLOR_BLUE,\
            COLOR_GREEN\
        );
    for (int pref_num=0; pref_num<STUDENT_NUM_PREFERENCES; pref_num++)
        printf("%lld ", student->preferences[pref_num]);
    printf("%s\n", COLOR_RESET);

    printf(\
            "%sStudent Preferences Filling Time: %s%lld%s\n",\
            COLOR_BLUE,\
            COLOR_GREEN,\
            student->preferences_filling_time,\
            COLOR_RESET\
        );

    printf("\n");
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Lab.h ####################//

#ifndef __Q1_LAB_H
#define __Q1_LAB_H

#include "../common.h"
#include "Mentor.h"

typedef struct Lab
{
    llint id;

    char name[LAB_NAME_LEN];
    llint num_mentors;
    llint taship_limit;

    Mentor **mentors;
    pthread_mutex_t lock;

    llint curr_max_taship;
    llint num_mentors_wo_max_taship;

    bool lab_mentors_available;
}
Lab;

Lab* new_lab_from_input(llint id);
void print_lab(Lab *lab);

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Course.h ####################//

#ifndef __Q1_COURSE_H
#define __Q1_COURSE_H

#include "../common.h"
#include "Mentor.h"
#include "Lab.h"

typedef struct Course
{
    llint id;

    /* Input Start */
    char name[COURSE_NAME_LEN+1];
    double interest_quotient;
    llint tut_slots_limit;
    llint num_labs;
    llint *lab_ids;
    /* Input End */

    pthread_mutex_t lock;               // Initialized in constructor
    pthread_cond_t tut_slots_cond;      // Initialized in constructor
    pthread_cond_t tut_session_cond;    // Initialized in constructor
    llint tut_slots;                    // Initialized in constructor
    Mentor *ta;                         // Initialized in constructor
    Lab *ta_lab;                        // Initialized in constructor
    bool is_open;                       // Initialized in constructor

} Course;

Course* new_course_from_input(llint id);
void print_course(Course *course);

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Mentor.h ####################//

#ifndef __Q1_MENTOR_H
#define __Q1_MENTOR_H

#include "../common.h"

typedef struct Mentor
{
    llint id;
    llint lab_id;
    llint taships_done;
    pthread_mutex_t lock;
}
Mentor;

Mentor* new_mentor(llint mentor_id, llint lab_id, llint taships);

#endif

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Mentor.c ####################//

#include "Mentor.h"
#include "../utils.h"

Mentor* new_mentor(llint mentor_id, llint lab_id, llint taships)
{
    Mentor *mentor = malloc(sizeof(Mentor));
    if (mentor == NULL)
        err_n_die("Failed to create a new Mentor");

    mentor->id = mentor_id;
    mentor->lab_id = lab_id;
    mentor->taships_done = 0;
    pthread_mutex_init(&mentor->lock, NULL);

    return mentor;
}

//###########FILE CHANGE ./main_folder/AKSHETT JINDAL_305920_assignsubmission_file_/2019114001_assignment_5/q1/src/classes/Student.h ####################//

#ifndef __Q1_STUDENT_H
#define __Q1_STUDENT_H

#include "../common.h"
#include "Course.h"

typedef struct Student
{
    llint id;

    double calibre_quotient;                                    /* Done */
    llint preferences[STUDENT_NUM_PREFERENCES];                 /* Done */
    llint preferences_filling_time;                             /* Done */

    bool (*withdraw_from_course)(struct Student*, Course*);     /* Done */

} Student;

Student* new_student_from_input(llint id);
void print_student(Student* student);

#endif
