
//###########FILE CHANGE ./main_folder/Lavisha Bhambri_305790_assignsubmission_file_/2020101088_assignment_5/q1/q1.c ####################//

#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define maxNumberAllowed 500        // Maximum number taken for the courses, labs & students array
#define SLEEP_TIME_WHEN_STUDENT_IS_FREE 3
#define SLEEP_TIME_WHEN_COURSE_IS_ALLOCATED 2
#define SLEEP_TIME_WHEN_TUTORIALS_ARE_STARTED 5

// Colours
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_BOLD_GREEN  "\e[1;32m"
#define ANSI_COLOR_BOLD_BLACK  "\e[1;30m"
#define ANSI_COLOR_BOLD_RED  "\e[1;31m"


struct course
{
    pthread_t thread_obj;           // Stores thread
    pthread_mutex_t mutex;          // Mutex variable
    pthread_cond_t can_allocate;    // conditional variable
    char name[maxNumberAllowed];    // Stores name of course
    int num_decided_slots;          // if slots are decided or not, if yes then > 1
    int num_filled_slots;           // if all decided slots are filled
    int id;                         // Course ID
    int thr_id;                     // Thread ID
    float interest_quotient;        // Tells the interest of the students for that particular course
    int max_allocated_slots_per_TA; // Max slots per TA
    int num_labs_accepting_TA;      // Number of labs that accept TA
    int labs_ID[maxNumberAllowed];  // Stores lab ID
    int withdrawn_status;           // Stores if the course is withdrawn or not, if 1 -> withdrawn, else not withdrawn
};

struct lab
{
    pthread_mutex_t mutex;                  // Mutex variable
    int id;                                 // Lab ID
    char name[maxNumberAllowed];            // Name of lab
    int num_TA_in_lab;                      // No. of TA in each lab
    int num_times_student_becomes_TA_in_lab;// Max times a student can become TA
    int record_of_TA[maxNumberAllowed];       // Record of TAship of student
    int status_of_TA[maxNumberAllowed];       // Status of TA (free or not)
};

struct student
{
    pthread_t thread_obj;                   // Stores thread
    int id;                                 // Student ID
    int thr_id;                             // Thread ID
    float student_calibre;                  // Stores Calibre of student
    int student_preferences[3];             // Preferences of student
    int time_at_which_student_fills_course; // Filling time of student
    int free_status;                        // tells if the student is free or not
    pthread_mutex_t mutex;                  // Mutex variable
};


// Stores the number of students, labs & courses
int num_students;
int num_labs;
int num_courses;
struct course *course_ptr[maxNumberAllowed];
struct student *student_ptr[maxNumberAllowed];
struct lab *lab_ptr[maxNumberAllowed];

int returnID(void *ptr) {
    return *((int *)ptr);
}

// Returns any random number between min to max.
int random_number(int min, int max) {
    // printf("FLOATING ************* %d %d %d",(max - min + 1), min, max);
    return (rand() % (max - min + 1)) + min;
}

// Checks if student is selected or not based on course interest & calibre of the student
bool isStudentSelected(int interest, int calibre) {
    int randomNum = random_number(1, 100);
    int prob = (interest * calibre) * 100;
    return prob > randomNum;
}


void *initialize_courses(void *ptr) {
    bool TA_found = false;
    int labSelected;
    int TASelected;
    int id = returnID(ptr); // storing ID   

    // Courses are searching for TAs in parallel
    while (true) {
        TA_found = false;
        for (int i = 0; i < course_ptr[id] -> num_labs_accepting_TA; i++) {
            // course_ptr[id]->labs_ID[i]; - This stores the labID

            for (int j = 0; j < lab_ptr[course_ptr[id]->labs_ID[i]]->num_TA_in_lab; j++) {
                    
                pthread_mutex_lock(&lab_ptr[course_ptr[id]->labs_ID[i]]->mutex); // locking the lab

                // Check if TA has been found or not
                if (lab_ptr[course_ptr[id]->labs_ID[i]]->record_of_TA[j] < lab_ptr[course_ptr[id]->labs_ID[i]]->num_times_student_becomes_TA_in_lab && lab_ptr[course_ptr[id]->labs_ID[i]]->status_of_TA[j] == 0) {
                    TA_found = true;
    
                    lab_ptr[course_ptr[id]->labs_ID[i]]->record_of_TA[j] = lab_ptr[course_ptr[id]->labs_ID[i]]->record_of_TA[j] + 1;
                    lab_ptr[course_ptr[id]->labs_ID[i]]->status_of_TA[j] = 1;
                    printf(ANSI_COLOR_BLUE  "TA %d from lab %s has been allocated to course %s for his %d TA ship\n" ANSI_COLOR_RESET, j, lab_ptr[course_ptr[id]->labs_ID[i]]->name, course_ptr[id]->name, lab_ptr[course_ptr[id]->labs_ID[i]]->record_of_TA[j]);
                    TASelected = j;
                    labSelected = course_ptr[id]->labs_ID[i];
                }
                pthread_mutex_unlock(&lab_ptr[course_ptr[id]->labs_ID[i]]->mutex); // unlocking the lab

                if (TA_found ==  true) break;
            }
            if (TA_found ==  true) break;
        }
    
        // Withdraw the course 
        if (!TA_found) {
            // *********** Lock the mutex again ******************
            pthread_mutex_lock(&course_ptr[id]->mutex);
            course_ptr[id] -> withdrawn_status = 1;
            printf(ANSI_COLOR_MAGENTA "Course %s does not have any TA mentors eligible and is removed from course offerings\n" ANSI_COLOR_RESET, course_ptr[id]->name);
            
            // Unlock the course
            pthread_mutex_unlock(&course_ptr[id] -> mutex);

            // Send signal as it can allocate
            pthread_cond_signal(&course_ptr[id] -> can_allocate); 
            break;

        }
        // If TA is found, choose random D, then cond signal & wait for students to come
        else {
            // *********** Lock the mutex again ******************
            pthread_mutex_lock(&course_ptr[id]->mutex);

            // Choosing the random value of D
            course_ptr[id]->num_decided_slots = random_number(1, course_ptr[id]->max_allocated_slots_per_TA);

            printf(ANSI_COLOR_CYAN "Course %s has been allocated %d seats\n" ANSI_COLOR_RESET, course_ptr[id]->name, course_ptr[id]->num_decided_slots);
            
            // Unlock the course
            pthread_mutex_unlock(&course_ptr[id]->mutex);
            
            // Send signal as it can allocate now
            pthread_cond_signal(&course_ptr[id]->can_allocate); 
            sleep(SLEEP_TIME_WHEN_COURSE_IS_ALLOCATED);

            // *********** Lock the mutex again ******************
            pthread_mutex_lock(&course_ptr[id]->mutex);
            course_ptr[id]->num_decided_slots = 0;
            printf(ANSI_COLOR_BOLD_GREEN "Course %s has started tut where slots filled are out of %d\n" ANSI_COLOR_RESET, course_ptr[id]->name, course_ptr[id]->max_allocated_slots_per_TA);
            course_ptr[id]->num_filled_slots = 0;

            // Unlock the course
            pthread_mutex_unlock(&course_ptr[id]->mutex);
            sleep(SLEEP_TIME_WHEN_TUTORIALS_ARE_STARTED); // NOw tutorials are started


            // *********** Lock the mutex again ******************
            pthread_mutex_lock(&lab_ptr[labSelected]->mutex);
            lab_ptr[labSelected]->status_of_TA[TASelected] = 0;
            printf(ANSI_COLOR_BOLD_BLACK "TA finished the TA ship\n" ANSI_COLOR_RESET);

            // Unlock the lab
            pthread_mutex_unlock(&lab_ptr[labSelected]->mutex);
     
        }
    }
    return NULL;
}


void *initialize_students(void *ptr) {

    int id = returnID(ptr); // storing ID
    sleep(student_ptr[id]->time_at_which_student_fills_course);  // sleep till its arrival time
    
    // Students fills the preferences
    printf(ANSI_COLOR_RED "Student %d has filled in preferences for course registration\n" ANSI_COLOR_RESET, id);

    while (student_ptr[id]->free_status != 0) { // While the student is not free

        if (student_ptr[id]->free_status == 1) { // now student becomes free for offering TAship

            // Check for Preference - 1
            int filled_course_id = student_ptr[id]->student_preferences[0];
            
            pthread_mutex_lock(&course_ptr[filled_course_id]->mutex); // lock that filled course

            // if the course is withdrawn && slots are not left, only then wait using conditional variable
            while (course_ptr[filled_course_id]->withdrawn_status == 0 && course_ptr[filled_course_id]->num_decided_slots <= 0) 
                pthread_cond_wait(&course_ptr[filled_course_id]->can_allocate, &course_ptr[filled_course_id]->mutex); // waiting on conditional variable
            
            // if course is not withdrawn, && its probability > random_number,then print & give signal
            if (course_ptr[filled_course_id]->withdrawn_status != 1) {
                if (isStudentSelected(course_ptr[filled_course_id]->interest_quotient, student_ptr[id]->student_calibre)) {
                    course_ptr[filled_course_id]->num_filled_slots += 1;
                    course_ptr[filled_course_id]->num_decided_slots -= 1;
                    printf(ANSI_COLOR_GREEN "Student %d has been allocated a seat in course %s\n" ANSI_COLOR_RESET, id, course_ptr[filled_course_id]->name);
                } 
                else {
                    printf(ANSI_COLOR_YELLOW "For student %d is not selected for the course %d.\n" ANSI_COLOR_RESET, id, course_ptr[filled_course_id]->id);
                    pthread_cond_signal(&course_ptr[filled_course_id] -> can_allocate);
                }
            }
            else {
                printf(ANSI_COLOR_YELLOW "For student %d, the course %d is no present (withdrawn)\n" ANSI_COLOR_RESET, id, course_ptr[filled_course_id]->id);
                pthread_cond_signal(&course_ptr[filled_course_id] -> can_allocate);
            }

            pthread_mutex_unlock(&course_ptr[filled_course_id] -> mutex); // unlock that filled course
            student_ptr[id] -> free_status = 0; 
            sleep(SLEEP_TIME_WHEN_STUDENT_IS_FREE);   
        }
    }
    return NULL;
}

// Function to check if any of the three - students, courses, labs == 0
void take_input() {
    if (num_students == 0)
    {
        printf(ANSI_COLOR_BOLD_RED "No student is present to fill preferences\n" ANSI_COLOR_RESET);
    }

    if (num_labs == 0)
    {
        printf(ANSI_COLOR_BOLD_RED "No lab is present for TAs to teach courses\n" ANSI_COLOR_RESET);
    }

    if (num_courses == 0)
    {
        printf(ANSI_COLOR_BOLD_RED "No course is present for TAs to teach\n" ANSI_COLOR_RESET);
    }
}


int main()
{   
    srand(time(0));
    int i = 0;
    scanf("%d", &num_students);
    scanf("%d", &num_labs);
    scanf("%d", &num_courses);
    printf("\n");
    take_input();   // Checking if any of the above three == 0

    // *************************** Taking required Input *********************************
    for (i = 0; i < num_courses; i++) {
        course_ptr[i] = (struct course *)malloc(sizeof(struct course));
        course_ptr[i]->id = i;
        scanf("%s",&course_ptr[i]->name);
        scanf("%f", &course_ptr[i]->interest_quotient);
        scanf("%d", &course_ptr[i]->max_allocated_slots_per_TA);
        scanf("%d",&course_ptr[i]->num_labs_accepting_TA);

        for (int j = 0; j < course_ptr[i]->num_labs_accepting_TA; j++)
            scanf("%d", &course_ptr[i]->labs_ID[j]);
    }   


    for (i = 0; i < num_students; i++) {
        student_ptr[i] = (struct student *)malloc(sizeof(struct student));
        student_ptr[i]->id = i;
        scanf("%f",&student_ptr[i]->student_calibre);

        for (int j = 0; j < 3; j++)
            scanf("%d", &student_ptr[i]->student_preferences[j]);
        scanf("%d", &student_ptr[i]->time_at_which_student_fills_course);
    }

    for (i = 0; i < num_labs; i++) {
        lab_ptr[i] = (struct lab *)malloc(sizeof(struct lab));
        lab_ptr[i]->id = i;
        scanf("%s", &lab_ptr[i]->name);
        scanf("%d", &lab_ptr[i]->num_TA_in_lab);
        scanf("%d", &lab_ptr[i]->num_times_student_becomes_TA_in_lab);
    }

    // ******************************* Initialising threads *****************************
    for (i = 0; i < num_courses; i++) {
        pthread_mutex_init(&(course_ptr[i]->mutex), NULL);
        pthread_cond_init(&(course_ptr[i]->can_allocate), NULL);
        course_ptr[i]->thr_id = pthread_create(&(course_ptr[i]->thread_obj), NULL, initialize_courses, (void *)(&(course_ptr[i]->id)));
    }

    for (i = 0; i < num_students; i++) {
        pthread_mutex_init(&(student_ptr[i]->mutex), NULL);
        student_ptr[i]->thr_id = pthread_create(&(student_ptr[i]->thread_obj), NULL, initialize_students, (void *)(&(student_ptr[i]->id)));
    }

    // ********************************** Join the threads ********************************
    for (int i = 0; i < num_students; i++) {
        pthread_join(student_ptr[i]->thread_obj, NULL);
    }

    for (int i = 0; i < num_courses; i++) {
        pthread_join(course_ptr[i]->thread_obj, NULL);
    }

    for (i = 0; i < num_labs; i++) {
        pthread_mutex_init(&(lab_ptr[i]->mutex), NULL);
    }

    return 0;
}