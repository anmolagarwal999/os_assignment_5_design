
//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/globals.h ####################//

#ifndef PROCESSSYNCHRONIZATION_GLOBALS_H
#define PROCESSSYNCHRONIZATION_GLOBALS_H
#include "entities.h"
#include <pthread.h>
extern int seconds;
extern pthread_mutex_t seconds_lock; // Use lock for updating time
extern Lab **iiit_labs;
extern Student **students;
extern Course **courses;
extern int student_count, lab_count, course_count;
extern pthread_mutex_t course_lock; // Use lock for updating time
extern int courses_left;
extern pthread_mutex_t student_lock; // Use lock for updating time
extern int students_left;
extern pthread_mutex_t print_lock;
#endif // PROCESSSYNCHRONIZATION_GLOBALS_H

//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/main.c ####################//

#include "entities.h"
#include "functions.h"
#include <pthread.h>
#include <stdio.h>
#include <time.h>

int main() {
	srand(time(0));
	pthread_mutex_init(&seconds_lock, NULL);
	// Input
	scanf("%d %d %d", &student_count, &lab_count, &course_count);

	courses_left = course_count;
	students_left = student_count;

	// Course Input
	courses = (Course **)malloc(course_count * sizeof(Course *));
	for (int i = 0; i < course_count; i++) {
		char name[20];
		double interest;
		int course_max_slot;
		int allowed_labs;
		scanf("%s %lf %d %d", name, &interest, &course_max_slot, &allowed_labs);
		int lab_ids[allowed_labs];
		for (int j = 0; j < allowed_labs; j++)
			scanf("%d", &lab_ids[j]);

		courses[i] = createCourse(name, interest, course_max_slot, lab_ids,
								  allowed_labs, i);
	}

	// Student input
	students = (Student **)malloc(student_count * sizeof(Student *));
	for (int i = 0; i < student_count; i++) {
		double caliber;
		int preferences[3];
		int submission_time;
		scanf("%lf", &caliber);
		for (int j = 0; j < 3; j++)
			scanf("%d", &preferences[j]);
		scanf("%d", &submission_time);
		students[i] = createStudent(caliber, submission_time, preferences, i);
	}
	// Lab input
	iiit_labs = (Lab **)malloc(lab_count * sizeof(Lab *));
	for (int i = 0; i < lab_count; i++) {
		char name[20];
		int ta_count;
		int tutorial_limit;
		scanf("%s %d %d", name, &ta_count, &tutorial_limit);
		iiit_labs[i] = createLab(name, ta_count, tutorial_limit, i);
	}
	pthread_t cleanup;
	pthread_create(&cleanup, NULL, cleanupThread, NULL);
	// Student Threads
	pthread_t student_threads[student_count];
	for (int i = 0; i < student_count; i++) {
		pthread_t currThread;
		pthread_create(&currThread, NULL, studentThread, (void *)students[i]);
		student_threads[i] = currThread;
	}
	// Lab threads
	pthread_t lab_threads[lab_count];
	for (int i = 0; i < lab_count; i++) {
		pthread_t currThread;
		pthread_create(&currThread, NULL, labThread, (void *)iiit_labs[i]);
		lab_threads[i] = currThread;
	}
	// Course threads
	pthread_t course_threads[course_count];
	for (int i = 0; i < course_count; i++) {
		pthread_t currThread;
		pthread_create(&currThread, NULL, courseThread, (void *)courses[i]);
		course_threads[i] = currThread;
	}

	time_t start_time = time(NULL);
	while (students_left) {
		time_t curr_time = time(NULL);
		if (curr_time - start_time > seconds) {
			pthread_mutex_lock(&seconds_lock);
			seconds++;
			pthread_mutex_unlock(&seconds_lock);
		}
	}
	// Making sure all threads terminate normally
	for (int i = 0; i < student_count; i++)
		pthread_join(student_threads[i], NULL);
	for (int i = 0; i < course_count; i++)
		pthread_join(course_threads[i], NULL);
	for (int i = 0; i < lab_count; i++)
		pthread_join(lab_threads[i], NULL);
	pthread_join(cleanup, NULL);
}
//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/functions.c ####################//

#include "functions.h"

char *suffix(int x) {
	x %= 10;
	if (x == 1)
		return "st";
	if (x == 2)
		return "nd";
	if (x == 3)
		return "rd";
	return "th";
}
// Returns whether student has submitted their preferences or not
int checkStudentRegistration(Student *s) {
	// If already submitted, then there's no reason to check time
	if (s->submitted)
		return 1;

	// Using lock to be safe, helps in debugging as well to output correctly
	pthread_mutex_lock(&seconds_lock);

	if (s->submission_time <= seconds)
		s->submitted = 1;

	pthread_mutex_unlock(&seconds_lock);
	return s->submitted;
}
// Determines whether a course is picked permanently by a student
int accepted(double caliber, double interest) {
	int caliber_int = (int)(caliber * 1000);
	int interest_int = (int)(interest * 1000);
	int prob = caliber_int * interest_int;
	int randomNumber = rand() % 1000000 + 1;
	return randomNumber <= prob;
}
// Simulates students via a thread
void *studentThread(void *arg) {
	Student *s = (Student *)arg;
	// Use a form of busy waiting to wait till you are registered to proceed
	while (!checkStudentRegistration(s))
		;
	pthread_mutex_lock(&print_lock);
	printf(GRN"Student %d has filled in preferences for course registration\n"WHT,
		   s->id);
	pthread_mutex_unlock(&print_lock);
	while (s->current_preference != 3) {
		pthread_mutex_lock(&s->lock);
		pthread_cond_wait(&s->condition_lock, &s->lock);
		// Upon receiving a signal, check whether the current preference course
		// was assigned
		if (s->assigned_course) {
			s->assigned_course = 0;
			int course_id = s->preferences[s->current_preference];
			Course *c = courses[course_id];
			// If the student finalizes this course
			if (accepted(s->caliber, c->interest)) {
				s->selected_course = course_id;
				s->current_preference = 3;
				pthread_mutex_lock(&print_lock);
				printf(MAG"Student %d has selected course %s permanently\n"WHT, s->id,
					   c->name);
				pthread_mutex_unlock(&print_lock);
				pthread_mutex_unlock(&s->lock);
				break;
			} else { // If the student withdraws;
				pthread_mutex_lock(&print_lock);
				printf(YEL"Student %d has withdrawn from course %s\n"WHT, s->id,
					   c->name);
				pthread_mutex_unlock(&print_lock);
			}
			// Will only be executed if we don't select course permanently
			// Since the student was assigned this preference, we must try for
			// the next preference
		}
		int prev_course = s->preferences[s->current_preference];
		s->current_preference++;
		if (s->current_preference == 3) {
			pthread_mutex_unlock(&s->lock);
			break;
		}
		int next_course = s->preferences[s->current_preference];

		pthread_mutex_lock(&print_lock);
		printf(YEL"Student %d has changed current preference from %s "
			   "(priority %d) to %s (priority %d)\n"WHT,
			   s->id, courses[prev_course]->name, s->current_preference,
			   courses[next_course]->name, s->current_preference + 1);
		pthread_mutex_unlock(&print_lock);
		pthread_mutex_unlock(&s->lock);
	}

	pthread_mutex_lock(&student_lock);
	students_left--;
	pthread_mutex_unlock(&student_lock);

	// Student ended up not getting
	if (s->selected_course == -1) {
		pthread_mutex_lock(&print_lock);
		printf(RED"Student %d could not get any of his preferred courses\n"WHT,
			   s->id);
		pthread_mutex_unlock(&print_lock);
	}
	return NULL;
}

void *labThread(void *arg) {
	Lab *lab = (Lab *)arg;

	pthread_mutex_lock(&lab->lock);

	pthread_cond_wait(&lab->condition_lock, &lab->lock);
	pthread_mutex_lock(&print_lock);
	printf(RED"Lab %s no longer has students available for TA ship\n"WHT, lab->name);
	pthread_mutex_unlock(&print_lock);

	pthread_mutex_unlock(&lab->lock);

	return NULL;
}
int randomSeatAllocate(int course_max_slots) {
	return rand() % course_max_slots + 1;
}
// Pushes students to go for their next preference in case they are waiting for
// a course that is not being offered anymore
void *cleanupThread(void *arg) {
	// Keep trying to clean up as long as there are students left for contention
	while (students_left) {
		for (int i = 0; i < student_count; i++) {
			Student *s = students[i];
			pthread_mutex_lock(&s->lock);
			// Avoiding students that are already completed
			if (s->current_preference != 3) {
				int course_id = s->preferences[s->current_preference];
				pthread_mutex_lock(&courses[course_id]->lock);
				// Check if the preference the student is waiting for is
				// available or not, if not, then wake up the student.
				if (!courses[course_id]->available) {
					pthread_cond_signal(&s->condition_lock);
				}
				pthread_mutex_unlock(&courses[course_id]->lock);
			}
			pthread_mutex_unlock(&s->lock);
		}
	}
	return NULL;
}
int studentAllocateSeats(Course *c, int max_seats) {
	// Keeps count of number of seats allocated
	int allocated = 0;
	for (int i = 0; i < student_count; i++) {
		Student *s = students[i];
		pthread_mutex_lock(&s->lock);
		if (!s->assigned_course && s->current_preference != 3) {
			int course_pref = s->preferences[s->current_preference];
			// If the student has submitted and this is the course it currently
			// prefers, then assign is
			if (c->id == course_pref && s->submitted) {
				s->assigned_course = 1;
				pthread_mutex_lock(&print_lock);
				printf(CYN"Student %d has been allocated a seat in course %s\n"WHT,
					   s->id, c->name);
				pthread_mutex_unlock(&print_lock);
				allocated++;
				if (allocated == max_seats) {
					pthread_mutex_unlock(&s->lock);
					break;
				}
			}
		}
		pthread_mutex_unlock(&s->lock);
	}
	return allocated;
}
void *courseThread(void *arg) {
	// Let some students register, else all courses will always start with 0
	// attendance tutorials.
	sleep(1);

	Course *course = (Course *)arg;
	int labs_left = 1;

	while (labs_left) {
		// Flag to indicate whether there are any labs with eligible TAs left
		labs_left = 0;
		// Stores the index of the lab of the TA taken, and the index of the TA
		// in the lab
		int labIndex = -1, taIndex;

		for (int i = 0; i < course->lab_count; i++) {
			int lab_id = course->lab_ids[i];
			int tas = iiit_labs[lab_id]->ta_count;

			for (int j = 0; j < tas; j++) {

				TA *curr_ta = iiit_labs[lab_id]->tas[j];
				pthread_mutex_lock(&curr_ta->lock);

				// If there is a TA that hasn't made use of all slots, say that
				// there are TAs left
				if (curr_ta->tutorialstaken <
					iiit_labs[lab_id]->tutorial_limit) {
					labs_left = 1;
				}

				// If a TA is available to take a tutorial, then take it
				if (curr_ta->available) {
					curr_ta->available = 0;
					labIndex = lab_id;
					taIndex = j;
					curr_ta->tutorialstaken++;

					// Indicate TA has been allocated
					pthread_mutex_lock(&print_lock);
					printf(CYN"TA %d from lab %s has been allocated to course %s "
						   "for his %d%s TA ship\n"WHT,
						   j, iiit_labs[lab_id]->name, course->name,
						   curr_ta->tutorialstaken,
						   suffix(curr_ta->tutorialstaken));
					pthread_mutex_unlock(&print_lock);
					curr_ta->course_id = course->id;

					// Check if the TA has any more attempts to take a tutorial
					// If not, reduce the number of eligible TAs of the lab
					if (curr_ta->tutorialstaken ==
						iiit_labs[lab_id]->tutorial_limit) {

						pthread_mutex_lock(&iiit_labs[lab_id]->lock);
						iiit_labs[lab_id]->eligibleTAs--;
						if (iiit_labs[lab_id]->eligibleTAs == 0) {
							pthread_cond_signal(
								&iiit_labs[lab_id]->condition_lock);
						}
						pthread_mutex_unlock(&iiit_labs[lab_id]->lock);
					}
					pthread_mutex_unlock(&curr_ta->lock);
					break;
				}
				pthread_mutex_unlock(&curr_ta->lock);
			}
			// Means that a TA has been picked
			if (labIndex != -1)
				break;
		}
		// This indicates that there might be TAs left, but they weren't
		// available. It's tested if there were TAs left in the while condition
		if (labIndex == -1)
			continue;
		// Allocate slots for course
		int slots = randomSeatAllocate(course->course_max_slot);
		pthread_mutex_lock(&print_lock);
		printf(BLU"Course %s has been allocated %d seats\n"WHT, course->name, slots);
		pthread_mutex_unlock(&print_lock);

		// Fill seats for course with students
		int allocated = studentAllocateSeats(course, slots);
		pthread_mutex_lock(&print_lock);
		printf(BLU"Tutorial has started for course %s with %d seats filled out of "
			   "%d\n"WHT,
			   course->name, allocated, slots);
		pthread_mutex_unlock(&print_lock);
		// Simulate the tutorial
		sleep(5);

		TA *curr_ta = iiit_labs[labIndex]->tas[taIndex];

		pthread_mutex_lock(&curr_ta->lock);
		// Don't make the TA available again if they have exhausted their limit
		if (curr_ta->tutorialstaken != iiit_labs[labIndex]->tutorial_limit)
			curr_ta->available = 1;
		// Finish the tutorial
		pthread_mutex_lock(&print_lock);
		printf(GRN"TA %d from lab %s has completed the tutorial and left the "
			   "course %s\n"WHT,
			   taIndex, iiit_labs[labIndex]->name, course->name);
		pthread_mutex_unlock(&print_lock);
		curr_ta->course_id = -1;
		pthread_mutex_unlock(&curr_ta->lock);

		for (int i = 0; i < student_count; i++) {
			Student *s = students[i];

			pthread_mutex_lock(&s->lock);
			if (s->current_preference != 3) {
				int course_id = s->preferences[s->current_preference];
				if (course->id == course_id && s->assigned_course) {
					pthread_cond_signal(&s->condition_lock);
				}
			}
			pthread_mutex_unlock(&s->lock);
		}
	}
	// If you've reached here, it means that there are no eligible TAs left
	pthread_mutex_lock(&course->lock);
	course->available = 0;
	pthread_mutex_unlock(&course->lock);

	pthread_mutex_lock(&course_lock);
	courses_left--;
	pthread_mutex_unlock(&course_lock);

	pthread_mutex_lock(&print_lock);
	printf(RED"Course %s doesn't have any TA's eligible and is removed from "
		   "course offerings\n"WHT,
		   course->name);
	pthread_mutex_unlock(&print_lock);
	return NULL;
}
//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/entities.h ####################//

#ifndef PROCESSSYNCHRONIZATION_ENTITIES_H
#define PROCESSSYNCHRONIZATION_ENTITIES_H
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
typedef struct course Course;
typedef struct student Student;
typedef struct ta TA;
typedef struct lab Lab;
struct course {
	int id;
	char *name;
	double interest;
	int course_max_slot;
	int lab_count;
	int *lab_ids;
	pthread_mutex_t lock;

	// Use lock for below variables
	int available;
};
struct student {
	int id;
	double caliber;
	int submission_time;
	int preferences[3];
	int current_preference;
	int selected_course; // Indicates the course selected by student. -1
						 // indicates not selected yet/ no course found
	int assigned_course; // Tells whether it was allocated a tutorial for the
						 // current preference.
	int submitted; // Decides whether the student should be considered for a
				   // tutorial slot
	pthread_mutex_t lock;
	pthread_cond_t condition_lock;
};
struct ta {
	int id;
	int course_id;
	int tutorialstaken;
	int available;
	pthread_mutex_t lock;
};
struct lab {
	int id;
	char *name;
	TA **tas;
	int ta_count;
	int eligibleTAs;	// Number of TAs left that are allowed to take tutorials
	int tutorial_limit; // Max number of tutorials a TA can take
	pthread_mutex_t lock;
	pthread_cond_t condition_lock;
};
Student *createStudent(double caliber, int submission_time,
					   const int preferences[3], int id);
Lab *createLab(char *name, int ta_count, int tutorial_limit, int id);
Course *createCourse(char *name, double interest, int course_max_slot,
					 const int *lab_ids, int lab_count, int id);
#endif // PROCESSSYNCHRONIZATION_ENTITIES_H

//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/globals.c ####################//

#include "globals.h"

Lab **iiit_labs;
Student **students;
Course **courses;
pthread_mutex_t seconds_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t course_lock =
	PTHREAD_MUTEX_INITIALIZER; // Use lock for updating time
pthread_mutex_t student_lock =
	PTHREAD_MUTEX_INITIALIZER; // Use lock for updating time
int student_count, lab_count, course_count;
int seconds = 0;
int courses_left = 0;
int students_left = 0;
pthread_mutex_t print_lock = PTHREAD_MUTEX_INITIALIZER;

//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/functions.h ####################//

#ifndef PROCESSSYNCHRONIZATION_FUNCTIONS_H
#define PROCESSSYNCHRONIZATION_FUNCTIONS_H
#include "entities.h"
#include "globals.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define BLK "\e[0;30m"
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define YEL "\e[0;33m"
#define BLU "\e[0;34m"
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
#define WHT "\e[0;37m"

int checkStudentRegistration(Student *s);
void *studentThread(void *arg);
void *courseThread(void *arg);
void *labThread(void *arg);
void *cleanupThread(void *arg);
#endif // PROCESSSYNCHRONIZATION_FUNCTIONS_H

//###########FILE CHANGE ./main_folder/Vidit Jain_305965_assignsubmission_file_/2020101134/q1/entities.c ####################//

#include "entities.h"
TA *createTA(int id) {
	TA *t = (TA *)malloc(sizeof(TA));
	t->available = 1;
	t->id = id;
	t->course_id = -1;
	t->tutorialstaken = 0;
	pthread_mutex_init(&t->lock, NULL);
	return t;
}

Student *createStudent(double caliber, int submission_time,
					   const int preferences[3], int id) {
	Student *s = (Student *)malloc(sizeof(Student));
	s->id = id;
	s->caliber = caliber;
	s->submission_time = submission_time;
	for (int i = 0; i < 3; i++)
		s->preferences[i] = preferences[i];
	s->submitted = 0;
	s->current_preference = 0;
	s->selected_course = -1;

	return s;
}

Lab *createLab(char *name, int ta_count, int tutorial_limit, int id) {
	Lab *t = (Lab *)malloc(sizeof(Lab));
	t->name = (char *)malloc((strlen(name) + 1) * sizeof(char));
	strcpy(t->name, name);
	t->ta_count = ta_count;
	t->tas = (TA **)malloc(t->ta_count * sizeof(TA *));
	for (int i = 0; i < ta_count; i++)
		t->tas[i] = createTA(i);
	t->eligibleTAs = t->ta_count;
	t->tutorial_limit = tutorial_limit;
	t->id = id;

	return t;
}

Course *createCourse(char *name, double interest, int course_max_slot,
					 const int *lab_ids, int lab_count, int id) {
	Course *c = (Course *)malloc(sizeof(Course));
	c->name = (char *)malloc((strlen(name) + 1) * sizeof(char));
	strcpy(c->name, name);
	c->interest = interest;
	c->lab_count = lab_count;
	c->lab_ids = (int *)malloc(c->lab_count * sizeof(int));
	for (int i = 0; i < lab_count; i++)
		c->lab_ids[i] = lab_ids[i];
	c->course_max_slot = course_max_slot;
	c->available = 1;
	c->id = id;
	return c;
}