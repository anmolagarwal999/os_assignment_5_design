
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/lab.h ####################//

#ifndef LABS_X
#define LABS_X

void initialize_labs_x(void);
void initialize_tas(int lab_id);
int total_tut_possible(void);

#endif
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/func.h ####################//

#ifndef F_X
#define F_X

int rand_bet(int lower, int upper);
double gen_rand();

#endif

//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/main.c ####################//

#include "headers.h"

int num_courses = 0;
int num_students = 0; ////////

int num_labs = 0;
int total_tuts = 0;
int tuts_left = 0;
int garbage = -1;

void print_input()
{

    printf("%d %d %d\n", num_students, num_labs, num_courses);

    for (int i = 1; i <= num_courses; i++)
    {
        printf("%s %.2f %d %d ", course_list[i].name, course_list[i].interest_quotient, course_list[i].max_slot, course_list[i].no_labs);
        for (int j = 0; j < course_list[i].no_labs; j++)
        {
            printf("%d ", course_list[i].labs[j].lab_id);
        }
        printf("\n");
    }

    for (int i = 1; i <= num_students; i++)
    {
        printf("%.2f %d %d %d %d", student_list[i].calibre_quotient, student_list[i].prefs[1], student_list[i].prefs[2], student_list[i].prefs[3], student_list[i].submission_time);
        // printf("%.2f  %d %d %d %d", student_list[i].calibre_quotient,student_list[i].pref1,student_list[i].pref2,student_list[i].pref3,student_list[i].submission_time);
        printf("\n");
    }

    for (int i = 1; i <= num_labs; i++)
    {
        printf("%s ", lab_list[i].name);
        printf("%d ", lab_list[i].total_mentors);
        printf("%d\n", lab_list[i].tutoring_limit);
    }
}

void input()
{
    //line 1
    //<num_students> <num_labs> <num_courses>
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    // printf("num_labs=%d\n", num_labs);

    for (int i = 1; i <= num_courses; i++)
    {

        course_list[i].course_id = i;
        scanf("\n%[a-zA-Z]", course_list[i].name);

        scanf("%f %d %d", &course_list[i].interest_quotient, &course_list[i].max_slot, &course_list[i].no_labs);
        for (int j = 0; j < course_list[i].no_labs; j++)
        {
            scanf("%d", &course_list[i].labs[j].lab_id);
        }
    }
    // printf("\n");

    // //input 3
    // //<calibre quotient of first student> <preference 1> <preference 2> <preference 3> <time after which he fills course preferences>
    for (int i = 1; i <= num_students; i++)
    {
        student_list[i].id = i;
        scanf("%f %d %d %d %d", &student_list[i].calibre_quotient, &student_list[i].prefs[1], &student_list[i].prefs[2], &student_list[i].prefs[3], &student_list[i].submission_time);
        // printf("%.2f  %d %d %d %d", student_list[i].calibre_quotient,student_list[i].pref1,student_list[i].pref2,student_list[i].pref3,student_list[i].submission_time);
    }

    for (int i = 1; i <= num_labs; i++)
    {
        scanf("\n%[a-zA-Z]", lab_list[i].name);
        scanf("%d", &lab_list[i].total_mentors);
        scanf("%d", &lab_list[i].tutoring_limit);
    }
    // // printf("\n");

    // print_input();

    if (num_students == 0)
    {
        printf(ANSI_RED "No student available.\n" ANSI_RESET);
        printf("Simulation Over.\n");

        exit(0);
    }

    if (num_courses == 0)
    {
        printf(ANSI_RED "No course is available to register.\n" ANSI_RESET);
        printf("Simulation Over.\n");

        exit(0);
    }

    if (num_labs == 0)
    {
        printf(ANSI_RED "No lab available to provide mentors.\n" ANSI_RESET);
        printf("Simulation Over\n");

        exit(0);
    }
}

int main()
{

    srand(time(NULL));

    input();

    initialize_labs_x();

    total_tut_possible();
    // printf("total tuts---%d\n", total_tuts);
    tuts_left = total_tuts;

    //initilazie  all the labs ,students,courses

    for (int i = 1; i <= num_courses; i++)
    {

        pthread_mutex_init(&(course_list[i].mutex), NULL);

        // course_list[i].th_course = pthread_create(&(course_list[i].th_course), NULL, initialize_course, (void *)(&i));
        if (pthread_create(&(course_list[i].th_course), NULL, initialize_course, (void *)(&i)) != 0)
        {
            perror("ok1");
        }
        // printf("created course thread %d\n", i);
    }
    // sleep(1);

    for (int i = 1; i <= num_students; i++)
    {

        pthread_mutex_init(&(student_list[i].mutex), NULL);

        // student_list[i].filled = -1;

        // student_list[i].th_student = pthread_create(&(student_list[i].th_student), NULL, initialize_student, (void *)(&i));
        if (pthread_create(&(student_list[i].th_student), NULL, initialize_student, (void *)(&i)) != 0)
        {
            perror("ok2");
        }

        // printf("created student thread %d\n", i);
        //perror
    }

    for (int i = 1; i <= num_labs; i++)
    {

        pthread_mutex_init(&lab_list[i].mutex, NULL);
        initialize_labs_x();
    }

    for (int i = 1; i <= num_courses; i++)
    {

        // pthread_join(pthread_t thread, void **value_ptr);
        // printf("bruh-courses\n");
        if (pthread_join(course_list[i].th_course, NULL) != 0)
        {
            // perror("");
        }
        // pthread_join(course_list[i].th_course, NULL);
    }
    for (int i = 1; i <= num_students; i++)
    {
        // printf("bruh-students\n");

        // pthread_join(student_list[i].th_student, NULL);
        if (pthread_join(student_list[i].th_student, NULL) != 0)
        {
            // perror("");
        }
    }

    printf(ANSI_GREEN "Simulation Over\n" ANSI_RESET);
    // fflush(stdout);

    return 0;
}

//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/students.h ####################//

#ifndef STUDENTS_X
#define STUDENTS_X

void *initialize_student(void *ptr);
int change_pref_or_exit(int student_id);
int confirm_course(int student_id, int course_id);
void seek_course(int student_id);

#endif
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/courses.h ####################//

#ifndef COURSES_X
#define COURSES_X

void *initialize_course(void *ptr);

void end_tut(int c_id);

void remove_course(int c_id);
void conduct_tut(int c_id);
int course_exhausted_chances(int c_id);
int check_tut_cond_n_conduct(int c_id);
void seek_ta_n_conduct(int c_id);

#endif
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/students.c ####################//

#include "global.h"
// #include "func.h"
#include "headers.h"

void *initialize_student(void *ptr)
{

    int id = *((int *)ptr);
    // printf("inside student thread %d\n", id);

    for (int i = 0; i < 10; i++)
    {
        student_list[id].withrawn[i] = 0;
    }

    student_list[id].filled = 0;
    student_list[id].current_pref = student_list[id].prefs[1];
    student_list[id].current_pref_no = 1;

    student_list[id].confirm = -1;
    student_list[id].past_pref = -1;
    // student_list[id].removed = -1;
    //--------------------------------------------------------------------------------------------------------------------------------------
    student_list[id].exit = 0;
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //only seek course when the priorities have been filled
    sleep(student_list[id].submission_time);
    seek_course(id);
    return NULL;
}

//return 1 if student wants to exit

int change_pref_or_exit(int student_id)
{

    if (student_list[student_id].current_pref_no < 3)
    {

        student_list[student_id].current_pref_no++;
        student_list[student_id].past_pref = student_list[student_id].current_pref; // dont name last_pref
        student_list[student_id].current_pref = student_list[student_id].prefs[student_list[student_id].current_pref_no];

        printf(ANSI_RED "Student %d has changed current preference from %d (priority %d) to %d (priority %d)\n" ANSI_RESET, student_id, student_list[student_id].current_pref, student_list[student_id].current_pref_no - 1, student_list[student_id].past_pref, student_list[student_id].current_pref_no);
    }
    else
    {
        //exit the simulation
        student_list[student_id].exit = 1;
        printf(ANSI_CYAN "Student either didn’t get any of their preferred courses or has withdrawn from them and has exited the simulation\n" ANSI_RESET);

        return student_list[student_id].exit;
    }
    return student_list[student_id].exit;
}

int confirm_course(int student_id, int course_id)
{

    int confirm_x = 0;

    double x = course_list[course_id].interest_quotient;
    double y = student_list[student_id].calibre_quotient;

    student_list[student_id].like_prob = x * y;

    double rand_x = gen_rand();
    if (rand_x >= student_list[student_id].like_prob)
    {
        confirm_x = 1;
    }
    else
    {
        confirm_x = 0;
    }
    return confirm_x;
}

void seek_course(int student_id)

{
    // printf(ANSI_BLUE "INSIDE SEEK COURSE \n" ANSI_RESET);

    while (student_list[student_id].confirm != 1 && student_list[student_id].withrawn[3] != 1)
    {
        // printf(ANSI_YELLOW "INSIDE SEEK COURSE WHILE LOOP\n" ANSI_RESET);

        printf(ANSI_RED "Student %d filled in preferences for course registration\n" ANSI_RESET, student_id);

        //CHECK IF COURSE IS REMOVED FROM THE PORTAL OR NOT
        if (course_list[student_list[student_id].current_pref].removed == 1)
        {
            // printf("student id %d-->CHECHING IF CURRENT PREF IS REMOVED\n", student_id);

            if (change_pref_or_exit(student_id) == 1)
            {
                printf("STUDENT %d EXITING.............\n", student_id);
                return;
            }
        }

        //WAIT FOR THE COURSE TO BECOME AVAILABLE

        // pthread_mutex_lock(&(student_list[student_id].mutex));

        // printf("student id %d-->current pref is %d\n", student_id, student_list[student_id].current_pref);

        if (course_list[student_list[student_id].current_pref].available != 1)
        {
            // printf("student id %d-->CHECHING IF CURRENT PREF IS AVAILABLE\n", student_id);

            sem_wait(&(course_list[student_list[student_id].current_pref].available_s));
            printf("Current pref is now available\n");
            course_list[student_list[student_id].current_pref].available = 1;
        }
        // pthread_mutex_unlock(&(student_list[student_id].mutex));

        // pthread_mutex_lock(&(student_list[student_id].mutex));

        if (course_list[student_list[student_id].current_pref].ta_present != 1)
        {

            //WAITING FOR TA TO BE ALLOTED
            // printf("student id %d-->here\n", student_id);
            sem_wait(&(course_list[student_list[student_id].current_pref].ta_alloted));
            // printf("ta allocated\n");
        }
        // pthread_mutex_lock(&(student_list[student_id].mutex));
        fflush(stdout);
        // printf("student id %d-->WAITING FOR SLOTS TO BE ASSIGNED in course %d\n", student_id, student_list[student_id].current_pref);
        pthread_mutex_lock(&(student_list[student_id].mutex));
        int y = 0;
        sem_getvalue(&(course_list[student_list[student_id].current_pref].rslots_assigned), &y);
        // printf(ANSI_YELLOW "\nbefore calling sem_wait current value of semaphore rslots_assigned for course %d is---%d\n\n" ANSI_RESET, student_list[student_id].current_pref, y);
        // pthread_mutex_unlock(&(student_list[student_id].mutex));
        sem_wait(&(course_list[student_list[student_id].current_pref].rslots_assigned));
        // pthread_mutex_lock(&(student_list[student_id].mutex));

        sem_getvalue(&(course_list[student_list[student_id].current_pref].rslots_assigned), &y);
        // printf(ANSI_YELLOW "\nafter calling sem_wait current value of semaphore rslots_assigned for course %d is---%d\n\n" ANSI_RESET, student_list[student_id].current_pref, y);
        pthread_mutex_unlock(&(student_list[student_id].mutex));
        // printf("rslots assigned for course %d------------------------------------------------------------------------------------------------------------------------\n", student_list[student_id].current_pref);
        // printf("course_list[student_list[student_id].current_pref].ta_present ---->%d\n\n", course_list[student_list[student_id].current_pref].ta_present);
        // printf("waiting for ta to be alloted #######################################################\n");

        // if (course_list[student_list[student_id].current_pref].ta_present != 1)
        // {

        //     //WAITING FOR TA TO BE ALLOTED
        //     printf("student id %d-->here\n", student_id);
        //     sem_wait(&(course_list[student_list[student_id].current_pref].ta_alloted));
        //     printf("ta allocated\n");
        // }
        // sem_wait(&(course_list[student_list[student_id].current_pref].ta_present));
        if (course_list[student_list[student_id].current_pref].ta_present == 1) /*a ta is present*/
        {
            //WAIT WHILE RAND SLOTS ARE NOT ASSIGNED
            // while (course_list[student_list[student_id].current_pref].rand_slots == 0)
            // {
            // }

            if (course_list[student_list[student_id].current_pref].filled_slots < course_list[student_list[student_id].current_pref].rand_slots)
            {

                printf(ANSI_GREEN "Student %d has been allocated a seat in course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                pthread_mutex_lock(&(student_list[student_id].mutex));
                int confirm = confirm_course(student_id, course_list[student_list[student_id].current_pref].course_id);

                if (confirm)
                {

                    printf(ANSI_MAGENTA "Student %d wants to select course %d permanently\n" ANSI_RESET, student_id, student_list[student_id].current_pref);

                    // pthread_mutex_lock(&(course_list[student_list[student_id].current_pref].mutex));
                    // pthread_mutex_lock(&(student_list[student_id].mutex));

                    course_list[student_list[student_id].current_pref].filled_slots++;
                    sem_post(&(course_list[student_list[student_id].current_pref].f_slots));
                    // printf("after course_list[student_list[student_id].current_pref].filled_slots++;-->filled slots==  %d\n", course_list[student_list[student_id].current_pref].filled_slots);

                    printf(ANSI_YELLOW "Student %d has selected course %d permanently\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                    pthread_mutex_unlock(&(student_list[student_id].mutex));

                    // pthread_mutex_unlock(&(course_list[student_list[student_id].current_pref].mutex));

                    //exit the simulation
                    printf(ANSI_GREEN "Student %d exiting simulation\n" ANSI_RESET, student_id);

                    pthread_exit(NULL);
                    // return;
                }

                else
                {
                    printf(ANSI_BLUE "Student %d has withdrawn from course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                    student_list[student_id].withrawn[student_list[student_id].current_pref_no] = 1;

                    if (change_pref_or_exit(student_id) == 1)
                    {
                        return;
                    }
                }

                //if seats are allocated the student will exit the simulation
            }
            else
            {

                printf(ANSI_BLUE "Student %d has withdrawn from course %d\n" ANSI_RESET, student_id, student_list[student_id].current_pref);
                student_list[student_id].withrawn[student_list[student_id].current_pref_no] = 1;
                if (change_pref_or_exit(student_id) == 1)
                {
                    return;
                }
            }
        }
    }

    printf(ANSI_GREEN "STUDENT %d EXITED THE SIMULATION\n" ANSI_RESET, student_id);
    pthread_exit(NULL);
    // return;
}

//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/global.h ####################//

#ifndef GLOBALS
#define GLOBALS

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

#define ANSI_RED "\x1b[31m"
#define ANSI_GREEN "\x1b[32m"
#define ANSI_YELLOW "\x1b[33m"
#define ANSI_BLUE "\x1b[34m"
#define ANSI_MAGENTA "\x1b[35m"
#define ANSI_CYAN "\x1b[36m"
#define ANSI_RESET "\x1b[0m"

#define max_courses 10
#define max_labs 10
#define max_students 25

extern int num_courses;
extern int num_students; ////////

extern int num_labs;

extern int tutoring_delay_time;

extern int total_tuts; //total tuts possible
extern int tuts_left;
extern int garbage;

struct lab
{
    // pthread_t th_lab;
    int lab_id;
    char name[10];
    int total_mentors;
    int tutoring_limit; //total no of times a ta can tutor considering all courses
    pthread_mutex_t mutex;
    // int ta_cur_term[10];
    int ta_terms[10];
    int ta_free[10]; //whether a ta is free or taking a tutorail

    //  struct ta tas[total_mentors+1];
    // int tas[total_mentors+1];
    // int tas[10];
    // struct ta tas[10];

    int active; //All people in the lab have exhausted the limit on the number of times someone can accept a TAship and
    //so, the lab will no longer send out any TAs
};

struct course
{
    pthread_t th_course;

    int course_id;
    char name[50];
    float interest_quotient;
    int no_labs; //no of labs from which the course accepts ta
    struct lab labs[10];
    int ta_present; //id of the ta which is currently tutoring(only one ta at a time can tutor)
    int max_slot;   //maximum no of students who can attend the course

    //initialize filled slots to 0 at the beginning of the program

    int filled_slots;

    pthread_mutex_t mutex;
    sem_t slot_fill_wait;
    sem_t f_slots; //current slots assigned according to the number of students and d
    int available;
    sem_t available_s;
    int current_ta;       //number will vary according to the lab chosen
    sem_t conducting_tut; ////1 or 0
    int rand_slots;
    int d;
    sem_t ta_alloted;          //1 or 0
    sem_t rslots_assigned;     //whether d is calcualted or not (0 or 1)
    int current_lab_id;        //of the current ta
    char current_lab_name[20]; //of the current ta
    int removed;
};

struct student
{

    pthread_t th_student;
    int id;
    int confirm;

    struct course courses[10]; //courses preference of the students in decreasing order of priority
    int withrawn[10];          //pref withrawn or not         ///if the student has withrawn from the course

    //withrawn is indexed according to pref no not course no
    //time after which he fills course preferences
    int submission_time;

    int filled; //becomes_available; //only becomes available after filling in course priorities

    float calibre_quotient;
    // int pref1;
    // int pref2;
    // int pref3;

    int prefs[4];

    int current_pref; //wait for this//this will store id not name

    int current_pref_no;
    int past_pref;
    double like_prob; //probability of liking the course=interest_of_the_course*student_calibre
    pthread_mutex_t mutex;
    int exit; //yes or no (1 for yes, 0 for no)
};

struct ta
{
    int id;
    int available;
    int terms;
    int free;
};

struct course course_list[max_courses];
struct lab lab_list[max_labs];
struct student student_list[max_students];
struct ta ta_list[20];

#endif
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/func.c ####################//

#include "headers.h"

int rand_bet(int lower, int upper)
{

    int num = (rand() % (upper - lower + 1)) + lower;

    return num;
}
double gen_rand()
{
    if ((double)rand() / (double)RAND_MAX)
    {
        return 0.4;
    }
    return (double)rand() / (double)RAND_MAX;
}
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/courses.c ####################//

#include "headers.h"

void *initialize_course(void *ptr)
{

    int id = *((int *)ptr);
    // printf("inside course thread %d\n", id);

    sem_init(&(course_list[id].slot_fill_wait), 0, 1);
    sem_init(&(course_list[id].available_s), 0, 1);
    sem_init(&(course_list[id].conducting_tut), 0, 0);
    sem_init(&(course_list[id].ta_alloted), 0, 0);
    sem_init(&(course_list[id].rslots_assigned), 0, 0);

    course_list[id].ta_present = -1;
    course_list[id].filled_slots = 0;
    course_list[id].available = 1;
    course_list[id].available = 0;

    // course_list[id].available = -1;
    course_list[id].removed = 0;
    // printf(ANSI_RED "NO OF LABS FROM WHICH COURSE %d ACCEPTS TA IS _____%d\n" ANSI_RESET, id, course_list[id].no_labs);
    for (int i = 0; i < course_list[id].no_labs; i++)
    {
        course_list[id].labs[i].total_mentors = lab_list[course_list[id].labs[i].lab_id].total_mentors;
        course_list[id].labs[i].tutoring_limit = lab_list[course_list[id].labs[i].lab_id].tutoring_limit;
        for (int j = 0; j < course_list[id].labs[i].total_mentors; j++)
        {
            course_list[id].labs[i].ta_free[j] = lab_list[course_list[id].labs[i].lab_id].ta_free[j];
        }
    }

    //only seek course when the priorities have been filled
    seek_ta_n_conduct(id);
    return NULL;
}

void end_tut(int c_id)
{
    //restore slots

    //
    pthread_mutex_lock(&(course_list[c_id].mutex));
    // course_list[c_id].filled_slots = max_slots - course_list[c_id].filled_slots;
    printf("TUTORIAL ENDED\n");
    printf(ANSI_RED "TA %d from lab %d has completed the tutorial and left the course %s\n" ANSI_RESET, course_list[c_id].current_ta, course_list[c_id].labs[course_list[c_id].current_lab_id].lab_id, course_list[c_id].name);

    course_list[c_id].available = 1;

    course_list[c_id].labs[course_list[c_id].current_lab_id].ta_free[course_list[c_id].current_ta] = 1;

    course_list[c_id].current_ta = -1;
    course_list[c_id].filled_slots = 0;
    course_list[c_id].rand_slots = 0;
    course_list[c_id].ta_present = -1;
    // sem_destroy(&(course_list[c_id].rslots_assigned));
    // sem_init(&(course_list[c_id].rslots_assigned), 0, 0);

    sem_destroy(&(course_list[c_id].slot_fill_wait));
    sem_destroy(&(course_list[c_id].available_s));

    sem_destroy(&(course_list[c_id].conducting_tut));

    sem_destroy(&(course_list[c_id].ta_alloted));
    sem_destroy(&(course_list[c_id].rslots_assigned));

    sem_init(&(course_list[c_id].slot_fill_wait), 0, 1);
    sem_init(&(course_list[c_id].available_s), 0, 1);
    sem_init(&(course_list[c_id].conducting_tut), 0, 0);
    sem_init(&(course_list[c_id].ta_alloted), 0, 0);
    sem_init(&(course_list[c_id].rslots_assigned), 0, 0);

    pthread_mutex_unlock(&(course_list[c_id].mutex));
}

void remove_course(int c_id)
{

    course_list[c_id].removed = 1;
}

void conduct_tut(int c_id)
{

    // sem_wait(&(course_list[c_id].conducting_tut));

    sleep(7);

    // sem_post(&(course_list[c_id].conducting_tut));
}

int course_exhausted_chances(int c_id)
{
    for (int i = 1; i <= num_labs; i++)
    {
        for (int j = 0; j < 10; j++)
        {

            if (lab_list[i].ta_terms[j] < lab_list[i].tutoring_limit)
            {
                return 0; //no
            }
        }
        // pthread_mutex_lock(&(course_list[c_id].mutex));
        lab_list[i].active = 0;
        // pthread_mutex_unlock(&(course_list[c_id].mutex));

        printf(ANSI_BLUE "Lab %d no longer has students available for TA ship\n" ANSI_RESET, i);
    }

    printf(ANSI_YELLOW "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_RESET, course_list[c_id].name); //u can also print this whether this func returns 1
    course_list[c_id].removed = 1;
    return 1; //yes it can no longer conduct tut
}

int check_tut_cond_n_conduct(int c_id)
{

    // ○ Let W be the number of students who currently have course C as their current preference.
    // ○ If W>=D, fill the D slots and start conducting the tutorial.
    // ○ If W<D, then leave the remaining (D-W) slots unfilled and start conducting the tutorial.
    // printf(ANSI_MAGENTA "inside the check_tut_cond_n_conduct() for course %d\n " ANSI_RESET, c_id);

    pthread_mutex_lock(&(course_list[c_id].mutex));

    //     Decide D as a random integer between 1 and “course_max_slots” (inclusive).

    int d = rand_bet(1, course_list[c_id].max_slot);
    course_list[c_id].rand_slots = d;

    printf(ANSI_CYAN "Course %d has been allocated %d seats\n" ANSI_RESET, c_id, course_list[c_id].rand_slots);
    sem_post(&(course_list[c_id].rslots_assigned));
    // printf(ANSI_YELLOW "\n\n initilaizing rand_slot semaphore for course %d\n" ANSI_RESET, c_id);
    sem_init(&(course_list[c_id].f_slots), 0, course_list[c_id].rand_slots);
    int x = 0;
    sem_getvalue(&(course_list[c_id].f_slots), &x);
    // printf("course_list[c_id].f_slots sempahore value form get value fn-->%d\n", x);
    // printf("course_list[c_id].filled_slots ---> %d\n", course_list[c_id].filled_slots);

    pthread_mutex_unlock(&(course_list[c_id].mutex));
    // sem_post(&(course_list[student_list[student_id].current_pref].ta_alloted));

    //once slots are assigned wait for the students to fill in the slots

    sleep(5);
    // “Tutorial has started for Course c_i with k seats filled out of j”

    while (course_list[c_id].filled_slots == 0)
    {
        sem_wait(&(course_list[c_id].f_slots));
    }
    // sem_wait(&(course_list[c_id].f_slots));

    printf(ANSI_YELLOW "Tutorial has started for Course %d with %d seats filled out of %d\n" ANSI_RESET, c_id, course_list[c_id].filled_slots, course_list[c_id].rand_slots);

    conduct_tut(c_id);

    // TA t_j from lab l_k has completed the tutorial and left the course c_i
    end_tut(c_id);
}

void seek_ta_n_conduct(int c_id)
{

    //check if any ta is available or not
    //assumed that there is no common ta between labs

    //for all the labs in that partciular course check which ta is available
    printf(ANSI_CYAN "COURSE %d STARTING TO SEEK TA\n" ANSI_RESET, c_id);
    while (course_exhausted_chances(c_id) != 1)
    {
        //select a ta
        // printf(ANSI_CYAN "COURSE %d STARTING TO SEEK TA --inside while loop\n" ANSI_RESET, c_id);

        // printf(ANSI_MAGENTA "inside course exhaused while loop of course %d\n" ANSI_RESET, c_id);

        for (int i = 0; i < course_list[c_id].no_labs; i++)
        {
            // printf(ANSI_YELLOW "inside the 1st if statemnet course exhaused while loop of course %d\n" ANSI_RESET, c_id);

            if (lab_list[course_list[c_id].labs[i].lab_id].active == 1)
            {
                // printf(ANSI_CYAN "inside the 2st if statemnet course exhaused while loop of course %d\n" ANSI_RESET, c_id);
                // printf("TOTAL MENTORS OF LAB %d ---->  %d\n", course_list[c_id].labs[i].lab_id, lab_list[course_list[c_id].labs[i].lab_id].total_mentors);
                // printf("TOTAL MENTORS OF LAB %d ---->  %d\n", course_list[c_id].labs[i].lab_id, course_list[c_id].labs[i].total_mentors);

                for (int j = 0; j < course_list[c_id].labs[i].total_mentors; j++)

                {
                    // printf("ok------------------\n");
                    // printf("course_list[c_id].labs[i].ta_free[j]------>%d\n", course_list[c_id].labs[i].ta_free[j]);
                    // printf("course_list[c_id].labs[i].ta_terms[j]--->%d\n", course_list[c_id].labs[i].ta_terms[j]);
                    // printf("course_list[c_id].labs[i].tutoring_limit--->%d\n", course_list[c_id].labs[i].tutoring_limit);

                    // if (course_list[c_id].labs[i].ta_free[j] == 1 && (course_list[c_id].labs[i].ta_terms[j] < course_list[c_id].labs[i].tutoring_limit))
                    if (lab_list[course_list[c_id].labs[i].lab_id].ta_free[j] == 1 && (course_list[c_id].labs[i].ta_terms[j] < course_list[c_id].labs[i].tutoring_limit))

                    {

                        //assign this ta to course

                        // printf(ANSI_RED "bruh\n" ANSI_RESET);
                        //change course to unavailable
                        //wait for the stuednts for the ta to be alloted is over - students can now resume confirming and withrawing course
                        sem_post(&(course_list[c_id].ta_alloted));
                        sem_getvalue(&(course_list[c_id].ta_alloted), &garbage);
                        // printf("course_list[c_id].ta_alloted--->%d\n", garbage);
                        printf(ANSI_YELLOW "TA %d from lab %s has been allocated to course %s,%d for %d th TA ship\n" ANSI_RESET, j, lab_list[course_list[c_id].labs[i].lab_id].name, course_list[c_id].name, c_id, course_list[c_id].labs[i].ta_terms[j]);

                        course_list[c_id].available = 0; //-----------------------????semaphore or is it to be locked under mutex??????????????????????????????????????????????????
                        course_list[c_id].ta_present = 1;
                        // printf("\ntA PRESENT__>%d\n", course_list[c_id].ta_present);
                        course_list[c_id].current_lab_id = course_list[c_id].labs[i].lab_id;
                        // course_list[c_id].current_lab_name = course_list[c_id].labs[i].name;

                        strcpy(course_list[c_id].current_lab_name, course_list[c_id].labs[i].name);

                        pthread_mutex_lock(&(course_list[c_id].mutex));

                        course_list[c_id].labs[i].ta_terms[j]++;
                        course_list[c_id].labs[i].ta_free[j] = 0; //or should i make this a semaphore since other courses which had earlier got this ta due to race

                        pthread_mutex_unlock(&(course_list[c_id].mutex));
                        break;
                    }
                }
                // printf("\n\n*********************************TOTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
                // check_tut_cond_n_conduct(c_id);
            }
            // printf("\n\n*********************************TOTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
            // check_tut_cond_n_conduct(c_id);
        }
        printf("\n\n*********************************TUTORIAL FOR COURSE %d IS STARTING***************************************************************\n", c_id);
        check_tut_cond_n_conduct(c_id);
    }

    if (course_exhausted_chances(c_id) == 1)
    {
        //remove_course(c_id);
        //here  u need to print the current status of the students
        exit(0);
    }
}

//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/lab.c ####################//

#include "headers.h"

//initialize all the labs
void initialize_labs_x(void)
{

    for (int i = 1; i <= num_labs; i++)
    {
        lab_list[i].active = 1;
    }

    //initialize all tas in a lab
    for (int i = 1; i <= num_labs; i++)
    {
        // int id = lab_list[i].lab_id;
        // printf(ANSI_BLUE "lab %d about to initilaize tas--\n" ANSI_RESET, i);
        // initialize_tas(id);
        // initialize_tas(id);
        initialize_tas(i);

        // printf(ANSI_YELLOW "initialized tas for lab %d\n" ANSI_RESET, i);
    }
}
//WHEN WILL THE LAB SEEK TA???????

//initialize tas in a lab
void initialize_tas(int lab_id)
{

    // int total_tas = lab_list[lab_id].total_mentors;
    // printf("total tas in lab %d is %d\n", lab_id, total_tas);
    // printf("total tas in lab %d is %d\n", lab_id, lab_list[lab_id].total_mentors);

    for (int i = 0; i < lab_list[lab_id].total_mentors; i++)
    {
        //  total_tas=lab_list[lab_id].tas[i].available=1;
        // printf(ANSI_RED "initilaising ta_free and ta_terms for lab %d\n" ANSI_RESET, lab_id);
        lab_list[lab_id].ta_terms[i] = 0;
        lab_list[lab_id].ta_free[i] = 1;
    }
}

// int is_active(int lab_id){
//     for(int i=0;i<=lab_list[lab_id].total_mentors;i++){
//         //check tutoring limit
//         if(lab_list[lab_id].ta_terms[i]<lab_list[lab_id].tutoring_limit){
//             //lab is active
//         }
//     }
//     //lab is not active
//     return 0;

// }

int total_tut_possible(void)
{

    //loop over all the labs
    for (int i = 1; i <= num_labs; i++)
    {
        total_tuts += (lab_list[i].total_mentors * (lab_list[i].tutoring_limit));
    }

    return total_tuts;
}
//###########FILE CHANGE ./main_folder/Surabhi Jain_305907_assignsubmission_file_/q1/headers.h ####################//

#ifndef HEADERS
#define HEADERS

#include <stdio.h>
#include <stdlib.h>
// #include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

#include <pthread.h>
// #include "lab.h"
// #include "students.h"
// #include "courses.h"

#include "global.h"
#include "students.h"
#include "lab.h"
#include "courses.h"
#include "func.h"

// #define debug(x) printf("\n\"%s\" is: %d\n", #x, x);

#endif

//study makefiles bruh