
//###########FILE CHANGE ./main_folder/DIVYANSH TIWARI_305914_assignsubmission_file_/q1/q1.c ####################//

#include "headers.h"

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

void init_input()
{
    scanf("%d%d%d", &num_students, &num_labs, &num_courses);

    ////////////////////////////////////////////////////////////////////////////////////// taking input of courses

    Courses = malloc(sizeof(course) * num_courses);

    for (int i = 0; i < num_courses; i++)
    {
        num_students_waiting[i] = 0;

        scanf("%s %lf %d %d", Courses[i].name, &Courses[i].interest_quotient, &Courses[i].max_seats_allowed, &Courses[i].num_labs);

        for (int j = 0; j < Courses[i].num_labs; j++)
        {
            scanf("%d", &Courses[i].lab_ids[j]);
        }
        Courses[i].curr_lab_id = -1;
        Courses[i].curr_ta_id = -1;
        Courses[i].done = 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////// taking input of students struct array

    Students = malloc(sizeof(student) * num_students);

    for (int i = 0; i < num_students; i++)
    {
        scanf("%lf %d %d %d %d", &Students[i].calibre_quotient, &Students[i].pref_one, &Students[i].pref_two, &Students[i].pref_three, &Students[i].time);

        Students[i].status = 1;
        Students[i].curr_pref = Students[i].pref_one;
        Students[i].permanent = -1;
        Students[i].status_change = 0;
        Students[i].curr_pref_number = 1;
    }

    for (int i = 0; i < num_students; i++)
    {
        num_students_waiting[Students[i].pref_one] += 1;
    }

    ///////////////////////////////////////////////////////////////////////////////////// taking input of labs

    Labs = malloc(sizeof(lab) * num_labs);

    for (int i = 0; i < num_labs; i++)
    {
        scanf("%s %d %d", Labs[i].name_lab, &Labs[i].num_tas, &Labs[i].max_tuts);

        for (int j = 0; j < Labs[i].num_tas; j++)
        {
            Labs[i].tas_working_status[j] = 0;
            Labs[i].taships_completed[j] = 0;
        }
    }
}

int print_end()
{

    int course_inactive = 1;
    for (int i = 0; i < num_courses; i++)
    {
        if (Courses[i].done == 0)
        {
            course_inactive = 0;
            break;
        }
    }

    if (course_inactive)
    {
        for (int i = 0; i < num_students; i++)
        {
            pthread_mutex_init(&Students[i].student_mutex, NULL);
            pthread_cond_init(&Students[i].student_condition, NULL);

            pthread_mutex_lock(&Students[i].student_mutex);
            if (Students[i].status != 7 && Students[i].permanent == -1)
            {
                Students[i].status_change = 0;
                pthread_cond_broadcast(&Students[i].student_condition);
            }
            pthread_mutex_unlock(&Students[i].student_mutex);
            pthread_mutex_destroy(&Students[i].student_mutex);
            pthread_cond_destroy(&Students[i].student_condition);

        }
        return 1;
    }
    else
    {
        int students_left = 0;
        for (int i = 0; i < num_students; i++)
        {
            if (Students[i].status != 7 && Students[i].permanent == -1)
            {
                students_left++;
            }
        }
        if (students_left == 0)
            return 1;
        else
            return 0;
    }
}

void *time_increment()
{
    // pthread_mutex_init(&time_mutex, NULL);

    // pthread_cond_init(&time_condition, NULL);

    while (1)
    {
        pthread_mutex_lock(&time_mutex);
        if (print_end())
            break;
        curr_time++;
        pthread_mutex_unlock(&time_mutex);
        pthread_cond_broadcast(&time_condition);
        sleep(1);
    }

    // pthread_mutex_destroy(&time_mutex);

    // pthread_cond_destroy(&time_condition);
}

void *student_function(void *arg)
{
    pthread_mutex_lock(&time_mutex);
    while (curr_time < Students[*(int *)arg].time)
    {
        pthread_cond_wait(&time_condition, &time_mutex);
    }
    pthread_mutex_unlock(&time_mutex);

    printf(BLUE "Student %d has filled in preferences for course registration\n" RESET, *(int *)arg);
    Students[*(int *)arg].status = 1;

    int alpha = *((int *)arg);

    pthread_mutex_init(&Students[alpha].student_mutex, NULL);
    pthread_cond_init(&Students[alpha].student_condition, NULL);

    while (1)
    {
        pthread_mutex_lock(&Students[alpha].student_mutex);

        if (Students[alpha].permanent == -1 && (Students[alpha].status == 1 || Students[alpha].status == 2) && Courses[Students[alpha].pref_one].done == 1 && Students[alpha].status_change == 0)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_one].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 3;
            Students[alpha].curr_pref = Students[alpha].pref_two;
            Students[alpha].curr_pref_number = 2;
            usleep(50000);
            printf(BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" RESET, alpha, Courses[Students[alpha].pref_one].name, Courses[Students[alpha].pref_two].name);
            pthread_mutex_unlock(&Students[alpha].student_mutex);

            continue;
        }
        if (Students[alpha].permanent == -1 && (Students[alpha].status == 3 || Students[alpha].status == 4) && Courses[Students[alpha].pref_two].done == 1 && Students[alpha].status_change == 0)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_two].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 5;
            Students[alpha].curr_pref = Students[alpha].pref_three;
            Students[alpha].curr_pref_number = 3;
            usleep(50000);
            printf(BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" RESET, alpha, Courses[Students[alpha].pref_two].name, Courses[Students[alpha].pref_three].name);
            pthread_mutex_unlock(&Students[alpha].student_mutex);

            continue;
        }
        if (Students[alpha].permanent == -1 && (Students[alpha].status == 5 || Students[alpha].status == 6) && Courses[Students[alpha].pref_three].done == 1 && Students[alpha].status_change == 0)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_three].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 7;
            usleep(50000);
            printf(BLUE "Student %d could not get any of his preferred courses\n" RESET, alpha);
            pthread_mutex_unlock(&Students[alpha].student_mutex);
            break;
        }

        pthread_cond_wait(&Students[alpha].student_condition, &Students[alpha].student_mutex);

        if (Students[alpha].status == 1 && Students[alpha].status_change == 1 && Students[alpha].permanent == -1)
        {
            printf(BLUE "Student %d has been allocated a seat in course %s\n" RESET, alpha, Courses[Students[alpha].pref_one].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 2;
        }
        else if (Students[alpha].status == 2 && Students[alpha].status_change == 1 && Students[alpha].permanent == -1)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_one].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 3;
            Students[alpha].curr_pref = Students[alpha].pref_two;
            usleep(50000);
            printf(BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" RESET, alpha, Courses[Students[alpha].pref_one].name, Courses[Students[alpha].pref_two].name);
        }
        else if (Students[alpha].status == 3 && Students[alpha].status_change == 1 && Students[alpha].permanent == -1)
        {
            printf(BLUE "Student %d has been allocated a seat in course %s\n" RESET, alpha, Courses[Students[alpha].pref_two].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 4;
        }
        else if (Students[alpha].status == 4 && Students[alpha].status_change == 1 && Students[alpha].permanent == -1)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_two].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 5;
            Students[alpha].curr_pref = Students[alpha].pref_three;

            usleep(50000);
            printf(BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" RESET, alpha, Courses[Students[alpha].pref_two].name, Courses[Students[alpha].pref_three].name);
        }
        else if (Students[alpha].status == 5 && Students[alpha].status_change == 1 && Students[alpha].permanent == -1)
        {
            printf(BLUE "Student %d has been allocated a seat in course %s\n" RESET, alpha, Courses[Students[alpha].pref_three].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 6;
        }
        else if (Students[alpha].status == 6 && Students[alpha].status_change == 1)
        {
            printf(BLUE "Student %d has withdrawn from course %s\n" RESET, alpha, Courses[Students[alpha].pref_three].name);
            Students[alpha].status_change = 0;
            Students[alpha].status = 7;
            usleep(50000);
            printf(BLUE "Student %d could not get any of his preferred courses\n" RESET, alpha);
            pthread_mutex_unlock(&Students[alpha].student_mutex);
            break;
        }

        if (Students[alpha].permanent != -1)
        {
            printf(BLUE "Student %d has selected course %s permanently\n" RESET, alpha, Courses[Students[alpha].permanent].name);
            pthread_mutex_unlock(&Students[alpha].student_mutex);
            break;
        }
        pthread_mutex_unlock(&Students[alpha].student_mutex);
        sleep(1);
    }

    // printf(GREEN "Student %d mar gya\n" RESET, alpha);
   // pthread_mutex_destroy(&Students[alpha].student_mutex);

    //pthread_cond_destroy(&Students[alpha].student_condition);
}

void *course_function(void *arg)
{
    int alpha = *((int *)arg);
    pthread_mutex_init(&Courses[alpha].course_mutex, NULL);
    pthread_cond_init(&Courses[alpha].course_condition, NULL);

    int seekers[num_students];
    for (int i = 0; i < num_students; i++)
        seekers[i] = -1;
    int seeker_index;

    while (1)
    {
        int check_done_students = 0;
        for (int i = 0; i < num_students; i++)
        {
            if (Students[i].permanent != -1 || Students[i].status == 7)
                check_done_students += 1;
        }

        if (check_done_students == num_students)
            break;

        seeker_index = 0;

        for (int i = 0; i < num_students; i++)
        {
            if (Students[i].curr_pref == alpha)
            {
                seekers[seeker_index] = i;
                seeker_index++;
            }
        }

        if(seeker_index == 0)
        {
            usleep(5000);
            continue;
        }
        int curr_lab_id;
        int curr_ta_id;
        int lab_min;
        int ta_min;
        int lab_found = 0;

        for (int i = 0; i < Courses[alpha].num_labs; i++)
        {
            int lab_index = Courses[alpha].lab_ids[i];
            int found = 1;

            for (int j = 0; j < Labs[lab_index].num_tas; j++)
            {
                if (Labs[lab_index].tas_working_status[j] == 0 && Labs[lab_index].taships_completed[j] < Labs[lab_index].max_tuts)
                {
                    found = 0;
                    break;
                }
            }

            if (found)
                continue;

            lab_min = -1;
            ta_min = -1;
            int aid = 1;
            for (int j = 0; j < Labs[lab_index].num_tas; j++)
            {
                if (aid == 1)
                {
                    if (Labs[lab_index].tas_working_status[j] == 0 && Labs[lab_index].taships_completed[j] < Labs[lab_index].max_tuts)
                    {
                        lab_min = lab_index;
                        ta_min = j;
                        lab_found = 1;
                        aid = 0;
                    }
                }
                else
                {
                    if (Labs[lab_index].taships_completed[j] < Labs[lab_min].taships_completed[ta_min] && Labs[lab_index].tas_working_status[j] == 0 && Labs[lab_index].taships_completed[j] < Labs[lab_index].max_tuts)
                    {
                        lab_min = lab_index;
                        ta_min = j;
                        lab_found = 1;
                    }
                }
            }
            if (lab_found)
                break;
        }

        if (lab_found)
        {
            Labs[lab_min].taships_completed[ta_min] += 1;
            printf(GREEN "TA %d from lab %s has been allocated to course %s for TA ship = %d\n" RESET, ta_min, Labs[lab_min].name_lab, Courses[alpha].name, Labs[lab_min].taships_completed[ta_min]);
            usleep(50000);
            Labs[lab_min].tas_working_status[ta_min] = 1;
            Courses[alpha].curr_lab_id = lab_min;
            Courses[alpha].curr_ta_id = ta_min;

            int seats = rand() % Courses[alpha].max_seats_allowed;
            seats += 1;

            if (seeker_index > seats)
            {
                for (int i = 0; i < seats; i++)
                {
                    Students[seekers[i]].status_change = 1;

                    pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                    usleep(5000);

                    Students[seekers[i]].status_change = 0;
                }

                printf(GREEN "Course %s has been allocated %d seats\n" RESET, Courses[alpha].name, seats);

                usleep(50000);

                printf(GREEN "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, Courses[alpha].name, seats, seats);

                sleep(1);

                printf(GREEN "TA %d from lab %s has completed the tutorial for course %s \n" GREEN, ta_min, Labs[lab_min].name_lab, Courses[alpha].name);
                Labs[lab_min].tas_working_status[ta_min] = 0;

                for (int i = 0; i < seats; i++)
                {
                    if (Students[seekers[i]].permanent != -1)
                        continue;

                    double probability = Courses[alpha].interest_quotient * Students[seekers[i]].calibre_quotient;
                    probability = probability * 100;

                    if (rand() % 100 < probability)
                    {
                        Students[seekers[i]].status_change = 1;
                        pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                        Students[seekers[i]].status_change = 0;
                    }
                    else
                    {
                        Students[seekers[i]].permanent = alpha;
                        pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                    }
                }
            }
            else
            {
                for (int i = 0; i < seeker_index; i++)
                {
                    Students[seekers[i]].status_change = 1;
                    pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                    usleep(5000);
                    Students[seekers[i]].status_change = 0;
                }

                printf(GREEN "Course %s has been allocated %d seats\n" RESET, Courses[alpha].name, seats);

                usleep(50000);

                printf(GREEN "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, Courses[alpha].name, seeker_index, seats);

                sleep(1);

                printf(GREEN "TA %d from lab %s has completed the tutorial for course %s \n" RESET, ta_min, Labs[lab_min].name_lab, Courses[alpha].name);
                Labs[lab_min].tas_working_status[ta_min] = 0;

                for (int i = 0; i < seeker_index; i++)
                {
                    if (Students[seekers[i]].permanent != -1)
                        continue;

                    double probability = Courses[alpha].interest_quotient * Students[seekers[i]].calibre_quotient;
                    probability = probability * 100;
                    if (rand() % 100 < probability)
                    {
                        Students[seekers[i]].status_change = 1;
                        pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                        usleep(5000);
                        Students[seekers[i]].status_change = 0;
                    }
                    else
                    {
                        Students[seekers[i]].permanent = alpha;
                        pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                        usleep(5000);
                    }
                }
            }
        }
        else
        {
            int exit = 1;

            for (int i = 0; i < Courses[alpha].num_labs; i++)
            {
                int check_lab_id = Courses[alpha].lab_ids[i];

                for (int j = 0; j < Labs[check_lab_id].num_tas; j++)
                {
                    if (Labs[check_lab_id].taships_completed[j] < Labs[check_lab_id].max_tuts)
                    {
                        exit = 0;
                        break;
                    }
                }
            }

            if (exit)
            {
                printf(RED "Course %s doesn't have any TA's eligible and is removed from course offerings\n" RESET, Courses[alpha].name);

                for (int i = 0; i < seeker_index; i++)
                {
                    Students[seekers[i]].status_change = 0;
                    pthread_cond_broadcast(&Students[seekers[i]].student_condition);
                    usleep(5000);
                    Students[seekers[i]].status_change = 0;
                }

                for (int i = 0; i < num_students; i++)
                    seekers[i] = -1;

                Courses[alpha].done = 1;
                sleep(1);
                break;
            }
            else
            {
                for (int i = 0; i < num_students; i++)
                    seekers[i] = -1;

                sleep(1);
                continue;
            }
        }

        for (int i = 0; i < num_students; i++)
            seekers[i] = -1;

        sleep(1);
    }

    // printf(RED "Course %s has ended\n" RESET, Courses[alpha].name);
   /// pthread_mutex_destroy(&Courses[alpha].course_mutex);
   // pthread_cond_destroy(&Courses[alpha].course_condition);
}

int main()
{
    init_input();
    srand(time(NULL));
    pthread_t time_thread, students_thread[num_students], courses_thread[num_courses];

    pthread_mutex_init(&time_mutex, NULL);

    pthread_cond_init(&time_condition, NULL);

    if (pthread_create(&time_thread, NULL, &time_increment, NULL) != 0)
    {
        perror("Time increment thread failed to create");
        return 1;
    }
    for (int i = 0; i < num_students; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;

        if (pthread_create(&students_thread[i], NULL, &student_function, (void *)a) != 0)
        {
            perror("Student thread failed to create");
            return 1;
        }
    }

    for (int i = 0; i < num_courses; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;

        if (pthread_create(&courses_thread[i], NULL, &course_function, (void *)a) != 0)
        {
            perror("course thread failed to create");
            return 1;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////=== join

    // if (pthread_join(time_thread, NULL) != 0)
    // {
    //     return 1;
    // }

    for (int i = 0; i < num_students; i++)
    {
        if (pthread_join(students_thread[i], NULL) != 0)
        {
            return 1;
        }
    }
    // for (int i = 0; i < num_courses; i++)
    // {
    //     if (pthread_join(courses_thread[i], NULL) != 0)
    //     {
    //         return 1;
    //     }
    // }

    pthread_mutex_destroy(&time_mutex);

    pthread_cond_destroy(&time_condition);

    return 0;
}
//###########FILE CHANGE ./main_folder/DIVYANSH TIWARI_305914_assignsubmission_file_/q1/headers.h ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

typedef struct course
{
    char name[100];
    double interest_quotient;
    int max_seats_allowed;
    int num_labs;
    int lab_ids[100];
    pthread_mutex_t course_mutex;
    pthread_cond_t course_condition;

    int done;
    int curr_lab_id;
    int curr_ta_id;

} course;

typedef struct student
{
    // necessary
    double calibre_quotient;
    int pref_one;
    int pref_two;
    int pref_three;
    int time;

    // flags
    int status;
    int permanent; // stores the course index if finalized
    int status_change;
    // char curr_course_name[100];
    int curr_pref;
    int curr_pref_number;

    pthread_mutex_t student_mutex;
    pthread_cond_t student_condition;
} student;

typedef struct lab
{
    // necessary
    char name_lab[100];
    int num_tas;
    int max_tuts;

    int tas_working_status[100];
    int taships_completed[100];

} lab;

int num_students, num_labs, num_courses, curr_time = 0;
int num_students_waiting[100] = {0};

course *Courses;
student *Students;
lab *Labs;

pthread_mutex_t time_mutex;
pthread_cond_t time_condition;