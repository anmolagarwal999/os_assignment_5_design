
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q1/student.c ####################//

#include "header.h"
float random;
void* student(){
    for(int i=0;i<num_students;i++){
        printf("Student %d has filled in his preference for course registration\n",i);
        sleep(students[i].fillTime);
        for(int j=0;j<num_courses;j++){
            random = (rand()%100)/100.0;
        }
    }
}
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q1/main.c ####################//

#include "header.h"

void printData(){
    for(int i=0;i<num_courses;i++){
        printf("%s %.2f %d %d",courses[i].name,courses[i].interest,courses[i].slots,courses[i].totalLabs);
        for(int j=0;j<courses[0].totalLabs;j++){
            printf(" %d",courses[0].LabIDs[j]);
        }
        printf("\n");
    }
    for(int i=0;i<num_students;i++){
        printf("%.2f %d %d %d %d\n",students[i].calibre,students[i].preference[0],students[i].preference[1],students[i].preference[2],students[i].fillTime);
    }
    for(int i=0;i<num_labs;i++){
        printf("%s %d %d\n",Labs[i].name,Labs[i].TA,Labs[i].limit);
        for(int j=0;j<Labs[i].TA;j++){
            Labs[i].chanceLeft[j]=Labs[i].limit;
        }
    }
}

int main(){
    scanf("%d %d %d",&num_students,&num_labs,&num_courses);
    courses = (Course*)malloc(num_courses*sizeof(Course));
    for(int i=0;i<num_courses;i++){
        scanf("%s %f %d %d",courses[i].name,&courses[i].interest,&courses[i].slots,&courses[i].totalLabs);
        for(int j=0;j<courses[i].totalLabs;j++){
            scanf("%d",&courses[i].LabIDs[j]);
        }
    }
    students = (Student*)malloc(num_students*sizeof(Student));
    for(int i=0;i<num_students;i++){
        scanf("%f %d %d %d %d",&students[i].calibre,&students[i].preference[0],&students[i].preference[1],&students[i].preference[2],&students[i].fillTime);
    }
    Labs = (Lab*)malloc(num_labs*sizeof(Lab));
    for(int i=0;i<num_labs;i++){
        scanf("%s %d %d",Labs[i].name,&Labs[i].TA,&Labs[i].limit);
    }
    printData();
    pthread_t tid;
    pthread_create(&tid,NULL,student,NULL);
    pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q1/header.h ####################//

#ifndef __PORTAL
#define __PORTAL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct{
    float calibre;
    int fillTime;
    int preference[3];
}Student;

Student* students;

typedef struct{
    char name[50];
    int TA;
    int limit; //number of times a member of lab can TA in a course
    int chanceLeft[100];
}Lab;

Lab* Labs;

typedef struct{
    char name[50];
    float interest;
    int slots; //number of slots which can be allocated by a TA
    int totalLabs;
    int LabIDs[100];
}Course;

Course* courses;

int num_students;
int num_labs;
int num_courses;

void* student();

#endif