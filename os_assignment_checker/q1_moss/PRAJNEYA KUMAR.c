
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/student.c ####################//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include "main.h"
#include "student.h"

void wait_for_slot(Student * s) {
    while(s->curr_pref!=-1) {
    	
        for(int i = 0; i < C; i++) {
            pthread_mutex_lock(&courses[i]->mutex);

            if(s->curr_alloc == -1 && courses[i]->d > 0 && courses[i]->ta_allocated >= 0 && courses[i]->uid == s->curr_pref) {
                pthread_mutex_lock(&s->mutex);
                s->curr_alloc = courses[i]->uid;
                pthread_mutex_unlock(&s->mutex);

                // EVENT 2
                printf(ANSI_COLOR_GREEN "Student %d has been allocated a seat in course %s\n" ANSI_COLOR_RESET, s->uid, courses[i]->name);

                courses[i]->d--;

                pthread_mutex_unlock(&courses[i]->mutex);
                break;
            }

            pthread_mutex_unlock(&courses[i]->mutex);
        }

        if(s->curr_pref != -1 && courses[s->curr_pref]->courseValid <= 0){
        	// EVENT 3
		    printf(ANSI_COLOR_YELLOW "Student %d has withdrawn from course %s\n" ANSI_COLOR_RESET, s->uid, courses[s->curr_pref]->name);
		    pthread_mutex_lock(&s->mutex);
			if(s->curr_pref==s->pref_one){
    			s->curr_alloc = -1;
    			s->curr_pref = s->pref_two;
    			// EVENT 4
    			printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_COLOR_RESET, s->uid, courses[s->pref_one]->name, courses[s->pref_two]->name);
    		}
    		else if(s->curr_pref==s->pref_two){
    			s->curr_alloc = -1;
    			s->curr_pref = s->pref_three;
    			// EVENT 4
    			printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_COLOR_RESET, s->uid, courses[s->pref_two]->name, courses[s->pref_three]->name);
    		}
    		else{
    			// EVENT 6
    			printf(ANSI_COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" ANSI_COLOR_RESET, s->uid);
    			s->curr_pref = -1;
    		}
    		pthread_mutex_unlock(&s->mutex);
        }
        
    }

    return;
}

void * studentRunner(void * a) {
    Student * s = (Student *)a;

    sleep(s->arr_time);
    // EVENT 1
    printf(ANSI_COLOR_RED "Student %d has filled in preferences for course registration\n" ANSI_COLOR_RESET, s->uid);

	wait_for_slot(s);

    return 0;
}


void initStudent(int i) {
    Student * s = (Student *) malloc(sizeof(Student));

    s->uid = i;
    s->finalised = 0;
    s->curr_alloc = -1;

    scanf("%lf %d %d %d %d", &s->callibre, &s->pref_one, &s->pref_two, &s->pref_three, &s->arr_time);

    s->curr_pref = s->pref_one;

    pthread_mutex_init(&s->mutex, 0);

    students[i] = s;
}

void createStudentThreads(int i) {
	pthread_create(&student_t[i], 0, studentRunner, students[i]); 
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/lab.h ####################//

#include "main.h"

#ifndef __LAB_H
#define __LAB_H

void initLab(int i);

void createLabThreads(int i);

#endif // __LAB_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/main.c ####################//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "student.h"
#include "course.h"
#include "lab.h"

void init(){
	srand(time(0));

	courses = (Course **) malloc(sizeof(Course *) * C);
	course_t = (pthread_t *) malloc(sizeof(pthread_t) * C);

	for(int i = 0; i < C; i++){
		initCourse(i);
	}

	students = (Student **) malloc(sizeof(Student *) * S);
	student_t = (pthread_t *) malloc(sizeof(pthread_t) * S);

	for(int i = 0; i < S; i++){
		initStudent(i);
	}

	iiit_labs = (Lab **) malloc(sizeof(Lab *) * L);
	lab_t = (pthread_t *) malloc(sizeof(pthread_t) * L);

	for(int i = 0; i < L; i++){
		initLab(i);
	}

	for(int i = 0; i < S; i++){
		createStudentThreads(i);
	}

	for(int i = 0; i < C; i++){
		createCourseThreads(i);
	}

	for(int i = 0; i < L; i++){
		createLabThreads(i);
	}
}

int main(){
	scanf("%d %d %d", &S, &L, &C);
	init();

	for(int i = 0; i < S; i++)
        pthread_join(student_t[i], 0);


    bool simOver = true;

    for(int i = 0; i<S; i++){
    	if(students[i]->curr_pref != -1){
    		simOver = false;
    		break;
    	}
    }

    if(simOver){
    	printf("SIMULATION ENDING\n");
    }

    // printf("SIMULATION ENDING\n");

    return 0;

}

int randRange(int l, int r) {
    if(l > r)
        return l;

    return (rand() % (r - l + 1)) + l;
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/student.h ####################//

#include "main.h"

#ifndef __STUDENT_H
#define __STUDENT_H

void initStudent(int i);

void createStudentThreads(int i);

#endif // __STUDENT_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/course.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include "main.h"
#include "course.h"

void * courseRunner(void * a) {
    Course * t = (Course *)a;

    while(1){
    	pthread_mutex_lock(&t->mutex);

    	for(int k = 0; k < t->p; k++){
    		Lab * l = iiit_labs[t->course_labs[k]];
    		pthread_mutex_lock(&l->mutex);
    		for(int i = 0; i < l->n_tas; i++){
	          // Check if TAs are available
	          if(l->availability[i] == 1 && l->curr_times[i] < l->max_times){
	            // if TA is not allocated
	            if(t->ta_allocated == -1){


	              // BONUS
	              bool equalOpp = true;
	              for(int j = 0; j < l->n_tas; j++){
	              	if(j!=i){
	              		if(l->curr_times[i]-l->curr_times[j]>0){
	              			equalOpp = false;
	              		}
	              	}
	              }

	              if(!equalOpp){
	              	pthread_mutex_lock(&l->mutex);
	              	break;
	              }

	              t->ta_allocated = i;
	              t->lab_allocated = l->uid;

	              //suffix for TASHIP, EVENT 11
	              if(l->curr_times[i] == 0){
	                printf(ANSI_COLOR_BYELLOW "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" ANSI_COLOR_RESET, i, l->name, t->name, l->curr_times[i]+1);
	              }
	              else if(l->curr_times[i] == 1){
	                printf(ANSI_COLOR_BYELLOW "TA %d from lab %s has been allocated to course %s for his %dnd TA ship\n" ANSI_COLOR_RESET, i, l->name, t->name, l->curr_times[i]+1);
	              }
	              else if(l->curr_times[i] == 2){
	                printf(ANSI_COLOR_BYELLOW "TA %d from lab %s has been allocated to course %s for his %drd TA ship\n" ANSI_COLOR_RESET, i, l->name, t->name, l->curr_times[i]+1);
	              }
	              else{
	                printf(ANSI_COLOR_BYELLOW "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" ANSI_COLOR_RESET, i, l->name, t->name, l->curr_times[i]+1);
	              }
	              l->availability[i] = 0;
	              l->curr_times[i]++;
	              break;
	            }
	          }
	        }
    		pthread_mutex_unlock(&l->mutex);
    	}


        if(t->ta_allocated >= 0 && t->tutorial == 0){
        	t->d = randRange(1, t->course_max_slots);
        	t->tutorial = 1;
        	t->tut_seats = t->d;
        	//EVENT 7
        	printf(ANSI_COLOR_WHITE "Course %s has been allotted %d seats\n" ANSI_COLOR_RESET, t->name, t->d);
        }

        if(t->tutorial){
        	int w = 0;
	        for(int i = 0; i < S; i++){
	        	if(students[i]->curr_pref == t->uid){
	        		w++;
	        	}
	        }

	        if(w!=0 && (w+t->d == t->tut_seats || t->d == 0)){
	        	// EVENT 8
	        	printf(ANSI_COLOR_GREY "Tutorial has started for %s with %d seats filled out of %d\n" ANSI_COLOR_RESET, t->name, t->tut_seats - t->d, t->tut_seats);
	        	// Sleep for 5 seconds for tutorial
		        sleep(5);

		        // end tutorial
		        t->tutorial = 0;
		        t->d = 0;
		        t->tut_seats = 0;

		        // TA exits
		        // EVENT 9
	        	printf(ANSI_COLOR_BRED "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_COLOR_RESET, t->ta_allocated, iiit_labs[t->lab_allocated]->name, t->name);
	    		t->ta_allocated = -1;
		        t->lab_allocated = -1;

	        	for(int i = 0; i < S; i++){
		        	if(students[i]->curr_pref >=0 && students[i]->curr_alloc == t->uid){
		        		Student * s = students[i];

		        		pthread_mutex_lock(&s->mutex);
						double probability = s->callibre * t->interest;

						bool finalises = (rand() % 100) < (probability * 100);

						if(finalises){
							// EVENT 5
				    		printf(ANSI_COLOR_MAGENTA "Student %d has selected %s permanently\n" ANSI_COLOR_RESET, s->uid, t->name);
				    		s->curr_pref = -1;
						}
						else{
							// EVENT 3
				    		printf(ANSI_COLOR_YELLOW "Student %d has withdrawn from course %s\n" ANSI_COLOR_RESET, s->uid, t->name);

				    		if(s->curr_pref==s->pref_one){
				    			s->curr_alloc = -1;
				    			s->curr_pref = s->pref_two;
				    			// EVENT 4
				    			printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_COLOR_RESET, s->uid, courses[s->pref_one]->name, courses[s->pref_two]->name);
				    		}
				    		else if(s->curr_pref==s->pref_two){
				    			s->curr_alloc = -1;
				    			s->curr_pref = s->pref_three;
				    			// EVENT 4
				    			printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_COLOR_RESET, s->uid, courses[s->pref_two]->name, courses[s->pref_three]->name);
				    		}
				    		else{
				    			// EVENT 6
				    			printf(ANSI_COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" ANSI_COLOR_RESET, s->uid);
				    			s->curr_pref = -1;
				    		}
						}

		        		pthread_mutex_unlock(&s->mutex);
		        	}
		        }

	        } 
        }

        if(t->courseValid>0){
	        t->courseValid = 0;

	        for(int k = 0; k < t->p; k++){
	          if(iiit_labs[t->course_labs[k]]->ta_worthy!=2){
	          	t->courseValid = 1;
	          }
	        }
	    }

        if(t->courseValid <= 0){
        	if(t->courseValid == 0){
        		t->courseValid = -1;
		    	//EVENT 10
		    	printf(ANSI_COLOR_BGREEN "Course %s doesn't have any TA's eligible and is removed from course offerings\n" ANSI_COLOR_RESET, t->name);
			}
        }

        pthread_mutex_unlock(&t->mutex);
    }

    return 0;
}

void initCourse(int i) {
    Course * t = (Course *) malloc(sizeof(Course));

    t->uid = i;
    t->ta_allocated = -1;
    t->d = 0;
    t->tutorial = 0;
    t->tut_seats = 0;
    t->lab_allocated = -1;
    t->courseValid = 1;

   	scanf("%s %lf %d %d", t->name, &t->interest, &t->course_max_slots, &t->p);

   	for(int j = 0; j < t->p; j++){
   		scanf("%d", &t->course_labs[j]);
   	}

    pthread_mutex_init(&t->mutex, 0);

    courses[i] = t;
}

void createCourseThreads(int i) {
	pthread_create(&course_t[i], 0, courseRunner, courses[i]); 
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/course.h ####################//

#include "main.h"

#ifndef __COURSE_H
#define __COURSE_H

void initCourse(int i);

void createCourseThreads(int i);

#endif // __COURSE_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/main.h ####################//

#include <pthread.h>

#ifndef __MAIN_H
#define __MAIN_H

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_GREY    "\x1b[38m"
#define ANSI_COLOR_BRED    "\x1b[39m"
#define ANSI_COLOR_BGREEN  "\x1b[40m"
#define ANSI_COLOR_BYELLOW "\x1b[41m"
#define ANSI_COLOR_BBLUE   "\x1b[42m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct Student {
    int uid;

    double callibre;
    int pref_one;
    int pref_two;
    int pref_three;
    int arr_time;

    int finalised;
    int curr_pref;
    int curr_alloc;

    pthread_mutex_t mutex;
} Student;

typedef struct Course {
    int uid;

   	char name[50];
   	double interest;
   	int course_max_slots; 
   	int p;
   	int course_labs[50];

   	int d;
   	int ta_allocated;
    int lab_allocated;
    int tutorial;
    int tut_seats;
    int courseValid;

    pthread_mutex_t mutex;
} Course;

typedef struct Lab {
    int uid;

   	char name[50];
   	int n_tas;
   	int max_times; 

   	int curr_times[50];
   	int availability[50];
    int ta_worthy;

    pthread_mutex_t mutex;
} Lab;

Student ** students;
Course ** courses;
Lab ** iiit_labs;

pthread_t * student_t,
			    * course_t,
			    * lab_t;

int S, // number of students
    L, // number of labs
    C; // number of courses

int randRange(int l, int r);

#endif // __MAIN_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q1/lab.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "lab.h"

void * labRunner(void * a) {
    Lab * l = (Lab *)a;
    while(l->ta_worthy!=2) {
      pthread_mutex_lock(&l->mutex);
      l->ta_worthy = 0;
      
      for(int i = 0; i < l->n_tas; i++){
        if(l->curr_times[i] < l->max_times){
          l->ta_worthy = 1;
        }
      }

      // EVENT 12
      if(!l->ta_worthy){
        l->ta_worthy = 2;
        printf(ANSI_COLOR_BBLUE "Lab %s no longer has students available for TA ship\n" ANSI_COLOR_RESET, l->name);
      }
      pthread_mutex_unlock(&l->mutex);
    }

    return 0;
}

void initLab(int i) {
    Lab * l = (Lab *) malloc(sizeof(Lab));

    l->uid = i;
    l->ta_worthy = 0;

   	scanf("%s %d %d", l->name, &l->n_tas, &l->max_times);

    for(int j = 0; j < l->n_tas; j++){
      l->curr_times[j] = 0;
      l->availability[j] = 1;
    }

    pthread_mutex_init(&l->mutex, 0);

    iiit_labs[i] = l;
}

void createLabThreads(int i) {
	pthread_create(&lab_t[i], 0, labRunner, iiit_labs[i]); 
}