
//###########FILE CHANGE ./main_folder/Debanil Chowdhury_305935_assignsubmission_file_/2021121001/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct TA
{
    int id;
} TA;

typedef struct Lab
{
    //char *name;
    char name[1024];
    int id;
    int n_i;      // number of TA
    int ta_limit; // number of times TA can teach
    TA *ta_list;  // list of TA
} Lab;

typedef struct Course
{
    char *name;
    int id;
    float interest;      // interest in course
    Lab *L;              // set of lab ids
    TA current_ta;       // current ta
    int course_max_slot; // max slots
    int D;               // slots assigned by current_ta
    int numOfLabs;       // int number of labs
} Course;

typedef struct Student
{
    int id;
    Course course_list[3]; // preference list
    int t;                 // time taken by student to fill pref
    Course current_pref;
    float prob[3]; // probability of dropping course
    float calibre; // calibre
    bool rejectCourse[3];
} Student;

int num_labs;          // number of labs
int num_students;      // number of students
int num_courses;       // number of courses
Course *course_list;   // list of courses
Student *student_list; // list of student
Lab *lab_list;         // list of labs

Course getCourseDetails(int id)
{
    Course C;
    C.id = id;
    char *str;
    size_t size = 10;
    str = (char *)malloc(size);
    getline(&str, &size, stdin);
    char *token;
    token = strtok(str, " \t");
    C.name = (char *)malloc(strlen(token));
    sscanf(token, "%s", C.name);
    token = strtok(NULL, " \t");
    sscanf(token, "%f", &C.interest);
    token = strtok(NULL, " \t");
    sscanf(token, "%d", &C.course_max_slot);
    token = strtok(NULL, " \t");
    sscanf(token, "%d", &C.numOfLabs);
    C.L = (Lab *)malloc(C.numOfLabs * sizeof(Lab));
    for (int i = 0; i < C.numOfLabs; i++)
    {
        token = strtok(NULL, " \t");
        sscanf(token, "%d", &C.L[i].id);
    }
    return C;
}

Student getStudentDetails(int id)
{
    Student S;
    S.id = id;
    int Pref[3];
    scanf("%f %d %d %d %d", &S.calibre, &Pref[0], &Pref[1], &Pref[2], &S.t);
    for (int i = 0; i < 3; i++)
    {
        S.course_list[i] = course_list[Pref[i]];
    }
    for (int i = 0; i < 3; i++)
    {
        S.prob[i] = S.calibre * S.course_list[i].interest;
    }
    for (int i = 0; i < 3; i++)
    {
        if (rand() <= S.prob[i] * RAND_MAX)
        {
            S.rejectCourse[i] = false;
        }
        else
        {
            S.rejectCourse[i] = true;
        }
    }
    return S;
}

Lab getLabDetails(int id)
{
    Lab L;
    L.id = id;
    scanf("%s %d %d", L.name, &L.n_i, &L.ta_limit);
    /*char name[1024];
    scanf("%s %d %d", name, &L.n_i, &L.ta_limit);
    L.name = (char *)malloc((sizeof(char) * strlen(name)) + 1);
    (L.name)[0] = '\0';
    strcat(L.name, name);*/
    /*
    char *str;
    size_t size = 10;
    str = (char *)malloc(size);
    getline(&str, &size, stdin);
    char *token;
    token = strtok(str, " \t");
    L.name = (char *)malloc(strlen(token));
    sscanf(token, "%s", L.name);
    token = strtok(NULL, " \t");
    sscanf(token, "%d", &L.n_i);
    token = strtok(NULL, " \t");
    sscanf(token, "%d", L.ta_limit);*/
    return L;
}

void inputfirstline()
{
    char *str;
    size_t size = 10;
    str = (char *)malloc(size);
    getline(&str, &size, stdin);
    sscanf(str, "%d %d %d", &num_students, &num_labs, &num_courses);
}

int main()
{
    inputfirstline();
    course_list = (Course *)malloc(num_courses * sizeof(Course));
    student_list = (Student *)malloc(num_courses * sizeof(Student));
    lab_list = (Lab *)malloc(num_courses * sizeof(Lab));
    for (int i = 0; i < num_courses; i++)
    {
        course_list[i] = getCourseDetails(i);
    }
    for (int i = 0; i < num_students; i++)
    {
        student_list[i] = getStudentDetails(i);
    }
    for (int i = 0; i < num_labs; i++)
    {
        lab_list[i] = getLabDetails(i);
    }
    // test
    for (int i = 0; i < num_courses; i++)
    {
        printf("%s %f %d %d ", course_list[i].name, course_list[i].interest, course_list[i].course_max_slot, course_list[i].numOfLabs);
        for (int j = 0; j < course_list[i].numOfLabs; j++)
        {
            printf("%d ", course_list[i].L[j]);
        }
        printf("\n");
    }
    for (int i = 0; i < num_students; i++)
    {
        printf("%f %d %d %d %d ", student_list[i].calibre, student_list[i].course_list[0].id, student_list[i].course_list[1].id, student_list[i].course_list[2].id, student_list[i].t);
        printf("\n");
    }
    for (int i = 0; i < num_labs; i++)
    {
        printf("%s %d %d ", lab_list[i].name, lab_list[i].n_i, lab_list[i].ta_limit);
        printf("\n");
    }

    return 0;
}