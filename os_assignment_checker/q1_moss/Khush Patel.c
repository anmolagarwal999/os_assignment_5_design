
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/student.c ####################//

#include "student.h" 
#include "utils.h"
#include "course.h"

void *StudentStimulater(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    int time = StudentList[id].time;
    student_PreferenceFilling(id); // filling preferences at alloted time
    while (1)
    {   
        sleep(2);
        student_CourseApplication(id); // applying for each course per preference   (locks used here)
        sleep(3);
        
        //printf("id:%d l:%lld\n",id,StudentList[id].existing);
        if(StudentList[id].existing==0){
            break;
        }

        student_CourseSelection(id); // selecting course based on prob

        if (student_Removed(id)){
            break;
        }     
        // if(coursesOver == num_courses){
        //     break;
        // } 
    }
    return NULL;
}

int student_Removed(ll id)
{
    if (StudentList[id].existing == 0)
    {
        return 1;
    }
    return 0;
}

void student_PreferenceFilling(ll id)
{
    int time = StudentList[id].time;
    pthread_mutex_lock(&time_lock[time]);
    while (timer != time)
    {
            pthread_cond_wait(&timeC[time], &time_lock[time]);
    }
    pthread_mutex_lock(&time_lock[time]);
    StudentList[id].existing = 1; // student has filled preferences, and hence it is in simulation
    printf(YELLOW"Student %lld has filled in preferences for course registration\n"RESET, id);
}

void student_CourseApplication(ll id)
{
    //printf("ENTERED student_CourseApplication for id : %lld\n",id);
    ll courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];
    ll nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
    ll pos = StudentList[id].prefPos;
    StudentList[id].isLearning = 0;
L2:
    if (CourseList[courseWantedID].isExisting == 1) // to check if course is existing
    {    
        //printf("C:%lld id:%lld\n",CourseList[courseWantedID].isStarting,id);
        if (CourseList[courseWantedID].isStarting == 1) // to confirm the tutorial hasnt started
        {
            pthread_mutex_lock(&student_CourseApplication_lock[courseWantedID]);
            if (CourseList[courseWantedID].currentStudentSize < CourseList[courseWantedID].D) // to see if capacity is not filled yet
            {
                CourseList[courseWantedID].currentStudentSize++;
                StudentList[id].isLearning = 1;
                printf(MAGENTA"Student %lld has been allocated a seat in course %s\n"RESET, id, CourseList[courseWantedID].courseName);
            }
            pthread_mutex_unlock(&student_CourseApplication_lock[courseWantedID]);
        }
        else // if tut is ongoing at the moment when student applies
        {
            pthread_mutex_lock(&mutex_lock2[courseWantedID]);

            while (CourseList[courseWantedID].isStarting == 0){
                if(coursesOver==num_courses){
                    goto L1;
                }
                pthread_cond_wait(&c2[courseWantedID], &mutex_lock2[courseWantedID]);
            }
                

            //printf("GOTTTT signal for course StudID : %lld prefPos : %lld\n", id, StudentList[id].prefPos);

            if (course_Removed(courseWantedID))
            {
                //printf("Course Removed : %lld for studID %lld\n", courseWantedID, id);
                goto L1;
            }

            if (CourseList[courseWantedID].isExisting == 1) // to check if course is existing
            {
                if (CourseList[courseWantedID].isStarting == 1) // to confirm the tutorial hasnt started
                {
                    pthread_mutex_lock(&student_CourseApplication_lock[courseWantedID]);
                    if (CourseList[courseWantedID].currentStudentSize < CourseList[courseWantedID].D) // to see if capacity is not filled yet
                    {
                        CourseList[courseWantedID].currentStudentSize++;
                        StudentList[id].isLearning = 1;
                        printf(MAGENTA"Student %lld has been allocated a seat in course %s\n"RESET, id, CourseList[courseWantedID].courseName);
                    }
                    pthread_mutex_unlock(&student_CourseApplication_lock[courseWantedID]);
                }
            }
            pthread_mutex_unlock(&mutex_lock2[courseWantedID]);
        }
    }
    else // if course is not existing
    {
    L1:
        if (StudentList[id].prefPos == 2) // to check if preferences are left
        {
            StudentList[id].isLearning = 1;
            StudentList[id].existing = 0; // since no preferences left, student is out of simulation
            studentsOver++;
            courseInterest[courseWantedID]--;
            printf(RED"Student %lld couldn’t get any of his preferred courses\n"GREEN, id);
        }
        else
        {
            printf(YELLOW"Student %lld has changed current preference from %s (priority %lld) to %s (priority %lld)\n"RESET, id, CourseList[courseWantedID].courseName, pos, CourseList[nextCourseWantedID].courseName, pos + 1);
            StudentList[id].prefPos++;
            courseInterest[courseWantedID]--;
            courseInterest[nextCourseWantedID]++;
            courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];
            nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
            StudentList[id].isLearning = 0;
            goto L2;
        }
    }

    if (StudentList[id].isLearning == 0)
    {
        if(coursesOver==num_courses){
            ;
        }else{
            goto L2;
        }
        
    }
    //printf("EXITED student_CourseApplication for id : %lld\n",id);
}

void student_CourseSelection(ll id) // after Tut is over, the student selects course based on probability
{
    //printf("ENTERED student_CourseSelection for id : %lld\n",id);
    ll courseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos];

    pthread_mutex_lock(&mutex_lock[courseWantedID]);
    //printf("fu id : %lld wantedID:%lld \n",id,courseWantedID);
    while (CourseList[courseWantedID].isOn == 1)
    {
        pthread_cond_wait(&c[courseWantedID], &mutex_lock[courseWantedID]);
    }
    pthread_mutex_unlock(&mutex_lock[courseWantedID]);
    //printf("fu1 id : %lld wantedID:%lld ",id,courseWantedID);

    float prob;
    ll nextCourseWantedID = StudentList[id].courseIDs[StudentList[id].prefPos + 1];
    ll pos = StudentList[id].prefPos;
    //printf("StudID : %lld prefPos : %lld CourseWanted Id : %lld next : %lld \n", id, pos, courseWantedID, nextCourseWantedID);
    prob = StudentList[id].calibre * CourseList[courseWantedID].interestQ;
    prob = prob * 100;
    float p1 = (rand() % 100);
    //printf("prob :%f id : %lld\n", prob, id);

    if (p1<prob) // to check if interest is enuf
    {
        studentsOver++;
        StudentList[id].existing = 0; // student is out of simulation since he has selected his prefered course
        printf(GREEN"Student %lld has selected course %s permanently\n"RESET, id, CourseList[courseWantedID].courseName);
    }
    else // if not selected, preferences updated
    {
        if (StudentList[id].existing == 1)
        {
            if (StudentList[id].prefPos == 2) // to check if preferences are left
            {
                StudentList[id].existing = 0; // since no preferences left, student is out of simulation
                courseInterest[courseWantedID]--;
                studentsOver++;
                printf(RED"Student %lld couldn’t get any of his preferred courses\n"RESET, id);
            }
            else
            {

                printf(YELLOW"Student %lld has changed current preference from %s (priority %lld) to %s (priority %lld)\n"RESET, id, CourseList[courseWantedID].courseName, pos, CourseList[nextCourseWantedID].courseName, pos + 1);
                StudentList[id].prefPos++;
                courseInterest[courseWantedID]--;
                courseInterest[nextCourseWantedID]++;
            }
        }
    }
    //printf("EXITED student_CourseSelection for id : %lld\n",id);
}

//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "utils.h"
#include "course.h"
#include "student.h"

void input()
{
    scanf("%lld %lld %lld", &num_student, &num_labs, &num_courses);

    // taking in CourseList
    for (int i = 0; i < num_courses; i++)
    {
        scanf("%s %f %lld %lld", CourseList[i].courseName, &CourseList[i].interestQ, &CourseList[i].course_max_slots, &CourseList[i].p);
        for (j = 0; j < CourseList[i].p; j++)
        {
            scanf("%lld", &CourseList[i].listp[j]);
        }
        CourseList[i].currentStudentSize = 0;
        CourseList[i].isOn = 0;
        CourseList[i].isExisting = 1;
        CourseList[i].TA_Allocated = -1;
        CourseList[i].isStarting=0;
    }

    // taking in StudentList
    for (int i = 0; i < num_student; i++)
    {
        scanf("%f %lld %lld %lld %f", &StudentList[i].calibre, &StudentList[i].courseIDs[0], &StudentList[i].courseIDs[1], &StudentList[i].courseIDs[2], &StudentList[i].time);
        courseInterest[StudentList[i].courseIDs[0]]++;
        StudentList[i].isLearning = 0;
        StudentList[i].prefPos = 0;
        StudentList[i].existing = 1;
    }

    // taking in LabList
    for (int i = 0; i < num_labs; i++)
    {
        scanf("%s %lld %lld", LabList[i].labName, &LabList[i].noOfTAs, &LabList[i].max);
        memset(LabList[i].courseDone, 0, sizeof(LabList[i].courseDone));
        memset(LabList[i].busy, 0, sizeof(LabList[i].busy));
        LabList[i].busy[i] = 0;
    }
}

void print()
{
    for (int i = 0; i < num_student; i++)
    {
        printf("%f %lld %lld %lld %f\n", StudentList[i].calibre, StudentList[i].courseIDs[StudentList[i].prefPos], StudentList[i].courseIDs[StudentList[i].prefPos + 1], StudentList[i].courseIDs[StudentList[i].prefPos + 2], StudentList[i].time);
    }
    for (int i = 0; i < num_labs; i++)
    {
        printf("%s %lld %lld", LabList[i].labName, LabList[i].noOfTAs, LabList[i].max);
    }
    for (int i = 0; i < num_courses; i++)
    {
        printf("\n");
        printf("%s %f %lld %lld ", CourseList[i].courseName, CourseList[i].interestQ, CourseList[i].course_max_slots, CourseList[i].p);
        for (j = 0; j < CourseList[i].p; j++)
        {
            printf("%lld ", CourseList[i].listp[j]);
        }
        printf("\n");
        CourseList[i].currentStudentSize = 0;
        CourseList[i].isOn = 0;
        CourseList[i].isExisting = 1;
    }
}

void *sleepingFunc(void *arg)
{
    timer = 0;
    while (1)
    {
        pthread_mutex_lock(&time_lock[timer + 1]);
        sleep(1);
        timer++;
        pthread_cond_broadcast(&timeC[timer]);
        pthread_mutex_lock(&time_lock[timer + 1]);
    }
}

int main()
{
    // pthread_mutex_init(&course_TA_Allocation_lock, NULL);
    // pthread_mutex_init(&student_CourseApplication_lock, NULL);
    studentsOver=0;
    coursesOver =0;
    memset(courseInterest, 0, sizeof(courseInterest));
    srand(time(0));
    input();
    printf("\n");

    pthread_create(&sleeper, NULL, sleepingFunc, NULL);
    
// -------------------- -------------------- -------------------- -------------------- -------------------- -------------------- --------------------

    // Creating threads
    for (int l = 0;  l < num_student; l++)
    { 
        pthread_t curr_tid;
        struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
        thread_input->id = l;
        pthread_create(&curr_tid, NULL, StudentStimulater, (void *)(thread_input));
        studThreadArr[l] = curr_tid;
    }

    for (int l = 0; l < num_courses; l++)
    {
     pthread_t curr_tid;
    struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
    thread_input->id = l;
    pthread_create(&curr_tid, NULL, courseStimulater, (void *)(thread_input));
    courseThreadArr[l] = curr_tid;
    }

    
// -------------------- -------------------- -------------------- -------------------- -------------------- -------------------- --------------------
    for (int l = 0; l < num_courses; l++)
    {
        pthread_join(courseThreadArr[l], NULL);
    }
    for (int l = 0; l < num_student; l++)
    {
        pthread_join(studThreadArr[l], NULL);
    }
}
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/student.h ####################//

#ifndef student
#define student

void *StudentStimulater(void *args);
void student_CourseSelection(long long int id);
void student_CourseApplication(long long int id);
void student_PreferenceFilling(long long int id);
int student_Removed(long long int id);

#endif
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/course.c ####################//

#include "course.h"
#include <string.h>
#include "utils.h"

void *courseStimulater(void *arg)
{
    ll id = ((struct thread_details *)arg)->id;
    turns[id] = 0;
    sleep(1);
    while (1)
    {
        //printf("TURN :%d id : %lld\n", turns[id], id);

        course_TA_Allocation(id); // allocating TA for current tut batch (used lock and conditional lock here coz we r accessing common resource)
        if (CourseList[id].TA_Allocated == -1)
        {
            //printf("bye id : %lld\n", id);
            coursesOver++;
            break;
        }
        course_size_Allocation(id); //  allocating size of current tut batch

        sleep(3); // waiting for students to sign up

        course_start(id); // starting tut

        sleep(1); // tut duration

        tut_over(id); // to end tut
        //printf("Students over : %lld\n", studentsOver);
        if (course_Removed(id))
        {
            pthread_mutex_lock(&mutex_lock2[id]);
            coursesOver++;
            printf(RED"Course %s doesn’t have any TA’s eligible and is removed from course offerings\n"RESET, CourseList[id].courseName);
            CourseList[id].isStarting=0;
            pthread_cond_broadcast(&c2[id]);
            pthread_mutex_unlock(&mutex_lock2[id]);
            break; // to check if the TAs are over for this course
        }
        if (studentsOver == num_student)
        {
            //printf("breaking over students are over\n");
            break;
        }

        turns[id]++;
    }
    return NULL;
}

void position_giver(ll pos)
{
    if (pos == 1)
    {
        strcpy(position, "1st");
    }
    if (pos == 2)
    {
        strcpy(position, "2nd");
    }
    if (pos == 3)
    {
        strcpy(position, "3rd");
    }
    if (pos >= 4)
    {
        char str[100], random[100];
        strcpy(random, "th");
        sprintf(str, "%lld", pos);
        strcat(str, random);
        strcpy(position, str);
    }
}

void course_size_Allocation(ll id)
{
    //printf("ENTERED course_size_Allocation for id : %lld\n",id);
    pthread_mutex_lock(&mutex_lock2[id]);
    CourseList[id].isOn = 1;
    CourseList[id].isStarting = 1;
    
    CourseList[id].D = (rand() % CourseList[id].course_max_slots) + 1;
    printf(BLUE"Course %s has been allocated %lld seats\n"RESET, CourseList[id].courseName, CourseList[id].D);
    pthread_cond_broadcast(&c2[id]);
    pthread_mutex_unlock(&mutex_lock2[id]);
    //printf("EXITED course_size_Allocation for id : %lld\n",id);
}

void course_TA_Allocation(ll id)
{
    
    //printf("ENTERED course_TA_Allocation for id : %lld\n",id);
    for (int f = 0; f < CourseList[id].p; f++)
    {
        ll labID = CourseList[id].listp[f];
        for (ll j = 0; j < LabList[labID].noOfTAs; j++)
        {
            pthread_mutex_lock(&lab_lock[id]);
            if (CourseList[id].TA_Allocated == -1)
            { //printf("H1 ");
                if (LabList[labID].courseDone[j] < LabList[labID].max)
                { //printf("H2 ");
                    //printf("LabList[%lld].busy[%lld]:%lld",labID,j,LabList[labID].busy[j]);
                    if (LabList[labID].busy[j] == 0)
                    {
                        //printf("H3 \n");
                        LabList[labID].courseDone[j]++;
                        LabList[labID].busy[j] = 1;

                        CourseList[id].labID_Allocated = labID;
                        CourseList[id].TA_Allocated = j;
                        //printf("J : %lld",j);
                        position_giver(LabList[labID].courseDone[j]);
                        printf(CYAN"TA %lld from lab %s has been allocated to course %s for his %s TA ship\n"RESET, j, LabList[labID].labName, CourseList[id].courseName, position);
                    }
                }
            }

            pthread_mutex_unlock(&lab_lock[id]);
        }
    }
   // printf("EXITED course_TA_Allocation for id : %lld\n",id);
    
}

void course_start(ll id)
{   
    CourseList[id].isStarting = 0;
    int flag=0;
    //printf("ENTERED course_start for id : %lld\n",id);
    //printf("hhh id : %lld over:%lld cinterest : %lld\n", id, studentsOver, courseInterest[id] );
    //while (CourseList[id].currentStudentSize == 0)
    // {
    //     sleep(1);
        //printf("hhh id : %lld over:%lld cinterest : %lld\n", id, studentsOver, courseInterest[id] );
        if (studentsOver >= num_student || course_Removed(id) || courseInterest[id]==0)
        {
            flag=1;
            //break;
        }
    //}
    
    CourseList[id].isOn = 1;
    printf(GREEN"Tutorial has started for Course %s with %lld seats filled out of %lld\n"RESET, CourseList[id].courseName, CourseList[id].currentStudentSize, CourseList[id].D);
    //printf("EXITED course_start for id : %lld\n",id);
}

void tut_over(ll id)
{
    //printf("ENTERED tut_over for id : %lld\n",id);
    pthread_mutex_lock(&mutex_lock[id]);
    char labName[100];
    strcpy(labName, LabList[CourseList[id].labID_Allocated].labName);
    printf(RED"TA %lld from lab %s has completed the tutorial for course %s \n"RESET, CourseList[id].TA_Allocated, labName, CourseList[id].courseName);
    LabList[CourseList[id].labID_Allocated].busy[CourseList[id].TA_Allocated] = 0;
    CourseList[id].isOn = 0;
    CourseList[id].TA_Allocated = -1;
    CourseList[id].currentStudentSize = 0;
    pthread_cond_broadcast(&c[id]);
    pthread_mutex_unlock(&mutex_lock[id]);
    //printf("EXITED tut_over for id : %lld\n",id);
}

int course_Removed(ll id)
{

    int TAsOver = 1;
    for (int l = 0; l < CourseList[id].p; l++)
    {
        int labID = CourseList[id].listp[l];
        for (int f = 0; f < LabList[labID].noOfTAs; f++)
        {
            if (LabList[labID].courseDone[f] < LabList[labID].max)
            {
                TAsOver = 0;
            }
        }
    }

    if (TAsOver) // if its 1, course removed
    {
        CourseList[id].isExisting = 0;
        CourseList[id].isOn = 0;
       
        return 1;
    }
    return 0;
}
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/course.h ####################//

#ifndef course
#define course

void *courseStimulater(void *arg);
void position_giver(long long int pos);
void course_size_Allocation(long long int id);
void course_TA_Allocation(long long int id);
void course_start(long long int id);
int  course_Removed(long long int id);
void tut_over(long long int id);

#endif
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q1/utils.h ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

typedef long long int ll;
time_t startTime;
ll studentsOver;
ll coursesOver;
ll timer;

ll courseInterest[100];


pthread_t courseThreadArr[10000];   // array of threads for each course
pthread_t studThreadArr[10000];     // array of threads for each student

pthread_mutex_t course_TA_Allocation_lock[100];           // mutex lock
pthread_mutex_t lab_lock[100];  
pthread_mutex_t student_CourseApplication_lock[100];           // mutex lock

pthread_t sleeper;

pthread_mutex_t time_lock[1000];
pthread_cond_t timeC[1000];

pthread_mutex_t mutex_lock[100];
pthread_cond_t c[100];

pthread_mutex_t mutex_lock2[100];
pthread_cond_t c2[100];

int turns[100];
struct Student
{
    float calibre;
    ll courseIDs[3];
    float time; 
    ll isLearning;                  // if students is allocated for tutorial already or not
    ll prefPos;                     // preferencePosition
    ll existing;                    // to check if student is in simulation or not
};

struct Course
{
    char courseName[50];
    float interestQ;
    ll course_max_slots;
    ll p;                  // number of labs from which the course accepts TAs
    ll listp[100];         // list of lab IDs

    ll D;                  // no of slots announced for current tutorial
    ll currentStudentSize; // no of students allocated for current tutorial of course

    ll isOn;               // if Course tutorial is On/Off
    ll isStarting;
    ll isExisting;         // if Course is in simulation or not

    char courseTA[100];    // TA name for current tutorial of course
    ll labID_Allocated;
    ll TA_Allocated;        // TA id allocated for curr Tut
};

struct Lab
{
    char labName[50];
    ll noOfTAs;
    ll courseDone[100];         // courseDone[i] tells no of courses TA[i] has done
    ll busy[100];               // busy[i] tells if TA[i] is allocated for a tutorial already or not
    ll max;                     // maximum no of tutorials TA of this lab can do
};

ll num_student, num_labs, num_courses, i, j;
char position[50];
struct Course CourseList[100];
struct Lab LabList[100];
struct Student StudentList[100];

struct thread_details
{
    int id;
};

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"

#define RESET   "\x1b[0m"
