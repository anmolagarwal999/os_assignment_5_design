
//###########FILE CHANGE ./main_folder/YASHWANTH KOTTU_305851_assignsubmission_file_/a5/q1/1.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

#define MAX_TAS 20
#define MAX_STUDENTS 20
#define MAX_COURSES 10
#define PROB 0.6

struct course_input
{
    char name[10];
    float int_quo;
    int max_slots;
    int max_labs;
    int *lab_num;
};

struct student_input
{
    float cal_quo;
    int *pf;
    int ti;
    int pf_pointer;
};

struct lab_input
{
    char name[10];
    int num_ta;
    int max_ta;
    int *count_ta_ship;
    int *ta_status;
};

struct buffer_input
{
    int *buff;
    int buff_count;
};

struct forbidden_cou
{
    int *cou_id;
    int c_nums;
};

typedef struct course_input courses;
typedef struct student_input students;
typedef struct lab_input la;
typedef struct buffer_input buffer;
typedef struct forbidden_cou f_cou;

pthread_mutex_t taLock[MAX_TAS];
pthread_mutex_t stuLock[MAX_STUDENTS];
pthread_mutex_t couLock[MAX_COURSES];
pthread_cond_t con_stu[MAX_STUDENTS];
sem_t buffer_sem[MAX_COURSES];

pthread_mutex_t lock_seat_alloc[MAX_STUDENTS];
pthread_cond_t cond_seat_alloc[MAX_COURSES];

int num_stu, num_lab, num_cou;
courses *cou;
students *stu;
la *lab;
buffer *buf;
f_cou *fb_cou;

void tut_info(int cou_num, int ta_id, char *lab_name, char *cou_name)
{
    printf("Tutorial has started " ANSI_COLOR_RED "for" ANSI_COLOR_RESET " Course %s with %d seats filled out of %d\n", cou[cou_num].name, buf[cou_num].buff_count, cou[cou_num].max_slots);

    // lock for respective students

    for (int i = 0; i < buf[cou_num].buff_count; i++)
    {
        pthread_mutex_lock(&stuLock[buf[cou_num].buff[i]]);
    }

    // lock the course

    pthread_mutex_lock(&couLock[cou_num]);

    sleep(10); // conduction of tutorial

    printf("TA %d from lab %s has completed the tutorial " ANSI_COLOR_RED "for" ANSI_COLOR_RESET " course %s\n", ta_id, lab_name, cou_name);

    // unlock respective students

    for (int i = 0; i < buf[cou_num].buff_count; i++)
    {
        pthread_mutex_unlock(&stuLock[buf[cou_num].buff[i]]);
    }

    // signal the corresponding thread so the student is freed and decision can be made

    for (int i = 0; i < buf[cou_num].buff_count; i++)
    {
        pthread_cond_signal(&con_stu[buf[cou_num].buff[i]]); // what should I use here SIGNAL or BROADCAST
    }

    // freeing all students from buffer

    for (int i = 0; i < buf[cou_num].buff_count; i++)
    {
        sem_post(&buffer_sem[cou_num]);
    }

    // clear the buffer ...

    buf[cou_num].buff_count = 0; // multiple threads at this point could hav access to this memory ... so a RACE condition can take place

    pthread_mutex_unlock(&couLock[cou_num]);
}

void *stu_init(void *arg)
{
    int off_mode;
    off_mode = *(int *)arg;

    int x = 0;

    int clock = (stu[off_mode].ti) * 1; // controlling how students enter the registration.

    sleep(clock);

    printf("Student %d has filled " ANSI_COLOR_RED "in " ANSI_COLOR_RESET "preferences " ANSI_COLOR_RED "for " ANSI_COLOR_RESET "course registration\n", off_mode);

    for (int i = 0; i < 3; i++)
    {
        // student is about to enter some buffer course

        for (int a = 0; a < fb_cou->c_nums; a++)
        {
            if (stu[off_mode].pf[i] == fb_cou->cou_id[a])
            {
                x = -1;
                break;
            }
        }

        if (x == 0)
        {
            // pthread_mutex_lock(&lock_seat_alloc[off_mode]);

            // pthread_cond_wait(&cond_seat_alloc[stu[off_mode].pf[i]], &lock_seat_alloc[off_mode]);

            // pthread_mutex_unlock(&lock_seat_alloc[off_mode]);
            
            sem_wait(&buffer_sem[stu[off_mode].pf[i]]);

            pthread_mutex_lock(&couLock[stu[off_mode].pf[i]]);

            pthread_mutex_lock(&stuLock[off_mode]);

            printf("Student %d has been allocated a seat " ANSI_COLOR_RED "in " ANSI_COLOR_RESET
                   "course %s\n",
                   off_mode, cou[stu[off_mode].pf[i]].name);

            buf[stu[off_mode].pf[i]].buff[buf[stu[off_mode].pf[i]].buff_count] = off_mode;

            (buf[stu[off_mode].pf[i]].buff_count)++;

            pthread_mutex_unlock(&couLock[stu[off_mode].pf[i]]);

            pthread_cond_wait(&con_stu[off_mode], &stuLock[off_mode]); // wait here till tutorial calls function or course is removed from the portal

            if (i == stu[off_mode].pf_pointer) // not removed from the portal
            {

                // perform arithmetic and tell if student confirms or goes away ... ???

                float prob;

                prob = cou[stu[off_mode].pf[i]].int_quo * stu[off_mode].cal_quo;

                if (prob < PROB && i + 1 < 3)
                {
                    printf("Student %d has withdrawn from course %s\n", off_mode, cou[stu[off_mode].pf[i]].name);

                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", off_mode, cou[stu[off_mode].pf[i]].name, i + 1,cou[stu[off_mode].pf[i+1]].name, i + 2);

                    (stu[off_mode].pf_pointer)++;
                }

                else
                {
                    if (prob >= PROB)
                    {
                        printf("Student %d has selected course %s permanently\n", off_mode, cou[stu[off_mode].pf[i]].name);
                        pthread_mutex_unlock(&stuLock[off_mode]);
                        pthread_exit(NULL);

                        // sleep(100);
                    }
                }
            }

            else
            {
                printf("Student %d has withdrawn from course %s\n", off_mode, cou[stu[off_mode].pf[i]].name);

                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", off_mode, cou[stu[off_mode].pf[i]].name, i + 1,cou[stu[off_mode].pf[i+1]].name, i + 2);
            }

            pthread_mutex_unlock(&stuLock[off_mode]);
        }

        else
        {
            if (i + 1 < 3)
            {
                // pthread_mutex_lock(&stuLock[off_mode]);
                
                printf("Student %d has withdrawn from course %s\n", off_mode, cou[stu[off_mode].pf[i]].name);

                printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", off_mode, cou[stu[off_mode].pf[i]].name, i + 1,cou[stu[off_mode].pf[i+1]].name, i + 2);

                (stu[off_mode].pf_pointer)++;

                // pthread_mutex_unlock(&stuLock[off_mode]);
            }
        }
    }

    printf("Student %d couldn’t get any of his preferred courses\n", off_mode);
}

void *cou_init(void *arg)
{
    int cou_num;
    cou_num = *(int *)arg;

    sleep(1);

    for (int i = 0; i < cou[cou_num].max_labs; i++)
    {

        for (int j = 0; j < lab[(cou[cou_num].lab_num[i])].num_ta; j++)
        {

            if ((lab[(cou[cou_num].lab_num[i])].count_ta_ship[j]) < lab[(cou[cou_num].lab_num[i])].max_ta)
            {
                pthread_mutex_trylock(&taLock[lab[(cou[cou_num].lab_num[i])].ta_status[j]]);

                printf("Course %s has been allocated %d seats\n", cou[cou_num].name, cou[cou_num].max_slots);
                
                // pthread_cond_broadcast(&cond_seat_alloc[cou_num]);

                // lab[(cou[cou_num].lab_num[i])].ta_status[j] = 1;

                int ta_id = lab[(cou[cou_num].lab_num[i])].ta_status[j];

                printf("TA %d from lab %s has been allocated to course %s " ANSI_COLOR_RED "for " ANSI_COLOR_RESET
                       "his %dst TA ship\n",
                       j, lab[(cou[cou_num].lab_num[i])].name, cou[cou_num].name, ++(lab[(cou[cou_num].lab_num[i])].count_ta_ship[j]));

                // conducts the tutorial

                sleep(3);

                tut_info(cou_num, j, lab[(cou[cou_num].lab_num[i])].name, cou[cou_num].name);

                // printf("TA %d from lab %s has completed the tutorial " ANSI_COLOR_RED "for" ANSI_COLOR_RESET " course %s\n", j, lab[(cou[cou_num].lab_num[i])].name, cou[cou_num].name);

                j = -1; // trying to re-iterate the same lab tas again

                pthread_mutex_unlock(&taLock[ta_id]);
            }

            if( j == -1)
            {
                i = -1;
                break;
            }
        }
    }

    // j = -1 ---> makes the same ta going to same course more probable ...

    //  the course has exhausted all possible tas from all possible labs

    pthread_mutex_lock(&couLock[cou_num]);

    printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", cou[cou_num].name);

    fb_cou->cou_id[fb_cou->c_nums] = cou_num;
    (fb_cou->c_nums)++;

    for (int i = 0; i < buf[cou_num].buff_count; i++)
    {
        (stu[buf[cou_num].buff[i]].pf_pointer)++;

        pthread_cond_signal(&con_stu[buf[cou_num].buff[i]]); // what should I use here SIGNAL or BROADCAST
    }

    buf[cou_num].buff_count = 0; // erasing all students       // should I use any other lock ... .. .

    pthread_mutex_unlock(&couLock[cou_num]);
}

int main(int argc, void **argv)
{

    // input taken

    scanf("%d %d %d", &num_stu, &num_lab, &num_cou);

    cou = malloc((num_cou) * sizeof(courses));

    if (cou == NULL)
    {
        printf("No memory\n");
        exit(0);
    }
    stu = malloc(num_stu * sizeof(students));

    if (stu == NULL)
    {
        printf("No memory\n");
        exit(0);
    }
    lab = malloc(num_lab * sizeof(la));

    if (lab == NULL)
    {
        printf("No memory\n");
        exit(0);
    }

    buf = malloc(num_cou * sizeof(buffer));
    if (buf == NULL)
    {
        printf("No memory\n");
        exit(0);
    }

    fb_cou = malloc(sizeof(f_cou));

    if (fb_cou == NULL)
    {
        printf("No memory\n");
        exit(0);
    }

    fb_cou->cou_id = malloc(num_cou * sizeof(int));

    fb_cou->c_nums = 0;

    for (int i = 0; i < (num_cou); i++)
    {

        scanf("%s %f %d %d", cou[i].name, &cou[i].int_quo, &cou[i].max_slots, &cou[i].max_labs);

        cou[i].lab_num = malloc(cou[i].max_labs * sizeof(int));

        for (int j = 0; j < cou[i].max_labs; j++)
        {
            scanf("%d", &(cou[i].lab_num[j]));
        }
    }

    for (int i = 0; i < num_stu; i++)
    {
        scanf("%f", &(stu[i].cal_quo));
        stu[i].pf = malloc(3 * sizeof(int));

        for (int j = 0; j < 3; j++)
        {
            scanf("%d", &stu[i].pf[j]);
        }
        scanf("%d", &(stu[i].ti));

        stu[i].pf_pointer = 0;
    }

    for (int i = 0; i < num_lab; i++)
    {
        scanf("%s %d %d", lab[i].name, &lab[i].num_ta, &lab[i].max_ta);

        lab[i].count_ta_ship = malloc(lab[i].num_ta * sizeof(int));

        lab[i].ta_status = malloc(lab[i].num_ta * sizeof(int));

        for (int j = 0; j < lab[i].num_ta; j++)
        {
            lab[i].count_ta_ship[j] = 0;
        }

        for (int j = 0; j < lab[i].num_ta; j++)
        {
            lab[i].ta_status[j] = 0;
        }
    }

    for (int i = 0; i < num_cou; i++)
    {
        buf[i].buff = malloc(cou[i].max_slots * sizeof(int));

        buf[i].buff_count = 0;
    }

    // thread creation for students

    pthread_t stu_th[num_stu];

    for (int i = 0; i < num_stu; i++)
    {
        pthread_mutex_init(&stuLock[i], NULL);
    }

    for (int i = 0; i < num_cou; i++)
    {
        pthread_mutex_init(&couLock[i], NULL);
    }

    for (int i = 0; i < num_stu; i++)
    {
        pthread_mutex_init(&lock_seat_alloc[i], NULL);
    }

    for (int i = 0; i < num_stu; i++)
    {
        pthread_cond_init(&con_stu[i], NULL);
    }

    for (int i = 0; i < num_cou; i++)
    {
        pthread_cond_init(&cond_seat_alloc[i], NULL);
    }

    for (int i = 0; i < num_cou; i++)
    {
        sem_init(&buffer_sem[i], 0, cou[i].max_slots);
    }

    for (int i = 0; i < num_stu; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;

        pthread_create(&stu_th[i], NULL, &stu_init, a);
    }

    // thread creation for courses

    pthread_t cou_th[num_cou];

    int TAS = 0;

    for (int i = 0; i < num_lab; i++)
    {
        TAS += lab[i].num_ta;
    }

    for (int i = 0; i < TAS; i++)
    {
        pthread_mutex_init(&taLock[i], NULL);
    }

    int count = 0;

    for (int i = 0; i < num_lab; i++)
    {
        for (int j = 0; j < lab[i].num_ta; j++)
        {
            lab[i].ta_status[j] = count;

            count++;
        }
    }

    for (int i = 0; i < num_cou; i++)
    {
        int *a = malloc(sizeof(int));
        *a = i;

        pthread_create(&cou_th[i], NULL, &cou_init, a);
    }

    for (int i = 0; i < num_stu; i++)
    {
        pthread_join(stu_th[i], NULL);
    }

    for (int i = 0; i < num_cou; i++)
    {
        pthread_join(cou_th[i], NULL);
    }

    for (int i = 0; i < TAS; i++)
    {
        pthread_mutex_destroy(&taLock[i]);
    }

    for (int i = 0; i < num_stu; i++)
    {
        pthread_cond_destroy(&con_stu[i]);
    }

    for (int i = 0; i < num_cou; i++)
    {
        sem_destroy(&buffer_sem[i]);
    }
    for (int i = 0; i < num_cou; i++)
    {
        pthread_mutex_destroy(&couLock[i]);
    }

    for (int i = 0; i < num_stu; i++)
    {
        pthread_mutex_destroy(&stuLock[i]);
    }

    return 0;
}
