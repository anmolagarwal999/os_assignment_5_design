
//###########FILE CHANGE ./main_folder/SHAVAK KANSAL_305817_assignsubmission_file_/2020101023/q1/main.c ####################//

//#include "utils.h"
#include "defs.h"
#include "useless.h"

int sth;

course *course_list;
student *stu_list;
lab *lab_list;

void *lab_thread(void *arg)
{
    int flag[sth];

    f(sth){
        flag[i] = 0;
    }

    f(sth){
        int empty_lab = 1;

        f1(lab_list[i].ta_num)
            if(lab_list[i].ta_list[i1].turns_left != 0)
               empty_lab = 0; 
        
        if(flag[i]==0 && empty_lab == 1){
            printf(BRED "Lab %s no longer has students available for TA ship\n", lab_list[i].lab_name);
            flag[i] = 1;
        }
    }
}

void *course_thread(void* arg){
    course* c = (course*)arg;

    //printf("Course %s started\n", c->name);
    while(1){

        int lab_index;
        int ta_index;

        f(c->num_labs){
            f1(lab_list[i].ta_num){
                if(pthread_mutex_trylock(&lab_list[i].ta_list[i1].lock)!=0)
                    continue;

                if(lab_list[i].ta_list[i1].turns_left > 0){
                    lab_list[i].ta_list[i1].turns_left--;
                    //pthread_mutex_unlock(&lab_list[i].ta_list[i1].lock);

                    lab_index = i;
                    ta_index = i1;
                    goto found;
                }
                else 
                    pthread_mutex_unlock(&lab_list[i].ta_list[i1].lock);
            }
        }
        
        printf(BRED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", c->name);
        c->course_end = 1;
        pthread_cond_broadcast(&c->tut_cond);

        break;

        found:
            ;
            int64_t random_slots = (rand()%c->max_slots) + 1;
            
            printf(BYEL "TA %d from lab %d has been allocated to course %s for %ldth TA ship\n", ta_index, lab_index, c->name, (lab_list[lab_index].max_turns - lab_list[lab_index].ta_list[ta_index].turns_left));
            printf(BYEL "Course %s has been allocated %ld seats\n", c->name ,random_slots);
            
            /* sem_init(&c->tut_slots,0, random_slots);
            f(random_slots){
                csem_post(&c->tut_slots);
            }

            pthread_mutex_lock(&c->semaphore_lock);
                int num_students;
                sem_getvalue(&c->tut_slots, &num_students);

            pthread_mutex_unlock(&c->semaphore_lock);
            */
            
            pthread_mutex_lock(&c->tut_cond_lock);
            
            f(random_slots){
                pthread_cond_signal(&c->tut_cond);
            }

            // while(c->students_attending == 0)
            //     pthread_cond_signal(&c->tut_cond);

            pthread_mutex_unlock(&c->tut_cond_lock);

            sleep(1);

            printf(BYEL "Tutorial has started for Course %s  with %ld seats filled out of %ld\n", c->name, c->students_attending, random_slots);
            
            sleep(6);

            pthread_mutex_lock(&c->in_tut_lock);
                pthread_cond_broadcast(&c->in_tut);
            pthread_mutex_unlock(&c->in_tut_lock);

            printf(BYEL "TA %d from lab %s has completed the tutorial and left the course %s\n", ta_index, lab_list[lab_index].lab_name, c->name);

            c->students_attending = 0;
            pthread_mutex_unlock(&lab_list[lab_index].ta_list[ta_index].lock);

    }
}


void *student_thread(void* arg){
    student* s = (student*)arg;

    sleep(s->time);

    printf(BBLU "Student %ld has filled in preferences for course registration\n", s->student_id);

    int pref[3];
    pref[0] = s->pref1;
    pref[1] = s->pref2;
    pref[2] = s->pref3;

    f(3){

        if(i!=0){
            printf(BBLU "Student %ld has changed current preference from %s (priority %d) to %s (priority %d)\n", s->student_id, course_list[pref[i-1]].name, i-1, course_list[pref[i]].name, i);
        }
        if(course_list[pref[i]].course_end == 1){
            continue;
        }
        pthread_mutex_lock(&course_list[pref[i]].tut_cond_lock);
        pthread_cond_wait(&course_list[pref[i]].tut_cond, &course_list[pref[i]].tut_cond_lock);
        pthread_mutex_unlock(&course_list[pref[i]].tut_cond_lock);

        if(course_list[pref[i]].course_end == 1){
            continue;
        }
    
        pthread_mutex_lock(&course_list[pref[i]].student_counter_lock);
            course_list[pref[i]].students_attending++;
            printf(BBLU "Student %ld has been allocated a seat for course %s\n", s->student_id, course_list[pref[i]].name);
        pthread_mutex_unlock(&course_list[pref[i]].student_counter_lock);

        pthread_mutex_lock(&course_list[pref[i]].in_tut_lock);
            pthread_cond_wait(&course_list[pref[i]].in_tut, &course_list[pref[i]].in_tut_lock);
        pthread_mutex_unlock(&course_list[pref[i]].in_tut_lock);

        int prob = course_list[pref[i]].interest_quotient*s->calibre*100;
        int random_num = rand()%100;

        if(random_num < prob){
            printf(BGRN "Student %ld has selected course %s permanently\n", s->student_id, course_list[pref[i]].name);
            goto finished;
        }
        else {
            if(!course_list[pref[i]].course_end)
                printf(BGRN "Student %ld has withdrawn from course %s\n", s->student_id, course_list[pref[i]].name);
        }
    }

    printf(BCYN "Student %ld couldn’t get any of his preferred courses\n", s->student_id);

    finished:;
}   

int main() {
    int num_courses, num_stu, num_labs;

    scanf("%d %d %d", &num_stu , &num_labs ,  &num_courses);

    sth = num_labs;    
    // course course_list[num_courses];
    // student stu_list[num_stu];
    // lab lab_list[num_labs];

    course_list = malloc(num_courses * sizeof(course));
    stu_list = malloc(num_stu * sizeof(student));
    lab_list = malloc(num_labs * sizeof(lab));

    f(num_courses){
        char t1[NAME_LENGTH];
        float t2;
        int64_t t3, t4;

        scanf("%s %f %ld %ld", t1, &t2, &t3, &t4);

        course_list[i].interest_quotient = t2;
        course_list[i].max_slots = t3;
        strcpy(course_list[i].name, t1);

        course_list[i].num_labs = t4;
        course_list[i].lab_list = (int64_t *)malloc(sizeof(int64_t) * t4);

        for(int j = 0; j < t4; j++){
            scanf("%ld", &(course_list[i].lab_list[j]));
        }

        sem_init(&course_list[i].tut_slots, 0, 0);
        sem_init(&course_list[i].tut_not_started_binary_semaphore, 0, 1);
        course_list[i].semaphore_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
        

        course_list[i].tut_cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
        course_list[i].tut_cond_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

        course_list[i].student_counter_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
        course_list[i].course_end = 0;
    }

    f(num_stu){
        float t1;
        int64_t t2,t3,t4,t5;

        scanf("%f %ld %ld %ld %ld",&t1,&t2,&t3,&t4,&t5);

        stu_list[i].student_id = i;
        stu_list[i].calibre = t1;
        stu_list[i].pref1 = t2;
        stu_list[i].pref2 = t3;
        stu_list[i].pref3 = t4;
        stu_list[i].time = t5;

        stu_list[i].student_id = i;

    }

    f(num_labs){
        char t1[NAME_LENGTH];
        int64_t t2,t3;

        scanf("%s %ld %ld",t1,&t2,&t3);

        strcpy(lab_list[i].lab_name, t1);
        lab_list[i].lab_id = i;
        lab_list[i].ta_num = t2;
        lab_list[i].max_turns = t3;

        lab_list[i].ta_list = (ta*)malloc(sizeof(ta) * lab_list[i].ta_num);

        for(int j=0;j<lab_list[i].ta_num;j++){
            lab_list[i].ta_list[j].ta_id = j;
            lab_list[i].ta_list[j].lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
            lab_list[i].ta_list[j].turns_left = lab_list[i].max_turns;
            lab_list[i].ta_list[j].course_id = -1;
        }
    }

    pthread_t *course_threads = malloc(sizeof(pthread_t) * num_courses);
    pthread_t *student_threads = malloc(sizeof(pthread_t) * num_stu);

    f(num_courses){
        pthread_create(&course_threads[i], NULL, course_thread, &course_list[i]);
    }

    f(num_stu){
        pthread_create(&student_threads[i], NULL, student_thread, &stu_list[i]);
    }

    pthread_t *lab_print = malloc(sizeof(pthread_t));

    pthread_create(lab_print, NULL, lab_thread, NULL);
    
    f(num_stu){
        pthread_join(student_threads[i], NULL);
    }

    f(num_courses){
        pthread_join(course_threads[i], NULL);
    }

    pthread_join(*lab_print, NULL);

    printf(BRED "Exiting simulation\n");
}


/* void student_thread1(void* arg){
    student* s = (student*)arg;

    sleep(s->time);

    printf("Student %ld has filled in preferences for course registration", s->student_id);

    pthread_mutex_lock(&course_list[s->pref1].tut_cond_lock);
    pthread_cond_wait(&course_list[s->pref1].tut_cond, &course_list[s->pref1].tut_cond_lock);

    if(course_list[s->pref1].course_end == 1){
        pthread_mutex_unlock(&course_list[s->pref1].tut_cond_lock);
    }
    else {
        printf("%s ", s->);
        pthread_mutex_unlock(&course_list[s->pref1].tut_cond_lock);
    }

    pthread_mutex_lock(&course_list[s->pref2].tut_cond_lock);
    pthread_cond_wait(&course_list[s->pref2].tut_cond, &course_list[s->pref2].tut_cond_lock);

    if(course_list[s->pref2].course_end == 1){
        pthread_mutex_unlock(&course_list[s->pref2].tut_cond_lock);
    }
    else {
        pthread_mutex_unlock(&course_list[s->pref2].tut_cond_lock);
    }

    pthread_mutex_lock(&course_list[s->pref3].tut_cond_lock);
    pthread_cond_wait(&course_list[s->pref3].tut_cond, &course_list[s->pref3].tut_cond_lock);

    if(course_list[s->pref3].course_end == 1){
        pthread_mutex_unlock(&course_list[s->pref3].tut_cond_lock);
    }
    else {
        pthread_mutex_unlock(&course_list[s->pref3].tut_cond_lock);
    }

} */
//###########FILE CHANGE ./main_folder/SHAVAK KANSAL_305817_assignsubmission_file_/2020101023/q1/utils.h ####################//

#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define NAME_LENGTH 128

#define f(x) for(int i=0;i<x;i++)
#define f1(x) for(int i1=0;i1<x;i1++)
#define f2(x) for(int i2=0;i2<x;i2++)
//###########FILE CHANGE ./main_folder/SHAVAK KANSAL_305817_assignsubmission_file_/2020101023/q1/defs.h ####################//

#include "utils.h"

typedef struct ta {
    pthread_mutex_t lock; //  lock associated with individual ta so as to make it thread safe
    int64_t ta_id; //  id of ta 
    int64_t course_id; //  id of ta they whose tutorial they are taking right now 
    int64_t turns_left; // turns left for the ta
} ta;

typedef struct lab {
    int64_t lab_id; //  id of lab 
    char lab_name[NAME_LENGTH]; // name of lab
    int64_t ta_num; // number of tas
    int64_t max_turns; // max number of turns of every ta
    ta *ta_list; // list of tas in this lab
} lab;

typedef struct course {
    char name[NAME_LENGTH]; // course name
    float interest_quotient; // Interest associated with the course
    int64_t max_slots; // Max slots that can be allotted in a course tutorial

    int64_t num_labs; //  Number of labs the ta's can be chosen from 
    int64_t *lab_list; //  list of the labs from which ta's can be selected 

    sem_t tut_slots; 
    sem_t tut_not_started_binary_semaphore;
    pthread_mutex_t semaphore_lock;

    pthread_cond_t tut_cond; // conditional variable for selecting students for the course tutorial
    pthread_mutex_t tut_cond_lock; //  lock associated with tut_cond 
    
    pthread_cond_t in_tut; //  conditional variable for signaling the end of the tut, student threads wait on it to indicate being in tut
    pthread_mutex_t in_tut_lock; //  lock associated with in_tut
    
    pthread_mutex_t student_counter_lock; //  lock associated with students_attending variable for it be thread safe
    int64_t course_end; // variable which indicates whether course has ended  1 - indicates end of course 0 - indicates course isnt ended yet
    int64_t students_attending; // Keeps count of how many students are in the current tut
} course;

typedef struct student {
    int64_t student_id; // id of student
    int64_t pref1; //  course id of first course preference
    int64_t pref2; // course id of second course preference
    int64_t pref3; //  course id of third course preference
    float calibre; // caliber of student
    int64_t time; //  time when student registers
} student;




//###########FILE CHANGE ./main_folder/SHAVAK KANSAL_305817_assignsubmission_file_/2020101023/q1/useless.h ####################//

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"