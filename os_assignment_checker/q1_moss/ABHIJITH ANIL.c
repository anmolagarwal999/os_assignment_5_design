
//###########FILE CHANGE ./main_folder/ABHIJITH ANIL_305962_assignsubmission_file_/2020101030_assignment_5/q1/q1.c ####################//

#include "defs.h"

pthread_mutex_t courselock[MAX_NO];
pthread_mutex_t studentlock[MAX_NO];
pthread_mutex_t* talock[MAX_NO];

pthread_cond_t studentwait[MAX_NO];
// pthread_cond_t tutorialwait[MAX_NO];
sem_t tutorialwait[MAX_NO];

// course* courses;
course courses[MAX_NO];
// stud* students;
stud students[MAX_NO];
// lab* labarr;
lab labarr[MAX_NO];
ta* tas[MAX_NO];


// The function used by the course thread to search for available TAs to be alloted to a course
// and take tutorial
// bonus part has been implemented
void searchTA(int* lablist, int numlabs, int coursenum)
{
    sem_wait(&tutorialwait[coursenum]);    //wait till at least 1 student takes course
    int selectta[numlabs];
    int min;                        //for the bonus part.
    for(int i=0;i<numlabs;i++)
    {
        selectta[i]=-1;
    }
    int checklab;                   // the lab that is being traversed in the iteration.
    for(int i=0;i<numlabs;i++)
    {
        min=100;
        checklab=lablist[i];
        if(labarr[checklab]->over==1)       //checking if lab has already exhausted its no. of TAs
        {   
            continue;                   
        }
        // for(int j=0;j<num_tas;j++)
        for(int j=0;j< labarr[checklab]->TAnum ;j++)
        {
            // tryta:
            pthread_mutex_lock(&talock[checklab][j]);
            // below if statement checks if the TA in a lab is free and is allowed to be alotted to a course
            if(tas[checklab][j]->lab_id == checklab && tas[checklab][j]->alloted==0 && tas[checklab][j]->timesalloted < labarr[checklab]->limit)
            {
                if(tas[checklab][j]->timesalloted <min) //get the TA who got alotted the minimum no. of times in a lab
                {
                    if(tas[checklab][j]->timesalloted == min-1 && tas[checklab][j]->alloted==1)
                    {
                        // printf("TA %d alloted so going to %d\n",j,j-1);
                        pthread_mutex_unlock(&talock[checklab][j]);
                        continue;
                    }
                    min=tas[checklab][j]->timesalloted;
                    selectta[i]=j;
                }
            }
            else if(j==labarr[checklab]->TAnum-1 && tas[checklab][j]->timesalloted == labarr[checklab]->limit && labarr[checklab]->over==0)
            {   
                labarr[checklab]->over=1;
                printf("Lab %s no longer has students available"BMAG" for "ANSI_RESET"TA ship\n",labarr[checklab]->name);
            }
            pthread_mutex_unlock(&talock[checklab][j]);
            
        }
        // TA has been selected, and is going to take a tutorial
        if(selectta[i]!=-1)
        {
            int k=selectta[i];
            pthread_mutex_lock(&talock[checklab][k]);
            tas[checklab][k]->alloted=1;
            tas[checklab][k]->courseno=coursenum;
            tas[checklab][k]->timesalloted++;
            printf("TA %d from lab %s allocated course %s"BMAG" for "ANSI_RESET"%dth TA ship\n",tas[checklab][k]->ta_id,labarr[checklab]->name, courses[coursenum]->name, tas[checklab][k]->timesalloted);
            pthread_mutex_unlock(&talock[checklab][k]);
            tutorial(tas[checklab][k]);
            return;
        }
    }
    // No ta is eligible to mentor the course, and thus removing the course
    courses[coursenum]->removeflag=1;
    printf("Course %s doesnt have any TA mentors eligible and is removed from course offerings\n",courses[coursenum]->name);
    freestudents(coursenum);
    return;
}

// The function used to free the students that are waiting for a tutorial of a removed course
// Traverses throught the array of students and signals their conditional variable.
void freestudents(int removedcourse)
{
    pthread_mutex_lock(&courselock[removedcourse]);
    for(int i=0;i<num_students;i++)
    {
        if(students[i]->currentcourse==removedcourse && students[i]->status=='c')
        {
            // printf("\n----------student %d attended tute, signalling----------\n",i);
            students[i]->status='f';
            students[i]->currentcourse=-1;
            students[i]->dontcheckprob=1;   //cant check if student has taken course, as course is withdrawn
            pthread_cond_signal(&studentwait[i]);
            // printf("\n---------signal complete----------\n");
        }
    }
    pthread_mutex_unlock(&courselock[removedcourse]);
}

// The function that deals with taking of a tutorial
void tutorial(ta takingTA)
{
    // printf("\n-----------tutorial function------------\n"); 
    int D=0;
    int coursenumber=takingTA->courseno;
    int max_slots=courses[takingTA->courseno]->maxslots;
    if(max_slots==1)
    {
        D=1;    //obvious
    }
    else
    {
        while(D==0)
            D= (rand()%max_slots);  //alloting a random number of seats
    }
    printf("Course %s has been allocated %d seats\n",courses[coursenumber]->name,D);
    int availseats=D;
    // at least 1 student has arrived, so starting tutorial (3 seconds)
    // taketut:
    pthread_mutex_lock(&courselock[coursenumber]);
    if(courses[coursenumber]->removeflag==1)
    {
        pthread_mutex_unlock(&courselock[coursenumber]);
        return;
    }
    // printf("\n--------------------in taketut------------------------\n");
    D=(courses[coursenumber]->stucount > D ? D : courses[coursenumber]->stucount);
    courses[coursenumber]->stucount-=D;
    printf("Tutorial has started"BMAG" for "ANSI_RESET"Course %s with %d seats filled out of %d\n",courses[coursenumber]->name,D,availseats);
    sleep(3);
    for(int k=0;k<D-1;k++)
    {
        sem_wait(&tutorialwait[coursenumber]);
    }
    // printf("\n--------------------after taketut------------------------\n");

    printf("TA %d from lab %s has"BMAG" completed the tutorial "ANSI_RESET"and left the course %s\n",takingTA->ta_id,labarr[takingTA->lab_id]->name,courses[coursenumber]->name);
    
    // After tutorial is over, signalling the students who attended the tutorial
    for(int i=0,k=0;i<num_students;i++)
    {
        if(k==D)
            break;
        if(students[i]->currentcourse==coursenumber && students[i]->status=='c')
        {
            // printf("\n----------student %d attended tute, signalling----------\n",i);
            students[i]->status='f';
            students[i]->currentcourse=-1;
            pthread_cond_signal(&studentwait[i]);
            k++;
            // printf("\n---------signal complete----------\n");
        }
    }
    pthread_mutex_unlock(&courselock[coursenumber]);
    takingTA->alloted=0;
}

// the function used by a student thread to apply to a particular course
void applycourse(int coursenum, int studindex)
{
    pthread_mutex_lock(&courselock[coursenum]);
    pthread_mutex_lock(&studentlock[studindex]);
    if(courses[coursenum]->removeflag==1)
    {
        pthread_mutex_unlock(&studentlock[studindex]);
        pthread_mutex_unlock(&courselock[coursenum]);
        return;
    }
    courses[coursenum]->stucount++;
    sem_post(&tutorialwait[coursenum]);
    // if(courses[coursenum]->stucount==1) //first student to be alloted a seat
    // {
    //     pthread_cond_signal(&tutorialwait[coursenum]);
    // }
    students[studindex]->currentcourse=coursenum;
    students[studindex]->status='c';
    printf("Student %d has been allocated a seat "BMAG"in"ANSI_RESET" course %s\n",studindex,courses[coursenum]->name);
    students[studindex]->dontcheckprob=0;
    pthread_cond_wait(&studentwait[studindex],&courselock[coursenum]);
    // The student waits for either the tutorial to finish, or for the course gets removed
    float prob=(students[studindex]->calibre)*(courses[coursenum]->interest);
    int randomnumber=(rand()%100);
    float randomprolty=(float)randomnumber/100.00;
    if(prob > randomprolty && students[studindex]->dontcheckprob==0)
    {
        // student takes a course if the prob is greater than some random number generated
        students[studindex]->qflag=1;
        printf("Student %d has selected course %s"BRED" permanently\n"ANSI_RESET,studindex,courses[coursenum]->name);
    }
    else
    {
        printf("Student %d has "BGRN"withdrawn"ANSI_RESET" from course %s\n",studindex,courses[coursenum]->name);
    }
    pthread_mutex_unlock(&studentlock[studindex]);
    pthread_mutex_unlock(&courselock[coursenum]);
    // printf("Student %d has been allocated a seat in course %s\n",studindex,courses[coursenum]->name);
}

// the thread handling funtion for course threads
void* coursehandler(void* args)
{
    int* currentcourseindex=(int*)args;
    course currentcourse=courses[*currentcourseindex];
    int* lablist=currentcourse->lablist;
    int numlabs=currentcourse->numlabs;
    // obtained the course info, searching for TA till the course gets removed
    while(currentcourse->removeflag!=1)
    {
        searchTA(lablist,numlabs,*currentcourseindex);
    }
    return NULL;
}

// thread handler function for student threads
void* studenthandler(void* args)
{
    int* stuindex=(int*)args;
    stud currstudent=students[*stuindex];
    int sleeptimer=currstudent->time;
    // obtained info of student.
    if(sleeptimer>1)
    {
        sleep(sleeptimer-1);    // sleep till time t
    }
    printf("Student %d has filled"BMAG" in "ANSI_RESET"preferences"BMAG" for "ANSI_RESET"course registration\n",*stuindex);
    int coursenum=-1;
    //loops through the student's preference and applies for course
    for(int i=0;i<3;i++)
    {
        if(currstudent->qflag==1)
        {
            // break;
            return NULL;
        }
        if(i!=0)
        {
            printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n",*stuindex,courses[currstudent->prefid[i-1]]->name,i,courses[currstudent->prefid[i]]->name,i+1);
        }
        coursenum=currstudent->prefid[i];
        applycourse(coursenum,*stuindex);
    }
    if(currstudent->qflag==1)
    {
        // break;
        return NULL;
    }
    currstudent->qflag=1;
    printf(BCYN"Student %d couldn’t get any of his preferred courses\n"ANSI_RESET,*stuindex);
    return NULL;
}


int main()
{
    srand(time(0));
    scanf("%d %d %d",&num_students,&num_labs,&num_courses);
    // courses=(course*)malloc(num_courses*sizeof(course));
    // students=(stud*)malloc(num_courses*sizeof(stud));
    // labarr=(lab*)malloc(num_courses*sizeof(lab));
    for(int i=0;i<num_courses;i++)
    {
        courses[i]=(course)malloc(sizeof(struct Course));
        scanf("%s %f %d %d",courses[i]->name,&courses[i]->interest,&courses[i]->maxslots,&courses[i]->numlabs);
        courses[i]->lablist=(int*)malloc(courses[i]->numlabs*sizeof(int));
        for(int j=0;j<courses[i]->numlabs;j++)
        {
            scanf("%d",&(courses[i]->lablist[j]));
        }
        courses[i]->removeflag=0;
        courses[i]->stucount=0;
        pthread_mutex_init(&courselock[i], NULL);
        sem_init(&tutorialwait[i],0,0);
        // pthread_cond_init(&tutorialwait[i],NULL);
    }
    for(int i=0;i<num_students;i++)
    {
        students[i]=(stud)malloc(sizeof(struct Student));
        scanf("%f %d %d %d %d",&students[i]->calibre,&students[i]->prefid[0],&students[i]->prefid[1],&students[i]->prefid[2],&students[i]->time);
        students[i]->status='w';
        students[i]->currentcourse=-1;
        students[i]->qflag=0;
        students[i]->dontcheckprob=0;
        pthread_mutex_init(&studentlock[i], NULL);
        pthread_cond_init(&studentwait[i],NULL);
    }
    for(int i=0;i<num_labs;i++)
    {
        labarr[i]=(lab)malloc(sizeof(struct Lab));
        scanf("%s %d %d",labarr[i]->name,&labarr[i]->TAnum,&labarr[i]->limit);
        num_tas=labarr[i]->TAnum;
        labarr[i]->over=0;
        tas[i]=(ta*)malloc(num_tas*sizeof(ta));
        talock[i]=(pthread_mutex_t*)malloc(num_tas*sizeof(pthread_mutex_t));
        for(int j=0;j<num_tas;j++)
        {
            tas[i][j]=(ta)malloc(sizeof(struct TA));
            pthread_mutex_init(&talock[i][j], NULL);
            tas[i][j]->alloted=0;
            tas[i][j]->ta_id=j;
            tas[i][j]->lab_id=i;
            tas[i][j]->courseno=-1;
            tas[i][j]->timesalloted=0;
        }
    }
    pthread_t coursethreads[num_courses];
    int coursearg[num_courses];
    pthread_t studentthreads[num_students];
    int studentarg[num_students];
    for(int i=0;i<num_courses;i++)
    {
        // int jaba=i;
        coursearg[i]=i;
        pthread_t curr_tid;
        pthread_create(&curr_tid,NULL,coursehandler,(void*)(&coursearg[i]));
        coursethreads[i]=curr_tid;
    }
    for(int i=0;i<num_students;i++)
    {
        // int jaba=i;
        studentarg[i]=i;
        pthread_t curr_tid;
        pthread_create(&curr_tid,NULL,studenthandler,(void*)(&studentarg[i]));
        studentthreads[i]=curr_tid;
    }
    for(int i=0;i<num_students;i++)
    {
        pthread_join(studentthreads[i],NULL);
    }
    printf(BYEL"\n---------------END OF SIMULATION :) -------------------------\n"ANSI_RESET);
    // pthread_t coursethreads[num_courses];
}

//###########FILE CHANGE ./main_folder/ABHIJITH ANIL_305962_assignsubmission_file_/2020101030_assignment_5/q1/defs.h ####################//

#ifndef DEFINITIONS
#define DEFINITIONS

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

#define MAX_NO 1000

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

struct Course {
    char name[20];
    float interest;
    int maxslots;
    int numlabs;
    int stucount;
    int* lablist;
    int removeflag; //0 if not removed 1 if removed
};

struct Lab {
    char name[20];
    int TAnum;
    int limit;
    int over;
};

struct Student {
    float calibre;
    int prefid[3];
    char status; // w= waiting, c=in class, f=finished course 
    int time;
    int currentcourse;
    int qflag;
    int dontcheckprob;
    // int pref2id;
    // int pref3id;

};

struct TA {
    int lab_id;
    int ta_id;
    int alloted; //0 if no course has been alloted, 1 if course has been alloted
    int courseno;
    int timesalloted;
    // int lockid;
};

typedef struct Course* course;
typedef struct Lab* lab;
typedef struct Student* stud;
typedef struct TA* ta;

int num_courses;
int num_students;
int num_labs;
int num_tas;

void searchTA(int* lablist, int numlabs, int coursenum);
void tutorial(ta takingTA);
void applycourse(int coursenum, int studindex);
void* coursehandler(void* args);
void* studenthandler(void* args);
void freestudents(int removedcourse);

#endif