
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/vector.c ####################//

#include "vector.h"
void init_vector(vector* v) {
    v->arr = NULL;
    v->size = 0;
    v->capacity = 0;
}
vector* init_vector_ptr() {
    vector* v = (vector*)malloc(sizeof(vector));
    v->size = 0;
    v->capacity = 0;
    v->arr = NULL;
    return v;
}
void pushback(vector* v, unsigned ele) {
    if (v == NULL) {
        printf("v is NULL in pushback. Initialise v using init_vector_ptr first!\n");
        assert(0);
    }
    if (v->arr == NULL) {
        if (v->size != 0) assert(0);
        v->arr = (unsigned*)malloc(sizeof(unsigned) * 2);
        assert(v->arr != 0);

        v->size = 0;
        v->capacity = 2;
    }
    if (v->size == v->capacity - 1) {
        v->arr = (unsigned*)realloc(v->arr, 2 * (v->capacity) * sizeof(unsigned));

        if (v->arr == NULL) {
            printf("Failed to allocate memory\n");
            assert(0);
        }
        v->capacity = v->capacity * 2;
    }
    v->arr[v->size] = ele;
    ++(v->size);
}
void print_vector(vector* v) {
    for (unsigned i = 0; i < v->size; ++i) {
        printf("%d ", v->arr[i]);
    }
    printf("\n");
}
void delete_vector(vector* v) {
    free(v->arr);
    v->capacity = 0;
    v->size = 0;
}
void delete_vector_ptr(vector** v) {
    free((*v)->arr);
    free(*v);
    *v = NULL;
}
void popback(vector* v) {
    if (v->size == 0) {
        printf("Underflow error! Cannot popback from an empty vector!\n");
        assert(0);
    }

    if (v->size == 0) {
        free(v->arr);
        v->arr = NULL;
        v->capacity = 0;
        return;
    }
    --(v->size);
    if (v->size * 2 <= v->capacity) {
        v->capacity = v->size;
        v->arr = (unsigned*)realloc(v->arr, v->size * sizeof(unsigned));
    }
}

//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/utils.c ####################//

#include "utils.h"

#include "vector.h"

void *courseFunc(void *arg) {
    unsigned courseID = ((thread_details *)arg)->id;
#if DEBUG >= 1
    printf(CYAN "CourseID in courseFunc: %u\n" RESET, courseID);
#endif
    assert(courseID < num_courses);
    assert(course != NULL);

    // iterate through L
    pthread_mutex_lock(&(course[courseID].lock));

    for (unsigned i = 0; i < course[courseID].numberOfLabs; ++i) {
        unsigned labID = course[courseID].L[i];

        // iterate through the available TAs to find a TA who's free
        pthread_mutex_lock(&(lab[labID].lock));

        for (unsigned j = 0; j < lab[labID].numberOfTAs; ++j) {
            if (!(lab[labID].ta[j].currentlyOccupied)) {
                lab[labID].ta[j].currentlyOccupied = true;
                vector studentsInClass;
                init_vector(&studentsInClass);

                printf(BRIGHT_BLUE
                       "TA %u from lab %s "
                       "has been allocated to course %s for his %u" RESET,
                       j, lab[labID].name,
                       course[courseID].name,
                       lab[labID].ta[j].numSessionsTaken);

                printOrdinalNumber(lab[labID].ta[j].numSessionsTaken);

                printf(BRIGHT_BLUE " TA ship\n" RESET);

                unsigned slotSize = ((unsigned)rand()) % course[courseID].maxNumberOfSlots + 1;  // number between 0 and max num of slots that the course permits

                unsigned slotsFilled = 0;
                for (unsigned k = 0;
                     k < num_students && slotsFilled <= slotSize;
                     ++k) {
                    if (pthread_mutex_trylock(&(student[k].lock)) != 0) {
                        continue;
                    }
                    if (student[k].hasFilledForm == false || student[k].currentlyOccupied == true || (student[k].currentPreference - student[k].preference > 3)) {
                        pthread_mutex_unlock(&(student[k].lock));
                        continue;
                    }

                    if (*(student[k].currentPreference) == courseID) {
                        pushback(&studentsInClass, student[k].StudentID);
                        printf(MAGENTA
                               "Student %u "
                               "has been allocated a seat in course %s\n" RESET,
                               student[k].StudentID,
                               course[courseID].name);
                        student[k].currentlyOccupied = true;
                        (student[k].currentPreference)++;
                    }

                    pthread_mutex_unlock(&(student[k].lock));
                }

                (lab[labID].ta[j].numSessionsTaken)++;
                lab[labID].ta[j].currentlyOccupied = false;

                delete_vector(&studentsInClass);
            }
        }

        pthread_mutex_unlock(&(lab[labID].lock));
    }

    pthread_mutex_unlock(&(course[courseID].lock));
    return NULL;
}

void *studentFunc(void *arg) {
    unsigned studentID = ((thread_details *)arg)->id;

#if DEBUG >= 1
    printf(CYAN "StudentID in studentFunc: %u\n" RESET, studentID);
#endif
    assert(studentID < num_students);

    pthread_mutex_lock(&(student[studentID].lock));

    // student fills form after waiting for t seconds
    sleep((unsigned)(student[studentID].timeOfFilling));
    student[studentID].hasFilledForm = true;

    printf("Student %u has filled in preferences for course registration\n", studentID);
    pthread_mutex_unlock(&(student[studentID].lock));

    return NULL;
}

void *labFunc(void *arg) {
    unsigned labID = ((thread_details *)arg)->id;
    assert(labID < num_labs);
    return NULL;
}

void printOrdinalNumber(unsigned n) {
    if (n % 10 == 1 && n % 100 != 11)
        printf(BRIGHT_BLUE "st" RESET);
    else if (n % 10 == 2 && n % 100 != 12)
        printf(BRIGHT_BLUE "nd" RESET);
    else if (n % 10 == 3 && n % 100 != 13)
        printf(BRIGHT_BLUE "rd" RESET);
    else
        printf(BRIGHT_BLUE "th" RESET);
}

//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/main.c ####################//

#include "header.h"
#include "input.h"
#include "utils.h"

pthread_t *course_thread = NULL, *stu_thread = NULL, *lab_thread = NULL;
// pthread_t course_thread[10000];
// pthread_t stu_thread[10000];
// pthread_t lab_thread[10000];

Course* course = NULL;
Student* student = NULL;
Lab* lab = NULL;
unsigned num_students, num_labs, num_courses;
// pthread_mutex_t *studentLock, *courseLock, *labLock;
pthread_mutex_t numLock;

int main() {
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

#if DEBUG >= 1
    printf("num_students = %d\nnum_labs = %d\nnum_courses = %d\n", num_students, num_labs, num_courses);
#endif

    course = (Course*)malloc(sizeof(Course) * num_courses);
    after_malloc_check(course);
    student = (Student*)malloc(sizeof(Student) * num_students);
    after_malloc_check(student);
    lab = (Lab*)malloc(sizeof(Lab) * num_labs);
    after_malloc_check(lab);

    // Taking input
    input();
    // inputCourses();
    // inputStudents();
    // inputLabs();

    // Allocating space to the thread arrays
    course_thread = (pthread_t*)malloc(sizeof(pthread_t) * num_courses);
    after_malloc_check(course_thread);
    stu_thread = (pthread_t*)malloc(sizeof(pthread_t) * (num_students + 1));
    after_malloc_check(stu_thread);
    lab_thread = (pthread_t*)malloc(sizeof(pthread_t) * num_labs);
    // after_malloc_check(lab_thread);

    // Allocating space to lock arrays
    // courseLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_courses);
    // after_malloc_check(courseLock);
    // studentLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_students);
    // after_malloc_check(studentLock);
    // labLock = (pthread_mutex_t*)malloc(sizeof(pthread_t) * num_labs);
    // after_malloc_check(labLock);

    // for (unsigned i = 0; i < num_courses; ++i) {
    //     pthread_mutex_init(&courseLock[i], NULL);
    // }
    // for (unsigned i = 0; i < num_students; ++i) {
    //     pthread_mutex_init(&studentLock[i], NULL);
    // }
    // for (unsigned i = 0; i < num_labs; ++i) {
    //     pthread_mutex_init(&labLock[i], NULL);
    // }
    pthread_mutex_init(&numLock, NULL);

    for (unsigned i = 0; i < num_courses; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&course_thread[i], NULL, courseFunc, (void*)temp);
        // pthread_create(&course_thread[i], NULL, courseFunc, &i);
    }
    for (unsigned i = 0; i < num_students; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&(stu_thread[i]), NULL, studentFunc, (void*)temp);
        // pthread_create(&stu_thread[i], NULL, studentFunc, &i);
    }
    for (unsigned i = 0; i < num_labs; ++i) {
        thread_details* temp = (thread_details*)malloc(sizeof(thread_details));
        temp->id = i;
        PthreadCreate(&lab_thread[i], NULL, labFunc, (void*)temp);
        // pthread_create(&lab_thread[i], NULL, labFunc, &i);
    }

    // // Waiting for the threads to finish execution
    for (unsigned i = 0; i < num_labs; ++i) {
        pthread_join(lab_thread[i], NULL);
    }
    for (unsigned i = 0; i < num_students; ++i) {
        pthread_join(stu_thread[i], NULL);
    }
    for (unsigned i = 0; i < num_courses; ++i) {
        pthread_join(course_thread[i], NULL);
    }

    // Delete locks
    // free(labLock);
    // free(courseLock);
    // free(studentLock);

    // Delete threads
    free(lab_thread);
    free(stu_thread);
    free(course_thread);

    // Deleting labs
    for (unsigned i = 0; i < num_labs; ++i) {
        free(lab[i].ta);
    }
    free(lab);

    // Deleting students
    free(student);

    // Deleting courses
    for (unsigned i = 0; i < num_courses; ++i) {
        free(course[i].L);
    }
    free(course);
    return 0;
}
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/input.c ####################//

#include "input.h"

void input() {
    inputCourses();
    inputStudents();
    inputLabs();
}

void inputCourses() {
    for (unsigned i = 0; i < num_courses; ++i) {
        course[i].CourseID = i;
        pthread_mutex_init(&(course[i].lock), NULL);

        scanf("%s %lf %u %u", course[i].name, &(course[i].interest), &(course[i].maxNumberOfSlots), &(course[i].numberOfLabs));

        assert(course[i].interest >= 0 && course[i].interest <= 1);
        assert(course[i].maxNumberOfSlots > 0);
        assert(course[i].numberOfLabs > 0);

        course[i].L = (unsigned*)malloc(sizeof(int) * course[i].numberOfLabs);

        for (unsigned j = 0; j < course[i].numberOfLabs; ++j) {
            scanf("%u", &(course[i].L[j]));
        }
    }

#if DEBUG == 2
    printf(GREEN "**COURSES**\n" RESET);
    for (unsigned i = 0; i < num_courses; ++i) {
        printf("Name: %s\n", course[i].name);
        printf("Interest: %lf\n", course[i].interest);
        printf("maxNumberOfSlots: %u\n", course[i].maxNumberOfSlots);
        printf("numberOfLabs: %u\n", course[i].numberOfLabs);
        printf("L: ");
        for (unsigned j = 0; j < course[i].numberOfLabs; ++j) {
            printf("%u ", course[i].L[j]);
        }
        printf("\n\n");
    }
#endif
}

void inputStudents() {
    for (unsigned i = 0; i < num_students; ++i) {
        student[i].currentPreference = &(student[i].preference[0]);
        student[i].StudentID = i;
        student[i].hasFilledForm = false;
        student[i].currentlyOccupied = false;

        pthread_mutex_init(&(student[i].lock), NULL);

        scanf("%lf", &(student[i].calibre));
        for (unsigned j = 0; j < 3; ++j) {
            scanf("%u", &(student[i].preference[j]));
        }
        scanf("%" PRIu64, &(student[i].timeOfFilling));
    }

#if DEBUG == 2
    printf(GREEN "**STUDENTS**\n" RESET);
    for (unsigned i = 0; i < num_students; ++i) {
        printf("calibre: %lf\n", student[i].calibre);
        printf("Preferences: ");
        for (unsigned j = 0; j < 3; ++j)
            printf("%u ", student[i].preference[j]);
        printf("\ntimeOfFilling: %" PRIu64 "\n\n", student[i].timeOfFilling);
    }
#endif
}

void inputLabs() {
    for (unsigned i = 0; i < num_labs; ++i) {
        lab[i].LabID = i;
        pthread_mutex_init(&(lab[i].lock), NULL);

        scanf("%s %u %u",
              lab[i].name,
              &(lab[i].numberOfTAs),
              &(lab[i].maxNumTACanMentor));

        lab[i].ta = (TA*)malloc(sizeof(TA) * lab[i].numberOfTAs);
        after_malloc_check(lab[i].ta);

        for (unsigned j = 0; j < lab[i].numberOfTAs; ++j) {
            lab[i].ta[j].currentlyOccupied = false;
            lab[i].ta[j].numSessionsTaken = 0;
        }
    }
#if DEBUG == 2
    printf(GREEN "**LABS**\n" RESET);
    for (unsigned i = 0; i < num_labs; ++i) {
        printf("name: %s\n", lab[i].name);
        printf("numberOfTAs: %u\n", lab[i].numberOfTAs);
        printf("maxNumTACanMentor: %u\n\n", lab[i].maxNumTACanMentor);
    }
#endif
}

//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/header.h ####################//

#ifndef HEADER_H
#define HEADER_H

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <grp.h>
#include <inttypes.h>
#include <limits.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/vector.h ####################//

/* HOW TO USE
METHOD 1 (recommended):
vector v;
init_vector(&v);
pushback(&v, unsigned ele); // to push back ele into v
popback(&v) // to pop back from v

v.size is the the number of elements currently is the vector
v.capacity is the capacity of the vector
DO NOT MODIFY v.size AND v.capacity UNLESS YOU KNOW WHAT YOU'RE DOING.
The functions mentioned above handle that.

delete_vector(&v); // to clear the contents of the vector

METHOD 2:
vector *v = init_vector_ptr(); // initialise v, preferably at the time of delaration
pushback(v, unsigned ele); // to push back ele into v
popback(v) // to pop back from v
delete_vector_ptr(&v) // to delete v
*/
#include "header.h"

#ifndef VECTOR_H
#define VECTOR_H

typedef struct vector vector;
struct vector {
    size_t size, capacity;
    unsigned* arr;
};

void init_vector(vector* v);
vector* init_vector_ptr();
void pushback(vector* v, unsigned ele);
void print_vector(vector* v);
void delete_vector(vector* v);
void delete_vector_ptr(vector** v);
void popback(vector* v);

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/utils.h ####################//

#ifndef UTILS_H
#define UTILS_H

#include "header.h"

#ifndef DEBUG
#define DEBUG 0
#endif

typedef struct Course {
    // CourseID = index in the original course arary (assumption)
    unsigned CourseID;

    char name[100];             // name of the course
    double interest;            // between 0 and 1
    unsigned maxNumberOfSlots;  // max number of slots which can be allotted by a TA, aka course_max_slot

    unsigned numberOfLabs;  // number of labs from which TAs can be chosen (i.e. size of L)
    unsigned *L;            // Lab IDs of the set of labs to choose TAs from

    pthread_mutex_t lock;

    // currentTA;

} Course;

typedef struct Student {
    // StudentID = index in the original student array
    unsigned StudentID;

    double calibre;

    unsigned preference[3];       // CourseIDs of preferences. preference[0] > preference[1] > preference[2], i.e., 0 is most preferred
    unsigned *currentPreference;  // not yet chosen

    uint64_t timeOfFilling;  // time of filling preferences, in seconds
    bool hasFilledForm;
    bool currentlyOccupied;

    pthread_mutex_t lock;
} Student;

typedef struct TA {
    bool currentlyOccupied;
    unsigned numSessionsTaken;

    // pthread_mutex_t lock;
} TA;

typedef struct Lab {
    // LabID = index in the original lab array
    unsigned LabID;

    char name[100];              // name of the lab
    unsigned numberOfTAs;        // number of TAs in the lab
    unsigned maxNumTACanMentor;  // max number of times a member of the lab can TA a course

    TA *ta;  // list of TAs in this lab
    pthread_mutex_t lock;
} Lab;

typedef struct thread_details {
    unsigned id;
} thread_details;

// arrays of threads
extern pthread_t *course_thread;
extern pthread_t *stu_thread;
extern pthread_t *lab_thread;

// extern pthread_t course_thread[10000];
// extern pthread_t stu_thread[10000];
// extern pthread_t lab_thread[10000];

extern unsigned num_students, num_labs, num_courses;

// arrays of Course, Student, Lab
extern Course *course;    // size: num_courses
extern Student *student;  // size: num_students
extern Lab *lab;          // size: num_labs

// Functions for the threads
void *courseFunc(void *arg);
void *studentFunc(void *arg);
void *labFunc(void *arg);

void printOrdinalNumber(unsigned n);  // Prints 'th' or 'nd' or 'st' depending on n. Eg: if n==1 then st because 1st

// Defining colours
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BRIGHT_GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define BRIGHT_BLUE "\x1b[94m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

// Mutex locks
// extern pthread_mutex_t *studentLock, *courseLock, *labLock;
// extern pthread_mutex_t *studentLock, *courseLock, *labLock;

extern pthread_mutex_t numLock;  // for num_courses, num_labs and num_students

// Some macros
#define PthreadCreate(newthread, attr, funcPtr, arg)          \
    if (pthread_create(newthread, attr, funcPtr, arg) != 0) { \
        perror("pthread_create() error");                     \
        exit(1);                                              \
    }

#define PthreadCondInit(cond, attr)            \
    if (pthread_cond_init(&cond, NULL) != 0) { \
        perror("pthread_cond_init() error");   \
        exit(1);                               \
    }

#define after_malloc_check(x)                  \
    if (x == NULL) {                           \
        printf("Failed to allocate memory\n"); \
        exit(EXIT_FAILURE);                    \
    }

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q1/input.h ####################//

#ifndef INPUT_H
#define INPUT_H

#include "header.h"
#include "utils.h"

// Master function
void input();

void inputCourses();
void inputStudents();
void inputLabs();

#endif