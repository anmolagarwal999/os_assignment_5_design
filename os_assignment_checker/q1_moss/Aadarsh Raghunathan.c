
//###########FILE CHANGE ./main_folder/Aadarsh Raghunathan_305881_assignsubmission_file_/2019113021/q1/q1.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <time.h>

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

struct students
{
    int id_stu;
    float calibre;
    int preferences[3];
    float time; // in seconds
    int curr_course;
    int picked;
    pthread_cond_t tut_end;
    int my_tut_done[3];
    int done;
    pthread_mutex_t tut_end_lock;
    int is_empty_tut;
};

struct courses
{
    int id_cour;
    char name[1000];
    float interest;
    int max_slots;
    int max_labs;
    int lab_list[10000];
    int running;
    int exist;
    pthread_cond_t general_tut_end;
};

struct labs
{
    int id_lab;
    char name[1000];
    int num_mentors;
    int max_TA_num;
};

struct ta
{
    int ta_id;
    int no_of_ta;
    int busy;
};

struct students s_arr[1000];
struct courses c_arr[1000];
struct labs l_arr[1000];
struct ta t_arr[1000][1000];

pthread_t student_arr[1000];
pthread_t course_arr[1000];
pthread_mutex_t ta_selection_lock;
pthread_cond_t general_tut_end = PTHREAD_COND_INITIALIZER;
pthread_cond_t empty_waiting_peeps = PTHREAD_COND_INITIALIZER;

int num_students, num_courses, num_labs;
int exited_students = 0;

void *course_simulation(void *arg)
{
    // while the course is not moved out , start by checking if a tutorial is running
    // if tutorial is not running , check if TA is available
    //if a TA is available , wake up the students

    pthread_mutex_init(&ta_selection_lock, NULL);
    struct courses *inp = (struct courses *)arg;
    while (inp->exist)
    {
        // 2d array of ta's in the format [lab][ta]
        // all courses can search for ta simultaneously
        //lock the process of picking ta
        if (exited_students == num_students)
        { // all students are done
            // printf("-----------------------Done-------------------------%s\n", inp->name);
            return NULL;
        }

        int atleast_one_student_flag = 0;
        for (int p = 0; p < num_students; p++)
        {
            if (s_arr[p].curr_course == inp->id_cour && s_arr[p].picked == 0 && !s_arr[p].done)
            {
                atleast_one_student_flag = 1;
            }
        }

        if (!atleast_one_student_flag)
        {
            continue;
        }

        int flag = 0;
        int sel_ta_lab, sel_ta;
        for (int i = 0; i < inp->max_labs; i++)
        {
            int l = inp->lab_list[i];
            for (int j = 0; j < l_arr[l].num_mentors; j++)
            {
                if (t_arr[i][j].busy == 0 && t_arr[i][j].no_of_ta < l_arr[l].max_TA_num)
                {
                    pthread_mutex_lock(&ta_selection_lock);
                    if (t_arr[i][j].busy == 0 && t_arr[i][j].no_of_ta < l_arr[l].max_TA_num)
                    {
                        t_arr[i][j].busy = 1;
                        t_arr[i][j].no_of_ta += 1;
                        flag = 1;
                        sel_ta_lab = i;
                        sel_ta = j;
                        printf(RED "TA %d from lab %s has been allocated to course %s for his %d TAship\n" RESET, j, l_arr[i].name, inp->name, t_arr[i][j].no_of_ta);
                    }
                    pthread_mutex_unlock(&ta_selection_lock);
                }
                if (flag == 1)
                {
                    break;
                }
            }
            if (flag == 1)
            {
                break;
            }
        }

        // now that the ta is picked , this ta can first delay for a bit to allow students to come for the tute, and then begin the tutorial
        // first generate the number of slots
        int num_slots = rand();
        num_slots = num_slots % inp->max_slots;
        num_slots = (num_slots + 1);
        printf(CYN "Course %s has been allocated %d seats\n" RESET, inp->name, num_slots);
        // sleep(2); // this sleep for 2 seconds is to allow the students to fill their preferences
        // sem_t tutorial_select;
        // sem_init(&tutorial_select, 0, 2);
        int count = 0;
        int selected_list[1000];
        for (int p = 0; p < num_students; p++)
        {
            if (s_arr[p].curr_course == inp->id_cour && s_arr[p].picked == 0 && count < num_slots && !s_arr[p].done)
            {
                s_arr[p].picked = 1;
                printf(MAG "Student number %d has been allocated a seat in course %s\n" RESET, p, inp->name);
                selected_list[count] = p;
                count++;
            }
        }

        if (flag == 0)
        { // could not find a ta
            // check if ta is giving lecture or dead
            int yeet = 0;
            for (int i = 0; i < inp->max_labs; i++)
            {
                int l = inp->lab_list[i];
                for (int j = 0; j < l_arr[l].num_mentors; j++)
                {
                    if (t_arr[i][j].no_of_ta < l_arr[l].max_TA_num)
                    {
                        yeet = 1;
                    }
                }
            }
            if (yeet == 1)
            { // there exists ta's just all are busy
                continue;
            }
            else
            {
                inp->exist = 0;
                printf(YEL "There are no TA's available for course %s and hence the course has exited the simulation\n" RESET, inp->name);
                for (int z = 0; z < count; z++)
                {
                    pthread_mutex_lock(&s_arr[selected_list[z]].tut_end_lock);
                    pthread_cond_signal(&s_arr[selected_list[z]].tut_end);
                    s_arr[selected_list[z]].is_empty_tut = 1;
                    pthread_mutex_unlock(&s_arr[selected_list[z]].tut_end_lock);
                }
                return NULL;
            }
        }
        // if 0 students , then dont start the tutorial
        //before allocating ta, check if there is atleast 1 student
        // students have been picked for courses
        printf(BLU "Tutorial has started for course %s with %d seats filled out of %d total seats\n" RESET, inp->name, count, num_slots);
        inp->running = 1;
        sleep(5); // tutorial going on

        // signal to wake up all students that have been picked for tutorial
        for (int p = 0; p < count; p++)
        {
            for (int x = 0; x < 3; x++)
            {
                if (s_arr[selected_list[p]].preferences[x] == inp->id_cour)
                {
                    s_arr[selected_list[p]].my_tut_done[x] = 1;
                }
            }
            pthread_cond_signal(&s_arr[selected_list[p]].tut_end);
        }
        pthread_mutex_lock(&ta_selection_lock);
        t_arr[sel_ta_lab][sel_ta].busy = 0;
        pthread_mutex_unlock(&ta_selection_lock);
        printf(YEL "Tutorial for course %s has ended\n" RESET, inp->name);
        inp->running = 0;
        for (int a = 0; a < num_students; a++)
        {
            pthread_mutex_lock(&s_arr[a].tut_end_lock);
            pthread_cond_signal(&inp->general_tut_end);
            pthread_mutex_unlock(&s_arr[a].tut_end_lock);
        }
    }
    return NULL;
}

void *student_simulation(void *arg)
{
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t tut_end_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t exit_student_lock = PTHREAD_MUTEX_INITIALIZER;
    struct students *inp = (struct students *)arg;
    sleep(inp->time);
    printf(BLU "Student %d has filled in preferences for course registration\n" RESET, inp->id_stu);
    int i;
    for (i = 0; i < 3; i++)
    {
        inp->curr_course = inp->preferences[i];
        inp->picked = 0; // they have not yet been picked for a tutorial for this course
        // if preference is gone, move to next one
        if ((c_arr[inp->curr_course].exist != 1) && (i < 2))
        {
            continue;
        }
        else if ((c_arr[inp->curr_course].exist != 1) && (i == 2))
        {
            printf(YEL "Student %d has exited the simulation\n" RESET, inp->id_stu);
            pthread_mutex_lock(&exit_student_lock);
            exited_students++;
            pthread_mutex_unlock(&exit_student_lock);
            return NULL;
        }
        // wait for current tutorial to end before moving to allocation
        if (c_arr[inp->curr_course].running)
        {
            pthread_mutex_lock(&inp->tut_end_lock);
            // wait for a signal to wake up this thread if a tutorial is not running
            while (!c_arr[inp->curr_course].running)
            {
                pthread_cond_wait(&c_arr[inp->curr_course].general_tut_end, &lock);
            }
            pthread_mutex_unlock(&inp->tut_end_lock);
            // if after the current tutorial the course gets moved out
            if ((c_arr[inp->curr_course].exist != 1) && (i < 2))
            {
                continue;
            }
            else if ((c_arr[inp->curr_course].exist != 1) && (i == 2))
            {
                printf(YEL "Student %d has exited the simulation\n" RESET, inp->id_stu);
                pthread_mutex_lock(&exit_student_lock);
                exited_students++;
                pthread_mutex_unlock(&exit_student_lock);
                return NULL;
            }
        }
        // try to join a tutorial

        int course_num = inp->curr_course;
        pthread_mutex_lock(&inp->tut_end_lock);
        while (!(inp->my_tut_done[i] == 1))
        {
            // wake up when the tutorial sends the ending signal
            pthread_cond_wait(&inp->tut_end, &tut_end_lock);
            inp->curr_course = -1; // set the current preference to -1 to stop the same student going into another tutorial
        }
        pthread_mutex_unlock(&inp->tut_end_lock);

        if (inp->is_empty_tut)
        {
            if (i < 2)
            {
                continue;
            }
            else
            {
                printf(YEL "Student %d has exited the simulation\n" RESET, inp->id_stu);
                return NULL;
            }
        }
        //calculate the probability of selecting the course
        float prob;
        prob = inp->calibre * c_arr[course_num].interest;
        // generate a random number in the range 0 to 1
        srand(time(NULL));
        float p = (rand() % 10001) / 10000.0;
        if (p >= prob)
        {
            if (i < 2)
            {
                printf(GRN "Student %d has not selected course %s and moved onto the next preference %d \n" RESET, inp->id_stu, c_arr[course_num].name, i + 1);
            }
            else
            {
                printf(MAG "Student %d has not selected course %s and exited the simulation" RESET, inp->id_stu, c_arr[course_num].name);
                inp->done = 1;
                inp->curr_course = -1;
                pthread_mutex_lock(&exit_student_lock);
                exited_students++;
                pthread_mutex_unlock(&exit_student_lock);
            }
        }
        else
        {
            printf(RED "Student %d has selected course %s permanently \n" RESET, inp->id_stu, c_arr[course_num].name);
            inp->done = 1;
            inp->curr_course = -1;
            pthread_mutex_lock(&exit_student_lock);
            exited_students++;
            pthread_mutex_unlock(&exit_student_lock);
        }
    }

    return NULL;
}

int main()
{
    scanf("%d%d%d", &num_students, &num_labs, &num_courses);
    int ch;
    while ((ch = getchar()) != '\n' && ch != EOF)
        ;
    // s_arr = (struct students *)malloc((num_students) * sizeof(struct students *));
    // c_arr = (struct courses *)malloc((num_courses) * sizeof(struct courses *));
    // l_arr = (struct labs *)malloc((num_labs) * sizeof(struct labs *));
    int i;
    char *course_info[1000];
    for (int q = 0; q < 1000; q++)
    {
        course_info[q] = (char *)malloc(1024 * sizeof(char));
    }
    for (int cour = 0; cour < num_courses; cour++)
    {
        struct courses c;
        char *line;
        size_t bufsize = 32;
        int line_size;
        line_size = getline(&line, &bufsize, stdin);
        course_info[0] = strtok(line, " ");
        int semicolon = 1;
        while (course_info[semicolon - 1] != NULL)
        {
            course_info[semicolon] = strtok(NULL, " ");
            semicolon++;
        }
        strcpy(c.name, course_info[0]);
        c.interest = atof(course_info[1]);
        c.max_slots = atoi(course_info[2]);
        c.max_labs = atoi(course_info[3]);
        c.running = 0;
        c.exist = 0;
        c.id_cour = cour;
        pthread_cond_init(&c.general_tut_end, NULL);
        int k = 0;
        while (k < c.max_labs)
        {
            c.lab_list[k] = atoi(course_info[k + 4]);
            k++;
        }
        int t;
        c_arr[cour] = c;
    }

    for (int stu = 0; stu < num_students; stu++)
    {
        struct students s;
        scanf("%f%d%d%d%f", &s.calibre, &s.preferences[0], &s.preferences[1], &s.preferences[2], &s.time);
        s.curr_course = -1;
        s.id_stu = stu;
        s.picked = 0;
        pthread_cond_init(&s.tut_end, NULL);
        s.done = 0;
        for (int m = 0; m < 3; m++)
        {
            s.my_tut_done[m] = 0;
        }
        pthread_mutex_init(&s.tut_end_lock, NULL);
        s.is_empty_tut = 0;
        s_arr[stu] = s;
    }

    while ((ch = getchar()) != '\n' && ch != EOF)
        ;

    char *lab_info[1000];
    for (int q = 0; q < 1000; q++)
    {
        lab_info[q] = (char *)malloc(1024 * sizeof(char));
    }

    for (int lab = 0; lab < num_labs; lab++)
    {
        struct labs l;
        char *line;
        size_t bufsize = 32;
        int line_size;
        line_size = getline(&line, &bufsize, stdin);
        lab_info[0] = strtok(line, " ");
        int semicolon = 1;
        while (lab_info[semicolon - 1] != NULL)
        {
            lab_info[semicolon] = strtok(NULL, " ");
            semicolon++;
        }
        strcpy(l.name, lab_info[0]);
        l.num_mentors = atoi(lab_info[1]);
        l.max_TA_num = atoi(lab_info[2]);
        l.id_lab = lab;
        struct ta t;
        for (int u = 0; u < l.num_mentors; u++)
        {
            t.no_of_ta = 0;
            t.busy = 0;
            t.ta_id = u;
            t_arr[lab][u] = t;
        }
        l_arr[lab] = l;
    }
    // printf("Input Successful");

    // check if each course exists

    for (int t = 0; t < num_courses; t++)
    {
        int flag = 0;
        for (int p = 0; p < c_arr[t].max_labs; p++)
        {
            if (l_arr[p].num_mentors >= 1 && l_arr[p].max_TA_num >= 1)
            {
                flag = 1;
                break;
            }
        }
        if (flag)
        {
            c_arr[t].exist = 1;
        }
    }

    int stud, cours;
    for (stud = 0; stud < num_students; stud++)
    {
        pthread_create(&student_arr[stud], NULL, student_simulation, (void *)&s_arr[stud]);
    }
    for (cours = 0; cours < num_courses; cours++)
    {
        pthread_create(&course_arr[cours], NULL, course_simulation, (void *)&c_arr[cours]);
    }
    for (stud = 0; stud < num_students; stud++)
    {
        pthread_join(student_arr[stud], NULL);
    }
    for (cours = 0; cours < num_courses; cours++)
    {
        pthread_join(course_arr[cours], NULL);
    }
    return 0;
}