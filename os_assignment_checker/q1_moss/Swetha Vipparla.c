
//###########FILE CHANGE ./main_folder/Swetha Vipparla_305815_assignsubmission_file_/Assignment 5/q1/q1.h ####################//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>

// Status of of students
#define C1AttentTut 1
#define C2AttentTut 2
#define C3AttentTut 3
#define C1Wait 4
#define C2Wait 5
#define C3Wait 6
#define C1Perm 7
#define C2Perm 8
#define C3Perm 9
#define exitStudent 10

// Status of the courses and labs
#define taWait 0
#define slotWait 1
#define tutBegin 2
#define WTDRN 3

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"

// pthread operations
#define lock(m) pthread_mutex_lock(&m)
#define unlock(m) pthread_mutex_unlock(&m)
#define wait(m, l) pthread_cond_wait(&m, &l)
#define broadcast(m) pthread_cond_broadcast(&m)
#define init(m) pthread_mutex_init(&m, NULL)
#define init_cond(m) pthread_cond_init(&m, NULL)
#define create(t, f, a) pthread_create(&t, NULL, &f, a)
#define join(t) pthread_join(t, NULL)

// structs for courses, students, and structs
typedef struct
{
    char name[100];
    int courseID;
    float interest;
    int course_max_slot;
    int noOfLabs;
    int status;
    int *lab_list;
} courseStruct;
typedef courseStruct *coursePointer;

typedef struct
{
    int student_id;
    float calibre;
    int firstPref;
    int secondPref;
    int thirdPref;
    int time;
    int status;
} studentStruct;
typedef studentStruct *studentPointer;

typedef struct
{
    int lab_id;
    char name[100];
    int **TA;
    int TA_num;
    int max_times;
    int status;
} labStruct;
typedef labStruct *labPointer;

int currentTime = 0;
int numCourses;
int numStudents;
int numLabs;
int coursesLeft;

coursePointer *course;
studentPointer *student;
labPointer *lab;
pthread_t time_thread;
pthread_mutex_t lockStudent;
pthread_mutex_t lock_checkTA;
pthread_mutex_t *lockLab;
pthread_cond_t checkTime = PTHREAD_COND_INITIALIZER;
pthread_cond_t check_TA = PTHREAD_COND_INITIALIZER;

//###########FILE CHANGE ./main_folder/Swetha Vipparla_305815_assignsubmission_file_/Assignment 5/q1/q1.c ####################//

#include "q1.h"

#define student(i, option) student[idStudentLeft[i]]->option

// checks if the all student has exited or permanently chosen or if all the courses have been withdrawn from
int isExit(void)
{
    int s = 0, c = 0, idStudentLeft[numStudents], i = 0;

    while (i < numCourses)
    {
        if (course[i]->status != WTDRN)
            c++;
        i++;
    }

    i = 0;

    while (i < numStudents)
    {
        if (student[i]->status < C1Perm)
        {
            idStudentLeft[s] = i;
            s++;
        }
        i++;
    }

    i = 0;

    if (s != 0 && c != 0)
    {
        while (i < s)
        {
            bool cond1 = student(i, status) < C1Wait;
            bool cond2 = student(i, status) == C1Wait && course[student(i, firstPref)]->status != WTDRN;
            bool cond3 = student(i, status) == C2Wait && course[student(i, secondPref)]->status != WTDRN;
            bool cond4 = student(i, status) == C3Wait && course[student(i, thirdPref)]->status != WTDRN;

            if (cond1 || cond2 || cond3 || cond4)
                return 0;

            i++;
        }
    }
    
    return 1;
}

// updates current timer
void *timer()
{
    while (1)
    {
        sleep(1);
        ++currentTime;
        
        if (!(currentTime % 5) && isExit())
            return NULL;

        broadcast(checkTime);
    }
    return NULL;
}

// updates the status of filling of preferences of the students
void *studentsFunction(void *index)
{
    int i = *(int *)index;
    lock(lockStudent);

    while (currentTime < student[i]->time)
        wait(checkTime, lockStudent);

    student[i]->status = C1Wait;
    unlock(lockStudent);
    printf(GREEN "Student %d has filled in preferences for course registration\n" RESET, i);
    return NULL;
}

#define labAllowed lab[allowed_lab]

// based on the labs allowed, the allot the labs to the TA's 
int availableTA(int course_num, int *currentTA, int *currentLab)
{
    int isTAThere = 0;
    
    // go through all the labs allowed and check if it's exhausted
    for (int l = 0; l < course[course_num]->noOfLabs; l++)
    {
        int allowed_lab = course[course_num]->lab_list[l];

        if (labAllowed->status == 1)
            continue;

        for (int i = 0; i < labAllowed->TA_num; i++)
        {
            if (labAllowed->TA[i][0] == 0 && labAllowed->TA[i][1] < labAllowed->max_times)
            {
                // lock the lab
                lock(lockLab[allowed_lab]);
                *currentTA = i, *currentLab = allowed_lab; // update the current TA and lab

                lab[*currentLab]->TA[*currentTA][0] = 1;
                ++(lab[*currentLab]->TA[*currentTA][1]);

                // unlock the lab
                unlock(lockLab[allowed_lab]);
                return 0;
            }
            
            if (isTAThere == 0 && labAllowed->TA[i][1] < labAllowed->max_times)
                isTAThere = 1;
        }
    }

    *currentLab = *currentTA = -1;

    if (isTAThere)
        return -1;
    else
        return -2;
}

void printPreference(int studentInterested, char* firstName, char* secondName, int n)
{
    printf(YELLOW "Student %d has changed current preference from %s to %s\n" RESET, studentInterested, firstName, secondName);

    if(!n)
        student[studentInterested]->status += 4; 
    else
        ++(student[studentInterested]->status);
}

#define cID course[courseID]

void printInterest(int studentInterested, int courseID) 
{ 
    printf(RED "Student %d has withdrawn from course %s\n" RESET, studentInterested, cID->name);

    if (student[studentInterested]->status == C3AttentTut)
    {
        printf(CYAN "Student %d could not get any of his preferred courses\n" RESET, studentInterested);
        student[studentInterested]->status = exitStudent;
        return;
    }

    if (student[studentInterested]->status == C1AttentTut)
        printPreference(studentInterested, course[student[studentInterested]->firstPref]->name, course[student[studentInterested]->secondPref]->name, 0);
    else
        printPreference(studentInterested, course[student[studentInterested]->secondPref]->name, course[student[studentInterested]->thirdPref]->name, 0);
    
    
    if (student[studentInterested]->status == C2Wait && course[student[studentInterested]->secondPref]->status == WTDRN)
        printPreference(studentInterested, course[student[studentInterested]->secondPref]->name, course[student[studentInterested]->thirdPref]->name, 1);
    
    if (student[studentInterested]->status == C3Wait && course[student[studentInterested]->thirdPref]->status == WTDRN)
    {
        printf(CYAN "Student %d could not get any of his preferred courses\n" RESET, studentInterested);
        student[studentInterested]->status = exitStudent;
    }
}

// check is the ta is eligible for the course and waits if the ta is not
void *coursesFunction(void *index)
{
    int courseID = *(int *)index, currentTA, currentLab;
    sleep(2);

    for(;;)
    {
        int eligible = availableTA(courseID, &currentTA, &currentLab);
        
        if (eligible == -2)
        {
            printf("Course %s doesn't have any TAs eligible and is removed from the course offerings\n", cID->name);

            --coursesLeft;
            cID->status = WTDRN;
            return NULL;
        }

        // handle deadlock when there is only one course left
        while (currentTA < 0 && coursesLeft > 1)
        {
            int run = 1;
            lock(lock_checkTA);

            while (run)
            {
                wait(check_TA, lock_checkTA);
                run = 0;
            }
            
            unlock(lock_checkTA);
            eligible = availableTA(courseID, &currentTA, &currentLab);
        }

        if (eligible == -2)
        {
            printf("Course %s doesn't have any TAs eligible and is removed from the course offerings\n", cID->name);
            --coursesLeft;
            cID->status = WTDRN;
            return NULL;
        }

        printf("TA %d from lab %s has been allocated to course %s for his %dst TA ship\n", currentTA, lab[currentLab]->name, cID->name, lab[currentLab]->TA[currentTA][1]);

        int notExhausted = 0, i = 0;

        while (i < numLabs) 
        {
            if (lab[i]->status != 1)
            {
                notExhausted = 0;

                for (int tas = 0; tas < lab[i]->TA_num; tas++)
                {
                    if (lab[i]->TA[tas][1] < lab[i]->max_times)
                    {
                        notExhausted = 1;
                        break;
                    }
                }

                if (!notExhausted)
                {
                    lock(lockLab[i]);
                    lab[i]->status = 1;
                    unlock(lockLab[i]);
                    printf(RED "Lab %s no longer has students available for TA ship\n" RESET, lab[i]->name);
                }
            }
            i++;
        }

        cID->status = slotWait;
        int maxSlot = cID->course_max_slot;

        // get the number of slots using rand
        int slots = rand() % maxSlot;

        printf(MAGENTA "Course %s has been alloted %d seats\n" RESET, cID->name, slots + 1);
        int W = 0, interestedStudent[numStudents];
        
        for (int s = 0; s < numStudents && W < slots + 1; s++)
        {
            int status = student[s]->status;
            bool cond1 = status == C1Wait && student[s]->firstPref == courseID;
            bool cond2 = status == C2Wait && student[s]->secondPref == courseID;
            bool cond3 = status == C3Wait && student[s]->thirdPref == courseID;

            if (cond1 || cond2 || cond3)
                interestedStudent[W] = s, ++W;

            else
                continue;
        }

        cID->status = tutBegin;

        int k = 0;
        while (k < W)
        {
            printf(GREEN "Student %d has been allocated a seat in course %s\n" RESET, interestedStudent[k], cID->name);
            student[interestedStudent[k++]]->status -= 3;
        }

        printf(ORANGE "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, cID->name, W, slots + 1);
        sleep(4);

        int chosen = 0;
        double probability;

        k = 0;
        while (k < W)
        {
            probability = (cID->interest) * (student[interestedStudent[k]]->calibre);

            // move to the next preferences when the course is withdrawn, where there will be 2 cases
            if(probability < 0.5)
                printInterest(interestedStudent[k], courseID);

            // permanently choose the course
            else
            {
                ++chosen;
                printf("Student %d has selected course %s permanently\n", interestedStudent[k], cID->name);
                student[interestedStudent[k]]->status += 6;
            }
            k++;
        }

        printf(LIGHT_PURPLE "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, currentTA, lab[currentLab]->name, cID->name);
        lab[currentLab]->TA[currentTA][0] = 0, cID->status = taWait;

        broadcast(check_TA);
    }
    return NULL;
}

#define dynMem(type, num) (type *)malloc(sizeof(type) * num)
#define mem(type, num) (type)malloc(sizeof(num))

int main()
{
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);
    coursesLeft = numCourses;

    course = dynMem(coursePointer, numCourses);
    student = dynMem(studentPointer, numStudents);
    lab = dynMem(labPointer, numLabs);
    
    int i = 0;
    while (i < numCourses)
    {
        course[i] = mem(coursePointer, courseStruct);
        course[i]->courseID = i, course[i]->status = taWait;

        scanf("%s %f %d %d", course[i]->name, &course[i]->interest, &course[i]->course_max_slot, &course[i]->noOfLabs);

        course[i]->lab_list = dynMem(int, course[i]->noOfLabs);

        if (!course[i]->lab_list)
            return 1;

        int j = 0;

        while(j < course[i]->noOfLabs)
            scanf("%d", &course[i]->lab_list[j++]);

        i++;
    }

    i = 0;

    while (i < numStudents)
    {
        student[i] = mem(studentPointer, studentStruct);
        student[i]->student_id = i, student[i]->status = 0;
        scanf("%f %d %d %d %d", &student[i]->calibre, &student[i]->firstPref, &student[i]->secondPref, &student[i]->thirdPref, &student[i]->time);
        i++;
    }

    i = 0;
    while (i < numLabs)
    {
        lab[i] = mem(labPointer, labStruct);
        lab[i]->lab_id = i;

        scanf("%s %d %d", lab[i]->name, &lab[i]->TA_num, &lab[i]->max_times);
        lab[i]->TA = dynMem(int *, lab[i]->TA_num);

        int j = 0;
        while (j < lab[i]->TA_num)
        {
            lab[i]->TA[j] = dynMem(int, 2);
            lab[i]->TA[j][0] = 0, lab[i]->TA[j++][1] = 0;
        }

        if (!lab[i]->max_times && !lab[i]->TA_num)
            lab[i]->status = 1;

        else
            lab[i]->status = 0;

        i++;
    }

    // initializing locks and other variables
    pthread_t thread_students[numStudents], thread_courses[numCourses];

    srand(time(0));
    init(lockStudent), init(lock_checkTA);

    lockLab = dynMem(pthread_mutex_t, numLabs);

    if (!lockLab)
        return 6;

    i = 0;
    while (i < numLabs)
        init(lockLab[i++]);

    i = 0; 
    while (i < numStudents)
    {
        int *a = malloc(sizeof(int));
        *a = i;

        if (create(thread_students[i], studentsFunction, a))
            return 3;
        i++;
    }

    i = 0;
    while (i < numCourses)
    {
        int *a = malloc(sizeof(int));
        *a = i;
        if (create(thread_courses[i], coursesFunction, a))
            return 4;
        i++;
    }

    if (create(time_thread, timer, NULL))
        return 5;

    i = 0;
    while (i < numStudents)
        join(thread_students[i++]);

    join(time_thread);

    i = 0;
    while (i < numCourses)
        join(thread_courses[i++]);
    
    pthread_mutex_destroy(&lockStudent), pthread_mutex_destroy(&lock_checkTA);

    i = 0;
    while (i < numLabs)
        pthread_mutex_destroy(&lockLab[i++]);

    pthread_cond_destroy(&checkTime), pthread_cond_destroy(&check_TA);

    // freeing dynamic memory
    i = 0;
    while (i < numCourses)
        free(course[i++]);

    free(course);

    i = 0;
    while (i < numStudents)
        free(student[i++]); 

    free(student);

    i = 0;
    while (i < numLabs)
    {
        for (int j = 0; j < lab[i]->TA_num; j++)
            free(lab[i]->TA[j]);

        free(lab[i]->TA), free(lab[i]);
        i++;
    }

    free(lab), free(lockLab);

    return 0;
}