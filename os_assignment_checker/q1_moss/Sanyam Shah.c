
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q1/student.c ####################//

#include "main.h"

void *handle_students(void *arg)
{
    clock_t difference;
    student *stud = (student *)arg;
    do
    {
        difference = clock() - before;
        difference /= CLOCKS_PER_SEC;
    } while (difference < stud->time);
    printf(RED "Student %d has filled in preferences for course registration\n" RESET, stud->id);
    stud->registered = true;
    while (1)
    {
        pthread_mutex_lock(studentMutex+stud->id);
        while (stud->attended==false)
        {
            pthread_cond_wait(allocated+stud->id,studentMutex+stud->id);
            if (c_list[stud->pref[stud->curr_pref]].removed==true)
            {
                break;
            }
            
        }
        if (stud->curr_pref >= 3)
        {
            printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
            stud->done = true;
            pthread_mutex_unlock(studentMutex+stud->id);
            break;
        }
        while (c_list[stud->pref[stud->curr_pref]].removed==true)
        {
            
            stud->curr_pref++;
            if (stud->curr_pref >= 3)
            {
                printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                stud->done = true;
                pthread_mutex_unlock(studentMutex+stud->id);
                return NULL;
            }
            stud->attended = false;
            printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
        }
        
        
        
        
        if (stud->attended == true)
        {
            int r = rand() % 100;
            float prob = (c_list[stud->pref[stud->curr_pref]].interest) * (stud->calibre) * 100.00f;
            if (r <= prob)
            {
                printf(CYN "Student %d has selected course %s permanently\n" RESET, stud->id, c_list[stud->pref[stud->curr_pref]].name);
                stud->done = true;
                pthread_mutex_unlock(studentMutex+stud->id);
                break;
            }
            else
            {
                printf(CYN "Student %d has withdrawn from course %s\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref]].name);
                
                stud->curr_pref++;
                if (stud->curr_pref >= 3)
                {
                    printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                    stud->done = true;
                    pthread_mutex_unlock(studentMutex+stud->id);
                    break;
                }
                stud->attended = false;
                printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
                while (c_list[stud->pref[stud->curr_pref]].removed==true)
                {
                    
                    stud->curr_pref++;
                    if (stud->curr_pref >= 3)
                    {
                        printf(CYN "Student %d couldn’t get any of his preferred courses\n" RESET, stud->id);
                        stud->done = true;
                        pthread_mutex_unlock(studentMutex+stud->id);
                        return NULL;
                    }
                    stud->attended = false;
                    printf(GRN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET,stud->id,c_list[stud->pref[stud->curr_pref-1]].name,stud->curr_pref,c_list[stud->pref[stud->curr_pref]].name,stud->curr_pref+1);
                }
            }
        }
        pthread_mutex_unlock(studentMutex+stud->id);
    }
    
    return NULL;
}

//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q1/main.c ####################//

#include "main.h"

int rng(int a, int b) { // [a, b]
	int dif = b - a + 1 ; 
	int rnd = rand() % dif ;
	return (a + rnd);
}

int	main()
{
    srand(time(0));

    scanf("%d%d%d",&num_studs,&num_labs,&num_courses);

    for (int i = 0; i < num_courses; i++)
    {
        c_list[i].id = i;
        c_list[i].removed = false;
        scanf("%s%f%d%d",c_list[i].name,&c_list[i].interest,&c_list[i].max_slots,&c_list[i].total_labs);
        c_list[i].lab_ids = (int*)malloc(sizeof(int)*(c_list[i].total_labs));
        for (int j = 0; j < c_list[i].total_labs; j++)
        {
            scanf("%d",&c_list[i].lab_ids[j]);
        }
        pthread_mutex_init(courseMutex+i,NULL);
    }
    for (int i = 0; i < num_studs; i++)
    {
        s_list[i].done = false;
        s_list[i].course_allocated = -1;
        s_list[i].registered = false;
        s_list[i].id = i;
        s_list[i].curr_pref = 0;
        s_list[i].attended = false;
        pthread_cond_init(allocated + i,NULL);
        scanf("%f%d%d%d%d",&s_list[i].calibre,&s_list[i].pref[0],&s_list[i].pref[1],&s_list[i].pref[2],&s_list[i].time);
        pthread_mutex_init(studentMutex+i,NULL);
    }
    for (int i = 0; i < num_labs; i++)
    {
        l_list[i].id = i;
        l_list[i].flag = true;
        scanf("%s%d%d",l_list[i].name,&l_list[i].capacity,&l_list[i].max_limit);
        l_list[i].lab_TAs = (int*)malloc(sizeof(int)*l_list[i].capacity);
        l_list[i].lk_TAs = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t)*l_list[i].capacity);
        for (int j = 0; j < l_list[i].capacity; j++)
        {
            l_list[i].lab_TAs[j] = 0;
            pthread_mutex_init(l_list[i].lk_TAs+j, NULL);
        }
    }
    before = clock();

    for (int i = 0; i < num_studs; i++)
    {
        pthread_create(&studentThread[i], NULL, handle_students,s_list+i);
    }
    for (int i = 0; i < num_courses; i++)
    {
        pthread_create(&courseThread[i], NULL, handle_courses,c_list+i);
    }
    for (int i = 0; i < num_studs; i++)
    {
        pthread_join(studentThread[i],NULL);

        // DESTROY BEFORE U SUBMIT

        pthread_mutex_destroy(studentMutex+i);
    }

    printf("Simulation over!\n");
    return 0;
}

//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q1/course.c ####################//

#include "main.h"

void *handle_courses(void *arg)
{
    course *cors = (course *)arg;
    while (1)
    {
        bool anyTA = false;
        for (int i = 0; i < cors->total_labs; i++)
        {
            bool chk = true;
            for (int j = 0; j < l_list[cors->lab_ids[i]].capacity; j++)
            {
                if (l_list[cors->lab_ids[i]].lab_TAs[j] < l_list[cors->lab_ids[i]].max_limit && l_list[cors->lab_ids[i]].flag == true)
                {
                    pthread_mutex_lock(l_list[cors->lab_ids[i]].lk_TAs + j);
                    chk = false;
                    anyTA = true;
                    l_list[cors->lab_ids[i]].lab_TAs[j]++;
                    if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 1)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dst TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 2)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dnd TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else if (l_list[cors->lab_ids[i]].lab_TAs[j] % 10 == 3)
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %drd TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }
                    else
                    {
                        printf(YEL "TA %d from lab %s has been allocated to course %s for %dth TA ship\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name, l_list[cors->lab_ids[i]].lab_TAs[j]);
                    }

                    int slots = rng(1, cors->max_slots);
                    printf(BLU "Course %s has been allocated %d seat(s)\n" RESET, cors->name, slots);
                    int w = 0;
                    // printf("%s\n",cors->name);
                    while (w==0)
                    {
                    	for (int k = 0; k < num_studs; k++)
		            {                                                        
		                if (s_list[k].registered == true && s_list[k].done == false && s_list[k].pref[s_list[k].curr_pref] == cors->id && s_list[k].attended == false && w < slots)
		                {
		                    pthread_mutex_lock(studentMutex + k);
		                    printf("Student %d has been allocated a seat in course %s\n", k, cors->name);
		                    // s_list[k].course_allocated = cors->id;
		                    s_list[k].attended = true;
		                    // pthread_cond_signal(allocated+k);
		                    // s_list[k].attended = true;
		                    w++;
		                    // pthread_mutex_unlock(studentMutex + k);
		                }

		            }
		     }
                    

                    printf(YEL "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, cors->name, w, slots);
                    sleep(2);
                    printf(BLU "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, j, l_list[cors->lab_ids[i]].name, cors->name);
                    
                    for (int k = 0; k < num_studs; k++)
                    {                          
                        pthread_mutex_unlock(studentMutex + k);
                        pthread_cond_signal(allocated + k);
                        
                    }
                    pthread_mutex_unlock(l_list[cors->lab_ids[i]].lk_TAs + j);
                }
            }
            if (chk && l_list[cors->lab_ids[i]].flag == true)
            {
                printf(WHT "Lab %s no longer has students available for TA ship\n" RESET, l_list[cors->lab_ids[i]].name);
                l_list[cors->lab_ids[i]].flag = false;
            }
        }
        if (anyTA == false)
        {
            cors->removed = true;
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", cors->name);
            while (1)
            {                
                for (int i = 0; i < num_studs; i++)
                {
                    if (s_list[i].pref[s_list[i].curr_pref] == cors->id)
                    {
                        // pthread_mutex_lock(studentMutex + i);
                        pthread_cond_signal(allocated + i);
                        // pthread_mutex_unlock(studentMutex + i);
                    }
                }
            }
        }
    }
}

//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q1/main.h ####################//

#ifndef __MAIN__H__
#define __MAIN__H__

#include "colors.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <semaphore.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdbool.h>

#define debug(x) printf("#%d : %d\n",x,x);

#define MAX_STUDENTS 500
#define MAX_LABS 500
#define MAX_COURSES 500
#define MAX_STRING_LENGTH 75  


typedef struct student
{
    int id;
    bool registered;
    float calibre;
    int pref[3];
    int curr_pref;
    int course_allocated;
    bool attended;
    int time;
    bool done;
} student;

typedef struct course
{
    int id;
    char name[MAX_STRING_LENGTH];
    float interest;
    int max_slots;
    int total_labs;
    int* lab_ids;
    bool removed;
} course;

typedef struct lab
{
    int id;
    char name[MAX_STRING_LENGTH];
    int capacity;
    int* lab_TAs;
    pthread_mutex_t* lk_TAs;
    int max_limit;
    bool flag;
} lab;

clock_t before;

pthread_cond_t allocated[MAX_STUDENTS];

int num_studs;
int num_labs;
int num_courses;

course c_list[MAX_COURSES];
pthread_t courseThread[MAX_COURSES];
pthread_mutex_t courseMutex[MAX_COURSES];

lab l_list[MAX_LABS];

student s_list[MAX_STUDENTS];
pthread_t studentThread[MAX_STUDENTS];
pthread_mutex_t studentMutex[MAX_STUDENTS];

int rng(int l, int r);
void* handle_students(void* arg);
void* handle_labs(void* arg);
void* handle_courses(void* arg);

#endif  //!__MAIN__H__
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q1/colors.h ####################//

#ifndef __COLORS__H__
#define __COLORS__H__

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m" 

#endif  //!__COLORS__H__