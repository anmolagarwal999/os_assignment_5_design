
//###########FILE CHANGE ./main_folder/Ritvik Aryan Kalra_305948_assignsubmission_file_/2019115002/q1/q1.c ####################//

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define TUT_TIME 5

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define DEBUG 1

typedef struct course {
    int id;
    char name[25];
    int course_max_slots;
    float interest;
    int n_labs;
    int *labs;
    int ta;
    int withdrawn;

    pthread_mutex_t course_lock;

} course;

typedef struct lab {
    int id;
    char name[20];
    int n_tas;
    int max_times;

    int *taught_courses;
    int *is_free;

    pthread_mutex_t *ta_lock;
    pthread_cond_t *ta_cond;
} lab;

typedef struct student {
    int id;
    float reg_time;
    int pref[3];
    int cur_pref;
    float calibre;
    int is_waiting;

    pthread_mutex_t student_lock;
    pthread_cond_t student_cond;

} student;

struct global_mutexes {
    pthread_mutex_t students_lock;
    pthread_mutex_t print_lock;
} global_locks;

struct global_students {
    int n_students;
    student *students;
    pthread_t *student_threads;
    int students_finished;
} global_students;

struct global_courses {
    int n_courses;
    course *courses;
    pthread_t *course_threads;
} global_courses;

struct global_labs {
    int n_labs;
    lab *ta_labs;
} global_labs;

void *course_thread(void *args) {
    course cur_course = *(course *)args;

    for (;;) {
        pthread_mutex_lock(&(global_locks.students_lock));
        if ((global_students.students_finished - global_students.n_students) ==
            0) {
            pthread_mutex_unlock(&(global_locks.students_lock));
            printf(
                "Course %s stopped as all students finalised/couldn't get "
                "their choices.\n",
                cur_course.name);
            return NULL;
        }
        pthread_mutex_unlock(&(global_locks.students_lock));

        int cur_ta, cur_lab;
        cur_ta = cur_lab = -1;

        for (int l = 0; l < cur_course.n_labs; l++) {
            for (int ta = 0; ta < global_labs.ta_labs[l].n_tas; ta++) {
                pthread_mutex_lock(&(global_labs.ta_labs[l]).ta_lock[ta]);

                if ((global_labs.ta_labs[l]).taught_courses[ta] <
                        (global_labs.ta_labs[l]).max_times &&
                    (global_labs.ta_labs[l]).is_free[ta]) {
                    cur_lab = l;
                    cur_ta = ta;
                    (global_labs.ta_labs[l]).is_free[ta] = 0;
                    (global_labs.ta_labs[l]).taught_courses[ta]++;

                    printf(RED
                           "TA %d from lab %s has been allocated to course %s "
                           "for %d TA ship\n" RESET,
                           ta, global_labs.ta_labs[l].name, cur_course.name,
                           global_labs.ta_labs[l].taught_courses[ta]);
                }

                pthread_mutex_unlock(&(global_labs.ta_labs[l]).ta_lock[ta]);

                if (cur_ta >= 0) {
                    break;
                }
            }

            if (cur_ta >= 0) {
                break;
            }
        }

        if (cur_ta < 0) {
            for (int l = 0; l < cur_course.n_labs; l++) {
                for (int ta = 0; ta < global_labs.ta_labs[l].n_tas; ta++) {
                    pthread_mutex_lock(&(global_labs.ta_labs[l]).ta_lock[ta]);

                    while (global_labs.ta_labs[l].taught_courses[ta] <
                               global_labs.ta_labs[l].max_times &&
                           !global_labs.ta_labs[l].is_free[ta]) {
                        pthread_cond_wait(
                            &(global_labs.ta_labs[l]).ta_cond[ta],
                            &(global_labs.ta_labs[l]).ta_lock[ta]);
                    }

                    if ((global_labs.ta_labs[l]).taught_courses[ta] <
                            (global_labs.ta_labs[l]).max_times &&
                        (global_labs.ta_labs[l]).is_free[ta]) {
                        cur_lab = l;
                        cur_ta = ta;
                        (global_labs.ta_labs[l]).is_free[ta] = 0;
                        (global_labs.ta_labs[l]).taught_courses[ta]++;
                        if (DEBUG) {
                            pthread_mutex_lock(&(global_locks.print_lock));
                            printf(RED
                                   "TA %d from lab %s has been allocated to "
                                   "course %s for %d TA ship\n" RESET,
                                   ta, global_labs.ta_labs[l].name,
                                   cur_course.name,
                                   global_labs.ta_labs[l].taught_courses[ta]);
                            pthread_mutex_unlock(&(global_locks.print_lock));
                        }
                    }

                    pthread_mutex_unlock(&(global_labs.ta_labs[l]).ta_lock[ta]);

                    if (cur_ta != -1) {
                        break;
                    }
                }
                if (cur_ta != -1) {
                    break;
                } else {
                    printf(GREEN
                           "Lab %s no longer has students available for TA "
                           "ship\n" RESET,
                           global_labs.ta_labs[l].name);
                }
            }
        }

        if (cur_ta == -1) {
            if (DEBUG) {
                printf("%swithdrw 1 %s course %s\n", RED, RESET,
                       cur_course.name);
            }

            pthread_mutex_lock(
                &(global_courses.courses[cur_course.id]).course_lock);

            if (DEBUG) {
                printf("%swithdrw 2 %s course %s\n", RED, RESET,
                       cur_course.name);
            }

            global_courses.courses[cur_course.id].withdrawn = 1;

            for (int s = 0; s < global_students.n_students; s++) {
                printf("%swithdrw%s student %d course %s\n", RED, RESET, s,
                       cur_course.name);
                pthread_mutex_lock(&(global_students.students[s]).student_lock);

                if (global_students.students[s].cur_pref == cur_course.id) {
                    global_students.students[s].is_waiting = 0;

                    pthread_cond_signal(
                        &(global_students.students[s]).student_cond);
                }
                pthread_mutex_unlock(
                    &(global_students.students[s]).student_lock);
            }

            pthread_mutex_unlock(
                &(global_courses.courses[cur_course.id]).course_lock);

            printf(YELLOW
                   "Course %s doesn’t have any TA’s eligible and is "
                   "removed from course offerings\n" RESET,
                   cur_course.name);

            printf("%swithdrew%s course %s\n", GREEN, RESET, cur_course.name);

            return NULL;
        }

        int total_slots = (rand() % cur_course.course_max_slots) + 1;
        printf(BLUE "Course %s has been allocated %d seats\n" RESET,
               cur_course.name, total_slots);
        int filled_slots = 0;
        int cur_students[total_slots + 5];

        for (int s = 0; s < global_students.n_students; s++) {
            pthread_mutex_lock(&(global_students.students[s]).student_lock);

            if (global_students.students[s].cur_pref == cur_course.id &&
                global_students.students[s].is_waiting) {
                cur_students[filled_slots] = s;
                filled_slots++;
                printf(MAGENTA
                       "Student %d has been allocated a seat in course "
                       "%s\n" RESET,
                       s, cur_course.name);
            }

            pthread_mutex_unlock(&(global_students.students[s]).student_lock);

            if (filled_slots >= total_slots) {
                break;
            }
        }

        printf(CYAN
               "Tutorial has started for Course %s with %d seats filled "
               "out of %d\n" RESET,
               cur_course.name, filled_slots, total_slots);

        sleep(5);

        pthread_mutex_lock(&(global_labs.ta_labs[cur_lab]).ta_lock[cur_ta]);
        global_labs.ta_labs[cur_lab].is_free[cur_ta] = 1;
        pthread_cond_broadcast(&(global_labs.ta_labs[cur_lab]).ta_cond[cur_ta]);
        pthread_mutex_unlock(&(global_labs.ta_labs[cur_lab]).ta_lock[cur_ta]);

        printf(MAGENTA
               "TA %d from lab %s has completed the tutorial and left the "
               "course %s\n" RESET,
               cur_ta, global_labs.ta_labs[cur_lab].name, cur_course.name);

        for (int i = 0; i < filled_slots; i++) {
            pthread_mutex_lock(
                &(global_students.students[cur_students[i]]).student_lock);

            global_students.students[cur_students[i]].is_waiting = 0;
            pthread_cond_signal(
                &(global_students.students[cur_students[i]]).student_cond);
            pthread_mutex_unlock(
                &(global_students.students[cur_students[i]]).student_lock);
        }
    }
}

void *student_thread(void *args) {
    int waiting_time = ((student *)args)->reg_time;
    sleep(waiting_time);
    student cur_student = *(student *)args;

    printf(YELLOW
           "Student %d has filled in preferences for course "
           "registration\n" RESET,
           cur_student.id);
    for (int i = 0; i < 3; i++) {
        pthread_mutex_lock(
            &(global_students.students[cur_student.id]).student_lock);
        global_students.students[cur_student.id].cur_pref =
            global_students.students[cur_student.id].pref[i];
        pthread_mutex_unlock(
            &(global_students.students[cur_student.id]).student_lock);

        pthread_mutex_lock(
            &(global_courses
                  .courses[global_students.students[cur_student.id].cur_pref])
                 .course_lock);
        int has_course_withdrawn =
            global_courses
                .courses[global_students.students[cur_student.id].cur_pref]
                .withdrawn;
        pthread_mutex_unlock(
            &(global_courses
                  .courses[global_students.students[cur_student.id].cur_pref])
                 .course_lock);

        pthread_mutex_lock(
            &(global_students.students[cur_student.id]).student_lock);
        if (!has_course_withdrawn) {
            global_students.students[cur_student.id].is_waiting = 1;
        } else {
            global_students.students[cur_student.id].is_waiting = 0;
        }

        while (global_students.students[cur_student.id].is_waiting) {
            pthread_cond_wait(
                &(global_students.students[cur_student.id]).student_cond,
                &(global_students.students[cur_student.id]).student_lock);
        }
        pthread_mutex_unlock(
            &(global_students.students[cur_student.id]).student_lock);

        pthread_mutex_lock(
            &(global_courses
                  .courses[global_students.students[cur_student.id].cur_pref])
                 .course_lock);
        has_course_withdrawn =
            global_courses
                .courses[global_students.students[cur_student.id].cur_pref]
                .withdrawn;
        pthread_mutex_unlock(
            &(global_courses
                  .courses[global_students.students[cur_student.id].cur_pref])
                 .course_lock);

        if (!has_course_withdrawn) {
            float finalising_prob =
                cur_student.calibre *
                global_courses
                    .courses[global_students.students[cur_student.id].cur_pref]
                    .interest;
            float outcome = (float)rand() / (float)(RAND_MAX);

            if (outcome <= finalising_prob) {
                printf(CYAN
                       "Student %d has selected course %s permanently\n" RESET,
                       cur_student.id,
                       global_courses
                           .courses[global_students.students[cur_student.id]
                                        .cur_pref]
                           .name);
                pthread_mutex_lock(
                    &(global_students.students[cur_student.id]).student_lock);
                global_students.students[cur_student.id].cur_pref = -1;
                pthread_mutex_unlock(
                    &(global_students.students[cur_student.id]).student_lock);

                pthread_mutex_lock(&(global_locks.students_lock));
                global_students.students_finished++;
                pthread_mutex_unlock(&(global_locks.students_lock));

                return NULL;
            }
            printf(
                "Student %d has withdrawn from course %s\n", cur_student.id,
                global_courses
                    .courses[global_students.students[cur_student.id].cur_pref]
                    .name);
        }

        if (i < 2) {
            printf(
                RED
                "Student %d has changed current preference from %s "
                "(priority %d) to %s (priority %d)\n" RESET,
                cur_student.id,
                global_courses
                    .courses[global_students.students[cur_student.id].pref[i]]
                    .name,
                i,
                global_courses
                    .courses[global_students.students[cur_student.id]
                                 .pref[i + 1]]
                    .name,
                i + 1);
        }
    }

    pthread_mutex_unlock(
        &(global_students.students[cur_student.id]).student_lock);
    printf("Student %d couldn’t get any of his preferred courses\n",
           cur_student.id);
    pthread_mutex_lock(&(global_locks.students_lock));
    global_students.students_finished++;
    pthread_mutex_unlock(&(global_locks.students_lock));

    return NULL;
}

void init() {
    fscanf(stdin, "%d %d %d", &global_students.n_students, &global_labs.n_labs,
           &global_courses.n_courses);

    global_students.students =
        malloc(sizeof(student) * global_students.n_students);
    global_labs.ta_labs = malloc(sizeof(lab) * global_labs.n_labs);
    global_courses.courses = malloc(sizeof(course) * global_courses.n_courses);

    for (int i = 0; i < global_courses.n_courses; i++) {
        scanf("%s %f %d %d", global_courses.courses[i].name,
              &(global_courses.courses[i]).interest,
              &(global_courses.courses[i]).course_max_slots,
              &(global_courses.courses[i]).n_labs);

        global_courses.courses[i].id = i;

        global_courses.courses[i].labs =
            malloc(sizeof(int) * global_courses.courses[i].n_labs);

        for (int j = 0; j < global_courses.courses[i].n_labs; j++) {
            scanf("%d", &(global_courses.courses[i]).labs[j]);
        }

        global_courses.courses[i].ta = -1;
        global_courses.courses[i].withdrawn = 0;

        pthread_mutex_init(&(global_courses.courses[i]).course_lock, NULL);
    }

    for (int i = 0; i < global_students.n_students; i++) {
        scanf("%f %d %d %d %f", &(global_students.students[i]).calibre,
              &(global_students.students[i]).pref[0],
              &(global_students.students[i]).pref[1],
              &(global_students.students[i]).pref[2],
              &(global_students.students[i]).reg_time);

        global_students.students[i].id = i;

        global_students.students[i].cur_pref = -1;
        global_students.students[i].is_waiting = 0;

        pthread_mutex_init(&(global_students.students[i]).student_lock, NULL);
        pthread_cond_init(&(global_students.students[i]).student_cond, NULL);
    }

    for (int i = 0; i < global_labs.n_labs; ++i) {
        scanf("%s %d %d", global_labs.ta_labs[i].name,
              &(global_labs.ta_labs[i]).n_tas,
              &(global_labs.ta_labs[i]).max_times);

        global_labs.ta_labs[i].id = i;

        global_labs.ta_labs[i].taught_courses =
            malloc(sizeof(int) * global_labs.ta_labs[i].n_tas);
        global_labs.ta_labs[i].is_free =
            malloc(sizeof(int) * global_labs.ta_labs[i].n_tas);
        global_labs.ta_labs[i].ta_lock =
            malloc(sizeof(pthread_mutex_t) * global_labs.ta_labs[i].n_tas);
        global_labs.ta_labs[i].ta_cond =
            malloc(sizeof(pthread_cond_t) * global_labs.ta_labs[i].n_tas);

        for (int j = 0; j < global_labs.ta_labs[i].n_tas; j++) {
            global_labs.ta_labs[i].taught_courses[j] = 0;
            global_labs.ta_labs[i].is_free[j] = 1;
            pthread_mutex_init(&(global_labs.ta_labs[i]).ta_lock[j], NULL);
            pthread_cond_init(&(global_labs.ta_labs[i]).ta_cond[j], NULL);
        }
    }

    global_students.students_finished = 0;
    pthread_mutex_init(&(global_locks.students_lock), NULL);

    global_courses.course_threads =
        malloc(sizeof(pthread_t) * global_courses.n_courses);
    global_students.student_threads =
        malloc(sizeof(pthread_t) * global_students.n_students);

    pthread_mutex_init(&(global_locks.print_lock), NULL);
}

int main() {
    init();

    for (int s = 0; s < global_students.n_students; s++) {
        pthread_create(&(global_students.student_threads[s]), NULL,
                       student_thread, &(global_students.students[s]));
    }
    for (int c = 0; c < global_courses.n_courses; c++) {
        pthread_create(&(global_courses.course_threads[c]), NULL, course_thread,
                       &(global_courses.courses[c]));
    }

    for (int s = 0; s < global_students.n_students; s++) {
        pthread_join(global_students.student_threads[s], NULL);
    }

    printf("Simulation Over\n");
}
