
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q1/labs.c ####################//

#include "uni.h"

void *lab(void *arg)
{
    int index = *(int*)arg;
    while(1)
    {
        rep(i,0,lbs[index].numTA)
        {
            if(lbs[index].teaching[i])
            {
                if(!lbs[index].slotsfilled[i])
                {
                    rep(j,0,num_students)
                    {
                        if(lbs[index].slots_select[i] == 0)break;
                        if(students[j].allocated_ta)continue;
                        int x = students[j].priority_level;
                        if(x==1 && lbs[index].current_course[i] == students[j].p1)
                        {
                            lbs[index].slots_select[i]--;
                            students[j].allocated_ta = true;
                        }
                        if(x==2 && lbs[index].current_course[i] == students[j].p2)
                        {
                            lbs[index].slots_select[i]--;
                            students[j].allocated_ta = true;
                        }
                        if(x==3 && lbs[index].current_course[i] == students[j].p3)
                        {
                            lbs[index].slots_select[i]--;
                            students[j].allocated_ta = true;
                        }
                    }
                    if(lbs[index].slots_select[i] == 0)
                    {
                        //printf("HI from false\n");
                        lbs[index].slotsfilled[i] = true;
                        pthread_cond_signal(&cond[lbs[index].current_course[i]]);
                        //printf("BYE from false\n");
                    }
                }

                if(lbs[index].slotsfilled[i])
                {
                    lbs[index].teaching[i] = false;
                    sleep(2);
                    printf("TA %d from lab %s has completed the tutorial for course %s\n",i, lbs[index].name,courses[lbs[index].current_course[i]].name);
                    rep(j,0,num_students)
                    {
                        int x = students[j].priority_level;
                        if(x==1 && lbs[index].current_course[i] == students[j].p1)
                            students[j].allocated_ta = false;
                        if(x==2 && lbs[index].current_course[i] == students[j].p2)
                        {
                            students[j].allocated_ta = false;
                            printf("OkDidi\n");
                        }
                        if(x==3 && lbs[index].current_course[i] == students[j].p3)
                            students[j].allocated_ta = false;
                    }
                    lbs[index].slotsfilled[i] = false;
                    pthread_cond_signal(&cond1[lbs[index].current_course[i]]);     
                }
            }
        }
    }
}
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q1/main.c ####################//

#include "uni.h"

int main()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);num_labs;
    courses  = malloc(num_courses * sizeof(struct course));
    students = malloc(num_students * sizeof(struct student));
    lbs      = malloc(num_labs * sizeof(struct lab));

    rep(i,0,num_courses)
        pthread_mutex_init(&cmutex[i], NULL);
    rep(i,0,num_labs)
        pthread_mutex_init(&lmutex[i], NULL);
    pthread_mutex_init(&smutex, NULL);

    rep(i,0,num_courses)
    {
        scanf("%s %Lf %d %d", courses[i].name, &courses[i].iquotient, &courses[i].slots, &courses[i].numlabs);
        courses[i].list = malloc(courses[i].numlabs * sizeof(int));
        rep(j,0,num_labs)
            scanf("%d", &courses[i].list[j]);
        courses[i].TAlloc = false;
        pthread_cond_init(&cond[i], NULL);
        pthread_cond_init(&cond1[i], NULL);
    }

    rep(i,0,num_students)
    {
        scanf("%Lf %d %d %d %d", &students[i].caliber, &students[i].p1, &students[i].p2, &students[i].p3, &students[i].time);
        students[i].priority_level = 0;
        students[i].allocated_ta = false;
    }

    rep(i,0,num_labs)
    {
        scanf("%s %d %d", lbs[i].name, &lbs[i].numTA, &lbs[i].numTimes);
        lbs[i].left = malloc(sizeof(int) * lbs[i].numTA);
        lbs[i].teaching = malloc(sizeof(bool) * lbs[i].numTA);
        lbs[i].slotsfilled = malloc(sizeof(bool) * lbs[i].numTA);
        lbs[i].slots_select = malloc(sizeof(int) * lbs[i].numTA);
        lbs[i].current_course = malloc(sizeof(int) * lbs[i].numTA);
        rep(j,0,lbs[i].numTA)
        {
            lbs[i].left[j] = lbs[i].numTimes;
            lbs[i].teaching[j] = false;
            lbs[i].slotsfilled[j] = false;
        }
    }

    rep(i,0,num_students)
    {
        int *x = malloc(sizeof(int));
        *x = i;
        pthread_create(&sthread[i], NULL, &stufunc, x);
    }
    rep(i,0,num_labs)
    {
        int *x = malloc(sizeof(int));
        *x = i;
        pthread_create(&lthread[i], NULL, &lab, x);
    }
    rep(i,0,num_courses)
    {
        int *x = malloc(sizeof(int));
        *x = i;
        pthread_create(&cthread[i], NULL, &course_alloc, x);
    }

    rep(i,0,num_students)
    {
        pthread_join(sthread[i], NULL);
    }
    rep(i,0,num_courses)
    {
        pthread_join(cthread[i], NULL);
    }
    rep(i,0,num_labs)
    {
        pthread_join(lthread[i], NULL);
    }

    rep(i,0,num_courses)
    {
        pthread_mutex_destroy(&cmutex[i]);
        pthread_cond_destroy(&cond[i]);
        pthread_cond_destroy(&cond1[i]);
    }
    rep(i,0,num_labs)
        pthread_mutex_destroy(&lmutex[i]);
    pthread_mutex_destroy(&smutex);
}

//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q1/students.c ####################//

#include "uni.h"

void *stufunc(void *arg)
{
    int id = *(int*)arg;
    sleep(students[id].time);
    printf("Student %d has filled in preference for course registration\n", id);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    students[id].priority_level++;
    pthread_mutex_lock(&cmutex[students[id].p1]); 
    while(!students[id].allocated_ta)
    {
        pthread_cond_wait(&cond[students[id].p1], &cmutex[students[id].p1]);
    }
    printf("Student %d has been allocated a seat in %s\n",id, courses[students[id].p1].name);
    while(students[id].allocated_ta)
    {
        pthread_cond_wait(&cond1[students[id].p1] , &cmutex[students[id].p1]);
    }
    if((double)rand() / (double)RAND_MAX < students[id].caliber * courses[students[id].p1].iquotient)
    {
        printf("Student %d has selected course %s permanantly\n", id, courses[students[id].p1].name);
        return NULL ;
    }
    else
    {
        printf("Student %d has withdrawn from course round 1 %s\n",id, courses[students[id].p1].name);
    }
    pthread_mutex_unlock(&cmutex[students[id].p1]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    students[id].priority_level++;
    pthread_mutex_lock(&cmutex[students[id].p2]); 
    while(!students[id].allocated_ta)
    {
        pthread_cond_wait(&cond[students[id].p2], &cmutex[students[id].p2]);
    }
    printf("Student %d has been allocated a seat in %s\n",id, courses[students[id].p2].name);
    while(students[id].allocated_ta)
    {
        pthread_cond_wait(&cond1[students[id].p2] , &cmutex[students[id].p2]);
    }
    if((double)rand() / (double)RAND_MAX < students[id].caliber * courses[students[id].p2].iquotient)
    {
        printf("Student %d has selected course %s permanantly round2\n", id, courses[students[id].p2].name);
        return NULL ;
    }
    else
    {
        printf("Student %d has withdrawn from course round 2 %s\n",id, courses[students[id].p2].name);
    }
    pthread_mutex_unlock(&cmutex[students[id].p2]);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    students[id].priority_level++;
    printf("HIIIIIIIIIIIIIIIIIIIIIIII\n");
    pthread_mutex_lock(&cmutex[students[id].p3]);
    while(!students[id].allocated_ta)
    {
        pthread_cond_wait(&cond[students[id].p3], &cmutex[students[id].p3]);
    }
    printf("Student %d has been allocated a seat in %s\n",id, courses[students[id].p3].name);
    while(students[id].allocated_ta)
    {
        pthread_cond_wait(&cond1[students[id].p3] , &cmutex[students[id].p3]);
    }
    if((double)rand() / (double)RAND_MAX < students[id].caliber * courses[students[id].p3].iquotient)
    {
        printf("Student %d has selected course %s permanantly\n", id, courses[students[id].p3].name);
        return NULL ;
    }
    else
    {
        printf("Student %d could not get any of his preferred choices\n",id);
    }
    pthread_mutex_unlock(&cmutex[students[id].p3]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q1/uni.h ####################//

#ifndef UNIVERSITY_H
#define UNIVERSITY_H


#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdbool.h>
#include<time.h>

#define MAX_STUDENTS 100000
#define MAX_COURSES  100000
#define MAX_LABS     100000
#define ld long double
#define rep(a,b,c) for(int a=b;a<c;a++)
struct student 
{
    ld    caliber;
    int   p1, p2, p3;
    int   time;
    bool  insim;
    int   priority_level;
    bool  allocated_ta;
};

struct course 
{
    char  name[20];
    int*  list;
    ld    iquotient;
    int   slots;
    int   numlabs;
    bool  TAlloc;
    bool  trigger;
    bool  active;
};

struct lab 
{
    char  name[20];
    int   numTA;
    int   numTimes;
    int * left;
    bool* teaching;
    bool* slotsfilled; 
    int * slots_select;
    int * current_course;
};

pthread_t sthread[MAX_STUDENTS];
pthread_t cthread[MAX_COURSES];
pthread_t lthread[MAX_LABS];

pthread_mutex_t cmutex[MAX_COURSES];
pthread_mutex_t lmutex[MAX_LABS];
pthread_mutex_t smutex;

pthread_cond_t cond  [MAX_COURSES];     //TA allocated?
pthread_cond_t cond1 [MAX_COURSES];     //TA un-allocated?

void *course_alloc(void *arg);
void *stufunc(void *arg);
void *lab(void *arg);

int mails;
int num_courses;
int num_students;
int num_labs;

struct course *courses;
struct student *students;
struct lab *lbs;

#endif
//###########FILE CHANGE ./main_folder/Yash Bhatia_305922_assignsubmission_file_/2020101007_Assignment5/q1/courses.c ####################//

#include "uni.h"

void *course_alloc(void *arg)
{
    int index = *(int *)arg;
    while(1)
    {
        bool fnd = false;
        int num_stud = 0;
        rep(i,0,num_students)
        {
            int x = students[i].priority_level;
            if(x == 1 && students[i].p1 == index)num_stud++;
            if(x == 2 && students[i].p2 == index)num_stud++;
            if(x == 3 && students[i].p3 == index)num_stud++;
        }
        if(num_stud == 0)continue;
        rep(i,0,courses[index].numlabs)
        {
            int lab_num = courses[index].list[i];
            rep(j,0,lbs[lab_num].numTA)
            {
                if(lbs[lab_num].left[j])
                {
                    if(fnd)break;


                    if(lbs[lab_num].teaching[j] == false && lbs[lab_num].left[j] > 0)
                    {
                        pthread_mutex_lock(&lmutex[lab_num]);
                        lbs[lab_num].left[j]--;
                        printf("TA %d from lab %s has been allocated to course %s for his %dth TA ship\n"
                        ,j,lbs[lab_num].name, courses[index].name, lbs[lab_num].numTimes - lbs[lab_num].left[j]);
                        fnd = true;
                        lbs[lab_num].slots_select[j] = rand()%courses[index].slots + 1;
                        lbs[lab_num].current_course[j] = index;
                        printf("Course %s has been allocated %d seats\n", courses[index].name, lbs[lab_num].slots_select[j]);
                        lbs[lab_num].teaching[j] = true;
                        pthread_mutex_unlock(&lmutex[lab_num]);
                    } 
                }
            }
        }
        pthread_cond_signal(&cond[index]);
    }
    free(arg);
}

