
//###########FILE CHANGE ./main_folder/Mayank Goel_305893_assignsubmission_file_/q1/q1.c ####################//

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct student
{
    long long int id;
    int fill_time;
    float calibre;
    int preferences[3];
} student;

typedef struct lab
{
    long long int id;
    char name[20];
    int num_tas;
    int max_courses;
} lab;

typedef struct course
{
    long long int id;
    char name[20];
    float interest;
    int course_max_slots;
    int num_labs;
    int *labs;
} course;

int num_students, num_labs, num_courses, ***lab_tas, *courses_spots, *student_status;
student *students;
lab *rlabs;
course *courses;
pthread_t *t_courses = NULL, *t_students = NULL;
pthread_mutex_t **lab_tas_lock, *courses_spots_lock, *student_status_lock;
pthread_cond_t **lab_tas_available, *courses_spots_available, *student_status_available;

void *simulate_student(void *args)
{

    student p_student = *(student *)args;
    int stud_id = p_student.id;
    sleep(p_student.fill_time);
    printf("Student %d has filled in preferences for course registration\n", stud_id);

    int current_pref = -1, current_pref_course = -1;
    for (long long int i = 0; i < 3; i++)
    {
        pthread_mutex_t cur_courses_spots_lock = courses_spots_lock[p_student.preferences[i]];
        pthread_mutex_lock(&cur_courses_spots_lock);
        if (courses_spots[p_student.preferences[i]] != -1 && current_pref_course == -1)
        {
            current_pref_course = courses_spots[p_student.preferences[i]];
            current_pref = i;
        }
        pthread_mutex_unlock(&cur_courses_spots_lock);
    }

    while (current_pref > -1)
    {

        int course_withdrawn = 0;

        pthread_mutex_t cur_student_lock = student_status_lock[stud_id];
        pthread_mutex_lock(&cur_student_lock);
        student_status[stud_id] = -1 * (current_pref_course + 1);
        pthread_mutex_unlock(&cur_student_lock);

        pthread_mutex_lock(&courses_spots_lock[current_pref_course]);

        while (!courses_spots[current_pref_course])
        {
            pthread_cond_wait(&courses_spots_available[current_pref_course], &courses_spots_lock[current_pref_course]);
        }
        if (courses_spots[current_pref_course] <= 0)
            course_withdrawn = 1;
        else
            courses_spots[current_pref_course]--;

        pthread_mutex_unlock(&courses_spots_lock[current_pref_course]);

        if (!course_withdrawn)
        {
            printf("Student %d has been allocated a seat in course %s\n", stud_id, courses[current_pref_course].name);

            pthread_mutex_lock(&student_status_lock[stud_id]);
            student_status[stud_id] = current_pref_course + 1;
            pthread_mutex_unlock(&student_status_lock[stud_id]);

            pthread_mutex_lock(&student_status_lock[stud_id]);
            pthread_cond_wait(&student_status_available[stud_id], &student_status_lock[stud_id]);
            pthread_mutex_unlock(&student_status_lock[stud_id]);

            float x = (float)rand() / (float)(RAND_MAX / 1.0);
            int condition = courses[current_pref_course].interest * p_student.calibre;
            if (x < condition)
            {
                printf("Student %d has selected course %s permanently\n", stud_id, courses[current_pref_course].name);
                pthread_mutex_lock(&student_status_lock[stud_id]);
                student_status[stud_id] = 0;
                pthread_mutex_unlock(&student_status_lock[stud_id]);

                return NULL;
            }
            else
            {
                printf("Student %d has withdrawn from course %s\n", stud_id, courses[current_pref_course].name);
                course_withdrawn = 1;
            }
        }
        if (course_withdrawn)
        {
            int previous_pref_course = current_pref_course;

            for (long long int i = current_pref + 1; i < 3; i++)
            {
                pthread_mutex_lock(&courses_spots_lock[p_student.preferences[i]]);
                int cur_stud_pref = courses_spots[p_student.preferences[i]];
                if (cur_stud_pref != -1)
                {
                    current_pref = i, current_pref_course = cur_stud_pref;
                }
                pthread_mutex_unlock(&courses_spots_lock[p_student.preferences[i]]);
            }
            if (previous_pref_course != current_pref_course)
                printf("Student %d has changed current preference from %s to %s\n", stud_id, courses[previous_pref_course].name, courses[current_pref_course].name);
            else
                current_pref = -1;
        }
    }

    if (current_pref == -1)
    {
        printf("Student %d did not get any of their preferred courses\n", stud_id);
    }

    return NULL;
}

void *simulate_course(void *args)
{
    course p_course = *(course *)args;
    int course_id = p_course.id;

    while (1)
    {

        int chosen_ta_lab_id = -1, chosen_ta_id = -1;

        for (int lab_no = 0; lab_no < p_course.num_labs; lab_no++)
        {

            for (int ta_num = 0; ta_num < rlabs[p_course.labs[lab_no]].num_tas; ta_num++)
            {

                pthread_mutex_lock(&lab_tas_lock[p_course.labs[lab_no]][ta_num]);

                while (lab_tas[p_course.labs[lab_no]][ta_num][0] > 0 && lab_tas[p_course.labs[lab_no]][ta_num][1] <= 0)
                {
                    pthread_cond_wait(&lab_tas_available[p_course.labs[lab_no]][ta_num], &lab_tas_lock[p_course.labs[lab_no]][ta_num]);
                }

                if (lab_tas[p_course.labs[lab_no]][ta_num][0] > 0 && lab_tas[p_course.labs[lab_no]][ta_num][1] > 0)
                {
                    lab_tas[p_course.labs[lab_no]][ta_num][0]--;
                    lab_tas[p_course.labs[lab_no]][ta_num][1] = 0;
                    chosen_ta_lab_id = p_course.labs[lab_no], chosen_ta_id = ta_num;
                }
                pthread_mutex_unlock(&lab_tas_lock[p_course.labs[lab_no]][ta_num]);

                if (chosen_ta_id != -1)
                    break;
            }

            if (chosen_ta_id != -1)
                break;
        }

        if (chosen_ta_id == -1)
        {

            pthread_mutex_lock(&courses_spots_lock[course_id]);
            courses_spots[course_id] = -1;
            pthread_mutex_unlock(&courses_spots_lock[course_id]);
            pthread_cond_broadcast(&courses_spots_available[course_id]);
            printf("Course %s does not have any TA's eligible and is removed from course offerings\n", p_course.name);

            return NULL;
        }

        pthread_mutex_t cur_lock = lab_tas_lock[chosen_ta_lab_id][chosen_ta_id];
        pthread_mutex_lock(&cur_lock);

        printf("TA %d from lab %s has been allocated to course %s for his %dst TA ship\n", chosen_ta_id, rlabs[chosen_ta_lab_id].name, p_course.name, rlabs[chosen_ta_lab_id].max_courses - lab_tas[chosen_ta_lab_id][chosen_ta_id][0]);
        int left_spots_in_lab = 0;
        pthread_mutex_unlock(&cur_lock);

        for (long long int i = 0; i < rlabs[chosen_ta_lab_id].num_tas; i++)
        {
            pthread_mutex_t cur_lock = lab_tas_lock[chosen_ta_lab_id][i];
            pthread_mutex_lock(&cur_lock);
            left_spots_in_lab += lab_tas[chosen_ta_lab_id][i][0];
            pthread_mutex_unlock(&cur_lock);
        }
        if (left_spots_in_lab == 0)
        {
            printf("Lab %s no longer has mentors available for TAship\n", rlabs[chosen_ta_lab_id].name);
        }

        int tut_slots = (rand() % p_course.course_max_slots) + 1;
        cur_lock = courses_spots_lock[course_id];
        pthread_mutex_lock(&cur_lock);
        courses_spots[course_id] = tut_slots;
        pthread_mutex_unlock(&cur_lock);

        printf("Course %s has been allocated %d seats\n", p_course.name, tut_slots);

        int waiting_students = tut_slots, filled_seats = 0;
        while ((filled_seats < 1) || (waiting_students != 0 && filled_seats < tut_slots))
        {
            pthread_cond_broadcast(&courses_spots_available[course_id]);

            waiting_students = 0, filled_seats = 0;
            for (long long int i = 0; i < num_students; i++)
            {
                pthread_mutex_lock(&student_status_lock[i]);
                if (student_status[i] == -1 * (course_id + 1))
                    waiting_students += 1;
                if (student_status[i] == (course_id + 1))
                {
                    filled_seats += 1;
                }
                pthread_mutex_unlock(&student_status_lock[i]);
            }

            if (waiting_students == 0)
                break;
        }

        filled_seats = 0;
        for (long long int i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(&student_status_lock[i]);
            if (student_status[i] == (course_id + 1))
                filled_seats += 1;
            pthread_mutex_unlock(&student_status_lock[i]);
        }

        printf("TA %d has started tutorial for Course %s with %d seats filled out of %d\n", chosen_ta_id, p_course.name, filled_seats, tut_slots);

        pthread_mutex_lock(&courses_spots_lock[course_id]);
        courses_spots[course_id] = 0;
        pthread_mutex_unlock(&courses_spots_lock[course_id]);

        sleep(3);

        for (long long int i = 0; i < num_students; i++)
        {
            pthread_mutex_lock(&student_status_lock[i]);
            if (student_status[i] == (course_id + 1))
            {
                pthread_cond_broadcast(&student_status_available[i]);
                student_status[i] = 0;
            }
            pthread_mutex_unlock(&student_status_lock[i]);
        }

        printf("TA %d from lab %s has completed the tutorial and left the course %s\n", chosen_ta_id, rlabs[chosen_ta_lab_id].name, p_course.name);

        pthread_mutex_t cur_ta_lock = lab_tas_lock[chosen_ta_lab_id][chosen_ta_id];
        pthread_mutex_lock(&cur_ta_lock);
        lab_tas[chosen_ta_lab_id][chosen_ta_id][1] = 1;
        pthread_cond_broadcast(&lab_tas_available[chosen_ta_lab_id][chosen_ta_id]);
        pthread_mutex_unlock(&cur_ta_lock);
    }

    return NULL;
}

int main()
{

#pragma region Input

    scanf("%d", &num_students);
    scanf("%d", &num_labs);
    scanf("%d", &num_courses);

    courses = malloc(sizeof(course) * num_courses);

    for (long long int i = 0; i < num_courses; ++i)
    {

        scanf("%s", courses[i].name);
        scanf("%f", &courses[i].interest);
        scanf("%d", &courses[i].course_max_slots);
        scanf("%d", &courses[i].num_labs);
        courses[i].id = i;
        courses[i].labs = malloc(4 * courses[i].num_labs);
        for (long long int ii = 0; ii < courses[i].num_labs; ii++)
        {
            scanf("%d", &courses[i].labs[ii]);
        }
    }

    students = malloc(sizeof(student) * num_students);

    for (long long int i = 0; i < num_students; ++i)
    {

        scanf("%f", &students[i].calibre);
        scanf("%d", &students[i].preferences[0]);
        scanf("%d", &students[i].preferences[1]);
        scanf("%d", &students[i].preferences[2]);
        scanf("%d", &students[i].fill_time);
        students[i].id = i;
    }

    rlabs = malloc(sizeof(lab) * num_labs);

    for (long long int i = 0; i < num_labs; ++i)
    {

        scanf("%s %d %d", rlabs[i].name, &rlabs[i].num_tas, &rlabs[i].max_courses);
        rlabs[i].id = i;
    }

#pragma endregion Input

    courses_spots_lock = malloc(sizeof(pthread_mutex_t) * num_courses), courses_spots_available = malloc(sizeof(pthread_cond_t) * num_courses), courses_spots = malloc(4 * num_courses);
    for (long long int i = 0; i < num_courses; ++i)
    {
        pthread_mutex_init(&courses_spots_lock[i], NULL), pthread_cond_init(&courses_spots_available[i], NULL);

        courses_spots[i] = 0;
    }

    lab_tas_lock = malloc(sizeof(pthread_mutex_t *) * num_labs), lab_tas_available = malloc(sizeof(pthread_cond_t *) * num_labs), lab_tas = malloc(sizeof(int **) * num_labs);
    for (long long int i = 0; i < num_labs; ++i)
    {
        lab_tas_lock[i] = malloc(sizeof(pthread_mutex_t) * rlabs[i].num_tas), lab_tas_available[i] = malloc(sizeof(pthread_cond_t) * rlabs[i].num_tas), lab_tas[i] = malloc(sizeof(int *) * rlabs[i].num_tas);
        for (long long int ii = 0; ii < rlabs[i].num_tas; ii++)
        {
            pthread_mutex_init(&lab_tas_lock[i][ii], NULL), pthread_cond_init(&lab_tas_available[i][ii], NULL);

            lab_tas[i][ii] = malloc(8);
            lab_tas[i][ii][0] = rlabs[i].max_courses;
            int assigned_stat = 1;
            lab_tas[i][ii][1] = assigned_stat;
        }
    }

    student_status_lock = malloc(sizeof(pthread_mutex_t) * num_students), student_status_available = malloc(sizeof(pthread_cond_t) * num_students), student_status = malloc(4 * num_students);
    for (long long int i = 0; i < num_students; ++i)
    {
        student_status[i] = students[i].preferences[0] + 1;
        student_status[i] *= -1;
        pthread_mutex_init(&student_status_lock[i], NULL), pthread_cond_init(&student_status_available[i], NULL);
    }

    t_courses = malloc(sizeof(pthread_t) * num_courses), t_students = malloc(sizeof(pthread_t) * num_students);
    for (long long int i = 0; i < num_students; i++)
    {
        pthread_create(&t_students[i], NULL, simulate_student, &students[i]);
    }
    for (long long int i = 0; i < num_courses; i++)
    {
        pthread_create(&t_courses[i], NULL, simulate_course, &courses[i]);
    }

    for (long long int i = 0; i < num_courses; i++)
    {
        pthread_join(t_courses[i], NULL);
    }
    for (long long int i = 0; i < num_students; i++)
    {
        pthread_join(t_students[i], NULL);
    }

    for (long long int i = 0; i < num_courses; ++i)
    {
        free(courses[i].labs);
    }
    free(courses), free(students), free(rlabs);
    free(courses_spots_lock), free(courses_spots_available), free(courses_spots);
    for (long long int i = 0; i < num_labs; ++i)
    {
        for (long long int ii = 0; ii < rlabs[i].num_tas; ii++)
        {
            free(lab_tas[i][ii]);
        }
        free(lab_tas[i]), free(lab_tas_lock[i]), free(lab_tas_available[i]);
    }
    free(lab_tas_available), free(lab_tas_lock), free(lab_tas);

    free(student_status), free(student_status_lock), free(student_status_available);

    free(t_courses), free(t_students);

    return 0;
}
