
//###########FILE CHANGE ./main_folder/Shivansh S_305819_assignsubmission_file_/final_assignment/q1/inputs.c ####################//

#include "header.h"

int getInput(char* tokens[], char* delims) {
    int index = 0;
    size_t len = MAXLEN;
    char* line = (char *) malloc(sizeof(char)*len);
    if(!fgets(line, len, stdin)) {
        printf("Error Number: %d\n", errno);
        perror("getInput, utils.c");
    } else {
        char* save_pointer;
        char* args = strtok_r(line, delims, &save_pointer);
        while(args!=NULL) {
            tokens[index] = args;
            index++;
            args = strtok_r(NULL, delims, &save_pointer);
        }
        tokens[index] = '\0';
    }
    return index;

}

void addCourse(char* tokens[]) {
   Courses[clen].name = tokens[0];
   Courses[clen].interest = atof(tokens[1]);
   Courses[clen].max_slots = atoi(tokens[2]);
   Courses[clen].num_labs = atoi(tokens[3]);
   Courses[clen].index = clen;
   int cindex = 0;
   int carg = 4;
   while(cindex < Courses[clen].num_labs) {
       Courses[clen].labids[cindex] = atoi(tokens[carg]);
       carg++;
       cindex++;
   }
   Courses[clen].labids[cindex] = '\0';
   Courses[clen].ta_id = -1;
   Courses[clen].ta_lab = -1;
   pthread_mutex_init(&Courses[clen].c_lock, NULL);
   pthread_cond_init(&Courses[clen].c_wait, NULL);
   Courses[clen].t_index = 0;
   Courses[clen].slots = 0;
   clen++;
}

void addStudent(char* tokens[]) {
    Students[slen].index = slen;
    Students[slen].calibre =  atof(tokens[0]);
    Students[slen].p1 = atoi(tokens[1]);
    Students[slen].p2 = atoi(tokens[2]);
    Students[slen].p3 = atoi(tokens[3]);
    Students[slen].fill_time = atoi(tokens[4]);
    Students[slen].inCourse = 0;
    Students[slen].current = Students[slen].p1;
    Students[slen].current_id = 1;
    pthread_mutex_init(&Students[slen].s_lock, NULL);
    pthread_cond_init(&Students[slen].wait_course, NULL);
    slen++;
}

void addLab(char* tokens[]) {
    Labs[llen].name = tokens[0];
    Labs[llen].num_std = atoi(tokens[1]);
    Labs[llen].repeat = atoi(tokens[2]);
    Labs[llen].isFree = (Labs[llen].repeat == 0)? 0: Labs[llen].num_std;
    for(int i=0;i<Labs[llen].num_std;i++) {
        Labs[llen].turns[i] = Labs[llen].repeat;
        Labs[llen].occupied[i] = 0;
        pthread_mutex_init(&Labs[llen].ta_lock[i], NULL);
    }
    llen++;
} 
//###########FILE CHANGE ./main_folder/Shivansh S_305819_assignsubmission_file_/final_assignment/q1/main.c ####################//

#include "header.h"

struct course *Courses = NULL;
struct student *Students = NULL;
struct lab *Labs = NULL;
int clen = 0;
int slen = 0;
int llen = 0;

int main () {
    char* delims = " \t\n";
    char* firstLine[MAXLEN];
    int firstLineLen = getInput(firstLine, delims);

    int num_students = atoi(firstLine[0]);
    int num_labs = atoi(firstLine[1]);
    int num_courses = atoi(firstLine[2]);
    
    Courses = (struct course*) malloc(sizeof(struct course)*num_courses);
    char* courseLine[MAXLEN];
    while (clen < num_courses) {
        int courseLineLen = getInput(courseLine, delims);
        addCourse(courseLine);
    }

    Students = (struct student*) malloc(sizeof(struct student)*num_students);
    char* studentLine[MAXLEN];
    while(slen < num_students) {
        int studentLineLem = getInput(studentLine, delims);
        addStudent(studentLine);
    }

    Labs = (struct lab*) malloc(sizeof(struct lab)*num_labs);
    char* labLine[MAXLEN];
    while(llen < num_labs) {
        int labLineLen = getInput(labLine, delims);
        addLab(labLine);
    }

    pthread_t s_thread;
    pthread_create(&s_thread, NULL, sMain, NULL);
    pthread_t c_thread;
    pthread_create(&c_thread, NULL, cMain, NULL);

    pthread_join(s_thread, NULL);
    pthread_join(c_thread, NULL);
    free(Courses);
    free(Students);
    free(Labs);

    return 0;
}
//###########FILE CHANGE ./main_folder/Shivansh S_305819_assignsubmission_file_/final_assignment/q1/students.c ####################//

#include "header.h"

void slotme(void* args) {
    struct student *myStudent = (struct student*) args; 
    struct course *myCourse = &Courses[myStudent->p1];
    pthread_mutex_lock(&myCourse->c_lock);

    // Waiting for allocation
    while(myCourse->t_index >= myCourse->slots) {
        pthread_cond_wait(&myStudent->wait_course, &myCourse->c_lock);
    }
    myStudent->inCourse = 1;
    myCourse->t_index += 1;
    if(myCourse->t_index <= myCourse->slots) {
        pthread_cond_signal(&myCourse->c_wait);
    }
    printf("%sStudent %d has been allocated a seat in course %s%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
    pthread_mutex_unlock(&myCourse->c_lock);

    // Wait for course to end
    pthread_mutex_lock(&myStudent->s_lock);
    while(myStudent->inCourse == 1) {
        pthread_cond_wait(&myStudent->wait_course, &myStudent->s_lock);
    }
    double prob = myCourse->interest * myStudent->calibre;
    double rand_num = rand()/((double) RAND_MAX);
    if (rand_num < prob) {
        printf("%sStudent %d has withdrawn from course %s%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
        if(myStudent->current_id == 1) {
            myStudent->current = myStudent->p2;
            myStudent->current_id = 2;
        } else if (myStudent->current_id == 2) {
            myStudent->current = myStudent->p3;
            myStudent->current_id = 3;
        }
    }
    else {
        printf("%sStudent %d has selected the course %s permanently%s\n", GREEN, myStudent->index, myCourse->name, GREEN);
    }
    pthread_mutex_unlock(&myStudent->s_lock);
}

void* enroll(void* args) {
    struct student *myStudent = (struct student*) args; 
    sleep(myStudent->fill_time);
    printf("%sStudent %d has filled in preferences for course registeration%s\n",GREEN, myStudent->index, GREEN);
    slotme((void *)myStudent);
}

void* sMain(void* args) {
    pthread_t threads[slen];
    int response = 0;
    for(int i=0;i<slen;i++) {
        response = pthread_create(&threads[i], NULL, enroll, (void *)&Students[i]);
        if(response < 0) {
            printf("Error Number: %d\n", errno);
            perror("sMain");
        }
    }

    for(int i=0;i<slen;i++) {
        pthread_join(threads[i], NULL);
    }
    
}
//###########FILE CHANGE ./main_folder/Shivansh S_305819_assignsubmission_file_/final_assignment/q1/header.h ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>

#define MAXLEN 2048
#define BLUE "\033[0;34m"
#define CYAN "\033[0;36m"
#define RED "\033[31m"
#define WHITE "\033[0;37m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"

struct student {
    int index;
    int inCourse;
    double calibre;
    int current;
    int current_id;
    int p1;
    int p2;
    int p3;
    pthread_mutex_t s_lock;
    pthread_cond_t wait_course;
    int fill_time;
};
struct course {
    int index;
    char* name;
    double interest;
    int max_slots;
    int num_labs;
    pthread_mutex_t c_lock;
    pthread_cond_t c_wait;
    int ta_id;
    int ta_lab;
    int labids[MAXLEN];
    int t_index;
    int slots;
};
struct lab {
    char* name;
    int num_std;
    int repeat;
    int isFree;
    pthread_mutex_t ta_lock[MAXLEN];
    int turns[MAXLEN];
    int occupied[MAXLEN];
};

extern int errno;
extern struct course *Courses;
extern int clen;
extern struct student *Students;
extern int slen;
extern struct lab *Labs;
extern int llen;

int main(void);
int getInput(char* tokens[], char* delims);
void addCourse(char* tokens[]);
void addStudent(char* tokens[]);
void addLab(char* tokens[]);
void* sMain(void* args);
void* cMain(void* args);
//###########FILE CHANGE ./main_folder/Shivansh S_305819_assignsubmission_file_/final_assignment/q1/courses.c ####################//

#include "header.h"

void* allocate(void* args) {
    struct course *myCourse = (struct course*) args;
    for(int i=0;i<myCourse->num_labs;i++) {
        struct lab *myLab = &Labs[myCourse->labids[i]];
            for(int j=0;j<myLab->num_std;j++) {
                pthread_mutex_lock(&myLab->ta_lock[j]);
                if (myLab->turns[j] > 0 && myLab->occupied[j] == 0) {
                    myLab->turns[j] -= 1;
                    myLab->occupied[j] = 1;
                    myCourse->ta_id = j;
                    myCourse->ta_lab = myCourse->labids[i];
                    printf("%sTA %d from lab %s has been allocated to course %s for his %d TA ship%s\n", BLUE, j, myLab->name, myCourse->name, (myLab->repeat - myLab->turns[j]), BLUE);
                }
                pthread_mutex_unlock(&myLab->ta_lock[j]);
                if(myCourse->ta_id != -1) {
                    break;
                }
            }
        if(myCourse->ta_id != -1) {
            break;
        }
    }
    int slots = rand()%(myCourse->max_slots - 1 + 1) + 1;
    pthread_mutex_lock(&myCourse->c_lock);
    myCourse->t_index = 0;
    int student_interest = 0;
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        if (myStudent->current == myCourse->index) {
            student_interest++;
        }
    }
    myCourse->slots = (student_interest >= slots) ? slots: student_interest;
    pthread_mutex_unlock(&myCourse->c_lock);
    printf("%sCourse %s has been allocated %d seats%s\n", RED, myCourse->name, myCourse->slots, RED);
    
    pthread_mutex_lock(&myCourse->c_lock);
    while(myCourse->t_index < myCourse->slots) {
        pthread_cond_wait(&myCourse->c_wait, &myCourse->c_lock);
    }
    pthread_mutex_unlock(&myCourse->c_lock);

    // take the tutorial
    printf("%sTutorial has started for Course %s with %d seats filled out of %d%s\n", RED, myCourse->name, myCourse->t_index, myCourse->slots, RED);
    sleep(5);
    struct lab *myLab = &Labs[myCourse->ta_lab];
    printf("%sTA %d from lab %s has completed the tutorial for course %s%s\n", BLUE, myCourse->ta_id, myLab->name, myCourse->name, BLUE);
    for(int i=0;i<slen;i++) {
        struct student* myStudent = &Students[i];
        pthread_mutex_lock(&myStudent->s_lock);
        if (myStudent->current == myCourse->index) {
            myStudent->inCourse = 0;
            pthread_cond_signal(&myStudent->wait_course);
        }
        pthread_mutex_unlock(&myStudent->s_lock);
    }
//    pthread_mutex_unlock(&myCourse->c_lock);
}

void* cMain(void* args) {
    pthread_t threads[clen];
    int response = 0;
    for(int i=0;i<clen;i++) {
        response = pthread_create(&threads[i], NULL, allocate, (void *)&Courses[i]);
        if(response < 0) {
            printf("Error Number: %d\n", errno);
            perror("cmain");
        }
    }

    for(int i=0;i<clen;i++) {
        pthread_join(threads[i], NULL);
    }
}
