
//###########FILE CHANGE ./main_folder/Tanmay Goyal_305861_assignsubmission_file_/q1/q1.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <semaphore.h>
#include <pthread.h>

#define ll long long
#define PI 3.14159265
#define br printf("\n")
#define fo(i, n) for (int i = 0; i < n; i++)
#define Fo(i, k, n) for (int i = k; k < n ? i < n : i > n; k < n ? i += 1 : i -= 1)
#define file_read                     \
    freopen("input.txt", "r", stdin); \
    freopen("output.txt", "w", stdout);
int m = 1e9 + 7;

#define MAX_NAME 100
#define MAX_LABS 100
#define MAX_TAS 100
#define MAX_COURSES 100
#define MAX_STUDS 1000

struct course
{
    char name[MAX_NAME];
    int id;
    float interest;
    int max_course_slot;
    int w;
    int curr_filled_slot;
    int num_labs_ta;
    int *lab_id;
    int ta_status;
    int tut_status;
    int exit_status;
    bool stat;
    bool check;
};

struct student
{
    float calibre;
    int id;
    int pref_1;
    int pref_2;
    int pref_3;
    int time;
    int status;
    int iter;
    bool stat;
};

struct lab
{
    int id;
    char name[MAX_NAME];
    int num_ta;
    int times_ta;
};

struct ta
{
    int lab_id;
    int ta_id;
    int completed;
    int curr_cid;
};

typedef struct course course;
typedef struct student student;
typedef struct lab lab;
typedef struct ta ta;

int num_stud, num_lab, num_course;
lab labs_data[MAX_LABS];
course courses_data[MAX_COURSES];
student students_data[MAX_STUDS];
ta ta_data[MAX_LABS][MAX_TAS];
pthread_mutex_t mutex_ta = PTHREAD_MUTEX_INITIALIZER;                    // for ta
pthread_cond_t cond_course[MAX_COURSES] = {PTHREAD_COND_INITIALIZER};    // for course
pthread_mutex_t mutex_course[MAX_COURSES] = {PTHREAD_MUTEX_INITIALIZER}; // for course
pthread_cond_t cond_student[MAX_STUDS] = {PTHREAD_COND_INITIALIZER};     // for student
pthread_mutex_t mutex_student[MAX_STUDS] = {PTHREAD_MUTEX_INITIALIZER};  // for student
pthread_mutex_t ongoing_tut = PTHREAD_MUTEX_INITIALIZER;                 // for tut
pthread_cond_t cond_tut = PTHREAD_COND_INITIALIZER;                      // for tut

ta selected[MAX_COURSES];

bool check(int id, course *c)
{
    fo(i, c->num_labs_ta)
    {
        if (c->lab_id[i] == id)
            return true;
    }
    return false;
}

void *init_course(void *arg)
{
    course *c = (course *)arg;
    while (true)
    {
        c->stat = false;
        fo(i, MAX_LABS)
        {
            fo(j, MAX_TAS)
            {
                if (check(ta_data[i][j].lab_id, c) && (ta_data[i][j].completed != labs_data[i].times_ta))
                {
                    if (ta_data[i][j].curr_cid == -1)
                    {
                        int w = (rand() % c->max_course_slot) + 1;
                        pthread_mutex_lock(&mutex_course[c->id]);
                        c->w = w;
                        printf("Course %s has been allocated %d seats\n", c->name, c->w);
                        pthread_mutex_lock(&mutex_ta);
                        ta_data[i][j].curr_cid = c->id;
                        c->ta_status = 1;
                        pthread_cond_broadcast(&cond_course[c->id]);
                        ta_data[i][j].completed++;
                        selected[c->id] = ta_data[i][j];
                        pthread_mutex_unlock(&mutex_ta);
                        pthread_mutex_unlock(&mutex_course[c->id]);
                        if (ta_data[i][j].completed == 1)
                        {
                            printf("TA %d from lab %s has been allocated to course %s for his 1st TA ship\n", j, labs_data[i].name, c->name);
                        }
                        else if (ta_data[i][j].completed == 2)
                        {
                            printf("TA %d from lab %s has been allocated to course %s for his 2nd TA ship\n", j, labs_data[i].name, c->name);
                        }
                        else if (ta_data[i][j].completed == 3)
                        {
                            printf("TA %d from lab %s has been allocated to course %s for his 3rd TA ship\n", j, labs_data[i].name, c->name);
                        }
                        else
                        {
                            printf("TA %d from lab %s has been allocated to course %s for his %dth TA ship\n", j, labs_data[i].name, c->name, ta_data[i][j].completed);
                        }
                        c->stat = true;
                        break;
                    }
                }
            }
            if (c->stat)
            {
                break;
            }
        }
        sleep(5);
        if (c->stat)
        {
            printf("Tutorial has started for the course %s with %d seats filled out of %d\n", c->name, c->curr_filled_slot, c->w);
            printf("TA %d from lab %s has completed the tutorial and left the course %s\n", selected[c->id].ta_id, labs_data[selected[c->id].lab_id].name, c->name);
            c->tut_status = 1;
            c->ta_status = 0;
            pthread_mutex_lock(&ongoing_tut);
            pthread_cond_broadcast(&cond_tut);
            pthread_mutex_unlock(&ongoing_tut);
        }
        else 
        {
            c->tut_status = 1;
            c->ta_status = 0;
        }
        int labval = -1;
        c->check = true;
        fo(i, MAX_LABS)
        {
            fo(j, MAX_TAS)
            {
                if (ta_data[i][j].completed != labs_data[i].times_ta)
                {
                    c->check = false;
                    labval = i;
                    break;
                }
            }
        }
        if (c->check)
        {
            printf("Lab %s no longer has students available for TA ship\n", labs_data[labval].name);
        }
        bool check_for_exit = true;
        fo(i, c->num_labs_ta)
        {
            fo(j, labs_data[c->lab_id[i]].num_ta)
            {
                if (ta_data[c->lab_id[i]][j].completed != labs_data[c->lab_id[i]].times_ta)
                {
                    check_for_exit = false;
                    break;
                }
                if (!check_for_exit)
                    break;
            }
        }
        if (check_for_exit)
        {
            c->exit_status = 1;
            printf("Course %s doesn't have any TA's eligible and is removed from course offerings\n", c->name);
            pthread_mutex_lock(&mutex_course[c->id]);
            c->ta_status = 1;
            pthread_cond_broadcast(&cond_course[c->id]);
            pthread_mutex_unlock(&mutex_course[c->id]);
            break;
        }
    }
    return NULL;
}

void *init_stud(void *arg)
{
    student *stud = (student *)arg;
    sleep(stud->time);
    printf("Student %d has filled in preferences for course registration\n", stud->id);
    stud->status = 0;
    while (true)
    {
        course *c = NULL;
        stud->stat = false;
        if (stud->status == 0)
        {
            pthread_mutex_lock(&mutex_course[stud->pref_1]);
            while (courses_data[stud->pref_1].ta_status == 0)
            {
                pthread_cond_wait(&cond_course[stud->pref_1], &mutex_course[stud->pref_1]);
            }
            pthread_mutex_unlock(&mutex_course[stud->pref_1]);
            if (courses_data[stud->pref_1].curr_filled_slot < courses_data[stud->pref_1].w && courses_data[stud->pref_1].exit_status == 0)
            {
                printf("Student %d has been allocated a seat in course %s\n", stud->id, courses_data[stud->pref_1].name);
                stud->stat = true;
                pthread_mutex_lock(&mutex_course[stud->pref_1]);
                courses_data[stud->pref_1].curr_filled_slot++;
                pthread_mutex_unlock(&mutex_course[stud->pref_1]);
                c = &courses_data[stud->pref_1];
            }
        }
        else if (stud->status == 1)
        {
            pthread_mutex_lock(&mutex_course[stud->pref_2]);
            while (courses_data[stud->pref_2].ta_status == 0)
            {
                pthread_cond_wait(&cond_course[stud->pref_2], &mutex_course[stud->pref_2]);
            }
            pthread_mutex_unlock(&mutex_course[stud->pref_2]);
            if (courses_data[stud->pref_2].curr_filled_slot < courses_data[stud->pref_2].w && courses_data[stud->pref_2].exit_status == 0)
            {
                printf("Student %d has been allocated a seat in course %s\n", stud->id, courses_data[stud->pref_2].name);
                stud->stat = true;
                pthread_mutex_lock(&mutex_course[stud->pref_2]);
                courses_data[stud->pref_2].curr_filled_slot++;
                pthread_mutex_unlock(&mutex_course[stud->pref_2]);
                c = &courses_data[stud->pref_2];
            }
        }
        else if (stud->status == 2)
        {
            pthread_mutex_lock(&mutex_course[stud->pref_3]);
            while (courses_data[stud->pref_3].ta_status == 0)
            {
                pthread_cond_wait(&cond_course[stud->pref_3], &mutex_course[stud->pref_3]);
            }
            pthread_mutex_unlock(&mutex_course[stud->pref_3]);
            if (courses_data[stud->pref_3].curr_filled_slot < courses_data[stud->pref_3].w && courses_data[stud->pref_3].exit_status == 0)
            {
                printf("Student %d has been allocated a seat in course %s\n", stud->id, courses_data[stud->pref_3].name);
                stud->stat = true;
                pthread_mutex_lock(&mutex_course[stud->pref_3]);
                courses_data[stud->pref_3].curr_filled_slot++;
                pthread_mutex_unlock(&mutex_course[stud->pref_3]);
                c = &courses_data[stud->pref_3];
            }
        }
        if (stud->status == 3)
        {
            printf("Student %d could not get any of his preferred courses\n", stud->id);
            break;
        }
        fo(i, num_course)
        {
            pthread_mutex_lock(&ongoing_tut);
            pthread_cond_wait(&cond_tut, &ongoing_tut);
            pthread_mutex_unlock(&ongoing_tut);
        }
        if (stud->stat)
        {
            if (courses_data[c->id].interest * stud->calibre >= (double)rand() / RAND_MAX)
            {
                stud->status = -1;
                printf("Student %d has selected the course %s permanently\n", stud->id, courses_data[c->id].name);
                break;
            }
            else
            {
                stud->status++;
                printf("Student %d has withdrawn from the course %s\n", stud->id, courses_data[c->id].name);
                if (stud->status == 1)
                {
                    printf("Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n", stud->id, courses_data[stud->pref_1].name, courses_data[stud->pref_2].name);
                }
                else if (stud->status == 2)
                {
                    printf("Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n", stud->id, courses_data[stud->pref_2].name, courses_data[stud->pref_3].name);
                }
            }
        }
        else
        {
            if (stud->status == 0 && courses_data[stud->pref_1].exit_status == 1)
            {
                stud->status = 1;
            }
            else if (stud->status == 1 && courses_data[stud->pref_2].exit_status == 1)
            {
                stud->status = 2;
            }
            else if (stud->status == 2 && courses_data[stud->pref_3].exit_status == 1)
            {
                stud->status = 3;
            }
        }
    }
    return NULL;
}

int main()
{

    srand(time(0));
    scanf("%d %d %d", &num_stud, &num_lab, &num_course);

    pthread_t stud_threads[num_stud];
    pthread_t course_threads[num_course];

    fo(i, num_course)
    {
        course c;
        scanf("%s %f %d %d", c.name, &c.interest, &c.max_course_slot, &c.num_labs_ta);
        c.lab_id = (int *)malloc(sizeof(int) * c.num_labs_ta);
        fo(j, c.num_labs_ta)
        {
            scanf("%d", &c.lab_id[j]);
        }
        c.id = i;
        c.curr_filled_slot = 0;
        c.tut_status = 0;
        c.ta_status = 0;
        c.exit_status = 0;
        c.w = c.max_course_slot;
        courses_data[i] = c;
    }

    fo(i, num_stud)
    {
        student s;
        scanf("%f %d %d %d %d", &s.calibre, &s.pref_1, &s.pref_2, &s.pref_3, &s.time);
        s.id = i;
        s.status = -2;
        s.iter = 0;
        students_data[i] = s;
    }

    fo(i, MAX_LABS)
    {
        ta t;
        t.lab_id = -1;
        t.ta_id = 0;
        t.completed = 0;
        t.curr_cid = -1;
        fo(j, MAX_TAS)
        {
            ta_data[i][j] = t;
        }
    }

    fo(i, num_lab)
    {
        lab l;
        scanf("%s %d %d", l.name, &l.num_ta, &l.times_ta);
        l.id = i;
        fo(j, l.num_ta)
        {
            ta t;
            t.lab_id = i;
            t.ta_id = j;
            t.curr_cid = -1;
            ta_data[i][j] = t;
        }
        labs_data[i] = l;
    }

    fo(i, num_stud)
    {
        pthread_create(&stud_threads[i], NULL, init_stud, &students_data[i]);
    }

    sleep(1);

    fo(i, num_course)
    {
        pthread_create(&course_threads[i], NULL, init_course, &courses_data[i]);
    }

    fo(i, num_stud)
    {
        pthread_join(stud_threads[i], NULL);
    }
}