
//###########FILE CHANGE ./main_folder/Sriram Devata_305951_assignsubmission_file_/2019113007/q1/main.c ####################//

#include <stdio.h>
#include <errno.h>
#include "structs.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int num_students, num_labs, num_courses;
student *students;
lab *rlabs;
course *courses;

pthread_t *t_courses = NULL, *t_students = NULL;

// lab_tas has the information about the remaining courses a TA can take and if the TA is busy
pthread_mutex_t **lab_tas_lock;
pthread_cond_t **lab_tas_available;
int ***lab_tas;

// courses_spots is the number of available spots in a course after it is allocated a TA
// -1 if it has been withdrawn
pthread_mutex_t *courses_spots_lock;
pthread_cond_t *courses_spots_available;
int *courses_spots;

// student_status is the status of a student
// if >= 0, it is the course where the student is attending a tutorial or has registered
// -i if the student is waiting for course i to be allocated
int *student_status;
pthread_mutex_t *student_status_lock;
pthread_cond_t *student_status_available;


void *simulate_student(void *args) {
    student p_student = *(student *)args;

    sleep(p_student.fill_time);
    printf(ANSI_COLOR_RED "Student %d has filled in preferences for course registration" ANSI_COLOR_RESET "\n", p_student.id);

    int current_pref = -1; // preference number - 0,1,2
    int current_pref_course = -1; // course id of the currently preferred course
    for (int i = 0; i < 3; i++){
        pthread_mutex_lock(&courses_spots_lock[p_student.preferences[i]]);
        if(courses_spots[p_student.preferences[i]] != -1 && current_pref_course == -1) {
            current_pref = i;
            current_pref_course = courses_spots[p_student.preferences[i]];
        }
        pthread_mutex_unlock(&courses_spots_lock[p_student.preferences[i]]);
    }

    while(current_pref > -1) {

        printf(ANSI_COLOR_GREEN "Student %d is looking for course %s" ANSI_COLOR_RESET" \n", p_student.id, courses[current_pref_course].name);

        int course_withdrawn = 0;

        pthread_mutex_lock(&student_status_lock[p_student.id]);
        student_status[p_student.id] = -1*(current_pref_course+1); // student_status has -(pref_course+1), meaning the student is waiting
        // the +1 is because course ids are 0-indexed, otherwise there'd
        // be no difference between -0 and +0
        pthread_mutex_unlock(&student_status_lock[p_student.id]);    

        pthread_mutex_lock(&courses_spots_lock[current_pref_course]);
        // wait for spots in the course to open up if there are no spots
        while(courses_spots[current_pref_course] == 0) {
            pthread_cond_wait(&courses_spots_available[current_pref_course], &courses_spots_lock[current_pref_course]);
        }
        // if this thread wakes up and the course is withdrawn, save the bool course_withdrawn
        // if the thread wakes up because of spots being available
        if (courses_spots[current_pref_course] > 0) {
            courses_spots[current_pref_course]--;
        }
        else {
            course_withdrawn = 1;
        }
        pthread_mutex_unlock(&courses_spots_lock[current_pref_course]);

        if(!course_withdrawn){
            printf(ANSI_COLOR_YELLOW "Student %d has been allocated a seat in course %s" ANSI_COLOR_RESET "\n", p_student.id, courses[current_pref_course].name);

            pthread_mutex_lock(&student_status_lock[p_student.id]);
            student_status[p_student.id] = current_pref_course+1;
            pthread_mutex_unlock(&student_status_lock[p_student.id]);

            pthread_mutex_lock(&student_status_lock[p_student.id]);
            while(student_status[p_student.id] != 0){
                pthread_cond_wait(&student_status_available[p_student.id], &student_status_lock[p_student.id]);
            }
            pthread_mutex_unlock(&student_status_lock[p_student.id]);

            // choose to finalize or withdraw from course
            float x = (float)rand()/(float)(RAND_MAX/1.0);
            if (x < courses[current_pref_course].interest * p_student.calibre) {
                printf(ANSI_COLOR_BLUE "Student %d has selected course %s permanently" ANSI_COLOR_RESET "\n", p_student.id, courses[current_pref_course].name);
                pthread_mutex_lock(&student_status_lock[p_student.id]);
                student_status[p_student.id] = 0;
                pthread_mutex_unlock(&student_status_lock[p_student.id]);    
                //printf("Student thread %d has ended\n", p_student.id);
                return NULL;
            }
            else {
                printf(ANSI_COLOR_MAGENTA "Student %d has withdrawn from course %s" ANSI_COLOR_RESET "\n", p_student.id, courses[current_pref_course].name);
                course_withdrawn = 1;
            }
        }
        if (course_withdrawn){
            int previous_pref_course = current_pref_course;
            // get next course if present course isn't available
            for (int i = current_pref+1; i < 3; i++){
                pthread_mutex_lock(&courses_spots_lock[p_student.preferences[i]]);
                if(courses_spots[p_student.preferences[i]] != -1) {
                    current_pref = i;
                    current_pref_course = courses_spots[p_student.preferences[i]];
                }
                pthread_mutex_unlock(&courses_spots_lock[p_student.preferences[i]]);
            }
            if (previous_pref_course == current_pref_course){
                current_pref = -1;
            }
            else {
                printf(ANSI_COLOR_CYAN "Student %d has changed current preference from %s to %s" ANSI_COLOR_RESET "\n", p_student.id, courses[previous_pref_course].name, courses[current_pref_course].name);
            }
        }
    }

    if (current_pref == -1){
        printf(ANSI_COLOR_YELLOW "Student %d did not get any of their preferred courses" ANSI_COLOR_RESET "\n", p_student.id);
    }

    //printf("Student thread %d has ended\n", p_student.id);

    return NULL;
}

void *simulate_course(void *args) {
    course p_course = *(course *)args;

    while(1) {

        int chosen_ta_lab_id = -1;
        int chosen_ta_id = -1;
        // go through each of the eligible labs
        for(int lab_no = 0; lab_no < p_course.num_labs; lab_no++){
            int lab_id = p_course.labs[lab_no];

            // go through each of the TAs in the lab
            for(int ta_num = 0; ta_num < rlabs[lab_id].num_tas; ta_num++){

                pthread_mutex_lock(&lab_tas_lock[lab_id][ta_num]);
                // if a TA is eligible to take a tutorial but is currently taking a tutorial for some other course
                // a broadcast will wake the condition
                while(lab_tas[lab_id][ta_num][0] > 0 && lab_tas[lab_id][ta_num][1] <= 0){
                    pthread_cond_wait(&lab_tas_available[lab_id][ta_num], &lab_tas_lock[lab_id][ta_num]);
                }
                // if it does not wait, or when it successfully comes out of the wait, then choose this ta for the course
                if(lab_tas[lab_id][ta_num][0] > 0 && lab_tas[lab_id][ta_num][1] > 0){
                    lab_tas[lab_id][ta_num][0]--;       // the ta used up a course
                    lab_tas[lab_id][ta_num][1] = 0;     // the ta is now busy conducting a tutorial here
                    chosen_ta_lab_id = lab_id;
                    chosen_ta_id = ta_num;
                }
                pthread_mutex_unlock(&lab_tas_lock[lab_id][ta_num]);

                // stop going through the tas if you chose one already
                if (chosen_ta_id != -1) break;
            }
            // stop going through the tas if you chose one already
            if (chosen_ta_id != -1) break;
        }

        // no ta was found for this course, the course is now removed
        if (chosen_ta_id == -1) {
            pthread_mutex_lock(&courses_spots_lock[p_course.id]);
            courses_spots[p_course.id] = -1;
            pthread_cond_broadcast(&courses_spots_available[p_course.id]);
            pthread_mutex_unlock(&courses_spots_lock[p_course.id]);
            printf(ANSI_COLOR_RED "Course %s does not have any TA's eligible and is removed from course offerings" ANSI_COLOR_RESET "\n", p_course.name);
            //printf("Course thread %s has ended\n", p_course.name);
            break;
        }

        pthread_mutex_lock(&lab_tas_lock[chosen_ta_lab_id][chosen_ta_id]);
        printf(ANSI_COLOR_GREEN "TA %d from lab %s has been allocated to course %s for their TA ship number %d" ANSI_COLOR_RESET "\n", chosen_ta_id, rlabs[chosen_ta_lab_id].name, p_course.name, rlabs[chosen_ta_lab_id].max_courses-lab_tas[chosen_ta_lab_id][chosen_ta_id][0]);
        pthread_mutex_unlock(&lab_tas_lock[chosen_ta_lab_id][chosen_ta_id]);

        int left_spots_in_lab = 0;
        for(int i = 0; i < rlabs[chosen_ta_lab_id].num_tas; i++){
            pthread_mutex_lock(&lab_tas_lock[chosen_ta_lab_id][i]);
            left_spots_in_lab += lab_tas[chosen_ta_lab_id][i][0];
            pthread_mutex_unlock(&lab_tas_lock[chosen_ta_lab_id][i]);
        }
        if (left_spots_in_lab == 0) {
            printf(ANSI_COLOR_BLUE "Lab %s no longer has mentors available for TAship" ANSI_COLOR_RESET "\n", rlabs[chosen_ta_lab_id].name);
        }

        // open tut_slots slots for the tutorial
        int tut_slots = (rand() % p_course.course_max_slots) + 1;
        pthread_mutex_lock(&courses_spots_lock[p_course.id]);
        courses_spots[p_course.id] = tut_slots;
        pthread_mutex_unlock(&courses_spots_lock[p_course.id]);

        printf(ANSI_COLOR_CYAN "Course %s has been allocated %d seats" ANSI_COLOR_RESET "\n", p_course.name, tut_slots);

        int waiting_students = tut_slots;
        int filled_seats = 0;
        while ((filled_seats < 1) || (waiting_students != 0 && filled_seats < tut_slots)){
            pthread_cond_broadcast(&courses_spots_available[p_course.id]);

            waiting_students = 0;
            filled_seats = 0;
            for(int i = 0; i < num_students; i++){
                pthread_mutex_lock(&student_status_lock[i]);
                if (student_status[i] == -1*(p_course.id+1))   waiting_students += 1;
                if (student_status[i] == (p_course.id+1))   {filled_seats += 1;
                //printf("Found that student %d filled %s as value %d\n", i, p_course.name, student_status[i]);
                }
                pthread_mutex_unlock(&student_status_lock[i]);
            }

            if(waiting_students == 0) break; // if noone is waiting, stop
        }

        filled_seats = 0;
        for(int i = 0; i < num_students; i++){
            pthread_mutex_lock(&student_status_lock[i]);
            if (student_status[i] == (p_course.id+1))   filled_seats += 1;
            pthread_mutex_unlock(&student_status_lock[i]);
        }

        printf(ANSI_COLOR_GREEN "TA %d has started tutorial for Course %s with %d seats filled out of %d" ANSI_COLOR_RESET "\n", chosen_ta_id, p_course.name, filled_seats, tut_slots);

        pthread_mutex_lock(&courses_spots_lock[p_course.id]);
        courses_spots[p_course.id] = 0;
        pthread_mutex_unlock(&courses_spots_lock[p_course.id]);

        sleep(5); // the ta is conducting a tutorial

        for (int i = 0; i < num_students; i++){
            pthread_mutex_lock(&student_status_lock[i]);
            if (student_status[i] == (p_course.id+1)){
                student_status[i] = 0;
                pthread_cond_broadcast(&student_status_available[i]);
                pthread_mutex_unlock(&student_status_lock[i]);
            }
            else {
                pthread_mutex_unlock(&student_status_lock[i]);
            }
        }

        printf(ANSI_COLOR_MAGENTA "TA %d from lab %s has completed the tutorial and left the course %s" ANSI_COLOR_RESET "\n", chosen_ta_id, rlabs[chosen_ta_lab_id].name, p_course.name);

        pthread_mutex_lock(&lab_tas_lock[chosen_ta_lab_id][chosen_ta_id]);
        lab_tas[chosen_ta_lab_id][chosen_ta_id][1] = 1; // the ta is now available to ta some other course
        pthread_cond_broadcast(&lab_tas_available[chosen_ta_lab_id][chosen_ta_id]);
        pthread_mutex_unlock(&lab_tas_lock[chosen_ta_lab_id][chosen_ta_id]);
    }

    //printf("Course thread %s has ended\n", p_course.name);

    return NULL;
}

int main() {

    #pragma region Input
    // ---------------------------- Taking Input ----------------------------------

    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    
    courses = malloc(sizeof(course) * num_courses);

    // taking all the inputs for the courses
    for (int i = 0; i < num_courses; ++i){
        courses[i].id = i;
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].course_max_slots, &courses[i].num_labs);

        // taking the list of eligible labs for the course
        courses[i].labs = malloc(sizeof(int) * courses[i].num_labs);
        for (int ii = 0; ii < courses[i].num_labs; ii++){
            scanf("%d", &courses[i].labs[ii]);
        }
    }

    #ifdef DEBUG
        for (int i = 0; i < num_courses; ++i){
            printf("Input: Course: %s, Interest: %f, Max_Slots: %d, Num_labs: %d, ", courses[i].name, courses[i].interest, courses[i].course_max_slots, courses[i].num_labs);
            printf("Labs: ");
            for (int ii = 0; ii < courses[i].num_labs; ii++){
                printf("%d ", courses[i].labs[ii]);
            }
            printf("\n");
        }
    #endif

    students = malloc(sizeof(student) * num_students);
    // taking all the inputs for the students
    for (int i = 0; i < num_students; ++i){
        students[i].id = i;
        scanf("%f %d %d %d %d", &students[i].calibre, &students[i].preferences[0], &students[i].preferences[1], &students[i].preferences[2], &students[i].fill_time);
    }

    #ifdef DEBUG
        for (int i = 0; i < num_students; ++i){
            printf("Input: Student ID: %d, Calibre: %f, Preferences: [%d %d %d], Time: %d\n", students[i].id, students[i].calibre, students[i].preferences[0], students[i].preferences[1], students[i].preferences[2], students[i].fill_time);
        }
    #endif

    rlabs = malloc(sizeof(lab) * num_labs);
    // taking all the inputs for the labs
    for (int i = 0; i < num_labs; ++i){
        rlabs[i].id = i;
        scanf("%s %d %d", rlabs[i].name, &rlabs[i].num_tas, &rlabs[i].max_courses);
    }

    #ifdef DEBUG
        for (int i = 0; i < num_labs; ++i){
            printf("Input: Lab: %s, Num_tas: %d, Max_courses: %d\n", rlabs[i].name, rlabs[i].num_tas, rlabs[i].max_courses);
        }
    #endif

    // ---------------------------- Finished Taking Input ----------------------------------
    #pragma endregion Input 

    courses_spots_lock = malloc(sizeof(pthread_mutex_t) * num_courses);
    courses_spots_available = malloc(sizeof(pthread_cond_t) * num_courses);
    courses_spots = malloc(sizeof(int) * num_courses);
    for (int i = 0; i < num_courses; ++i){
        pthread_mutex_init(&courses_spots_lock[i], NULL);
        pthread_cond_init(&courses_spots_available[i], NULL);
        courses_spots[i] = 0;
    }

    // student_in_tut_lock = malloc(sizeof(pthread_mutex_t) * num_students);
    // student_in_tut_available = malloc(sizeof(pthread_cond_t) * num_students);
    // student_in_tut = malloc(sizeof(int) * num_students);
    // for (int i = 0; i < num_students; ++i){
    //     pthread_mutex_init(&student_in_tut_lock[i], NULL);
    //     pthread_cond_init(&student_in_tut_available[i], NULL);
    //     student_in_tut[i] = -1;
    // }

    lab_tas_lock = malloc(sizeof(pthread_mutex_t *) * num_labs);
    lab_tas_available = malloc(sizeof(pthread_cond_t *) * num_labs);
    lab_tas = malloc(sizeof(int **) * num_labs);
    for (int i = 0; i < num_labs; ++i){
        lab_tas_lock[i] = malloc(sizeof(pthread_mutex_t) * rlabs[i].num_tas);
        lab_tas_available[i] = malloc(sizeof(pthread_cond_t) * rlabs[i].num_tas);
        lab_tas[i] = malloc(sizeof(int *) * rlabs[i].num_tas);
        for (int ii = 0; ii < rlabs[i].num_tas; ii++){
            pthread_mutex_init(&lab_tas_lock[i][ii], NULL);
            pthread_cond_init(&lab_tas_available[i][ii], NULL);
            lab_tas[i][ii] = malloc(sizeof(int) * 2);
            lab_tas[i][ii][0] = rlabs[i].max_courses;
            lab_tas[i][ii][1] = 1;
        }
    }

    student_status_lock = malloc(sizeof(pthread_mutex_t) * num_students);
    student_status_available = malloc(sizeof(pthread_cond_t) * num_students);
    student_status = malloc(sizeof(int) * num_students);
    for (int i = 0; i < num_students; ++i){
        student_status[i] = -1 * (students[i].preferences[0]+1);
        pthread_mutex_init(&student_status_lock[i], NULL);
        pthread_cond_init(&student_status_available[i], NULL);
    }

    // allocate space and create threads
    t_courses = malloc(sizeof(pthread_t) * num_courses);
    t_students = malloc(sizeof(pthread_t) * num_students);
    for (int i = 0; i < num_students; i++){
        pthread_create(&t_students[i], NULL, simulate_student, &students[i]);
    }
    for (int i = 0; i < num_courses; i++){
        pthread_create(&t_courses[i], NULL, simulate_course, &courses[i]);
    }

    // wait for all threads to finish
    for (int i = 0; i < num_courses; i++) {
        pthread_join(t_courses[i], NULL);
    }
    for (int i = 0; i < num_students; i++) {
        pthread_join(t_students[i], NULL);
    }


    // free all malloc'd memory
    for (int i = 0; i < num_courses; ++i){
        free(courses[i].labs);
    }
    free(courses);
    free(students);
    free(rlabs);

    // free(student_in_tut_lock);
    // free(student_in_tut_available);
    // free(student_in_tut);

    free(courses_spots_lock);
    free(courses_spots_available);
    free(courses_spots);
    for (int i = 0; i < num_labs; ++i){
        for (int ii = 0; ii < rlabs[i].num_tas; ii++){
            free(lab_tas[i][ii]);
        }
        free(lab_tas[i]);
        free(lab_tas_lock[i]);
        free(lab_tas_available[i]);
    }
    free(lab_tas_available);
    free(lab_tas_lock);
    free(lab_tas);
    free(student_status);
    free(student_status_lock);
    free(student_status_available);

    free(t_courses);
    free(t_students);

    return 0;
}

//###########FILE CHANGE ./main_folder/Sriram Devata_305951_assignsubmission_file_/2019113007/q1/structs.h ####################//

typedef struct student {
    int id;
    int fill_time;
    float calibre;
    int preferences[3];
} student;

typedef struct lab {
    int id;
    char name[20];
    int num_tas;
    int max_courses;
} lab;

typedef struct course {
    int id;
    char name[20];
    float interest;
    int course_max_slots;
    int num_labs;
    int *labs;
} course;
