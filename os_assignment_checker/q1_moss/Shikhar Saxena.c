
//###########FILE CHANGE ./main_folder/Shikhar Saxena_305903_assignsubmission_file_/2021121010/q1/main.c ####################//

#include "headers.h"

typedef struct Course
{
	int id;
	char name[25];				// Course Name
	float interest;       // Interest associated with course
	int max_slot;				  // Maximum number of students that can attend tutorial
	int p; 								// Total number of assorted labs
	int* lab_i;						// Lab ids (associated with this course)
	pthread_t thread_id;	// Course thread id
} Course;

typedef struct Student
{
	int id;
	int pref[3];          // Course preferences of student
	int time;							// Time of registration
	float calibre;				// Calibre Quotient
	pthread_t thread_id;  // Student thread id
} Student;

typedef struct Lab
{
	int id;
	char name[25];
	int num_mentors; 			// No of mentors	
	int no_of_times;      // No of times a lab member can TA in course
} Lab;

typedef enum mentor_state {
		
	AVAILABLE,					// Availabe to be allocated to a course
	ALLOCATED, 		      // Allocated to some course
	FULL, 							// Has taken maximum number of tuts possible

} mentor_state;

int num_students, num_labs, num_courses;

Course* courses;
Student* students; 
Lab* avail_labs;

mentor_state** TAs; 		// Mentors info (indexed by lab id, lab mentor id)
int** tut_taken;				// No of tuts taken (indexed by lab id, lab mentor id)

sem_t** mentor_lock;		// Lock on each mentor (to read and/or modify their state) 

sem_t* vacant_seats;		// Vacant Seats (for each course)
sem_t* allocated_seats;	// Allocated Seats (for each course)
sem_t* seat_allocation_possible; // Binary semaphore (indicates if seats can be allocated to students or not)

void input() {

	scanf("%d", &num_students);
	scanf("%d", &num_labs);
	scanf("%d", &num_courses);

	courses = mem_malloc(num_courses, sizeof(Course));
	vacant_seats = mem_malloc(num_courses, sizeof(sem_t));
	seat_allocation_possible = mem_malloc(num_courses, sizeof(sem_t));
	allocated_seats = mem_malloc(num_courses, sizeof(sem_t));
	
	students = mem_malloc(num_students, sizeof(Student));
	avail_labs = mem_malloc(num_labs, sizeof(Lab));

	tut_taken = mem_malloc(num_labs, sizeof(int*));
	TAs = mem_malloc(num_labs, sizeof(mentor_state*));
	mentor_lock = mem_malloc(num_labs, sizeof(sem_t*));

	// Input courses
	for(int i = 0; i < num_courses; i++) {
		courses[i].id = i;
		scanf("%s", courses[i].name); 
		scanf("%f", &courses[i].interest); 
		scanf("%d", &courses[i].max_slot); 
		scanf("%d", &courses[i].p);

		courses[i].lab_i = mem_malloc(courses[i].p, sizeof(int));

		sem_init(&vacant_seats[i], 0, 0);
		sem_init(&seat_allocation_possible[i], 0, 0);
		sem_init(&allocated_seats[i], 0, 0);

		for(int j = 0; j < courses[i].p; j++) {
			scanf("%d", &courses[i].lab_i[j]);
		}
	}

	// Input Students
	for(int i = 0; i < num_students; i++) {

		students[i].id = i;
		scanf("%f", &students[i].calibre);

		for(int j = 0; j < 3; j++)
			scanf("%d", &students[i].pref[j]);

		scanf("%d", &students[i].time);
	}

	// Input Labs
	for(int i = 0; i < num_labs; i++) {
		avail_labs[i].id = i;

		scanf("%s", avail_labs[i].name);
		scanf("%d", &avail_labs[i].num_mentors);

		tut_taken[i] = mem_calloc(avail_labs[i].num_mentors, sizeof(int));
		TAs[i] = mem_malloc(avail_labs[i].num_mentors, sizeof(mentor_state));
		mentor_lock[i] = mem_malloc(avail_labs[i].num_mentors, sizeof(sem_t));

		/* Init mentor locks */
		for(int j = 0; j < avail_labs[i].num_mentors; j++)
			sem_init( (*(mentor_lock + i) + j), 0, 1);

		scanf("%d", &avail_labs[i].no_of_times);
	}
}

void terminate() {

	for(int i = 0; i < num_courses; i++) 
		free(courses[i].lab_i);

	free(courses);
	free(students);

	for(int i = 0; i < num_labs; i++) {
		free(tut_taken[i]);
		free(TAs[i]);
		free(mentor_lock[i]);
	}

	free(mentor_lock);
	free(tut_taken);
	free(TAs);
	free(avail_labs);
	free(vacant_seats);
	free(allocated_seats);	
	free(seat_allocation_possible);
}

// Student thread waits for getting allocated a seat in course course_id
void allocation(int student_id, int course_id) {

	/* wait for seat at this course */
	sem_wait(&vacant_seats[course_id]);

	// Will block if tut has started
	sem_wait(&seat_allocation_possible[course_id]);

	printf("%sStudent %d has been allocated a seat in course %s%s\n", CYAN, student_id, courses[course_id].name, RESET);
	
	sem_post(&allocated_seats[course_id]);

	sem_post(&seat_allocation_possible[course_id]);
}

void* student_start(void* arg) {

	Student* student = arg;

	sleep(student->time); /* sleep for the time till student registers */

	/* Register this student */
	printf("%sStudent %d has filled in preferences for course registration%s\n", YELLOW, student->id, RESET);

student_allocation:
	allocation(student->id, student->pref[0]);

	/* Attends Tutorial */

	int p = (int)(courses[student->pref[0]].interest * student->calibre * 100);

	int random_value = (rand() % 100) + 1;

	if(random_value < p) { /* Withdraw from this course */
		printf("%sStudent %d has withdrawn from course %s%s\n", BLUE, student->id, courses[student->pref[0]].name, RESET);
		
		/* Leave the seat */
		sem_wait(&allocated_seats[student->pref[0]]);
		sem_post(&vacant_seats[student->pref[0]]);

		goto student_allocation;
	}

	else { /* Select course permanently */
		printf("%sStudent %d has selected the course %s permanently%s\n",BLUE, student->id, courses[student->pref[0]].name, RESET);
		/* Leave the seat */
		sem_wait(&allocated_seats[student->pref[0]]);
		sem_post(&vacant_seats[student->pref[0]]);

	}

	return NULL;
}

void* course_start(void* arg) {

	Course* this_course = arg;
	/* Get a TA for this course */

	int alloc_ta[2]; // 0: lab_id, 1: mentor_id

ta_allocation:

	for(int i = 0; i < this_course->p; i++) {

		int lab_id = this_course->lab_i[i];

		for(int j = 0; j < avail_labs[lab_id].num_mentors; j++) {

			sem_wait(&mentor_lock[lab_id][j]);
			
			if(TAs[lab_id][j] == AVAILABLE) {
				TAs[lab_id][j] = ALLOCATED;
				alloc_ta[0] = lab_id;
				alloc_ta[1] = j;
				sem_post(&mentor_lock[lab_id][j]);
				goto allocation_done;
			}

			sem_post(&mentor_lock[lab_id][j]);
		}
	}

	return NULL;

allocation_done:
	
	printf("%sTA %d from lab %s has been allocated to course %s for his %d TA ship%s\n", GREEN, alloc_ta[1], avail_labs[alloc_ta[0]].name, this_course->name, ++tut_taken[alloc_ta[0]][alloc_ta[1]], RESET);

	/* Decide D */
	int D = (rand() % this_course->max_slot) + 1;

	printf("%sCourse %s has been allocated %d seats%s\n", RED, this_course->name, D, RESET);

	// Post D seats
	for(int i = 0; i < D; i++) {
		sem_post(&vacant_seats[this_course->id]);
	}

	sem_post(&seat_allocation_possible[this_course->id]);

	/* Tut has started */
	sem_wait(&seat_allocation_possible[this_course->id]);

	// allocated_seats contains the no of seats allocated
	int disp_alloc_seats;

	sem_getvalue(&allocated_seats[this_course->id], &disp_alloc_seats);

	printf("%sTutorial has started for Course %s with %d seats filled out of %d%s\n", MAGENTA, this_course->name, disp_alloc_seats, D, RESET);

	sleep(5); /* sleep for tutorial */

	/* TA leaves the course */
	sem_wait(&mentor_lock[alloc_ta[0]][alloc_ta[1]]);
		
	int tuts = tut_taken[alloc_ta[0]][alloc_ta[1]];
	TAs[alloc_ta[0]][alloc_ta[1]] = (tuts == avail_labs[alloc_ta[0]].no_of_times) ? FULL : AVAILABLE;

	sem_post(&mentor_lock[alloc_ta[0]][alloc_ta[1]]);

	goto ta_allocation;

	return NULL;
}

int main(int argc, char const *argv[])
{
	srand(time(0));

	input();

	// Create thread for each student
	for(int i = 0; i < num_students; i++)
		pthread_create(&(students[i].thread_id), NULL, student_start, &students[i]);

	// Create thread for each course
	for(int i = 0; i < num_courses; i++)
		pthread_create(&(courses[i].thread_id), NULL, course_start, &courses[i]);	

	// Wait for all threads to complete
	for(int i = 0; i < num_students; i++)
		pthread_join(students[i].thread_id, NULL);

	for(int i = 0; i < num_courses; i++)
		pthread_join(courses[i].thread_id, NULL);	

	terminate();
	return 0;
}
//###########FILE CHANGE ./main_folder/Shikhar Saxena_305903_assignsubmission_file_/2021121010/q1/headers.h ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

// Display Error and exit 
void display_error(const char* err) {
	fprintf(stderr, "%s\n", err);
	exit(1);
}

// Allocate using malloc
void* mem_malloc(int num, int size) {
	void* ptr = malloc(num * size);
	if(ptr == NULL) display_error("memory allocation error");
	return ptr;
}

// Allocate using calloc
void* mem_calloc(int num, int size) {
	void* ptr = calloc(num, size);
	if(ptr == NULL) display_error("memory allocation error");
	return ptr;
}