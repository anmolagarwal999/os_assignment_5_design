
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/student.c ####################//

#include "student.h"



void fill_registration(student *arg)
{
    sleep(arg->time);
    printf(RED "Student %d has filled in preferences for course registration" RESET "\n", arg->id);
    arg->state = waiting;
}

void *init_student(void *arg)
{
    student *s = (student *)arg;
    //printf("%d %f %d %d %d %d\n", s->id, s->calibre, s->pref1, s->pref2, s->pref3, s->time);
    while (1)
    {
        while (s->state == not_registered)
        {
            pthread_mutex_lock(&student_lock[s->id]);
            fill_registration(s);
            pthread_mutex_unlock(&student_lock[s->id]);
        }
        if (s->state == waiting)
        {
            if (COURSES[s->pref[s->current_pref]].state == withdrawn)
            {
                pthread_mutex_lock(&student_lock[s->id]);
                if (STUDENTS[s->id].current_pref < 2)
                {
                    STUDENTS[s->id].current_pref += 1;
                    printf( "Student %d has changed preference from the course %s(%d) to course %s(%d)\n" RESET, STUDENTS[s->id].id, COURSES[STUDENTS[s->id].pref[STUDENTS[s->id].current_pref - 1]].name, STUDENTS[s->id].current_pref - 1, COURSES[STUDENTS[s->id].pref[STUDENTS[s->id].current_pref]].name, STUDENTS[s->id].current_pref);
                }
                else
                {
                    printf("Student %d couldn't get any of his preferred courses\n" RESET, STUDENTS[s->id].id);
                    STUDENTS[s->id].state = exited_simulation;
                }
                pthread_mutex_unlock(&student_lock[s->id]);
            }
            else
            {
                pthread_mutex_lock(&student_lock[s->id]);
                pthread_cond_wait(&tutorial[s->pref[s->current_pref]][s->id], &student_lock[s->id]);
                pthread_mutex_unlock(&student_lock[s->id]);
            }
        }
        while (s->state == attending_tut)
        {
        }
        if (s->state == exited_simulation || s->state == selected_course)
            pthread_exit(NULL);
    }
}

//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/globals.h ####################//

#ifndef GLOBALS_H
#define GLOBALS_H

#include "student.h"
#include "lab.h"
#include "course.h"

#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <time.h>
#include <sys/syscall.h>

#define MAX_TAS 20
#define MAX_COURSES 20
#define MAX_LABS 20
#define MAX_STUDENTS 100

typedef struct course_args course_args;
typedef struct course course;
typedef struct student student;
typedef struct lab lab;
typedef struct TA ta;

extern lab *LABS;
extern course *COURSES;
extern student *STUDENTS;

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"

void init_lab_locks();
void init_TA_locks();
void init_student_locks();
void init_tutorials();
void assign_TA(course *arg);

int check_labs(course *c);
int check_Tas(int index);


int check_students();

void conduct_tutorial(int lab_id, int ta_id);

extern int num_courses;
extern int num_students;
extern int num_labs;

extern int times;


extern pthread_mutex_t pref_lock[MAX_COURSES];

extern pthread_mutex_t get_TA_lock[MAX_COURSES][MAX_TAS];
extern pthread_cond_t get_TA[MAX_COURSES][MAX_TAS];

extern pthread_mutex_t student_lock[MAX_STUDENTS];
extern pthread_mutex_t lab_lock[MAX_LABS];

extern pthread_cond_t tutorial[MAX_COURSES][MAX_STUDENTS];



#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/lab.h ####################//

#ifndef LAB_H
#define LAB_H

#include "globals.h"

typedef struct lab lab;
typedef struct TA ta;

enum Lab_state
{
    TA_available,
    No_TA_available,
    All_busy
};

enum TA_state
{
    assigned,
    in_tut,
    unassigned
};
struct lab
{
    char *name;
    int id;
    int num_TA;
    int quota;
    enum Lab_state state;
    ta *Tas;
    int bonus;
};

struct TA
{
    int id;
    int taship;
    int current_course;
    enum TA_state state;
};

void create_lab(lab *Lab);
void *init_lab(void *arg);

#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/main.c ####################//

#include "globals.h"
#include <signal.h>

lab *LABS;
student *STUDENTS;
course *COURSES;

int num_courses;
int num_students;
int num_labs;

pthread_mutex_t pref_lock[MAX_COURSES];
int times;

void *time_update(void *arg)
{
    times = 0;
    while (check_students() == 1)
    {

        sleep(1);
        if (times % 4 == 0)
        {
            for (int i = 0; i < num_students; i++)
            {
                if (STUDENTS[i].state != exited_simulation && STUDENTS[i].state != selected_course)
                    printf("student %d %s %d\n", STUDENTS[i].id, COURSES[STUDENTS[i].pref[STUDENTS[i].current_pref]].name, STUDENTS[i].state);
            }
            for (int i = 0; i < num_courses; i++)
            {
                printf("course:%s state:%d\n", COURSES[i].name, COURSES[i].state);
                printf("check labs = %d\n",check_labs(&COURSES[i]));
            }
            for (int i = 0; i < num_labs; i++)
            {
                printf("Lab %s state:%d\n", LABS[i].name, LABS[i].state);
                for (int j = 0; j < LABS[i].num_TA; j++)
                {
                    printf("    TA %d state %d\n", LABS[i].Tas[j].id, LABS[i].Tas[j].state);
                }
            }
        }

        times++;
    }
}

int main()
{

    pthread_t timer;
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    LABS = malloc(sizeof(lab) * num_labs);
    STUDENTS = malloc(sizeof(student) * num_students);
    COURSES = malloc(sizeof(course) * num_courses);
    pthread_t s[num_students], l[num_labs], c[num_courses];
    init_TA_locks();
    init_student_locks();
    init_tutorials();

    init_lab_locks();

    for (int i = 0; i < num_courses; i++)
    {
        COURSES[i].id = i;
        COURSES[i].state = waiting_TA;
        COURSES[i].name = malloc(sizeof(char) * 100);
        scanf("%s %f %d %d", COURSES[i].name, &COURSES[i].interest, &COURSES[i].course_max_slot, &COURSES[i].num_labs);
        COURSES[i].Labs = malloc(sizeof(int) * COURSES[i].num_labs);
        for (int j = 0; j < COURSES[i].num_labs; j++)
        {
            scanf("%d", &COURSES[i].Labs[j]);
        }
    }
    for (int i = 0; i < num_students; i++)
    {
        int pref1, pref2, pref3;
        scanf("%f %d %d %d %d", &STUDENTS[i].calibre, &pref1, &pref2, &pref3, &STUDENTS[i].time);
        STUDENTS[i].id = i;
        STUDENTS[i].pref[0] = pref1;
        STUDENTS[i].pref[1] = pref2;
        STUDENTS[i].pref[2] = pref3;

        STUDENTS[i].current_pref = 0;
        STUDENTS[i].state = not_registered;
    }
    for (int i = 0; i < num_labs; i++)
    {
        LABS[i].name = malloc(sizeof(char) * 100);
        LABS[i].id = i;
        scanf("%s %d %d", LABS[i].name, &LABS[i].num_TA, &LABS[i].quota);
        create_lab(&LABS[i]);
    }
    //pthread_create(&timer, NULL, time_update, NULL);
    for (int i = 0; i < num_labs; i++)
    {
        int rc = pthread_create(&l[i], NULL, init_lab, &LABS[i]);
        assert(rc == 0);
    }
    for (int i = 0; i < num_courses; i++)
    {
        int rc = pthread_create(&c[i], NULL, init_course, &COURSES[i]);
        assert(rc == 0);
    }
    for (int i = 0; i < num_students; i++)
    {
        pthread_create(&s[i], NULL, init_student, &STUDENTS[i]);
    }

    for (int i = 0; i < num_labs; i++)
    {
        pthread_join(l[i], NULL);
    }
    for (int i = 0; i < num_students; i++)
    {
        pthread_join(s[i], NULL);
    }
    for (int i = 0; i < num_courses; i++)
    {
        pthread_join(c[i], NULL);
    }
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/student.h ####################//

#ifndef STUDENT_H
#define STUDENT_H


#include "globals.h"

typedef struct student student;
enum student_state
{
    not_registered,
    waiting,
    attending_tut,
    selected_course,
    exited_simulation
};

struct student
{
    int id;
    int pref[3];
    int current_pref;
    float calibre;
    int time;
    enum student_state state;
};

void fill_registration(student *arg);
void *init_student(void *arg);

#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/course.c ####################//

#include "course.h"
#include <string.h>

void *init_course(void *arg)
{
    course *c = (course *)arg;
    while (1)
    {
        while (c->state == waiting_TA)
        {
            if (check_labs(c) == -1)
            {
                COURSES[c->id].state = withdrawn;
                printf(GREEN "Course %s doesn't have any TA's eligible and is removed from course offerings\n" RESET, COURSES[c->id].name);
                for (int i = 0; i < num_students; i++)
                {
                    while ((STUDENTS[i].pref[STUDENTS[i].current_pref] == c->id) && (STUDENTS[i].state == waiting))
                    {
                        pthread_cond_signal(&tutorial[c->id][i]);
                    }
                }
                break;
            }
            else if (check_labs(c) == 1)
                assign_TA(c);
            else if (check_labs(c) == 0)
            {

                //busy wait as there is no restriction on the courses for this
            }
        }

        if (c->state == withdrawn)
        {
            printf("COURSE %s exited\n", c->name);
            pthread_exit(NULL);
        }
    }
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/globals.c ####################//

#include "globals.h"
lab *LABS;
student *STUDENTS;
course *COURSES;

pthread_mutex_t get_TA_lock[MAX_COURSES][MAX_TAS];
pthread_cond_t get_TA[MAX_COURSES][MAX_TAS];

pthread_mutex_t lab_lock[MAX_LABS];

pthread_mutex_t student_lock[MAX_STUDENTS];
pthread_cond_t tutorial[MAX_COURSES][MAX_STUDENTS];

int times;
void init_lab_locks()
{
    for (int i = 0; i < num_labs; i++)
    {
        int rc = pthread_mutex_init(&lab_lock[i], NULL);
        assert(rc == 0);
    }
}

void init_TA_locks()
{
    for (int i = 0; i < num_labs; i++)
    {

        for (int j = 0; j < LABS[i].num_TA; j++)
        {
            int rc = pthread_mutex_init(&get_TA_lock[i][j], NULL);
            assert(rc == 0);
            rc = pthread_cond_init(&get_TA[i][j], NULL);
            assert(rc == 0);
        }
    }
}

void init_student_locks()
{
    for (int i = 0; i < num_students; i++)
    {
        int rc = pthread_mutex_init(&student_lock[i], NULL);
        assert(rc == 0);
    }
}

void init_tutorials()
{
    for (int i = 0; i < num_courses; i++)
    {
        for (int j = 0; j < num_students; j++)
        {
            int rc = pthread_cond_init(&tutorial[i][j], NULL);
            assert(rc == 0);
        }
    }
}

void assign_TA(course *arg)
{

    int rc;

    int cond = 0;
    int i, j;
    for (i = 0; i < arg->num_labs; i++)
    {
        for (j = 0; j < LABS[arg->Labs[i]].num_TA; j++)
        {
            if (LABS[arg->Labs[i]].Tas[j].state == unassigned)
            {
                pthread_mutex_lock(&get_TA_lock[arg->Labs[i]][j]);

                if (LABS[arg->Labs[i]].Tas[j].state == unassigned && LABS[arg->Labs[i]].Tas[j].taship < LABS[arg->Labs[i]].quota && LABS[arg->Labs[i]].Tas[j].taship < LABS[arg->Labs[i]].bonus)
                {
                    LABS[arg->Labs[i]].Tas[j].taship += 1;
                    LABS[arg->Labs[i]].Tas[j].current_course = arg->id;
                    LABS[arg->Labs[i]].Tas[j].state = assigned;
                    cond = 1;
                    pthread_mutex_unlock(&get_TA_lock[arg->Labs[i]][j]);

                    break;
                }
                pthread_mutex_unlock(&get_TA_lock[arg->Labs[i]][j]);
            }
        }
        if (cond == 1)
        {
            break;
        }
    }
    if (cond == 1)
    {
        printf(BLUE "TA %d from lab %s has been allocated to course %s for the %d TA ship\n" RESET, LABS[arg->Labs[i]].Tas[j].id, LABS[arg->Labs[i]].name, arg->name, LABS[arg->Labs[i]].Tas[j].taship);

        arg->state = waiting_slots;
        sleep(1);
        while (arg->state == waiting_slots)
        {

            conduct_tutorial(arg->Labs[i], j);
        }
    }
}

int check_labs(course *c)
{
    for (int i = 0; i < c->num_labs; i++)
    {
        if (LABS[c->Labs[i]].state == TA_available)
        {
            if (check_Tas(c->Labs[i]) == 1)
            {
                return 1;
            }
        }
    }
    for (int i = 0; i < c->num_labs; i++)
    {
        if (LABS[c->Labs[i]].state == TA_available)
        {
            return 0;
        }
    }
    return -1;
}
int check_Tas(int index)
{

    for (int j = 0; j < LABS[index].num_TA; j++)
    {
        if (LABS[index].Tas[j].state == unassigned)
        {
            return 1;
        }
    }

    return -1;
}

int check_students()
{
    for (int i = 0; i < num_students; i++)
    {
        if (STUDENTS[i].state != exited_simulation && STUDENTS[i].state != selected_course)
        {
            return 1;
        }
    }
    return -1;
}

void conduct_tutorial(int lab_id, int ta_id)
{
    pthread_mutex_lock(&get_TA_lock[lab_id][ta_id]);

    srand(time(NULL));
    int D = (rand() % COURSES[LABS[lab_id].Tas[ta_id].current_course].course_max_slot) + 1;
    int W = 0;

    for (int i = 0; i < num_students; i++)
    {
        if ((STUDENTS[i].pref[STUDENTS[i].current_pref] == LABS[lab_id].Tas[ta_id].current_course) && (STUDENTS[i].state == waiting))
        {
            W++;
        }
    }
    sleep(1); //wait 1 time unit

    pid_t x = syscall(__NR_gettid);
    if (D > W)
    {
        D = W;
    }

    int i, j;
    for (i = 0, j = 0; j < D; i++)
    {
        if ((STUDENTS[i].pref[STUDENTS[i].current_pref] == LABS[lab_id].Tas[ta_id].current_course) && (STUDENTS[i].state == waiting))
        {
            pthread_cond_signal(&tutorial[LABS[lab_id].Tas[ta_id].current_course][i]);
            pthread_mutex_lock(&student_lock[i]);

            STUDENTS[i].state = attending_tut;
            printf(PURPLE "Student %d has been allocated a seat in course %s\n" RESET, STUDENTS[i].id, COURSES[LABS[lab_id].Tas[ta_id].current_course].name);
            pthread_mutex_unlock(&student_lock[i]);

            j++;
        }
    }

    printf(YELLOW "Course %s has been allocated %d seats\n" RESET, COURSES[LABS[lab_id].Tas[ta_id].current_course].name, D);
    printf(GREEN "Tutorial has started for course %s with %d seats filled out of %d\n" RESET, COURSES[LABS[lab_id].Tas[ta_id].current_course].name, D, COURSES[LABS[lab_id].Tas[ta_id].current_course].course_max_slot);
    LABS[lab_id].Tas[ta_id].state = in_tut;
    pthread_mutex_unlock(&get_TA_lock[lab_id][ta_id]);
    sleep(2);

    pthread_mutex_lock(&get_TA_lock[lab_id][ta_id]);
    for (int i = 0, j = 0; j < D; i++)
    {
        if (STUDENTS[i].pref[STUDENTS[i].current_pref] == LABS[lab_id].Tas[ta_id].current_course && STUDENTS[i].state == attending_tut)
        {
            pthread_cond_signal(&tutorial[LABS[lab_id].Tas[ta_id].current_course][i]);
            pthread_mutex_lock(&student_lock[i]);
            srand(time(NULL));
            float selection = (STUDENTS[i].calibre * COURSES[LABS[lab_id].Tas[ta_id].current_course].interest);
            if (((float)rand() / (float)RAND_MAX) < selection)
            {
                STUDENTS[i].state = selected_course;
                printf(GREEN "Student %d has select the course %s permanently\n" RESET, STUDENTS[i].id, COURSES[LABS[lab_id].Tas[ta_id].current_course].name);
            }
            else
            {
                printf(RED "Student %d has withdrawn from the course %s\n" RESET, STUDENTS[i].id, COURSES[LABS[lab_id].Tas[ta_id].current_course].name);

                if (STUDENTS[i].current_pref < 2)
                {
                    STUDENTS[i].current_pref += 1;
                    STUDENTS[i].state = waiting;
                    printf(BLUE "Student %d has changed preference from the course %s(%d) to course %s(%d)\n" RESET, STUDENTS[i].id, COURSES[STUDENTS[i].pref[STUDENTS[i].current_pref - 1]].name, STUDENTS[i].current_pref - 1, COURSES[STUDENTS[i].pref[STUDENTS[i].current_pref]].name, STUDENTS[i].current_pref);
                }
                else
                {
                    printf(RED "Student %d couldn't get any of his preferred courses\n" RESET, STUDENTS[i].id);
                    STUDENTS[i].state = exited_simulation;
                }
            }
            pthread_mutex_unlock(&student_lock[i]);

            j++;
        }
    }
    COURSES[LABS[lab_id].Tas[ta_id].current_course].state = waiting_TA;

    printf(RED "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, ta_id, LABS[lab_id].name, COURSES[LABS[lab_id].Tas[ta_id].current_course].name);

    LABS[lab_id].Tas[ta_id].state = unassigned;
    LABS[lab_id].Tas[ta_id].current_course = -1;

    pthread_mutex_unlock(&get_TA_lock[lab_id][ta_id]);
}

//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/course.h ####################//

#ifndef COURSE_H
#define COURSE_H

#include "globals.h"
#include <pthread.h>
#include "lab.h"




typedef struct course course;
typedef struct lab lab;


enum course_state
{
    waiting_TA,
    waiting_slots,
    conducting_tut,
    withdrawn
};
struct course
{
    int id;
    char *name;
    float interest;
    int num_labs;
    int *Labs;
    int course_max_slot;
    enum course_state state;
};



void *init_course(void *arg);



#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q1/lab.c ####################//

#include "lab.h"

void create_lab(lab *Lab)
{
    Lab->Tas = malloc(sizeof(ta) * Lab->num_TA);
    Lab->state = TA_available;
    Lab->bonus = 1;
    for (int i = 0; i < Lab->num_TA; i++)
    {
        Lab->Tas[i].id = i;
        Lab->Tas[i].taship = 0;
        Lab->Tas[i].state = unassigned;
        Lab->Tas[i].current_course = -1;
    }
}

int check_busy(ta *t)
{
    if (t->state != unassigned)
        return 1;
    else
        return -1;
}
int check_quota(ta *t, int quota)
{
    if (t->taship < quota)
        return 1;
    else if (t->state == unassigned)
        return -1;
    return 1;
}

void *init_lab(void *arg)
{
    lab *l = (lab *)arg;
    int busy, quota;
    while (1)
    {
        busy = 0;
        quota = 0;
        for (int i = 0; i < l->num_TA; i++)
        {
            if (check_busy(&l->Tas[i]) == 1)
            {
                busy = 1;
            }
            else
            {
                busy = 0;
                break;
            }
        }
        if (busy == 1)
        {
        }
        else
        {
            pthread_mutex_lock(&lab_lock[l->id]);
            LABS[l->id].state = TA_available;
            pthread_mutex_unlock(&lab_lock[l->id]);
            for (int i = 0; i < l->num_TA; i++)
            {
                if (check_quota(&l->Tas[i], l->quota) == -1)
                {
                    quota = 1;
                }
                else
                {
                    quota = 0;
                    break;
                }
            }
            if (quota == 1)
            {

                pthread_mutex_lock(&lab_lock[l->id]);
                LABS[l->id].state = No_TA_available;
                pthread_mutex_unlock(&lab_lock[l->id]);
                printf(BOLD LIGHT_PINK "LAB %s no longer has students available for TAship\n" RESET, l->name);

                pthread_exit(NULL);
            }
        }
        int cond = 0;
        for (int i = 0; i < l->num_TA; i++)
        {
            if (LABS[l->id].Tas[i].taship == LABS[l->id].bonus)
            {
                cond = 1;
            }
            else
            {
                cond = 0;
                break;
            }
        }
        if (cond == 1 && LABS[l->id].bonus < LABS[l->id].quota)
        {
            pthread_mutex_lock(&lab_lock[l->id]);
            LABS[l->id].bonus += 1;
            pthread_mutex_unlock(&lab_lock[l->id]);
        }
    }
}