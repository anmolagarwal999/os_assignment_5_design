
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/student.c ####################//

#include "student.h"

extern course * courseptr;
extern student * studentptr;
extern lab * labptr;

extern pthread_mutex_t timelock;
extern pthread_cond_t timecond;

extern int time_elapsed;
extern int num_students;

//the following function is for the student_threads

void * execute_student(void * s_element)
{

//***************************************************************
	//course registration.
	pthread_mutex_lock(&timelock);
	student * s_arr = (student *)s_element;

	while(time_elapsed < s_arr->time)
	{
		pthread_cond_wait(&timecond, &timelock);
	}
	pthread_mutex_unlock(&timelock);
	//course registration done here
//********************************************************************
	pthread_mutex_init(&(s_arr->student_status_lock), NULL);
	pthread_cond_init(&(s_arr->student_status_cond), NULL);

	printf(Student_Color "Student %d has filled in preferences for course registration\n" RESET, s_arr->id);

	//pthread_mutex_lock(&(s_arr->student_status_lock));

	//pthread_mutex_unlock(&(s_arr->student_status_lock));
	s_arr->current_pref = s_arr->pref[0];
	s_arr->status = S_WFFC;
	int selected = -1;
	while(1)
	{
		pthread_mutex_lock(&(s_arr->student_status_lock));
		if(s_arr->status == S_WFFC)
		{
			char * name = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has his current preference as %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFFC)
		{
			char * name = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTFC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[0])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[0])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[0];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_WFSC;
				s_arr->current_pref = s_arr->pref[1];
			}
		}
		else if(s_arr->status == S_WFSC)
		{
			char * pname = &(courseptr + s_arr->pref[1])->name[0];
			char * bname = &(courseptr + s_arr->pref[0])->name[0];
			printf(Student_Color);
			printf("Student %d has changed current preference from %s to %s\n", s_arr->id, bname, pname);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFSC)
		{
			char * name = &(courseptr + s_arr->pref[1])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTSC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[1])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[1])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[1];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_WFTC;
				s_arr->current_pref = s_arr->pref[2];
			}
		}
		else if(s_arr->status == S_WFTC)
		{
			char * pname = &(courseptr + s_arr->pref[2])->name[0];
			char * bname = &(courseptr + s_arr->pref[1])->name[0];
			printf(Student_Color);
			printf("Student %d has changed current preference from %s to %s\n", s_arr->id, bname, pname);
			printf(RESET);
			// this wait will be removed by the course thread when it chooses the TA and allots seat to this student.
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_ATFTC)
		{
			char * name = &(courseptr + s_arr->pref[2])->name[0];
			printf(Student_Color);
			printf("Student %d has been allocated a seat in course %s\n", s_arr->id, name);
			printf(RESET);
			// this wait will be removed by the course thread when tute is completed.
			s_arr->current_pref = -1;
			pthread_cond_wait(&(s_arr->student_status_cond), &(s_arr->student_status_lock));
		}
		else if(s_arr->status == S_FTTC)
		{
			int chance = rand()%10000;
			double check = ((courseptr + s_arr->pref[2])->interest) * (s_arr->calibre) * 10000;
			int prob = (int)check;
			//printf("Prob is : %d Chance is %d\n", prob, chance);

			char * name = &(courseptr + s_arr->pref[2])->name[0];
			if(chance < prob)// he liked the course
			{
				selected = s_arr->pref[2];
				s_arr->status = S_SLCTD;
			}
			else//he withdrew the course.
			{
				printf(Student_Color);
				printf("Student %d has withdrawn from course %s\n", s_arr->id, name);
				printf(RESET);
				s_arr->status = S_BYE;
				s_arr->current_pref = -1;
			}
		}
		else if(s_arr->status == S_SLCTD)
		{
			char * name = &(courseptr + selected)->name[0];
			printf(Student_Color);
			printf("Student %d has selected course %s permanently\n", s_arr->id, name);
			printf(RESET);
			//pthread_mutex_unlock(&(s_arr->student_status_lock));
			break;
		}
		else if(s_arr->status == S_BYE)
		{
			printf(Student_Color "Student %d couldn't get any of his preferred courses\n" RESET, s_arr->id);
			//pthread_mutex_unlock(&(s_arr->student_status_lock));
			break;
		}
		pthread_mutex_unlock(&(s_arr->student_status_lock));
		usleep(5000);
	}

	pthread_mutex_destroy(&(s_arr->student_status_lock));
	pthread_cond_destroy(&(s_arr->student_status_cond));

}

//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/states.h ####################//

#ifndef _STATES
#define _STATES

// STUDENT STATES ARE AS FOLLOWS

#define S_NR 	0		// Not registered yet
#define S_WFFC 	1  		//Waiting For First Course
#define S_ATFFC 2		//Attending Tute For First Course
#define S_WFSC 	3
#define S_ATFSC 4
#define S_WFTC 	5
#define S_ATFTC 6
#define S_SLCTD 7		//Selected
#define S_BYE 	8		//Did not like any course bechara....
#define S_FTFC 	9
#define S_FTSC 	10
#define S_FTTC 	11		// It means student finished attending that tute and yet to take decision.


// COURSE STATES ARE AS FOLLOWS

#define C_WFTA 	0 		//Waiting For TA
#define C_WFS 	1		//Waiting For Slots
#define	C_TUTE 	2		// Tutes going on
#define C_BYE	3 		// Course withdrawn 

//  LAB STATES ARE AS FOLLOWS

#define L_OPEN	0		//has tas to be selected
#define L_CLOSE	1		// No tas available


// STATES FOR TA

#define TA_FREE 	0		// it means he is not ta for any course at the moment
#define TA_WORKING 	1		// it means he is a ta for any course.

#endif
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/main.c ####################//

#include <stdio.h>
#include <unistd.h> 	//for the sleep function
#include <stdlib.h> 	// for the srand() and rand() function.
#include <time.h> 		// for the time(NULL) thing
#include <pthread.h> 	// this is for the threads.

#include "states.h"		// possible states of different entities
#include "colors.h"		// color codes for different colors
#include "datatypes.h"	// definition of different datatypes used

#include "student.h"	//all the functions related to student threads.
#include "courses.h"
//global variables.
extern int num_students, num_labs, num_courses;
extern int time_elapsed;

pthread_mutex_t timelock;
pthread_cond_t timecond;

//the following function is for time_thread;
void * time_tracker()
{
	time_elapsed = 0;
	sleep(1);
	while(1)
	{
		pthread_mutex_lock(&timelock);
		time_elapsed++;
		printf(RED "Time elapsed is -- %d seconds.............\n" RESET, time_elapsed);
		pthread_mutex_unlock(&timelock);
		pthread_cond_broadcast(&timecond);
		sleep(1);
	}
}

int main()
{

// input part starts here
//****************************************************************************************************
	srand(time(NULL));
	extern int num_students, num_labs, num_courses;
	extern course * courseptr;
	extern student * studentptr;
	extern lab * labptr;

	printf(RED);
	scanf( "%d %d %d" , &num_students, &num_labs, &num_courses); 			//taking input for the numbers
	printf(RESET);

	//declaring respective struct arrays;

	student arr_students[num_students];
	course arr_courses[num_courses];
	lab arr_labs[num_labs];

	courseptr = arr_courses;
	studentptr = arr_students;
	labptr = arr_labs;

	// input for the courses part

	printf(GREEN);
	for(int i = 0; i < num_courses; i++)
	{
		scanf( "%s" , arr_courses[i].name);
		scanf( "%lf %d %d" , &arr_courses[i].interest, &arr_courses[i].max_slots, &arr_courses[i].n_labs);
		arr_courses[i].id = i;
		arr_courses[i].present_students = 0;
		arr_courses[i].status = C_WFTA;

		for(int j = 0; j < arr_courses[i].n_labs ; j++)
			scanf( "%d" , &arr_courses[i].lab_id[j]);
	}
	printf(RESET);


	// input for the student part

	printf(BLUE);
	for(int i = 0; i < num_students; i++)
	{
		scanf( "%lf %d %d %d" , &arr_students[i].calibre, &arr_students[i].pref[0], &arr_students[i].pref[1], &arr_students[i].pref[2]);
		scanf( "%d" , &arr_students[i].time);

		arr_students[i].id = i;
		arr_students[i].status = S_NR;
	}
	printf(RESET);


	// input for the labs part

	printf(MAGENTA);
	for(int i = 0; i < num_labs; i++)
	{
		scanf( "%s" , arr_labs[i].name);
		scanf( "%d %d" , &arr_labs[i].members, &arr_labs[i].limit);

		arr_labs[i].status = L_OPEN;
		arr_labs[i].present_ta = 0;

		//allocating TA status for the particular lab.
		for(int j = 0; j < arr_labs[i].members ; j++)
		{
			arr_labs[i].tas[j].id = j;
			arr_labs[i].tas[j].status = TA_FREE;
			arr_labs[i].tas[j].no_taship = 0;
		}

	}
	printf(RESET);
//input part ends here
//************************************************************************************************
// processing part starts here.

	pthread_mutex_init(&timelock, NULL);
	pthread_cond_init(&timecond, NULL);

	for(int i = 0; i < num_labs; i++)
	{
		pthread_mutex_init(&arr_labs[i].ta_lock, NULL);
	}

	// We have to create thread of each student and course since they have to work in parallel.
	pthread_t student_threads[num_students];

	for(int i = 0; i < num_students; i++)
		if(pthread_create(&student_threads[i], NULL, &execute_student, (void *)&arr_students[i]) != 0)
			perror("Failed to create student thread");

	// We have to create thread of each courses.
	pthread_t course_threads[num_courses];

	for(int i = 0; i < num_courses; i++)
		if(pthread_create(&course_threads[i], NULL, &execute_course, (void *)&arr_courses[i]) != 0)
			perror("Failed to create courses thread");

	//Time thread runs infinitely so we wont wait for this thread execution to end.
	// and hence no pthread_join is used for this.
	pthread_t time_thread;
	printf(RED "Time elapsed is -- %d seconds.............\n" RESET, time_elapsed);
	if(pthread_create(&time_thread, NULL, &time_tracker, NULL) != 0)
		perror("Failed to create time thread");


	//Joining of threads here to ensure that they finished the execution.
	for(int i = 0; i < num_students; i++)
		if(pthread_join(student_threads[i], NULL) != 0 )
			perror("Failed to join student thread");

	pthread_mutex_destroy(&timelock);
	pthread_cond_destroy(&timecond);

	for(int i = 0; i < num_labs; i++)
	{
		pthread_mutex_destroy(&arr_labs[i].ta_lock);
	}

	return 0;
}

//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/student.h ####################//

#include "datatypes.h"
#include "colors.h"
#include "states.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

void * execute_student(void * s_element);
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/courses.h ####################//

#include "datatypes.h"
#include "colors.h"
#include "states.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

void * execute_course(void * c_element);

//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/courses.c ####################//

#include "courses.h"

void * execute_course(void * c_element)
{
	course * c_arr = (course *)c_element;
	int markem[num_students];	//to mark the id of students as 1 for those who took the course.
	for(int i = 0; i < num_students; i++)
		markem[i] = 0;

	
	while(1)
	{
		// checking how many students have got their course preference.
		for(int i = 0; i < num_students; i++)
		{
			//pthread_mutex_lock(&studentptr[i].student_status_lock);
			if(studentptr[i].current_pref == c_arr->id && markem[studentptr[i].id] == 0 )
			{
				if(studentptr[i].status == S_WFFC || studentptr[i].status == S_WFSC || studentptr[i].status == S_WFTC)
				{
					markem[studentptr[i].id] = 1;
					c_arr->present_students++;
				}
			}
			//pthread_mutex_unlock(&studentptr[i].student_status_lock);
		}
		int tafound = 0;
		int slots_alloted, slots_filled;
		if(c_arr->present_students > 0 && c_arr->status == C_WFTA)//allocate TA.
		{

			for(int i = 0; i < c_arr->n_labs; i++)
			{
				int present_lab = c_arr->lab_id[i];
				if(tafound)break;
				if(labptr[present_lab].status == L_OPEN)
				{
					pthread_mutex_lock(&labptr[present_lab].ta_lock);
					char * name = labptr[present_lab].name;

					if(labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship >= labptr[present_lab].limit)
					{
						labptr[present_lab].status = L_CLOSE;
						printf(TA_Color);
						printf("Lab %s no longer has students available for TA ship\n", name);
						printf(RESET);
					}
					else
					{
						tafound++;
						c_arr->ta_lab = present_lab;
						c_arr->ta_id = labptr[present_lab].present_ta;
						labptr[present_lab].tas[labptr[present_lab].present_ta].status = TA_WORKING;
						labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship++;
						printf(TA_Color);
						printf("TA %d from lab %s has been allocated to course %s for TAship %d\n", c_arr->ta_id, name, c_arr->name,labptr[present_lab].tas[labptr[present_lab].present_ta].no_taship);
						printf(RESET);
						labptr[present_lab].present_ta = (labptr[present_lab].present_ta + 1) % (labptr[present_lab].members);
						c_arr->status = C_WFS;
						//pthread_mutex_unlock(&labptr[present_lab].ta_lock);

					}
					pthread_mutex_unlock(&labptr[present_lab].ta_lock);

				}
				else continue; //go search in the next lab.
			}

			if(tafound == 0)// withdraw the course;
			{
				printf(Course_Color);
				printf("Course %s does not have any TA's eligible and is removed from course offerings\n", c_arr->name);
				printf(RESET);
				c_arr->status = C_BYE;
			}

		}
		else if(c_arr->status == C_WFS )
		{
			slots_alloted = rand()%(c_arr->max_slots);
			slots_alloted++;
			slots_filled = 0;
			printf(Course_Color);
			printf("Course %s has been allocated %d seats\n", c_arr->name, slots_alloted);
			printf(RESET);
			//change student state to attending tute;

			for(int i = 0; i < num_students && slots_filled < slots_alloted ; i++)
			{
				if(markem[i] == 1)
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					slots_filled++;
					if(studentptr[i].status ==  S_WFFC)
						studentptr[i].status = S_ATFFC;
					else if(studentptr[i].status == S_WFSC)
						studentptr[i].status = S_ATFSC;
					else if(studentptr[i].status == S_WFTC)
						studentptr[i].status = S_ATFTC;

					markem[i] = 2;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&(studentptr[i].student_status_cond));
				}
			}
			c_arr->status = C_TUTE;
			c_arr->present_students -= slots_filled;

		}
		else if(c_arr->status == C_TUTE)
		{
			//change student state to finished tute.	;
			printf(Course_Color);
			printf("Tutorial has started for Course %s with %d seats filled out of %d\n", c_arr->name, slots_filled, slots_alloted);
			printf(RESET);
			sleep(2); //Assuming tute goes on for 2 seconds.
			//Now tute is completed.
			printf(Course_Color);
			printf("TA %d from lab %s has completed the tutorial and left the course %s\n", c_arr->ta_id, labptr[c_arr->ta_lab].name, c_arr->name );
			printf(RESET);
			tafound = 0;
			c_arr->status = C_WFTA;

			for(int i = 0; i < num_students; i++)
			{
				if(markem[i] == 2)// 2 means they are attending tute.
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					if(studentptr[i].status == S_ATFFC)
						studentptr[i].status = S_FTFC;
					else if(studentptr[i].status == S_ATFSC)
						studentptr[i].status = S_FTSC;
					else if(studentptr[i].status == S_ATFTC)
						studentptr[i].status = S_FTTC;
					markem[i] = 0;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&(studentptr[i].student_status_cond));
				}
			}

		}
		else if(c_arr->status == C_BYE)
		{
			for(int i = 0; i < num_students; i++)
			{
				if(markem[i] == 1)//1 means waiting
				{
					pthread_mutex_lock(&studentptr[i].student_status_lock);
					if(studentptr[i].status ==  S_WFFC)
						studentptr[i].status = S_WFSC;
					else if(studentptr[i].status == S_WFSC)
						studentptr[i].status = S_WFTC;
					else if(studentptr[i].status == S_WFTC)
						studentptr[i].status = S_BYE;

					markem[i] = 0;
					pthread_mutex_unlock(&studentptr[i].student_status_lock);
					pthread_cond_signal(&studentptr[i].student_status_cond);
				}
			}
			//break;//removed break bcoz after this course thread ends some students choose this course and ends up in deadlock;
			//so without break it will let the students go on to next course.
		}
		usleep(5000);

	}// while(1) loop ends here.

}

//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/colors.h ####################//

#ifndef _COLORS
#define _COLORS

#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define RESET "\033[0m"

#define Course_Color GREEN
#define Student_Color YELLOW
#define TA_Color	MAGENTA

#endif
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q1/datatypes.h ####################//

#ifndef _DATATYPES
#define _DATATYPES
#include <pthread.h>

typedef struct course course;
typedef struct student student;
typedef struct lab lab;
typedef struct ta ta;

//These are used to point to the respective arrays.
course * courseptr;
student * studentptr;
lab * labptr;

//global variables.
int num_students, num_labs, num_courses;
int time_elapsed;

struct course
{	
	int id;					// id of the course.	( 0 based indexing )
	char name[100];			// name of the course
	double interest; 		//interest quotient lies between 0 and 1
	int max_slots; 			// course max_slots
	int n_labs;				// number of labs from which course accepts TAs.
	int lab_id[50];			// ids of the labs from which course accepts TAs.

	//status variable is used to store the status of the course
	int status;
	// course chooses a TA only if present_students are greater than zero.
	int present_students; // this is to know how many students are presently interested in the course.

	int ta_lab;
	int ta_id;
};


struct student
{
	int id;					// student id ( 0 based indexing )
	double calibre;			// calibre quotient of student
	int pref[3];			// Course if of student preference. Pref 1 is 1st priority
	int time;				// time at which he/she registers for the course.
	int current_pref;		// waiting for which course, if attending tute then -1.
	int status;				// status variable is to store the status of the student.
	pthread_mutex_t student_status_lock; // this is reqd as courses can change status of student structs.
	pthread_cond_t student_status_cond;
};

struct ta
{
	int id;				// id of the ta under each lab
	int no_taship;		// no of ta ships done.
	int status; 		//presently free or doing a ta work


};

struct lab
{
	int id;					// lab is ( 0 based indexing )
	char name[100];			// name of the lab
	int members; 			// no. of TAs in the lab
	int limit;				// no. of times a member of the lab can become a TA

	int status;
	ta tas[100];			// stores the details of the TAs under this lab.
	int present_ta;			
	// In order to execute the bonus point I am allocating TAs in the order of their ids, so present_ta keeps the id
	// of the TA who can be hired.
	pthread_mutex_t ta_lock; // to avoid multiple courses choosing same TA at the same moment.
	
};



#endif