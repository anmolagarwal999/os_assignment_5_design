
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q1/student.c ####################//

#include "q1.h"

// Function to wait for, attend and leave the tutorial
void joinTutorial(Student *S)
{
    pthread_mutex_lock(&S->mutex);

    // Before waiting for the tutorial, setting status
    S->enrolled = STUDENT_WAITING;

    // Waiting to get allotted a seat, and then till tutorial is over
    while (S->enrolled != STUDENT_LEFT_TUTORIAL)
        pthread_cond_wait(&S->cond_tut, &S->mutex);

    // Leaving the tutorial
    pthread_mutex_unlock(&S->mutex);
}

// The thread for students
void *studentThread(void *arg)
{
    Student *S = (Student *)arg;

    // Waiting for the initial time to simulate choosing of course preferences
    sleep(S->time);
    pthread_mutex_lock(&print_lock);
    printf("Student %d has filled in preferences for course registration\n", S->index);
    pthread_mutex_unlock(&print_lock);
    S->current_priority = 0;

    while (S->current_priority < 3)
    {
        /*------CORE MULTITHREADING LOGIC------*/

        // Checking if course is available
        if (!Course_available[S->courses[S->current_priority]])
        {
            S->current_priority++;

            // If this happened to be the student's last course
            if (S->current_priority >= 3)
            {
                pthread_mutex_lock(&print_lock);
                printf(GREEN "Student %d couldn’t get any of his preferred courses\n" RESET, S->index);
                pthread_mutex_unlock(&print_lock);
                break;
            }
            else
            {
                pthread_mutex_lock(&print_lock);
                printf(GREEN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET, S->index, Courses[S->courses[S->current_priority - 1]].course_name, S->current_priority, Courses[S->courses[S->current_priority]].course_name, S->current_priority + 1);
                pthread_mutex_unlock(&print_lock);
                continue;
            }
        }

        int prio = S->current_priority;

        // Wait for and eventually attend the tutorial
        joinTutorial(S);

        // If course was removed and student was upgraded in the meantime
        if (prio < S->current_priority)
        {
            if (S->current_priority >= 3)
            {
                pthread_mutex_lock(&print_lock);
                printf(GREEN "Student %d couldn’t get any of his preferred courses\n" RESET, S->index);
                pthread_mutex_unlock(&print_lock);
                break;
            }
            else
            {
                pthread_mutex_lock(&print_lock);
                printf(GREEN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET, S->index, Courses[S->courses[S->current_priority - 1]].course_name, S->current_priority, Courses[S->courses[S->current_priority]].course_name, S->current_priority + 1);
                pthread_mutex_unlock(&print_lock);
                continue;
            }
        }

        /*------PERMANENTLY CHOOSING/WITHDRAWING------*/

        pthread_mutex_lock(&S->mutex);

        // Calculating probability of student to choose course permanently
        double probability = S->calibre * Courses[S->courses[S->current_priority]].interest;

        // Choosing permanently
        if (probability >= PROB_OF_CHOOSING)
        {
            pthread_mutex_lock(&print_lock);
            printf(GREEN "Student %d has selected the course %s permanently\n" RESET, S->index, Courses[S->courses[S->current_priority]].course_name);
            pthread_mutex_unlock(&print_lock);
            S->current_priority = 3;
            pthread_mutex_unlock(&S->mutex);
            break;
        }
        // Withdrawing
        else
        {
            pthread_mutex_lock(&print_lock);
            printf(GREEN "Student %d has withdrawn from course %s\n" RESET, S->index, Courses[S->courses[S->current_priority]].course_name);
            pthread_mutex_unlock(&print_lock);
            S->current_priority++;
        }

        /*------EXITING SIMULATION OR CHANGING PRIORITY------*/

        // If the student could not choose a course permanently
        pthread_mutex_lock(&print_lock);

        if (S->current_priority >= 3)
            printf(GREEN "Student %d couldn’t get any of his preferred courses\n" RESET, S->index);
        else
            printf(GREEN "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET, S->index, Courses[S->courses[S->current_priority - 1]].course_name, S->current_priority, Courses[S->courses[S->current_priority]].course_name, S->current_priority + 1);

        pthread_mutex_unlock(&print_lock);
        pthread_mutex_unlock(&S->mutex);
    }

    pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q1/main.c ####################//

#include "q1.h"

Lab *Labs = NULL;
Course *Courses = NULL;
Student *Students = NULL;
bool *Course_available = NULL;
int num_students = 0, num_labs = 0, num_courses = 0;
pthread_mutex_t print_lock;

// Function to take input and initialise values
void takeInput()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    // Allocate memory for the arrays
    Labs = (Lab *)malloc(num_labs * sizeof(Lab));
    Courses = (Course *)malloc(num_courses * sizeof(Course));
    Course_available = (bool *)malloc(num_courses * sizeof(bool));
    Students = (Student *)malloc(num_students * sizeof(Student));

    // Taking input for courses
    for (int i = 0; i < num_courses; i++)
    {
        scanf("%s %lf %d %d", Courses[i].course_name, &Courses[i].interest, &Courses[i].course_max_slot, &Courses[i].num_labs);
        Courses[i].index = i;
        Course_available[i] = true;

        // Mallocing an array for labs
        Courses[i].clabs = (int *)malloc(Courses[i].num_labs * sizeof(int));

        for (int j = 0; j < Courses[i].num_labs; j++)
            scanf("%d", &Courses[i].clabs[j]);
    }

    // Taking input for students
    for (int i = 0; i < num_students; i++)
    {
        scanf("%lf %d %d %d %d", &Students[i].calibre, &Students[i].courses[0], &Students[i].courses[1], &Students[i].courses[2], &Students[i].time);
        Students[i].current_priority = STUDENT_CHOOSING;
        Students[i].enrolled = STUDENT_CHOOSING;
        Students[i].index = i;
        pthread_mutex_init(&Students[i].mutex, NULL);
        pthread_cond_init(&Students[i].cond_tut, NULL);
    }

    // Taking input for labs
    for (int i = 0; i < num_labs; i++)
    {
        int n_i;
        scanf("%s %d %d", Labs[i].name, &n_i, &Labs[i].limit);
        Labs[i].num_TAs = n_i;
        Labs[i].TA_times = (int *)calloc(n_i, sizeof(int));
        Labs[i].TA_available = (bool *)malloc(n_i * sizeof(bool));
        Labs[i].TA_mutexes = (pthread_mutex_t *)malloc(n_i * sizeof(pthread_mutex_t));
        for (int j = 0; j < n_i; j++)
        {
            Labs[i].TA_available[j] = true;
            pthread_mutex_init(&Labs[i].TA_mutexes[j], NULL);
        }
    }

    pthread_mutex_init(&print_lock, NULL);
}

// Function to create the threads for each entity instance
void createThreads()
{
    // Creating threads for courses
    for (int i = 0; i < num_courses; i++)
        pthread_create(&Courses[i].tid, NULL, courseThread, &Courses[i]);

    // Creating threads for students
    for (int i = 0; i < num_students; i++)
        pthread_create(&Students[i].tid, NULL, studentThread, &Students[i]);
}

// Function to perform cleanup before exiting
void cleanup()
{
    // For students
    for (int i = 0; i < num_students; i++)
    {
        pthread_join(Students[i].tid, NULL);
        pthread_mutex_destroy(&Students[i].mutex);
        pthread_cond_destroy(&Students[i].cond_tut);
    }

    // For courses
    for (int i = 0; i < num_courses; i++)
    {
        pthread_join(Courses[i].tid, NULL);
        free(Courses[i].clabs);
    }

    // For labs
    for (int i = 0; i < num_labs; i++)
    {
        free(Labs[i].TA_times);
        for (int j = 0; j < Labs[i].num_TAs; j++)
            pthread_mutex_destroy(&Labs[i].TA_mutexes[j]);
        free(Labs[i].TA_mutexes);
    }

    free(Labs);
    free(Courses);
    free(Students);

    pthread_mutex_destroy(&print_lock);
}

int main(void)
{
    srand(time(0));
    takeInput();
    createThreads();
    cleanup();
}
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q1/q1.h ####################//

#ifndef _Q1_H_
#define _Q1_H_

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

// Constants
#define MAX_NAME_LEN 100
#define PROB_OF_CHOOSING 1

// States (S->current_priority)
#define STUDENT_CHOOSING -1        // Student is waiting and choosing preferences

// States (S->enrolled)
#define STUDENT_WAITING 0          // Student is waiting for a course to be available
#define STUDENT_IN_TUTORIAL 1      // Student is in the tutorial
#define STUDENT_LEFT_TUTORIAL 2    // Student has left the tutorial

// Colours
#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define RESET "\x1B[0m"

// The Lab entity
typedef struct Lab
{
    char name[MAX_NAME_LEN];        // Lab name
    int num_TAs;                    // Number of TAs the lab has
    int limit;                      // Maximum number of times a TA can take a tutorial
    int *TA_times;                  // Array of times each TA has taken a tutorial
    bool *TA_available;             // Array of booleans indicating whether a TA is available
    pthread_mutex_t *TA_mutexes;    // Array of mutex locks for each TA
} Lab;

// The Course entity
typedef struct Course
{
    pthread_t tid;                  // Thread ID corresponding to this Course struct 
    int index;                      // Index of Course struct in Courses array
    char course_name[MAX_NAME_LEN]; // Course name
    double interest;                // Interest level in course
    int num_labs;                   // Number of labs from which Course can choose a TA
    int *clabs;                     // Array of labs from which Course can choose a TA
    int course_max_slot;            // Maximum number of slots for a tutorial in this Course
} Course;

// The Student entity
typedef struct Student
{
    pthread_t tid;                  // Thread ID corresponding to this Student struct
    int index;                      // Index of Student struct in Students array
    int courses[3];                 // Array of courses student is interested in
    int time;                       // Time after which student chooses preferred courses
    double calibre;                 // Calibre level of students
    int current_priority;           // Index of current course chosen in Student::courses array
    int enrolled;                   // 0 before tutorial, 1 in tutorial and 2 after tutorial
    pthread_mutex_t mutex;          // Mutex lock for this Student struct
    pthread_cond_t cond_tut;        // Conditional variable for tutorial
} Student;

// Declaring the global arrays
extern Lab *Labs;
extern Course *Courses;
extern bool *Course_available;      // An array that indicates whether a course has terminated or not
extern Student *Students;
extern int num_students, num_labs, num_courses;
extern pthread_mutex_t print_lock;

// Student function declarations
void joinTutorial(Student *S);
void *studentThread(void *arg);

// Course function declarations
void chooseTA(Course *C, int *l, int *t);
void allotStudents(Course *C, int *num_students_preferring, bool to_be_chosen[]);
void releaseStudents(Course *C, bool to_be_chosen[]);
void removeCourse(Course *C);
void *courseThread(void *arg);

#endif
//###########FILE CHANGE ./main_folder/ARJUN MURALEEDHARAN_305984_assignsubmission_file_/q1/course.c ####################//

#include "q1.h"

// Function to choose a TA to take a tutorial
// LOGIC: First look for free TAs, then look for TAs who are not free but could be eligible
void chooseTA(Course *C, int *l, int *t)
{
    bool TA_found = false;

    // 1. Looking for free TAs (who are not taking a course)
    // Choosing a lab to select a TA from
    for (*l = 0; *l < C->num_labs; (*l)++)
    {
        // Finding an unoccupied TA from that lab
        for (*t = 0; *t < Labs[C->clabs[*l]].num_TAs; (*t)++)
        {
            // Locking the TA
            pthread_mutex_lock(&Labs[C->clabs[*l]].TA_mutexes[*t]);

            // If the TA is available
            if (Labs[C->clabs[*l]].TA_available[*t] && Labs[C->clabs[*l]].TA_times[*t] < Labs[C->clabs[*l]].limit)
            {
                TA_found = true;
                Labs[C->clabs[*l]].TA_available[*t] = false;
                break;
            }

            pthread_mutex_unlock(&Labs[C->clabs[*l]].TA_mutexes[*t]);
        }

        // If a TA was found
        if (TA_found)
            break;
    }

    // 2. If no free TA was found yet, look for eligible TAs who are currently taking a course
    if (!TA_found)
    {
        // Choosing a lab to select a TA from
        for (*l = 0; *l < C->num_labs; (*l)++)
        {
            // Finding an eligible TA from that lab
            for (*t = 0; *t < Labs[C->clabs[*l]].num_TAs; (*t)++)
            {
                // Locking the TA or waiting for them to become available
                pthread_mutex_lock(&Labs[C->clabs[*l]].TA_mutexes[*t]);

                // If the TA is available
                if (Labs[C->clabs[*l]].TA_times[*t] < Labs[C->clabs[*l]].limit)
                {
                    TA_found = true;
                    Labs[C->clabs[*l]].TA_available[*t] = false;
                    break;
                }

                pthread_mutex_unlock(&Labs[C->clabs[*l]].TA_mutexes[*t]);
            }

            // If a TA was found
            if (TA_found)
                break;
        }
    }
}

// Function to allot seats in tutorial to students
void allotStudents(Course *C, int *num_students_preferring, bool to_be_chosen[])
{
    for (int i = 0; i < num_students; i++)
    {
        // If the student is waiting for this course
        if (Students[i].current_priority >= 0 && Students[i].current_priority < 3 && Students[i].courses[Students[i].current_priority] == C->index && Students[i].enrolled == STUDENT_WAITING)
        {
            to_be_chosen[i] = true;
            (*num_students_preferring)++;
        }
        else
            to_be_chosen[i] = false;
    }

    // Selecting a random number for the course slot
    int d = rand() % C->course_max_slot + 1;
    int actual_number = (d > *num_students_preferring ? *num_students_preferring : d);
    pthread_mutex_lock(&print_lock);
    printf(MAGENTA "Course %s has been allocated %d seats\n" RESET, C->course_name, d);
    pthread_mutex_unlock(&print_lock);

    // Allocating the slots
    for (int i = 0, allotted = 0; i < num_students && allotted < actual_number; i++)
    {
        if (to_be_chosen[i])
        {
            Students[i].enrolled = STUDENT_IN_TUTORIAL;
            allotted++;
            pthread_mutex_lock(&print_lock);
            printf(GREEN "Student %d has been allocated a seat in course %s\n" RESET, i, C->course_name);
            pthread_mutex_unlock(&print_lock);
        }
    }

    pthread_mutex_lock(&print_lock);
    printf(YELLOW "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, C->course_name, actual_number, d);
    pthread_mutex_unlock(&print_lock);
}

// Function to release the students once the tutorial is over
void releaseStudents(Course *C, bool to_be_chosen[])
{
    for (int i = 0; i < num_students; i++)
        if (to_be_chosen[i] && Students[i].enrolled == STUDENT_IN_TUTORIAL && Students[i].courses[Students[i].current_priority] == C->index)
        {
            Students[i].enrolled = STUDENT_LEFT_TUTORIAL;
            pthread_cond_signal(&Students[i].cond_tut);
        }
}

// Function to perform actions for students are interested in a terminated course
void removeCourse(Course *C)
{
    for (int i = 0; i < num_students; i++)
    {
        // If the student is waiting for this course
        if (Students[i].current_priority >= 0 && Students[i].current_priority < 3 && Students[i].courses[Students[i].current_priority] == C->index && (Students[i].enrolled == STUDENT_WAITING || Students[i].enrolled == STUDENT_IN_TUTORIAL))
        {
            Students[i].current_priority++;
            Students[i].enrolled = STUDENT_LEFT_TUTORIAL;
            pthread_cond_signal(&Students[i].cond_tut);
        }
    }
}

// The thread for courses
void *courseThread(void *arg)
{
    Course *C = (Course *)arg;

    for (;;)
    {
        /*------CHOOSING TA------*/

        int l; // Lab index
        int t; // TA index
        chooseTA(C, &l, &t);

        // If no TA could be chosen, course is dropped, breaking out of infinite loop
        if (l == C->num_labs)
        {
            pthread_mutex_lock(&print_lock);
            printf(RED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" RESET, C->course_name);
            pthread_mutex_unlock(&print_lock);
            break;
        }

        // Checking if students are currently interested in the course
        bool student_interested = false;
        for (int i = 0; i < num_students; i++)
        {
            // If the student is interested in this course
            if (Students[i].current_priority >= 0 && Students[i].current_priority < 3 && Students[i].courses[Students[i].current_priority] == C->index && Students[i].enrolled == STUDENT_WAITING)
            {
                student_interested = true;
                break;
            }
        }

        if (!student_interested)
        {
            Labs[C->clabs[l]].TA_available[t] = true;
            pthread_mutex_unlock(&Labs[C->clabs[l]].TA_mutexes[t]);

            // Checking if any students are interested at all in the course
            bool all_students_finished = true;
            for (int i = 0; i < num_students; i++)
                for (int j = Students[i].current_priority; j < 3; j++)
                    if (Students[i].courses[j] == C->index)
                    {
                        all_students_finished = false;
                        goto check;
                    }

        check:
            // If no student is interested in this course, course is dropped, breaking out of infinite loop
            if (all_students_finished)
                break;

            continue;
        }

        /*------ASSIGNING TA TO COURSE------*/

        // Updating TA status
        Labs[C->clabs[l]].TA_times[t]++;

        // Checking number of times TA was assigned
        pthread_mutex_lock(&print_lock);
        printf(YELLOW "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" RESET, t, Labs[C->clabs[l]].name, C->course_name, Labs[C->clabs[l]].TA_times[t]);
        printf(YELLOW "Course %s has been allocated TA %d from lab %s\n" RESET, C->course_name, t, Labs[C->clabs[l]].name);
        pthread_mutex_unlock(&print_lock);

        /*------ALLOTTING SLOTS------*/

        int num_students_preferring = 0;
        bool to_be_chosen[num_students];
        allotStudents(C, &num_students_preferring, to_be_chosen);

        /*------TAKING TUTORIAL------*/

        // Sleeping for a while to simulate the TA taking the tutorial
        sleep(2);

        /*------RELEASING STUDENTS------*/

        releaseStudents(C, to_be_chosen);

        /*------RELEASING TA FROM COURSE------*/

        // Release the TA
        pthread_mutex_lock(&print_lock);
        printf(YELLOW "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, t, Labs[C->clabs[l]].name, C->course_name);
        pthread_mutex_unlock(&print_lock);

        // Checking the lab to see if it can offer any more TAs
        bool any_TA_available = false;
        for (int i = 0; i < Labs[C->clabs[l]].num_TAs; i++)
            if (Labs[C->clabs[l]].TA_times[i] < Labs[C->clabs[l]].limit)
            {
                any_TA_available = true;
                break;
            }

        pthread_mutex_lock(&print_lock);

        if (!any_TA_available)
            printf(RED "Lab %s no longer has students available for TA ship\n" RESET, Labs[C->clabs[l]].name);
        
        pthread_mutex_unlock(&print_lock);        

        // Releasing the TA, making them free for other tutorials if eligible
        Labs[C->clabs[l]].TA_available[t] = true;
        pthread_mutex_unlock(&Labs[C->clabs[l]].TA_mutexes[t]);
    }

    // Removing the course from the course offerings
    Course_available[C->index] = false;
    removeCourse(C);

    pthread_exit(NULL);
}