
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/util.c ####################//

// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern availability_type availability;

void get_input()
{
    scanf("%d %d %d", &input.students, &input.labs, &input.courses);

    set.students = malloc(sizeof(student_type) * input.students);
    set.labs = malloc(sizeof(lab_type) * input.labs);
    set.courses = malloc(sizeof(course_type) * input.courses);

    for (int i = 0; i < input.courses; i++)
    {
        set.courses[i].id = i;
        scanf("%s %f %d %d", set.courses[i].name, &set.courses[i].interest, &set.courses[i].course_max_slots, &set.courses[i].num_labs);

        set.courses[i].labs = malloc(sizeof(int) * set.courses[i].num_labs);

        for (int j = 0; j < set.courses[i].num_labs; j++)
            scanf("%d", &set.courses[i].labs[j]);
    }

    for (int i = 0; i < input.students; i++)
    {
        set.students[i].id = i;
        scanf("%f %d %d %d %d", &set.students[i].calibre, &set.students[i].preferences[0], &set.students[i].preferences[1], &set.students[i].preferences[2], &set.students[i].fill_time);
    }

    for (int i = 0; i < input.labs; i++)
    {
        set.labs[i].id = i;
        scanf("%s %d %d", set.labs[i].name, &set.labs[i].num_tas, &set.labs[i].max_courses);
    }
}

void initialise_all()
{
    locks.lab_tas = malloc(sizeof(pthread_mutex_t *) * input.labs);
    locks.course_spots = malloc(sizeof(pthread_mutex_t) * input.courses);
    locks.student_status = malloc(sizeof(pthread_mutex_t) * input.students);

    availability.lab_tas = malloc(sizeof(pthread_cond_t *) * input.labs);
    availability.course_spots = malloc(sizeof(pthread_cond_t) * input.courses);
    availability.student_status = malloc(sizeof(pthread_cond_t) * input.students);

    status.lab_tas = malloc(sizeof(int **) * input.labs);
    status.course_spots = malloc(sizeof(int) * input.courses);
    status.student_status = malloc(sizeof(int) * input.students);

    for (int i = 0; i < input.labs; i++)
    {
        locks.lab_tas[i] = malloc(sizeof(pthread_mutex_t) * set.labs[i].num_tas);
        availability.lab_tas[i] = malloc(sizeof(pthread_cond_t) * set.labs[i].num_tas);
        status.lab_tas[i] = malloc(sizeof(int *) * set.labs[i].num_tas);

        for (int j = 0; j < set.labs[i].num_tas; j++)
        {
            status.lab_tas[i][j] = malloc(sizeof(int) * 2);
            status.lab_tas[i][j][0] = set.labs[i].max_courses;
            status.lab_tas[i][j][1] = 1;
            pthread_mutex_init(&locks.lab_tas[i][j], NULL);
            pthread_cond_init(&availability.lab_tas[i][j], NULL);
        }
    }

    for (int i = 0; i < input.courses; i++)
    {
        status.course_spots[i] = 0;
        pthread_mutex_init(&locks.course_spots[i], NULL);
        pthread_cond_init(&availability.course_spots[i], NULL);
    }

    for (int i = 0; i < input.students; ++i)
    {
        status.student_status[i] = -1 * (set.students[i].preferences[0] + 1);
        pthread_mutex_init(&locks.student_status[i], NULL);
        pthread_cond_init(&availability.student_status[i], NULL);
    }
}


//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/sims.h ####################//

#ifndef SIMS_H
#define SIMS_H

void *student_simulator(void *args);
void *course_simulator(void *args);

#endif
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/main.c ####################//

// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "util.h"
#include "sims.h"

input_type input;
set_type set;

threads_type threads;

status_type status;
locks_type locks;
availability_type availability;

signed main()
{
    threads.courses = NULL;
    threads.students = NULL;

    get_input();
    initialise_all();

    threads.courses = malloc(sizeof(pthread_t) * input.courses);
    threads.students = malloc(sizeof(pthread_t) * input.students);

    for (int i =0; i<input.students; i++)
        pthread_create(&threads.students[i], NULL, student_simulator, &set.students[i]);

    for (int i =0; i<input.students; i++)
        pthread_join(threads.students[i], NULL);

    exit(0);
}

//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/util.h ####################//

#ifndef UTIL_H
#define UTIL_H

void get_input();
void initialise_all();

#endif
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/sims.c ####################//

// including necessary libraries
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern availability_type availability;

void *student_simulator(void *args)
{
    student_type student = *(student_type *)args;

    student.current_preference_number = -1;
    student.current_course_preference = -1;

    sleep(student.fill_time);

    printf("\033[31;1mStudent %d has filled in preferences for course registration\033[0;0m\n", student.id);

    for (int i = 0; i < MAX_PREF; i++)
    {
        pthread_mutex_lock(&locks.course_spots[student.preferences[i]]);
        
        if (status.course_spots[student.preferences[i]] != 1 && student.current_course_preference == -1)
        {
            student.current_preference_number = i;
            student.current_course_preference = status.course_spots[student.preferences[i]];
        }
        
        pthread_mutex_unlock(&locks.course_spots[student.preferences[i]]);
    }

    while (student.current_preference_number > -1)
    {
        int withdrawn = 0;

        printf("\033[32;1mStudent %d is looking for course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

        pthread_mutex_lock(&locks.student_status[student.id]);
        status.student_status[student.id] = -1 * (student.current_course_preference + 1);
        pthread_mutex_unlock(&locks.student_status[student.id]);

        pthread_mutex_lock(&locks.course_spots[student.current_course_preference]);

        while (status.course_spots[student.current_course_preference] == 0)
            pthread_cond_wait(&availability.course_spots[student.current_course_preference], &locks.course_spots[student.current_course_preference]);

        if (status.course_spots[student.current_course_preference] > 0)
            status.course_spots[student.current_course_preference]--;
        else
            withdrawn = 1;

        pthread_mutex_unlock(&locks.course_spots[student.current_course_preference]);

        if (withdrawn == 0)
        {
            printf("\033[33;1mStudent %d has been allocated a seat in course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

            pthread_mutex_lock(&locks.student_status[student.id]);
            status.student_status[student.id] = student.current_course_preference + 1;
            pthread_mutex_unlock(&locks.student_status[student.id]);

            pthread_mutex_lock(&locks.student_status[student.id]);

            while (status.student_status[student.id] != 0)
                pthread_cond_wait(&availability.student_status[student.id], &locks.student_status[student.id]);

            pthread_mutex_unlock(&locks.student_status[student.id]);

            float x = (float)rand() / (float)(RAND_MAX / 1.0);
            if (x < set.courses[student.current_course_preference].interest * student.calibre)
            {
                printf("\033[34;1mStudent %d has selected course %s permanently\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);

                pthread_mutex_lock(&locks.student_status[student.id]);
                status.student_status[student.id] = 0;
                pthread_mutex_unlock(&locks.student_status[student.id]);

                return 0;
            }
            else
            {
                printf("\033[35;1mStudent %d has withdrawn from course %s\033[0;0m\n", student.id, set.courses[student.current_course_preference].name);
                withdrawn = 1;
            }
        }

        if (withdrawn == 1)
        {
            int previous_course_preference = student.current_course_preference;

            for (int i = student.current_preference_number + 1; i < MAX_PREF; i++)
            {

                pthread_mutex_lock(&locks.course_spots[student.preferences[i]]);

                if (status.course_spots[student.preferences[i]] != -1)
                {
                    student.current_preference_number = i;
                    student.current_course_preference = status.course_spots[student.preferences[i]];
                }
    
                pthread_mutex_unlock(&locks.course_spots[student.preferences[i]]);
            }
            if (previous_course_preference == student.current_course_preference)
                student.current_preference_number = -1;
            else
                printf("\033[36;1mStudent %d has changed current preference from %s to %s\033[0;0m\n", student.id, set.courses[previous_course_preference].name, set.courses[student.current_course_preference].name);
        }
    }

    if (student.current_preference_number == -1)
        printf("\033[33;1mStudent %d did not get any of their preferred courses\033[0;0m\n", student.id);

    return 0;
}

void *course_simulator(void *args)
{
    course_type course = *(course_type *)args;

    while (1)
    {
        course.chosen_ta_lab_id = -1;
        course.chosen_ta_id = -1;

        for (int i = 0; i < course.num_labs; i++)
        {
            int lab_id = course.labs[i];

            for (int j = 0; j < set.labs[lab_id].num_tas; j++)
            {

                pthread_mutex_lock(&locks.lab_tas[lab_id][j]);

                while (status.lab_tas[lab_id][j][0] > 0 && status.lab_tas[lab_id][j][1] <= 0)
                {
                    pthread_cond_wait(&availability.lab_tas[lab_id][j], &locks.lab_tas[lab_id][j]);
                }

                if (status.lab_tas[lab_id][j][0] > 0 && status.lab_tas[lab_id][j][1] > 0)
                {
                    status.lab_tas[lab_id][j][0]--;
                    status.lab_tas[lab_id][j][1] = 0;

                    course.chosen_ta_lab_id = lab_id;
                    course.chosen_ta_id = j;
                }

                pthread_mutex_unlock(&locks.lab_tas[lab_id][j]);

                if (course.chosen_ta_id != -1)
                    break;
            }
            if (course.chosen_ta_id != -1)
                break;
        }

        if (course.chosen_ta_id == -1)
        {
            pthread_mutex_lock(&locks.course_spots[course.id]);
            
            status.course_spots[course.id] = -1;
            pthread_cond_broadcast(&availability.course_spots[course.id]);
            
            pthread_mutex_unlock(&locks.course_spots[course.id]);
            
            printf("\033[31;1mCourse %s does not have any TA's eligible and is removed from course offerings\033[0;0m\n", course.name);
            break;
        }

        pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);
        printf("\033[32;1mTA %d from lab %s has been allocated to course %s for their TA ship number %d\033[0;0m\n", course.chosen_ta_id, set.labs[course.chosen_ta_lab_id].name, course.name, set.labs[course.chosen_ta_lab_id].max_courses - status.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id][0]);
        pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        int left_spots_in_lab = 0;
        for (int i = 0; i < set.labs[course.chosen_ta_lab_id].num_tas; i++)
        {
            pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][i]);
            left_spots_in_lab += status.lab_tas[course.chosen_ta_lab_id][i][0];
            pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][i]);
        }
        if (left_spots_in_lab == 0)
            printf("\033[34;1mLab %s no longer has mentors available for TAship\033[0;0m\n", set.labs[course.chosen_ta_lab_id].name);

        int tut_slots = (rand() % course.course_max_slots) + 1;

        pthread_mutex_lock(&locks.course_spots[course.id]);
        status.course_spots[course.id] = tut_slots;
        pthread_mutex_unlock(&locks.course_spots[course.id]);

        printf("\033[36;1mCourse %s has been allocated %d seats\033[0;0m\n", course.name, tut_slots);

        int waiting_students = tut_slots;
        int filled_seats = 0;

        while ((filled_seats < 1) || (waiting_students != 0 && filled_seats < tut_slots))
        {
            pthread_cond_broadcast(&availability.course_spots[course.id]);

            waiting_students = 0;
            filled_seats = 0;

            for (int i = 0; i < input.students; i++)
            {
                pthread_mutex_lock(&locks.student_status[i]);

                if (status.student_status[i] == -1 * (course.id + 1))
                    waiting_students += 1;
                if (status.student_status[i] == (course.id + 1))
                    filled_seats += 1;
                
                pthread_mutex_unlock(&locks.student_status[i]);
            }

            if (waiting_students == 0)
                break;
        }

        filled_seats = 0;
        for (int i = 0; i < input.students; i++)
        {
            pthread_mutex_lock(&locks.student_status[i]);

            if (status.student_status[i] == (course.id + 1))
                filled_seats += 1;
            
            pthread_mutex_unlock(&locks.student_status[i]);
        }

        printf("\033[32;1mTA %d has started tutorial for Course %s with %d seats filled out of %d\033[0;0m\n", course.chosen_ta_id, course.name, filled_seats, tut_slots);

        pthread_mutex_lock(&locks.course_spots[course.id]);
        status.course_spots[course.id] = 0;
        pthread_mutex_unlock(&locks.course_spots[course.id]);

        sleep(TUT_TIME);

        for (int i = 0; i < input.students; i++)
        {
            pthread_mutex_lock(&locks.student_status[i]);

            if (status.student_status[i] == (course.id + 1))
            {
                status.student_status[i] = 0;
                pthread_cond_broadcast(&availability.student_status[i]);
            }

            pthread_mutex_unlock(&locks.student_status[i]);
        }

        printf("\033[35;1mTA %d from lab %s has completed the tutorial and left the course %s\033[0;0m\n", course.chosen_ta_id, set.labs[course.chosen_ta_lab_id].name, course.name);

        pthread_mutex_lock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        status.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id][1] = 1; // the ta is now available to ta some other course
        pthread_cond_broadcast(&availability.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);

        pthread_mutex_unlock(&locks.lab_tas[course.chosen_ta_lab_id][course.chosen_ta_id]);
    
    }

    return 0;
}
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q1/defs.h ####################//

#ifndef DEFS_H
#define DEFS_H

#define MAX_NAME 20
#define MAX_PREF 3
#define TUT_TIME 5

typedef struct input_type
{
    int students;
    int labs;
    int courses;
} input_type;

typedef struct student_type
{
    int id;
    int fill_time;
    float calibre;
    int preferences[MAX_PREF];
    int current_course_preference;
    int current_preference_number;
} student_type;

typedef struct lab_type
{
    int id;
    char name[MAX_NAME];
    int num_tas;
    int max_courses;
} lab_type;

typedef struct course_type
{
    int id;
    char name[MAX_NAME];
    float interest;
    int course_max_slots;
    int num_labs;
    int *labs;
    int chosen_ta_lab_id;
    int chosen_ta_id;
} course_type;

typedef struct set_type
{
    course_type *courses;
    student_type *students;
    lab_type *labs;
} set_type;

typedef struct threads_type
{
    pthread_t *students;
    pthread_t *courses;
} threads_type;

typedef struct status_type
{
    int ***lab_tas;
    int *course_spots;
    int *student_status;
} status_type;

typedef struct locks_type
{
    pthread_mutex_t **lab_tas;
    pthread_mutex_t *course_spots;
    pthread_mutex_t *student_status;
} locks_type;

typedef struct availability_type
{
    pthread_cond_t **lab_tas;
    pthread_cond_t *course_spots;
    pthread_cond_t *student_status;
} availability_type;

#endif