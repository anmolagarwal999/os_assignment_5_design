
//###########FILE CHANGE ./main_folder/George Paul_305816_assignsubmission_file_/2021121006/q1/all.h ####################//

#ifndef _ALL_H
#define _ALL_H

#define MAX_SIZE 50

struct course
{
	int cID;
	char cName[MAX_SIZE];
	float cInterest;
	int cSlots;
	int cLabsNo;
	int cLabs[MAX_SIZE];
};

struct stud
{
	int sID;
	float sCal;
	int sP1;
	int sP2;
	int sP3;
	int sTime;
};

struct lab
{
	int lID;
	char lName[MAX_SIZE];
	int lTasNo;
	int lMaxTaShips;
};

extern struct course couA[MAX_SIZE];
extern struct stud stuA[MAX_SIZE];
extern struct lab labA[MAX_SIZE];

void* courseR(void*);
void* studentR(void*);
void* labR(void*);

#endif
//###########FILE CHANGE ./main_folder/George Paul_305816_assignsubmission_file_/2021121006/q1/student.c ####################//

#include <stdio.h>
#include <stdlib.h>

#include "all.h"

// student routine - one per thread
void* studentR(void* input)
{
	int* k = (int*)input;
	struct stud s = stuA[*k];
	printf("starting thread for student %d with Cal %f.\n", s.sID, s.sCal);
	free(input);
}
//###########FILE CHANGE ./main_folder/George Paul_305816_assignsubmission_file_/2021121006/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "all.h"
// global arrays
struct course couA[MAX_SIZE] = {};
struct stud stuA[MAX_SIZE] = {};
struct lab labA[MAX_SIZE] = {};

int main()
{
	// freopen("/home/george/stuff/OS5/input.txt", "r", stdin);	// DEBUG - take input from a file
	// input sizes
	int studentsNo, labsNo, coursesNo;
	scanf("%d %d %d", &studentsNo, &labsNo, &coursesNo);

	// input courses
	for(int i = 0; i<coursesNo; i++)
	{
		struct course newCourse;
		newCourse.cID = i;
		scanf("%s", newCourse.cName);
		scanf("%f", &newCourse.cInterest);
		scanf("%d", &newCourse.cSlots);
		scanf("%d", &newCourse.cLabsNo);
		for(int j = 0; j<newCourse.cLabsNo; j++)
		{
			scanf("%d", &newCourse.cLabs[j]);
		}
		couA[i] = newCourse;
	}

	// input students
	for(int i = 0; i<studentsNo; i++)
	{
		struct stud newStud;
		newStud.sID = i;
		scanf("%f", &newStud.sCal);
		scanf("%d", &newStud.sP1);
		scanf("%d", &newStud.sP2);
		scanf("%d", &newStud.sP3);
		scanf("%d", &newStud.sTime);
		stuA[i] = newStud;
	}

	// input labs
	for(int i = 0; i<labsNo; i++)
	{
		struct lab newLab;
		newLab.lID = i;
		scanf("%s", newLab.lName);
		scanf("%d", &newLab.lTasNo);
		scanf("%d", &newLab.lMaxTaShips);
		labA[i] = newLab;
	}

	// DEBUG - print all courses, students and labs
	// for(int i = 0; i<coursesNo; i++)
	// {
	// 	struct course newCourse = couA[i];
	// 	printf("cID: %d\n", newCourse.cID);
	// 	printf("cName: %s,\t", newCourse.cName);
	// 	printf("cInterest: %f,\t", newCourse.cInterest);
	// 	printf("cSlots: %d,\t", newCourse.cSlots);
	// 	printf("cLabsNo: %d,\ncLabs: ", newCourse.cLabsNo);
	// 	for(int j = 0; j<newCourse.cLabsNo; j++)
	// 	{
	// 		printf("%d, ", newCourse.cLabs[j]);
	// 	}
	// 	printf("\n---\n");
	// }
	// printf("\n----------\n----------\n");
	// for(int i = 0; i<studentsNo; i++)
	// {
	// 	struct stud newStud = stuA[i];
	// 	printf("sID: %d, ",newStud.sID);
	// 	printf("sCal: %f, ", newStud.sCal);
	// 	printf("sP1: %d, ", newStud.sP1);
	// 	printf("sP2: %d,", newStud.sP2);
	// 	printf("sP3: %d,", newStud.sP3);
	// 	printf("sTime: %d\n", newStud.sTime);
	// }
	// printf("\n----------\n----------\n");
	// for(int i = 0; i<labsNo; i++)
	// {
	// 	struct lab newLab = labA[i];
	// 	printf("lID: %d, ", newLab.lID);
	// 	printf("lName: %s, ", newLab.lName);
	// 	printf("lTasNo: %d, ", newLab.lTasNo);
	// 	printf("lMaxTaShips: %d\n", newLab.lMaxTaShips);
	// }
	// printf("\n----------\n----------\n");

	// -------create threads
	pthread_t ct[MAX_SIZE];			//courses
	for(int i = 0; i<coursesNo; i++)
	{
		int* arg = malloc(sizeof(int));	*arg = i;
		if(pthread_create(&ct[i], NULL, courseR, arg) != 0) perror("Couldn't create course thread.");
		// arg freed in courseR
	}
	pthread_t st[MAX_SIZE];			// students
	for(int i = 0; i<studentsNo; i++)
	{
		int* arg = malloc(sizeof(int));	*arg = i;
		if(pthread_create(&st[i], NULL, studentR, arg) != 0) perror("Couldn't create student thread.");
		// arg freed in studentR
	}
	pthread_t lt[MAX_SIZE];			// labs
	for(int i = 0; i<labsNo; i++)
	{
		int* arg = malloc(sizeof(int));	*arg = i;
		if(pthread_create(&lt[i], NULL, labR, arg) != 0) perror("Couldn't create lab thread.");
		// arg freed in labR
	}


	// -------join threads
	for(int i = 0; i<coursesNo; i++)
	{
		if(pthread_join(ct[i], NULL) != 0) perror("Couldn't join course thread.");
		else printf("course ID %d thread joined\n", i);
	}
	for(int i = 0; i<studentsNo; i++)
	{
		if(pthread_join(st[i], NULL) != 0) perror("Couldn't join student thread.");
		else printf("student ID %d thread joined\n", i);
	}
	for(int i = 0; i<labsNo; i++)
	{
		if(pthread_join(lt[i], NULL) != 0) perror("Couldn't join lab thread.");
		else printf("lab ID %d thread joined\n", i);
	}
}
//###########FILE CHANGE ./main_folder/George Paul_305816_assignsubmission_file_/2021121006/q1/course.c ####################//

#include <stdio.h>
#include <stdlib.h>

#include "all.h"

// course routine - one per thread
void* courseR(void* input)
{
	int* k = (int*)input;
	struct course c = couA[*k];
	printf("starting thread for course %d with name %s.\n", c.cID, c.cName);
	free(input);
}
//###########FILE CHANGE ./main_folder/George Paul_305816_assignsubmission_file_/2021121006/q1/lab.c ####################//

#include <stdio.h>
#include <stdlib.h>

#include "all.h"

// lab routine - one per thread
void* labR(void* input)
{
	int* k = (int*)input;
	struct lab l = labA[*k];
	printf("starting thread for lab %d with Tas %d.\n", l.lID, l.lTasNo);
	free(input);
}