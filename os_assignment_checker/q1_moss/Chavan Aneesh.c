
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q1/main.c ####################//

#include "defs.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

Student** all_students;
Lab** all_labs;
Course** all_courses;

int total_students, total_labs, total_courses;

// pthread_mutex_t mutex_all_init = PTHREAD_MUTEX_INITIALIZER;
// int all_init = 0;

int main(void){
    scanf("%d %d %d", &total_students, &total_labs, &total_courses);
    pthread_t student_tids[total_students], lab_tids[total_labs], course_tids[total_courses];

    // array of all entities
    all_students = (Student**) malloc(sizeof(Student*) * total_students);
    all_labs = (Lab**) malloc(sizeof(Lab*) * total_labs);
    all_courses = (Course**) malloc(sizeof(Course*) * total_courses);


    // parse courses
    for(int i = 0; i < total_courses; i++){
        char name_buf[100];
        float interest;
        int max_slots, num_labs;
        scanf("%s %f %d %d", name_buf, &interest, &max_slots, &num_labs);
        int lab_ids[num_labs];
        for(int j = 0; j < num_labs; j++){
            scanf("%d", &(lab_ids[j]));
        }

        all_courses[i] = init_course(i, name_buf, interest, max_slots, num_labs, lab_ids);

        name_buf[0] = '\0';
    }

    // parse students
    for(int i  = 0; i < total_students; i++){
        float calibre;
        int c1, c2, c3, delay;
        scanf("%f %d %d %d %d", &calibre, &c1, &c2, &c3, &delay);

        all_students[i] = init_student(i, delay, c1, c2, c3, calibre);
    }

    // parse labs
    for(int i = 0; i < total_labs; i++){
        char name_buf[100];
        int num_TAs, limit;

        scanf("%s %d %d", name_buf, &num_TAs, &limit);
        
        all_labs[i] = init_lab(i, name_buf, num_TAs, limit);

        for(int j = 0; j < num_TAs; j++){
            all_labs[i]->TA_list[j] = init_TA(j);
        }

        name_buf[0] = '\0';
    }

//--  create threads  ------------------------------------------------------------------------------------------------------------------------------------------------//

    // labs
    for(int i = 0; i < total_labs; i++){
        printf("Creating lab %d %s\n", all_labs[i]->id, all_labs[i]->name);
        pthread_create(&(lab_tids[all_labs[i]->id]), NULL, lab_thread, (void*) &(all_labs[i]->id));
    }

    // courses
    for(int i = 0; i < total_courses; i++){
        printf("Creating course %d %s\n", all_courses[i]->id, all_courses[i]->name);
        pthread_create(&(course_tids[all_courses[i]->id]), NULL, course_thread, (void*) &(all_courses[i]->id));
    }

    // students
    for(int i = 0; i < total_students; i++){
        printf("Creating student %d\n", all_students[i]->id);
        pthread_create(&(student_tids[all_students[i]->id]), NULL, student_thread, (void*) &(all_students[i]->id));
    }


    // print_all(total_courses, total_students, total_labs, all_students, all_labs, all_courses);

//--  clean up  ------------------------------------------------------------------------------------------------------------------------------------------------//

    for(int i = 0; i < total_students; i++){
        pthread_join(student_tids[i], NULL);
    }    

    printf("Simulation complete, all students have exited\n");

    for(int i = 0; i < total_students; i++)
        clear_student(all_students[i]);

    for(int i = 0; i < total_labs; i++)
        clear_lab(all_labs[i]);

    for(int i = 0; i < total_courses; i++)
        clear_course(all_courses[i]);

}

// debugging
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q1/threads.c ####################//

#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>

extern Student** all_students;
extern Lab** all_labs;
extern Course** all_courses;

extern int total_students;

extern pthread_mutex_t mutex_all_init;
extern int all_init;

void* student_thread(void* vp_s_id){
    int s_id = *(int*) vp_s_id;
    Student *s = all_students[s_id];

    sleep(s->delay);
    // student now enters registration
    fprintf(stdout, STUDENT_COLOR"Student %d has filled in preferences for course registration\n"NORMAL_COLOR,
            s->id);

    for(int i = 0; i < 3; i++){
        *(s->p_cnt_pref) = i;

        Course* c = all_courses[s->courses[i]];

        // is the tute still available
        if(*(c->p_available) == 1){
            fprintf(stdout, STUDENT_COLOR"Student %d has been allocated a seat in course %d\n"NORMAL_COLOR,
                    s->id, c->id);
            sem_post(&c->tute_starting);

            *s->p_enrolled = 1;

            pthread_mutex_lock(&c->mutex_cnt_waiting);
            (*c->p_cnt_waiting)++;
            pthread_mutex_unlock(&c->mutex_cnt_waiting);
            
            sem_wait(&c->waiting);

            // student has completed the tute
            *s->p_enrolled = 1;
            if (*(c->p_available)){
                float prob = s->calibre * c->interest;
                if (prob < CUTOFF){
                    // student withdraws
                    fprintf(stdout, STUDENT_COLOR"Student %d has withdrawn from %s (course %d)\n"NORMAL_COLOR,
                            s->id, c->name, c->id);
                }
                else{
                    // student accepts this course and exits the simulation
                    fprintf(stdout, STUDENT_SUCCESS_COLOR"Student %d has selected %s (course %d) permanently\n"NORMAL_COLOR,
                            s->id, c->name, c->id);
                    
                    return NULL;
                }
            }
        }

        if (i != 2){
            fprintf(stdout, STUDENT_COLOR"Student %d has changed current preference from %s (id %d) to %s (id %d)\n"NORMAL_COLOR,
                    s->id, c->name, c->id, all_courses[s->courses[i+1]]->name, all_courses[s->courses[i+1]]->id);
        }

        sleep(STUDENT_REGISTER_DELAY);
    }

    // student hasnt chosen any of the three courses
    fprintf(stdout, STUDENT_SUCCESS_COLOR"Student %d couldn't get any of their preferred courses\n"NORMAL_COLOR,
            s->id);
    return NULL;    
}


void* course_thread(void* vp_c_id){    
    int c_id = *(int*) vp_c_id;
    Course *c = all_courses[c_id];

    printf(COURSE_COLOR"Course %d %s started\n"NORMAL_COLOR, c_id, c->name);

    // loop represents the cycle of:    find TA
    //                                  wait for students
    //                                  run tute
    //                                  return TA

    while(1){
        // Loop to select a TA
        printf(COURSE_COLOR"Course %d %s looking for a TA\n"NORMAL_COLOR, c_id, c->name);
        sleep(COURSE_LOOK_DELAY);
        while(c->TA_present == 0){
            for(int l_id = 0; l_id < c->num_labs; l_id++){
                Lab *l = all_labs[c->lab_list[l_id]];

                // only checks labs with available TAs
                pthread_mutex_lock(&l->mutex_any_free);
                if(*l->p_any_free){
                    pthread_mutex_unlock(&l->mutex_any_free);

                    pthread_mutex_lock(&l->mutex_num_waiting);
                    (*l->p_num_waiting)++;
                    sem_post(&l->course_waiting);
                    pthread_mutex_unlock(&l->mutex_num_waiting);

                    sem_wait(&l->TA_ready);


                    // double check, in case the lab ran out before this TA request
                    pthread_mutex_lock(&l->mutex_any_free);
                    if (*l->p_any_free){
                        pthread_mutex_unlock(&l->mutex_any_free);

                        c->cnt_TA_id = *(l->p_selected_TA);
                        c->cnt_lab_id = l->id;
                        c->TA_present = 1;
                        sem_post(&l->TA_ack);
                        break;                                  // TA found, exiting this loop
                    }
                    else{
                        pthread_mutex_unlock(&l->mutex_any_free);
                    }
                }
                else{
                    pthread_mutex_unlock(&l->mutex_any_free);
                }
            }

            if(c->TA_present == 1)
                break;

            // all labs checked once, checking
            int any_left = 0;
            for(int l_id = 0; l_id < c->num_labs; l_id++){
                Lab *l = all_labs[c->lab_list[l_id]];

                pthread_mutex_lock(&l->mutex_any_free);
                if(*l->p_any_free == 1){
                    pthread_mutex_unlock(&l->mutex_any_free);
                    any_left = 1;
                    break;
                }
                else{
                    pthread_mutex_unlock(&l->mutex_any_free);
                }
            }

            if(any_left == 0){
                fprintf(stdout, COURSE_COLOR"Course %d has no TAs and is no longer offered\n"NORMAL_COLOR, 
                        c->id);
                *(c->p_available) = 0;
                while(1){
                    sem_wait(&c->tute_starting);
                    sem_post(&c->waiting);
                }
            }
        }

        fprintf(stdout, TA_COLOR"Ta %d from lab %d has been allocated to course %d for TA ship no. %d\n"NORMAL_COLOR, 
                c->cnt_TA_id, c->cnt_lab_id, c->id, *(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->p_TAships));
        // return NULL;
        if(c->cnt_lab_id == -1 || c->cnt_TA_id == -1)
            assert(0);

        // Loop to set up a tutorial
        srand(time(NULL));
        c->D = (rand() % c->course_max_slot) + 1;

        fprintf(stdout, COURSE_COLOR"Course %d has been allocated %d seats\n"NORMAL_COLOR, 
                c->id, c->D);

        int final_tute_size;

        assert(c->cnt_TA_id != -1 && c->cnt_lab_id != -1);

        // loop to check if there are enough students to begin
        while(1){
            int in_queue;
            pthread_mutex_lock(&c->mutex_cnt_waiting);
            in_queue = *c->p_cnt_waiting;
            pthread_mutex_unlock(&c->mutex_cnt_waiting);

            // fprintf(stdout, COURSE_COLOR"Course %d's tutorial has started the search\n"NORMAL_COLOR, c->id);

            if(in_queue >= c->D){
                final_tute_size = c->D;
                break;
            }
            else{
                // fprintf(stdout, COURSE_COLOR"Course %d's tutorial is checking for students\n"NORMAL_COLOR, c->id);
                // check if any students have this course but havent registered
                int any_not_enrolled = 0;

                // check if there is at least one student who has yet to enroll
                for (int i = 0; i < total_students; i++){
                    Student* s = all_students[i];
                    if (s->courses[*s->p_cnt_pref] == c->id  && *(s->p_enrolled) == 0){
                        any_not_enrolled = 1;
                        // fprintf(stdout, COURSE_COLOR"Course %d's is waiting on a student\n"NORMAL_COLOR, c->id);
                        break;
                    }
                }

                if (any_not_enrolled == 0){
                    pthread_mutex_lock(&c->mutex_cnt_waiting);
                    final_tute_size = *c->p_cnt_waiting;
                    pthread_mutex_unlock(&c->mutex_cnt_waiting);
                    break;
                }
            }
        }

        fprintf(stdout, COURSE_COLOR"Course %d's tutorial has started with %d seats\n"NORMAL_COLOR, 
                c->id, final_tute_size);

        // start the tute
        sleep(TUTE_DELAY);
        for (int i = 0; i < final_tute_size; i++){
            sem_wait(&c->tute_starting);
            sem_post(&c->waiting);
        }

        pthread_mutex_lock(&c->mutex_cnt_waiting);
        *c->p_cnt_waiting -= final_tute_size;
        pthread_mutex_unlock(&c->mutex_cnt_waiting);

        pthread_mutex_lock(&(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->mutex));
        *(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->p_free) = 1;
        pthread_mutex_unlock(&(all_labs[c->cnt_lab_id]->TA_list[c->cnt_TA_id]->mutex));
    
        fprintf(stdout, TA_COLOR"Ta %d from lab %d left course %d\n"NORMAL_COLOR, 
                c->cnt_TA_id ,c->cnt_lab_id, c->id);

        c->cnt_TA_id = -1;
        c->cnt_lab_id = -1;
        c->TA_present = 0;
    }
}

void* lab_thread(void* vp_l_id){
    int l_id = *(int*) vp_l_id;
    Lab *l = all_labs[l_id];
    
    // loop represents normal function
    while(1){
        // unblocked when a course is waiting, first checks for a free TA, assigns it to the course
        sem_wait(&l->course_waiting);
        for(int ta_found = 0; ta_found != 1;){      // stores if a ta was successfully found or not

            for(int ta_id = 0; ta_id < l->num_TAs; ta_id++){
                TA *cnt_TA = l->TA_list[ta_id];

                pthread_mutex_lock(&cnt_TA->mutex);

                if(*(cnt_TA->p_free) && (*(cnt_TA->p_TAships) < l->cnt_TA_limit) && *(cnt_TA->p_TAships) < l->TA_limit){
                    *(l->p_selected_TA) = cnt_TA->id;
                    *(cnt_TA->p_free) = 0;
                    (*(cnt_TA->p_TAships))++;
                    pthread_mutex_unlock(&cnt_TA->mutex);    

                    sem_post(&l->TA_ready);
                    sem_wait(&l->TA_ack);

                    pthread_mutex_lock(&l->mutex_num_waiting);
                    (*l->p_num_waiting)--;
                    pthread_mutex_unlock(&l->mutex_num_waiting);

                    *(l->p_selected_TA) = -1;
                    ta_found = 1;
                    break;
                }
                // ta is at ta limit, check if all others are at ta limit
                else if(*(cnt_TA->p_free) && *(cnt_TA->p_TAships) >= l->cnt_TA_limit && *(cnt_TA->p_TAships) < l->TA_limit){
                    int all_at_max = 1;
                    for (int i = 0; i < l->num_TAs; i++){
                        if (i != ta_id){
                            TA *t = l->TA_list[i];
                            // pthread_mutex_lock(&t->mutex);
                            if (*(t->p_TAships) < l->cnt_TA_limit){
                                // pthread_mutex_unlock(&t->mutex);
                                all_at_max = 0;
                                break;
                            }
                            // else
                                // pthread_mutex_unlock(&t->mutex);
                        }
                    }

                    if (all_at_max){
                        l->cnt_TA_limit++;

                        *(l->p_selected_TA) = cnt_TA->id;
                        *(cnt_TA->p_free) = 0;
                        (*(cnt_TA->p_TAships))++;
                        pthread_mutex_unlock(&cnt_TA->mutex);    

                        sem_post(&l->TA_ready);
                        sem_wait(&l->TA_ack);

                        pthread_mutex_lock(&l->mutex_num_waiting);
                        (*l->p_num_waiting)--;
                        pthread_mutex_unlock(&l->mutex_num_waiting);

                        *(l->p_selected_TA) = -1;
                        ta_found = 1;
                        break;
                    }
                    else{
                        pthread_mutex_unlock(&cnt_TA->mutex);
                    }
                }
                else{
                    pthread_mutex_unlock(&cnt_TA->mutex);
                }
                
            }

            // check if all tas are at the lab limit, if they are the lab shuts down
            int any_left = 0;
            for(int ta_id = 0; ta_id < l->num_TAs; ta_id++){
                TA *cnt_TA = l->TA_list[ta_id];
                if(*(cnt_TA->p_TAships) < l->TA_limit){
                    any_left = 1;
                    break;
                }
            }

            if(any_left == 0){
                fprintf(stdout, LAB_COLOR"Lab %d no longer has students available for TA ship\n"NORMAL_COLOR, l_id);
                
                pthread_mutex_lock(&l->mutex_any_free);
                *(l->p_any_free) = 0;
                pthread_mutex_unlock(&l->mutex_any_free);

                pthread_mutex_lock(&l->mutex_num_waiting);
                while(*(l->p_num_waiting) > 0){
                    sem_post(&l->TA_ready);
                }
                pthread_mutex_unlock(&l->mutex_num_waiting);
                
                return NULL;
            }

            // printf("lab %d looping again\n", l->id);
        }
    }
}
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q1/defs.h ####################//

#ifndef __DEFS__
#define __DEFS__

#include <pthread.h>
#include <semaphore.h>

#define STUDENT_COLOR               "\x1b[31m"
#define STUDENT_SUCCESS_COLOR       "\x1b[34m"
#define TA_COLOR                    "\x1b[32m"
#define COURSE_COLOR                "\x1b[33m"
#define LAB_COLOR                   "\x1b[35m"
#define NORMAL_COLOR                "\x1b[0m"

#define CUTOFF                      0.55f
#define TUTE_DELAY 5
#define COURSE_LOOK_DELAY 1
#define STUDENT_REGISTER_DELAY 1


void start_lab(int lab_id);
void start_student(int student_id);
void start_course(int course_id);


// structs

struct s_TA{
    int id;                     // ta id, assigned sequentially
    int* p_TAships;                // current no. of ta ships, init to 0
    int* p_free;                   // is this ta currently free
    pthread_mutex_t mutex;
};
typedef struct s_TA TA;

struct s_lab{
    int id;                     // global lab id
    char* name;                 
    
    int num_TAs;                // no. of assigned tas
    int* p_any_free;               // are any tas free
    pthread_mutex_t mutex_any_free; 

    int* p_num_waiting;             // how many courses are waiting for a ta
    pthread_mutex_t mutex_num_waiting;
    
    TA** TA_list;                // array of all assigned tas
    int TA_limit;               // limit on no of TAships

    int cnt_TA_limit;           // TAs need to wait until all TAs are at this limit

    sem_t course_waiting;       // binary sem to store how many courses are waiting for a ta
    int* p_selected_TA;            // id of the selected ta if any, otherwise set to -1
    sem_t TA_ready;             // binary sem to signal if a ta is selected
    sem_t TA_ack;               // binary sem, has the course copied the ta id
};
typedef struct s_lab Lab;

struct s_course{
    int id;
    char* name;
    float interest;
    int course_max_slot;

    int num_labs;
    int* lab_list;              // list of the ids of associated labs

    int D;                      // size of tute
    int TA_present;             // is a ta ready to take a tute
    int* p_available;              // is the course still available to students

    int cnt_TA_id;
    int cnt_lab_id;

    sem_t waiting;
    sem_t tute_starting;

    int* p_cnt_waiting;
    pthread_mutex_t mutex_cnt_waiting;
};
typedef struct s_course Course;

struct s_student{
    int id;
    int* p_cnt_pref;               // index of current preference
    int courses[3];             // array of course ids 
    float calibre;

    int delay;                  // delay before the student enters
    int* p_enrolled;               // is the student waiting for a tute
    int* p_registered;             // is the student looking for a course
};
typedef struct s_student Student;

// initfns

TA*         init_TA(int id);
Lab*        init_lab(int id, char* name, int num_TAs, int TA_limit);
Course*     init_course(int id, char* name, float interest, int max_slots, int num_labs, int* lab_list);
Student*    init_student(int id, int delay, int course1, int course2, int course3, float calibre);
void        clear_TA(TA* t);
void        clear_lab(Lab* l);
void        clear_course(Course* c);
void        clear_student(Student* s);

void        print_TA(TA* t);
void        print_Lab(Lab* l);
void        print_Course(Course* c);
void        print_Student(Student* s);
void        print_all(int n_c, int n_s, int n_l ,Student** all_students, Lab** all_labs, Course** all_courses);

// threads.c

void* student_thread(void* vp_s_id);
void* lab_thread(void* vp_l_id);
void* course_thread(void* vp_c_id);

#endif

/*
studno      labno       courseno

courses: name  float  max  lab_n  lab1 lab2 ...

student: calibre   c1  c2  c3 delay

lab:    name    num_TAs     limit   
*/


//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q1/initfns.c ####################//

#include "defs.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

TA* init_TA(int id){
    TA* t = (TA*) malloc(sizeof(TA)); 
    if(t == NULL){
        perror("init_TA");
        return NULL;
    }

    t->id = id;
    t->p_TAships = (int*) malloc(sizeof(int));
    t->p_free = (int*) malloc(sizeof(int));
    *(t->p_TAships) = 0;
    *(t->p_free) = 1;
    if(pthread_mutex_init(&(t->mutex), NULL) != 0){
        perror("init_TA mutex");
        free(t);
        return NULL;
    }

    return t;
}

void clear_TA(TA* t){
    if(pthread_mutex_destroy(&(t->mutex)) != 0)
        perror("clear_TA");
    free(t->p_TAships);
    free(t->p_free);
    free(t);

    pthread_mutex_destroy(&t->mutex);

    return;
}

void print_TA(TA* t){
    printf("id: %d\ttaships: %d\tbusy: %d\n", t->id, *(t->p_TAships), *(t->p_free));
}

Lab* init_lab(int id, char* name, int num_TAs, int TA_limit){
    Lab* l = (Lab*) malloc(sizeof(Lab)); 
    if(l == NULL){
        perror("init_lab");
        return NULL;
    }

    l->id = id;
    l->name = (char*) malloc(strlen(name) + 1);
    strcpy(l->name, name);
    l->num_TAs = num_TAs;
    l->p_any_free = (int*) malloc(sizeof(int));
    *l->p_any_free = 1;
    l->p_num_waiting = (int*) malloc(sizeof(int));
    *l->p_num_waiting = 0;
    l->cnt_TA_limit = 0;
    l->TA_limit = TA_limit;
    l->p_selected_TA = (int*) malloc(sizeof(int));
    *(l->p_selected_TA) = -1;

    l->TA_list = (TA**) malloc(sizeof(TA*) * num_TAs);
    if (l->TA_list == NULL){
        perror("init_lab");
        return NULL;
    }
    for (int i = 0; i < num_TAs; i++){
        l->TA_list[i] = init_TA(i);
    }

    int a = sem_init(&l->course_waiting, 0, 0);
    int b = sem_init(&l->TA_ready, 0, 0);
    int c = sem_init(&l->TA_ack, 0, 0);    

    if(a||b||c != 0){
        perror("sem_init");

        free(l->name);

        for(int i = 0; i < num_TAs; i++){
            clear_TA(l->TA_list[i]);
        }
        free(l->TA_list);

        sem_destroy(&(l->course_waiting));
        sem_destroy(&(l->TA_ready));
        sem_destroy(&(l->TA_ack));

        return NULL;
    }

    pthread_mutex_init(&l->mutex_any_free, NULL);
    pthread_mutex_init(&l->mutex_num_waiting, NULL);


    return l;
}

void clear_lab(Lab* l){
    free(l->name);
    free(l->p_selected_TA);
    free(l->p_any_free);
    free(l->p_num_waiting);

    for(int i = 0; i < l->num_TAs; i++){
        clear_TA(l->TA_list[i]);
    }
    free(l->TA_list);

    int a = sem_destroy(&(l->course_waiting));
    int b = sem_destroy(&(l->TA_ready));
    int c = sem_destroy(&(l->TA_ack));

    if (a||b||c != 0)
        perror("clear_lab");

    pthread_mutex_destroy(&l->mutex_any_free);
    pthread_mutex_destroy(&l->mutex_num_waiting);


    free(l);
}

void print_Lab(Lab* l){
    printf("id: %d\n%s\nnum tas: %d\ncnt_limit: %d\n", l->id, l->name, l->num_TAs, l->cnt_TA_limit);
    for(int i = 0; i < l->num_TAs; i++){
        print_TA(l->TA_list[i]);
    }
    printf("ta limit: %d\nselected ta: %d\n", l->TA_limit, *(l->p_selected_TA));
}

Course* init_course(int id, char* name, float interest, int max_slots, int num_labs, int* lab_list){
    Course* c = (Course*) malloc(sizeof(Course));
    if(c == NULL){
        perror("init_course");
        return NULL;
    }

    c->id = id;
    c->name = (char*) malloc(strlen(name) + 1);
    strcpy(c->name, name);
    c->interest = interest;
    c->course_max_slot = max_slots;
    c->num_labs = num_labs;
    
    c->lab_list = (int*) malloc(sizeof(int) * num_labs);
    for(int i = 0; i < num_labs; i++){
        c->lab_list[i] = lab_list[i];
    }

    c->D = 1000;
    c->TA_present = 0;
    c->p_available = (int*) malloc(sizeof(int));
    *(c->p_available) = 1;
    c->cnt_TA_id = -2;
    c->cnt_lab_id = -2;
    c->p_cnt_waiting = (int*) malloc(sizeof(int));
    *(c->p_cnt_waiting) = 0;

    if(sem_init(&(c->waiting), 0, 0) != 0){
        perror("init_course");
        free(c->name);
        free(c->lab_list);
    }

    if(sem_init(&(c->tute_starting), 0, 0) != 0){
        perror("init_course");
        free(c->name);
        free(c->lab_list);
    }

    if(pthread_mutex_init(&(c->mutex_cnt_waiting), NULL) != 0){
        perror("init_course mutex");
        free(c->name);
        free(c->lab_list);
        return NULL;
    }

    return c;
}

void clear_course(Course* c){
    free(c->name);
    free(c->lab_list);
    free(c->p_available);
    free(c->p_cnt_waiting);

    if(sem_destroy(&(c->waiting))){
        perror("clear_course");
        return;
    }

    pthread_mutex_destroy(&c->mutex_cnt_waiting);

    free(c);

    return;
}

void print_Course(Course* c){
    printf("id: %d\n%s\ninterest: %f\nmax_slots: %d\nnum_labs %d\nlab ids:\n", c->id, c->name, c->interest, c->course_max_slot, c->num_labs);
    for(int i = 0; i < c->num_labs; i++){
        printf("%d\n", c->lab_list[i]);
    }
    printf("D: %d\nta_present: %d\navailable: %d\n\n", c->D, c->TA_present, *(c->p_available));
}

Student* init_student(int id, int delay, int course1, int course2, int course3, float calibre){
    Student* s = (Student*) malloc(sizeof(Student));

    s->id = id;
    s->p_cnt_pref = (int*) malloc(sizeof(int));
    *(s->p_cnt_pref) = 0;
    s->courses[0] = course1;
    s->courses[1] = course2;
    s->courses[2] = course3;
    s->calibre = calibre;

    s->delay = delay;
    s->p_enrolled = (int*) malloc(sizeof(int));
    s->p_registered = (int*) malloc(sizeof(int));

    *(s->p_enrolled) = 0;
    *(s->p_registered) = 0;

    return s;
}

void clear_student(Student* s){
    free(s->p_cnt_pref);
    free(s->p_enrolled);
    free(s->p_registered);
    free(s);

    return;
}

void print_Student(Student* s){
    printf("id: %d\ncnt_pref: %d\ncourse1: %d\ncourse2: %d\ncourse3: %d\n", s->id, *(s->p_cnt_pref), s->courses[0], s->courses[1], s->courses[2]);
    printf("calibre: %f\ndelay: %d\nenrolled: %d\nregistered: %d\n\n", s->calibre, s->delay, *(s->p_enrolled), *(s->p_registered));
}

void print_all(int n_c, int n_s, int n_l ,Student** all_students, Lab** all_labs, Course** all_courses){
    for(int i = 0; i < n_c; i++){
        print_Course(all_courses[i]);
    }

    printf("\n");

    for(int i = 0; i < n_s; i++){
        print_Student(all_students[i]);
    }

    printf("\n");

    for(int i = 0; i < n_l; i++){
        print_Lab(all_labs[i]);
    }
}