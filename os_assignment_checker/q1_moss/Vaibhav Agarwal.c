
//###########FILE CHANGE ./main_folder/Vaibhav Agarwal_305934_assignsubmission_file_/q1/q1.c ####################//

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>

// color code

void yellow(){
    printf("\033[1;33m");
}
void blue(){
    printf("\033[1;34m");
}

void reset(){
    printf("\033[0m");
}

void green(){
    printf("\033[1;32m");
}
void red(){
    printf("\033[1;31m");
}
void purple(){
    printf("\033[1;35m");
}
int rng(int a, int b){ // [a, b]
    int dif = b - a + 1;
    int rnd = rand() % dif;
    return (a + rnd);
}
typedef struct ta{
    int id;
    int course_id;                    // stores the id of the course it is taking 
    int no_of_taship_completed;       // stores how many taship it has completed
    int taship_ongoing;               // 0-> ta is idle // 1-> he is taking tut for course[course_id] 
}ta;
// course
typedef struct course{
    char name[256];
    float interest;
    int max_slots;
    int no_of_labs;       
    int lab_list[100];    // assuming course do not take tas from more than 10 lasb
    int course_removed;   // if 1 that means course has been removed from the portal
    int course_id;
    // int no_of_student_interested;

}course;
// pthread_mutex_t mutexCourse[50];
// labs -> max 50 ta's are allowed
typedef struct lab{
    int is_available;        // if 1 -> means ta some ta is there who has not completed its quota else 0
    char name[256];
    int taship_limit;
    int no_of_ta;            // stores how many ta lab has  // ids are 0 indexed
    ta tas[51];              // stores the info about the tas in a lab
    int no_of_ta_active;           // stores how many tas are there who has not completed their quota
    int lab_id;
}lab;
lab Labs[50];
pthread_t labThread[50];
pthread_mutex_t mutexTas[50][50];
// pthread_cond_t condLabs[50];
// student
typedef struct student{
    int id;   
    float calbre;
    int entry_time;
    int course_pref[3]; 
    int has_finalized_course;  // if 0 , it has not finalized else yes , it has finalalized
    int is_available;  // will tell if student has filled its choices or not?
    int current_pref_no;  // tell which priority is it at this moment
    int in_a_tut;         // will tell if a student is in a tut or not?
    int course;           // tells which course he is planning to take // -1 means no signal was not sent from any course 
}student;

int no_of_students , no_of_courses , no_of_labs;

student Students[500];
pthread_t studentThread[500];
pthread_mutex_t mutexStudent[500];
pthread_cond_t condStudent[500];


course Courses[50];
pthread_t courseThread[50];







// function definition
void* handleStudent(void* );
void* handleCourse(void* );

// functions to print in diff color

// #endif 
// rng(a,b) gives a random integer between a and b



void *handleCourse(void *args){
    red();
    course *course1 = (course *)args;

    // will store the id's of the student selected for a tutorial

    
    printf("Course %s has been initialized\n", course1->name);
    reset();

    int iter_count = 0;
    int students_selected[100];
    int cnt = 0;
    int flag = 0;
    cnt++;
    while (cnt){
    check_lab_removal_start:
        iter_count = 0; // check for lab removal after every 10 iterations
        flag = 0;
        cnt++;
        // check if course is available
        for (int i = 0; i < course1->no_of_labs; i++){
            lab *lab1 = &Labs[course1->lab_list[i]];
            int max_taship = lab1->taship_limit;
            int cntttt = lab1->no_of_ta , tmppp = 0;
            while(cntttt--)
            {
                if (lab1->tas[tmppp].no_of_taship_completed < lab1->taship_limit)
                {
                    flag = 1;
                    break;
                }
                tmppp++;
            }

            if (flag == 1)
                break;
        }
        if(flag == 0){
            red();
            printf("Course %s has been removed from the portal\n", course1->name);
        }

        if (flag < 1){
            // course as been removed
            course1->course_removed = 1;
            reset();
            int cntt = 0;
            int num = no_of_students;
            while(num--){
                // pthread_mutex_lock(&mutexStudent[i]);
                if (Students[cnt].course_pref[Students[cnt].current_pref_no] == course1->course_id){
                    Students[cnt].course = Students[cnt].course_pref[Students[cnt].current_pref_no];
                    pthread_cond_signal(&condStudent[Students[cnt].id]);
                }
                cnt++;
            }
            return NULL;
        }

        // find a ta for this course from the ta list
        int ta_no = -1;
        int lab_no = -1;
        int course_ta_found = 0;
        ta *ta_selected;

        while (course_ta_found == 0){
            iter_count++;
            int max_limit_for_iter = 10;
            if (iter_count > max_limit_for_iter)
                goto check_lab_removal_start;
            

            // iterate through the list of tas
            for (int i = 0; i < course1->no_of_labs; i++){
                red();
                lab *lab1 = &Labs[course1->lab_list[i]];
                    // printf("Start data\n\n");
                for (int j = 0; j < lab1->no_of_ta; j++){
                        ta *ta1 = &lab1->tas[j];
                        if ((ta1->no_of_taship_completed < lab1->taship_limit)){
                            if((ta1->taship_ongoing == 0) ){
                            // printf("Course %s lab_name: %s ta_id: %d before\n", course1->name, lab1->name, ta1->id);

                            if (pthread_mutex_trylock(&mutexTas[lab1->lab_id][j]) != 0)
                                continue;
                            course_ta_found = 1;
                            ta1->taship_ongoing = 1;
                            ta1->no_of_taship_completed = ta1->no_of_taship_completed + 1;
                            int for_id = course1->course_id;
                            ta1->course_id = for_id;
                            ta_selected = ta1;
                            ta_no = ta1->id;
                            lab_no = lab1->lab_id;
                            break;
                            }
                        }
                    }
                red();
                if (course_ta_found == 1)
                    break;
            }
        }

        if (course_ta_found == 1){
            // assign the ta to this course
            reset();
            green();
            printf("Course %s has been allocated TA %d from lab %s\n", course1->name, ta_no, Labs[lab_no].name);
            reset();
            // printf("Course %s lab_name: %s ta_id: %d after\n", course1->name, Labs[lab_no].name, ta_no);
            int printtt = 0;
            yellow();
            printf("TA %d from lab %s has been allocated to course %s for his %d taship\n", ta_no, Labs[lab_no].name, course1->name, ta_selected->no_of_taship_completed);
            reset();

            // allocate random number of seats between 1 and max_slot for this course
            int slots = rng(1, course1->max_slots);
            printtt++;
            if(printtt == 10000) break;
            green();
            printtt++;
            printf("Course %s has been allocated %d seats\n", course1->name, slots);
            reset();

            int num_student_selected = 0;

            while (num_student_selected == 0){

                // select students
                for (int i = 0; i < no_of_students; i++){

                    if ((Students[i].course_pref[Students[i].current_pref_no] == course1->course_id))
                    {
                        if((Students[i].in_a_tut == 0)){
                        green();
                        if(Students[i].is_available != -1){
                        pthread_mutex_lock(&mutexStudent[i]);
                        Students[i].in_a_tut = 1;
                        students_selected[num_student_selected] = i;
                        Students[i].course = course1->course_id;
                        num_student_selected++;

                        printf("Student %d has been allocated course %s\n", i, course1->name);
                        // sleep(1);
                        }
                        reset();
                        }
                    }

                    if (num_student_selected >= slots)
                        break;

                }

                if (num_student_selected == 0){
                    // make this thread go for sleep for one sec before checking again
                    sleep(num_student_selected + 1);
                }
            }

            // start the tutorial // go to sleep for 2 sec
            purple();
            printf("Tutorial has started for course %s with %d seats fulled out of %d\n", course1->name, num_student_selected, slots);
            sleep(2);

            // tutorial has ended
            purple();
            printf("ta %d from lab %s has completed tut for course %s\n", ta_no, Labs[lab_no].name, course1->name);
            reset();
            int lol = num_student_selected , coun = 0;
            // signal all the selected students . that tut has ended and they can update their choices
            while(lol--){
                pthread_mutex_unlock(&mutexStudent[students_selected[coun]]);
                pthread_cond_signal(&condStudent[students_selected[coun]]);
                coun++;
            }

            ta_selected->taship_ongoing = 0;

            // checking for ta removal
            int max_taship = Labs[lab_no].taship_limit;
            int lab_flag = 0;
            int cnttt = Labs[lab_no].no_of_ta;
            while (cnttt--){
                if (Labs[lab_no].tas[cnttt].no_of_taship_completed < max_taship)
                    lab_flag = 1;
                
            }

            if (Labs[lab_no].is_available != -1){
                green();
                if(lab_flag == 0){
                printf("Lab %s has been removed\n", Labs[lab_no].name);
                Labs[lab_no].is_available = -1;
                }
                reset();
            }

            pthread_mutex_unlock(&mutexTas[lab_no][ta_selected->id]);
        }
    }
}
void *handleStudent(void *args){
    blue();
    student *student1 = (student *)args;
    int id = student1->id;
    int t = student1->entry_time;
    sleep(t);

    printf("Students %d has filled its course preference\n", student1->id);
    reset();

    student1->is_available = 1;
    int z = 1;
    while (z){
        pthread_mutex_lock(&mutexStudent[id]);
start:
        student1->in_a_tut =0;
        student1->course = -1;
        // wait for a signal from course to indicate that a tutorial has been completed
        if(Courses[student1->course_pref[student1->current_pref_no]].course_removed == -1){
            pthread_cond_wait(&condStudent[id], &mutexStudent[id]);
        }

        student1->in_a_tut = 0;

        // check if signal was because course has been removed
        // course has been removed
        // move to the next course which is available
        red();
        if (Courses[student1->course].course_removed == 1){
            int pref = student1->current_pref_no;

            // check which one is possible
            if ((pref + 1 < 3) && (pref == 1) && (Courses[student1->course_pref[pref + 1]].course_removed == -1)){ // pref 1
                student1->current_pref_no++;
                pref = student1->current_pref_no;

                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 1, Courses[student1->course_pref[pref]].name, pref);
                // pthread_mutex_unlock(&mutexStudent[id]);
                // goto start;
            }
            else if ((pref + 2 < 3) && (Courses[student1->course_pref[pref + 2]].course_removed == -1)){ // pref 0 , -
                student1->current_pref_no ++;
                student1->current_pref_no ++;
                pref = student1->current_pref_no;

                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 2, Courses[student1->course_pref[pref]].name, pref);
                // pthread_mutex_unlock(&mutexStudent[id]);
                // goto start;
            }
            else{
                printf("Student %d has exited the simulation\n", student1->id);
                student1->is_available = -1;
                pthread_mutex_unlock(&mutexStudent[id]);
                return NULL;
                goto end;
            }
            goto start;
            end :;
        }
        reset();
        // student has completed the tut of some course
        student1->in_a_tut = 0;

        // Deciding whether student selects or not
        float value = ((float)rand() * 1.0) / RAND_MAX;


        int course_no = student1->course_pref[student1->current_pref_no];
        // int pref_no = student1->current_pref_no;
        course *course1 = &Courses[student1->course_pref[student1->current_pref_no]];

        float probability = (student1->calbre) * (course1->interest) * 1.0;

        purple();
        if (value > probability){ // select this course
            printf("Student %d has withdrawn from the %s course\n", id, course1->name);
            reset();
            
            int pref = student1->current_pref_no;
            red();
            // if ( (pref + 1 < 3) && (Courses[student1->course_pref[pref + 1]].course_removed == -1))
            if((pref < 1) && (Courses[student1->course_pref[pref + 2]].course_removed == -1)){ // 0 -1
                student1->current_pref_no++;
                student1->current_pref_no++;
                pref = student1->current_pref_no;
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 2, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else if ((pref < 2) && (pref == 1) && (Courses[student1->course_pref[pref + 1]].course_removed == -1)){ // 1
                student1->current_pref_no += 1;
                pref = student1->current_pref_no;
                pref++;
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 1, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else{
                student1->is_available = -1;
                printf("Student %d has exited the simulation\n", student1->id);
                reset();
                pthread_mutex_unlock(&mutexStudent[id]);
                return NULL;
            }
        }
        else{ // change priority if this is not the third course or else exit the simulation
            printf("Student %d has selected the %s permanently\n", id, course1->name);
            reset();
            Students[id].has_finalized_course = 1;
            student1->is_available = -1;
            pthread_mutex_unlock(&mutexStudent[id]);
            return NULL;
        }
    }
}
void handleInput(){
    scanf("%d %d %d", &no_of_students, &no_of_labs, &no_of_courses);

    // course data  // name , interest , max_slot , no of labs , list of labs
    for (int i = 0; i < no_of_courses; i++){
        Courses[i].course_id = i;
        scanf("%s %f %d %d", Courses[i].name, &Courses[i].interest, &Courses[i].max_slots, &Courses[i].no_of_labs);
        int cnt = 0;
        for (int j = 0; j < Courses[i].no_of_labs; j++){
            cnt++;
            Courses[i].course_removed = -1;
            scanf("%d", &Courses[i].lab_list[j]);
        }
        // cout << "Course " << Courses[i].name << " has " << cnt << " labs" << endl;
        

    }

    // input students dara
    // calibre(float) , pref1 , pref2 , pref3 , entry_time
    int zero = 0,one = 1;
    for (int i = 0; i < no_of_students; i++){
        Students[i].id = i;
        Students[i].in_a_tut = 0;
        Students[i].is_available = zero-1;
    }
    
    for (int i = 0; i < no_of_students; i++){
        Students[i].current_pref_no = zero;
        Students[i].has_finalized_course = one-1;
        Students[i].course = one-2;
        scanf("%f %d %d %d %d", &Students[i].calbre, &Students[i].course_pref[0], &Students[i].course_pref[1], &Students[i].course_pref[2], &Students[i].entry_time);
    }

    // input labs data
    // name , no_of_ta , max_taship_allowed
    int labs = no_of_labs , tmp = 0;
    while (1){
        int i = tmp;
        Labs[i].lab_id = i;
        scanf("%s %d %d", Labs[i].name, &Labs[i].no_of_ta, &Labs[i].taship_limit);
        // initialize the info about the tas in a lab
        int lab_wise_ta = Labs[i].no_of_ta;
        while(lab_wise_ta){
            Labs[i].tas[Labs[i].no_of_ta-lab_wise_ta].taship_ongoing = 0;
            Labs[i].tas[Labs[i].no_of_ta-lab_wise_ta].no_of_taship_completed = 0;
            Labs[i].tas[Labs[i].no_of_ta-lab_wise_ta].id = Labs[i].no_of_ta-lab_wise_ta;
            lab_wise_ta--;
        }
        lab_wise_ta = Labs[i].no_of_ta;
        while(lab_wise_ta){
            Labs[i].no_of_ta_active = Labs[i].no_of_ta;
            Labs[i].is_available = 1;
            Labs[i].tas[Labs[i].no_of_ta-lab_wise_ta].course_id = -1;
            lab_wise_ta--;
        }
        tmp++;
        // for (int j = 0; j < Labs[i].no_of_ta; j++){
        // }
        if(tmp == labs){
            break;
        }
    }
}
void func_pthread_mutex_init(int k){
    int i=0,j=0;
    while(i<k){
        while(j<50){
            pthread_mutex_init(&mutexTas[i][j], NULL);
            j++;
        }
        i++;
    }
}

int main(){
    srand(time(0));
    // freopen("input.txt", "r" , stdin);
    handleInput();
    // set locks
    int calc = 0;
    for (int i = 0; i < no_of_students; i++){
        pthread_mutex_init(&mutexStudent[i], NULL);
        pthread_cond_init(&condStudent[i], NULL);
        calc += Students[i].calbre;
    }
    func_pthread_mutex_init(no_of_labs);
    // for (int i = 0; i < no_of_labs; i++){
    //     for (int j = 0; j < 50; j++){
    //         pthread_mutex_init(&mutexTas[i][j], NULL);
    //     }
    // }
    calc = 0;
    // setup threads
    for (int i = 0; i < no_of_students; i++){
        pthread_create(&studentThread[i], NULL, handleStudent, &Students[i]);
        calc++;
    }

    for (int i = 0; i < no_of_courses; i++){
        calc++;
        pthread_create(&courseThread[i], NULL, handleCourse, &Courses[i]);
    }

    // join student thread
    for (int i = 0; i < no_of_students; i++){
        calc++;
        pthread_join(studentThread[i], NULL);
    }
    // cout << "calc: " << calc << endl;

    printf("Simulation Over\n");
    return 0;
}