
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q1/main.c ####################//

#include "headers.h"

// rng(a,b) gives a random integer between a and b
int rng(int a, int b)
{ // [a, b]
    int dif = b - a + 1;
    int rnd = rand() % dif;
    return (a + rnd);
}

// color code
void red()
{
    printf("\033[1;31m");
}

void yellow()
{
    printf("\033[1;33m");
}

void reset()
{
    printf("\033[0m");
}

void green()
{
    printf("\033[1;32m");
}

void blue()
{
    printf("\033[1;34m");
}

void purple()
{
    printf("\033[1;35m");
}

void handleInput()
{
    scanf("%d %d %d", &no_of_students, &no_of_labs, &no_of_courses);

    // course data  // name , interest , max_slot , no of labs , list of labs
    for (int i = 0; i < no_of_courses; i++)
    {
        scanf("%s %f %d %d", Courses[i].name, &Courses[i].interest, &Courses[i].max_slots, &Courses[i].no_of_labs);

        for (int j = 0; j < Courses[i].no_of_labs; j++)
        {
            scanf("%d", &Courses[i].lab_list[j]);
        }

        Courses[i].course_id = i;
        Courses[i].course_removed = -1;
    }

    // input students dara
    // calibre(float) , pref1 , pref2 , pref3 , entry_time
    for (int i = 0; i < no_of_students; i++)
    {
        scanf("%f %d %d %d %d", &Students[i].calbre, &Students[i].course_pref[0], &Students[i].course_pref[1], &Students[i].course_pref[2], &Students[i].entry_time);
        Students[i].id = i;
        Students[i].has_finalized_course = 0;
        Students[i].current_pref_no = 0;
        Students[i].is_available = -1;
        Students[i].in_a_tut = 0;
        Students[i].course = -1;
    }

    // input labs data
    // name , no_of_ta , max_taship_allowed
    for (int i = 0; i < no_of_labs; i++)
    {
        scanf("%s %d %d", Labs[i].name, &Labs[i].no_of_ta, &Labs[i].taship_limit);

        // initialize the info about the tas in a lab
        for (int j = 0; j < Labs[i].no_of_ta; j++)
        {
            Labs[i].tas[j].id = j;
            Labs[i].tas[j].course_id = -1;
            Labs[i].tas[j].no_of_taship_completed = 0;
            Labs[i].tas[j].taship_ongoing = 0;
            Labs[i].is_available = 1;
            Labs[i].no_of_ta_active = Labs[i].no_of_ta;
        }

        Labs[i].lab_id = i;
    }
}


int main()
{
    srand(time(0));
    handleInput();

    // set locks
    for (int i = 0; i < no_of_students; i++)
    {
        pthread_mutex_init(&mutexStudent[i], NULL);
        pthread_cond_init(&condStudent[i], NULL);
    }

    for (int i = 0; i < no_of_labs; i++)
    {
        for (int j = 0; j < 50; j++)
        {
            pthread_mutex_init(&mutexTas[i][j], NULL);
        }
    }

    // setup threads
    for (int i = 0; i < no_of_students; i++)
        pthread_create(&studentThread[i], NULL, handleStudent, &Students[i]);

    for (int i = 0; i < no_of_courses; i++)
        pthread_create(&courseThread[i], NULL, handleCourse, &Courses[i]);

    // join student thread
    for (int i = 0; i < no_of_students; i++)
        pthread_join(studentThread[i], NULL);

    printf("Simulation Over\n");
    return 0;
}
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q1/students.c ####################//

#include "headers.h"

// one thread per student
void *handleStudent(void *args)
{
    student *student1 = (student *)args;
    int id = student1->id;
    int t = student1->entry_time;
    sleep(t);

    blue();
    printf("Students %d has filled its course preference\n", student1->id);
    reset();

    student1->is_available = 1;

    while (1)
    {
        pthread_mutex_lock(&mutexStudent[id]);
start:
        student1->course = -1;
        student1->in_a_tut =0;
        // wait for a signal from course to indicate that a tutorial has been completed
        if(Courses[student1->course_pref[student1->current_pref_no]].course_removed == -1)
        {
            pthread_cond_wait(&condStudent[id], &mutexStudent[id]);
        }

        student1->in_a_tut = 0;

        // check if signal was because course has been removed
        // course has been removed
        // move to the next course which is available
        if (Courses[student1->course].course_removed == 1)
        {
            int pref = student1->current_pref_no;

            // check which one is possible
            if ((pref + 1 < 3) && (Courses[student1->course_pref[pref + 1]].course_removed == -1))
            {
                student1->current_pref_no++;
                pref = student1->current_pref_no;

                red();
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 1, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else if ((pref + 2 < 3) && (Courses[student1->course_pref[pref + 2]].course_removed == -1))
            {
                student1->current_pref_no += 2;
                pref = student1->current_pref_no;

                red();
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 2, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else
            {
                red();
                printf("Student %d has exited the simulation\n", student1->id);
                reset();
                student1->is_available = -1;
                pthread_mutex_unlock(&mutexStudent[id]);
                return NULL;
            }
        }

        // student has completed the tut of some course
        student1->in_a_tut = 0;

        // Deciding whether student selects or not
        float value = ((float)rand() * 1.0) / RAND_MAX;

        int pref_no = student1->current_pref_no;

        int course_no = student1->course_pref[pref_no];
        course *course1 = &Courses[course_no];

        float probability = (student1->calbre) * (course1->interest) * 1.0;

        if (value <= probability) // select this course
        {
            purple();
            printf("Student %d has selected the %s permanently\n", id, course1->name);
            Students[id].has_finalized_course = 1;
            reset();
            student1->is_available = -1;
            pthread_mutex_unlock(&mutexStudent[id]);
            return NULL;
        }
        else // change priority if this is not the third course or else exit the simulation
        {
            purple();
            printf("Student %d has withdrawn from the %s course\n", id, course1->name);
            reset();
            
            int pref = student1->current_pref_no;
            
            if ( (pref + 1 < 3) && (Courses[student1->course_pref[pref + 1]].course_removed == -1))
            {
                student1->current_pref_no++;
                pref = student1->current_pref_no;

                red();
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 1, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else if ((pref + 2 < 3) && (Courses[student1->course_pref[pref + 2]].course_removed == -1))
            {
                student1->current_pref_no += 2;
                pref = student1->current_pref_no;

                red();
                printf("Student id %d has changed its preference from %s (priority %d) to %s (priority %d)\n", student1->id, Courses[student1->course].name, pref - 2, Courses[student1->course_pref[pref]].name, pref);
                reset();
                // pthread_mutex_unlock(&mutexStudent[id]);
                goto start;
            }
            else
            {
                red();
                student1->is_available = -1;
                printf("Student %d has exited the simulation\n", student1->id);
                reset();
                pthread_mutex_unlock(&mutexStudent[id]);
                return NULL;
            }
        }
    }
}
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q1/courses.c ####################//

#include "headers.h"

// will initialize the course , then will search for an ta to be assigned // allocate seats // get student // tutorial begin // tut end // ta free and
// student makes choices // repeat
void *handleCourse(void *args)
{
    course *course1 = (course *)args;

    // will store the id's of the student selected for a tutorial
    int students_selected[100];

    red();
    printf("Course %s has been initialized\n", course1->name);
    reset();

    int iter_count = 0;
    int flag = 0;

    while (1)
    {

    check_lab_removal_start:
        iter_count = 0; // check for lab removal after every 10 iterations
        flag = 0;

        // check if course is available
        for (int i = 0; i < course1->no_of_labs; i++)
        {
            lab *lab1 = &Labs[course1->lab_list[i]];
            int max_taship = lab1->taship_limit;

            for (int j = 0; j < lab1->no_of_ta; j++)
            {
                if (lab1->tas[j].no_of_taship_completed < max_taship)
                {
                    flag = 1;
                    break;
                }
            }

            if (flag == 1)
                break;
        }

        if (flag == 0)
        {
            // course as been removed
            red();
            printf("Course %s has been removed from the portal\n", course1->name);

            course1->course_removed = 1;
            reset();

            for (int i = 0; i < no_of_students; i++)
            {
                // pthread_mutex_lock(&mutexStudent[i]);
                if (Students[i].course_pref[Students[i].current_pref_no] == course1->course_id)
                {
                    Students[i].course = course1->course_id;
                    pthread_cond_signal(&condStudent[Students[i].id]);
                }
            }
            return NULL;
        }

        // find a ta for this course from the ta list
        int ta_no = -1;
        int lab_no = -1;
        int course_ta_found = 0;
        ta *ta_selected;

        while (course_ta_found == 0)
        {
            iter_count++;
            if (iter_count > 10)
            {
                goto check_lab_removal_start;
            }

            // iterate through the list of tas
            for (int i = 0; i < course1->no_of_labs; i++)
            {

                lab *lab1 = &Labs[course1->lab_list[i]];

                {

                    red();
                    // printf("Start data\n\n");

                    for (int j = 0; j < lab1->no_of_ta; j++)
                    {

                        ta *ta1 = &lab1->tas[j];
                        if ((ta1->taship_ongoing == 0) && (ta1->no_of_taship_completed < lab1->taship_limit))
                        {
                            // printf("Course %s lab_name: %s ta_id: %d before\n", course1->name, lab1->name, ta1->id);

                            if (pthread_mutex_trylock(&mutexTas[lab1->lab_id][j]) != 0)
                            {
                                continue;
                            }

                            course_ta_found = 1;
                            ta1->taship_ongoing = 1;
                            ta1->no_of_taship_completed++;
                            ta1->course_id = course1->course_id;
                            ta_selected = ta1;
                            ta_no = ta1->id;
                            lab_no = lab1->lab_id;
                            break;
                        }
                    }

                    red();
                }

                if (course_ta_found == 1)
                    break;
            }
        }

        if (course_ta_found == 1)
        {
            // assign the ta to this course
            green();
            printf("Course %s has been allocated TA %d from lab %s\n", course1->name, ta_no, Labs[lab_no].name);
            yellow();
            printf("TA %d from lab %s has been allocated to course %s for his %d taship\n", ta_no, Labs[lab_no].name, course1->name, ta_selected->no_of_taship_completed);
            reset();

            // allocate random number of seats between 1 and max_slot for this course
            int slots = rng(1, course1->max_slots);

            green();
            printf("Course %s has been allocated %d seats\n", course1->name, slots);
            reset();

            int num_student_selected = 0;

            while (num_student_selected == 0)
            {

                // select students
                for (int i = 0; i < no_of_students; i++)
                {

                    if ((Students[i].is_available != -1) && (Students[i].course_pref[Students[i].current_pref_no] == course1->course_id) && (Students[i].in_a_tut == 0))
                    {
                        pthread_mutex_lock(&mutexStudent[i]);
                        Students[i].in_a_tut = 1;
                        students_selected[num_student_selected] = i;
                        Students[i].course = course1->course_id;
                        num_student_selected++;

                        green();
                        printf("Student %d has been allocated course %s\n", i, course1->name);
                        reset();
                    }

                    if (num_student_selected >= slots)
                        break;

                }

                if (num_student_selected == 0)
                {
                    // make this thread go for sleep for one sec before checking again
                    sleep(1);
                }
            }

            // start the tutorial // go to sleep for 2 sec
            purple();
            printf("Tutorial has started for course %s with %d seats fulled out of %d\n", course1->name, num_student_selected, slots);
            sleep(2);

            // tutorial has ended
            purple();
            printf("ta %d from lab %s has completed tut for course %s\n", ta_no, Labs[lab_no].name, course1->name);
            reset();

            // signal all the selected students . that tut has ended and they can update their choices
            for (int i = 0; i < num_student_selected; i++)
            {
                pthread_mutex_unlock(&mutexStudent[students_selected[i]]);
                pthread_cond_signal(&condStudent[students_selected[i]]);
            }

            ta_selected->taship_ongoing = 0;

            // checking for ta removal
            int max_taship = Labs[lab_no].taship_limit;
            int lab_flag = 0;

            for (int i = 0; i < Labs[lab_no].no_of_ta; i++)
            {
                if (Labs[lab_no].tas[i].no_of_taship_completed < max_taship)
                {
                    lab_flag = 1;
                }
            }

            if (lab_flag == 0 && Labs[lab_no].is_available != -1)
            {
                green();
                printf("Lab %s has been removed\n", Labs[lab_no].name);
                reset();
                Labs[lab_no].is_available = -1;
            }

            pthread_mutex_unlock(&mutexTas[lab_no][ta_selected->id]);
        }
    }
}
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q1/headers.h ####################//

#ifndef _headers
#define  _headers

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>

#define MAX_STUDENT 500
#define MAX_COURSE 50
#define MAX_LABS 50
#define MAX_TA_IN_LAB 50
#define MAX_LABS_IN_COURSE 50

// student
typedef struct student
{
    int id;   
    float calbre;
    int entry_time;
    int course_pref[3]; 
    int has_finalized_course;  // if 0 , it has not finalized else yes , it has finalalized
    int is_available;  // will tell if student has filled its choices or not?
    int current_pref_no;  // tell which priority is it at this moment
    int in_a_tut;         // will tell if a student is in a tut or not?
    int course;           // tells which course he is planning to take // -1 means no signal was not sent from any course 
}student;

// course
typedef struct course
{
    int course_id;
    char name[256];
    float interest;
    int max_slots;
    int no_of_labs;       
    int lab_list[100];    // assuming course do not take tas from more than 10 lasb
    int course_removed;   // if 1 that means course has been removed from the portal
    // int no_of_student_interested;

}course;

typedef struct ta
{
    int id;
    int course_id;                    // stores the id of the course it is taking 
    int no_of_taship_completed;       // stores how many taship it has completed
    int taship_ongoing;               // 0-> ta is idle // 1-> he is taking tut for course[course_id] 
}ta;

// labs -> max 50 ta's are allowed
typedef struct lab{
    int lab_id;
    char name[256];
    int taship_limit;
    int no_of_ta;            // stores how many ta lab has  // ids are 0 indexed
    ta tas[51];              // stores the info about the tas in a lab
    int is_available;        // if 1 -> means ta some ta is there who has not completed its quota else 0
    int no_of_ta_active;           // stores how many tas are there who has not completed their quota
}lab;

int no_of_students , no_of_courses , no_of_labs;

student Students[MAX_STUDENT];
pthread_t studentThread[MAX_STUDENT];
pthread_mutex_t mutexStudent[MAX_STUDENT];
pthread_cond_t condStudent[MAX_STUDENT];

course Courses[MAX_COURSE];
pthread_t courseThread[MAX_COURSE];
// pthread_mutex_t mutexCourse[MAX_COURSE];

lab Labs[MAX_LABS];
pthread_t labThread[MAX_LABS];
pthread_mutex_t mutexTas[MAX_LABS][MAX_TA_IN_LAB];
// pthread_cond_t condLabs[MAX_LABS];

// function definition
void* handleStudent(void* );
void* handleCourse(void* );

// functions to print in diff color
void red () ;
void yellow () ;
void reset ();
void green ();
void blue ();
void purple();

int rng(int , int);

#endif 