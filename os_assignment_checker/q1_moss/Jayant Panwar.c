
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/labs.c ####################//

#include "headers.h"
#include "colors.h"
#include "labs.h"

void *labSim(void *a)
{
  Lab *l = (Lab *)a;
  while (l->ta_worthy != 2 && l->ta_limit == false)
  {
    pthread_mutex_lock(&l->mutex);
    l->ta_worthy = 0;
    l->ta_limit = true;

    for (int i = 0; i < l->n_tas; i++)
    {
      if (l->curr_times[i] > l->max_times)
      {
        continue;
      }
      l->ta_limit = false;
      l->ta_worthy = 1;
    }

    // EVENT 12
    if (!l->ta_worthy && l->ta_limit == true)
    {
      l->ta_worthy = 2;
      printf(COLOR_BRMAGENTA "Lab %s no longer has students available for TA ship\n" COLOR_RESET, l->name);
    }
    pthread_mutex_unlock(&l->mutex);
  }

  return 0;
}

void createLabThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&lab_thread[i], 0, labSim, college_labs[i]);
  }
}

void initLab(int i)
{
  int UID = i;
  Lab *l = (Lab *)malloc(sizeof(Lab));
  scanf("%s %d %d", l->name, &l->n_tas, &l->max_times);

  l->uid = UID;
  l->ta_worthy = 0;
  l->ta_limit = false;

  int numTAs = l->n_tas;
  int j = 0;
  while (j < numTAs)
  {
    l->curr_times[j] = 0;
    l->availability[j] = true;
    j++;
  }

  pthread_mutex_init(&l->mutex, 0);

  college_labs[i] = l;
}
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/main.c ####################//

#include "headers.h"
#include "init.h"

int randomRange(int l, int r)
{
  if (l > r)
  {
    return l;
  }

  return (rand() % (r - l + 1)) + l;
}

int main()
{
  scanf("%d %d %d", &Students, &Labs, &Courses);

  initialization();

  int x = 0;
  int simOver = 0;

  while (x < Students)
  {
    pthread_join(student_thread[x], 0);
    x++;
  }

  simOver = 1;
  x = 0;

  while (x < Students && simOver)
  {
    if (students[x]->curr_pref != -1)
    {
      simOver = 0;
    }
    x++;
  }

  if (simOver)
  {
    printf("Simulation Completed!");
    printf("\n");
  }

  return 0;
}
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/students.h ####################//

#ifndef __STUDENTS_H
#define __STUDENTS_H

void initStudent(int i);

void createStudentThreads(int i, int j);

#endif
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/init.h ####################//

#ifndef INIT_H
#define INIT_H

void initialization();

#endif
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/labs.h ####################//

#ifndef __LABS_H
#define __LABS_H

void initLab(int i);

void createLabThreads(int i, int j);

#endif
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/courses.h ####################//

#ifndef __COURSES_H
#define __COURSES_H

void initCourse(int i);

void createCourseThreads(int i, int j);

#endif
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/students.c ####################//

#include "headers.h"
#include "students.h"
#include "colors.h"
#include <stdbool.h>

void allocateSlot(Student *s, bool waiting)
{
  while (s->curr_pref != -1 && waiting)
  {
    int i = 0;
    while (i < Courses)
    {
      pthread_mutex_lock(&courses[i]->mutex);

      if (s->curr_alloc == -1 && waiting && courses[i]->d > 0)
      {
        if (courses[i]->ta_allocated >= 0 && courses[i]->uid == s->curr_pref)
        {
          pthread_mutex_lock(&s->mutex);
          int UID = courses[i]->uid;
          s->curr_alloc = UID;
          pthread_mutex_unlock(&s->mutex);

          // EVENT 2
          UID = s->uid;
          printf(COLOR_GREEN "Student %d has been allocated a seat in course %s\n" COLOR_RESET, s->uid, courses[i]->name);

          courses[i]->d--;
          UID = s->uid;

          pthread_mutex_unlock(&courses[i]->mutex);
          break;
        }
      }

      pthread_mutex_unlock(&courses[i]->mutex);
      i++;
    }

    if (s->curr_pref != -1 && courses[s->curr_pref]->courseValid <= 0)
    {
      int UID;
      UID = s->uid;
      // EVENT 3
      printf(COLOR_YELLOW "Student %d has withdrawn from course %s\n" COLOR_RESET, UID, courses[s->curr_pref]->name);
      pthread_mutex_lock(&s->mutex);
      if (s->curr_pref == s->pref[0])
      {
        s->curr_alloc = -1;
        s->curr_pref = s->pref[1];
        // EVENT 4
        printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" COLOR_RESET, UID, courses[s->pref[0]]->name, courses[s->pref[1]]->name);
      }
      else if (s->curr_pref == s->pref[1])
      {
        s->curr_alloc = -1;
        s->curr_pref = s->pref[2];
        // EVENT 4
        printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" COLOR_RESET, s->uid, courses[s->pref[1]]->name, courses[s->pref[2]]->name);
      }
      else
      {
        // EVENT 6
        printf(COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" COLOR_RESET, s->uid);
        int flag = -1;
        s->curr_pref = flag;
      }
      pthread_mutex_unlock(&s->mutex);
    }
  }

  return;
}

void *studentSim(void *a)
{
  int time;
  Student *s = (Student *)a;

  time = s->arr_time;
  sleep(time);

  // EVENT 1
  printf(COLOR_RED "Student %d has filled in preferences for course registration\n" COLOR_RESET, s->uid);

  allocateSlot(s, true);
  return 0;
}

void createStudentThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&student_thread[i], 0, studentSim, students[i]);
  }
}

void initStudent(int i)
{
  int UID = i;
  Student *s = (Student *)malloc(sizeof(Student));

  scanf("%lf %d %d %d %d", &s->callibre, &s->pref[0], &s->pref[1], &s->pref[2], &s->arr_time);

  s->curr_pref = s->pref[0];

  s->uid = UID;
  s->finalised = 0;
  s->curr_alloc = -1;

  pthread_mutex_init(&s->mutex, 0);

  students[i] = s;
}
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/courses.c ####################//

#include "headers.h"
#include "colors.h"
#include "courses.h"
#include <stdbool.h>

void *courseSim(void *a)
{
  Course *t = (Course *)a;
  bool running = true;

  while (running)
  {
    pthread_mutex_lock(&t->mutex);
    int no_labs = t->p;
    int k = 0;
    while (k < no_labs)
    {
      Lab *l = college_labs[t->course_labs[k]];
      char *labname = l->name;
      if (strlen(labname) > 80)
      {
        labname = l->name;
      }
      pthread_mutex_lock(&l->mutex);
      for (int i = 0; i < l->n_tas; i++)
      {
        // checking if the TAs are available or not
        int maxTimes = l->max_times;
        int currTimes = l->curr_times[i];
        if (l->availability[i] == true && currTimes < maxTimes)
        {
          // checking in case the TA is not allocated yet
          if (t->ta_allocated == -1)
          {

            // BONUS part
            int equalOpp = 1;
            int numTAs = l->n_tas;
            for (int j = 0; j < numTAs; j++)
            {
              if (j == i)
              {
                continue;
              }
              if (l->curr_times[i] - l->curr_times[j] > 0)
              {
                equalOpp = 0;
              }
            }

            if (!equalOpp)
            {
              pthread_mutex_lock(&l->mutex);
              break;
            }

            t->ta_allocated = i;
            t->lab_allocated = l->uid;

            // printing the TAship with proper numbering, EVENT 11
            switch (l->curr_times[i])
            {
            case 0:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dst TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 1:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dnd TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 2:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %drd TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            case 3:
              printf(COLOR_BRBLUE "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n" COLOR_RESET, i, l->name, t->name, l->curr_times[i] + 1);
              break;

            default:
              break;
            }
            l->availability[i] = false;
            l->curr_times[i]++;
            break;
          }
        }
      }
      pthread_mutex_unlock(&l->mutex);
      k++;
    }

    int allocTAs = t->ta_allocated;
    if (allocTAs >= 0 && t->tutorial == false)
    {
      bool checkTute = true;
      int maxSlots = t->course_max_slots;
      t->d = randomRange(1, maxSlots);
      t->tutorial = checkTute;
      t->tut_seats = t->d;
      // EVENT 7
      int seats = t->d;
      printf(COLOR_WHITE "Course %s has been allotted %d seats\n" COLOR_RESET, t->name, seats);
    }

    if (t->tutorial)
    {
      int w = 0;
      int x = 0;
      while (x < Students)
      {
        int UID = t->uid;
        if (students[x]->curr_pref == UID)
        {
          w++;
        }
        x++;
      }

      if (w != 0 && (w + t->d == t->tut_seats || t->d == 0))
      {
        // EVENT 8
        int totalSeats = t->tut_seats;
        int filledSeats = totalSeats - t->d;
        printf(COLOR_BRRED "Tutorial has started for %s with %d seats filled out of %d\n" COLOR_RESET, t->name, filledSeats, totalSeats);
        // sleeping for 2 seconds to mimic the tutorial duration
        sleep(2);

        // resettting values after tutorial finished
        int reset = 0;
        t->tutorial = false;
        t->d = reset;
        t->tut_seats = reset;

        // TA has left
        // EVENT 9
        printf(COLOR_BRGREEN "TA %d from lab %s has completed the tutorial and left the course %s\n" COLOR_RESET, t->ta_allocated, college_labs[t->lab_allocated]->name, t->name);
        int flag = -1;
        t->ta_allocated = flag;
        t->lab_allocated = flag;

        x = 0;
        while (x < Students)
        {
          int currentPref = students[x]->curr_pref;
          int currentAlloc = students[x]->curr_alloc;
          if (currentPref >= 0 && currentAlloc == t->uid)
          {
            double probability;
            Student *s = students[x];

            pthread_mutex_lock(&s->mutex);
            probability = s->callibre;
            probability *= t->interest;

            bool finalised = (rand() % 100) < (probability * 100);

            if (finalised)
            {
              // EVENT 5
              int UID = s->uid;
              printf(COLOR_MAGENTA "Student %d has selected %s permanently\n" COLOR_RESET, UID, t->name);
              s->curr_pref = flag;
            }
            else
            {
              // EVENT 3
              printf(COLOR_YELLOW "Student %d has withdrawn from course %s\n" COLOR_RESET, s->uid, t->name);

              if (s->curr_pref == s->pref[0])
              {
                s->curr_alloc = -1;
                s->curr_pref = s->pref[1];
                // EVENT 4
                printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" COLOR_RESET, s->uid, courses[s->pref[0]]->name, courses[s->pref[1]]->name);
              }
              else if (s->curr_pref == s->pref[1])
              {
                s->curr_alloc = -1;
                s->curr_pref = s->pref[2];
                // EVENT 4
                printf(COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" COLOR_RESET, s->uid, courses[s->pref[1]]->name, courses[s->pref[2]]->name);
              }
              else
              {
                // EVENT 6
                int UID = s->uid;
                int flag = -1;
                printf(COLOR_CYAN "Student %d couldn't get any of his preferred courses\n" COLOR_RESET, UID);
                s->curr_pref = flag;
              }
            }

            pthread_mutex_unlock(&s->mutex);
          }
          x++;
        }
      }
    }

    if (t->courseValid > 0)
    {
      t->courseValid = 0;

      int num_labs = t->p;
      int k = 0;

      while (k < num_labs)
      {
        if (college_labs[t->course_labs[k]]->ta_worthy != 2)
        {
          t->courseValid = 1;
        }
        k++;
      }
    }

    if (t->courseValid == 0)
    {
      int flag = -1;
      t->courseValid = flag;
      // EVENT 10
      printf(COLOR_BRYELLOW "Course %s doesn't have any TA's eligible and is removed from course offerings\n" COLOR_RESET, t->name);
    }

    pthread_mutex_unlock(&t->mutex);
  }

  return 0;
}

void createCourseThreads(int i, int j)
{
  if (j == 0)
  {
    pthread_create(&course_thread[i], 0, courseSim, courses[i]);
  }
}

void initCourse(int i)
{
  int UID = i;
  Course *t = (Course *)malloc(sizeof(Course));

  scanf("%s %lf %d %d", t->name, &t->interest, &t->course_max_slots, &t->p);

  int num_labs = t->p;
  for (int j = 0; j < num_labs; j++)
  {
    scanf("%d", &t->course_labs[j]);
  }

  int flag = -1;
  t->uid = UID;
  t->ta_allocated = flag;
  t->d = 0;
  t->tutorial = false;
  t->tut_seats = 0;
  t->lab_allocated = flag;
  t->courseValid = 1;

  pthread_mutex_init(&t->mutex, 0);

  courses[i] = t;
}
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/colors.h ####################//

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_BRRED "\x1b[91m"
#define COLOR_BRGREEN "\x1b[92m"
#define COLOR_BRYELLOW "\x1b[93m"
#define COLOR_BRBLUE "\x1b[94m"
#define COLOR_BRMAGENTA "\x1b[95m"
#define COLOR_RESET "\x1b[0m"
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/headers.h ####################//

#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int Students; // no of students
int Labs;     // no of labs
int Courses;  // no of courses

typedef struct Student
{
  int uid;

  double callibre;
  int arr_time;
  int finalised;
  int curr_pref;
  int curr_alloc;
  int pref[3]; // stores all the three preferences

  pthread_mutex_t mutex;
} Student;

typedef struct Course
{
  int uid;

  double interest;
  int course_max_slots;
  int p;
  int d;
  int ta_allocated;
  char name[100];
  int course_labs[100];

  int lab_allocated;
  bool tutorial;
  int tut_seats;
  int courseValid;

  pthread_mutex_t mutex;
} Course;

typedef struct Lab
{
  int uid;

  char name[100];
  int curr_times[100];
  bool availability[100];

  int n_tas;
  int max_times;
  int ta_worthy;
  bool ta_limit;

  pthread_mutex_t mutex;
} Lab;

Student **students;
Course **courses;
Lab **college_labs;

pthread_t *student_thread;
pthread_t *course_thread;
pthread_t *lab_thread;

int randomRange(int l, int r);
//###########FILE CHANGE ./main_folder/Jayant Panwar_305782_assignsubmission_file_/q1/init.c ####################//

#include "headers.h"
#include "students.h"
#include "labs.h"
#include "courses.h"
#include "init.h"

void initializeEntities()
{
  int x = 0;

  while (x < Courses)
  {
    initCourse(x);
    x++;
  }
  x = 0;

  while (x < Students)
  {
    initStudent(x);
    x++;
  }
  x = 0;

  while (x < Labs)
  {
    initLab(x);
    x++;
  }
}

void createThreads()
{
  int x = 0;

  while (x < Students)
  {
    createStudentThreads(x, 0);
    x++;
  }
  x = 0;

  while (x < Courses)
  {
    createCourseThreads(x, 0);
    x++;
  }
  x = 0;

  while (x < Labs)
  {
    createLabThreads(x, 0);
    x++;
  }
}

void initialization()
{
  srand(time(0));

  courses = (Course **)malloc(sizeof(Course *) * Courses);
  course_thread = (pthread_t *)malloc(sizeof(pthread_t) * Courses);

  students = (Student **)malloc(sizeof(Student *) * Students);
  student_thread = (pthread_t *)malloc(sizeof(pthread_t) * Students);

  college_labs = (Lab **)malloc(sizeof(Lab *) * Labs);
  lab_thread = (pthread_t *)malloc(sizeof(pthread_t) * Labs);

  initializeEntities();
  createThreads();
}