
//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q1/q1.h ####################//

#ifndef Q1_H
#define Q1_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define NOT_REG false
#define ATD_TUT_C1 true
#define ATD_TUT_C2 2
#define ATD_TUT_C3 3
#define STUD_EXIT 10

#define WAIT_TA false
#define WAIT_SLOT true
#define TUT_ON 2
#define WTDRN 3

#define EXHAUSTED true
#define NT_EXHSTD false

#define WAIT_C1 4
#define WAIT_C2 5
#define WAIT_C3 6
#define PERM_C1 7
#define PERM_C2 8
#define PERM_C3 9
#define NT_ALTD false
#define TA_BUSY true

typedef struct CourseStruct CourseStruct;
typedef struct StudentStruct StudentStruct;
typedef struct LabStruct LabStruct;

typedef CourseStruct *ptr_course_struct;
typedef StudentStruct *ptr_student_struct;
typedef LabStruct *ptr_lab_struct;

struct CourseStruct
{
    int course_id;
    char name[50];
    float interest;
    int course_max_slot;
    int no_of_labs;
    int *lab_list;
    int status;
};

struct StudentStruct
{
    float calibre;
    int student_id;
    int time;
    int pref1,pref2,pref3;
    int status;
};

struct LabStruct
{
    int lab_id;
    int status;
    int TA_num;
    char name[50];
    int max_times;
    int **TA;
};

#endif

//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q1/q1.c ####################//

#include "functions.h"
#include <stdbool.h>
#include "colors.h"

// #defines
#define fo(x, y) for(int x = 0; x < y; ++x)

// Global variables
int nCourses, nStudents, nLabs, nCoursesLeft, timeNow;

// Structs variables
ptr_course_struct *course;
ptr_student_struct *student;
ptr_lab_struct *lab;
pthread_t time_thread;

// mutex and condition locks
pthread_mutex_t lockStudent, lockCheckTA, *lockLab;
pthread_cond_t checkTime = PTHREAD_COND_INITIALIZER, checkTA = PTHREAD_COND_INITIALIZER;

int main()
{
    // Input variables
    scanf("%d%d%d", &nStudents, &nLabs, &nCourses);
    nCoursesLeft = nCourses;
    course = (ptr_course_struct *)(malloc(sizeof(ptr_course_struct) * nCourses));

    // Initialize course structs
    fo(i, nCourses)
    {
        course[i] = malloc(sizeof(CourseStruct));
        float interest;
        int maxSlot, nLabs;
        scanf("%s %f %d %d", course[i]->name, &interest, &maxSlot, &nLabs);

        course[i]->interest = interest;
        course[i]->course_max_slot = maxSlot;
        course[i]->status = WAIT_TA;
        course[i]->no_of_labs = nLabs;
        course[i]->lab_list = malloc(sizeof(int) * nLabs);
        course[i]->course_id = i;

        if (!course[i]->lab_list)
            return 1;
        fo (j, course[i]->no_of_labs)
            scanf("%d", &course[i]->lab_list[j]);
    }

    // Initialize student structs
    student = (ptr_student_struct *)(malloc(sizeof(ptr_student_struct) * nStudents));
    fo(i, nStudents)
        student[i] = (ptr_student_struct)malloc(sizeof(StudentStruct)), student[i]->student_id = i, student[i]->status = NOT_REG,
        scanf("%f%d%d%d%d", &student[i]->calibre, &student[i]->pref1, &student[i]->pref2, &student[i]->pref3, &student[i]->time);

    // Initialize lab structs
    lab = (ptr_lab_struct *)(malloc(sizeof(ptr_lab_struct) * nLabs));

    fo(i, nLabs)
    {
        lab[i] = (ptr_lab_struct)malloc(sizeof(LabStruct)), lab[i]->lab_id = i;

        int taNum, maxTime;
        scanf("%s%d%d", lab[i]->name, &taNum, &maxTime);

        lab[i]->TA_num = taNum;
        lab[i]->max_times = maxTime;
        lab[i]->TA = malloc(sizeof(int *) * lab[i]->TA_num);

        fo(j, lab[i]->TA_num)
            lab[i]->TA[j] = (int *)malloc(sizeof(int) * 2), lab[i]->TA[j][0] = NT_ALTD, lab[i]->TA[j][1] = 0;

        lab[i]->status = lab[i]->max_times && lab[i]->TA_num ? NT_EXHSTD : EXHAUSTED;
    }

    // Initialize mutex and condition locks
    pthread_t thread_students[nStudents];
    pthread_t thread_courses[nCourses];
    srand(time(0));
    pthread_mutex_init(&lockStudent, NULL), pthread_mutex_init(&lockCheckTA, NULL);

    lockLab = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * nLabs);
    if (lockLab == NULL)
        return 6;

    // Initialize lab mutex locks
    fo(labs, nLabs)
        pthread_mutex_init(&lockLab[labs], NULL);

    fo(i, nStudents)
    {
        int *id = malloc(sizeof(int)); *id = i;
        if (pthread_create(&thread_students[i], NULL, &studentFunc, id))
            return 3;
    }

    // Initialize course mutex locks
    fo(i, nCourses)
    {
        int *id = malloc(sizeof(int));
        *id = i;
        if (pthread_create(&thread_courses[i], NULL, &coursesFunc, id))
            return 4;
    }

    // Initialize time thread
    if (pthread_create(&time_thread, NULL, &timerFunc, NULL))
        return 5;

    // Wait for all threads to finish
    fo(i, nStudents)
        pthread_join(thread_students[i], NULL);

    // Wait for all threads to finish
    pthread_join(time_thread, NULL);

    fo(i, nCourses)
        pthread_join(thread_courses[i], NULL);

    // Destroy mutex and condition locks
    pthread_mutex_destroy(&lockStudent);
    pthread_mutex_destroy(&lockCheckTA);

    // Destroy lab mutex locks
    fo(labs, nLabs)
        pthread_mutex_destroy(&lockLab[labs]);

    pthread_cond_destroy(&checkTime);
    pthread_cond_destroy(&checkTA);

    // Free memory
    fo(i, nCourses)
        free(course[i]);

    free(course);

    fo(i, nStudents)
        free(student[i]);

    free(student);

    fo(i, nLabs)
    {
        fo(j, lab[i]->TA_num)
            free(lab[i]->TA[j]);
        free(lab[i]->TA);
        free(lab[i]);
    }
    free(lab);
    free(lockLab);

    return 0;
}

//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q1/functions.c ####################//

#include "functions.h"
#include "colors.h"
#include <stdbool.h>

#define fo(x, y) for(int x = 0; x < y; ++x)

// Print to console
void log0(int a, char* b)
{
    printf(CYAN "Student %d has been allocated a seat in course %s\n" RESET, a, b);
}

void log1(int i)
{
    printf(GREEN "Student %d has filled in preferences for course registration\n" RESET, i);
}

void log3(char* name)
{
    printf(RED "Course %s doesnt have any TAs eligible and is removed from the course offerings\n" RESET, name);
}

void log4(int a, char* b, char* c, int d)
{
    printf(MAGENTA "TA %d from lab %s has been allocated to course %s for his %d TA ship\n" RESET, a, b, c, d);
}

void log5(char* a, int b)
{
    printf(BLUE "Course %s has been alloted %d seats\n" RESET, a, b);
}

void log6(char* a, int b, int c)
{
    printf(YELLOW "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, a, b, c);
}

void log7(int a, char* b)
{
    printf(BLUE "Student %d has selected course %s permanently\n" RESET, a, b);
}

void log8(int a, char* b)
{
    printf(RED "Student %d has withdrawn from course %s\n" RESET, a, b);
}

void log9(int a)
{
    printf("Student %d couldnt get any of his preferred courses\n", a);
}

void log11(int a, char* b, char* c)
{
    printf("Student %d has changed current preference from %s to %s\n", a, b, c);
}

void log12(int a, char* b, char* c)
{
    printf("Student %d has changed current preference from %s to %s\n", a, b, c);
}

void log13(int a)
{
    printf("Student %d couldnt get any of his preferred courses\n", a);
}

void log14(int a, char* b, char* c)
{
    printf("TA %d from lab %s has completed the tutorial and left the course %s\n", a, b, c);
}

void log15(char* a)
{
    printf("Lab %s no longer has students available for TA ship\n", a);
}

// Returns true if all courses are over or all students have been permanently allocated or have exited
// Returns false otherwise
int isExit(void)
{
    int s, c;
    s = c = 0;
    int i = -1;

    int studentLeftID[nStudents];

    while (++i < nCourses) if (WTDRN != course[i]->status) c++; // Counts the number of courses that have not been filled
    i = -1;
    while (++i < nStudents ) if (PERM_C1 > student[i]->status) studentLeftID[s] = i, s++; // Counts the number of students that have not been allocated a course

#define st student[studentLeftID[i]]->status
#define pr student[studentLeftID[i]]->pref1

    if (c && s) // both of them are true
    {
        fo(i, s)
            if (st < WAIT_C1)
                return 0;
            else if (course[pr]->status < WTDRN)
                if (st == WAIT_C1 || st == WAIT_C2 || st == WAIT_C3)
                    return 0;
        return 1;
    }
    else
        return 1;
#undef st
#undef pr
}

void *timerFunc()
{
    while(true) // Infinite loop to keep timer running
    {
        sleep(1);
        ++timeNow;

        if (isExit() && timeNow % 5 == 0) // condition to exit
            return NULL;

        pthread_cond_broadcast(&checkTime);
    }
    return NULL;
}

int TAAvailable(int courseNum, int *currentTA, int *currentLab)
{
    int isTALeft = 0;
    fo(l, course[courseNum]->no_of_labs) // loop through all labs
    {
        int allowedLab = course[courseNum]->lab_list[l];
        if (lab[allowedLab]->status == EXHAUSTED)
            continue;
        fo(i, lab[allowedLab]->TA_num) // loop through all TAs
        {
            if (NT_ALTD == lab[allowedLab]->TA[i][0] && lab[allowedLab]->max_times > lab[allowedLab]->TA[i][1])
            {
                pthread_mutex_lock(&lockLab[allowedLab]);
                *currentTA = i, *currentLab = allowedLab;
                lab[*currentLab]->TA[*currentTA][0] = TA_BUSY, lab[*currentLab]->TA[*currentTA][1]++;
                pthread_mutex_unlock(&lockLab[allowedLab]);
                return 0;
            }
            isTALeft = (isTALeft == 0 && lab[allowedLab]->TA[i][1] < lab[allowedLab]->max_times)? 1 : isTALeft; // if any TA is left
        }
    }
    *currentTA = *currentLab = -1;
    return isTALeft? -1 : -2; // Returns -1 if any TA is left, -2 if no TA is left
}

void *studentFunc(void *index)
{
    int i = *(int*)index;
    pthread_mutex_lock(&lockStudent);

    while (student[i]->time > timeNow) // Wait for the time to be right
        pthread_cond_wait(&checkTime, &lockStudent);

    student[i]->status = WAIT_C1; // Change status to WAIT_C1
    pthread_mutex_unlock(&lockStudent); // Unlock the mutex
    log1(i);

    return false;
}

void *coursesFunc(void *index)
{
    int courseID = *(int *)index, currentTA, currentLab; // Declaring variables
    sleep(2); // Wait for the student to be allocated a course

    while (true)
    {
        int eligible = TAAvailable(courseID, &currentTA, &currentLab); // Check if TA is available

        if (eligible == -2)
        {
            log3(course[courseID]->name);
            --nCoursesLeft, course[courseID]->status = WTDRN;
            return 0;
        }

        while (1 < nCoursesLeft && 0 > currentTA)
        {
            int run = true;
            pthread_mutex_lock(&lockCheckTA);

            while (run)
                pthread_cond_wait(&checkTA, &lockCheckTA), run = false;

            pthread_mutex_unlock(&lockCheckTA), eligible = TAAvailable(courseID, &currentTA, &currentLab);
        }

        if (eligible == -2)
        {
            log3(course[courseID]->name);
            --nCoursesLeft, course[courseID]->status = WTDRN;
            return 0;
        }

        log4(currentTA, lab[currentLab]->name, course[courseID]->name, lab[currentLab]->TA[currentTA][1]);
        updateLabStatus();
        course[courseID]->status = WAIT_SLOT;
        int slots = (rand() % (course[courseID]->course_max_slot)) + 1;
        int W = 0, studsInterested[nStudents];
        log5(course[courseID]->name, slots);

#define st studsInterested[W]
#define sts studsInterested[s]
#define st1 student[s]->status

        for (int s = 0; slots > W && nStudents > s; ++s)
            if (st1 == WAIT_C1 && student[s]->pref1 == courseID)
                st = s, W++;
            else if (st1 == WAIT_C2 && student[s]->pref2 == courseID)
                st = s, W++;
            else if (st1 == WAIT_C3 && student[s]->pref3 == courseID)
                st = s, W++;
            else
                continue;

        course[courseID]->status = TUT_ON;
        fo(s, W)
            log0(sts, course[courseID]->name), student[sts]->status -= 3;

        log6(course[courseID]->name, W, slots);
        sleep(5);
        int chosen = 0;
        double probability;
        fo(s, W)
        {
            probability = (course[courseID]->interest) * (student[sts]->calibre);
            if (probability > 0.5)
                ++chosen,
                log7(sts, course[courseID]->name), student[sts]->status += 6;
            else
            {
                log8(sts, course[courseID]->name);

                if (ATD_TUT_C3 == student[sts]->status)
                {
                    log9(sts), student[sts]->status = STUD_EXIT;
                    continue;
                }

                (ATD_TUT_C1 == student[sts]->status)?
                    log11(sts, course[student[sts]->pref1]->name, course[student[sts]->pref2]->name):
                    log11(sts, course[student[sts]->pref2]->name, course[student[sts]->pref3]->name);

                student[sts]->status += 4;

                if (course[student[sts]->pref2]->status == WTDRN || course[student[sts]->pref3]->status == WTDRN && student[sts]->status)
                    if (student[sts]->status == WAIT_C2)
                        log12(sts, course[student[sts]->pref2]->name, course[student[sts]->pref3]->name), student[sts]->status++;
                    else if (student[sts]->status == WAIT_C3)
                        log13(sts), student[sts]->status = STUD_EXIT;
                    else;
                else;
            }
        }

        log14(currentTA, lab[currentLab]->name, course[courseID]->name);
        lab[currentLab]->TA[currentTA][0] = NT_ALTD;
        course[courseID]->status = WAIT_TA, pthread_cond_broadcast(&checkTA);
    }
    return false;
}

int updateLabStatus()
{
    int notExhausted = 0;
    fo(labs, nLabs)
    {
        if (lab[labs]->status == EXHAUSTED)
            continue;
        notExhausted = 0;
        fo(tas, lab[labs]->TA_num)
            if (lab[labs]->max_times > lab[labs]->TA[tas][1])
            {
                notExhausted = true;
                break;
            }

        if (notExhausted == false)
        {
            pthread_mutex_lock(&lockLab[labs]);
            lab[labs]->status = EXHAUSTED;
            pthread_mutex_unlock(&lockLab[labs]);
            log15(lab[labs]->name);
        }
    }
    return 0;
}

//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q1/colors.h ####################//

#ifndef COLORS_H
#define COLORS_H
/******** COLORS ********/

#define RED "\033[1;31m"
#define GREEN "\033[1;32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#endif

//###########FILE CHANGE ./main_folder/Shubh Agarwal_305958_assignsubmission_file_/2020101131/q1/functions.h ####################//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "q1.h"

int isExit(void);
int updateLabStatus();
void *timerFunc();
void *studentFunc(void *index);
int TAAvailable(int courseNum, int *currentTA, int *currentLab);
void *coursesFunc(void *index);

extern int nCourses, nStudents, nLabs, nCoursesLeft, timeNow;
extern ptr_course_struct *course;
extern ptr_student_struct *student;
extern ptr_lab_struct *lab;
extern pthread_t time_thread;

extern pthread_mutex_t lockStudent, lockCheckTA, *lockLab;
extern pthread_cond_t checkTime, checkTA;

#endif
