
//###########FILE CHANGE ./main_folder/NACHIKET PATIL_305960_assignsubmission_file_/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

int numStudents, numLabs, numCourses;

pthread_t *studentThreads, *courseThreads;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
// pthread_cond_t c = PTHREAD_COND_INITIALIZER;


typedef struct Course {
    int active;
    char *courseName;
    double interestQuotient;
    int slots;
    int maxCourseSlots;
    int numLabs;
    int *LabIDs;
    int currTA;
    int currLab;
} Course;
Course *courses;

typedef struct stu {
    double calibre;
    int courseIDs[3];
    int registerTime;
    int active;
    int currPref;
    int currCourse;
} Student;
Student *students;

typedef struct lab {
    char *labName;
    int numTAs;
    int maxNumCanTA; // number of times a member of lab can TA
    struct _TA {
        enum stat {busy, available} Status;
        int remainder;
        int courseID;
    } *TA;
} Lab;
Lab *Labs;

int changePreferences(int i) {
    int next_prior = 3;
    int currentPreference = students[i].currPref;
    switch (currentPreference)
    {
    case 0: printf(BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)" RESET "\n", 
                        i, courses[students[i].courseIDs[0]].courseName, courses[students[i].courseIDs[1]].courseName);
            fflush(stdout);
            if (students[i].courseIDs[1] != -1 && 
                courses[students[i].courseIDs[1]].active) {
                next_prior = 1;
            } else {
                printf(BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)" RESET "\n", 
                        i, courses[students[i].courseIDs[1]].courseName, courses[students[i].courseIDs[2]].courseName);
                fflush(stdout);
                if (students[i].courseIDs[2] != -1 && 
                    courses[students[i].courseIDs[2]].active) {
                    next_prior = 2;
                }
            }
        break;
    case 1: printf(BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)" RESET "\n", 
                        i, courses[students[i].courseIDs[1]].courseName, courses[students[i].courseIDs[2]].courseName);
            fflush(stdout);
            if (students[i].courseIDs[2] != -1 && 
                courses[students[i].courseIDs[2]].active) {
                next_prior = 2;
            }
        break;
    }
    pthread_mutex_lock(&lock);
    students[i].courseIDs[currentPreference] = -1;
    if (next_prior == 3) {
        printf(RED "Student %d could not get any of his preferred courses" RESET "\n"); fflush(stdout);
        students[i].active = 0;
        // pthread_cond_signal(&c);
    }
    pthread_mutex_unlock(&lock);
    return next_prior;
}

void deleteCourse(int ID) {
    pthread_mutex_lock(&lock);
    courses[ID].active = 0;
    pthread_mutex_unlock(&lock);
    for (int i = 0; i < numStudents; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (students[i].active && students[i].courseIDs[j] == ID) {
                if (students[i].currPref == j)
                    changePreferences(i);
                pthread_mutex_lock(&lock);
                students[i].courseIDs[j] = -1;
                pthread_mutex_unlock(&lock);
                break;
            }
        }
    }
}

void* activateStudent(void *studentID) {
    int ID = *((int*)studentID);
    Student *curr = &students[ID];
    sleep(curr->registerTime);
    // printf(RED "THREAD ID: " YELLOW "%lu" RESET "\n", pthread_self());
    if (curr->active == 1) 
        return (void*)0;
    pthread_mutex_lock(&lock);
    curr->active = 1;
    pthread_mutex_unlock(&lock);
    printf(GREEN "Student %d has filled in preferences for course registration" RESET "\n", ID);
    fflush(stdout);
    return (void*)0;
}

void* courseInit(void *CourseID) {
    int ID = *((int*)CourseID);
    Course *curr = &courses[ID];
    while (1) {

        int needed = 0;
        for (int i = 0; i < numStudents; ++i) {
            if (students[i].active != 1) continue;
            for (int j = 0; j < 3; ++j) {
                if (students[i].courseIDs[j] != -1) {
                    needed = 1; break;
                }
            }
        }
        if (needed = 0) 
            break;
            
        // printf(RED "THREAD ID: " YELLOW "%lu" RESET "\n", pthread_self());
        int TAfound = 0, TApossible = 0;

        if (curr->slots == -1) {
            return (void*)0;
        }

        pthread_mutex_lock(&lock);
        curr->slots = 1 + (rand() % (curr->maxCourseSlots));
        printf(MAGENTA "Course %s has been allocated %d seats" RESET "\n", curr->courseName, curr->slots);
        fflush(stdout);
        curr->currTA = -1;
        curr->currLab = -1;
        pthread_mutex_unlock(&lock);

        do {
            TApossible = 0;
            for (int i = 0; i < curr->numLabs; ++i) {
                int labID = curr->LabIDs[i];
                int max_TA_ships = Labs[labID].maxNumCanTA;
                char *currLabName = Labs[labID].labName;
                for (int j = 0; j < Labs[labID].numTAs; ++j) {
                    struct _TA *currTA = &(Labs[labID].TA[j]);
                    
                    if (currTA->remainder > 0) 
                        TApossible = 1;
                    // TA is available and has chances remaining to be a TA
                    if (currTA->Status && currTA->remainder > 0) {
                        pthread_mutex_lock(&lock);
                        --(currTA->remainder);
                        currTA->Status = busy;
                        currTA->courseID = ID;
                        curr->currTA = j;
                        curr->currLab = labID;
                        switch (max_TA_ships - currTA->remainder)
                        {
                        case 1: printf(GREEN "TA %d from lab %s has been allocated to course %s for his 1st TA ship\n", 
                                        j, 
                                        currLabName, 
                                        curr->courseName);
                            break;
                        case 2: printf(GREEN "TA %d from lab %s has been allocated to course %s for his 2nd TA ship\n", 
                                        j, 
                                        currLabName, 
                                        curr->courseName);
                            break;
                        case 3: printf(GREEN "TA %d from lab %s has been allocated to course %s for his 3rd TA ship\n", 
                                        j, 
                                        currLabName, 
                                        curr->courseName);
                            break;
                        default: printf(GREEN "TA %d from lab %s has been allocated to course %s for his %dth TA ship\n", 
                                        j, 
                                        currLabName, 
                                        curr->courseName, 
                                        (max_TA_ships - currTA->remainder));
                            break;
                        }
                        fflush(stdout);
                        pthread_mutex_unlock(&lock);
                        TAfound = 1;
                        break;
                    }
                }
                if (TAfound) break;
            }
            if (TAfound) break;
        } while (TApossible);

        if (TApossible == 0) {
            deleteCourse(ID);
            return (void*)0;
        } 
        int filled = 0;
        pthread_mutex_lock(&lock);
        for (int i = 0; i < numStudents; ++i) {
            if (students[i].active && students[i].currCourse == -1 && students[i].courseIDs[students[i].currPref] == ID) {
                // pthread_mutex_lock(&lock);
                students[i].currCourse = ID;
                printf("Student %d has been allocated a seat in course %s\n", i, curr->courseName);
                fflush(stdout);
                // pthread_mutex_unlock(&lock);
                filled++;
                if (filled == curr->slots) 
                    break;
            }
        }
        printf(CYAN "Tutorial has started for course %s with %d slots filled out of %d" RESET "\n", curr->courseName, filled, curr->slots);
        fflush(stdout);
        sleep(1);
        printf(CYAN "TA %d from lab %s has completed the tutorial for the course %s" RESET "\n", curr->currTA, Labs[curr->currLab].labName, curr->courseName);
        fflush(stdout);
        // pthread_mutex_lock(&lock);
        Labs[curr->currLab].TA[curr->currTA].courseID = -1; 
        Labs[curr->currLab].TA[curr->currTA].Status = available;
        curr->slots = -1;
        // pthread_mutex_unlock(&lock);
        for (int i = 0; i < numStudents; ++i) {
            if (students[i].courseIDs[students[i].currPref] == ID) {
                double prob = students[i].calibre * curr->interestQuotient;
                double random = ((double)rand() / (double)RAND_MAX);
                // pthread_mutex_lock(&lock);
                students[i].currCourse = -1;
                // pthread_mutex_unlock(&lock);
                if (random <= prob) {
                    // pthread_mutex_lock(&lock);
                    // printf(RED "THREAD ID: " YELLOW "%lu" RESET "\n", pthread_self());
                    printf(YELLOW "Student %d has selected the course %s permanently" RESET "\n", i, curr->courseName);
                    fflush(stdout);
                    students[i].active = 0;
                    // pthread_mutex_unlock(&lock);
                    pthread_mutex_unlock(&lock);
                } else {
                    // printf(RED "THREAD ID: " YELLOW "%lu" RESET "\n", pthread_self());
                    printf(MAGENTA "Student %d has withdrawn from the course %s" RESET "\n", i, curr->courseName);
                    fflush(stdout);
                    pthread_mutex_unlock(&lock);
                    changePreferences(i);
                }
            }
        }
        
    }   
    return (void*)0;
}

int main() {
    srand(time(NULL));
    printf(RED);
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);
    printf(RESET);
    char buffer[1048576]={};

    // course info 
    // Course courses[numCourses];
    courses = (Course*)malloc(sizeof(Course) * numCourses);
    courseThreads = (pthread_t*)malloc(sizeof(pthread_t) * numCourses);
    
    for (int i = 0; i < numCourses; ++i) {
        courses[i].active = 1;
        printf(GREEN);
        scanf("%s %lf %d %d", buffer, &courses[i].interestQuotient, &courses[i].maxCourseSlots, &courses[i].numLabs);
        printf(RESET);
        int nameLen = strlen(buffer);
        courses[i].courseName = strdup(buffer);
        memset(buffer, 0, sizeof(char) * (nameLen + 5));
        courses[i].LabIDs = (int*)malloc(sizeof(int) * courses[i].numLabs);
        for (int j = 0; j < courses[i].numLabs; ++j) {
            printf(GREEN);
            scanf("%d", &(courses[i].LabIDs[j]));
            printf(RESET);
        }
    }

    // Student info
    students = (Student*)malloc(sizeof(Student) * numStudents);
    studentThreads = (pthread_t*)malloc(sizeof(pthread_t) * numStudents);
    
    for (int i = 0; i < numStudents; ++i) {
        printf(BLUE);
        scanf("%lf %d %d %d %d", &students[i].calibre, 
                                 &(students[i].courseIDs[0]), &(students[i].courseIDs[1]), &(students[i].courseIDs[2]), 
                                 &students[i].registerTime);
        printf(RESET);
        students[i].currPref = 0;
        students[i].active = 0;
    }

    // Lab info
    Labs = (Lab*)malloc(sizeof(Lab) * numLabs);
    
    for (int i = 0; i < numLabs; ++i) {
        printf(MAGENTA);
        scanf("%s %d %d", buffer, &Labs[i].numTAs, &Labs[i].maxNumCanTA);
        printf(RESET);
        Labs[i].TA = (struct _TA*)malloc(Labs[i].numTAs * sizeof(struct _TA));
        for (int t = 0; t < Labs[i].numTAs; ++t) {
            Labs[i].TA[t].Status = available;
            Labs[i].TA[t].remainder = Labs[i].maxNumCanTA;
            Labs[i].TA[t].courseID = -1;
        }
        int nameLen = strlen(buffer);
        Labs[i].labName = strdup(buffer);
        memset(buffer, 0, sizeof(char) * (nameLen + 5));
    }
    fflush(stdout);

    for (int i = 0; i < numStudents; ++i) {
        int ID = i;
        pthread_create(&studentThreads[i], NULL, &activateStudent, (void*)&ID);
    }
    for (int i = 0; i < numStudents; ++i) {
        pthread_join(studentThreads[i], NULL);
    }

    for (int i = 0; i < numCourses; ++i) {
        int ID = i;
        pthread_create(&courseThreads[i], NULL, &courseInit, (void*)&ID);
    }

    for (int i = 0; i < numCourses; ++i) {
        pthread_join(courseThreads[i], NULL);
    }

    free(courseThreads); free(studentThreads);
    courseThreads = NULL; studentThreads = NULL;
    for (int i = 0; i < numCourses; ++i) {
        free(courses[i].courseName); courses[i].courseName = NULL;
        free(courses[i].LabIDs); courses[i].LabIDs = NULL;
    } 
    free(courses); courses = NULL;
    free(students); students = NULL;
    for (int i = 0; i < numLabs; ++i) {
        free(Labs[i].labName); Labs[i].labName = NULL;
        free(Labs[i].TA); Labs[i].TA = NULL;
    }
    free(Labs); Labs = NULL;
    
    pthread_mutex_destroy(&lock);
    // pthread_cond_destroy(&c);
    return 0;
}
