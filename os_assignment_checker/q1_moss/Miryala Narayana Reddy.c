
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/student.c ####################//

#include "student.h"

void *simulate_student(void *student_details)
{
    student *student_x = (student *)student_details;

    // register for course
    // wait

    pthread_mutex_lock(&stimer_lock);
    while (stimer != student_x->time)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);
    //   printf("student %d time  = %d\n"RESET_COLOR,student_x->number,stimer);

    printf(BLUE_COLOR "Student %d filled in preferences for course registration\n" RESET_COLOR, student_x->number);
    bool selected_permanently = false;
    if (registered_for_course(student_x->preference_course_1)) // this function will wait till a seat is free to be allocated
    {
        printf(BLUE_COLOR "Student %d has been allocated a seat in course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_1].name);

        // wait for tut to be  over
        pthread_mutex_lock(&course_list[student_x->preference_course_1].tutorial_lock);
        while (course_list[student_x->preference_course_1].tutorial == false)
            pthread_cond_wait(&course_list[student_x->preference_course_1].tutorial_cond, &course_list[student_x->preference_course_1].tutorial_lock);
        pthread_mutex_unlock(&course_list[student_x->preference_course_1].tutorial_lock);
        // end of tut

        student_x->prob = student_x->calibre * course_list[student_x->preference_course_1].interest;
        if (student_x->prob < MIN_PROB_OF_LIKING_COURSE)
        {
            // update course count
            withdrawn_from_course(student_x->preference_course_1);
            printf(BLUE_COLOR "Student %d has withdrawn from course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_1].name);
        }
        else
        {
            printf(BLUE_COLOR "Student %d has selected course %s permanently\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_1].name);
            student_x->preference = student_x->preference_course_1;
            selected_permanently = true;
            student_x->selected_permanently = true;
            pthread_mutex_lock(&course_list[student_x->preference_course_1].course_exit_lock);
            pthread_cond_wait(&course_list[student_x->preference_course_1].course_exit_cond, &course_list[student_x->preference_course_1].course_exit_lock);
            pthread_mutex_unlock(&course_list[student_x->preference_course_1].course_exit_lock);
            printf(CYAN_COLOR "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_1].name, course_list[student_x->preference_course_2].name);
            selected_permanently = false;
            student_x->selected_permanently = false;
        }
    }

    if (!selected_permanently)
    {
        if (registered_for_course(student_x->preference_course_2)) // this function will wait till a seat is free to be allocated
        {
            printf(BLUE_COLOR "Student %d has been allocated a seat in course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_2].name);
            // wait for tut to be  over
            pthread_mutex_lock(&course_list[student_x->preference_course_2].tutorial_lock);
            while (course_list[student_x->preference_course_2].tutorial == false)
                pthread_cond_wait(&course_list[student_x->preference_course_2].tutorial_cond, &course_list[student_x->preference_course_2].tutorial_lock);
            pthread_mutex_unlock(&course_list[student_x->preference_course_2].tutorial_lock);
            // end of tut
            student_x->prob = student_x->calibre * course_list[student_x->preference_course_2].interest;
            if (student_x->prob < MIN_PROB_OF_LIKING_COURSE)
            {
                withdrawn_from_course(student_x->preference_course_2);
                printf(BLUE_COLOR "Student %d has withdrawn from course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_2].name);
            }
            else
            {
                printf(BLUE_COLOR "Student %d has selected course %s permanently\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_2].name);
                student_x->preference = student_x->preference_course_2;
                selected_permanently = true;
                student_x->selected_permanently = true;
                pthread_mutex_lock(&course_list[student_x->preference_course_2].course_exit_lock);
                pthread_cond_wait(&course_list[student_x->preference_course_2].course_exit_cond, &course_list[student_x->preference_course_2].course_exit_lock);
                pthread_mutex_unlock(&course_list[student_x->preference_course_2].course_exit_lock);
                printf(CYAN_COLOR "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_2].name, course_list[student_x->preference_course_3].name);
                selected_permanently = false;
                student_x->selected_permanently = false;
            }
        }
    }

    if (!selected_permanently)
    {
        if (registered_for_course(student_x->preference_course_3)) // this function will wait till a seat is free to be allocated
        {
            printf(BLUE_COLOR "Student %d has been allocated a seat in course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_3].name);
            // wait for tut to be  over
            pthread_mutex_lock(&course_list[student_x->preference_course_3].tutorial_lock);
            while (course_list[student_x->preference_course_3].tutorial == false)
                pthread_cond_wait(&course_list[student_x->preference_course_3].tutorial_cond, &course_list[student_x->preference_course_3].tutorial_lock);
            pthread_mutex_unlock(&course_list[student_x->preference_course_3].tutorial_lock);
            // end of tut
            student_x->prob = student_x->calibre * course_list[student_x->preference_course_3].interest;
            if (student_x->prob < MIN_PROB_OF_LIKING_COURSE)
            {
                withdrawn_from_course(student_x->preference_course_3);
                printf(BLUE_COLOR "Student %d has withdrawn from course %s\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_3].name);
            }
            else
            {
                printf(BLUE_COLOR "Student %d has selected course %s permanently\n" RESET_COLOR, student_x->number, course_list[student_x->preference_course_3].name);
                student_x->preference = student_x->preference_course_3;
                selected_permanently = true;
                student_x->selected_permanently = true;
                pthread_mutex_lock(&course_list[student_x->preference_course_3].course_exit_lock);
                pthread_cond_wait(&course_list[student_x->preference_course_3].course_exit_cond, &course_list[student_x->preference_course_3].course_exit_lock);
                pthread_mutex_unlock(&course_list[student_x->preference_course_3].course_exit_lock);
                selected_permanently = false;
                student_x->selected_permanently = false;
                // Student i has changed current preference from course_x (priority k) to course_y (priority k+1
            }
        }
    }
    if (!selected_permanently)
    {
        student_x->in_simulation = false;
        printf(BLUE_COLOR "Student %d couldn’t get any of his preferred courses\n" RESET_COLOR, student_x->number);
    }
    // printf("student %d %f %d %d %d %d\n"RESET_COLOR, student_x->number, student_x->calibre,\
    //  student_x->preference_course_1, student_x->preference_course_2, student_x->preference_course_3,\
    //   student_x->time);
    return NULL;
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/lab.h ####################//

#ifndef __LAB_H__
#define __LAB_H__

#include "user.h"

struct ta
{
    int num_courses;
    bool is_free;
    pthread_mutex_t ta_lock;
    pthread_cond_t ta_cond;
};

typedef struct ta ta;

struct lab
{
    int lab_id;
    char name[100];
    int num_students;
    ta *student_ta;
    int num_of_times_TA_limit;
};

typedef struct lab lab;
int num_labs;

extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/stimer.h ####################//

#ifndef __STIMER_H__
#define __STIMER_H__

#include "user.h"


extern pthread_mutex_t stimer_lock ;
extern pthread_cond_t stimer_cond;

extern bool start_clock;
extern pthread_mutex_t start_clock_lock;
extern pthread_cond_t start_clock_cond;

void increament_timer(int arg);
void simulate_timer(int time);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/main.c ####################//

#include "user.h"
#include <signal.h>

int stimer = -1;
pthread_mutex_t stimer_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t stimer_cond = PTHREAD_COND_INITIALIZER;


int main()
{
    init_all_threads();
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/student.h ####################//

#ifndef __STUDENT_H__
#define __STUDENT_H__

#include "user.h"

#define MIN_PROB_OF_LIKING_COURSE 0.25

struct student
{
    int number;
    int time;
    double calibre;
    int preference_course_1;
    int preference_course_2;
    int preference_course_3;
    double prob;
    int current_preference;
    pthread_t tid;
    bool in_simulation;
    int preference;
    bool selected_permanently;
    pthread_mutex_t preference_lock;
    pthread_cond_t preference_cond;
};

typedef struct student student;

int num_students;

extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;

void *simulate_student(void *student_details);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/course.c ####################//

#include "course.h"

bool registered_for_course(int i)
{
    bool flag = false;
    pthread_mutex_lock(&course_list[i].student_cnt_lock);
    if (course_list[i].student_cnt < course_list[i].course_max_slot)
    {
        course_list[i].student_cnt++;
        pthread_cond_signal(&course_list[i].student_cnt_cond);
        flag = true;
    }
    pthread_mutex_unlock(&course_list[i].student_cnt_lock);
    return flag;
}

void withdrawn_from_course(int i)
{
    pthread_mutex_lock(&course_list[i].student_cnt_lock);
    if (course_list[i].student_cnt > 0)
    {
        course_list[i].student_cnt--;
        pthread_cond_signal(&course_list[i].student_cnt_cond);
    }
    pthread_mutex_unlock(&course_list[i].student_cnt_lock);
}

void *simulate_course(void *course_details)
{
    course *course_x = (course *)course_details;

    pthread_mutex_lock(&stimer_lock);
    while (stimer == -1)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);

    printf(GREEN_COLOR "Course %s has been allocated %d seats\n" RESET_COLOR, course_x->name, course_x->course_max_slot);

    pthread_mutex_lock(&course_x->student_cnt_lock);
    while (course_x->student_cnt == 0)
    {
        pthread_cond_wait(&course_x->student_cnt_cond, &course_x->student_cnt_lock);
    }
    pthread_mutex_unlock(&course_x->student_cnt_lock);

    // look for TAs
    int ta_num;
    int ta_lab;
    while (true)
    {
        // bool new_student_added = false;
        bool found_ta = false;

        // int num_stds = course_x->student_cnt;
        for (int i = 0; i < course_x->num_labs; i++)
        {
            lab *lab_x = &lab_list[i];
            for (int j = 0; j < lab_x->num_students; j++)
            {
                // use semaphore / mutex locks or thread to update the ta allocation
                pthread_mutex_lock(&lab_x->student_ta[j].ta_lock);
                if (lab_x->student_ta[j].is_free && (lab_x->student_ta[j].num_courses < lab_x->num_of_times_TA_limit))
                {
                    lab_x->student_ta[j].is_free = false;
                    lab_x->student_ta[j].num_courses++;
                    found_ta = true;
                    ta_num = j;
                    ta_lab = i;
                    printf(PINK_COLOR "TA %d from lab %s has been allocated to course %s for his TA ship number  %d\n" RESET_COLOR, j, lab_x->name, course_x->name, lab_x->student_ta[j].num_courses);
                    pthread_mutex_unlock(&lab_x->student_ta[j].ta_lock);
                    break;
                }
                pthread_mutex_unlock(&lab_x->student_ta[j].ta_lock);
            }
        }
        if (!found_ta)
        {
            // have to do some more work.....
            course_x->in_simulation = false;
            printf(RED_COLOR "Course %s does not have any TA mentors eligible and is removed from course offerings\n" RESET_COLOR, course_x->name);
            pthread_mutex_lock(&course_x->course_exit_lock);
            pthread_cond_broadcast(&course_x->course_exit_cond);
            pthread_mutex_unlock(&course_x->course_exit_lock);
            // remove the course from the list and exit thread
            break;
        }

        // conduct tutorial
        pthread_mutex_lock(&course_x->tutorial_lock);
        course_x->tutorial = true;
        pthread_cond_broadcast(&course_x->tutorial_cond);
        printf(PINK_COLOR "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET_COLOR, course_x->name, course_x->student_cnt, course_x->course_max_slot);
        sleep(1);
        lab_list[ta_lab].student_ta[ta_num].is_free = true;
        pthread_mutex_unlock(&course_x->tutorial_lock);
        printf(PINK_COLOR "TA %d from lab %s has completed the tutorial for the course %s\n" RESET_COLOR, ta_num, lab_list[ta_lab].name, course_x->name);

        pthread_mutex_lock(&course_x->student_cnt_lock);
        pthread_cond_wait(&course_x->student_cnt_cond, &course_x->student_cnt_lock);
        pthread_mutex_unlock(&course_x->student_cnt_lock);
    }
    // allow students to register
    // printf(GREEN_COLOR"course %s %f %d %d\n"RESET_COLOR, course_x->name, course_x->interest, course_x->num_labs, course_x->course_max_slot);

    // conduct tutorial.
}
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/course.h ####################//

#ifndef __COURSE_H__
#define __COURSE_H__

#include "user.h"

struct course
{
    char name[100];
    double interest;
    int num_labs;
    int lab_ids[50];
    int course_max_slot;

    pthread_t tid;

    int student_cnt;
    pthread_mutex_t student_cnt_lock;
    pthread_cond_t student_cnt_cond;

    int tutorial;
    pthread_mutex_t tutorial_lock;
    pthread_cond_t tutorial_cond;

    bool in_simulation;
    pthread_mutex_t course_exit_lock;
    pthread_cond_t course_exit_cond;

};

typedef struct course course;

int num_courses;

extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;

void *simulate_course(void *course_details);
bool registered_for_course(int i);
void withdrawn_from_course(int i);

#endif

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/stimer.c ####################//

#include "stimer.h"
#include <signal.h>

void increament_timer(int arg)
{
    pthread_mutex_lock(&stimer_lock);
    stimer++;
    pthread_cond_broadcast(&stimer_cond);
    pthread_mutex_unlock(&stimer_lock);
    // printf("timer  = %d\n", stimer);
}

void simulate_timer(int time)
{
    signal(SIGALRM, increament_timer);
    int cnt = 0;
    while (stimer <= time)
    {
        alarm(1);
        pause();
        for (int i = 0; i < num_students; i++)
        {
            if ((!student_list[i].in_simulation) || student_list[i].selected_permanently)
            {
                cnt++;
            }
        }
        if (cnt == num_students)
        {
            sleep(2);
            cnt = 2;
            for (int i = 0; i < num_students; i++)
            {
                if ((!student_list[i].in_simulation) || student_list[i].selected_permanently)
                {
                    cnt++;
                }
            }
            break;
        }
        else
        {
            cnt = 0;
        }
    }
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/user.c ####################//

#include "user.h"

void init_all_threads()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);

    course_list = (course *)(malloc(sizeof(course) * num_courses));
    student_list = (student *)(malloc(sizeof(student) * num_students));
    lab_list = (lab *)(malloc(sizeof(lab) * num_labs));

    for (int i = 0; i < num_courses; i++)
    {
        pthread_mutex_init(&course_list[i].student_cnt_lock, NULL);
        pthread_cond_init(&course_list[i].student_cnt_cond, NULL);
        pthread_mutex_init(&course_list[i].tutorial_lock, NULL);
        pthread_cond_init(&course_list[i].tutorial_cond, NULL);
        scanf("%s %lf %d %d", course_list[i].name, &course_list[i].interest, &course_list[i].course_max_slot, &course_list[i].num_labs);
        for (int j = 0; j < course_list[i].num_labs; j++)
        {
            scanf("%d", &course_list[i].lab_ids[j]);
        }
        pthread_create(&course_list[i].tid, NULL, simulate_course, (void *)(&course_list[i]));
    }
    int max_time = 0;
    for (int i = 0; i < num_students; i++)
    {
        scanf("%lf %d %d %d %d", &student_list[i].calibre, &student_list[i].preference_course_1, &student_list[i].preference_course_2, &student_list[i].preference_course_3, &student_list[i].time);
        student_list[i].number = i;
        student_list[i].in_simulation = true;
        student_list[i].selected_permanently = false;
        pthread_create(&student_list[i].tid, NULL, simulate_student, (void *)(&student_list[i]));
        if (max_time < student_list[i].time)
        {
            max_time = student_list[i].time;
        }
    }

    for (int i = 0; i < num_labs; i++)
    {
        scanf("%s %d %d", lab_list[i].name, &lab_list[i].num_students, &lab_list[i].num_of_times_TA_limit);

        lab_list[i].student_ta = (ta *)malloc(sizeof(ta) * lab_list[i].num_students);
        for (int j = 0; j < lab_list[i].num_students; j++)
        {
            pthread_mutex_init(&lab_list[i].student_ta[j].ta_lock,NULL);
            pthread_cond_init(&lab_list[i].student_ta[j].ta_cond,NULL);
            lab_list[i].student_ta[j].num_courses = 0;
            lab_list[i].student_ta[j].is_free = true;
        }
    }

    simulate_timer(max_time*1.5);
    printf(GREEN_COLOR "exiting simulation...\n" RESET_COLOR);
    raise(SIGINT);
    // for (int i = 0; i < num_courses; i++)
    // {
    //     pthread_join(course_list[i].tid, NULL);
    // }
    // for (int i = 0; i < num_students; i++)
    // {
    //     pthread_join(student_list[i].tid, NULL);
    // }
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/lab.c ####################//

#include "lab.h"

// not used
void *simulate_lab(void *lab_details)
{
    lab *lab_x = (lab *)lab_details;
    //  students allocated as TAs to different courses

    printf("lab %s %d %d\n", lab_x->name, lab_x->num_students, lab_x->num_of_times_TA_limit);
}
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q1/user.h ####################//

#ifndef __USER_H__
#define __USER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

#include "student.h"
#include "course.h"
#include "lab.h"
#include "stimer.h"

#define RED_COLOR    "\x1b[31m"
#define GREEN_COLOR  "\x1b[32m"
#define YELLOW_COLOR "\x1b[33m"
#define BLUE_COLOR   "\x1b[34m"
#define PINK_COLOR   "\x1b[35m"
#define CYAN_COLOR   "\x1b[36m"
#define RESET_COLOR  "\x1b[0m"

extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;
struct course *course_list;
struct student *student_list;
struct lab *lab_list;

void init_all_threads();

#endif