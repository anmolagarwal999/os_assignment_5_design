
//###########FILE CHANGE ./main_folder/Dheeraja Rajreddygari_305824_assignsubmission_file_/q1/q1.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef struct course {
	char name[200];
	int course_id;
	float interest;
	int num_labs;
	int * labs_;
	int cur_ta_lab;
	int cur_ta_num;
	int course_max_slot;

	int students_interested;
	pthread_mutex_t students_interested_lock;

	int slots_available;
	pthread_cond_t slots_available_cond;
	pthread_mutex_t slots_available_lock;

	pthread_cond_t begin_tut_cond;
	pthread_mutex_t begin_tut_lock;

	int tut_ended;
	pthread_cond_t end_tut_cond;
	pthread_mutex_t end_tut_lock;
	// int withdrawn;
} course_t;

typedef struct student {
	int student_id;
	float calibre;
	int time_arrive;
	int course_pref[3];
	int cur_pref; // init to 0
} student_t;

typedef struct lab {
	char name[200];
	int num_mentors;
	int * mentor_state;
	int * num_tuts;
	int max_tuts;
	int mentors_available;
} lab_t;

int num_students, num_courses, num_labs;
student_t * students;
course_t * courses;
lab_t * labs_;
pthread_mutex_t * lab_lock;
// pthread_mutex_t * courses_lock;

void * simulate_student(void * s) {
	// int student_num = *((int*)s);
	// student_t cur_student = students[student_num];
	student_t cur_student = *((student_t*)s);
	int student_num = cur_student.student_id;
	
	// student fills in pref
	sleep(cur_student.time_arrive);

	printf("Student %d has filled in preferences for course registration.\n", student_num);

	while(cur_student.cur_pref < 3)
	{
		pthread_mutex_lock(&courses[cur_student.cur_pref].slots_available_lock);
		// course withdrawn, go to next pref
		if(courses[cur_student.cur_pref].slots_available == -1) 
		{
			pthread_mutex_unlock(&courses[cur_student.cur_pref].slots_available_lock);
			cur_student.cur_pref++;

			if(cur_student.cur_pref == 1 || cur_student.cur_pref == 2)
				printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d).\n", 
					student_num, courses[cur_student.cur_pref - 1].name, cur_student.cur_pref, 
					courses[cur_student.cur_pref].name, cur_student.cur_pref + 1);
		
			continue;
		}

		// course not withdrawn
		pthread_mutex_lock(&courses[cur_student.cur_pref].students_interested_lock);
		courses[cur_student.cur_pref].students_interested++;
		pthread_mutex_unlock(&courses[cur_student.cur_pref].students_interested_lock);

		while(courses[cur_student.cur_pref].slots_available == 0)
			pthread_cond_wait(&courses[cur_student.cur_pref].slots_available_cond, &courses[cur_student.cur_pref].slots_available_lock);
		
		pthread_mutex_lock(&courses[cur_student.cur_pref].students_interested_lock);
		courses[cur_student.cur_pref].students_interested--;
		pthread_mutex_unlock(&courses[cur_student.cur_pref].students_interested_lock);

		if(courses[cur_student.cur_pref].slots_available == -1) 
		{
			pthread_mutex_unlock(&courses[cur_student.cur_pref].slots_available_lock);
			cur_student.cur_pref++;

			if(cur_student.cur_pref == 1 || cur_student.cur_pref == 2)
				printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d).\n", 
					student_num, courses[cur_student.cur_pref - 1].name, cur_student.cur_pref, 
					courses[cur_student.cur_pref].name, cur_student.cur_pref + 1);	
		
			continue;
		}

		courses[cur_student.cur_pref].slots_available--;
		pthread_mutex_unlock(&courses[cur_student.cur_pref].slots_available_lock);

		printf("Student %d has been allocated a seat in course %s.\n", student_num, courses[cur_student.cur_pref].name);
	
		// signal start tut
		pthread_mutex_lock(&courses[cur_student.cur_pref].begin_tut_lock);
		pthread_cond_signal(&courses[cur_student.cur_pref].begin_tut_cond);
		pthread_mutex_unlock(&courses[cur_student.cur_pref].begin_tut_lock);

		// wait for tut end
		pthread_mutex_lock(&courses[cur_student.cur_pref].end_tut_lock);
		while(courses[cur_student.cur_pref].tut_ended == 0)
			pthread_cond_wait(&courses[cur_student.cur_pref].end_tut_cond, &courses[cur_student.cur_pref].end_tut_lock);
		pthread_mutex_unlock(&courses[cur_student.cur_pref].end_tut_lock);
		
		// accept or reject course
		float prob_accept = courses[cur_student.cur_pref].interest * cur_student.calibre;
		float prob = (float)(rand()%1001) / (float)1000;
		int accept;
		if(prob <= prob_accept)
		{
			printf("Student %d has selected course %s permanently.\n", student_num, courses[cur_student.cur_pref].name);
			return NULL;
		}
		else 
		{
			// wthdraw from course
			printf("Student %d has withdrawn from course %s.\n", student_num, courses[cur_student.cur_pref].name);
		
			cur_student.cur_pref++;
			if(cur_student.cur_pref == 1 || cur_student.cur_pref == 2)
				printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d).\n", 
					student_num, courses[cur_student.cur_pref - 1].name, cur_student.cur_pref, 
					courses[cur_student.cur_pref].name, cur_student.cur_pref + 1);
		
			continue;	
		}
	}

	// student did not get any preference
	printf("Student %d couldn’t get any of his preferred courses.\n", student_num);
	return NULL; // ig?
}

void * simulate_course(void *s) {
	course_t cur_course = *((course_t*) s);
	int course_num = cur_course.course_id;

	while(1)
	{
		// find mentor
		courses[course_num].cur_ta_lab = courses[course_num].cur_ta_num = -1;
		while(courses[course_num].cur_ta_lab == -1)
		{
			int labs_available = 0;
			for(int lab = 0; courses[course_num].cur_ta_lab == -1 && lab < courses[course_num].num_labs; lab++)
			{
				pthread_mutex_lock(&lab_lock[lab]);
				if(labs_[lab].mentors_available == 0) 
				{
					pthread_mutex_unlock(&lab_lock[lab]);
					continue;
				}
				labs_available++;
				for(int mentor = 0; mentor < labs_[lab].num_mentors; mentor++)
				{
					if(labs_[lab].mentor_state[mentor] == -1 && labs_[lab].num_tuts[mentor] < labs_[lab].max_tuts)
					{
						// mentor found
						courses[course_num].cur_ta_lab = lab;
						courses[course_num].cur_ta_num = mentor;
						labs_[lab].mentor_state[mentor] = course_num;
						labs_[lab].num_tuts[mentor]++;

						printf("TA %d from lab %s has been allocated to course %s for %d-th TA ship.\n", 
							mentor, labs_[lab].name, courses[course_num].name, labs_[lab].num_tuts[mentor]);
					
						if(labs_[lab].num_tuts[mentor] == labs_[lab].max_tuts)
						{	
							labs_[lab].mentors_available--;
							if(labs_[lab].mentors_available == 0)
							printf("Lab %s no longer has students available for TA ship.\n", labs_[lab].name);
						}
						break;
					}
				}
				pthread_mutex_unlock(&lab_lock[lab]);
			}

			if(labs_available == 0)
			{
				// withdaw, set something to -1
				pthread_mutex_lock(&courses[course_num].slots_available_lock);
				pthread_mutex_lock(&courses[course_num].students_interested_lock);

				courses[course_num].slots_available = -1;
				// for(int i=0; i < courses[course_num].students_interested; i++)
				// 	pthread_cond_signal(&courses[course_num].slots_available_cond);
				pthread_cond_broadcast(&courses[course_num].slots_available_cond);

				pthread_mutex_unlock(&courses[course_num].students_interested_lock);
				pthread_mutex_unlock(&courses[course_num].slots_available_lock);

				printf("Course %s doesn't have any TA's eligible and is removed from course offerings.\n", courses[course_num].name);
				return NULL;
			}
			sleep(1);
		}

		// ta found
		pthread_mutex_lock(&courses[course_num].end_tut_lock);
		courses[course_num].tut_ended = 0;
		pthread_mutex_unlock(&courses[course_num].end_tut_lock);

		// schedule tuts
		pthread_mutex_lock(&courses[course_num].slots_available_lock);
		int num_slots = courses[course_num].slots_available = (rand()%courses[course_num].course_max_slot) + 1;
		pthread_mutex_unlock(&courses[course_num].slots_available_lock);

		printf("Course %s has been allocated %d seats.\n", courses[course_num].name, num_slots);
	
		while(1)
		{
			pthread_mutex_lock(&courses[course_num].slots_available_lock);
			pthread_mutex_lock(&courses[course_num].students_interested_lock);

			if(courses[course_num].slots_available > 0 && courses[course_num].students_interested > 0)
			{
				pthread_mutex_unlock(&courses[course_num].students_interested_lock);
				pthread_cond_signal(&courses[course_num].slots_available_cond);
				pthread_mutex_unlock(&courses[course_num].slots_available_lock);

				// wait for student to signal
				pthread_mutex_lock(&courses[course_num].begin_tut_lock);
				pthread_cond_wait(&courses[course_num].begin_tut_cond, &courses[course_num].begin_tut_lock);
				pthread_mutex_unlock(&courses[course_num].begin_tut_lock);
			}
			else 
			{
				pthread_mutex_unlock(&courses[course_num].students_interested_lock);
				pthread_mutex_unlock(&courses[course_num].slots_available_lock);
				break;			
			}
		}

		// make sure new students dont register now
		pthread_mutex_lock(&courses[course_num].slots_available_lock);

		// all available students are here, begin tut
		int slots_filled = num_slots - courses[course_num].slots_available;

		printf("Tutorial has started for Course %s with %d seats filled out of %d.\n", 
			courses[course_num].name, slots_filled, num_slots);
	
		sleep(2);

		// tut done, signal students		
		printf("TA %d from lab %s has completed the tutorial and left the course %s.\n", 
			courses[course_num].cur_ta_num, labs_[courses[course_num].cur_ta_lab].name, courses[course_num].name);
	
		pthread_mutex_lock(&courses[course_num].end_tut_lock);
		courses[course_num].tut_ended = 1;
		for(int i=0; i < slots_filled; i++) 
		{
			pthread_cond_signal(&courses[course_num].end_tut_cond);
		}
		pthread_mutex_unlock(&courses[course_num].end_tut_lock);

		courses[course_num].slots_available = 0;
		pthread_mutex_unlock(&courses[course_num].slots_available_lock);
	}
}

int main() {
	// take input
	scanf("%d %d %d", &num_students, &num_labs, &num_courses);

	courses = (course_t*) malloc(num_courses * sizeof(course_t));
	students = (student_t*) malloc(num_students * sizeof(student_t));
	labs_ = (lab_t*) malloc(num_labs * sizeof(lab_t));
	lab_lock = (pthread_mutex_t*) malloc(num_labs * sizeof(pthread_mutex_t));

	if(courses == NULL || students == NULL || labs_ == NULL || lab_lock == NULL)
	{
		printf("Error: malloc failed\n");
		return 0;
	}

	// input course 
	for(int i = 0; i < num_courses; i++)
	{
		scanf(" %s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].course_max_slot, &courses[i].num_labs);
		courses[i].labs_ = (int *) malloc(courses[i].num_labs * sizeof(int));
		if(courses[i].labs_ == NULL)
		{
			printf("Error: malloc failed\n");
			return 0;
		}

		for(int j = 0; j < courses[i].num_labs; j++)
		scanf(" %d", &courses[i].labs_[j]);

		courses[i].cur_ta_lab = courses[i].cur_ta_num = -1;

		courses[i].students_interested = 0;
		pthread_mutex_init(&courses[i].students_interested_lock, NULL);

		courses[i].slots_available = 0;
		pthread_cond_init(&courses[i].slots_available_cond, NULL);
		pthread_mutex_init(&courses[i].slots_available_lock, NULL);

		pthread_cond_init(&courses[i].begin_tut_cond, NULL);
		pthread_mutex_init(&courses[i].begin_tut_lock, NULL);

		courses[i].tut_ended = 0;
		pthread_cond_init(&courses[i].end_tut_cond, NULL);
		pthread_mutex_init(&courses[i].end_tut_lock, NULL);
	}

	// input student
	for(int i = 0; i < num_students; i++)
	{
		scanf("%f", &students[i].calibre);
		scanf(" %d %d %d", &students[i].course_pref[0], &students[i].course_pref[1], &students[i].course_pref[2]);
		scanf(" %d", &students[i].time_arrive);
		students[i].cur_pref = 0;
	}

	// input lab
	for(int i = 0; i < num_labs; i++)
	{
		scanf("%s %d %d", labs_[i].name, &labs_[i].num_mentors, &labs_[i].max_tuts);

		labs_[i].mentors_available = labs_[i].num_mentors;
		labs_[i].mentor_state = (int *) malloc(labs_[i].num_mentors * sizeof(int));
		labs_[i].num_tuts = (int *) malloc(labs_[i].num_mentors * sizeof(int));

		if(labs_[i].mentor_state == NULL || labs_[i].num_tuts == NULL)
		{
			printf("Error: malloc failed\n");
			return 0;
		}

		for(int j = 0; j < labs_[i].num_mentors; j++)
		{
			labs_[i].mentor_state[j] = -1;
			labs_[i].num_tuts[j] = 0;
		}
	}

	// make threads
	pthread_t tid_courses[num_courses], tid_students[num_students];
	
	for(int i=0; i<num_students; i++)
	{
		students[i].student_id = i;
		pthread_create(&tid_students[i], NULL, simulate_student, (void*)&students[i]);
	}

	for(int i=0; i<num_courses; i++)
	{
		courses[i].course_id = i;
		pthread_create(&tid_courses[i], NULL, simulate_course, (void*)&courses[i]);
	}

	// wait for all
	for(int i=0; i<num_students; i++)
		pthread_join(tid_students[i], NULL);

	for(int i=0; i<num_courses; i++)
		pthread_join(tid_courses[i], NULL);

	return 0;
}