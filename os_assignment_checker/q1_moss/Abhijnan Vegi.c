
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/globals.h ####################//

#if !defined(CP_GLOBALS_H)
#define CP_GLOBALS_H

#include "structs.h"

extern lab *ilabs;
extern course *courses;
extern student *students;

extern int lab_c;
extern int course_c;
extern int student_c;

extern pthread_mutex_t stu_lock;

#endif // CP_GLOBALS_H

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/sims.h ####################//

#if !defined(CP_SIMS_H)
#define CP_SIMS_H

void *course_sim(void *course_arg);
void *student_sim(void *stu_arg);

#endif // CP_SIMS_H

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>

#include "globals.h"
#include "init.h"
#include "sims.h"

lab *ilabs;
course *courses;
student *students;

int lab_c;
int course_c;
int student_c;

pthread_mutex_t stu_lock;

void init_glob()
{
    scanf("%d %d %d", &student_c, &lab_c, &course_c);
    ilabs = (lab *)malloc(sizeof(lab) * lab_c);
    courses = (course *)malloc(sizeof(course) * course_c);
    students = (student *)malloc(sizeof(student) * student_c);

    for (int i = 0; i < course_c; i++)
    {
        char name[128];
        double interest;
        int max_slot_c;
        int lab_c;
        scanf("%s %lf %d %d", name, &interest, &max_slot_c, &lab_c);
        int *lab_ids = (int *)malloc(sizeof(int) * lab_c);
        for (int j = 0; j < lab_c; j++)
        {
            scanf("%d", &lab_ids[j]);
        }
        init_course(&courses[i], name, interest, max_slot_c, lab_c, lab_ids);
    }

    for (int i = 0; i < student_c; i++)
    {
        double calibre;
        int *prefs = (int *)malloc(sizeof(int) * 3);
        int apply_time;
        scanf("%lf %d %d %d %d", &calibre, &prefs[0], &prefs[1], &prefs[2], &apply_time);
        init_student(&students[i], calibre, prefs, apply_time);
    }

    pthread_mutex_init(&stu_lock, NULL);

    for (int i = 0; i < lab_c; i++)
    {
        char name[128];
        int ta_c, ta_times;

        scanf("%s %d %d", name, &ta_c, &ta_times);
        init_lab(&ilabs[i], name, ta_c, ta_times);
    }
}

void start_sim()
{
    pthread_t stu_threads[student_c];
    // Start students sims
    for (int i = 0; i < student_c; i++)
    {
        pthread_create(&stu_threads[i], NULL, student_sim, &students[i]);
    }
    
    pthread_t course_threads[course_c];
    // Start course sims
    for (int i = 0; i < course_c; i++)
    {
        pthread_create(&course_threads[i], NULL, course_sim, &courses[i]);
    }

    // Wait for all threads to finish
    for (int i = 0; i < student_c; i++)
    {
        pthread_join(stu_threads[i], NULL);
    }
    printf("All students have exited the simulation\n");
    for (int i = 0; i < course_c; i++)
    {
        pthread_cancel(course_threads[i]);
    }
}

int main()
{
    init_glob();
    printf("🚀 Starting simulation\n");
    start_sim();
    printf("🎊 Simulation complete\n");

    return 0;
}
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/init.h ####################//

#if !defined(INIT_H)
#define INIT_H

void init_ta(struct ta *ta);
void init_lab(struct lab *lab, char *name, int ta_c, int ta_times);
void init_course(
    course *course,
    char *name,
    double interest,
    int max_slot_c,
    int lab_c,
    int *labs
);

void init_student(
    student *student,
    double calibre,
    int* prefs,
    int apply_time
);

#endif // INIT_H

//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/sims.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#include "globals.h"
#include "structs.h"

void *course_sim(void *course_arg)
{
    course *c = (course *)course_arg;
    int lab_no = 0, ta_no = 0;
search:;
    // Wait for students to register for course
    while (!c->stu_wait_c);
    // Search for TAs
    bool has_tas = true;
    while (has_tas)
    {
        has_tas = false;
        for (int i = 0; i < c->lab_c; i++)
        {
            if (ilabs[c->labs[i]].ta_avail_c > 0)
            {
                has_tas = true;
                for (int j = 0;j < ilabs[c->labs[i]].ta_c; j++)
                {
                    int ret = pthread_mutex_trylock(&(ilabs[c->labs[i]].tas[j].ta_lock));
                    if (!ret)
                    {
                        lab_no = c->labs[i];
                        ta_no = j;
                        goto found;
                    }
                }
            }
        }
    }
    // If no TAs will be available withdraw
    if (!has_tas)
    {
        c->withdrawn = true;
        pthread_mutex_lock(&c->course_lock);
        pthread_cond_broadcast(&c->course_cond);
        pthread_mutex_unlock(&c->course_lock);
        printf(RED"Course %s doesn't have any TA's eligible and is removed from course offerings\n"RESET, c->name);
        pthread_exit(NULL);
    }

// TA found
found:;
    lab *l = &ilabs[lab_no];
    ta *t = &l->tas[ta_no];
    // Update ta stats
    t->ta_timed++;
    printf(GREEN"TA %d from lab %s has been allocated to course %s for %d TA ship\n"RESET,ta_no,l->name,c->name,t->ta_timed);
    if (t->ta_timed == l->ta_times)
    {
        l->ta_avail_c--;
        if (l->ta_avail_c == 0)
        {
            printf(RED"Lab %s no longer has students available for TA ship\n"RESET, l->name);
        }
    }
    // Allocate seats
    int seats_allocated = rand() % c->max_slot_c + 1;
    c->tut_wait_c = 0;
    printf(BLUE"Course %s has been allocated %d seats\n"RESET, c->name, seats_allocated);

    // Wakeup seats_allocated students
    pthread_mutex_lock(&c->course_lock);
    int available_students = c->stu_wait_c;
    for (int i = 0; i < seats_allocated;i++)
    {
        pthread_cond_signal(&c->course_cond);
    }
    pthread_mutex_unlock(&c->course_lock);
    // Wait for allocated students to join
    while (c->tut_wait_c != seats_allocated && c->tut_wait_c != available_students);

    // Run tutorial
    printf(YELLOW"Tutorial has started for Course %s with %d seats filled out of %d\n"RESET, c->name, c->tut_wait_c, seats_allocated);
    sleep(1);

    // Broadcast end of tutorial
    pthread_mutex_lock(&c->tut_lock);
    printf(YELLOW"TA %d from lab %s has completed the tutorial and the left the course %s\n"RESET, ta_no, l->name, c->name);
    pthread_cond_broadcast(&c->tut_cond);
    pthread_mutex_unlock(&c->tut_lock);


    // Release TA
    if (t->ta_timed != l->ta_times)
    {
        pthread_mutex_unlock(&t->ta_lock);
    }
    goto search;
}

void* student_sim(void* stu_arg)
{
    student *stu = (student *)stu_arg;
    int stu_no = stu - students;

    // Sleep till registration
    sleep(stu->apply_time);

    // Register
    printf("Student %d has filled in preferences for course registration\n", stu_no);

    // Wait for courses
    for (int i = 0; i < 3; i++)
    {
        course *c = &courses[stu->prefs[i]];
        if (c->withdrawn)
        {
            goto change_prio;
        }
        // Register as waiting
        pthread_mutex_lock(&c->course_lock);
        c->stu_wait_c++;

        // Wait for course to be available
        pthread_cond_wait(&c->course_cond, &c->course_lock);
        c->stu_wait_c--;
        // Check again for course withdrawn
        if (c->withdrawn)
        {
            pthread_mutex_unlock(&c->course_lock);
            goto change_prio;
        }
        printf(MAGENTA"Student %d has been allocated a seat in course %s\n"RESET, stu_no, c->name);
        pthread_mutex_unlock(&c->course_lock);

        // Wait for tutorial
        pthread_mutex_lock(&c->tut_lock);
        c->tut_wait_c++;
        pthread_cond_wait(&c->tut_cond, &c->tut_lock);
        pthread_mutex_unlock(&c->tut_lock);


        double prob = c->interest * stu->calibre;
        if (rand() % 100 < prob * 100)
        {
            printf(MAGENTA"Student %d has selected course %s permanently\n"RESET, stu_no, c->name);
            pthread_exit(NULL);
        }
        else 
        {
            goto change_prio;
        }
        change_prio:;
        if ( i != 2)
            printf(CRIMSON"Student %d has changed current preference from course %s (priority %d) to course %s (priority %d)\n"RESET, stu_no, c->name, i, courses[stu->prefs[i + 1]].name, i + 1);
    }
    // Student exits the simulation
    printf(RED"Student %d either didn't get any of their preferred courses or has withdrawn from them and exited the simulation\n"RESET, stu_no);
    pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/structs.h ####################//

#if !defined(CP_STRUCTS_H)
#define CP_STRUCTS_H

#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>

#define BLACK "\x1b[30m"
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define WHITE "\x1b[37m"
#define CRIMSON "\x1b[38m"
#define RESET "\x1b[0m"

typedef struct ta {
    int ta_timed;
    pthread_mutex_t ta_lock;
} ta;

typedef struct lab
{
    char *name;
    int ta_c;
    int ta_times;
    int ta_avail_c;
    ta *tas;
    pthread_mutex_t lab_lock;
} lab;

typedef struct course {
    char *name;
    double interest;
    int max_slot_c;
    int lab_c;
    int *labs;
    int stu_wait_c;
    int tut_wait_c;
    bool withdrawn;
    pthread_mutex_t course_lock;
    pthread_cond_t course_cond;
    pthread_mutex_t tut_lock;
    pthread_cond_t tut_cond;
} course;

typedef struct student {
    double calibre;
    int* prefs;
    int apply_time;
} student;
#endif // CP_STRUCTS_H
//###########FILE CHANGE ./main_folder/Abhijnan Vegi_305793_assignsubmission_file_/q1/init.c ####################//

#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#include "structs.h"
#include "init.h"

void init_ta(struct ta *ta)
{
    ta->ta_timed = 0;
    pthread_mutex_init(&ta->ta_lock, NULL);
}

void init_lab(struct lab *lab, char*name, int ta_c, int ta_times)
{
    lab->name = strdup(name);
    lab->ta_c = ta_c;
    lab->ta_times = ta_times;
    lab->ta_avail_c = ta_c;

    pthread_mutex_init(&lab->lab_lock, NULL);
    
    lab->tas = malloc(sizeof(struct ta) * ta_c);
    for (int i = 0; i < ta_c; i++)
    {
        init_ta(&lab->tas[i]);
    }
}

void init_course(
    course *course,
    char *name,
    double interest,
    int max_slot_c,
    int lab_c,
    int *labs
)
{
    course->name = strdup(name);
    course->interest = interest;
    course->max_slot_c = max_slot_c;
    course->lab_c = lab_c;
    course->labs = labs;
    course->stu_wait_c = 0;
    course->tut_wait_c = 0;
    course->withdrawn = false;

    pthread_mutex_init(&course->course_lock, NULL);
    pthread_cond_init(&course->course_cond, NULL);
    pthread_mutex_init(&course->tut_lock, NULL);
    pthread_cond_init(&course->tut_cond, NULL);
}

void init_student(
    student *student,
    double calibre,
    int* prefs,
    int apply_time
)
{
    student->calibre = calibre;
    student->prefs = prefs;
    student->apply_time = apply_time;
}
