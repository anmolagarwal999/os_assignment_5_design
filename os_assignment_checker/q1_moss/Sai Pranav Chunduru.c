
//###########FILE CHANGE ./main_folder/Sai Pranav Chunduru_305855_assignsubmission_file_/2020101033/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include"q1.h"

#define BLK "\e[1;30m"
#define RED "\e[1;31m"
#define GRN "\e[1;32m"
#define YEL "\e[1;33m"
#define BLU "\e[1;34m"
#define MAG "\e[1;35m"
#define CYN "\e[1;36m"
#define RST "\x1b[0m"

int num_students,num_labs,num_courses;

struct Course
{
    char name[20];
    double interest;
    int max;
    int num_labs;
    int *labs;
};
typedef struct Course Course;
struct Student
{
    double calibre;
    int pref1;
    int pref2;
    int pref3;
    int time;
};
typedef struct Student Student;
struct Lab
{
    char name[20];
    int num_TAs;
    int max_TAs;
};
typedef struct Lab Lab;


int randNum(long int lower, long int upper) {
    return (int) (random() % (upper - lower + 1) + lower);
}

void pthreadCreate(pthread_t *thread, const pthread_attr_t *attr,
                   void *(*start_routine)(void *), void *arg) {
    if (pthread_create(thread, attr,
                       start_routine, arg) != 0) {
        perror("Thread was not created");
    }
}

void pthreadJoin(pthread_t thread, void **retval) {
    if (pthread_join(thread, retval) != 0) {
        perror("Thread could not join");
    }

}

void condWait(pthread_cond_t *cv, pthread_mutex_t *mutex) {
    if (pthread_cond_wait(cv, mutex) != 0) {
        perror("Cant cond wait on cv");
    }
}

void mutexLock(pthread_mutex_t *mutex) {
    fflush(stdout);
    if (pthread_mutex_lock(mutex) != 0) {
        perror("Cant lock mutex");
    }
}

void mutexUnlock(pthread_mutex_t *mutex) {
    fflush(stdout);
    if (pthread_mutex_unlock(mutex) != 0) {
        perror("Cant lock mutex");
    }
}


int main()
{
    
}
//###########FILE CHANGE ./main_folder/Sai Pranav Chunduru_305855_assignsubmission_file_/2020101033/q1/q1.h ####################//

#ifndef _LOL_
#define _LOL_

int num_students,num_labs,num_courses;

struct Course
{
    char name[20];
    double interest;
    int max;
    int num_labs;
    int *labs;
};
typedef struct Course Course;
struct Student
{
    double calibre;
    int pref1;
    int pref2;
    int pref3;
    int time;
};
typedef struct Student Student;
struct Lab
{
    char name[20];
    int num_TAs;
    int max_TAs;
};
typedef struct Lab Lab;

#endif