
//###########FILE CHANGE ./main_folder/BALARAMAKRISHNA P_305961_assignsubmission_file_/2020101024/q1/q1.c ####################//

#include <malloc.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define CourseNameLenght 100
#define LabNameLenght 100
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define EVENT1 300
#define EVENT2 300
#define EVENT3 300
#define EVENT4 300
#define EVENT5 300
#define EVENT6 300
#define EVENT7 300
#define EVENT8 300
#define EVENT9 300
#define EVENT10 300
#define EVENT11 300
#define EVENT12 300

int num_students, num_Labs, num_courses;
int active, exited;
pthread_rwlock_t rwlock; 
pthread_mutex_t exitL, printLock; 

struct lab
{
 char Name[LabNameLenght];
 int N_TAs;
 int Max_Tuts;

 pthread_mutex_t lock; 
 int* TimesTut;
 bool* Available;
 bool Alive;
};
typedef struct lab lab;
lab* Labs;


struct student
{
  float calibre;
  int Preference;
  int Preferences[3];
  int Pid;
  int time;

  bool Modifing;
  int id;
  bool alive;
};
typedef struct student student;
student* students;

struct course
{
  char Name[CourseNameLenght];
  float Interest;
  int max_slots;
  int N_Labs;
  int* AssortedLabs;

  pthread_rwlock_t lock;
  bool Active;
};
typedef struct course course;
course* courses;


void intput();
int SelectTA(int Lab, int* time);
int getTA(int *listOfLabs, int N, int *whichlab, int *whichtime);
void *studentActivation(void *Args);
void Move_NextPref(int index);
int *getStudents(int CourseId, int NSlots, int *No);
void *Tutorial(void *Args); //course index

bool Choise(int stdIndex, int CourseId);
int comps(const void * A, const void *B);

int main()
{
  //Initailizing data
  intput();
  qsort(students, num_students, sizeof(struct student), comps);
  srand(time(0));

  pthread_t Tutorials[num_courses], Activation;
  int Ret, Arguments[num_courses];
  for(int i = 0; i < num_courses; i++)
    Arguments[i] = i;

  sleep(1);
  Ret = pthread_create(&Activation, NULL, studentActivation, NULL);
  assert(Ret == 0);
  
  for(int i = 0; i < num_courses; i++)
  {
    Ret = pthread_create(&Tutorials[i], NULL, Tutorial, (void*)&Arguments[i]);
    assert(Ret == 0);
  }


  //termination
  while(exited != num_students);
  exit(0);
}


void intput()
{
  scanf("%d %d %d", &num_students, &num_Labs, &num_courses);
  courses = (course*)malloc(num_courses*sizeof(struct course));
  students = (student*)malloc(num_students*sizeof(struct student));
  Labs = (lab*)malloc(num_Labs*sizeof(struct lab));
  active = 0;
  exited = 0;

  int temp;
  for(int i = 0; i < num_courses; i++)
  {
    scanf("%s %f %d %d", courses[i].Name, &courses[i].Interest, &courses[i].max_slots, &courses[i].N_Labs);

    courses[i].AssortedLabs = (int *)malloc(courses[i].N_Labs * sizeof(int));
    for(int j = 0; j < courses[i].N_Labs; j++)
      scanf("%d", &courses[i].AssortedLabs[j]);

    courses[i].Active = true;
    temp = pthread_rwlock_init(&courses[i].lock, NULL);
    assert(temp == 0);
  }

  for(int i = 0; i < num_students; i++)
  {
    scanf("%f %d %d %d %d", &students[i].calibre, &students[i].Preferences[0], &students[i].Preferences[1], &students[i].Preferences[2], &students[i].time);
    students[i].Pid = 0;
    students[i].Preference = students[i].Preferences[0];
    students[i].id = i;
    students[i].Modifing = false;
    students[i].alive = true;
  }

  for(int i = 0; i < num_Labs; i++)
  {
    scanf("%s %d %d", Labs[i].Name, &Labs[i].N_TAs, &Labs[i].Max_Tuts);

    Labs[i].TimesTut = (int*)malloc(Labs[i].N_TAs*sizeof(int));
    for(int j = 0; j < Labs[i].N_TAs; j++)
      Labs[i].TimesTut[j] = 0;

    Labs[i].Available = (bool*)malloc(Labs[i].N_TAs*sizeof(bool));
    for(int j = 0; j < Labs[i].N_TAs; j++)
      Labs[i].Available[j] = true;
    
    temp = pthread_mutex_init(&Labs[i].lock, NULL);
    assert(temp == 0);

    if(Labs[i].Max_Tuts > 0 && Labs[i].N_TAs > 0)
      Labs[i].Alive = true;
    else
      Labs[i].Alive = false;
  }

  temp = pthread_rwlock_init(&rwlock, NULL);
  assert(temp == 0);

  temp = pthread_mutex_init(&printLock, NULL);
  assert(temp == 0);

  temp = pthread_mutex_init(&exitL, NULL);
  assert(temp == 0);
}


int SelectTA(int Lab, int *time)
{
  char PrintEvent12[EVENT12];
  int ret = -2;
  pthread_mutex_lock(&Labs[Lab].lock);

  if(Labs[Lab].Alive == false)
  {
    pthread_mutex_unlock(&Labs[Lab].lock);
    return -1;
  }
  
  bool fine = false;
  for(int i = 0; i < Labs[Lab].N_TAs; i++)
    if(Labs[Lab].TimesTut[i] < Labs[Lab].Max_Tuts)
      fine = true;
  if(fine == false)
  {
    Labs[Lab].Alive = false;
    sprintf(PrintEvent12, "Lab %s no longer has students available for TA ship", Labs[Lab].Name);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_BLUE    "%s"  ANSI_COLOR_RESET "\n", PrintEvent12);
    pthread_mutex_unlock(&printLock);
    pthread_mutex_unlock(&Labs[Lab].lock);
    return -1;
  }

  for(int i = 0; i <Labs[Lab].N_TAs; i++)
    if(Labs[Lab].TimesTut[i] < Labs[Lab].Max_Tuts && Labs[Lab].Available[i] == true)
      {
        Labs[Lab].TimesTut[i]++;
        Labs[Lab].Available[i] = false;
        ret = i;
        *time = Labs[Lab].TimesTut[i];
        break;
      }

  pthread_mutex_unlock(&Labs[Lab].lock);
  return ret;
}


int comps(const void * A, const void *B)
{
 // A - B;; //verify
 int a = ((student*)A)->time;
 int b = ((student*)B)->time;
 return a - b;
}


void *studentActivation(void *Args)
{
 int time = 0, N; 
 char ToPrintE_1[EVENT1];
 
 while(active != num_students)  //reading and not wrtting at the same time. (same thread)
 {
    sleep(1);
    time++;

    N = 0;
    for(int i = 0; students[active + i].time == time; i++)
      N++;

    if(N != 0)
    {
      for(int i = 0; i < N; i++)
      {
        sprintf(ToPrintE_1, "Student %d has filled in preferences for course registration", students[active + i].id);
        pthread_mutex_lock(&printLock);
        printf(ANSI_COLOR_RED "%s" ANSI_COLOR_RESET "\n", ToPrintE_1);
        pthread_mutex_unlock(&printLock);
      }
    }

    pthread_rwlock_wrlock(&rwlock);
    active += N;
    pthread_rwlock_unlock(&rwlock);

    int CourseId;
    for(int i = 0; i < N; i++)
    {
      CourseId = students[active - N + i].Preference;  //First course. no two preferences are same.
      pthread_rwlock_rdlock(&courses[CourseId].lock);
      if(courses[CourseId].Active == false)
      {
        students[active - N + i].Modifing = true;
        Move_NextPref(active - N + i);
      }
      pthread_rwlock_unlock(&courses[CourseId].lock);
    }
 }

 return NULL;
}

/* int id, N;
 pthread_rwlock_rdlock(&rwlock);
 N = active;
 pthread_rwlock_unlock(&rwlock);
 for(int i = 0; i < N; i++)
  if(students[i].alive == true && students[i].Preference == id && students[i].Modifing == false);*/


void Move_NextPref(int index)
{
  char PrintE6[EVENT6], PrintE4[EVENT4];
  student *StudI = &students[index];
  int pid = students[index].Pid, CourseId;
  assert(pid <= 2);
  assert(students[index].Modifing == true);

  pid++;
  while(pid <= 2)  
  {
    sprintf(PrintE4, "Student %d has changed current preference from %s(priority %d) to %s(priority %d)", StudI->id, courses[StudI->Preferences[pid -1]].Name, pid -1, courses[StudI->Preferences[pid]].Name, pid);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_BLUE    "%s"  ANSI_COLOR_RESET "\n", PrintE4);
    pthread_mutex_unlock(&printLock);

    CourseId = students[index].Preferences[pid];

    pthread_rwlock_rdlock(&courses[CourseId].lock);
    if(courses[CourseId].Active == true)
    {
      students[index].Preference = CourseId;
      students[index].Pid = pid;
      students[index].Modifing = false;
      pthread_rwlock_unlock(&courses[CourseId].lock);
      return;
    }  
    pthread_rwlock_unlock(&courses[CourseId].lock);
    pid++;
  }

  //exit
  students[index].alive = false;
  students[index].Pid = 10;
  students[index].Modifing = false;

  pthread_mutex_lock(&exitL);
  exited++;
  pthread_mutex_unlock(&exitL);

  sprintf(PrintE6, "Student %d couldn’t get any of his preferred courses", students[index].id);
  pthread_mutex_lock(&printLock);
  printf(ANSI_COLOR_CYAN    "%s"  ANSI_COLOR_RESET "\n", PrintE6);
  pthread_mutex_unlock(&printLock);
}


//Slots store the index of students not id;
void *Tutorial(void *Args) //course index
{
  char ToPrintE3[EVENT3], ToPrintE5[EVENT5], ToPrintE7[EVENT7], ToPrintE8[EVENT8], ToPrintE9[EVENT9], ToPrintE10[EVENT10], ToPrintE11[EVENT11];
  int CourseId = *(int*)Args, TA, fromLab, TimesNow;
  int *Slots, NSlots, N, Filled_slots; 
  sleep(1.2);

  while(1)
  {
    TA = getTA(courses[CourseId].AssortedLabs, courses[CourseId].N_Labs, &fromLab, &TimesNow);
    if(TA == -1)
      break;
    
    sprintf(ToPrintE11, "TA %d from lab %s has been allocated to course %s for %d TA ship", TA, Labs[fromLab].Name, courses[CourseId].Name, TimesNow);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_CYAN    "%s"  ANSI_COLOR_RESET "\n", ToPrintE11);
    pthread_mutex_unlock(&printLock);

    NSlots = rand() % courses[CourseId].max_slots + 1;

    sprintf(ToPrintE7, "Course %s has been allocated %d seats", courses[CourseId].Name, NSlots);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_RED     "%s"  ANSI_COLOR_RESET "\n", ToPrintE7);
    pthread_mutex_unlock(&printLock);

    Slots = getStudents(CourseId, NSlots, &Filled_slots);

    sprintf(ToPrintE8, "Tutorial has started for Course %s with %d seats filled out of %d", courses[CourseId].Name, Filled_slots, NSlots);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_GREEN   "%s"  ANSI_COLOR_RESET "\n", ToPrintE8);
    pthread_mutex_unlock(&printLock);

    sleep(2);

    sprintf(ToPrintE9, "TA %d from lab %s has completed the tutorial and left the course %s", TA, Labs[fromLab].Name, courses[CourseId].Name);
    pthread_mutex_lock(&printLock);
    printf(ANSI_COLOR_YELLOW  "%s"  ANSI_COLOR_RESET "\n", ToPrintE9);
    pthread_mutex_unlock(&printLock);

    Labs[fromLab].Available[TA] = true;

    bool Take;
    for(int i = 0; i < Filled_slots; i++)
    {
      Take = Choise(Slots[i], CourseId);

      if(Take == true)
      {
        students[Slots[i]].alive = false;
        pthread_mutex_lock(&exitL);
        exited++;
        pthread_mutex_unlock(&exitL);

        sprintf(ToPrintE5, "Student %d has selected course %s permanently", students[Slots[i]].id, courses[CourseId].Name);
        pthread_mutex_lock(&printLock);
        printf(ANSI_COLOR_MAGENTA "%s"  ANSI_COLOR_RESET "\n", ToPrintE5);
        pthread_mutex_unlock(&printLock);
      }
      else
      {
        sprintf(ToPrintE3, "Student %d has withdrawn from course %s", students[Slots[i]].id, courses[CourseId].Name);
        pthread_mutex_lock(&printLock);
        printf(ANSI_COLOR_YELLOW  "%s"  ANSI_COLOR_RESET "\n", ToPrintE3);
        pthread_mutex_unlock(&printLock);
         
        Move_NextPref(Slots[i]);
      }
    }

    free(Slots);
  }


  pthread_rwlock_wrlock(&courses[CourseId].lock);
  courses[CourseId].Active = false;
  pthread_rwlock_unlock(&courses[CourseId].lock);

  sprintf(ToPrintE10, "Course %s doesn’t have any TA’s eligible and is removed from course offerings", courses[CourseId].Name);
  pthread_mutex_lock(&printLock);
  printf(ANSI_COLOR_BLUE    "%s"  ANSI_COLOR_RESET "\n", ToPrintE10);
  pthread_mutex_unlock(&printLock);


  int Total;
  Slots = getStudents(CourseId, num_students, &Total);
  for(int i = 0; i < Total; i++)
      Move_NextPref(Slots[i]);

  return NULL;
}


int *getStudents(int CourseId, int NSlots, int *No)
{
    char ToPrintE2[EVENT2];
    int N, filled = 0;
    int *Slots = (int*)malloc(NSlots*sizeof(int));

    pthread_rwlock_rdlock(&rwlock);
    N = active;
    pthread_rwlock_unlock(&rwlock);

    for(int i = 0; i < N && filled <= NSlots; i++)
      if(students[i].alive == true && students[i].Preference == CourseId && students[i].Modifing == false)
      {
        Slots[filled] = i;
        students[i].Modifing = true;
        filled++;
        
        sprintf(ToPrintE2, "Student %d has been allocated a seat in course %s", students[i].id, courses[CourseId].Name);
        pthread_mutex_lock(&printLock);
        printf(ANSI_COLOR_GREEN   "%s"  ANSI_COLOR_RESET "\n", ToPrintE2);
        pthread_mutex_unlock(&printLock);
      }

    *No = filled;
    return Slots;
}

int getTA(int *listOfLabs, int N, int *whichLab, int* whichtime)
{
  bool Fine;
  int TA;
  while (1)
  {
    Fine = false;

    for (int i = 0; i < N; i++)
    {
      TA = SelectTA(listOfLabs[i], whichtime);
      if (TA >= 0)
      {
        *whichLab = listOfLabs[i];
        return TA;
      }

      if(TA == -2)
        Fine = true;
      
      if(TA == -1);
    }

    if(Fine == false)
      return -1;
  }

}


bool Choise(int stdIndex, int CourseId)
{
  float prob = students[stdIndex].calibre * courses[CourseId].Interest;
  int ProbInt = (int)(prob * 100000);
  int randNo = rand() % 100000;

  if(randNo < ProbInt)
    return true;
  else
    return false;
}