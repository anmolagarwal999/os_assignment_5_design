
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

void main()
{
    if(2)
        printf("hello\n");
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/student.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* studentFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int i = *(int*)arg;
    // printf("S%d: %d %d %d\n", Student[i].StudentID, Student[i].Pref[0], Student[i].Pref[1], Student[i].Pref[2]);
    // pthread_mutex_unlock(&lock);

    usleep(10);
    courseRegister(i);

    int c = Student[i].Current;
    int cid = Student[i].Pref[c];

    pthread_cond_wait(&cond_seats_available[cid], &Course[cid].Lock);
    allocateSeats(cid);

    pthread_mutex_lock(&Course[cid].Lock);
    pthread_cond_signal(&cond_allocation[Course[cid].CourseID]);
    pthread_mutex_unlock(&Course[cid].Lock);

    pthread_cond_wait(&cond_seats_available[cid], &Course[cid].Lock);
    selectCourse(i, cid);

    return NULL;
}

void courseRegister(int i)
{
    sleep(Student[i].FillTime);
    Student[i].Status = FILLED;
    printf(COLOR_GREEN "Student %d has filled in preferences for course registration\n" COLOR_RESET, i);
}

void allocateSeats(int C)
{
    for(int i = 0; i < numStudents; i++)
    {
        if(Student[i].Status == NOT_FILLED)
            continue;

        if(Course[C].SlotsFilled == Course[C].Slots)
            return;

        if(Student[i].Pref[Student[i].Current] == Course[C].CourseID)
        {
            printf(COLOR_RED "Student %d has been allocated a seat in course %s\n" COLOR_RESET, i, Course[C].Name);
            // printf("pref = %s current = %d\n", Course[Student[i].Pref[Student[i].Current]].Name, Student[i].Current);
            Course[C].SlotsFilled++;
        }
    }
}

void selectCourse(int i, int j)
{
    a();

    tStudent S = Student[i];
    tCourse C = Course[j];

    float p = C.Interest * S.Calibre;
    if(Prob(p))
    {
        printf(COLOR_BLUE "Student %d has selected the course %s permanently\n", i, C.Name);
        return;
    }
    printf(COLOR_BLUE "Student %d has withdrawn from course %s \n", i, C.Name);
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/main.c ####################//

// Each person is a thread
// Each TA is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "student.c"
#include "course.c"
// #include "seat.c"
// #include "goal.c"
// #include "leave.c"

void main()
{
    printf(COLOR_YELLOW "Simulation has started!\n" COLOR_RESET);
    input();
    printf("------------------------------------------------------------------------\n");

    // Thread for each student
    thStudent = (pthread_t*)malloc(numStudents * sizeof(pthread_t));
    for(int i = 0; i < numStudents; i++)
    {
        pthread_create(&thStudent[i], NULL, studentFunction, &i);
        usleep(20);
    }

    sleep(1);

    // Thread for each course
    thCourse = (pthread_t*)malloc(numCourses * sizeof(pthread_t));
    for(int i = 0; i < numCourses; i++)
    {
        pthread_create(&thCourse[i], NULL, courseFunction, &i);
        usleep(20);
    }

    threadExit();
}
    /*
    // Person Threads
    tPersonID PersonID[num_people];
    int c = 0;
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
        }
    }

    // Goal Threads
    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

    // Join threads
    join();

    printf("------------------------------------------------------------------------\n");
}
*/

void threadExit()
{
    // Exit student threads
    for (int i = 0; i < numStudents; i++)
    {
        pthread_exit(thStudent[i]);
    }
    // Exit course threads
    for (int i = 0; i < numCourses; i++)
    {
        pthread_exit(thCourse[i]);
    }
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/seat.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf(COLOR_YELLOW "%s could not get a seat\n" COLOR_RESET, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    // Push
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;
    Zone[team].NumSpectators++;

    // Display
    printf(COLOR_YELLOW "%s has got a seat in zone %c (%d/%d)\n" COLOR_RESET, 
            Group[i].Person[j].Name,
            getZoneAsChar(team),
            Zone[team].NumSpectators,
            Zone[team].Capacity);

    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    // Person already left
    if(Group[i].Person[j].status == WAITING)
        return;

    // Spectating time over
    printf(COLOR_MAGENTA "%s watched the match for %d seconds and is leaving\n" COLOR_RESET, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    // Pop
    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&lock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&lock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}

//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/person.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf(COLOR_CYAN "%s has reached the stadium\n" COLOR_RESET, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf(COLOR_BLUE "Group %d is leaving for dinner\n" COLOR_RESET, i+1);
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/course.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void* courseFunction(void* arg)
{
    // pthread_mutex_lock(&lock);
    int i = *(int*)arg;
    // pthread_mutex_unlock(&lock);

    allocateMentor(i);

    pthread_cond_wait(&cond_allocation[i], &Course[i].Lock);
    tutorial(i);

    pthread_mutex_lock(&Course[i].Lock);
    pthread_cond_signal(&cond_seats_available[i]);
    pthread_mutex_unlock(&Course[i].Lock);

    return NULL;
}

void allocateMentor(int C)
{
    // Course already occupied
    if(Course[C].Status == OCCUPIED)
        return;

    // printf("%s:\n", Course[C].Name);

    // Iterate through course's lab id's
    for(int i = 0; i < Course[C].NumLabs; i++)
    {
        tLab L = Lab[Course[C].LabID[i]];
        
        // Iterate though the lab's mentors
        for(int j = 0; j < L.NumMentors; j++)
        {
            tMentor M = L.Mentor[j];

            // Mentor is taking sesh
            if(M.Status == OCCUPIED)
                continue;

            // Allocate M to C
            Course[C].Status = OCCUPIED;
            Lab[Course[C].LabID[i]].Mentor[j].Status = OCCUPIED;

            Lab[Course[C].LabID[i]].Mentor[j].Num++;
            int Num = Lab[Course[C].LabID[i]].Mentor[j].Num;

            Course[C].Mentor = Lab[Course[C].LabID[i]].Mentor[j];
            Course[C].MentorID = j;
            // printf("ID = %d\n", Course[C].Mentor.ID);
            Course[C].Lab = Lab[Course[C].LabID[i]];
            printf(COLOR_CYAN "TA %d from lab %s has been allocated to course %s for his %d%s TA ship\n" COLOR_RESET,
                j, L.Name, Course[C].Name, Num, getSuffix(Num));

            numSlots(C);

            pthread_mutex_lock(&Course[C].Lock);
            pthread_cond_signal(&cond_seats_available[Course[C].CourseID]);
            pthread_mutex_unlock(&Course[C].Lock);

            usleep(20);
            // allocateSeats(C);
            
            return;
        }
    }
}

void numSlots(int i)
{
    Course[i].Slots = randint(Course[i].MaxSlots);
    printf(COLOR_YELLOW "Course %s has been allocated %d seats\n" COLOR_RESET, Course[i].Name, Course[i].Slots);
}

char* getSuffix(int n)
{
    int secondLastDigit = (n/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(n)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}

void tutorial(int i)
{
    tCourse C = Course[i];
    printf(COLOR_MAGENTA "Tutorial has started for course %s with %d seats filled out of %d\n" COLOR_RESET, C.Name, C.SlotsFilled, C.Slots);

    // Time taken for tutorial
    sleep(1);

    printf(COLOR_MAGENTA "TA %d from lab %s has completed the tutorial for course %s\n" COLOR_RESET,
        C.MentorID, C.Lab.Name, C.Name);
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/utility.h ####################//

#include "libraries.h"

void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Fish\n");
}
void b()
{
    printf("Shark\n");
}

int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}

// Random number between 1 and L (both inclusive)
int randint(int L)
{
    srand(time(NULL));
    return (rand() % L) + 1;
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/leave.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(P.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf(COLOR_RED "%s is leaving due to bad performance of his team\n" COLOR_RESET, Zone[Z].Seat[i].Person.Name);
        printf("%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;

        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        pthread_mutex_lock(&lock);
        pthread_cond_signal(&cond_seat_freed);
        pthread_mutex_unlock(&lock);
    }
}

//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"

// Course/Mentor status
#define FREE 0
#define OCCUPIED 1

// Student status
#define NOT_FILLED 0
#define FILLED 1

// Types
typedef uint Time;

// Mentor Structure
typedef struct stMentor tMentor;
struct stMentor
{
    int dump[10];
    int ID;
    int Num;
    int Status;
    pthread_mutex_t MentorLock;
};

// Lab Structure
typedef struct stLab tLab;
struct stLab
{
    char Name[10];
    int NumMentors;
    tMentor* Mentor;
    int Max;
};

// Course Structure
typedef struct stCourse tCourse;
struct stCourse
{
    int CourseID;
    pthread_mutex_t Lock;
    char Name[10];
    float Interest;

    int MaxSlots;
    int Slots;
    int SlotsFilled;

    int NumLabs;
    int* LabID;
    
    int Status;

    tMentor Mentor;
    int MentorID;
    tLab Lab;
};



// Student Structure
typedef struct stStudent tStudent;
struct stStudent
{
    // int dump[10];
    int StudentID;
    float Calibre;
    int Current;
    int Pref[2];
    Time FillTime;
    int Status;
};


#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/input.c ####################//

#include "libraries.h"
#include "variables.h"
// #include "functions.h"
#include "utility.h"

void input()
{
    printf(COLOR_BLUE "#Students | #Labs | #Courses\n" COLOR_RESET);
    scanf("%d %d %d", &numStudents, &numLabs, &numCourses);

    inputCourses();
    inputStudents();
    inputLabs();
}

void inputCourses()
{
    Course = (tCourse*)malloc(numCourses * sizeof(tCourse));
    cond_seats_available = (pthread_cond_t*)malloc(numCourses * sizeof(pthread_cond_t));
    cond_allocation = (pthread_cond_t*)malloc(numCourses * sizeof(pthread_cond_t));

    for(int i = 0; i < numCourses; i++)
    {
        // Prefix
        printf(COLOR_RED "C%d: " COLOR_RESET, i+1);

        // Set Course ID and status
        Course[i].CourseID = i;
        Course[i].Status = FREE;
        Course[i].SlotsFilled = 0;
        pthread_cond_init(&cond_seats_available[i], NULL);
        pthread_cond_init(&cond_allocation[i], NULL);

        // Input course variables
        scanf("%s %f %d %d", 
                &Course[i].Name,
                &Course[i].Interest,
                &Course[i].MaxSlots,
                &Course[i].NumLabs);

        // Input lab id's for this course
        Course[i].LabID = (int*)malloc(Course[i].NumLabs * sizeof(int));
        for(int j = 0; j < Course[i].NumLabs; j++)
        {
            scanf("%d", &Course[i].LabID[j]);
        }
    }
}

void inputStudents()
{
    Student = (tStudent*)malloc(numStudents * sizeof(tStudent));

    // Iterate through all the students
    for(int i = 0; i < numStudents; i++)
    {
        flushSTDIN();

        // Prefix
        printf(COLOR_CYAN "S%d: " COLOR_RESET, i+1);

        // Set student ID
        Student[i].StudentID = i;
        Student[i].Current = 0;
        Student[i].Status = NOT_FILLED;

        // Input student variables
        scanf("%f %d %d %d %d", 
                &Student[i].Calibre,
                &Student[i].Pref[0],
                &Student[i].Pref[1],
                &Student[i].Pref[2],
                &Student[i].FillTime);
    }
}

void inputLabs()
{
    Lab = (tLab*)malloc(numLabs * sizeof(tLab));

    for(int i = 0; i < numLabs; i++)
    {
        flushSTDIN();

        // Prefix
        printf(COLOR_GREEN "L%d: " COLOR_RESET, i+1);

        // Initialize all TA's
        Lab[i].Mentor = (tMentor*)malloc((Lab[i].NumMentors + 1) * sizeof(tMentor));
        for(int j = 0; j < Lab[i].NumMentors; j++)
        {
            Lab[i].Mentor[j].ID = j;
            Lab[i].Mentor[j].Num = 0;
            Lab[i].Mentor[j].Status = FREE;
            pthread_mutex_init(&Lab[i].Mentor[j].MentorLock, NULL);
        }

        // Input lab variables
        scanf("%s %d %f", 
                &Lab[i].Name,
                &Lab[i].NumMentors,
                &Lab[i].Max);
    }   
    printf("\n");
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

clock_t getTime();
void delay(int ms);
clock_t start;

int main()
{
    start = clock();
    delay(5000);
    printf("Hello\n");
}

clock_t getTime()
{
    clock_t diff = clock() - start;
    return (diff * 1000)/CLOCKS_PER_SEC;
}

void delay(int ms)
{
    clock_t timeDelay = ms + clock();
    while(timeDelay > clock());
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/variables.h ####################//

#ifndef VAR_H
#define VAR_H

const int numStudents;    
const int numLabs;    
const int numCourses;    

tCourse* Course;
tStudent* Student;
tLab* Lab;

pthread_t* thStudent;
pthread_t* thCourse;
pthread_t* thLab;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t* goal_thread;
pthread_cond_t* cond_seats_available;
pthread_cond_t* cond_allocation;
// int seat_freed = 0;

#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputCourses();
    void inputStudents();
    void inputLabs();
    void printZone(int i);
    void printGroup(int i);

// Main
void threadExit();

// Student
void* studentFunction(void* arg);
    void courseRegister(int i);
    void allocateSeats(int C);
    void selectCourse(int S, int C);

// Course
void* courseFunction(void* arg);
    void allocateMentor(int i);
    void numSlots(int i);
    char* getSuffix(int n);
    void tutorial(int C);

// Lab
void* labFunction(void* arg);

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();
int randint(int L);

#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q1/src/goal.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf(COLOR_GREEN "Team H has scored their %d%s goal!\n" COLOR_RESET, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf(COLOR_GREEN "Team A has scored their %d%s goal!\n" COLOR_RESET, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
        return NULL;
    }
    // Goal Missed
    printf(COLOR_GREEN "Team %c has missed!\n" COLOR_RESET, Goal[i].Team);
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}