
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/student.c ####################//

#include <malloc.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "student.h"
#include "course.h"
#include "wrapper.h"
#include "globals.h"

Student *all_students;

void student_init(int n) {
    all_students = (Student *) malloc(n * sizeof(Student));
    assert(all_students);
    for (int i = 0; i < n; i++) {
        all_students[i].id = i;
    }
}

void *student_process(void *input) {
    Student *student = (Student *) input;
    sleep(student->enterTime);
    printf(C_STUDENT "Student %d has filled in preferences for course registration\n" RESET, student->id);
    int found = 0;
    for (int i = 0; i < 3 && !found; i++) {
        Course *course = &all_courses[student->preference[i]];
        if (course->withdrawn) {
            if (i < 2)
                printf(C_STUDENT "Student %d changed current preference from course %s (priority %d) to course %s (priority %d)\n" RESET, student->id, course->name, i + 1, all_courses[student->preference[i + 1]].name, i + 2);
            else
                printf(C_STUDENT "Student %d couldn't get any of his preferred courses\n" RESET, student->id);
            continue;
        }
        Pthread_mutex_lock(&course->courseLock);
        course->prefer++;
        if (course->prefer == 1)
            Pthread_cond_signal(&course->emptyCond);
        Pthread_cond_wait(&course->openCond, &course->courseLock);
        course->prefer--;
        //might have gotten because of broadcast
        if (course->withdrawn) {
            Pthread_mutex_unlock(&course->courseLock);
            if (i < 2)
                printf(C_STUDENT "Student %d changed current preference from course %s (priority %d) to course %s (priority %d)\n" RESET, student->id, course->name, i + 1, all_courses[student->preference[i + 1]].name, i + 2);
            else
                printf(C_STUDENT "Student %d couldn't get any of his preferred courses\n" RESET, student->id);
            continue;
        }
        printf(C_STUDENT "Student %d has been allocated a seat in course %s\n" RESET, student->id, course->name);
        Pthread_mutex_unlock(&course->courseLock);

        Pthread_mutex_lock(&course->tutLock);
        course->tutSlots++;
        Pthread_cond_signal(&course->fullCond);
        Pthread_cond_wait(&course->tutCond, &course->tutLock);
        Pthread_mutex_unlock(&course->tutLock);

        double prob = course->interestQuotient * student->calibre;
        double randomSample = (double) rand() / RAND_MAX;
        if (randomSample <= prob) {
            printf(C_STUDENT "Student %d has selected course %s permanently\n" RESET, student->id, course->name);
            found = 1;
        }
        else {
            if (i < 2)
                printf(C_STUDENT "Student %d changed current preference from course %s (priority %d) to course %s (priority %d)\n" RESET, student->id, course->name, i + 1, all_courses[student->preference[i + 1]].name, i + 2);
            else
                printf(C_STUDENT "Student %d couldn't get any of his preferred courses\n" RESET, student->id);
        }
    }

    return NULL;
}
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/wrapper.h ####################//

#ifndef PARALLELISM_WRAPPER_H
#define PARALLELISM_WRAPPER_H

#include <pthread.h>

int Pthread_create(pthread_t *nt, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
int Pthread_join(pthread_t thread, void **retval);
int Pthread_cancel(pthread_t thread);
int Pthread_mutex_lock(pthread_mutex_t *p);
int Pthread_mutex_unlock(pthread_mutex_t *p);
int Pthread_mutex_init(pthread_mutex_t *p, const pthread_mutexattr_t *ptr);
int Pthread_cond_init(pthread_cond_t *p, const pthread_condattr_t *ptr);
int Pthread_cond_signal(pthread_cond_t *p);
int Pthread_cond_broadcast(pthread_cond_t *p);
int Pthread_cond_wait(pthread_cond_t *p, pthread_mutex_t *x);

#endif //PARALLELISM_WRAPPER_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/globals.h ####################//

#ifndef PARALLELISM_GLOBALS_H
#define PARALLELISM_GLOBALS_H

#define MIN(a, b) (a < b) ? a : b

#define C_STUDENT     "\x1b[35m"
#define C_LAB   "\x1b[32m"
#define C_COURSE  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

extern int num_students;
extern int num_labs;
extern int num_courses;

#endif //PARALLELISM_GLOBALS_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/lab.h ####################//

#ifndef PARALLELISM_LAB_H
#define PARALLELISM_LAB_H

#include <pthread.h>

#define MAX_LAB_NAME 256

struct lab {
    int id;
    char *name;
    int numTA;
    int TALimit;
    int *taTimes;
    int available;
    pthread_mutex_t *taLock;
};

typedef struct lab Lab;

extern Lab *all_labs;

void lab_init(int n);
void *lab_process(void *input);

#endif //PARALLELISM_LAB_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/main.c ####################//

#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <pthread.h>
#include "student.h"
#include "course.h"
#include "lab.h"
#include "wrapper.h"

int num_students, num_labs, num_courses;

int main() {
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    student_init(num_students);
    lab_init(num_labs);
    course_init(num_courses);

    for (int i = 0; i < num_courses; i++) {
        scanf("%s %lf %d", all_courses[i].name, &all_courses[i].interestQuotient, &all_courses[i].course_max_slots);
        scanf("%d", &all_courses[i].numValidLabs);
        all_courses[i].validLabs = (int *) malloc(all_courses[i].numValidLabs * sizeof(int));
        assert(all_courses[i].validLabs);
        for (int j = 0; j < all_courses[i].numValidLabs; j++)
            scanf("%d", &all_courses[i].validLabs[j]);
    }

    for (int i = 0; i < num_students; i++) {
        scanf("%lf", &all_students[i].calibre);
        for (int j = 0; j < 3; j++)
            scanf("%d", &all_students[i].preference[j]);
        scanf("%lld", &all_students[i].enterTime);
    }

    for (int i = 0; i < num_labs; i++) {
        scanf("%s %d %d", all_labs[i].name, &all_labs[i].numTA, &all_labs[i].TALimit);
        all_labs[i].taTimes = (int *) malloc(all_labs[i].numTA * sizeof(int));
        assert(all_labs[i].taTimes);
        all_labs[i].taLock = (pthread_mutex_t *) malloc(all_labs[i].numTA * sizeof(pthread_mutex_t));
        assert(all_labs[i].taLock);
        for (int j = 0; j < all_labs[i].numTA; j++)
            Pthread_mutex_init(&all_labs[i].taLock[j], NULL);
    }

    pthread_t *studentThreads = (pthread_t *) malloc(num_students * sizeof(pthread_t));
    assert(studentThreads);
    for (int i = 0; i < num_students; i++)
        Pthread_create(&studentThreads[i], NULL, student_process, &all_students[i]);

    pthread_t *courseThreads = (pthread_t *) malloc(num_courses * sizeof(pthread_t));
    assert(courseThreads);
    for (int i = 0; i < num_courses; i++)
        Pthread_create(&courseThreads[i], NULL, course_process, &all_courses[i]);

    pthread_t *labThreads = (pthread_t *) malloc(num_labs * sizeof(pthread_t));
    assert(labThreads);
    for (int i = 0; i < num_labs; i++)
        pthread_create(&labThreads[i], NULL, lab_process, &all_labs[i]);

    for (int i = 0; i < num_students; i++)
        pthread_join(studentThreads[i], NULL);

    for (int i = 0; i < num_labs; i++)
        pthread_cancel(labThreads[i]);

    for (int i = 0; i < num_courses; i++)
        pthread_cancel(courseThreads[i]);

    return 0;
}
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/student.h ####################//

#ifndef PARALLELISM_STUDENT_H
#define PARALLELISM_STUDENT_H

struct student {
    int id;
    int preference[3];
    long long int enterTime;
    double calibre;
};

typedef struct student Student;

extern Student *all_students;

void student_init(int n);
void *student_process(void *input);

#endif //PARALLELISM_STUDENT_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/course.c ####################//

#include "course.h"
#include "globals.h"
#include "lab.h"
#include "wrapper.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

Course *all_courses;

void course_init(int n) {
    all_courses = (Course *) malloc(n * sizeof(Course));
    assert(all_courses);
    for (int i = 0; i < n; i++) {
        all_courses[i].id = i;
        all_courses[i].prefer = 0;
        all_courses[i].tutSlots = 0;
        all_courses[i].withdrawn = 0;
        Pthread_mutex_init(&all_courses[i].tutLock, NULL);
        Pthread_mutex_init(&all_courses[i].courseLock, NULL);
        Pthread_cond_init(&all_courses[i].tutCond, NULL);
        Pthread_cond_init(&all_courses[i].openCond, NULL);
        Pthread_cond_init(&all_courses[i].emptyCond, NULL);
        Pthread_cond_init(&all_courses[i].fullCond, NULL);
        all_courses[i].name = (char *) malloc(MAX_COURSE_NAME);
        assert(all_courses[i].name);
    }
}

void take_tutorial(Course *course) {
    int pickSlots = (rand() % course->course_max_slots) + 1;
    printf(C_COURSE "Course %s has been allocated %d seats\n" RESET, course->name,
           pickSlots);
    Pthread_mutex_lock(&course->courseLock);
    int numAttendingTut = MIN(course->prefer, pickSlots);
    Pthread_mutex_unlock(&course->courseLock);

    for (int i = 0; i < numAttendingTut; i++) {
        Pthread_mutex_lock(&course->courseLock);
        Pthread_cond_signal(&course->openCond);
        Pthread_mutex_unlock(&course->courseLock);
    }

    Pthread_mutex_lock(&course->tutLock);
    while (numAttendingTut != course->tutSlots)
        Pthread_cond_wait(&course->fullCond, &course->tutLock);
    Pthread_mutex_unlock(&course->tutLock);

    Pthread_mutex_lock(&course->courseLock);
    printf(C_COURSE
           "Tutorial has started for course %s with %d filled out of %d\n" RESET,
           course->name, course->tutSlots, pickSlots);
    sleep(2);
    Pthread_mutex_lock(&course->tutLock);
    for (int i = 0; i < course->tutSlots; i++) {
        Pthread_cond_signal(&course->tutCond);
    }
    course->tutSlots = 0;
    Pthread_mutex_unlock(&course->tutLock);
    Pthread_mutex_unlock(&course->courseLock);
}

void *course_process(void *input) {
    Course *course = (Course *) input;
    int found = 1;
    while (found) {
        found = 0;
        for (int i = 0; i < course->numValidLabs; i++) {
            Lab currentLab = all_labs[course->validLabs[i]];
            if (!currentLab.available)
                continue;
            for (int j = 0; j < currentLab.numTA; j++) {
                Pthread_mutex_lock(&course->courseLock);
                while (course->prefer == 0)
                    Pthread_cond_wait(&course->emptyCond, &course->courseLock);
                Pthread_mutex_unlock(&course->courseLock);
                if (currentLab.taTimes[j] == currentLab.TALimit)
                    continue;
                Pthread_mutex_lock(&currentLab.taLock[j]);
                found = 1;
                if (currentLab.taTimes[j] == currentLab.TALimit) {
                    Pthread_mutex_unlock(&currentLab.taLock[j]);
                    continue;
                }
                printf(C_LAB "TA %d from lab %s has been allocated to course %s for "
                       "%dth TAship\n" RESET,
                       j, currentLab.name, course->name, currentLab.taTimes[j] + 1);
                take_tutorial(course);
                currentLab.taTimes[j]++;
                printf(C_COURSE "TA %d from lab %s has completed the tutorial and left "
                       "the course %s\n" RESET,
                       j, currentLab.name, course->name);
                Pthread_mutex_unlock(&currentLab.taLock[j]);
            }
        }
    }
    Pthread_mutex_lock(&course->courseLock);
    course->withdrawn = 1;
    printf(C_COURSE "Course %s doesn't have any TAs eligible and is removed from "
           "course offerings\n" RESET,
           course->name);
    Pthread_cond_broadcast(&course->openCond);
    Pthread_mutex_unlock(&course->courseLock);

    return NULL;
}

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/wrapper.c ####################//

#include <assert.h>
#include "wrapper.h"

int Pthread_create(pthread_t *nt, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg) {
    int rc = pthread_create(nt, attr, start_routine, arg);
    assert(rc == 0);
    return 0;
}

int Pthread_join(pthread_t thread, void **retval) {
    int rc = pthread_join(thread, retval);
    assert(rc == 0);
    return 0;
}

int Pthread_cancel(pthread_t thread) {
    int rc = pthread_cancel(thread);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_lock(pthread_mutex_t *p) {
    int rc = pthread_mutex_lock(p);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_unlock(pthread_mutex_t *p) {
    int rc = pthread_mutex_unlock(p);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_init(pthread_mutex_t *p, const pthread_mutexattr_t *ptr) {
    int rc = pthread_mutex_init(p, ptr);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_init(pthread_cond_t *p, const pthread_condattr_t *ptr) {
    int rc = pthread_cond_init(p, ptr);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_signal(pthread_cond_t *p) {
    int rc = pthread_cond_signal(p);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_broadcast(pthread_cond_t *p) {
    int rc = pthread_cond_broadcast(p);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_wait(pthread_cond_t *p, pthread_mutex_t *x) {
    int rc = pthread_cond_wait(p, x);
    assert(rc == 0);
    return 0;
}

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/course.h ####################//

#ifndef PARALLELISM_COURSE_H
#define PARALLELISM_COURSE_H

#define MAX_COURSE_NAME 256
#include "pthread.h"

struct course {
    int id;
    char *name;
    double interestQuotient;
    int course_max_slots;
    int numValidLabs;
    int *validLabs;
    int prefer;
    pthread_mutex_t courseLock;
    pthread_cond_t tutCond;
    pthread_cond_t openCond;
    int tutSlots;
    pthread_mutex_t tutLock;
    int withdrawn;
    pthread_cond_t emptyCond;
    pthread_cond_t fullCond;
};

typedef struct course Course;

extern Course *all_courses;

void course_init(int n);
void *course_process(void *input);

#endif //PARALLELISM_COURSE_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q1/lab.c ####################//

#include <malloc.h>
#include <assert.h>
#include "lab.h"
#include "globals.h"

Lab *all_labs;

void lab_init(int n) {
    all_labs = (Lab *) malloc(n * sizeof(Lab));
    assert(all_labs);
    for (int i = 0; i < n; i++) {
        all_labs[i].id = i;
        all_labs[i].available = 1;
        all_labs[i].name = (char *) malloc(MAX_LAB_NAME);
        assert(all_labs[i].name);
    }
}

void *lab_process(void *input) {
    Lab *lab = (Lab *) input;
    int found = 1;
    while (found) {
        found = 0;
        for (int i = 0; i < lab->numTA; i++) {
            if (lab->taTimes[i] < lab->TALimit) {
                found = 1;
                break;
            }
        }
    }
    printf(C_LAB "Lab %s no longer has students available for TAship\n" RESET, lab->name);
    lab->available = 0;
    return NULL;
}
