
//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "input.h"
#include "students.h"
#include "courses.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


int main() {
//     struct course* course_info;
// struct lab* lab_info;
    student_info = (malloc(sizeof(struct student)*MAX_STUDENTS));
    course_info = malloc(sizeof(struct course)*(MAX_COURSES));
    lab_info = malloc(sizeof(struct lab)*MAX_LABS);
    takeinput();

    for(int x=0;x<num_students;x++){
        pthread_mutex_init(&student_info[x].mutex, NULL);
        pthread_cond_init(&student_info[x].cond, NULL);
        student_info[x].id = x;
        student_info[x].cur_pref = 0;
    }

    for(int x=0;x<num_courses;x++){
        course_removed[x]=0;
        course_names[x] = course_info[x].name;
        course_info[x].id = x;
    }

    for(int x=0;x<num_labs;x++){
        for(int y=0;y<lab_info[x].num_students;y++){
            pthread_mutex_init(&lab_info[x].mutex[y], NULL);
            lab_info[x].ta_availability[y]=1;
            lab_info[x].labs_taken[y]=0;
        }
    }

    for(int x=0;x<num_students;x++){
        pthread_create(&student_threads[x],NULL,student_thread,&student_info[x]);
    }

    for(int x=0;x<num_courses;x++){
        pthread_create(&course_threads[x],NULL,course_thread,&course_info[x]);
    }

    for(int x=0;x<num_courses;x++){
        pthread_join(course_threads[x],NULL);
    }
    
    for(int x=0;x<num_students;x++){
        pthread_join(student_threads[x],NULL);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/students.h ####################//

#include <pthread.h>
#ifndef STUDENTS_H
#define STUDENTS_H

#define MAX_STUDENTS       1000

struct student {
    double calibre;
    int sleep_time;
    int prefs[3];
    int id;
    int cur_pref;
    int status;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

struct student *student_info;
int num_students;

pthread_t student_threads[MAX_STUDENTS];

void *student_thread(void *arg);

#endif
//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/courses.h ####################//

#include <pthread.h>
#ifndef COURSES_H
#define COURSES_H

#define MAX_LABS           1000
#define MAX_COURSES        1000
#define MAX_TAS            1000
struct course {
    char name[1000];
    double interest;
    int max_slots;
    int id;
    int allowed_labs;
    int labs_list[MAX_LABS];
};

struct lab {
    char name[1000];
    int num_students;
    int max_taships;
    int ta_availability[MAX_TAS];
    int labs_taken[MAX_TAS];
    pthread_mutex_t mutex[MAX_TAS];
};

struct course* course_info;
struct lab* lab_info;
int num_courses;
int num_labs;

int course_removed[MAX_COURSES];
char *course_names[MAX_COURSES];

pthread_t course_threads[MAX_COURSES];


void *course_thread(void *arg);

#endif
//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/students.c ####################//

#include "students.h"
#include "courses.h"
#include <unistd.h>
#include <stdio.h>
void *student_thread(void *arg)
{
    struct student *student = (struct student *)arg;
    // printf("%d %d\n",student->id,student->sleep_time);
    sleep(student->sleep_time);

    printf("Student %d has filled in preferences for course registration\n",student->id);

    while(1)
    {
        int p = student->cur_pref;
        if(student->cur_pref >= 3){
            break;
        }
        if(course_removed[student->prefs[student->cur_pref]] == 0){
            pthread_mutex_lock(&student->mutex);
            student->status = 0;
            // printf("1\n");
            while(student->status!=2)
                pthread_cond_wait(&student->cond, &student->mutex);
            pthread_mutex_unlock(&student->mutex);
            // printf("2\n");
            if(p<student->cur_pref){
                if (student->cur_pref >= 3)
                {
                    printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                    break;
                }
                else
                {
                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
                    continue;
                }
            }

            pthread_mutex_lock(&student->mutex);
            double prob = student->calibre * course_info[student->prefs[student->cur_pref]].interest;
            if(prob > 0.75){
                printf("Student %d has selected the course %s permanently\n", student->id, course_info[student->prefs[student->cur_pref]].name);
                student->cur_pref = 3;
                pthread_mutex_unlock(&student->mutex);
                break;
            }
            else{
                printf("Student %d has withdrawn from course %s\n" , student->id, course_info[student->prefs[student->cur_pref]].name);
                student->cur_pref++;
                if (student->cur_pref >= 3)
                {
                    printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                }
                else
                {
                    printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
                }
                pthread_mutex_unlock(&student->mutex);
            }
        }
        else{
            student->cur_pref++;
            if(student->cur_pref >= 3){
                printf("Student %d couldn’t get any of his preferred courses\n", student->id);
                break;
            }
            printf("Student %d has changed current preference from %s (priority %d) to %s (priority %d)", student->id, course_names[student->prefs[student->cur_pref-1]], student->cur_pref, course_names[student->prefs[student->cur_pref]], student->cur_pref+1);
            continue;
        }
    }
    pthread_exit(NULL);

}
//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/input.c ####################//

#include <stdio.h>
#include "input.h"
#include "courses.h"
#include "students.h"


void takeinput()
{
    scanf("%d %d %d", &num_students, &num_labs, &num_courses);
    // printf("%d %d %d\n",num_students,num_labs,num_courses);
    //Reading course info
    for(int x = 0; x<num_courses; x++){
        scanf("%s %lf %d %d", course_info[x].name, &course_info[x].interest , &course_info[x].max_slots, &course_info[x].allowed_labs);
        for(int y=0;y<course_info[x].allowed_labs;y++){
            scanf("%d", &course_info[x].labs_list[y]);
        }
        // printf("%s %lf %d %d\n", course_info[x].name, course_info[x].interest , course_info[x].max_slots, course_info[x].allowed_labs);
    }
    //-------------------------
    //Reading student info
    for(int x=0;x<num_students;x++){
        scanf("%lf %d %d %d %d", &student_info[x].calibre, &student_info[x].prefs[0], &student_info[x].prefs[1], &student_info[x].prefs[3], &student_info[x].sleep_time);
        // printf("%lf %d %d %d %d", student_info[x].calibre, student_info[x].prefs[0], student_info[x].prefs[1], student_info[x].prefs[3], student_info[x].sleep_time);
        // printf("%d %d\n", student_info[x].id,student_info[x].sleep_time);
    }
    //-------------------------
    //Reading lab info
    for(int x=0;x<num_labs;x++){
        scanf("%s %d %d", lab_info[x].name, &lab_info[x].num_students, &lab_info[x].max_taships);
    }
    //-------------------------
}
//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/courses.c ####################//

#include "courses.h"
#include "students.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
void *course_thread(void *arg)
{
    struct course *course = (struct course*)arg;
    // printf("4\n");
    while(1)
    {
        int flag=0;
        int flag2=0;
        int y=0;
        int x=0;
        for(;x<course->allowed_labs;x++){
            int lab_num = course->labs_list[x];
            for(;y<lab_info[lab_num].num_students;y++)
            {
                pthread_mutex_lock(&lab_info[lab_num].mutex[y]);
                if(lab_info[lab_num].ta_availability[y]==1 && lab_info[lab_num].labs_taken[y] < lab_info[x].max_taships){
                    
                    lab_info[lab_num].ta_availability[y]=0;
                    // lab_info[lab_num].labs_taken[y]++;
                    flag=1;
                    break;
                }
                if(lab_info[lab_num].labs_taken[y]<lab_info[lab_num].max_taships){
                    flag2=1;
                }
                pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
            }
            if(flag)
                break;
        }

        
        // printf("5\n");
        // if(!flag && !flag2){
        //     continue;
        // check tas
        // printf("3\n");
        // for(int x=0;x<course->allowed_labs;x++){
        //     int lab_num = course->labs_list[x];
        //     for(int y=0;y<lab_info[lab_num].num_students;y++)
        //     {
        //         pthread_mutex_lock(&lab_info[lab_num].mutex[y]);
        //         if(lab_info[lab_num].labs_taken[y]<lab_info[x].max_taships && lab_info[lab_num].ta_availability[y]==1){
        //             flag2=1;
        //             pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
        //             break;
        //         }
        //         pthread_mutex_unlock(&lab_info[lab_num].mutex[y]);
        //     }
        //     if(flag2)
        //         break;
        // }

        if(flag==0 && flag2==0){
            printf("Course %s doesn’t have any TA’s eligible and is removed from course offerings\n", course->name);
            break;
        }

// check students
        int flag1 = 0;
        for(int x=0;x<num_students;x++){
            if(student_info[x].cur_pref<3 && student_info[x].prefs[student_info[x].cur_pref] == course->id && student_info[x].status==0){
                flag1 = 1;
                break;
            }
        }

        if(flag1==0){
            
        }
        // chooseta
        
        x = course->labs_list[x];
        printf("TA %d from lab %s has been allocated to course %s for his %dth TA ship\n", y, lab_info[x].name, course->name, lab_info[x].labs_taken[y]);
        printf("Course %s has been allocated TA %d from lab %s\n", course->name, y, lab_info[x].name);

        // allot
        int can_choose[num_students];
        int total=0;
        for(int x=0;x<num_students;x++){
            if(student_info[x].cur_pref<3 && student_info[x].prefs[student_info[x].cur_pref] == course->id && student_info[x].status==0){
                can_choose[x]=1;
                total++;
            }
            else
                can_choose[x]=0;
        }

        int t = (rand()% course->max_slots)+1;
        printf("Course %s has been allocated %d seats\n" , course->name, t);
        int final = t;
        if(final>total)
            final = total;

        int done =0;
        for(int i=0;i<num_students;i++){
            if(can_choose[x]){
                done++;
                student_info[i].status=1;
                printf("Student %d has been allocated a seat in course %s\n", i, course->name);
            }
            if(done==final)
                break;
        }
        printf("Tutorial has started for Course %s with %d seats filled out of %d\n", course->name, final, t);
        sleep(2);

        //release

        for (int i = 0; i < num_students; i++)
            if (can_choose[i] && student_info[x].status==1)
            {
                student_info[i].status=2;
                pthread_cond_signal(&student_info[i].cond);
            }

        printf("TA %d from lab %s has completed the tutorial and left the course %s\n", y, lab_info[x].name, course->name);

        int flag3=0;
        for(int i=0;i<lab_info[x].num_students;i++){
            if(lab_info[x].labs_taken[i]<lab_info->max_taships){
                flag3=1;
                break;
            }
        }
        if(!flag3){
            printf("Lab %s no longer has students available for TA ship\n", lab_info[x].name);
        }

        lab_info[x].ta_availability[y]=1;
        pthread_mutex_unlock(&lab_info[x].mutex[y]);

        
    }

    for(int x=0;x<num_students;x++){
        if((student_info[x].status==0 || student_info[x].status==1) && student_info[x].cur_pref<3 && student_info[x].prefs[student_info->cur_pref]==course->id)
        {
            student_info[x].cur_pref++;
            student_info[x].status=2;
            pthread_cond_signal(&student_info[x].cond);
        }
    }
    pthread_exit(NULL);
}

//###########FILE CHANGE ./main_folder/Aarush Jain_305901_assignsubmission_file_/2020101016_assignment_5/q1/input.h ####################//

#ifndef Q1_INPUT_H
#define Q1_INPUT_H

void takeinput();

#endif //Q1_INPUT_H
