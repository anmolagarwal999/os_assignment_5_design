
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/include/globals.h ####################//

#ifndef __COURSESIM_GLOBALS
#define __COURSESIM_GLOBALS

extern int num_students;
extern int num_labs;
extern int num_courses;

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/include/lab.h ####################//

#ifndef __COURSESIM_LAB
#define __COURSESIM_LAB

#include <semaphore.h>

typedef struct Lab{
	char *name;
	int num_students;
	int num_left;
	int limit;
	int *ta;
	pthread_mutex_t *ta_lock;
	sem_t ta_free;
} Lab;

void create_lab(Lab *l);
void destroy_lab(Lab *l);
void read_labs(Lab **L, int num_labs);
void *sim_lab(void *arg);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/include/student.h ####################//

#ifndef __COURSESIM_STUDENT
#define __COURSESIM_STUDENT

typedef struct Student{
	int pref[3];
	double CQ;
	int allotted;
	int time_filled;
	int final_choice;
	int id;

	pthread_mutex_t lock;
} Student;

// Util
int student_time_cmp(const void *, const void *);

void create_student(Student *s, int id);
void destroy_student(Student *s);
void read_students(Student **S, int num_students);

void *spawn_students(void *arg);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/include/course.h ####################//

#ifndef __COURSESIM_COURSE
#define __COURSESIM_COURSE

enum CourseStates {LOOKING_FOR_TA, WAITING_FOR_STUDENTS, TUT_IN_PROGRESS, REMOVED};

typedef struct Course{
	char *name;
	int state;
	double interest;
	int max_slots;
	int occupied_slots;
	int allotted_slots;
	int labs_size;
	int *labs;

	pthread_mutex_t lock;
	pthread_mutex_t cond_lock;
	pthread_cond_t finished;
} Course;

void create_course(Course *c);
void delete_course(Course *c);
void read_courses(Course **C);
Course *getCourseByID(int id, Course *C);
void *sim_course(void *arg);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/include/utils.h ####################//

#ifndef __COURSESIM_UTILS
#define __COURSESIM_UTILS

#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define PINK "\x1B[35m"
#define CYAN "\x1B[36m"
#define WHITE "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct pair{
	void *first;
	void *second;
} pair;

void cprintf(const char *color, const char *format, ...);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/src/student.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>

#include "../include/globals.h"
#include "../include/student.h"
#include "../include/course.h"
#include "../include/utils.h"

void create_student(Student *s, int id){
	s->pref[0] = s->pref[1] = s->pref[2] = -1;
	s->CQ = 0.0;
	s->allotted = -1;
	s->time_filled = 0;
	s->final_choice = -1;
	s->id = id;
	pthread_mutex_init(&(s->lock), NULL);
}

void destroy_student(Student *s){
	pthread_mutex_destroy(&(s->lock));
	memset(s, 0, sizeof(Student));
	free(s);
}

void read_students(Student **S, int num_students){
	
	Student *T = *S = calloc(num_students, sizeof(Student));

	for(int i=0; i < num_students; i++){
		Student *s = &T[i];
		create_student(s, i);
		scanf("%lf %d %d %d %d", &(s->CQ), &(s->pref[0]), &(s->pref[1]), &(s->pref[2]), &(s->time_filled));
	}
}

int student_time_cmp(const void *a, const void *b){
	Student *A = (Student*) a;
	Student *B = (Student*) b;
	return A->time_filled > B->time_filled;
}

bool mathamagic(double prob){
	if(prob > 0.5) return true;
	else return false;
}

void *sim_student(void *arg){

	pair *parg = (pair*) arg;
	Student *s = (Student*) parg->first;
	Course *courses = (Course*) parg->second;
	
	int cur_pref = 0;
	srand(time(0));
	// int unique = rand();
	while(cur_pref < 3){
		Course *mypref = getCourseByID(s->pref[cur_pref], courses);

		// Course thread releases lock only when waiting for students, sanity check in if
		pthread_mutex_lock(&(mypref->lock));

		if(mypref->state == REMOVED){
			pthread_mutex_unlock(&(mypref->lock));
			// Change preferences
			Course *old = mypref;
			cur_pref++;
			if(cur_pref < 3){
				mypref = getCourseByID(s->pref[cur_pref], courses);
				// TODO print together
				cprintf(GREEN, "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", \
							s->id, old->name, cur_pref, mypref->name, cur_pref+1);
			}
			continue;
		}
		else if(mypref->state == WAITING_FOR_STUDENTS && mypref->occupied_slots < mypref->allotted_slots){
			// We have a slot, assign student the slot and attend tutorial
			mypref->occupied_slots++;
			pthread_mutex_unlock(&(mypref->lock));

			cprintf(CYAN, "Student %d has been allocated a seat in course %s\n", s->id, mypref->name);
			pthread_mutex_lock(&(mypref->cond_lock));
			pthread_cond_wait(&(mypref->finished), &(mypref->cond_lock));
			pthread_mutex_unlock(&(mypref->cond_lock));


			// Tut is over
			double prob = mypref->interest * s->CQ;
			if(mathamagic(prob)){
				// Student locks preference
				cprintf(WHITE, "Student %d has selected course %s permanently\n", s->id, mypref->name);
				pthread_exit(NULL);
			}
			else{
				// Student has withdrawn
				cprintf(RED, "Student %d has withdrawn from the course %s\n", s->id, mypref->name);

				// Change preferences
				Course *old = mypref;
				cur_pref++;
				if(cur_pref < 3){
					mypref = getCourseByID(s->pref[cur_pref], courses);
					// TODO print together
					cprintf(GREEN, "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n", \
							s->id, old->name, cur_pref, mypref->name, cur_pref+1);
				}
			}
		}
		else{
			pthread_mutex_unlock(&(mypref->lock));
			pthread_mutex_lock(&(mypref->cond_lock));
			pthread_cond_wait(&(mypref->finished), &(mypref->cond_lock));
			pthread_mutex_unlock(&(mypref->cond_lock));
		}
	}

	cprintf(YELLOW, "Student %d couldn't get any of his preferred courses\n", s->id);
	pthread_exit(NULL);
}

void *spawn_students(void *arg){

	int cur = 0;
	pair *parg = (pair*) arg;
	Student *Students = (Student*) parg->first;

	pthread_t *sim_student_t = malloc(sizeof(pthread_t) * num_students);

	for(int time = 0; ; time++, sleep(1)){

		if(cur == num_students) break;

		while(cur < num_students && Students[cur].time_filled == time){
			cprintf(BLUE, "Student %d has filled in preferences for course registration\n", Students[cur].id);
			
			// Spawn thread here
			// pthread_t *sim_student_t = malloc(sizeof(pthread_t));

			// Create argument which is Student + Courses base ptr
			pair *argptr = malloc(sizeof(pair));
			argptr->first = (void*) &Students[cur];
			argptr->second = (void*) parg->second;
			pthread_create(&sim_student_t[cur], NULL, sim_student, argptr);
			cur++;
		}
	}

	for(int i=0; i<num_students; i++){
		pthread_join(sim_student_t[i], NULL);
	}
	pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/src/utils.c ####################//

#include <stdio.h>
#include <stdarg.h>

#include "../include/utils.h"

void cprintf(const char *color, const char *format, ...) {
    printf("%s", color);

    va_list argptr;
    va_start(argptr, format);
    vprintf(format, argptr);
    va_end(argptr);

    printf("%s", RESET);
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/src/course.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <unistd.h>

#include "../include/course.h"
#include "../include/lab.h"
#include "../include/globals.h"
#include "../include/utils.h"

void create_course(Course *c){
	c->name = NULL;
	c->state = LOOKING_FOR_TA;
	c->interest = 0.0;
	c->max_slots = 0;
	c->occupied_slots = 0;
	c->allotted_slots = -1;
	c->labs_size = 0;
	c->labs = NULL;
	pthread_mutex_init(&(c->lock), NULL);
	pthread_mutex_init(&(c->cond_lock), NULL);
	pthread_cond_init(&(c->finished), NULL);

	pthread_mutex_lock(&(c->lock));
}

void delete_course(Course *c){
	pthread_mutex_destroy(&(c->lock));
	pthread_cond_destroy(&(c->finished));
	pthread_mutex_destroy(&(c->cond_lock));
	memset(c, 0, sizeof(Course));
	free(c);
}

Course *getCourseByID(int id, Course *C){
	if(id >= 0 && id < num_courses)
		return &C[id];
	return NULL;
}

void *sim_course(void *arg){
	pair *parg = (pair*) arg;
	Course *c = parg->first;
	Lab *Labs = parg->second;
	
	pthread_mutex_t *current_ta_lock;
	int holding_ta = -1;
	char *holding_lab = NULL;

	while(c->state != REMOVED){

		if(c->state == LOOKING_FOR_TA){

			// Search for free TA
			bool NO_FREE_TA = true;
			for(int i=0; i < c->labs_size; i++){
				int l_id = c->labs[i];
				Lab *lab = &Labs[l_id];

				for(int j=0; j < lab->num_students; j++){
					// TA in use by some other course, come back later
					if(pthread_mutex_trylock(&(lab->ta_lock[j])) != 0){
						NO_FREE_TA = false;
					}
					else{
						if(lab->ta[j] <= 0){
							pthread_mutex_unlock(&(lab->ta_lock[j]));
						}
						else{
							// Found TA
							NO_FREE_TA = false;
							lab->ta[j]--;
							current_ta_lock = &(lab->ta_lock[j]);
							holding_ta = j;
							holding_lab = lab->name;
							cprintf(RED, "TA %d from lab %s has been allocated to course %s for their %d(nt/rd/st) TA ship\n", \
									j, lab->name, c->name, lab->limit - lab->ta[j]);

							if(lab->ta[j] == 0){
								sem_post(&(lab->ta_free));
							}

							c->occupied_slots = 0;
							c->allotted_slots = rand()%(c->max_slots) + 1;

							cprintf(GREEN, "Course %s has been allocated %d seats\n", c->name, c->allotted_slots);
							goto ta_found;
						}
					}
				}
			}

			ta_found:
			if(NO_FREE_TA){
				c->state = REMOVED;
				break;
			}
			if(holding_ta != -1)
				c->state = WAITING_FOR_STUDENTS;
		}
		else if(c->state == WAITING_FOR_STUDENTS){
			pthread_mutex_unlock(&(c->lock));
			// TODO are we allowed to sleep here?
			sleep(1);

			// Conduct tutorial
			pthread_mutex_lock(&(c->lock));
			cprintf(YELLOW, "Tutorial has started for Course %s with %d seats filled out of %d\n", \
					c->name, c->occupied_slots, c->allotted_slots);
			c->state = TUT_IN_PROGRESS;
			sleep(2);
			pthread_mutex_unlock(current_ta_lock);
			current_ta_lock = NULL;
			cprintf(BLUE, "TA %d from lab %s has completed the tutorial and left the course %s\n", \
					holding_ta, holding_lab, c->name);
			holding_ta = -1;
			holding_lab = NULL;
			c->state = LOOKING_FOR_TA;
			pthread_mutex_lock(&(c->cond_lock));
			pthread_cond_broadcast(&(c->finished));
			pthread_mutex_unlock(&(c->cond_lock));
		}
	}
	pthread_mutex_unlock(&(c->lock));
	pthread_exit(NULL);
}

void read_courses(Course **C){
	Course *T = *C = calloc(num_courses, sizeof(Course));
	char buf[1024];

	for(int i=0; i<num_courses; i++){
		Course *c= &T[i];
		
		create_course(c);
		scanf("%s %lf %d %d", buf, &(c->interest), &(c->max_slots), &(c->labs_size));
		c->name = strdup(buf);

		c->labs = malloc(sizeof(int) * c->labs_size);
		for(int j=0; j < c->labs_size; j++){
			int t; scanf("%d", &t);
			c->labs[j] = t;
		}
	}
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/src/q1.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#include "../include/lab.h"
#include "../include/course.h"
#include "../include/student.h"
#include "../include/utils.h"

int num_students = 0; 
int num_labs = 0;
int num_courses = 0;
Course *Courses;
Student *Students;
Lab *Labs;

pthread_t spawn_students_t;

int main(void){
	
	scanf("%d %d %d", &num_students, &num_labs, &num_courses);

	read_courses(&Courses);
	read_students(&Students, num_students);
	read_labs(&Labs, num_labs);
	qsort(Students, num_students, sizeof(Student), student_time_cmp);

	pthread_t lab_thread_t[num_labs];
	for(int i=0; i<num_labs; i++)
		pthread_create(&lab_thread_t[i], NULL, sim_lab, &Labs[i]);

	pair *spawn_students_arg = malloc(sizeof(pair));
	spawn_students_arg->first = (void*) Students;
	spawn_students_arg->second = (void*) Courses;
	pthread_create(&spawn_students_t, NULL, spawn_students, spawn_students_arg);

	pthread_t course_thread_t[num_courses];
	pair course_args[num_courses];
	for(int i=0; i<num_courses; i++){
		course_args[i].first = &Courses[i];
		course_args[i].second = Labs;
		pthread_create(&course_thread_t[i], NULL, sim_course, &course_args[i]);
	}

	pthread_join(spawn_students_t, NULL);
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q1/src/lab.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#include "../include/lab.h"
#include "../include/utils.h"

void create_lab(Lab *l){
	l->name = NULL;
	l->num_students = 0;
	l->limit = 0;
	l->ta = NULL;
	l->ta_lock = NULL;
	sem_init(&(l->ta_free), 0, 0);
}

void destroy_lab(Lab *l){
	sem_destroy(&(l->ta_free));
	memset(l, 0, sizeof(Lab));
	free(l);
}

void *sim_lab(void *arg){
	Lab *lab = (Lab*) arg;
	int ct = 0;
	while(sem_wait(&(lab->ta_free))){
		ct++;
		if(ct == lab->num_students){
			cprintf(PINK, "Lab %s no longer has students available for TA ship\n", lab->name);
			pthread_exit(NULL);
		}
	}
	pthread_exit(NULL);
}

void read_labs(Lab **L, int num_labs){
	
	Lab *T = *L = calloc(num_labs, sizeof(Lab));
	char buf[1024];

	for(int i=0; i < num_labs; i++){
		Lab *l = &T[i];
		create_lab(l);
		scanf("%s %d %d", buf, &(l->num_students), &(l->limit));

		l->name = strdup(buf);
		l->ta = malloc(l->num_students * sizeof(int));
		l->ta_lock = malloc(l->num_students * sizeof(pthread_mutex_t));

		for(int j=0; j < l->num_students; j++){
			l->ta[j] = l->limit;
			pthread_mutex_init(&(l->ta_lock[j]), NULL);
		}
	}
}
