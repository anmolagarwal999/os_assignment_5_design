
//###########FILE CHANGE ./main_folder/Karthik Viswanathan_305865_assignsubmission_file_/2019113015_assignment_5/q1/q1.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// ensure that no course is being accessed by 2 ta's
// ensure that no ta is taing 2 courses

typedef struct student
{
    int id;                           // student id 
    int pref[3];                      // preference[i] stores the ith preference (course id) of the student
    int cur_pref;                     // current preference
    float caliber;                    // student caliber
    int time;                         // time when student fills preferences
    int alloced;                      // has the student been alloced this course
    int tut_done;                     // has tut been done for the student
    int c_status;
    pthread_mutex_t s;                // student mutex
    pthread_cond_t scond;             // signal condition
} student;
typedef struct course
{
    int id;                           // course id
    int cur_ta;                       // current ta of the course
    int cur_lab;                      // current lab of the corresponding ta
    int shortlisted_labs[100];        // shortlisted labs
    int shortlisted_lab_cnt;          // shortlisted lab count
    float interest;                   // interest for the course
    char *name;                       // name of the course
    int max_slots;                    // max slots for this course
    int alloced_students[1000];       // students alloced
    int alloced;                      // alloced students
    int tutafter;                     // after tut student counter
    int moved_out;                    // has it been moved out?
    int d;                            
    sem_t tut_alloc;                  // alloc a tut
    sem_t tut_done; 
    pthread_mutex_t alloced_lock;     // lock for students who have been allocated this course to change parameters
    pthread_mutex_t c;
    pthread_cond_t cond;
} course;
typedef struct lab
{
    int id;                           // lab id
    int ta_ids[100];                  // ta ids for ta's in the lab
    int ta_head;                      // ta queue head
    int total_tas;                    // total number of ta's
    char *name;                       // name of the lab
    int max_times;                    // number of times a ta can ta
    int ta_count[100];                // number of times a TA has TA-ed
    int ta_available[100];            // is the ta available?
    int done;                         // is lab done
} lab;

student s_argv[100];
course c_argv[100];
lab l_argv[100];

int students_done = 0;
int students_finished = 0;

pthread_t *c_tid;
pthread_t *s_tid;
pthread_t *l_tid;
pthread_mutex_t* c_mutexes;
pthread_mutex_t* s_mutexes;
pthread_mutex_t* l_mutexes;
pthread_mutex_t global_course_mutex;
pthread_mutex_t global_student_mutex;

// initialize counts
int student_count, course_count, lab_count;

int generate_random(int upper, int lower)
{
    return rand() % (upper - lower + 1) + upper;
}

void simulate_tutorial()
{
    // put a sleep command
    sleep(2);
}

void update_finish()
{
    pthread_mutex_lock(&global_student_mutex);
    students_finished++;
    pthread_mutex_unlock(&global_student_mutex);
}
void *simulate_student(void *arg)
{
    int id = (int) arg;
    s_argv[id].cur_pref = -1;
    sleep(s_argv[id].time);
    int pref[3];
    pref[0] = s_argv[id].pref[0];
    pref[1] = s_argv[id].pref[1];
    pref[2] = s_argv[id].pref[2];
    s_argv[id].tut_done = 0;
    pthread_mutex_lock(&s_argv[id].s);
    s_argv[id].c_status = 0;
    pthread_mutex_unlock(&s_argv[id].s);
    printf("Student %d has filled in preferences for course registration \n", id);
    for (int i = 0; i <= 2; i++)
    {
        /*
        if (c_argv[s_argv[id].pref[i]].moved_out)
        {
            printf("student %d changing preferences \n", id);
            continue;
        }
        */
        int cid = s_argv[id].pref[i];
        s_argv[id].c_status = 0;
        s_argv[id].cur_pref = cid;
        //wait for course to be assigned to student
        printf(ANSI_COLOR_RED "waiting for course allocation %d %s \n" ANSI_COLOR_RESET, id, c_argv[cid].name);
        sem_wait(&c_argv[cid].tut_alloc);
        pthread_mutex_lock(&s_argv[id].s);
        s_argv[id].c_status = 1;
        pthread_mutex_unlock(&s_argv[id].s);
        pthread_mutex_lock(&c_argv[cid].alloced_lock);
        if (!c_argv[cid].moved_out)
        printf(ANSI_COLOR_RED "Student %d has been allocated a seat in course %s \n" ANSI_COLOR_RESET, id, c_argv[cid].name);
        c_argv[cid].alloced++;
        if (c_argv[cid].alloced == c_argv[cid].d)
        {
            pthread_mutex_lock(&c_argv[cid].c);
            pthread_cond_signal(&c_argv[cid].cond);
            pthread_mutex_unlock(&c_argv[cid].c);
        }
        pthread_mutex_unlock(&c_argv[cid].alloced_lock);
        sem_wait(&c_argv[cid].tut_done);
        pthread_mutex_lock(&c_argv[cid].alloced_lock);
        c_argv[cid].tutafter++;
        if (c_argv[cid].d == c_argv[cid].tutafter)
        {
            pthread_mutex_lock(&c_argv[cid].c);
            pthread_cond_signal(&c_argv[cid].cond);
            pthread_mutex_unlock(&c_argv[cid].c);
        }
        pthread_mutex_unlock(&c_argv[cid].alloced_lock);
        float prob = c_argv[cid].interest * s_argv[id].caliber;
        int withdraw = (rand() % 100) < ((float) prob * 100);
        pthread_mutex_lock(&s_argv[id].s);
        pthread_mutex_unlock(&s_argv[id].s);
        if (!withdraw && c_argv[cid].moved_out == 0)
        {
            printf(ANSI_COLOR_BLUE "Student %d has selected the course %s permanently \n" ANSI_COLOR_RESET, id, c_argv[cid].name);
            pthread_mutex_lock(&global_student_mutex);
            students_finished++;
            pthread_mutex_unlock(&global_student_mutex);
            return NULL;
        }
        else if (withdraw || c_argv[cid].moved_out == 1)
        {
            /*
            if (c_argv[cid].moved_out == 0)
                printf(ANSI_COLOR_RED "student %d has withdrawn from the course %s \n" ANSI_COLOR_RESET, id, c_argv[cid].name);
            if (c_argv[cid].moved_out == 1)
                printf(ANSI_COLOR_RED "student %d has not selected moved out course %s \n" ANSI_COLOR_RESET, id, c_argv[cid].name);
            */
            if (i == 2)
            {
                printf(ANSI_COLOR_BLUE "Student %d couldn’t get any of his preferred courses \n" ANSI_COLOR_RESET , id);
                pthread_mutex_lock(&global_student_mutex);
                students_finished++;
                pthread_mutex_unlock(&global_student_mutex);
                return NULL;
            }
            printf(ANSI_COLOR_RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d) \n" ANSI_COLOR_RESET, id, c_argv[cid].name, i, c_argv[pref[i+1]].name, i+1);
        }
    }
    return NULL;
}
void lock(int id)
{
    // OLD-------------
    for (int i = 0; i < student_count; i++)
    {
        if (s_argv[i].cur_pref == id)
        {
            pthread_mutex_lock(&s_argv[i].s);
        }
    }
}
void unlock(int id)
{
    // OLD--------------
    for (int i = 0; i < student_count; i++)
    {
        if (s_argv[i].cur_pref == id)
        {
            pthread_mutex_unlock(&s_argv[i].s);
        }
    }
}
void cond_wait_alloced(int id)
{
    pthread_mutex_lock(&c_argv[id].c);
    while(c_argv[id].alloced < c_argv[id].d)
    {
        pthread_cond_wait(&c_argv[id].cond, &c_argv[id].c);
    }
    pthread_mutex_unlock(&c_argv[id].c);
}
void cond_wait_tut(int id)
{
    pthread_mutex_lock(&c_argv[id].c);
    while(c_argv[id].tutafter < c_argv[id].d)
    {
        pthread_cond_wait(&c_argv[id].cond, &c_argv[id].c);
    }
    pthread_mutex_unlock(&c_argv[id].c);
}
void alloc_students(int id, int d)
{
    /* old----
    int alloced = 0;
    for (int i = 0; i < student_count; i++)
    {
        //printf("pref: %d, id: %d", s_argv[i].cur_pref, id);
        if (s_argv[i].cur_pref == id)
        {
            //printf("alloced: %d %s \n", i, c_argv[id].name);
            s_argv[i].alloced = 1;
            pthread_mutex_lock(&s_argv[i].s);
            pthread_cond_signal(&s_argv[i].scond);
            pthread_mutex_unlock(&s_argv[i].s);
            c_argv[id].alloced_students[alloced] = i;
            alloced++;
            if (alloced == slots)
            {
                break;
            }
        }
    }
    return alloced;
    ---*/
    for (int i = 0; i < d; i++)
    {
        sem_post(&c_argv[id].tut_alloc);
    }
    cond_wait_alloced(id);
    // done
}
void dealloc_students(int id, int d)
{
    for (int i = 0; i < d; i++)
    {
        sem_post(&c_argv[id].tut_done);
    }
    //printf("waiting to finish for course %s \n", c_argv[id].name);
    cond_wait_tut(id);
}
int cnt_students(int id)
{
    int alloced = 0;
    for (int i = 0; i < student_count; i++)
    {
        pthread_mutex_lock(&s_argv[id].s);
        if (s_argv[i].cur_pref == id && s_argv[i].c_status != 1)
        {
            //printf("%d \n", i);
            alloced++;
        }
        pthread_mutex_unlock(&s_argv[id].s);
    }
    return alloced;
}
void set_status(int id)
{
    // OLD---------------
    for (int i = 0; i < student_count; i++)
    {
        if (s_argv[id].cur_pref == id && s_argv[i].c_status == 1)
        {
            s_argv[i].c_status = 0;
        }
    }
}
void tut_over(int id, int alloced)
{
    // OLD------------------
    for (int i = 0; i < alloced; i++)
    {
        int sid = c_argv[id].alloced_students[i];
        s_argv[sid].tut_done = 1;
        //printf("tut over: %d  %s \n", i, c_argv[id].name);
    }
    for (int i = 0; i < student_count; i++)
    {
        if (s_argv[i].cur_pref == id)
        {
            pthread_cond_signal(&s_argv[i].scond);
        }
    }
}
void exit_course(int id)
{
    // OLD----------------
    for (int i = 0; i < student_count; i++)
    {
        if (s_argv[i].cur_pref == id)
        {
            s_argv[i].tut_done = 2;
            s_argv[i].alloced = 1;
            pthread_cond_signal(&s_argv[i].scond);
        }
    }
}
void revert(int id, int d)
{
    // OLD-----------------
    for (int i = 0; i < d; i++)
    {
        sem_post(&c_argv[id].tut_alloc);
    }
    cond_wait_alloced(id);
    for (int i = 0; i < d; i++)
    {
        sem_post(&c_argv[id].tut_done);
    }
}
void *simulate_course(void *arg)
{
    int id = (int) arg;
    while(1)
    {
        if (students_finished == student_count)
        {
            printf(ANSI_COLOR_GREEN "shutting off %s \n" ANSI_COLOR_RESET, c_argv[id].name);
            return NULL;
        }
        int waiting_students = cnt_students(id);
        //printf("in here for course %s %d \n", c_argv[id].name, waiting_students);
        if (waiting_students <= 0)
        {
            continue;
        }
        if (c_argv[id].moved_out == 1)
        {
            int d = waiting_students;
            c_argv[id].alloced = d;
            c_argv[id].d = d;
            revert(id, d);
            continue;
        }
        //printf("in here for course %s %d \n", c_argv[id].name, waiting_students);
        int cur_ta = -1;
        int cur_lab = -1;
        int d = 0;
        int flag = 0;
        int full_ta = 0;
        int full_lab = 0;
        pthread_mutex_lock(&global_course_mutex);
        for (int i = 0; i < c_argv[id].shortlisted_lab_cnt; i++)
        {
            int lid = c_argv[id].shortlisted_labs[i];
            full_ta = 0;
            if (l_argv[lid].ta_head >= 0)
            {
                cur_lab = l_argv[lid].id;
                cur_ta = l_argv[cur_lab].ta_ids[0];
                l_argv[cur_lab].ta_count[cur_ta]++;
                for (int i = 1; i <= l_argv[cur_lab].ta_head; i++)
                {
                    l_argv[cur_lab].ta_ids[i - 1] = l_argv[cur_lab].ta_ids[i];
                }
                l_argv[cur_lab].ta_head--;
                break;
            }
            for (int j = 0; j < l_argv[lid].total_tas; j++)
            {
                if (l_argv[lid].ta_count[j] == l_argv[lid].max_times)
                {
                    full_ta++;
                }
            }
            if (l_argv[lid].total_tas == full_ta)
            {
                full_lab++;
                if (l_argv[lid].done == 0)
                {
                    printf("Lab %s no longer has students available for TA ship \n", l_argv[lid].name);
                    l_argv[lid].done = 1;
                }
            }
        }
        pthread_mutex_unlock(&global_course_mutex);
        d = generate_random(c_argv[id].max_slots, 1);
        int max_slots = d; 
        if (waiting_students < d || d == 0)
        {
            d = waiting_students;
        }
        c_argv[id].alloced = 0;
        c_argv[id].d = d;
        if (full_lab == c_argv[id].shortlisted_lab_cnt && c_argv[id].d > 0)
        {
            c_argv[id].moved_out = 1;
            printf(ANSI_COLOR_GREEN "Course %s does not have any TA's eligible and is removed from course offerings \n" ANSI_COLOR_RESET, c_argv[id].name);
            revert(id, d);
            continue;
        }
        if (cur_ta == -1 && cur_lab == -1)
        {
            continue;
        }
         printf(ANSI_COLOR_GREEN "Course %s has been allocated %d seats \n" ANSI_COLOR_GREEN, c_argv[id].name, max_slots);
        //printf("tut alloc going on: %s %d \n", c_argv[id].name, waiting_students);
        // allocate students
        alloc_students(id, d);
        //printf("tut alloc done: %s %d \n", c_argv[id].name, waiting_students);
        printf(ANSI_COLOR_GREEN "Tutorial has started for course %s with %d slots filled out of %d\n" ANSI_COLOR_RESET, c_argv[id].name, d, max_slots);
        //printf(ANSI_COLOR_GREEN "ta %d from %s chosen to ta %s with num students : %d \n" ANSI_COLOR_RESET, cur_ta, l_argv[cur_lab].name, c_argv[id].name, d);
        simulate_tutorial();
        //set_status(id);
        c_argv[id].tutafter = 0;
        printf(ANSI_COLOR_GREEN "TA %d from lab %s has completed the tutorial for the course %s \n" ANSI_COLOR_RESET, cur_ta, l_argv[cur_lab].name, c_argv[id].name);
        // deallocate these students from the tutorial
        dealloc_students(id, d);
        pthread_mutex_lock(&global_course_mutex);
        if (l_argv[cur_lab].ta_count[cur_ta] < l_argv[cur_lab].max_times)
        {
            //printf(ANSI_COLOR_MAGENTA"ta count of current ta: %d %s %d \n" ANSI_COLOR_RESET, cur_ta, l_argv[cur_lab].name, l_argv[cur_lab].ta_count[cur_ta]);
            l_argv[cur_lab].ta_head++;
            l_argv[cur_lab].ta_ids[l_argv[cur_lab].ta_head] = cur_ta;
        }
        pthread_mutex_unlock(&global_course_mutex);
        //printf("finished iter for the course %s \n", c_argv[id].name);
    }
}

int main(void)
{
    srand(time(NULL));

    // create threads and mutexes
    pthread_t *s_tid = (pthread_t *) malloc(sizeof(pthread_t) * student_count);
    s_mutexes = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t) * student_count);

    pthread_t *c_tid = (pthread_t *) malloc(sizeof(pthread_t) * course_count);
    c_mutexes = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t) * course_count);

    int line_size = 1000;
    scanf("%d%d%d", &student_count, &lab_count, &course_count);
    getchar();
    for (int i = 1; i <= course_count; i++)
    {
        char *line = (char *)malloc(10000);
        char *token = (char *)malloc(100);
        c_argv[i - 1].id = i - 1;
        getline(&line, &line_size, stdin);
        token = strtok(line, " ");
        int k = 0;
        while (token != NULL)
        {
            if (k == 0)
            {
                c_argv[i - 1].name = (char *)malloc(100);
                strcpy(c_argv[i - 1].name, token);
            }
            if (k == 1)
            {
                c_argv[i - 1].interest = atof(token);
            }
            if (k == 2)
            {
                c_argv[i - 1].max_slots = atoi(token);
            }
            if (k == 3)
            {
                c_argv[i - 1].shortlisted_lab_cnt = atoi(token);
            }
            else if (k > 3)
            {
                c_argv[i - 1].shortlisted_labs[k - 4] = atoi(token);
            }
            k++;
            token = strtok(NULL, " ");
        }
        sem_init(&c_argv[i - 1].tut_alloc, 0, 0);
        sem_init(&c_argv[i - 1].tut_done, 0, 0);
        pthread_mutex_init(&c_argv[i - 1].c, NULL);
        pthread_cond_init(&c_argv[i - 1].cond, NULL);
        c_argv[i - 1].alloced = 0;
        c_argv[i - 1].tutafter = 0;
        free(line);
        free(token);
    }
    for (int i = 1; i <= student_count; i++)
    {
        s_argv[i - 1].id = i - 1;
        scanf("%f %d %d %d %d", &(s_argv[i - 1].caliber), &(s_argv[i - 1].pref[0]), &(s_argv[i - 1].pref[1]), &(s_argv[i - 1].pref[2]), &(s_argv[i - 1].time));
        pthread_mutex_init(&s_argv[i - 1].s, NULL);
    }
    getchar();
    for (int i = 1; i <= lab_count; i++)
    {
        char *line = (char *)malloc(1000);   
        l_argv[i - 1].id = i - 1;
        getline(&line, &line_size, stdin);
        char *token = strtok(line, " ");
        int k = 0;
        while (token != NULL)
        {
            if (k == 0)
            {
                l_argv[i - 1].name = (char *)malloc(100);
                strcpy(l_argv[i - 1].name, token);
            }
            if (k == 1)
            {
                l_argv[i - 1].total_tas = atoi(token);
            }
            if (k == 2)
            {
                l_argv[i - 1].max_times = atoi(token);
            }
            k++;
            token = strtok(NULL, " ");
        }
        l_argv[i - 1].ta_head = l_argv[i - 1].total_tas - 1;
        for (int j = 0; j <= l_argv[i - 1].ta_head; j++)
        {
            l_argv[i - 1].ta_ids[j] = j;
        }
        for (int j = 0; j < l_argv[i - 1].total_tas; j++)
        {
            l_argv[i - 1].ta_available[j] = 1;
        }
        l_argv[i - 1].done = 0;
        free(line);
    }
    printf("\nSimulation started\n");
    // initialize threads and mutexes
    for (int i = 0; i < student_count; i++)
    {
        if (pthread_create(&s_tid[i], NULL, &simulate_student, (void *)i))
        {
            perror("Error initializing students");
            return -1;
        }
        if (pthread_mutex_init(&s_mutexes[i], NULL))
        {
            perror("Error initializing student mutexes");
            return -1;
        }
    }
    for (int i = 0; i < course_count; i++)
    {
        if (pthread_create(&c_tid[i], NULL, &simulate_course, (void*) i)) 
        {
            perror("Error initializing courses");
            return -1;
        }
        if (pthread_mutex_init(&c_mutexes[i], NULL)) 
        {
            perror("Error initializing course mutexes");
            return -1;
        }
    }
    // join the threads
    for (int i = 0; i < student_count; i++) 
    {
        pthread_join(s_tid[i], NULL);
    }
    for (int i = 0; i < course_count; i++) 
    {
        pthread_join(c_tid[i], NULL);
    }
    printf("Simulation over \n");
}