
//###########FILE CHANGE ./main_folder/SHREYAS PRADHAN_305972_assignsubmission_file_/2019113004/q1/q1_header.h ####################//

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>

#define TUT_TIME 5

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define DEBUG 0

typedef struct course
{
    int id;
    char name[25];
    int course_max_slots;
    float interest;
    int n_labs;
    int *labs;
    int ta;
    int withdrawn;

    pthread_mutex_t course_lock;

} course;

typedef struct lab
{
    int id;
    char name[20];
    int n_tas;
    int max_times;

    // TA section - each array is of size n_tas
    int *taught_courses;
    int *is_free;

    pthread_mutex_t *ta_lock;
    pthread_cond_t *ta_cond; // so courses can wait for a TA.

} lab;

typedef struct student
{
    int id;
    float reg_time;
    int pref[3];
    int cur_pref;
    float calibre;
    int is_waiting;

    pthread_mutex_t student_lock;
    pthread_cond_t student_cond; // so students can wait for a course

} student;

int n_students;
student *students;
pthread_t *student_threads;
int students_finished;
pthread_mutex_t students_lock;

int n_courses;
course *courses;
pthread_t *course_threads;

int n_labs;
lab *ta_labs;

// aux variables
pthread_mutex_t print_lock;

void init();
void *course_thread(void *args);
void *student_thread(void *args);

//###########FILE CHANGE ./main_folder/SHREYAS PRADHAN_305972_assignsubmission_file_/2019113004/q1/q1.c ####################//

#include "q1_header.h"

int main()
{
    // take input and initialise stuff
    init();

    //start threads

    // student threads
    for (int s = 0; s < n_students; s++)
    {
        pthread_create(&student_threads[s], NULL, student_thread, &students[s]);
    }
    // course threads
    for (int c = 0; c < n_courses; c++)
    {
        pthread_create(&course_threads[c], NULL, course_thread, &courses[c]);
    }

    // waiting for threads to finish execution

    // student threads
    for (int s = 0; s < n_students; s++)
    {
        pthread_join(student_threads[s], NULL);
    }
    // course threads
    for (int c = 0; c < n_courses; c++)
    {
        pthread_join(course_threads[c], NULL);
    }

    // end of program

    return 0;
}

void *course_thread(void *args)
{
    course cur_course = *(course *)args;

    while (1)
    {
        pthread_mutex_lock(&students_lock);
        if (students_finished == n_students)
        {
            // all students finished so simulation must be stopped

            pthread_mutex_unlock(&students_lock);

            if (!DEBUG)
            {
                pthread_mutex_lock(&print_lock);
                printf("Course %s stopped as all students finalised/couldn't get their choices.\n", cur_course.name);
                pthread_mutex_unlock(&print_lock);
            }
            return NULL;
        }
        pthread_mutex_unlock(&students_lock);

        int cur_lab = -1;
        int cur_ta = -1;

        // checking if any TA free
        // if (DEBUG)
        // {
        //     printf("%sta_find%s course %s\n", YELLOW, RESET, cur_course.name);
        // }
        for (int l = 0; l < cur_course.n_labs; l++)
        {
            for (int ta = 0; ta < ta_labs[l].n_tas; ta++)
            {
                pthread_mutex_lock(&ta_labs[l].ta_lock[ta]);

                if (ta_labs[l].taught_courses[ta] < ta_labs[l].max_times && ta_labs[l].is_free[ta])
                {
                    cur_lab = l;
                    cur_ta = ta;
                    ta_labs[l].is_free[ta] = 0;
                    ta_labs[l].taught_courses[ta]++;

                    if (!DEBUG)
                    {
                        pthread_mutex_lock(&print_lock);
                        // event 11 - part 1/2
                        printf(RED "TA %d from lab %s has been allocated to course %s for %d TA ship\n" RESET, ta, ta_labs[l].name, cur_course.name, ta_labs[l].taught_courses[ta]);
                        pthread_mutex_unlock(&print_lock);
                    }
                }

                pthread_mutex_unlock(&ta_labs[l].ta_lock[ta]);

                if (cur_ta != -1)
                {
                    break;
                }
            }

            if (cur_ta != -1)
            {
                break;
            }
        }

        // if TA not free then wait
        if (cur_ta == -1)
        {
            for (int l = 0; l < cur_course.n_labs; l++)
            {
                for (int ta = 0; ta < ta_labs[l].n_tas; ta++)
                {
                    pthread_mutex_lock(&ta_labs[l].ta_lock[ta]);

                    while (ta_labs[l].taught_courses[ta] < ta_labs[l].max_times && !ta_labs[l].is_free[ta])
                    {
                        // if (DEBUG)
                        // {
                        //     printf("%swaiting%s - course %s for ta %d from lab %d\n", YELLOW, RESET, cur_course.name, ta, l);
                        // }
                        pthread_cond_wait(&ta_labs[l].ta_cond[ta], &ta_labs[l].ta_lock[ta]);
                        // if (DEBUG)
                        // {
                        //     printf("%sresumed%s - course %s for ta %d from lab %d\n", CYAN, RESET, cur_course.name, ta, l);
                        // }
                    }

                    if (ta_labs[l].taught_courses[ta] < ta_labs[l].max_times && ta_labs[l].is_free[ta])
                    {
                        cur_lab = l;
                        cur_ta = ta;
                        ta_labs[l].is_free[ta] = 0;
                        ta_labs[l].taught_courses[ta]++;
                        if (!DEBUG)
                        {
                            pthread_mutex_lock(&print_lock);
                            // event 11 - part 2/2
                            printf(RED "TA %d from lab %s has been allocated to course %s for %d TA ship\n" RESET, ta, ta_labs[l].name, cur_course.name, ta_labs[l].taught_courses[ta]);
                            pthread_mutex_unlock(&print_lock);
                        }
                    }

                    pthread_mutex_unlock(&ta_labs[l].ta_lock[ta]);

                    if (cur_ta != -1)
                    {
                        break;
                    }
                }
                if (cur_ta != -1)
                {
                    break;
                }
                else
                {
                    // lab 1 has no TAs now
                    if (!DEBUG)
                    {
                        pthread_mutex_lock(&print_lock);
                        // event 12
                        printf(GREEN "Lab %s no longer has students available for TA ship\n" RESET, ta_labs[l].name);
                        pthread_mutex_unlock(&print_lock);
                    }
                }
            }
        }

        // if still not able to find any TA then end course
        if (cur_ta == -1)
        {
            // end course code here

            if (DEBUG)
            {
                printf("%swithdrw 1 %s course %s\n", RED, RESET, cur_course.name);
            }

            // first lock everything
            pthread_mutex_lock(&courses[cur_course.id].course_lock);

            if (DEBUG)
            {
                printf("%swithdrw 2 %s course %s\n", RED, RESET, cur_course.name);
            }

            courses[cur_course.id].withdrawn = 1;

            for (int s = 0; s < n_students; s++)
            {
                if (DEBUG)
                {
                    printf("%swithdrw%s student %d course %s\n", RED, RESET, s, cur_course.name);
                }

                pthread_mutex_lock(&students[s].student_lock);

                if (students[s].cur_pref == cur_course.id)
                {
                    students[s].is_waiting = 0;

                    // if (DEBUG)
                    // {
                    //     printf("withdrw - student %d course %s\n", s, cur_course.name);
                    // }
                    pthread_cond_signal(&students[s].student_cond);
                }
                pthread_mutex_unlock(&students[s].student_lock);
            }

            pthread_mutex_unlock(&courses[cur_course.id].course_lock);
            if (!DEBUG)
            {
                pthread_mutex_lock(&print_lock);
                // event 10
                printf(YELLOW "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" RESET, cur_course.name);
                pthread_mutex_unlock(&print_lock);
            }

            if (DEBUG)
            {
                printf("%swithdne%s course %s\n", GREEN, RESET, cur_course.name);
            }

            return NULL;
        }

        // TA is now selected
        // if (DEBUG)
        // {
        //     printf("%stafound%s course %s\n", BLUE, RESET, cur_course.name);
        // }
        int total_slots = (rand() % cur_course.course_max_slots) + 1;
        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            // event 7
            printf(BLUE "Course %s has been allocated %d seats\n" RESET, cur_course.name, total_slots);
            pthread_mutex_unlock(&print_lock);
        }
        int filled_slots = 0;
        int *cur_students = malloc(sizeof(int) * total_slots);

        for (int s = 0; s < n_students; s++)
        {
            pthread_mutex_lock(&students[s].student_lock);

            if (students[s].cur_pref == cur_course.id && students[s].is_waiting)
            {
                cur_students[filled_slots] = s;
                filled_slots++;
                if (!DEBUG)
                {
                    pthread_mutex_lock(&print_lock);
                    // event 2
                    printf(MAGENTA "Student %d has been allocated a seat in course %s\n" RESET, s, cur_course.name);
                    pthread_mutex_unlock(&print_lock);
                }
            }

            pthread_mutex_unlock(&students[s].student_lock);

            if (filled_slots >= total_slots)
            {
                break;
            }
        }

        // students have been selected now lets start tut
        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            // event 8
            printf(CYAN "Tutorial has started for Course %s with %d seats filled out of %d\n" RESET, cur_course.name, filled_slots, total_slots);
            pthread_mutex_unlock(&print_lock);
        }

        sleep(TUT_TIME);

        // tut done. now free the TA and all the students
        pthread_mutex_lock(&ta_labs[cur_lab].ta_lock[cur_ta]);
        ta_labs[cur_lab].is_free[cur_ta] = 1;
        pthread_cond_broadcast(&ta_labs[cur_lab].ta_cond[cur_ta]);
        pthread_mutex_unlock(&ta_labs[cur_lab].ta_lock[cur_ta]);

        if (!DEBUG)
        {
            pthread_mutex_lock(&print_lock);
            // event 9
            printf(MAGENTA "TA %d from lab %s has completed the tutorial and left the course %s\n" RESET, cur_ta, ta_labs[cur_lab].name, cur_course.name);
            pthread_mutex_unlock(&print_lock);
        }

        for (int i = 0; i < filled_slots; i++)
        {
            pthread_mutex_lock(&students[cur_students[i]].student_lock);

            students[cur_students[i]].is_waiting = 0;
            // if (DEBUG)
            // {
            //     printf("tutdone - student %d course %s\n", cur_students[i], cur_course.name);
            // }
            pthread_cond_signal(&students[cur_students[i]].student_cond);

            pthread_mutex_unlock(&students[cur_students[i]].student_lock);
        }

        // freeing the allocated temp array
        free(cur_students);
    }
}

void *student_thread(void *args)
{
    student cur_student = *(student *)args;

    // wait until registration time
    sleep(cur_student.reg_time);
    if (!DEBUG)
    {
        pthread_mutex_lock(&print_lock);
        // event 1
        printf(YELLOW "Student %d has filled in preferences for course registration\n" RESET, cur_student.id);
        pthread_mutex_unlock(&print_lock);
    }

    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_lock(&students[cur_student.id].student_lock);
        students[cur_student.id].cur_pref = students[cur_student.id].pref[i];
        pthread_mutex_unlock(&students[cur_student.id].student_lock);

        pthread_mutex_lock(&courses[students[cur_student.id].cur_pref].course_lock);
        int has_course_withdrawn = courses[students[cur_student.id].cur_pref].withdrawn;
        pthread_mutex_unlock(&courses[students[cur_student.id].cur_pref].course_lock);

        pthread_mutex_lock(&students[cur_student.id].student_lock);
        if (!has_course_withdrawn)
        {
            students[cur_student.id].is_waiting = 1;
        }
        else
        {
            students[cur_student.id].is_waiting = 0;
        }

        while (students[cur_student.id].is_waiting)
        {
            // if (DEBUG)
            // {
            //     printf("%swaiting%s - student %d course %s\n", RED, RESET, cur_student.id, courses[students[cur_student.id].cur_pref].name);
            // }

            pthread_cond_wait(&students[cur_student.id].student_cond, &students[cur_student.id].student_lock);

            // if (DEBUG)
            // {
            //     printf("%sresumed%s - student %d course %s\n", GREEN, RESET, cur_student.id, courses[students[cur_student.id].cur_pref].name);
            // }
        }
        pthread_mutex_unlock(&students[cur_student.id].student_lock);

        pthread_mutex_lock(&courses[students[cur_student.id].cur_pref].course_lock);
        has_course_withdrawn = courses[students[cur_student.id].cur_pref].withdrawn;
        pthread_mutex_unlock(&courses[students[cur_student.id].cur_pref].course_lock);

        if (!has_course_withdrawn)
        {
            // decide wether to continue
            float finalising_prob = cur_student.calibre * courses[students[cur_student.id].cur_pref].interest;
            float outcome = (float)rand() / (float)(RAND_MAX);

            if (outcome <= finalising_prob)
            {
                // end student thread
                if (!DEBUG)
                {
                    pthread_mutex_lock(&print_lock);
                    // event 5
                    printf(CYAN "Student %d has selected course %s permanently\n" RESET, cur_student.id, courses[students[cur_student.id].cur_pref].name);
                    pthread_mutex_unlock(&print_lock);
                }

                pthread_mutex_lock(&students[cur_student.id].student_lock);
                students[cur_student.id].cur_pref = -1;
                pthread_mutex_unlock(&students[cur_student.id].student_lock);

                pthread_mutex_lock(&students_lock);
                students_finished++;
                pthread_mutex_unlock(&students_lock);

                return NULL;
            }
            if (!DEBUG)
            {
                // did not want to continue course so move on to next course
                pthread_mutex_lock(&print_lock);
                // event 3
                printf("Student %d has withdrawn from course %s\n", cur_student.id, courses[students[cur_student.id].cur_pref].name);
                pthread_mutex_unlock(&print_lock);
            }
        }

        // change course
        if (i < 2)
        {
            if (!DEBUG)
            {
                pthread_mutex_lock(&print_lock);
                // event 4
                printf(RED "Student %d has changed current preference from %s (priority %d) to %s (priority %d)\n" RESET, cur_student.id, courses[students[cur_student.id].pref[i]].name, i, courses[students[cur_student.id].pref[i + 1]].name, i + 1);
                pthread_mutex_unlock(&print_lock);
            }
        }
    }

    pthread_mutex_unlock(&students[cur_student.id].student_lock);
    if (!DEBUG)
    {
        pthread_mutex_lock(&print_lock);
        // event 6
        printf("Student %d couldn’t get any of his preferred courses\n", cur_student.id);
        pthread_mutex_unlock(&print_lock);
    }

    pthread_mutex_lock(&students_lock);
    students_finished++;
    pthread_mutex_unlock(&students_lock);

    return NULL;
}

void init()
{
    // input total number of students, labs, courses
    scanf("%d %d %d", &n_students, &n_labs, &n_courses);

    students = malloc(sizeof(student) * n_students);
    ta_labs = malloc(sizeof(lab) * n_labs);
    courses = malloc(sizeof(course) * n_courses);

    // course info
    for (int i = 0; i < n_courses; i++)
    {
        scanf("%s %f %d %d", courses[i].name, &courses[i].interest, &courses[i].course_max_slots, &courses[i].n_labs);

        courses[i].id = i;

        courses[i].labs = malloc(sizeof(int) * courses[i].n_labs);

        for (int j = 0; j < courses[i].n_labs; j++)
        {
            scanf("%d", &courses[i].labs[j]);
        }

        courses[i].ta = -1;
        courses[i].withdrawn = 0;

        pthread_mutex_init(&courses[i].course_lock, NULL);
    }

    // student info
    for (int i = 0; i < n_students; i++)
    {
        scanf("%f %d %d %d %f", &students[i].calibre, &students[i].pref[0], &students[i].pref[1], &students[i].pref[2], &students[i].reg_time);

        students[i].id = i;

        students[i].cur_pref = -1;
        students[i].is_waiting = 0;

        pthread_mutex_init(&students[i].student_lock, NULL);
        pthread_cond_init(&students[i].student_cond, NULL);
    }

    // lab info
    for (int i = 0; i < n_labs; ++i)
    {
        scanf("%s %d %d", ta_labs[i].name, &ta_labs[i].n_tas, &ta_labs[i].max_times);

        ta_labs[i].id = i;

        ta_labs[i].taught_courses = malloc(sizeof(int) * ta_labs[i].n_tas);
        ta_labs[i].is_free = malloc(sizeof(int) * ta_labs[i].n_tas);
        ta_labs[i].ta_lock = malloc(sizeof(pthread_mutex_t) * ta_labs[i].n_tas);
        ta_labs[i].ta_cond = malloc(sizeof(pthread_cond_t) * ta_labs[i].n_tas);

        // initialising tas
        for (int j = 0; j < ta_labs[i].n_tas; j++)
        {
            ta_labs[i].taught_courses[j] = 0;
            ta_labs[i].is_free[j] = 1;
            pthread_mutex_init(&ta_labs[i].ta_lock[j], NULL);
            pthread_cond_init(&ta_labs[i].ta_cond[j], NULL);
        }
    }

    students_finished = 0;
    pthread_mutex_init(&students_lock, NULL);

    // allocating space for threads array
    course_threads = malloc(sizeof(pthread_t) * n_courses);
    student_threads = malloc(sizeof(pthread_t) * n_students);

    // aux
    pthread_mutex_init(&print_lock, NULL);
}
