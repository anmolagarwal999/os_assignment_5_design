
//###########FILE CHANGE ./main_folder/Anusha Nath Roy_305788_assignsubmission_file_/q1/q1.c ####################//

// ---------------- headers ------------------------
#include <stdio.h>
#include <pwd.h>
#include <signal.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#define ll long long int
#include <pthread.h>

#define new (type, length) malloc(sizeof(type) * (length))
#define delete (x) free(x)
#define max_num_courses 100
#define max_num_students 100
#define max_num_Labs 100
#define max_ta_num 100
int num_students = 0;
int num_Labs = 0;
int num_courses = 0;
pthread_mutex_t print_terminal = PTHREAD_MUTEX_INITIALIZER;
// ---------------- colors -------------------------
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

// ---------------- structs ------------------------

//course
struct course
{
    pthread_mutex_t c_lock; // lock
    pthread_cond_t c_cond;  // condtion
    pthread_mutex_t t_lock; // lock
    pthread_cond_t t_cond;  // condtion
    pthread_t c_thread;     // the thread
    char name[100];
    float interestQ;
    int maxTASlots;
    int maxLabsTA;
    int LabIDs[200];
    int hasTA;
    int currentlab;
    int currentTA;
    int nStudents;
    int isDeleted;
};

typedef struct course course;
course courses[max_num_courses];

//student
struct student
{
    pthread_mutex_t s_lock; // lock
    pthread_cond_t s_cond;  // condtion
    pthread_t s_thread;     // the thread
    float calibreQ;
    int p1;
    int p2;
    int p3;
    int time;
    int currentpval;       // curent preference value
    int currentpi;        // current preference index
};

typedef struct student student;
student students[max_num_students];

struct TA
{
    pthread_mutex_t t_lock; // lock
    pthread_cond_t t_cond;  // condtion
    pthread_t t_thread;     // the thread
    int turns;
    int available;
};

typedef struct TA TA;

//lab
struct lab
{
    pthread_mutex_t l_lock; // lock
    pthread_cond_t l_cond;  // condtion
    pthread_t l_thread;     // the thread
    char name[100];
    int nTAs;
    int maxTAn;
    int TAused;
    TA labTAs[max_ta_num];
    int notAvailable;
};

typedef struct lab lab;
lab Labs[max_num_Labs];

//---------------- printTest -------------------

void printT()
{
}

//---------------- course utility---------------------
int searchTA(int course_index)
{
    int TAfound = 0;
    pthread_mutex_lock(&courses[course_index].c_lock);
    for (int i = 0; i < courses[course_index].maxLabsTA; i++)
    {

        int labID = courses[course_index].LabIDs[i];
        //int TAfoundLab = 0;
        if (labID >= max_num_Labs)
        {
            printf(ANSI_COLOR_RED "error: inputed wrong lab index\n" ANSI_COLOR_RESET);
        }

        pthread_mutex_lock(&Labs[labID].l_lock);
        if (Labs[labID].TAused < Labs[labID].nTAs * Labs[labID].maxTAn)
        {
            for (int j = 0; j < Labs[labID].nTAs; j++)
            {
                pthread_mutex_lock(&Labs[labID].labTAs[j].t_lock);
                if (Labs[labID].labTAs[j].available && (Labs[labID].labTAs[j].turns < Labs[labID].maxTAn))
                {
                    int TAassigned = j;
                    int TAshipNumber = Labs[labID].labTAs[j].turns + 1;
                    Labs[labID].labTAs[j].turns++;
                    Labs[labID].TAused++;
                    courses[course_index].hasTA = 1;
                    courses[course_index].currentTA = TAassigned;
                    courses[course_index].currentlab = labID;
                    Labs[labID].labTAs[j].available = 0;
                    TAfound = 1;
                    pthread_mutex_lock(&print_terminal);
                    printf(ANSI_COLOR_BLUE "TA %d from lab %s has been allocated to course %s for his %d TA ship\n" ANSI_COLOR_RESET, TAassigned, Labs[labID].name, courses[course_index].name, TAshipNumber);
                    pthread_mutex_unlock(&print_terminal);
                }
                pthread_mutex_unlock(&Labs[labID].labTAs[j].t_lock);
            }
        }
        else
        {
            pthread_mutex_lock(&print_terminal);
            if (Labs[labID].notAvailable != -1)
                printf(ANSI_COLOR_RED "Lab %s no longer has students available for TA ship\n" ANSI_COLOR_RESET, Labs[labID].name);
            pthread_mutex_unlock(&print_terminal);
            Labs[labID].notAvailable = -1;
        }

        pthread_mutex_unlock(&Labs[labID].l_lock);
    }
    pthread_mutex_unlock(&courses[course_index].c_lock);
    return TAfound;
}

void take_tut(int index)
{
    time_t t;
    srand((unsigned)time(&t));
    pthread_mutex_lock(&courses[index].c_lock);
    int W = courses[index].nStudents;
    if (courses[index].currentlab != -1 && courses[index].currentTA != -1)
    {
        int D = (rand() % courses[index].maxTASlots) + 1;
        printf(ANSI_COLOR_YELLOW "Course %s has been allocated %d seats\n" ANSI_COLOR_RESET, courses[index].name, D);
        int seatsfilled = 0;
        if (W >= D)
        {
            courses[index].nStudents -= D;
            for(int k=0 ; k<D; k++){
                pthread_cond_signal(&courses[index].c_cond);
            }
            seatsfilled = D;
        }
        else
        {
            courses[index].nStudents = 0;
            for(int k=0 ; k<W; k++){
                pthread_cond_signal(&courses[index].c_cond);
            }
            seatsfilled = W;
        }
            pthread_mutex_lock(&(Labs[courses[index].currentlab]).l_lock);
            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_GREEN "Tutorial has started for Course %s with %d seats filled out of %d\n" ANSI_COLOR_RESET, courses[index].name, seatsfilled, D);
            pthread_mutex_unlock(&print_terminal);
            sleep(2);
            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_RED "TA %d from lab %s has completed the tutorial and left the course %s\n" ANSI_COLOR_RESET, courses[index].currentTA, Labs[courses[index].currentlab].name, courses[index].name);
            Labs[courses[index].currentlab].labTAs[courses[index].currentTA].available = 1;
            courses[index].currentlab = -1;
            courses[index].currentTA = -1;
            pthread_mutex_unlock(&print_terminal);
            pthread_mutex_unlock(&(Labs[courses[index].currentlab]).l_lock);
    }
    pthread_mutex_unlock(&courses[index].c_lock);
}

int check_deleted(int index){
    pthread_mutex_lock(&courses[index].c_lock);
    int number_of_labs = courses[index].maxLabsTA;
    int number_of_full_labs = 0;
    for (int i = 0; i < courses[index].maxLabsTA; i++)
    {

        int labID = courses[index].LabIDs[i];
        //int TAfoundLab = 0;
        if (labID >= max_num_Labs)
        {
             pthread_mutex_lock(&print_terminal);
             printf(ANSI_COLOR_RED "error: inputed wrong lab index\n" ANSI_COLOR_RESET);
             pthread_mutex_unlock(&print_terminal);
        }
        pthread_mutex_lock(&Labs[labID].l_lock);
        if(Labs[labID].notAvailable == -1){
            number_of_full_labs++;
        }
        pthread_mutex_unlock(&Labs[labID].l_lock);

    }
    if(number_of_full_labs == number_of_labs){

            for(int k=0 ; k<courses[index].nStudents; k++){
                pthread_cond_signal(&courses[index].c_cond);
            }

             courses[index].isDeleted = 1;
             pthread_mutex_lock(&print_terminal);
             printf(ANSI_COLOR_RED "Course %s doesn’t have any TA’s eligible and is removed from course offerings\n" ANSI_COLOR_RESET, courses[index].name);
             pthread_mutex_unlock(&print_terminal);
             return 1;
    }
    pthread_mutex_unlock(&courses[index].c_lock);
    return 0;
}

//---------------- student utility---------------------

void course_wait(int index){
    pthread_mutex_lock(&students[index].s_lock);
    int course_choice = students[index].currentpval;
    pthread_mutex_lock(&courses[course_choice].c_lock);
    if(!courses[course_choice].isDeleted)
    {
        courses[course_choice].nStudents++;
        sleep(1);
        // pthread_mutex_lock(&print_terminal);
        // printf(ANSI_COLOR_MAGENTA "Student %d is waiting for a seat in course %s\n" ANSI_COLOR_RESET, index,courses[course_choice].name);
        // pthread_mutex_unlock(&print_terminal);
        while(!(courses[course_choice].isDeleted) && courses[course_choice].currentTA != -1){
            pthread_cond_wait(&courses[course_choice].c_cond, &courses[course_choice].c_lock);
        }

        if(!(courses[course_choice].isDeleted)){
            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_GREEN "Student %d has been allocated a seat in course %s\n" ANSI_COLOR_RESET, index,courses[course_choice].name);
            pthread_mutex_unlock(&print_terminal);
        }

    }
    pthread_mutex_unlock(&courses[course_choice].c_lock);
    pthread_mutex_unlock(&students[index].s_lock);
}

int course_stud_change(int index){
    
    time_t t;
    int withdraw = 0;
    srand((unsigned)time(&t));
    float random_tester = (double)rand() / ((double)RAND_MAX + 1);

    pthread_mutex_lock(&students[index].s_lock);
    int course_choice = students[index].currentpval;
    pthread_mutex_lock(&courses[course_choice].c_lock);
    int course_interest = courses[course_choice].interestQ; 
    pthread_mutex_unlock(&courses[course_choice].c_lock);
    float select_probability =  course_interest * students[index].calibreQ;
    if(select_probability > random_tester && !(courses[index].isDeleted)){
        pthread_mutex_lock(&print_terminal);
        printf(ANSI_COLOR_CYAN "Student %d has selected course %s permanently\n" ANSI_COLOR_RESET, index,courses[students[index].currentpval].name);
        pthread_mutex_unlock(&print_terminal); 
        withdraw = 1;         
    }
    else{
        
        if(!(courses[index].isDeleted))
        {
            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_CYAN "Student %d has withdrawn from course %s\n" ANSI_COLOR_RESET, index,courses[students[index].currentpval].name);
            pthread_mutex_unlock(&print_terminal);
        }
        printf(ANSI_COLOR_CYAN "." ANSI_COLOR_RESET);
        pthread_mutex_lock(&courses[students[index].currentpval].c_lock);
        if(courses[students[index].currentpval].nStudents != 0)
            courses[students[index].currentpval].nStudents--;
        pthread_mutex_unlock(&courses[students[index].currentpval].c_lock);
        int prevpval = students[index].currentpval;

        if(students[index].currentpi == 1){
            students[index].currentpi = 2;
            students[index].currentpval = students[index].p2;

            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 1) to %s (priority 2)\n" ANSI_COLOR_RESET, index,courses[prevpval].name,courses[students[index].currentpval].name);
            pthread_mutex_unlock(&print_terminal);
            
        }
        else if(students[index].currentpi == 2){
            students[index].currentpi = 3;
            students[index].currentpval = students[index].p3;

            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_BLUE "Student %d has changed current preference from %s (priority 2) to %s (priority 3)\n" ANSI_COLOR_RESET, index,courses[prevpval].name,courses[students[index].currentpval].name);
            pthread_mutex_unlock(&print_terminal);
        }
        else if(students[index].currentpi == 3){
            students[index].currentpi = -1;
            students[index].currentpval = -1;            
            pthread_mutex_lock(&print_terminal);
            printf(ANSI_COLOR_CYAN "Student %d couldn’t get any of his preferred courses\n" ANSI_COLOR_RESET, index);
            pthread_mutex_unlock(&print_terminal);
            withdraw = 1;
        }
    }
  
    pthread_mutex_unlock(&students[index].s_lock);
    return withdraw;

}

//---------------- handler --------------------
void *course_handler(void *(arg))
{
    while (1)
    {
        int index = *(int *)arg;
        printf("In course thread: %d\n", index);

        sleep(3);
        int TAfound = searchTA(index);
        sleep(3);
        take_tut(index);
        sleep(3);
        int isDeleted = check_deleted(index);
        sleep(3);
        if(isDeleted) break;
    }
}


void *student_handler(void *(arg))
{
    int index = *(int *)arg;
    printf("In student thread: %d\n", index);
    sleep(students[index].time);
    pthread_mutex_lock(&print_terminal);
    printf(ANSI_COLOR_YELLOW "Student %d has filled in preferences for course registration\n" ANSI_COLOR_RESET, index);
    pthread_mutex_unlock(&print_terminal);
    while(1){
        sleep(3);
        course_wait(index);
        sleep(3);
        int exit_course = course_stud_change(index);
        sleep(3);
        if(exit_course){
            break;
        }

    }

}

//----------------- main ------------------------

int main()
{

    // ------------------- take inputs -----------------------
    scanf("%d %d %d", &num_students, &num_Labs, &num_courses);

    //input courses
    pthread_t Tcourses[num_courses];
    for (int i = 0; i < num_courses; i++)
    {
        scanf("%s %f %d %d", courses[i].name, &courses[i].interestQ, &courses[i].maxTASlots, &courses[i].maxLabsTA);
        for (int j = 0; j < courses[i].maxLabsTA; j++)
        {
            scanf("%d", &courses[i].LabIDs[j]);
        }
        courses[i].hasTA = 0;
        courses[i].currentlab = -1;
        courses[i].currentTA = -1;
        courses[i].nStudents = 0;
        courses[i].isDeleted = 0;
        pthread_cond_init(&courses[i].c_cond, NULL);
        
    }

    //input students
    pthread_t Tstudents[num_students];
    for (int i = 0; i < num_students; i++)
    {
        scanf("%f %d %d %d %d", &students[i].calibreQ, &students[i].p1, &students[i].p2, &students[i].p3, &students[i].time);
        students[i].currentpval= students[i].p1;
        students[i].currentpi = 1;
    }

    //input Labs
    pthread_t TLabs[num_Labs];
    for (int i = 0; i < num_Labs; i++)
    {
        scanf("%s %d %d", Labs[i].name, &Labs[i].nTAs, &Labs[i].maxTAn);
        Labs[i].TAused = 0;
        Labs[i].notAvailable = 0;
        for (int j = 0; j < Labs[i].nTAs; j++)
        {
            Labs[i].labTAs[j].available = 1;
            Labs[i].labTAs[j].turns = 0;
        }
    }

    // create threads
    for (int i = 0; i < num_courses; i++)
    {
        int *passed_index = malloc(sizeof(int));
        *passed_index = i;
        pthread_mutex_init(&courses[i].c_lock, NULL);
        pthread_create(&Tcourses[i], NULL, course_handler, (void *)passed_index);
    }

    for (int i = 0; i < num_students; i++)
    {
        int *passed_index = malloc(sizeof(int));
        *passed_index = i;
        pthread_create(&Tstudents[i], NULL, student_handler, (void *)passed_index);
    }

    for (int i = 0; i < num_students; i++)
    {
        pthread_join(Tstudents[i], NULL);
    }

    for (int i = 0; i < num_courses; i++)
    {
        pthread_join(Tcourses[i], NULL);
    }



    /*
        //print
        for(int i=0; i<num_courses; i++){
            printf("%s %f %d %d", courses[i].name, courses[i].interestQ, courses[i].maxTASlots, courses[i].maxLabsTA);
            for(int j=0; j<courses[i].maxLabsTA; j++){
                    printf("%d", courses[i].LabIDs[j]);
            }
            printf("\n ***************");
        }
        for(int i=0; i<num_students; i++){
            printf("%f %d %d %d %d", students[i].calibreQ, students[i].p1 , students[i].p2, students[i].p3, students[i].time);
            printf("\n ***************");
        }  

        for(int i=0; i<num_Labs; i++){
            printf("%s %d %d", Labs[i].name, Labs[i].nTAs, Labs[i].maxTAn);
            printf("\n ***************");
        }
    */
}
