
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q2/groupWait.c ####################//

#include "header.h"

void* group(){
    for(int i=1;i<=G;i++){
        if(groupWaiting[i] == groupCapacity[i]){
            printf(ANSI_COLOR_YELLOW "Group %d left for dinner\n",i);
            pthread_mutex_lock(&groupLock);
            groupWaiting[i]=-1;
            groupCount++;
            pthread_mutex_unlock(&groupLock);
        }
    }
}
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q2/football.c ####################//

#include "header.h"

void* game(){
    srand(time(0));
    sem_wait(&synchro);
    for(int i=0;i<numGoals;i++){
        float prob = (rand()%100 + 1)/100.0;
        if(Goals[i].timeElapsed == tick){
            if(Goals[i].conversionProb >= prob){
                if(Goals[i].Team == 'H'){
                    homeGoal++;
                    printf(ANSI_COLOR_RESET "Team H has scored their %d Goal\n",homeGoal);
                }else{
                    awayGoal++;
                    printf(ANSI_COLOR_RESET "Team A has scored their %d Goal\n",awayGoal);
                }
            }
            else{
                if(Goals[i].Team == 'H'){
                    printf(ANSI_COLOR_RESET "Team H has missed their chance for %d Goal\n",homeGoal+1);
                }else{
                    printf(ANSI_COLOR_RESET "Team A has missed their chance for %d Goal\n",awayGoal+1);
                }
            }
        }
    }
    sem_post(&synchro);
}
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q2/main.c ####################//

#include "header.h"

void DataStoring(char* Name, char zone, int rT, int P, int pG){
    strcpy(spectators[specIndex].name,Name);
    spectators[specIndex].Zone=zone;
    spectators[specIndex].Group = groupIndex;
    spectators[specIndex].reachingTime=rT;
    spectators[specIndex].patienceTime=P;
    spectators[specIndex].patienceGoals=pG;
    spectators[specIndex].left = 0;
    spectators[specIndex].got=0;
    specIndex++;
}

void printData(){
    printf("Zone capacity: H=%d A=%d N=%d\n",H,A,N);
    printf("Spectating time: %d\n",X);
    printf("Number of Groups: %d\n",G);
    for(int i=1;i<=G;i++){
        printf("Number of people in Group-%d: %d\n",i,groupCapacity[i]);
    }
    printf("Number of people reach stadium: %d\n",specIndex);
    for(int i=0;i<specIndex;i++){
        printf("Name:%s\tZone:%c\tGroup:%d\tTimetoReach:%d\tPatienceValue:%d\tPatienceGoals:%d\n",spectators[i].name,spectators[i].Zone,spectators[i].Group,spectators[i].reachingTime,spectators[i].patienceTime,spectators[i].patienceGoals);
    }
    printf("Number of Chances for Goals: %d\n",numGoals);
    for(int i=0;i<numGoals;i++){
        printf("Team:%c\tTime Elapsed:%d\tconversionProbability:%.2f\n",Goals[i].Team,Goals[i].timeElapsed,Goals[i].conversionProb);
    }
    printf("__________________________________________________\n\n");
}

int main(){
    groupCount=0;
    tick=0;
    scanf("%d %d %d",&H,&A,&N);
    scanf("%d",&X);
    scanf("%d",&G);
    sem_init(&synchro,0,2);
    if (pthread_mutex_init(&groupLock, NULL) != 0) {
        printf("\n mutex init has failed\n");
        return 1;
    }
    groupCapacity = (int*)malloc((G+1)*sizeof(int));
    groupWaiting = (int*)calloc(G+1,sizeof(int));
    spectators = (Person*)malloc(sizeof(Person)*500);
    groupIndex=1;
    specIndex=0;
    homeGoal=0;
    awayGoal=0;
    seatAvailable[0]=H;
    seatAvailable[1]=A;
    seatAvailable[2]=N;
    char* Name;
    char zone;
    int rT; //reachingtime
    int P; //patiencetime
    int pG; //patienceGoals
    for(groupIndex=1;groupIndex<=G;groupIndex++){
        scanf("%d",&groupCapacity[groupIndex]);
        for(int j=0;j<groupCapacity[groupIndex];j++){
            scanf("%s %c %d %d %d",Name,&zone,&rT,&P,&pG);
            DataStoring(Name,zone,rT,P,pG);
        }
    }
    scanf("%d\n",&numGoals);
    Goals = (Goal*)malloc(sizeof(Goal)*numGoals);
    for(int j=0;j<numGoals;j++){
        scanf("%c %d %f\n",&Goals[j].Team,&Goals[j].timeElapsed,&Goals[j].conversionProb);
    }
    remainIndex=specIndex;
    printData();

    while(groupCount!=G){
        pthread_create(&tid0,NULL,game,NULL);
        pthread_create(&tid1,NULL,ticket,NULL);
        pthread_create(&tid2,NULL,group,NULL);
        pthread_join(tid0,NULL);
        pthread_join(tid1,NULL);
        pthread_join(tid2,NULL);
        tick++;
    }
    pthread_exit(NULL);
    pthread_mutex_destroy(&groupLock);
    sem_destroy(&synchro);
    return 0;
}


//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q2/header.h ####################//

#ifndef __CLASSICO
#define __CLASSICO

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_BLACK   "\e[1;90m"

#define WHTHB "\e[0;107m"

sem_t semaphore;
sem_t synchro;

typedef struct{
    char name[50];
    char Zone;
    int Group;
    int reachingTime;
    int patienceTime;
    int patienceGoals;
    int left;
    int got;
}Person;

Person* spectators;

typedef struct{
    char Team;
    int timeElapsed;
    float conversionProb;
}Goal;

Goal* Goals; 

int H,A,N; //capacity of zones
int X; //spectating time
int G; //Number of groups
int* groupCapacity; //Number of people in each group
int* groupWaiting;
int seatAvailable[3];
int groupIndex,specIndex; //zoneIndex: which zone current person is, watchIndex:total number of people in stadium
// waitIndex: total number of people waiting for ticket, groupIndex: keep track of groups, specIndex: number of people arriving at stadium
int numGoals; //number of chances for goal
int remainIndex;
void DataStoring(char* Name, char zone, int rT, int P, int pG);
void printData();
void* ticket();
void* game();
void* group();

int tick;
int homeGoal,awayGoal;
int groupCount;
pthread_mutex_t groupLock;

pthread_t tid0;
pthread_t tid1;
pthread_t tid2;
#endif
//###########FILE CHANGE ./main_folder/Mayank Shukla_305871_assignsubmission_file_/q2/ticket.c ####################//

#include "header.h"

void* ticket(){
    sem_wait(&synchro);
    for(int i=0;i<specIndex;i++){
        if(spectators[i].Zone == 'H' && spectators[i].patienceGoals<=homeGoal && tick > spectators[i].reachingTime && spectators[i].left==0){
            printf(ANSI_COLOR_RED "%s is leaving due to bad performance of his team\n",spectators[i].name);
            spectators[i].left=1;
            seatAvailable[0]++;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if(spectators[i].Zone == 'A' && spectators[i].patienceGoals<=awayGoal && tick > spectators[i].reachingTime && spectators[i].left==0){
            printf(ANSI_COLOR_RED "%s is leaving due to bad performance of his team\n",spectators[i].name);
            spectators[i].left=1;
            seatAvailable[1]++;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if(spectators[i].reachingTime == tick && spectators[i].left==0){
            printf(ANSI_COLOR_MAGENTA "%s has reached the stadium\n",spectators[i].name);                
            if(seatAvailable[0]>0 && spectators[i].Zone == 'H'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[0]--;
                spectators[i].got=1;
            }
            if(seatAvailable[1]>0 && spectators[i].Zone == 'A'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[1]--;
                spectators[i].got=1;
            }
            if(seatAvailable[2]>0 && spectators[i].Zone == 'N'){
                printf(ANSI_COLOR_CYAN "%s has got the seat in zone %c\n",spectators[i].name,spectators[i].Zone);
                seatAvailable[2]--;
                spectators[i].got=1;
            }
        }
        
        if(spectators[i].reachingTime < tick && spectators[i].patienceTime == tick  && spectators[i].got==0 && spectators[i].left==0){
            printf(ANSI_COLOR_BLUE "%s has not got the seat\n",spectators[i].name);
            spectators[i].left=1;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
        if((tick-spectators[i].reachingTime) == X && spectators[i].left==0){
            printf(ANSI_COLOR_BLACK "%s has watched the game for %d seconds and now leaving\n",spectators[i].name,X);
            if(spectators[i].Zone == 'H')seatAvailable[0]++;
            if(spectators[i].Zone == 'A')seatAvailable[1]++;
            if(spectators[i].Zone == 'N')seatAvailable[2]++;
            spectators[i].left=1;
            remainIndex--;
            if(groupCapacity[spectators[i].Group] > 1){
                printf(ANSI_COLOR_GREEN "%s is waiting for their friends at the exit\n",spectators[i].name);
                pthread_mutex_lock(&groupLock);
                groupWaiting[spectators[i].Group]++;
                pthread_mutex_unlock(&groupLock);
            }
        }
    }
    sem_post(&synchro);
}

