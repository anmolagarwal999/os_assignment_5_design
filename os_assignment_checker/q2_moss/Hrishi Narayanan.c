
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/util.c ####################//

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void get_input()
{
    scanf("%d %d %d %d", &input.h_capacity, &input.a_capacity, &input.n_capacity, &input.spectation_time);

    set.people = malloc(sizeof(person_type) * 1);

    scanf("%d", &input.groups);

    input.people = 0;
    for (int i = 1; i <= input.groups; i++)
    {
        int in_group;
        scanf("%d", &in_group);

        for (int j = 0; j < in_group; j++)
        {
            set.people = realloc(set.people, sizeof(person_type) * (input.people + 1));

            scanf("%s %c %d %d %d", set.people[input.people].name, &set.people[input.people].type, &set.people[input.people].reach, &set.people[input.people].patience, &set.people[input.people].goals);

            set.people[input.people].group = i;
            set.people[input.people].id = input.people;

            input.people++;
        }
    }

    set.teams = malloc(sizeof(team_type) * NUM_TEAMS);

    set.teams[0].type = 'H';
    set.teams[1].type = 'A';
    for (int i = 0; i < NUM_TEAMS; i++)
    {
        set.teams[i].id = i;
        set.teams[i].chances = 0;
        set.teams[i].previous_chance_time = malloc(sizeof(int) * 1);
        set.teams[i].goal_probability = malloc(sizeof(float) * 1);
    }

    int chances, time;
    char team;

    scanf("%d\n", &chances);

    int prev_a_time = 0, prev_h_time = 0;

    for (int i = 0; i < chances; i++)
    {
        scanf("%c", &team);

        if (team == 'H')
        {
            set.teams[0].previous_chance_time = realloc(set.teams[0].previous_chance_time, sizeof(int) * (set.teams[0].chances + 1));
            set.teams[0].goal_probability = realloc(set.teams[0].goal_probability, sizeof(float) * (set.teams[0].chances + 1));

            scanf("%d %f\n", &time, &set.teams[0].goal_probability[set.teams[0].chances]);

            set.teams[0].previous_chance_time[set.teams[0].chances] = time - prev_h_time;
            prev_h_time = time;

            set.teams[0].chances++;
        }

        else if (team == 'A')
        {
            set.teams[1].previous_chance_time = realloc(set.teams[1].previous_chance_time, sizeof(int) * (set.teams[1].chances + 1));
            set.teams[1].goal_probability = realloc(set.teams[1].goal_probability, sizeof(float) * (set.teams[1].chances + 1));

            scanf("%d %f\n", &time, &set.teams[1].goal_probability[set.teams[1].chances]);

            set.teams[1].previous_chance_time[set.teams[1].chances] = time - prev_a_time;
            prev_a_time = time;

            set.teams[1].chances++;
        }
    }
}

void initialise_all()
{
    for(int i = 0; i < NUM_TEAMS; i++){
        pthread_mutex_init(&locks.goals[i], NULL);
        pthread_cond_init(&condition.goals[i], NULL);
        status.goals[i] = 0;
    }

    condition.person_zone = malloc(sizeof(pthread_cond_t) * input.people);
    status.person_zone = malloc(sizeof(char) * input.people);
    locks.person_zone = malloc(sizeof(pthread_mutex_t) * input.people);
    
    for (int i = 0; i < input.people; i++){
        pthread_cond_init(&condition.person_zone[i], NULL);
        pthread_mutex_init(&locks.person_zone[i], NULL);
        status.person_zone[i] = 'E';
    }
}
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/find.c ####################//

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"

extern input_type input;
extern set_type set;

extern threads_type threads;
extern sem_type sem;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void *find_zone_h(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.h_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'H';

        printf("\033[32;1m%s (%c) got a seat in zone H\033[0;0m\n", person.name, person.type);
    }
    else
        sem_post(&sem.h_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}

void *find_zone_n(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.n_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' == 0 || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'N';

        printf("\033[32;1m%s (%c) got a seat in zone N\033[32;1m\n", person.name, person.type);
    }

    else
        sem_post(&sem.n_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}

void *find_zone_a(void *args)
{
    person_type person = *(person_type *)args;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += person.patience;

    if (sem_timedwait(&sem.a_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&locks.person_zone[person.id]);

        if (status.person_zone[person.id] == 'E')
            status.person_zone[person.id] = 'D';

        pthread_mutex_unlock(&locks.person_zone[person.id]);

        return 0;
    }

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'D' || status.person_zone[person.id] == 'E')
    {
        status.person_zone[person.id] = 'A';

        printf("\033[32;1m%s (%c) got a seat in zone A\033[0;0m\n", person.name, person.type);
    }

    else
        sem_post(&sem.a_zone);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    return 0;
}

//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/sims.h ####################//

#ifndef SIMS_H
#define SIMS_H

void *team_simulator(void *args);
void *entry_exit_simulator(void *args);

#endif
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/main.c ####################//

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "util.h"
#include "sims.h"

input_type input;
set_type set;

threads_type threads;
sem_type sem;

status_type status;
locks_type locks;
condition_type condition;

signed main()
{
    get_input();
    initialise_all();

    sem_init(&sem.h_zone, 0, input.h_capacity);
    sem_init(&sem.a_zone, 0, input.a_capacity);
    sem_init(&sem.n_zone, 0, input.n_capacity);

    threads.people = malloc(sizeof(pthread_t) * input.people);
    threads.teams = malloc(sizeof(pthread_t) * NUM_TEAMS);

    for (int i = 0; i < input.people; i++)
        pthread_create(&threads.people[i], NULL, entry_exit_simulator, &set.people[i]);

    for (int i = 0; i < NUM_TEAMS; i++)
        pthread_create(&threads.teams[i], NULL, team_simulator, &set.teams[i]);

    for (int i = 0; i < input.people; i++)
        pthread_join(threads.people[i], NULL);

    for (int i = 0; i < NUM_TEAMS; i++)
        pthread_join(threads.teams[i], NULL);

    for (int i = 0; i < NUM_TEAMS; i++)
    {
        status.goals[i] = -1;
        pthread_cond_broadcast(&condition.goals[i]);
        pthread_mutex_destroy(&locks.goals[i]);
        pthread_cond_destroy(&condition.goals[i]);
        pthread_cancel(threads.teams[i]);
    }

    sem_destroy(&sem.h_zone);
    sem_destroy(&sem.a_zone);
    sem_destroy(&sem.n_zone);

    exit(0);
}
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/util.h ####################//

#ifndef UTIL_H
#define UTIL_H

void get_input();
void initialise_all();

#endif
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/find.h ####################//

#ifndef FIND_H
#define FIND_H

void *find_zone_h (void *args);
void *find_zone_n (void *args);
void *find_zone_a (void *args);

#endif
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/sims.c ####################//

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "defs.h"
#include "find.h"

extern input_type input;
extern set_type set;

extern threads_type threads;
extern sem_type sem;

extern status_type status;
extern locks_type locks;
extern condition_type condition;

void *team_simulator(void *args)
{
    team_type team = *(team_type *)args;

    for (int i = 0; i < team.chances; i++)
    {
        sleep(team.previous_chance_time[i]);

        float x = (float)rand() / (float)(RAND_MAX / 1.0);

        if (x >= team.goal_probability[i])
            printf("\033[36;1mTeam %c missed their chance to score a goal\033[0;0m\n", team.type);
        else
        {
            pthread_mutex_lock(&locks.goals[team.id]);
            status.goals[team.id] += 1;
            printf("\033[36;1mTeam %c has scored goal number %d\033[0;0m\n", team.type, status.goals[team.id]);
            pthread_mutex_unlock(&locks.goals[team.id]);
        }

        pthread_cond_broadcast(&condition.goals[team.id]);
    }

    return 0;
}

void *game_reaction_simulator(void *args)
{
    person_type person = *(person_type *)args;

    if (person.type == 'H')
    {
        pthread_mutex_lock(&locks.goals[1]);

        while (status.goals[1] < person.goals && status.goals[1] >= 0)
            pthread_cond_wait(&condition.goals[1], &locks.goals[1]);

        if (status.goals[1] >= person.goals)
            printf("\033[31;1m%s got enraged and went to the exit\033[0;0m\n", person.name);

        pthread_mutex_unlock(&locks.goals[1]);

        pthread_cond_broadcast(&condition.person_zone[person.id]);
    }

    else if (person.type == 'A')
    {
        pthread_mutex_lock(&locks.goals[0]);

        while (status.goals[0] < person.goals && status.goals[0] >= 0)
            pthread_cond_wait(&condition.goals[0], &locks.goals[0]);

        if (status.goals[0] >= person.goals)
            printf("\033[31;1m%s got enraged and went to the exit\033[0;0m\n", person.name);

        pthread_mutex_unlock(&locks.goals[0]);

        pthread_cond_broadcast(&condition.person_zone[person.id]);
    }

    return 0;
}

void *seating_simulator(void *args)
{
    person_type person = *(person_type *)args;

    if (person.type == 'H')
    {
        pthread_t h_thread;
        pthread_t n_thread;

        pthread_create(&h_thread, NULL, find_zone_h, &person);
        pthread_create(&n_thread, NULL, find_zone_n, &person);

        pthread_join(h_thread, NULL);
        pthread_join(n_thread, NULL);
    }
    else if (person.type == 'A')
    {
        pthread_t a_thread;

        pthread_create(&a_thread, NULL, find_zone_a, &person);

        pthread_join(a_thread, NULL);
    }
    else if (person.type == 'N')
    {
        pthread_t h_thread;
        pthread_t a_thread;
        pthread_t n_thread;

        pthread_create(&h_thread, NULL, find_zone_h, &person);
        pthread_create(&a_thread, NULL, find_zone_a, &person);
        pthread_create(&n_thread, NULL, find_zone_n, &person);

        pthread_join(h_thread, NULL);
        pthread_join(a_thread, NULL);
        pthread_join(n_thread, NULL);
    }

    return 0;
}

void *entry_exit_simulator(void *args)
{
    person_type person = *(person_type *)args;
    sleep(person.reach);

    printf("\033[31;1m%s has reached the stadium\033[0;0m\n", person.name);

    pthread_t thread;
    pthread_create(&thread, NULL, seating_simulator, &person);
    pthread_join(thread, NULL);

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'G' || status.person_zone[person.id] == 'D')
    {
        printf("\033[34;1m%s did not find a seat in any of the zones\033[0;0m\n", person.name);
        pthread_mutex_unlock(&locks.person_zone[person.id]);

        printf("\033[35;1m%s left the stadium\033[0;0m\n", person.name);
        return 0;
    }

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    pthread_create(&thread, NULL, game_reaction_simulator, &person);

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += input.spectation_time;

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (pthread_cond_timedwait(&condition.person_zone[person.id], &locks.person_zone[person.id], &ts) == ETIMEDOUT)
        printf("\033[33;1m%s watched the match for %d seconds and is leaving\033[0;0m\n", person.name, input.spectation_time);

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    pthread_mutex_lock(&locks.person_zone[person.id]);

    if (status.person_zone[person.id] == 'H')
        sem_post(&sem.h_zone);

    if (status.person_zone[person.id] == 'A')
        sem_post(&sem.a_zone);

    if (status.person_zone[person.id] == 'N')
        sem_post(&sem.n_zone);

    status.person_zone[person.id] = 'G';

    pthread_mutex_unlock(&locks.person_zone[person.id]);

    printf("\033[35;1m%s left the stadium\033[0;0m\n", person.name);

    return 0;
}
//###########FILE CHANGE ./main_folder/Hrishi Narayanan_305937_assignsubmission_file_/2019113022/q2/defs.h ####################//

#ifndef DEFS_H
#define DEFS_H

#define _GNU_SOURCE

#define MAX_NAME 20
#define NUM_TEAMS 2

typedef struct input_type
{
    int h_capacity;
    int a_capacity;
    int n_capacity;
    int spectation_time;
    int groups;
    int people;
} input_type;

typedef struct sem_type
{
    sem_t h_zone;
    sem_t a_zone;
    sem_t n_zone;
} sem_type;

typedef struct person_type
{
    char name[MAX_NAME];
    char type;
    int id;
    int group;
    int reach;
    int patience;
    int goals;
} person_type;

typedef struct team_type
{
    char type;
    int id;
    int chances;
    int *previous_chance_time;
    float *goal_probability;
} team_type;

typedef struct set_type
{
    person_type *people;
    team_type *teams;
} set_type;

typedef struct threads_type
{
    pthread_t *people;
    pthread_t *teams;
} threads_type;

typedef struct status_type
{
    int goals[NUM_TEAMS];
    char *person_zone;
} status_type;

typedef struct locks_type
{
    pthread_mutex_t goals[NUM_TEAMS];
    pthread_mutex_t *person_zone;
} locks_type;

typedef struct condition_type
{
    pthread_cond_t goals[NUM_TEAMS];
    pthread_cond_t *person_zone;
} condition_type;

#endif