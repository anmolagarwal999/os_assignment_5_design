
//###########FILE CHANGE ./main_folder/Keshav Bajaj_305797_assignsubmission_file_/2019115010/q2/q2.c ####################//

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int cap_in_h, cap_in_a, cap_in_n, spec_time, num_people = 0;
sem_t h_zone, a_zone, n_zone;

typedef struct person
{
    char name[20], fan_type;
    int id, reach_time, patience_time, num_goals;
} person;

typedef struct team
{
    char team_type;
    int id, num_chances;
    int *time_prev_chance;
    float *probability_goal;
} team;

typedef struct input
{
    int num;
} input;

person *people;
team *teams;

pthread_t *t_people, *t_teams;

// variables of the number of goals that each team has scored
int goals[2];
pthread_cond_t goals_changed[2];
pthread_mutex_t goals_lock[2];

// variables to keep track of the zones that a person is in: H,A,N,D(Didn't find any zone),E(Entrance),G(Gone)
char *person_in_zone;
pthread_mutex_t *person_zone_lock;
pthread_cond_t *person_moved;

void *vacancy_h(void *args)
{
    // store the patience time of the person
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);

    person personp = *(person *)args;
    ts.tv_sec += personp.patience_time;

    int required_id = personp.id;
    // wait for a seat to be allocated in h_zone for the patience time
    if (sem_timedwait(&h_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&person_zone_lock[required_id]);
        // if the person was at the entrance till now, change the zone info so that he didn't get any zone
        if (person_in_zone[required_id] - 'E' == 0)
            person_in_zone[required_id] = 'D';

        pthread_mutex_unlock(&person_zone_lock[required_id]);
        return NULL;
    }

    pthread_mutex_lock(&person_zone_lock[required_id]);
    // if the person was at the entrance or didn't get any seat from the other zone threads, allocate this zone
    if (person_in_zone[required_id] - 'E' == 0 || person_in_zone[required_id] - 'D' == 0)
    {
        person_in_zone[required_id] = 'H';
        printf(GREEN "%s got a seat in zone H \n" RESET, personp.name);
    }
    // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
    else
        sem_post(&h_zone);
    pthread_mutex_unlock(&person_zone_lock[required_id]);

    return NULL;
}

void *vacancy_a(void *args)
{
    // store the patience time of the person
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);

    person personp = *(person *)args;
    ts.tv_sec += personp.patience_time;

    int required_id = personp.id;
    // wait for a seat to be allocated in h_zone for the patience time
    if (sem_timedwait(&a_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&person_zone_lock[required_id]);
        // if the person was at the entrance till now, change the zone info so that he didn't get any zone
        if (person_in_zone[required_id] - 'E' == 0)
        {
            person_in_zone[required_id] = 'D';
        }
        pthread_mutex_unlock(&person_zone_lock[required_id]);
        return NULL;
    }

    pthread_mutex_lock(&person_zone_lock[required_id]);
    // if the person was at the entrance or didn't get any seat from the other zone threads, allocate this zone
    if (person_in_zone[required_id] - 'E' == 0 || person_in_zone[required_id] - 'D' == 0)
    {
        person_in_zone[required_id] = 'A';
        printf(GREEN "%s got a seat in zone A \n" RESET, personp.name);
    }
    // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
    else
    {
        sem_post(&a_zone);
    }
    pthread_mutex_unlock(&person_zone_lock[required_id]);

    return NULL;
}

void *vacancy_n(void *args)
{
    // store the patience time of the person
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);

    person personp = *(person *)args;
    ts.tv_sec += personp.patience_time;

    int required_id = personp.id;
    // wait for a seat to be allocated in h_zone for the patience time
    if (sem_timedwait(&n_zone, &ts) == -1 && errno == ETIMEDOUT)
    {
        pthread_mutex_lock(&person_zone_lock[required_id]);
        // if the person was at the entrance till now, change the zone info so that he didn't get any zone
        if (person_in_zone[required_id] - 'E' == 0)
        {
            person_in_zone[required_id] = 'D';
        }
        pthread_mutex_unlock(&person_zone_lock[required_id]);
        return NULL;
    }

    pthread_mutex_lock(&person_zone_lock[required_id]);
    // if the person was at the entrance or didn't get any seat from the other zone threads, allocate this zone
    if (person_in_zone[required_id] - 'E' == 0 || person_in_zone[required_id] - 'D' == 0)
    {
        person_in_zone[required_id] = 'N';
        printf(GREEN "%s got a seat in zone N \n" RESET, personp.name);
    }
    // if the person got allocated some zone, or left even, increment the semaphore since the person didn't use a seat
    else
    {
        sem_post(&n_zone);
    }
    pthread_mutex_unlock(&person_zone_lock[required_id]);

    return NULL;
}

void *watch_match(void *args)
{
    person personp = *(person *)args;

    // if the person is a H fan, then look at the goals of the Away team
    char fan_type = personp.fan_type;
    int index = 0;
    if (fan_type == 'H' || fan_type == 'A')
    {
        if (fan_type == 'H')
            index = 1;
        else
            index = 0;
        pthread_mutex_lock(&goals_lock[index]);
        // wait until the goals of the opposing team doesn't enrage
        // this person, and until the match isn't over
        while (goals[index] < personp.num_goals && goals[index] >= 0)
            pthread_cond_wait(&goals_changed[index], &goals_lock[index]);

        int check = goals[index] >= personp.num_goals;
        if (check)
            printf(RED "%s is leaving due to bad performance of his team \n" RESET, personp.name);

        pthread_mutex_unlock(&goals_lock[index]);
        pthread_cond_broadcast(&person_moved[personp.id]);
    }

    return NULL;
}

void *person_enter_exit(void *args)
{
    person personp = *(person *)args;

    int required_id = personp.id;

    pthread_t thread;

    // person comes to the gate at time reach_time
    sleep(personp.reach_time);

    printf(RESET "%s has reached the stadium \n" RESET, personp.name);

    if (personp.fan_type == 'N')
    {
        int check_found = 0;
        pthread_t h_thread;
        pthread_create(&h_thread, NULL, vacancy_h, &personp);
        if (check_found == 1)
            printf("%s (%c) found a seat in zone H\n", personp.name, personp.fan_type);
        pthread_t a_thread;
        pthread_create(&a_thread, NULL, vacancy_a, &personp);
        if (check_found == 2)
            printf("%s (%c) found a seat in zone A\n", personp.name, personp.fan_type);
        pthread_t n_thread;
        pthread_create(&n_thread, NULL, vacancy_n, &personp);
        if (check_found == 3)
            printf("%s (%c) found a seat in zone N\n", personp.name, personp.fan_type);
        pthread_join(h_thread, NULL);
        pthread_join(a_thread, NULL);
        pthread_join(n_thread, NULL);
    }
    if (personp.fan_type == 'A')
    {
        pthread_t a_thread;
        pthread_create(&a_thread, NULL, vacancy_a, &personp);
        pthread_join(a_thread, NULL);
    }
    if (personp.fan_type == 'H')
    {
        int check_found = 0;
        pthread_t h_thread;
        pthread_create(&h_thread, NULL, vacancy_h, &personp);
        if (check_found == 1)
            printf("%s (%c) found a seat in zone H\n", personp.name, personp.fan_type);
        pthread_t n_thread;
        pthread_create(&n_thread, NULL, vacancy_n, &personp);
        if (check_found == 2)
            printf("%s (%c) found a seat in zone N\n", personp.name, personp.fan_type);
        pthread_join(h_thread, NULL);
        pthread_join(n_thread, NULL);
    }

    // if the person did not find any seat
    pthread_mutex_lock(&person_zone_lock[required_id]);
    if (person_in_zone[required_id] - 'D' == 0 || person_in_zone[required_id] - 'G' == 0)
    {
        pthread_mutex_unlock(&person_zone_lock[required_id]);
        printf(BLUE "%s did not find a seat in any of the zones \n" RESET, personp.name);
        return NULL;
    }
    pthread_mutex_unlock(&person_zone_lock[required_id]);

    // simulate the person inside the game
    pthread_create(&thread, NULL, watch_match, &personp);

    required_id = personp.id;
    // set timespec for the spectating time
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += spec_time;

    pthread_mutex_lock(&person_zone_lock[required_id]);
    // simulate_person will signal the conditional variable if the person wants to leave
    if (pthread_cond_timedwait(&person_moved[required_id], &person_zone_lock[required_id], &ts) == ETIMEDOUT)
    {
        printf(RED "%s watched the match for %d seconds and is leaving \n" RESET, personp.name, spec_time);
    }
    pthread_mutex_unlock(&person_zone_lock[required_id]);

    // Once the person wants to leave, increment the semaphore
    pthread_mutex_lock(&person_zone_lock[required_id]);
    if (person_in_zone[required_id] - 'H' == 0)
        sem_post(&h_zone);
    if (person_in_zone[required_id] - 'A' == 0)
        sem_post(&a_zone);
    if (person_in_zone[required_id] - 'N' == 0)
        sem_post(&n_zone);
    person_in_zone[required_id] = 'G';

    pthread_mutex_unlock(&person_zone_lock[required_id]);
    return NULL;
}

void *team_func(void *args)
{
    team p_team = *(team *)args;

    int i, length = p_team.num_chances;
    for (int i = 0; i < length; i++)
    {
        // sleep until the time elapses till the next chance to score a goal
        sleep(p_team.time_prev_chance[i]);

        // choose whether the team has scored a goal or not
        float x = (float)rand() / (float)RAND_MAX;
        if (x > p_team.probability_goal[i])
            printf(CYAN "Team %c missed their chance to score a goal \n" RESET, p_team.team_type);

        else
        {
            // team has scored a goal
            pthread_mutex_lock(&goals_lock[p_team.id]);
            goals[p_team.id]++;
            pthread_mutex_unlock(&goals_lock[p_team.id]);
            printf(CYAN "Team %c has scored goal number %d \n" RESET, p_team.team_type, goals[p_team.id]);
        }
        // broadcast after every goal scoring chance, even if the team did not score
        pthread_cond_broadcast(&goals_changed[p_team.id]);
    }

    return NULL;
}

int main()
{

    int num_groups, num_in_group;
    scanf("%d %d %d", &cap_in_h, &cap_in_a, &cap_in_n);

    // initialize the semaphores corresponding to the number of seats in each zone
    sem_init(&h_zone, 0, cap_in_h);
    sem_init(&a_zone, 0, cap_in_a);
    sem_init(&n_zone, 0, cap_in_n);

    scanf("%d", &spec_time);
    scanf("%d", &num_groups);

    int i = 0, j = 0;
    people = malloc(sizeof(person));
    for (i = 1; i <= num_groups; i++)
    {
        scanf("%d", &num_in_group);
        for (j = 0; j < num_in_group; j++)
        {
            people = realloc(people, sizeof(person) * (num_people + 1));
            people[num_people].id = num_people;
            scanf("%s %c %d %d %d", people[num_people].name, &people[num_people].fan_type, &people[num_people].reach_time, &people[num_people].patience_time, &people[num_people].num_goals);
            num_people++;
        }
    }

    t_people = malloc(sizeof(pthread_t) * num_people);
    t_teams = malloc(sizeof(pthread_t) * 2);
    teams = malloc(sizeof(team) * 2);

    for (int i = 0; i < 2; i++)
    {
        teams[i].team_type = 'H';
        teams[i].num_chances = 0;
        teams[i].id = i;
        teams[i].time_prev_chance = malloc(sizeof(int));
        teams[i].probability_goal = malloc(sizeof(float));
        pthread_mutex_init(&goals_lock[i], NULL);
        pthread_cond_init(&goals_changed[i], NULL);
        goals[i] = 0;
    }
    teams[1].team_type = 'A';

    int goal_scoring_chances;
    int prev_a_time = 0;
    char inp_team;
    int prev_h_time = 0;
    int inp_time;

    scanf("%d\n", &goal_scoring_chances);
    for (int i = 0; i < goal_scoring_chances; i++)
    {
        scanf("%c", &inp_team);
        int index = 0;
        if (inp_team - 'H' == 0)
        {
            teams[index].time_prev_chance = realloc(teams[index].time_prev_chance, sizeof(int) * (teams[index].num_chances + 1));
            teams[index].probability_goal = realloc(teams[index].probability_goal, sizeof(float) * (teams[index].num_chances + 1));

            scanf("%d %f\n", &inp_time, &teams[index].probability_goal[teams[index].num_chances]);
            // store the time from the previous chance to goal
            teams[index].time_prev_chance[teams[index].num_chances] = inp_time - prev_h_time;
            prev_h_time = inp_time;

            teams[index].num_chances++;
        }
        if (inp_team - 'A' == 0)
        {
            index = 1;
            teams[index].time_prev_chance = realloc(teams[index].time_prev_chance, sizeof(int) * (teams[index].num_chances + index));
            teams[index].probability_goal = realloc(teams[index].probability_goal, sizeof(float) * (teams[index].num_chances + index));

            scanf("%d %f\n", &inp_time, &teams[index].probability_goal[teams[index].num_chances]);
            // store the time from the previous chance to goal
            teams[index].time_prev_chance[teams[index].num_chances] = inp_time - prev_a_time;
            prev_a_time = inp_time;

            teams[index].num_chances++;
        }
    }

    person_moved = malloc(sizeof(pthread_cond_t) * num_people);
    person_zone_lock = malloc(sizeof(pthread_mutex_t) * num_people);
    person_in_zone = malloc(sizeof(char) * num_people);

    for (int i = 0; i < num_people; i++)
    {
        pthread_cond_init(&person_moved[i], NULL);
        person_in_zone[i] = 'E';
        pthread_mutex_init(&person_zone_lock[i], NULL);
    }

    // allocate space and create threads
    for (int i = 0; i < num_people; i++)
    {
        input *temp = (input *)malloc(sizeof(input));
        temp->num = i;
        pthread_create(&t_people[i], NULL, person_enter_exit, &people[i]);
    }

    pthread_create(&t_teams[0], NULL, team_func, &teams[0]);
    pthread_create(&t_teams[1], NULL, team_func, &teams[1]);

    // wait for the threads to finish
    for (int i = 0; i < num_people; i++)
        pthread_join(t_people[i], NULL);

    pthread_join(t_teams[0], NULL);
    pthread_join(t_teams[1], NULL);

    goals[0] = -1;
    pthread_cond_broadcast(&goals_changed[0]);
    pthread_mutex_destroy(&goals_lock[0]);
    pthread_cond_destroy(&goals_changed[0]);
    pthread_cancel(t_teams[0]);
    goals[1] = -1;
    pthread_cond_broadcast(&goals_changed[1]);
    pthread_mutex_destroy(&goals_lock[1]);
    pthread_cond_destroy(&goals_changed[1]);
    pthread_cancel(t_teams[1]);

    return 0;
}
