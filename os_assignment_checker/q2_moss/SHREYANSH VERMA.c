
//###########FILE CHANGE ./main_folder/SHREYANSH VERMA_305938_assignsubmission_file_/2019113012/q2/q2.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <errno.h>


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int h_capacity,n_capacity,a_capacity,x,n_group,n_goal;
int itr = 0,num,total = 0,team_a_goals = 0,team_b_goals;

pthread_mutex_t zone_mutx[3];
pthread_mutex_t team_a_mutx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t team_b_mutx = PTHREAD_MUTEX_INITIALIZER;


sem_t zone_wait[3];

int waiting_fella = 0;
int pop_inside_stands = 0;

pthread_mutex_t waiting_fella_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t stands_mutex = PTHREAD_MUTEX_INITIALIZER;

sem_t stud_wait,stand_wait;

typedef struct zonestruct
{
    int cur_cap;
    int ful_cap;
} zones;

typedef struct person
{
    char name[100];
    int time_to_reach;
    char team_sup;
    int patience_time;
    int num_goals;
    int is_waiting;
    int allocated_zone;
    char allocated_zone_name;

} person;

typedef struct goals
{
    char team_name;
    int num_goal;
    int time_elapsed;
    float prob;
} goals;

person person_arr[100];
goals goal_arr[100];
zones zone_arr[3];

void *person_sync(void * arg)   // Person thread
{
    long long int indx = (long long int)arg;
    sleep(person_arr[indx].time_to_reach);
    printf(ANSI_COLOR_BLUE"%s has reached the stadium\n"ANSI_COLOR_RESET,person_arr[indx].name);
   
    struct timespec ts1;
    if (clock_gettime(CLOCK_REALTIME, &ts1) == -1)
    {
        return NULL;
    }
    ts1.tv_sec += person_arr[indx].patience_time;
    int s;
    while(1)
    {
        // printf("person = %s,team sup = %c\n",person_arr[indx].name,person_arr[indx].team_sup);
        pthread_mutex_lock(&waiting_fella_mutex); //Checking for emtpy slots in the stadium
        if(person_arr[indx].team_sup=='H')
        {
            // printf("Here name = %s\n",person_arr[indx].name);
            pthread_mutex_lock(&zone_mutx[0]);
            if(zone_arr[0].cur_cap<zone_arr[0].ful_cap)
            {
                zone_arr[0].cur_cap++;
                person_arr[indx].allocated_zone=0;
                person_arr[indx].allocated_zone_name = 'H';
                pthread_mutex_unlock(&zone_mutx[0]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[0]);
            pthread_mutex_lock(&zone_mutx[2]);
            if(zone_arr[2].cur_cap<zone_arr[2].ful_cap)
            {
                zone_arr[2].cur_cap++;
                person_arr[indx].allocated_zone=2;
                person_arr[indx].allocated_zone_name = 'N';
                pthread_mutex_unlock(&zone_mutx[2]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[2]);
        }
        else if(person_arr[indx].team_sup=='N')
        {
            pthread_mutex_lock(&zone_mutx[0]);
            if(zone_arr[0].cur_cap<zone_arr[0].ful_cap)
            {
                zone_arr[0].cur_cap++;
                person_arr[indx].allocated_zone=0;
                person_arr[indx].allocated_zone_name = 'H';
                pthread_mutex_unlock(&zone_mutx[0]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[0]);
            pthread_mutex_lock(&zone_mutx[2]);
            if(zone_arr[2].cur_cap<zone_arr[2].ful_cap)
            {
                zone_arr[2].cur_cap++;
                person_arr[indx].allocated_zone=2;
                person_arr[indx].allocated_zone_name = 'N';
                pthread_mutex_unlock(&zone_mutx[2]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[2]);
            pthread_mutex_lock(&zone_mutx[1]);
            if(zone_arr[1].cur_cap<zone_arr[1].ful_cap)
            {
                zone_arr[1].cur_cap++;
                person_arr[indx].allocated_zone_name = 'A';
                person_arr[indx].allocated_zone=1;
                pthread_mutex_unlock(&zone_mutx[1]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[1]);
        }
        else
        {
            pthread_mutex_lock(&zone_mutx[1]);
            if(zone_arr[1].cur_cap<zone_arr[1].ful_cap)
            {
                // printf("perv Inside name = %s current seats = %d, total %d\n",person_arr[indx].name,zone_arr[1].cur_cap,zone_arr[1].ful_cap);
                zone_arr[1].cur_cap +=1;
                person_arr[indx].allocated_zone=1;
                person_arr[indx].allocated_zone_name = 'A';
                // printf("Inside name = %s current seats = %d, total %d\n",person_arr[indx].name,zone_arr[1].cur_cap,zone_arr[1].ful_cap);
                pthread_mutex_unlock(&zone_mutx[1]);
                pthread_mutex_unlock(&waiting_fella_mutex);
                break;
            }
            pthread_mutex_unlock(&zone_mutx[1]);
        }
        waiting_fella++;
        pthread_mutex_unlock(&waiting_fella_mutex);
        while ((s = sem_timedwait(&stud_wait, &ts1)) == -1 && errno == EINTR) //Time dependent waint on semaphore on the ticket counter
        continue;
        if (s == -1)
        {
            if (errno == ETIMEDOUT)
            {
                // printf("sem_timedwait() timed out\n");
                printf(ANSI_COLOR_RED"%s could not get a seat\n"ANSI_COLOR_RESET,person_arr[indx].name);
                pthread_mutex_lock(&waiting_fella_mutex);
                waiting_fella--;
                pthread_mutex_unlock(&waiting_fella_mutex);
                printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                return NULL;
            }
            else
            {
              
                
                // perror("sem_timedwait");
            }
        } 
        else
        {
                // printf("HEllo I am %s\n",person_arr[indx].name);
                if(person_arr[indx].team_sup=='H')
                {
                    pthread_mutex_lock(&zone_mutx[0]);
                    if(zone_arr[0].cur_cap<zone_arr[0].ful_cap)
                    {
                        zone_arr[0].cur_cap++;
                        person_arr[indx].allocated_zone=0;
                        person_arr[indx].allocated_zone_name = 'H';
                        pthread_mutex_unlock(&zone_mutx[0]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[0]);
                    pthread_mutex_lock(&zone_mutx[2]);
                    if(zone_arr[2].cur_cap<zone_arr[2].ful_cap)
                    {
                        zone_arr[2].cur_cap++;
                        person_arr[indx].allocated_zone=2;
                        person_arr[indx].allocated_zone_name = 'N';
                        pthread_mutex_unlock(&zone_mutx[2]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[2]);
                }
                else if(person_arr[indx].team_sup=='N')
                {
                    pthread_mutex_lock(&zone_mutx[0]);
                    if(zone_arr[0].cur_cap<zone_arr[0].ful_cap)
                    {
                        zone_arr[0].cur_cap++;
                        person_arr[indx].allocated_zone=0;
                        person_arr[indx].allocated_zone_name = 'H';
                        pthread_mutex_unlock(&zone_mutx[0]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[0]);
                    pthread_mutex_lock(&zone_mutx[2]);
                    if(zone_arr[2].cur_cap<zone_arr[2].ful_cap)
                    {
                        zone_arr[2].cur_cap++;
                        person_arr[indx].allocated_zone=2;
                        person_arr[indx].allocated_zone_name = 'N';
                        pthread_mutex_unlock(&zone_mutx[2]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[2]);
                    pthread_mutex_lock(&zone_mutx[1]);
                    if(zone_arr[1].cur_cap<zone_arr[1].ful_cap)
                    {
                        zone_arr[1].cur_cap++;
                        person_arr[indx].allocated_zone=1;
                        person_arr[indx].allocated_zone_name = 'A';
                        pthread_mutex_unlock(&zone_mutx[1]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[1]);
                }
                else
                {
                    pthread_mutex_lock(&zone_mutx[1]);
                    if(zone_arr[1].cur_cap<zone_arr[1].ful_cap)
                    {
                        // printf("perv Inside name = %s current seats = %d, total %d\n",person_arr[indx].name,zone_arr[1].cur_cap,zone_arr[1].ful_cap);
                        zone_arr[1].cur_cap +=1;
                        person_arr[indx].allocated_zone=1;
                        person_arr[indx].allocated_zone_name = 'A';
                        // printf("Inside name = %s current seats = %d, total %d\n",person_arr[indx].name,zone_arr[1].cur_cap,zone_arr[1].ful_cap);
                        pthread_mutex_unlock(&zone_mutx[1]);
                        // pthread_mutex_unlock(&waiting_fella_mutex);
                        break;
                    }
                    pthread_mutex_unlock(&zone_mutx[1]);
                }
                continue;
                // printf("sem_timedwait() succeeded1\n");
        }
    }
    printf(ANSI_COLOR_CYAN"%s got a seat in zone %c\n"ANSI_COLOR_RESET,person_arr[indx].name,person_arr[indx].allocated_zone_name);
    pthread_mutex_lock(&stands_mutex);
    pop_inside_stands++;
    pthread_mutex_unlock(&stands_mutex);
    struct timespec ts2;
    if (clock_gettime(CLOCK_REALTIME, &ts2) == -1)
    {
        return NULL;
    }
    ts2.tv_sec += x;
    int s1;
    while(1)
    {
        if(person_arr[indx].team_sup=='H') // Goal check conditions.
        {
            // printf("Hi inside team goal is %d and my expec is %d\n",team_b_goals,person_arr[indx].num_goals);
            pthread_mutex_lock(&team_a_mutx);
            if(team_a_goals>=person_arr[indx].num_goals)
            {
                printf(ANSI_COLOR_RED"%s is leaving due to bad performance of his team \n"ANSI_COLOR_RESET,person_arr[indx].name);
                pthread_mutex_unlock(&team_a_mutx);
                pthread_mutex_lock(&stands_mutex);
                pop_inside_stands--;
                pthread_mutex_unlock(&stands_mutex);
                pthread_mutex_lock(&waiting_fella_mutex);
                int temp_waiting = waiting_fella;
                for(int i=0;i<temp_waiting;i++)
                {
                    sem_post(&stud_wait);
                }
                pthread_mutex_unlock(&waiting_fella_mutex);
                printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                return NULL;

            }
            pthread_mutex_unlock(&team_a_mutx);
        }
        if(person_arr[indx].team_sup=='A')
        {
            pthread_mutex_lock(&team_b_mutx);
            if(team_b_goals>=person_arr[indx].num_goals)
            {
                printf(ANSI_COLOR_RED"%s is leaving due to bad performance of his team \n"ANSI_COLOR_RESET,person_arr[indx].name);
                pthread_mutex_unlock(&team_b_mutx);
                pthread_mutex_lock(&stands_mutex);
                pop_inside_stands--;
                pthread_mutex_unlock(&stands_mutex);
                pthread_mutex_lock(&waiting_fella_mutex);
                int temp_waiting = waiting_fella;
                for(int i=0;i<temp_waiting;i++)
                {
                    sem_post(&stud_wait);
                }
                pthread_mutex_unlock(&waiting_fella_mutex);
                printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                return NULL;

            }
            pthread_mutex_unlock(&team_b_mutx);
        }
        while ((s1 = sem_timedwait(&stand_wait, &ts2)) == -1 && errno == EINTR) //Second timed semaphore representing wait in stadium
        continue;
        if (s1 == -1)
        {
            if (errno == ETIMEDOUT)
            {
                // printf("sem_timedwait() timed out\n");
                printf(ANSI_COLOR_RED"%s watched the match for %d seconds and is leaving \n"ANSI_COLOR_RESET,person_arr[indx].name,x);
                pthread_mutex_lock(&zone_mutx[person_arr[indx].allocated_zone]);
                zone_arr[person_arr[indx].allocated_zone].cur_cap--;
                pthread_mutex_unlock(&zone_mutx[person_arr[indx].allocated_zone]);
                pthread_mutex_lock(&stands_mutex);
                pop_inside_stands--;
                pthread_mutex_unlock(&stands_mutex);
                pthread_mutex_lock(&waiting_fella_mutex);
                int temp_waiting = waiting_fella;
                for(int i=0;i<temp_waiting;i++) //Signalling persons at the ticket counter to wake up.
                {
                    sem_post(&stud_wait);
                }
                pthread_mutex_unlock(&waiting_fella_mutex);
                printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                return NULL;
            }
            else
            {
                // perror("sem_timedwait");
            }
        } 
        else
        {
                // printf("Hey wake up %s in the stands\n",person_arr[indx].name);
                if(person_arr[indx].team_sup=='H')
                {
                    if(team_a_goals>=person_arr[indx].num_goals)
                    {
                        printf(ANSI_COLOR_RED"%s is leaving due to bad performance of his team \n"ANSI_COLOR_RESET,person_arr[indx].name);
                        pthread_mutex_lock(&stands_mutex);
                        pop_inside_stands--;
                        pthread_mutex_unlock(&stands_mutex);
                        pthread_mutex_lock(&waiting_fella_mutex);
                        int temp_waiting = waiting_fella;
                        for(int i=0;i<temp_waiting;i++)
                        {
                            sem_post(&stud_wait);
                        }
                        pthread_mutex_unlock(&waiting_fella_mutex);
                        printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                        return NULL;
                        
                    }
                }
                else if(person_arr[indx].team_sup=='A')
                {
                    // printf("Hi inside team goal is %d and my expec is %d\n",team_b_goals,person_arr[indx].num_goals);
                    if(team_b_goals>=person_arr[indx].num_goals)
                    {
                        printf(ANSI_COLOR_RED"%s is leaving due to bad performance of his team \n"ANSI_COLOR_RESET,person_arr[indx].name);
                        pthread_mutex_lock(&stands_mutex);
                        pop_inside_stands--;
                        pthread_mutex_unlock(&stands_mutex);
                        pthread_mutex_lock(&waiting_fella_mutex);
                        int temp_waiting = waiting_fella;
                        for(int i=0;i<temp_waiting;i++)
                        {
                            sem_post(&stud_wait);
                        }
                        pthread_mutex_unlock(&waiting_fella_mutex);
                        printf(ANSI_COLOR_RED"%s is leaving for dinner \n"ANSI_COLOR_RESET,person_arr[indx].name);
                        return NULL;
                        
                    }
                }
                continue;
                // printf("sem_timedwait() succeeded2\n");
        }
    }
}

void *goal_sync(void * arg)
{
    long long int indx = (long long int)arg;
    sleep(goal_arr[indx].time_elapsed);
    if(goal_arr[indx].prob>0.5) // Checking goal probablility
    {
        if(goal_arr[indx].team_name=='A')
        {
            pthread_mutex_lock(&team_a_mutx);
            printf(ANSI_COLOR_YELLOW"Team %c has scored their %d goal\n"ANSI_COLOR_RESET,goal_arr[indx].team_name,++team_a_goals);
            int temp_stands = pop_inside_stands;
            pthread_mutex_lock(&stands_mutex);
            for(int i=0;i<temp_stands;i++)
            {
                sem_post(&stand_wait); // Waking up people at stands to check against the goal threshold.
            }
            pthread_mutex_unlock(&stands_mutex);
            pthread_mutex_unlock(&team_a_mutx);
            // pthread_mutex_unlock(&team_a_mutx);
            

        }
        else
        {
            pthread_mutex_lock(&team_b_mutx);
            printf(ANSI_COLOR_YELLOW"Team %c has scored their %d goal\n"ANSI_COLOR_RESET,goal_arr[indx].team_name,++team_b_goals);
            int temp_stands = pop_inside_stands;
            pthread_mutex_lock(&stands_mutex);
            for(int i=0;i<temp_stands;i++)
            {
                sem_post(&stand_wait);
            }
            pthread_mutex_unlock(&stands_mutex);
            pthread_mutex_unlock(&team_b_mutx);

        }
    }
    else
    {
        if(goal_arr[indx].team_name=='A')
        {
            pthread_mutex_lock(&team_a_mutx);
            printf(ANSI_COLOR_YELLOW"Team %c missed the chance to score their %d goal\n"ANSI_COLOR_RESET,goal_arr[indx].team_name,team_a_goals+1);
            pthread_mutex_unlock(&team_a_mutx);

        }
        else
        {
            pthread_mutex_lock(&team_b_mutx);
            printf(ANSI_COLOR_YELLOW"Team %c missed the chance to score their %d goal\n"ANSI_COLOR_RESET,goal_arr[indx].team_name,team_b_goals+1);
            pthread_mutex_unlock(&team_b_mutx);
        }
    }

}

int main()
{
    pthread_t person_thr[100];
    pthread_t goal_thr[100];

    sem_init(&stud_wait, 0, 0);
    sem_init(&stand_wait, 0, 0);

    scanf("%d %d %d",&h_capacity,&a_capacity,&n_capacity);

    zone_arr[0].ful_cap = h_capacity;
    zone_arr[0].cur_cap = 0;
    zone_arr[1].ful_cap = a_capacity;
    zone_arr[1].cur_cap = 0;
    zone_arr[2].ful_cap = n_capacity;
    zone_arr[2].cur_cap = 0;

    pthread_mutex_init(&zone_mutx[0], NULL);
    pthread_mutex_init(&zone_mutx[1], NULL);
    pthread_mutex_init(&zone_mutx[2], NULL);

    scanf("%d",&x);
    scanf("%d",&n_group);
    for(int i=0;i<n_group;i++)
    {
        scanf("%d",&num);
        for(int j=0;j<num;j++)
        {
            scanf("%s %c %d %d %d",person_arr[itr].name,&person_arr[itr].team_sup,&person_arr[itr].time_to_reach,&person_arr[itr].patience_time,&person_arr[itr].num_goals);
            person_arr[itr].is_waiting = 1;
            itr++;
        }
    }
    scanf("%d",&n_goal);

    for(int i=0;i<n_goal;i++)
    {
        scanf(" %c %d %f",&goal_arr[i].team_name,&goal_arr[i].time_elapsed,&goal_arr[i].prob);
        goal_arr[i].num_goal=0;
    }
    // printf("BEfore full = %d %d %d\n",zone_arr[0].ful_cap,zone_arr[1].ful_cap,zone_arr[2].ful_cap);
    sleep(5);
    for(int i=0;i<itr;i++)
    {
        pthread_create(&person_thr[i],NULL,person_sync,(void *)(long long int)i);
        // sle
    }
    for(int i=0;i<n_goal;i++)
    {
        pthread_create(&goal_thr[i],NULL,goal_sync,(void *)(long long int)i);
        // sle
    }

    for(int i=0;i<itr;i++)
    {
        pthread_join(person_thr[i],NULL);
    }

    for(int i=0;i<n_goal;i++)
    {
        pthread_join(goal_thr[i],NULL);
    }
}