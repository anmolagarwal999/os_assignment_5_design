
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/spectator.c ####################//

#include "headers.h"

void *handleSpectator(void *args)
{
    spectator *spectator1 = (spectator *)args;

    int t = spectator1->entry_time;
    sleep(t);

    yellow();
    printf("%s has reached the stadium\n", spectator1->name);
    reset();

    int patience_time = spectator1->patience_level;
    int spectator_id = spectator1->spectator_id;

    // getting time
    struct timespec timeToWait;
    clock_gettime(CLOCK_REALTIME, &timeToWait);

    timeToWait.tv_sec += patience_time;
    pthread_mutex_lock(&mutexSpectator[spectator_id]);

    spectator1->entered = 1;

    sem_post(&semaphore);
    sem_post(&semaphore);

    int ret1 = pthread_cond_timedwait(&condSpectator[spectator_id], &mutexSpectator[spectator_id], &timeToWait);
    pthread_mutex_unlock(&mutexSpectator[spectator_id]);

    // printf("hello %s", spectator1->name);

    if (ret1 == 0) // person has been allocated a seat
    {
         // if no of goals has already exceeded the enraged value then quit
        if((spectator1->team_code == 'H' && Team_Goal[1] >= spectator1->enraged_no) || (spectator1->team_code == 'A' && Team_Goal[0]>=spectator1->enraged_no))
        {
            goto checkpoint1;
        }

        // update time
        clock_gettime(CLOCK_REALTIME, &timeToWait);
        timeToWait.tv_sec += spectating_time;

        pthread_mutex_lock(&mutexSpectator[spectator_id]);
        int ret2 = pthread_cond_timedwait(&condSpectator[spectator_id], &mutexSpectator[spectator_id], &timeToWait);

        if (ret2 == 0)
        {
            checkpoint1: // label
            spectator1->left = 1;
            // signal was sent and that means other team has exceeded the no of goals
            red();
            printf("%s is leaving due to bad performance of his team\n", spectator1->name);
            reset();

            pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
            Zone[spectator1->zone_no].seat_left++;
            pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }

        else if (ret2 == ETIMEDOUT)
        {
            // No signal was sent and thus person completed its spectating time
            spectator1->left = 1;
            red();
            printf("%s has watched the match for %d time is now leaving\n", spectator1->name, spectating_time);
            reset();

            pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
            Zone[spectator1->zone_no].seat_left++;
            pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }
        else
        {
            perror("Error in cond timedwait: ");
            pthread_mutex_unlock(&mutexSpectator[spectator_id]);
            return NULL;
        }
    }
    else if (ret1 == ETIMEDOUT)
    {
        // patience level exceeded
        // spectator leaves the simulation
        spectator1->left = 1;
        red();
        printf("%s could not get a seat\n", spectator1->name);
        reset();

        pthread_mutex_lock(&mutexZone[spectator1->zone_no]);
        Zone[spectator1->zone_no].seat_left++;
        pthread_mutex_unlock(&mutexZone[spectator1->zone_no]);

        return NULL;
    }
    else
    {
        perror("Error in cond timedwait: ");
        return NULL;
    }
}

//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/stadium.c ####################//

#include "headers.h"

void* handleStadium(void* args)
{
    while(1)
    {

    }
}
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/main.c ####################//

#include "headers.h"

// color code
void red()
{
    printf("\033[1;31m");
}
void yellow()
{
    printf("\033[1;33m");
}
void reset()
{
    printf("\033[0m");
}
void green()
{
    printf("\033[1;32m");
}
void blue()
{
    printf("\033[1;34m");
}
void purple()
{
    printf("\033[1;35m");
}

void handle_input()
{
    for (int i = 0; i < 3; i++)
    {
        scanf("%d", &Zone[i].capacity);
        Zone[i].seat_left = Zone[i].capacity;
    }

    scanf("%d %d", &spectating_time, &num_groups);

    no_of_spectator = 0;

    for (int i = 1; i <= num_groups; i++)
    {
        int no_of_person;
        scanf("%d", &no_of_person);

        // printf("Value of no_of_person: %d\n" , no_of_person);

        for (int j = 0; j < no_of_person; j++)
        {
            scanf("%s %c %d %d %d", Spectator[no_of_spectator].name, &Spectator[no_of_spectator].team_code, &Spectator[no_of_spectator].entry_time, &Spectator[no_of_spectator].patience_level, &Spectator[no_of_spectator].enraged_no);
            Spectator[no_of_spectator].entered = -1;
            Spectator[no_of_spectator].left = -1;
            Spectator[no_of_spectator].is_seated = -1;
            Spectator[no_of_spectator].spectator_id = no_of_spectator;

            // for neutral fans
            if (Spectator[no_of_spectator].enraged_no == -1)
                Spectator[no_of_spectator].enraged_no = 9999999;

            no_of_spectator++;
        }
    }

    scanf("%d", &no_of_goal_scoring_chances);

    for (int i = 0; i < no_of_goal_scoring_chances; i++)
    {
        getchar();
        scanf("%c %d %f", &Goals[i].team_code, &Goals[i].time, &Goals[i].probability);
    }

    // initialize other variables
    Zone[0].id = 'H';
    Zone[1].id = 'A';
    Zone[2].id = 'N';

    Team_Goal[0] = 0;
    Team_Goal[1] = 0;
}

void check_input()
{
    for(int i=0; i <3 ; i++)
    {
        printf("Zone capacity: %d\n" , Zone[i].capacity);
    }

    printf("Spectating time: %d no_of_group: %d\n" , spectating_time , num_groups);

    printf("total no of spectator: %d\n" , no_of_spectator);

    for(int i=0; i < no_of_spectator; i++)
    {
        printf("name: %s zone_id: %c entry_time: %d patience_time: %d num_goals: %d\n" , Spectator[i].name , Spectator[i].team_code , Spectator[i].entry_time , Spectator[i].patience_level , Spectator[i].enraged_no);
    }

    printf("No of goals: %d\n" , no_of_goal_scoring_chances);

    for(int i=0; i < no_of_goal_scoring_chances; i++)
    {
        printf("Team code: %c time: %d prob: %f\n" , Goals[i].team_code , Goals[i].time  , Goals[i].probability );
    }
}



int main()
{
    srand(time(NULL));
    handle_input();
    // check_input();

    sem_init(&semaphore , 0 , 0);

    // init zone lock
    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_init(&mutexZone[i], NULL);
        pthread_create(&threadZone[i], NULL, handleZone, &Zone[i]);
    }

    // init mutex lock
    for (int i = 0; i < no_of_spectator; i++)
    {
        pthread_mutex_init(&mutexSpectator[i], NULL);
        pthread_cond_init(&condSpectator[i], NULL);
        pthread_create(&threadSpectator[i], NULL, handleSpectator, &Spectator[i]);
    }

    for (int i = 0; i < no_of_goal_scoring_chances; i++)
    {
        pthread_create(&threadGoal[i], NULL, handleGoal, &Goals[i]);
    }

    // join thread
    for (int i = 0; i < no_of_spectator; i++)
    {
        pthread_join(threadSpectator[i], NULL);
    }

    for(int i=0; i< no_of_goal_scoring_chances; i++)
    pthread_join(threadGoal[i] , NULL);

    printf("Simulation Ended\n");
}

//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/goal.c ####################//

#include "headers.h"

void *handleGoal(void *args)
{
    goal *goal1 = (goal *)args;
    int t = goal1->time;

    sleep(t);

    float random_value =  (float)rand() / RAND_MAX;
    // printf("random value is %f\n" , random_value);

    if (random_value <= goal1->probability)
    {
        // update the goal of the team
        int x;
        if (goal1->team_code == 'H')
            x = 0;
        else
            x = 1;

        Team_Goal[x]++;

        char Team_id = goal1->team_code;

        green();
        printf("Team %c has scored their %dth goal\n", Team_id, Team_Goal[x]);
        reset();

        // check which players have exceeded their enraged value
        for (int i = 0; i < no_of_spectator; i++)
        {
            spectator *spectator1 = &Spectator[i];

            if ( (spectator1->entered==1) && (spectator1->is_seated==1) && (spectator1->left == -1) && (spectator1->team_code != Team_id) && (Team_Goal[x] >= spectator1->enraged_no))
            {
                // signal the player that he must leave now
                pthread_cond_signal(&condSpectator[i]);
            }
        }

        return NULL;
    }
    else
    {
        red();
        if (goal1->team_code == 'A')
        {
            printf("Team A has missed its chance to score its %d goal\n", Team_Goal[1] + 1);
        }
        else
        {
            printf("Team H has missed its chance to score its %d goal\n", Team_Goal[0] + 1);
        }
        reset();
        return NULL;
    }
}
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/headers.h ####################//

#ifndef _headers
#define _headers
#define _OPEN_THREADS

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <memory.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>

#define MAX_NO_OF_SPECTATOR 200
#define MAX_GOALS 100

typedef struct zone
{
    char id; // id = [Zone H , Zone A , Zone N]
    int capacity;
    int seat_left;
} zone;

typedef struct spectator
{
    int spectator_id;
    char name[100];
    int entry_time;
    int group_no;
    char team_code; // H(home) , A (Away) or N (neutral)
    int patience_level;
    int enraged_no; // stores the number of goals opponent need to have to enrage this person
    int entered;
    int left;
    int is_seated;
    char zone_seated;
    int zone_no;
} spectator;

typedef struct goal
{
    int time;
    char team_code;
    float probability;
} goal;

int Team_Goal[2]; // stores which team has stored how many goal
                  // goal[0] -> home goal[1]-> away team

int spectating_time;
int num_groups;
int no_of_goal_scoring_chances;

int no_of_spectator;

spectator Spectator[MAX_NO_OF_SPECTATOR];
pthread_t threadSpectator[MAX_NO_OF_SPECTATOR];
pthread_cond_t condSpectator[MAX_NO_OF_SPECTATOR];
pthread_mutex_t mutexSpectator[MAX_NO_OF_SPECTATOR];

zone Zone[3];                 // Zone[0]-> H , Zone[1]->A , Zone[2]->N
pthread_mutex_t mutexZone[3]; // 0->H , 1->A , 2->N
pthread_t threadZone[3];      // 0->H , 1-> A , 2-> N

goal Goals[MAX_GOALS];
pthread_t threadGoal[MAX_GOALS];

sem_t semaphore;

void *handleZone(void *);
void *handleSpectator(void *);
void *handleGoal(void *);

// functions to print in diff color
void red () ;
void yellow () ;
void reset ();
void green ();
void blue ();
void purple();

#endif
//###########FILE CHANGE ./main_folder/Hardik Gupta_305806_assignsubmission_file_/2020101045_assignment_5/q2/zone.c ####################//

#include "headers.h"

void *handleZone(void *args)
{
    zone *zone1 = (zone *)args;
    int zone_no;
    if (zone1->id == 'H')
        zone_no = 0;
    else if (zone1->id == 'A')
        zone_no = 1;
    else
        zone_no = 2;

    // loop infinitely checking if any student has entered for this zone but has not been seated
    while (1)
    {
        sem_wait(&semaphore);
        for (int i = 0; i < no_of_spectator; i++)
        {
            pthread_mutex_lock(&mutexSpectator[i]);
            if ((Spectator[i].entered == 1) && (Spectator[i].is_seated == -1) && (Spectator[i].left == -1))
            {
                // printf("Name: %s Zone: %c\n" , Spectator[i].name , zone1->id);
                if (Spectator[i].team_code == 'N')
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else if (Spectator[i].team_code == 'A' && (zone1->id == 'A'))
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else if ( (Spectator[i].team_code == 'H') && ((zone1->id == 'N') || (zone1->id == 'H')))
                {
                    // if seat is available in this zone // then give it to him
                    if (zone1->seat_left > 0)
                    {
                        pthread_mutex_lock(&mutexZone[zone_no]);
                        zone1->seat_left--;
                        Spectator[i].is_seated = 1;
                        Spectator[i].zone_seated = zone1->id;
                        Spectator[i].zone_no = zone_no;

                        purple();
                        printf("%s has goat seat in Zone %c\n", Spectator[i].name, zone1->id);
                        reset();

                        pthread_mutex_unlock(&mutexSpectator[i]);
                        pthread_cond_signal(&condSpectator[i]);
                        pthread_mutex_unlock(&mutexZone[zone_no]);
                    }
                    else
                    {
                        pthread_mutex_unlock(&mutexSpectator[i]);
                    }
                }
                else
                {
                    pthread_mutex_unlock(&mutexSpectator[i]);
                }
            }
            else
            {
                pthread_mutex_unlock(&mutexSpectator[i]);   
            }
        }
    }
}