
//###########FILE CHANGE ./main_folder/Nitin Rajasekar_305900_assignsubmission_file_/2020101117/q2/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

pthread_mutex_t m_mutex;

pthread_mutex_t w_mutex;
pthread_mutex_t p_mutex;

pthread_cond_t a_cond;

struct member
{
    int g_id;
    int g_count;
    char name[50];
    char zone;
    int time;
    int patience;
    int goal_lim;
    char stand_got;
    struct member *next;
};

struct chance
{
    char team;
    int elapsed;
    float prob;

    struct chance *next;
    pthread_mutex_t mutex;
};

struct member *first_m;
struct chance *first_c;
int a_goal = 1;
int h_goal = 1;
int num_chances;
int chances_left;
int h_num, a_num, n_num;
int h_left, a_left, n_left;
int done;
int dinner[50];

void red()
{
    printf("\033[1;31m");
}

void yellow()
{
    printf("\033[1;33m");
}
void green()
{
    printf("\033[0;32m");
}
void blue()
{
    printf("\033[1;34m");
}
void cyan()
{
    printf("\033[1;36m");
}
void purple()
{
    printf("\033[1;35m");
}
void bcyan()
{
    printf("\033[0;37m");
}
void bpurple()
{
    printf("\033[0;37m");
}
void reset()
{
    printf("\033[0m");
}

void *member_f(void *argument)
{
    struct member *arg = (struct member *)argument;
    done = 0;
    sleep(arg->time);
    red();
    printf("%s %c has reached the stadium\n", arg->name, arg->zone);
    reset();

    sleep(0.1);
    int seconds = time(NULL);

    if (arg->zone == 'A')
    {
        if (a_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone A\n", arg->name);
            reset();
            arg->stand_got = 'A';

            a_left--;
        }
        else
        {
            while (time(NULL) - seconds < arg->patience)
            {
                if (a_left > 0)
                {

                    yellow();

                    printf("%s has got a seat in zone A\n", arg->name);
                    reset();
                    a_left--;
                    break;
                }
                pthread_mutex_lock(&w_mutex);
                while (done == 0)

                    pthread_cond_wait(&a_cond, &w_mutex);
                pthread_mutex_unlock(&w_mutex);
            }

            if (arg->stand_got == 'B')
            {
                bcyan();
                printf("%s could not get a seat\n", arg->name);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);

                goto exit;
            }
        }

        while (1 > 0)
        {
            if (h_goal >= arg->goal_lim)
            {
                cyan();
                printf("%s has left the stadium due to his teams bad performance\n", arg->name);
                reset();
                goto exit;
            }
            if (time(NULL) - seconds > arg->time)
            {
                blue();
                printf("%s has watched the match for %d seconds and is leaving\n", arg->name, arg->time);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);

                goto exit;
            }
        }
    }
    else if (arg->zone == 'H')
    {

        if (h_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone H\n", arg->name);
            reset();
            arg->stand_got = 'H';
            h_left--;
        }
        else if (n_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone N\n", arg->name);
            reset();
            arg->stand_got = 'N';

            n_left--;
        }

        else
        {
            while (time(NULL) - seconds < arg->patience)
            {
                if (h_left > 0)
                {
                    yellow();
                    printf("%s has got a seat in zone H\n", arg->name);
                    reset();
                    h_left--;
                    break;
                }
                else if (n_left > 0)
                {
                    yellow();
                    printf("%s has got a seat in zone N\n", arg->name);
                    reset();
                    n_left--;
                    break;
                }
            }
            if (arg->stand_got == 'B')
            {

                bcyan();
                printf("%s could not get a seat\n", arg->name);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);
                goto exit;
            }
        }
        while (1 > 0)
        {
            if (a_goal >= arg->goal_lim)
            {
                cyan();
                printf("%s has left the stadium due to his teams bad performance\n", arg->name);
                reset();
                goto exit;
            }
            if (time(NULL) - seconds > arg->time)
            {
                blue();
                printf("%s has watched the match for %d seconds and is leaving\n", arg->name, arg->time);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);

                goto exit;
            }
        }
    }
    else if (arg->zone == 'N')
    {

        if (h_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone H\n", arg->name);
            reset();
            arg->stand_got = 'H';
            h_left--;
        }
        else if (n_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone N\n", arg->name);
            reset();
            arg->stand_got = 'N';

            n_left--;
        }
        else if (a_left > 0)
        {
            yellow();
            printf("%s has got a seat in zone A\n", arg->name);
            reset();
            arg->stand_got = 'A';

            a_left--;
        }

        else
        {
            while (time(NULL) - seconds < arg->patience)
            {
                if (h_left > 0)
                {
                    yellow();
                    printf("%s has got a seat in zone H\n", arg->name);
                    reset();
                    h_left--;
                    break;
                }
                else if (n_left > 0)
                {
                    yellow();
                    printf("%s has got a seat in zone N\n", arg->name);
                    reset();
                    n_left--;
                    break;
                }
                else if (a_left > 0)
                {
                    yellow();
                    printf("%s has got a seat in zone A\n", arg->name);
                    reset();
                    a_left--;
                    break;
                }
            }
            if (arg->stand_got == 'B')
            {
                bcyan();
                printf("%s could not get a seat\n", arg->name);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);
                goto exit;
            }
        }
        while (1 > 0)
        {

            if (time(NULL) - seconds > arg->time)
            {
                blue();
                printf("%s has watched the match for %d seconds and is leaving\n", arg->name, arg->time);
                reset();
                //printf("%s is waiting for his friends at the exit\n", arg->name);

                goto exit;
            }
        }
    }
exit:
    bpurple();
    printf("%s is waiting for his friends at the exit\n", arg->name);
    reset();
    if (arg->stand_got == 'A')
    {
        pthread_mutex_lock(&p_mutex);

        a_left++;
        pthread_mutex_unlock(&p_mutex);
    }
    else if (arg->stand_got == 'H')
    {
        pthread_mutex_lock(&p_mutex);

        h_left++;
        pthread_mutex_unlock(&p_mutex);
    }
    else if (arg->stand_got == 'N')
    {
        pthread_mutex_unlock(&p_mutex);

        n_left++;
        pthread_mutex_unlock(&p_mutex);
    }

    dinner[arg->g_id]++;
    pthread_mutex_lock(&w_mutex);
    if (a_left > 0)
    {
        pthread_cond_signal(&a_cond);
    }
    pthread_mutex_unlock(&w_mutex);

    done = 1;

    if (dinner[arg->g_id] >= arg->g_count)
    {
        purple();
        printf("Group %d are leaving for dinner\n", arg->g_id + 1);
        reset();
    }
    return NULL;
}

void *chance_f(void *argument)
{
    //pthread_mutex_lock(&m_mutex);

    struct chance *arg = (struct chance *)argument;

    int rel_goal;
    if (arg->team == 'A')
        rel_goal = a_goal;
    else

        rel_goal = h_goal;

    sleep(arg->elapsed);
    float missed = (rand() % 101) / 100.0;
    if (missed > arg->prob)
    {
        if (arg->team == 'A')
        {
            green();
            printf("Team %c has missed a chance to get their %d goal\n", arg->team, a_goal);
            reset();
        }
        else
        {
            green();
            printf("Team %c has missed a chance to get their %d goal\n", arg->team, h_goal);
            reset();
        }
    }
    else
    {
        if (arg->team == 'A')
        {
            green();
            printf("Team %c has scored their %d goal\n", arg->team, a_goal);
            reset();
            a_goal++;
        }
        else
        {
            green();
            printf("Team %c has scored their %d goal\n", arg->team, h_goal);
            reset();
            h_goal++;
        }
    }

    //printf("Time elapsed is %d\n", arg->elapsed);
    //pthread_mutex_unlock(&m_mutex);
    chances_left--;
    return NULL;
}

int main()
{
    int i, j;
    srand(time(0));

    int spec_time;
    int num_groups;
    int mem_num;
    int total_mem = 0;
    for (i = 0; i < 50; i++)
    {
        dinner[i] = 0;
    }
    pthread_mutex_init(&(m_mutex), NULL);
    pthread_mutex_init(&(w_mutex), NULL);
    pthread_mutex_init(&(p_mutex), NULL);

    first_m = (struct member *)malloc(sizeof(struct member));
    struct member *begin_m = first_m;
    first_c = (struct chance *)malloc(sizeof(struct chance));
    struct chance *begin_c = first_c;
    scanf("%d", &h_num);
    scanf("%d", &a_num);
    scanf("%d", &n_num);
    h_left = h_num;
    a_left = a_num;
    n_left = n_num;

    scanf("%d", &spec_time);

    scanf("%d", &num_groups);

    for (i = 0; i < num_groups; i++)
    {
        while (getchar() != '\n')
            ;

        scanf("%d", &mem_num);
        for (j = 0; j < mem_num; j++)
        {
            while (getchar() != '\n')
                ;
            //printf("h\n");
            struct member *upcoming = (struct member *)malloc(sizeof(struct member));

            scanf("%s", upcoming->name);

            scanf(" %c", &upcoming->zone);
            scanf("%d", &upcoming->time);

            scanf("%d", &upcoming->patience);

            scanf("%d", &upcoming->goal_lim);
            total_mem++;
            upcoming->g_id = i;
            upcoming->g_count = mem_num;
            upcoming->stand_got = 'B';
            begin_m->next = upcoming;
            begin_m = upcoming;
        }
        //printf("b\n");
    }
    //printf("Names taken\n");
    while (getchar() != '\n')
        ;
    scanf("%d", &num_chances);
    chances_left = num_chances;
    //printf("Its %d", num_chances);

    for (i = 0; i < num_chances; i++)
    {
        struct chance *upcoming = (struct chance *)malloc(sizeof(struct chance));
        while (getchar() != '\n')
            ;

        scanf("%c", &upcoming->team);
        scanf("%d", &upcoming->elapsed);
        scanf("%f", &upcoming->prob);
        begin_c->next = upcoming;
        begin_c = upcoming;
    }

    pthread_t member_t[100];
    pthread_t chance_t[num_chances];

    begin_m = first_m->next;
    int x = 0;
    for (i = 0; i < total_mem; i++)
    {

        pthread_create(&member_t[i], NULL, member_f, (void *)begin_m);

        begin_m = begin_m->next;
    }

    begin_c = first_c->next;
    for (i = 0; i < num_chances; i++)
    {
        pthread_mutex_init(&(begin_c->mutex), NULL);
        pthread_create(&chance_t[i], NULL, chance_f, (void *)begin_c);
        begin_c = begin_c->next;
    }
    for (i = 0; i < total_mem; i++)
    {
        pthread_join(member_t[i], NULL);
    }

    for (i = 0; i < num_chances; i++)
    {
        pthread_join(chance_t[i], NULL);
    }
    //printf("%d %d %d\n", a_left, h_left, n_left);
    begin_c = first_c->next;
    for (i = 0; i < num_chances; i++)
    {
        pthread_mutex_destroy(&(begin_c->mutex));
        begin_c = begin_c->next;
    }
}