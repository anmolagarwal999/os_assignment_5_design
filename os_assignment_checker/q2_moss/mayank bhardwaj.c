
//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q2/q2.c ####################//

#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>       // for clock_t, clock(), CLOCKS_PER_SEC

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// HOW TO COMPILE
// gcc -g -pthread q2.c
// then run ./a.out

int num_people=0;      // Total number of people who enter the stadium
int goal_to_wait;    // After this much goal opponent team member with (H,A) will go to gate and wait
int spectating_time; // time for which ideally every person watching should wait
int num_groups;      // total number of groups
int k;               // Number of people inside groups
int G;               // Number of goal scoring chances throughout the match
int values_of_k[100];

// At no time should a ZONE have an empty seat if some person who is eligible to buy a ticket at this zone is waiting.
struct zone
{
    char name; // H,A,N
    int max_cap;
    int curr_cap;
};

struct person
{
    char name[100];
    char Type_fan; // home fan, away fan , neutral fan(H,A,N)
    int arrival_time;
    int patience_time; // patience value for which that person will wait
    int goals_enraged; // -1 for neutral fans
    // char elgibilty[3];// for H,A,N in respective order if it is '1' then the person is eligible for that
    //                   // else not eligible if it is '0'
    // int counter_spectating_time;     // for hyderabad people who will watch for max 'X' unit
    // int state; // has value between 1-5 as i am not implementing bonus
};

struct chance
{
    char team;                  // i.e H/A
    int time_from_start;        // Time from start after which the chance was created
    float favourable_probability; // number between 0 and 1
};



void* person_routine(void* arg)
{   
    struct person *my_persons = (struct person*)arg;
    int time_to_reach_stadium= (*my_persons).arrival_time;
    sleep(time_to_reach_stadium);
    printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n",(*my_persons).name);
    return NULL;
}



int main()
{   
    // clock_t begin=clock();
    // TAKING THE INPUT PART
    struct zone ZONE[3];
    ZONE[0].name = 'H';
    ZONE[1].name = 'A';
    ZONE[2].name = 'N';
    ZONE[0].curr_cap = 0;
    ZONE[1].curr_cap = 0;
    ZONE[2].curr_cap = 0;
    for (int i = 0; i < 3; i++)
    {
        scanf("%d", &ZONE[i].max_cap);
    }
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);
    struct person PERSON[num_groups][10000];

    

    for (int i = 0; i < num_groups; i++)
    {
        scanf("%d", &k); // Number of people inside groups;
        num_people+=k;
        values_of_k[i]=k;
        for (int j = 0; j < k; j++)
        {
            scanf("%s %c %d %d %d", PERSON[i][j].name, &PERSON[i][j].Type_fan, &PERSON[i][j].arrival_time, &PERSON[i][j].patience_time, &PERSON[i][j].goals_enraged);
        }
    }
    scanf("%d", &G);
    struct chance CHANCE[G];
    for (int i = 0; i < G; i++)
    {
        scanf(" %c %d %f", &CHANCE[i].team, &CHANCE[i].time_from_start, &CHANCE[i].favourable_probability);
    }

    // starting with the threading part
    pthread_t spectators[num_people];
    int counter_num_people=0;


    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_create(&spectators[counter_num_people++],NULL,&person_routine,(void*)&PERSON[i][j])!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }
    int counter_people=0;

    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_join(spectators[counter_people++],NULL)!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }

    return 0;
}

// sample input
/*

2 1 2
3
2
3
Vibhav N 3 2 -1 
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H1 1
A 2 0.95 
A 3 0.5 
H 5 0.85 
H 6 0.4

*/
//###########FILE CHANGE ./main_folder/mayank bhardwaj_305887_assignsubmission_file_/2020101068_assignment_5/q2/._q2.c ####################//

#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>       // for clock_t, clock(), CLOCKS_PER_SEC

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// HOW TO COMPILE
// gcc -g -pthread q2.c
// then run ./a.out

int num_people=0;      // Total number of people who enter the stadium
int goal_to_wait;    // After this much goal opponent team member with (H,A) will go to gate and wait
int spectating_time; // time for which ideally every person watching should wait
int num_groups;      // total number of groups
int k;               // Number of people inside groups
int G;               // Number of goal scoring chances throughout the match
int values_of_k[100];

// At no time should a ZONE have an empty seat if some person who is eligible to buy a ticket at this zone is waiting.
struct zone
{
    char name; // H,A,N
    int max_cap;
    int curr_cap;
};

struct person
{
    char name[100];
    char Type_fan; // home fan, away fan , neutral fan(H,A,N)
    int arrival_time;
    int patience_time; // patience value for which that person will wait
    int goals_enraged; // -1 for neutral fans
    // char elgibilty[3];// for H,A,N in respective order if it is '1' then the person is eligible for that
    //                   // else not eligible if it is '0'
    // int counter_spectating_time;     // for hyderabad people who will watch for max 'X' unit
    // int state; // has value between 1-5 as i am not implementing bonus
};

struct chance
{
    char team;                  // i.e H/A
    int time_from_start;        // Time from start after which the chance was created
    float favourable_probability; // number between 0 and 1
};



void* person_routine(void* arg)
{   
    struct person *my_persons = (struct person*)arg;
    int time_to_reach_stadium= (*my_persons).arrival_time;
    sleep(time_to_reach_stadium);
    printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n",(*my_persons).name);
    return NULL;
}



int main()
{   
    // clock_t begin=clock();
    // TAKING THE INPUT PART
    struct zone ZONE[3];
    ZONE[0].name = 'H';
    ZONE[1].name = 'A';
    ZONE[2].name = 'N';
    ZONE[0].curr_cap = 0;
    ZONE[1].curr_cap = 0;
    ZONE[2].curr_cap = 0;
    for (int i = 0; i < 3; i++)
    {
        scanf("%d", &ZONE[i].max_cap);
    }
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);
    struct person PERSON[num_groups][10000];

    

    for (int i = 0; i < num_groups; i++)
    {
        scanf("%d", &k); // Number of people inside groups;
        num_people+=k;
        values_of_k[i]=k;
        for (int j = 0; j < k; j++)
        {
            scanf("%s %c %d %d %d", PERSON[i][j].name, &PERSON[i][j].Type_fan, &PERSON[i][j].arrival_time, &PERSON[i][j].patience_time, &PERSON[i][j].goals_enraged);
        }
    }
    scanf("%d", &G);
    struct chance CHANCE[G];
    for (int i = 0; i < G; i++)
    {
        scanf(" %c %d %f", &CHANCE[i].team, &CHANCE[i].time_from_start, &CHANCE[i].favourable_probability);
    }

    // starting with the threading part
    pthread_t spectators[num_people];
    int counter_num_people=0;


    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_create(&spectators[counter_num_people++],NULL,&person_routine,(void*)&PERSON[i][j])!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }
    int counter_people=0;

    for(int i=0;i<num_groups;i++)
    {   
        for(int j=0;j<values_of_k[i];j++)
        {
            if(pthread_join(spectators[counter_people++],NULL)!=0)
            {
                perror("Failed to create thread\n");
                return 1;
            }
        }
    }

    return 0;
}

// sample input
/*

2 1 2
3
2
3
Vibhav N 3 2 -1 
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H1 1
A 2 0.95 
A 3 0.5 
H 5 0.85 
H 6 0.4

*/