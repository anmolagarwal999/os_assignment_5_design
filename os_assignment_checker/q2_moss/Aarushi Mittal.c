
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

void main()
{
        printf("test success\n");
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/main.c ####################//

// Each person is a thread
// Each seat is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "seat.c"
#include "person.c"
#include "goal.c"
#include "leave.c"

void main()
{
    printf( GREEN "Simulation has started!\n"  CLEAR);
    input();
    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");

    //Person Threads
    per_Thrd();

    // Goal Threads
    g_thrd();

    // Join threads
    join();

    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");
}
void per_Thrd()
{
     tPersonID PersonID[num_people];
    int c = 0;
    
    // Person Threads
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
        }
    }

}
void g_thrd()
{

    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

}

void join()
{
    // Join person threads
    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < Group[i].k; j++)
        {
            // pthread_join(Group[i].th[j], NULL);
            pthread_exit(Group[i].th[j]);
        }
    }

    // Join goal threads
    for(int i = 0; i < G; i++)
     {
          pthread_join(goal_thread[i], NULL);
         pthread_exit(goal_thread[i]);
     }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/seat.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf( YELLOW "%s could not get a seat\n"   CLEAR, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    // Push
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;
    Zone[team].NumSpectators++;

    // Display
    printf( YELLOW "%s has got a seat in zone %c (%d/%d)\n"   CLEAR, 
            Group[i].Person[j].Name,
            getZoneAsChar(team),
            Zone[team].NumSpectators,
            Zone[team].Capacity);

    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    // Person already left
    if(Group[i].Person[j].status == WAITING)
        return;

    // Spectating time over
    printf( MAGENTA "%s watched the match for %d seconds and is leaving\n"   CLEAR, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    // Pop
    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&lock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&lock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/person.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf( CYAN "%s has reached the stadium\n"   CLEAR, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf( BLUE "Group %d is leaving for dinner\n"   CLEAR, i+1);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/utility.h ####################//

#include "libraries.h"
//clears input buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
void coord(int i, int j)
{
    printf("(%d, %d) %s\n", i+1, j+1, Group[i].Person[j].Name);
}

int getZoneAsInt(char c)
{
	if(c =='H')
	return HOME;
	else if(c == 'A')
		return AWAY;
		else if(c == 'N')
		return NEUT;
    
}

char getZoneAsChar(int zone)
{
    if(zone == HOME)
    return 'H';
    else if(zone == AWAY)
    		return 'A';
    	else if(zone == NEUT)
    		return 'N';
}

int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}



//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/leave.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(P.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf( RED "%s is leaving due to bad performance of his team\n"  CLEAR, Zone[Z].Seat[i].Person.Name);
        printf(BLUE"%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;

        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        pthread_mutex_lock(&lock);
        pthread_cond_signal(&cond_seat_freed);
        pthread_mutex_unlock(&lock);
    }
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define CLEAR   "\x1b[0m"

// Types
typedef uint Time;

// Person Structure
typedef struct stPerson tPerson;
struct stPerson
{
    int dump[10];
    char Name[100];
    char SupportTeam;
    char SeatedZone;
    Time ArrivalTime;
    Time Patience;
    int EnrageNum;
    int status;
};

// Seat Structure
typedef struct stSeat tSeat;
struct stSeat
{
    tPerson Person;
    int i;
    int j;
};

// Zone Structure
typedef struct stZone tZone;
struct stZone
{
    char Type;
    uint Capacity;
    uint NumSpectators;
    tPerson* Spectator;
    tSeat* Seat;
    pthread_mutex_t* SeatLocks;
};
tZone Zone[3];

// Group Structure
typedef struct stGroup tGroup;
struct stGroup
{
    int dump[10];
    int k;
    int Waiting;
    tPerson* Person;
    pthread_t th[];
};
tGroup* Group;

// Goal Structure
typedef struct stGoal tGoal;
struct stGoal
{
    int dump[30];
    char Team;
    Time GoalTime;
    float GoalProb;
    pthread_mutex_t GoalLock;
};
tGoal* Goal;

// Person info structure
struct stPersonID
{
    int dump[10];
    int groupNo;
    int personNo;
};
typedef struct stPersonID tPersonID;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/input.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"


void inputZones()
{
    Zone[HOME].Type = 'H';         // Home
    Zone[AWAY].Type = 'A';         // Away
    Zone[NEUT].Type = 'N';         // Neutral

    // Input zone capacities
    printf(BLUE "Home | Away | Neutral\n" CLEAR);
    scanf("%d %d %d", &Zone[0].Capacity, &Zone[1].Capacity, &Zone[2].Capacity);

    // Iterate through all the zones
    for(int i = 0; i < 3; i++)
    {
        // Intialize zones. Create locks for every seat in a zone
        Zone[i].NumSpectators = 0;
        Zone[i].Spectator = (tPerson*)malloc(Zone[i].Capacity * sizeof(tPerson));
        Zone[i].Seat = malloc(Zone[i].Capacity * sizeof(tSeat));
        Zone[i].SeatLocks = malloc(Zone[i].Capacity * sizeof(pthread_mutex_t));
        
        // Initialize the lock for each seat in a zone
        for(int j = 0; j < Zone[i].Capacity; j++)
        {
            pthread_mutex_init(&Zone[i].SeatLocks[j], NULL);
        }
    }
}

void inputGroups()
{
    Group = (tGroup*)malloc(num_groups * sizeof(tGroup));

    // Iterate through the groups
    for(int i = 0; i < num_groups; i++)
    {
        flushSTDIN();
        printf( RED "\n(Group %d)\n"   CLEAR, i+1);

        // Number of people in group i
        printf( RED "Number: "   CLEAR);
        scanf("%d", &Group[i].k);
        num_people += Group[i].k;

        // Allocate memory for all persons in a group
        Group[i].Person = (tPerson*)malloc(Group[i].k * sizeof(tPerson));

        // Initially, no one is waiting
        Group[i].Waiting = 0;
        
        // Input persons in the group
        inputPersons(i);
    }
}

void inputPersons(int i)
{
    // Iterate through all the persons in the group
    for(int j = 0; j < Group[i].k; j++)
    {
        // Prefix
        printf( RED "P%d: " CLEAR, j+1);

        // Input person variables
        scanf("%s %c %d %d %d", &Group[i].Person[j].Name,&Group[i].Person[j].SupportTeam,&Group[i].Person[j].ArrivalTime,&Group[i].Person[j].Patience,&Group[i].Person[j].EnrageNum);
        // Group[i].Person[j].status = REACHED;
    }
}

void inputGoals()
{
    Goal = (tGoal*)malloc(G * sizeof(tGoal));

    for(int i = 0; i < G; i++)
    {
        pthread_mutex_init(&Goal[i].GoalLock, NULL);
        flushSTDIN();

        // Prefix
        printf( GREEN "G%d: "   CLEAR, i+1);

        // Input goal variables
        scanf("%c %d %f", &Goal[i].Team,&Goal[i].GoalTime,&Goal[i].GoalProb);
    }   
    printf("\n");
}

void input()
{
    inputZones();

    printf( RED "Spectating Time: " CLEAR);
    scanf("%d", &X);

    // Groups, Persons
    printf( YELLOW "Number of groups: " CLEAR);
    scanf("%d", &num_groups);
    inputGroups();

    // Goals
    printf( GREEN "\nNumber of goal scoring chances: "   CLEAR);
    scanf("%d", &G);
    goal_thread = (pthread_t*)malloc(G * sizeof(pthread_t));
    inputGoals();
}

void printZone(int i)
{
    printf("\nType = %c\n", Zone[i].Type);
    printf("Capacity = %d\n", Zone[i].Capacity);
    for(int j = 0; j < Zone[i].NumSpectators; j++)
    {
        printf("%d: %s\n", j+1, Zone[i].Spectator[j].Name);
    }
    printf("\n");
}

void printGroup(int i)
{
    printf( RED "(Group %d)\nNumber: %d\n",i+1,Group[i].k);
    

    for(int j = 0; j < Group[i].k; j++)
    {
        printf(RED"P %d :",j+1);

        printf("%s\t%c %d %d %d\n", Group[i].Person[j].Name, Group[i].Person[j].SupportTeam ,Group[i].Person[j].ArrivalTime,Group[i].Person[j].Patience, Group[i].Person[j].EnrageNum);
    }
    printf("\n");
}

void printGoals()
{
    for(int i = 0; i < G; i++)
    {
        printf("G%d: %c %d %f\n", i+1, Goal[i].Team, Goal[i].GoalTime, Goal[i].GoalProb);
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

clock_t getTime();
void delay(int ms);
clock_t start;

int main()
{
    start = clock();
    delay(5000);
    printf("test\n");
}

clock_t getTime()
{
    clock_t diff = clock() - start;
    return (diff * 1000)/CLOCKS_PER_SEC;
}

void delay(int ms)
{
    clock_t timeDelay = ms + clock();
    while(timeDelay > clock());
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/variables.h ####################//

#ifndef VAR_H
#define VAR_H

#define HOME_TEAM "FC Messilona"
#define AWAY_TEAM "Benzdrid CF"

enum tzone{HOMEs,AWAY,NEUT};
enum tstatus{REACHED=11,WAITING,EXIT}

int X;               // Spectating time
int num_groups;      // Total number of groups
int num_people = 0;
int G;               // Number of goal scoring chances

int Goals[2];

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t* goal_thread;
pthread_cond_t cond_seat_freed = PTHREAD_COND_INITIALIZER;
int seat_freed = 0;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputZones();
    void inputGroups();
    void inputGoals();
    void inputPersons(int i);
    void printZone(int i);
    void printGroup(int i);

// Leave
void leaveHOME();
void leaveAWAY();

// Seat
void seat(int i, int j, int seatZone, int seatNum);
int seatAvailable(int i);
void noSeat(int G, int P);
int probHome();
int probNeut();
int probAway();

// Goal
void* goal_function(void* arg);
    char* getGoalSuffix(int G);

// Person
void* person_function(void* arg);
    void reach(int i, int j);
    void patience(int G, int P);
    void dinner(int i);

// Main
void per_Thrd();
void g_thrd();
void join();

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/2020111008_assignment5/AdvancedOS-main/q2/src/goal.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf( GREEN "Team H has scored their %d%s goal!\n"  CLEAR, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf( GREEN "Team A has scored their %d%s goal!\n"  CLEAR, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
        return NULL;
    }
    // Goal Missed
    printf( RED "Team %c has missed!\n"  CLEAR, Goal[i].Team);

    pthread_exit(goal_thread[i]);
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

void main()
{
        printf("test success\n");
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/main.c ####################//

// Each person is a thread
// Each seat is a lock

// Libraries
#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "input.c"
#include "seat.c"
#include "person.c"
#include "goal.c"
#include "leave.c"

void main()
{
    printf( GREEN "Simulation has started!\n"  CLEAR);
    input();
    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");

    //Person Threads
    per_Thrd();

    // Goal Threads
    g_thrd();

    // Join threads
    join();

    printf("< - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >\n");
}
void per_Thrd()
{
     tPersonID PersonID[num_people];
    int c = 0;
    
    // Person Threads
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
        }
    }

}
void g_thrd()
{

    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

}

void join()
{
    // Join person threads
    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < Group[i].k; j++)
        {
            // pthread_join(Group[i].th[j], NULL);
            pthread_exit(Group[i].th[j]);
        }
    }

    // Join goal threads
    for(int i = 0; i < G; i++)
     {
          pthread_join(goal_thread[i], NULL);
         pthread_exit(goal_thread[i]);
     }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/seat.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf( YELLOW "%s could not get a seat\n"   CLEAR, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    // Push
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;
    Zone[team].NumSpectators++;

    // Display
    printf( YELLOW "%s has got a seat in zone %c (%d/%d)\n"   CLEAR, 
            Group[i].Person[j].Name,
            getZoneAsChar(team),
            Zone[team].NumSpectators,
            Zone[team].Capacity);

    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    // Person already left
    if(Group[i].Person[j].status == WAITING)
        return;

    // Spectating time over
    printf( MAGENTA "%s watched the match for %d seconds and is leaving\n"   CLEAR, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    // Pop
    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&lock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&lock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/person.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf( CYAN "%s has reached the stadium\n"   CLEAR, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf( BLUE "Group %d is leaving for dinner\n"   CLEAR, i+1);
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/utility.h ####################//

#include "libraries.h"
//clears input buffer
void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("Water\n");
}
void b()
{
    printf("Warm\n");
}
void coord(int i, int j)
{
    printf("(%d, %d) %s\n", i+1, j+1, Group[i].Person[j].Name);
}

int getZoneAsInt(char c)
{
	if(c =='H')
	return HOME;
	else if(c == 'A')
		return AWAY;
		else if(c == 'N')
		return NEUT;
    
}

char getZoneAsChar(int zone)
{
    if(zone == HOME)
    return 'H';
    else if(zone == AWAY)
    		return 'A';
    	else if(zone == NEUT)
    		return 'N';
}

int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(0));
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}



//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/leave.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(P.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf( RED "%s is leaving due to bad performance of his team\n"  CLEAR, Zone[Z].Seat[i].Person.Name);
        printf(BLUE"%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;

        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        pthread_mutex_lock(&lock);
        pthread_cond_signal(&cond_seat_freed);
        pthread_mutex_unlock(&lock);
    }
}

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define CLEAR   "\x1b[0m"

// Types
typedef uint Time;

// Person Structure
typedef struct stPerson tPerson;
struct stPerson
{
    int dump[10];
    char Name[100];
    char SupportTeam;
    char SeatedZone;
    Time ArrivalTime;
    Time Patience;
    int EnrageNum;
    int status;
};

// Seat Structure
typedef struct stSeat tSeat;
struct stSeat
{
    tPerson Person;
    int i;
    int j;
};

// Zone Structure
typedef struct stZone tZone;
struct stZone
{
    char Type;
    uint Capacity;
    uint NumSpectators;
    tPerson* Spectator;
    tSeat* Seat;
    pthread_mutex_t* SeatLocks;
};
tZone Zone[3];

// Group Structure
typedef struct stGroup tGroup;
struct stGroup
{
    int dump[10];
    int k;
    int Waiting;
    tPerson* Person;
    pthread_t th[];
};
tGroup* Group;

// Goal Structure
typedef struct stGoal tGoal;
struct stGoal
{
    int dump[30];
    char Team;
    Time GoalTime;
    float GoalProb;
    pthread_mutex_t GoalLock;
};
tGoal* Goal;

// Person info structure
struct stPersonID
{
    int dump[10];
    int groupNo;
    int personNo;
};
typedef struct stPersonID tPersonID;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/input.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"
#include "utility.h"


void inputZones()
{
    Zone[HOME].Type = 'H';         // Home
    Zone[AWAY].Type = 'A';         // Away
    Zone[NEUT].Type = 'N';         // Neutral

    // Input zone capacities
    printf(BLUE "Home | Away | Neutral\n" CLEAR);
    scanf("%d %d %d", &Zone[0].Capacity, &Zone[1].Capacity, &Zone[2].Capacity);

    // Iterate through all the zones
    for(int i = 0; i < 3; i++)
    {
        // Intialize zones. Create locks for every seat in a zone
        Zone[i].NumSpectators = 0;
        Zone[i].Spectator = (tPerson*)malloc(Zone[i].Capacity * sizeof(tPerson));
        Zone[i].Seat = malloc(Zone[i].Capacity * sizeof(tSeat));
        Zone[i].SeatLocks = malloc(Zone[i].Capacity * sizeof(pthread_mutex_t));
        
        // Initialize the lock for each seat in a zone
        for(int j = 0; j < Zone[i].Capacity; j++)
        {
            pthread_mutex_init(&Zone[i].SeatLocks[j], NULL);
        }
    }
}

void inputGroups()
{
    Group = (tGroup*)malloc(num_groups * sizeof(tGroup));

    // Iterate through the groups
    for(int i = 0; i < num_groups; i++)
    {
        flushSTDIN();
        printf( RED "\n(Group %d)\n"   CLEAR, i+1);

        // Number of people in group i
        printf( RED "Number: "   CLEAR);
        scanf("%d", &Group[i].k);
        num_people += Group[i].k;

        // Allocate memory for all persons in a group
        Group[i].Person = (tPerson*)malloc(Group[i].k * sizeof(tPerson));

        // Initially, no one is waiting
        Group[i].Waiting = 0;
        
        // Input persons in the group
        inputPersons(i);
    }
}

void inputPersons(int i)
{
    // Iterate through all the persons in the group
    for(int j = 0; j < Group[i].k; j++)
    {
        // Prefix
        printf( RED "P%d: " CLEAR, j+1);

        // Input person variables
        scanf("%s %c %d %d %d", &Group[i].Person[j].Name,&Group[i].Person[j].SupportTeam,&Group[i].Person[j].ArrivalTime,&Group[i].Person[j].Patience,&Group[i].Person[j].EnrageNum);
        // Group[i].Person[j].status = REACHED;
    }
}

void inputGoals()
{
    Goal = (tGoal*)malloc(G * sizeof(tGoal));

    for(int i = 0; i < G; i++)
    {
        pthread_mutex_init(&Goal[i].GoalLock, NULL);
        flushSTDIN();

        // Prefix
        printf( GREEN "G%d: "   CLEAR, i+1);

        // Input goal variables
        scanf("%c %d %f", &Goal[i].Team,&Goal[i].GoalTime,&Goal[i].GoalProb);
    }   
    printf("\n");
}

void input()
{
    inputZones();

    printf( RED "Spectating Time: " CLEAR);
    scanf("%d", &X);

    // Groups, Persons
    printf( YELLOW "Number of groups: " CLEAR);
    scanf("%d", &num_groups);
    inputGroups();

    // Goals
    printf( GREEN "\nNumber of goal scoring chances: "   CLEAR);
    scanf("%d", &G);
    goal_thread = (pthread_t*)malloc(G * sizeof(pthread_t));
    inputGoals();
}

void printZone(int i)
{
    printf("\nType = %c\n", Zone[i].Type);
    printf("Capacity = %d\n", Zone[i].Capacity);
    for(int j = 0; j < Zone[i].NumSpectators; j++)
    {
        printf("%d: %s\n", j+1, Zone[i].Spectator[j].Name);
    }
    printf("\n");
}

void printGroup(int i)
{
    printf( RED "(Group %d)\nNumber: %d\n",i+1,Group[i].k);
    

    for(int j = 0; j < Group[i].k; j++)
    {
        printf(RED"P %d :",j+1);

        printf("%s\t%c %d %d %d\n", Group[i].Person[j].Name, Group[i].Person[j].SupportTeam ,Group[i].Person[j].ArrivalTime,Group[i].Person[j].Patience, Group[i].Person[j].EnrageNum);
    }
    printf("\n");
}

void printGoals()
{
    for(int i = 0; i < G; i++)
    {
        printf("G%d: %c %d %f\n", i+1, Goal[i].Team, Goal[i].GoalTime, Goal[i].GoalProb);
    }
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/test.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

clock_t getTime();
void delay(int ms);
clock_t start;

int main()
{
    start = clock();
    delay(5000);
    printf("test\n");
}

clock_t getTime()
{
    clock_t diff = clock() - start;
    return (diff * 1000)/CLOCKS_PER_SEC;
}

void delay(int ms)
{
    clock_t timeDelay = ms + clock();
    while(timeDelay > clock());
}
//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/variables.h ####################//

#ifndef VAR_H
#define VAR_H

#define HOME_TEAM "FC Messilona"
#define AWAY_TEAM "Benzdrid CF"

enum tzone{HOMEs,AWAY,NEUT};
enum tstatus{REACHED=11,WAITING,EXIT}

int X;               // Spectating time
int num_groups;      // Total number of groups
int num_people = 0;
int G;               // Number of goal scoring chances

int Goals[2];

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_t* goal_thread;
pthread_cond_t cond_seat_freed = PTHREAD_COND_INITIALIZER;
int seat_freed = 0;

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputZones();
    void inputGroups();
    void inputGoals();
    void inputPersons(int i);
    void printZone(int i);
    void printGroup(int i);

// Leave
void leaveHOME();
void leaveAWAY();

// Seat
void seat(int i, int j, int seatZone, int seatNum);
int seatAvailable(int i);
void noSeat(int G, int P);
int probHome();
int probNeut();
int probAway();

// Goal
void* goal_function(void* arg);
    char* getGoalSuffix(int G);

// Person
void* person_function(void* arg);
    void reach(int i, int j);
    void patience(int G, int P);
    void dinner(int i);

// Main
void per_Thrd();
void g_thrd();
void join();

// Utility
void flushSTDIN();
void a();
void b();
int P(float n);
float R();

#endif

//###########FILE CHANGE ./main_folder/Aarushi Mittal_305880_assignsubmission_file_/AdvancedOS-main/q2/src/goal.c ####################//

#include "libraries.h"
#include "variables.h"
#include "functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf( GREEN "Team H has scored their %d%s goal!\n"  CLEAR, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf( GREEN "Team A has scored their %d%s goal!\n"  CLEAR, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
        return NULL;
    }
    // Goal Missed
    printf( RED "Team %c has missed!\n"  CLEAR, Goal[i].Team);

    pthread_exit(goal_thread[i]);
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}