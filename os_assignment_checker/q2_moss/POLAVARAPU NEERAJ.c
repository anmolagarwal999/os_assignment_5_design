
//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/main.c ####################//

#include "headers.h"

pthread_mutex_t mutexgrouplock[10000];
pthread_cond_t c[10000];

int main()
{
    n_a = 0;
    n_h = 0;
    srand(time(0));
    scaninput();
    goalscount_a = 0;
    goalscount_h = 0;
    for (int i = 0; i < numberofgroups; i++)
    {
        groupcount[i] = 0;
    }
    for (int i = 0; i < numberofgroups; i++)
    {
        pthread_mutex_init(&mutexgrouplock[i], NULL);
        pthread_cond_init(&c[i], NULL);
        cond[i] = 0;
    }

    sem_init(&mutex_a, 0, numberof_a);
    sem_init(&mutex_h, 0, numberof_h);
    sem_init(&mutex_n, 0, numberof_n);
    for (int i = 0; i < numberofpersons; i++)
    {
        sem_init(&goallock[i], 0, 0);
    }
    pthread_t persons[numberofpersons];
    pthread_t matches;

    for (long int i = 0; i < numberofpersons; i++)
    {
        int x = pthread_create(&persons[i], NULL, person, (void *)i);
        if (x != 0)
            printf("Thread can't be created : [%s]\n", strerror(x));
    }

    int y = pthread_create(&matches, NULL, match, NULL);
    if (y != 0)
        printf("Thread can't be created : [%s]\n", strerror(y));

    for (int i = 0; i < numberofpersons; i++)
    {
        pthread_join(persons[i], NULL);
    }

    for (int i = 0; i < numberofpersons; i++)
    {
        sem_destroy(&goallock[i]);
    }
    sem_destroy(&mutex_a);
    sem_destroy(&mutex_h);
    sem_destroy(&mutex_n);

    for (int i = 0; i < numberofgroups; i++)
    {
        pthread_mutex_destroy(&mutexgrouplock[i]);
        pthread_cond_destroy(&c[i]);
    }
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/personthread.c ####################//

#include "headers.h"

void *person(void *arg)
{
    int id = (long int)arg;
    sleep(jointime[id]);

    printf(COLOR_RED "%s has reached the stadium\n" COLOR_RESET, nameofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += patiencetime[id];
    int x;
    if (teamofperson[id] == 'A')
    {
        x = sem_timedwait(&mutex_a, &tm[id]);
    }
    if (teamofperson[id] == 'H')
    {
        x = sem_timedwait(&mutex_h, &tm[id]);
    }
    if (teamofperson[id] == 'N')
    {
        x = sem_timedwait(&mutex_n, &tm[id]);
    }

    if (x == -1 && errno == ETIMEDOUT)
    {
        printf(COLOR_YELLOW "%s could not get a seat\n" COLOR_RESET, nameofperson[id]);
        goto l1;
    }

    printf(COLOR_MAGENTA "%s has got a seat in zone %c\n" COLOR_RESET, nameofperson[id], teamofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += spectime;

    if (teamofperson[id] == 'H' || teamofperson[id] == 'A')
    {
        int y = sem_timedwait(&goallock[id], &tm[id]);
        if (y == -1 && errno == ETIMEDOUT)
        {
            printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
            goto l2;
        }
        else if (y == 0)
        {
            printf(COLOR_MAGENTA "%s is leaving due to bad performance of his team\n" COLOR_RESET, nameofperson[id]);
        }
        sem_post(&goallock[id]);
    }
    else
    {
        sleep(spectime);
        printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
    }
l2:;
    if (teamofperson[id] == 'A')
        sem_post(&mutex_a);
    if (teamofperson[id] == 'H')
        sem_post(&mutex_h);
    if (teamofperson[id] == 'N')
        sem_post(&mutex_n);
l1:;

    // pthread_mutex_lock(&mutexgrouplock[groupid[id]]);
    // groupcount[groupid[id]]++;
    // if (groupcount[groupid[id]] == groupsize[groupid[id]])
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     cond[groupid[id]] = 1;
    //     pthread_cond_signal(&c[groupid[id]]);
    // }
    // else
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     while (cond[groupid[id]] == 0)
    //         pthread_cond_wait(&c[groupid[id]], &mutexgrouplock[groupid[id]]);
    // }

    printf(COLOR_BLUE "%s is leaving for dinner\n" COLOR_RESET, nameofperson[id]);
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/._main.c ####################//

#include "headers.h"

void *person(void *arg)
{
    int id = (long int)arg;
    sleep(jointime[id]);

    printf(COLOR_RED "%s has reached the stadium\n" COLOR_RESET, nameofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += patiencetime[id];
    int x;
    if (teamofperson[id] == 'A')
    {
        x = sem_timedwait(&mutex_a, &tm[id]);
    }
    if (teamofperson[id] == 'H')
    {
        x = sem_timedwait(&mutex_h, &tm[id]);
    }
    if (teamofperson[id] == 'N')
    {
        x = sem_timedwait(&mutex_n, &tm[id]);
    }

    if (x == -1 && errno == ETIMEDOUT)
    {
        printf(COLOR_YELLOW "%s could not get a seat\n" COLOR_RESET, nameofperson[id]);
        goto l1;
    }

    printf(COLOR_MAGENTA "%s has got a seat in zone %c\n" COLOR_RESET, nameofperson[id], teamofperson[id]);

    clock_gettime(CLOCK_REALTIME, &tm[id]);
    tm[id].tv_sec += spectime;

    if (teamofperson[id] == 'H' || teamofperson[id] == 'A')
    {
        int y = sem_timedwait(&goallock[id], &tm[id]);
        if (y == -1 && errno == ETIMEDOUT)
        {
            printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
            goto l2;
        }
        else if (y == 0)
        {
            printf(COLOR_MAGENTA "%s is leaving due to bad performance of his team\n" COLOR_RESET, nameofperson[id]);
        }
        sem_post(&goallock[id]);
    }
    else
    {
        sleep(spectime);
        printf(COLOR_GREEN "%s watched the match for 3 seconds and is leaving\n" COLOR_RESET, nameofperson[id]);
    }
l2:;
    if (teamofperson[id] == 'A')
        sem_post(&mutex_a);
    if (teamofperson[id] == 'H')
        sem_post(&mutex_h);
    if (teamofperson[id] == 'N')
        sem_post(&mutex_n);
l1:;

    // pthread_mutex_lock(&mutexgrouplock[groupid[id]]);
    // groupcount[groupid[id]]++;
    // if (groupcount[groupid[id]] == groupsize[groupid[id]])
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     cond[groupid[id]] = 1;
    //     pthread_cond_signal(&c[groupid[id]]);
    // }
    // else
    // {
    //     pthread_mutex_unlock(&mutexgrouplock[groupid[id]]);
    //     while (cond[groupid[id]] == 0)
    //         pthread_cond_wait(&c[groupid[id]], &mutexgrouplock[groupid[id]]);
    // }

    printf(COLOR_BLUE "%s is leaving for dinner\n" COLOR_RESET, nameofperson[id]);
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/scaninput.c ####################//

#include "headers.h"

void scaninput()
{
    scanf("%d", &numberof_h);
    scanf("%d", &numberof_a);
    scanf("%d", &numberof_n);
    scanf("%d", &spectime);
    scanf("%d", &numberofgroups);

    int z = 0;
    for (int i = 0; i < numberofgroups; i++)
    {
        int x;
        scanf("%d", &x);
        groupsize[i] = x;
        for (int j = 0; j < x; j++)
        {
            scanf("%s %c", nameofperson[z], &teamofperson[z]);
            //scanf("%d", &jointime[z]);
            // scanf("%c", &teamofperson[z]);
            scanf("%d", &jointime[z]);
            scanf("%d", &patiencetime[z]);
            scanf("%d", &irrigoals[z]);
            if (teamofperson[z] == 'A')
            {
                arr_a[n_a] = z;
                n_a++;
            }
            if (teamofperson[z] == 'H')
            {
                arr_h[n_h] = z;
                n_h++;
            }
            groupid[z] = i;
            z++;
        }
    }
    numberofpersons = z;
    scanf("%d", &chances);
    for (int i = 0; i < chances; i++)
    {
        scanf(" %c", &chanceteam[i]);
        scanf("%d", &chancetime[i]);
        scanf("%f", &chanceprob[i]);
    }

    // numberof_h = 2;
    // numberof_a = 1;
    // numberof_n = 2;
    // spectime = 3;
    // strcpy(nameofperson[0], "Vaibhav");
    // teamofperson[0] = 'N';
    // jointime[0] = 3;
    // patiencetime[0] = 2;
    // irrigoals[0] = -1;

    // strcpy(nameofperson[1], "Sarthak");
    // teamofperson[1] = 'H';
    // jointime[1] = 1;
    // patiencetime[1] = 3;
    // irrigoals[1] = 2;

    // strcpy(nameofperson[2], "Ayush");
    // teamofperson[2] = 'A';
    // jointime[2] = 2;
    // patiencetime[2] = 1;
    // irrigoals[2] = 4;

    // strcpy(nameofperson[3], "Rachit");
    // teamofperson[3] = 'H';
    // jointime[3] = 1;
    // patiencetime[3] = 2;
    // irrigoals[3] = 4;

    // strcpy(nameofperson[4], "Roshan");
    // teamofperson[4] = 'N';
    // jointime[4] = 2;
    // patiencetime[4] = 1;
    // irrigoals[4] = -1;

    // strcpy(nameofperson[5], "Adarsh");
    // teamofperson[5] = 'A';
    // jointime[5] = 1;
    // patiencetime[5] = 2;
    // irrigoals[5] = 1;

    // strcpy(nameofperson[6], "Pranav");
    // teamofperson[6] = 'N';
    // jointime[6] = 3;
    // patiencetime[6] = 1;
    // irrigoals[6] = -1;

    // chances = 5;
    // chanceteam[0] = 'H';
    // chancetime[0] = 1;
    // chanceprob[0] = 1;

    // chanceteam[1] = 'A';
    // chancetime[1] = 2;
    // chanceprob[1] = 0.95;

    // chanceteam[2] = 'A';
    // chancetime[2] = 3;
    // chanceprob[2] = 0.5;

    // chanceteam[3] = 'H';
    // chancetime[3] = 5;
    // chanceprob[3] = 0.85;

    // chanceteam[4] = 'H';
    // chancetime[4] = 6;
    // chanceprob[4] = 0.4;

    // numberofpersons = 7;

    // n_a = 2;
    // n_h = 2;
    // arr_a[0] = 2;
    // arr_a[1] = 5;
    // arr_h[0] = 1;
    // arr_h[1] = 3;
    // groupsize[0] = 3;
    // groupsize[1] = 4;

    // groupid[0] = 0;
    // groupid[1] = 0;
    // groupid[2] = 0;
    // groupid[3] = 1;
    // groupid[4] = 1;
    // groupid[5] = 1;
    // groupid[6] = 1;
}

/*
2 1 2
3
2
3
Vibhav N 3 2 -1
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4
*/
//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/matchthread.c ####################//

#include "headers.h"

void *match(void *arg)
{
    int time = 0;
    for (int i = 0; i < chances; i++)
    {
        int t = chancetime[i] - time;
        sleep(t);
        float random = (rand() % 100 + 1) / (float)100;
        if (chanceprob[i] > random)
        {
            if (chanceteam[i] == 'H')
            {
                goalscount_h++;
                printf("Team H has scored their %dth goal\n", goalscount_h);
                for (int j = 0; j < n_a; j++)
                {
                    if (irrigoals[arr_a[j]] == goalscount_h)
                    {
                        sem_post(&goallock[arr_a[j]]);
                    }
                }
            }
            if (chanceteam[i] == 'A')
            {
                goalscount_a++;
                printf("Team A has scored their %dth goal\n", goalscount_a);
                for (int j = 0; j < n_h; j++)
                {
                    if (irrigoals[arr_h[j]] == goalscount_a)
                    {
                        sem_post(&goallock[arr_h[j]]);
                    }
                }
            }
        }
        else
        {
            if (chanceteam[i] == 'H')
            {
                printf("Team H missed the chance to score their %dth goal\n", goalscount_h + 1);
            }
            if (chanceteam[i] == 'A')
            {
                printf("Team A missed the chance to score their %dth goal\n", goalscount_a + 1);
            }
        }

        time = chancetime[i];
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/._matchthread.c ####################//

#include "headers.h"

void *match(void *arg)
{
    int time = 0;
    for (int i = 0; i < chances; i++)
    {
        int t = chancetime[i] - time;
        sleep(t);
        float random = (rand() % 100 + 1) / (float)100;
        if (chanceprob[i] > random)
        {
            if (chanceteam[i] == 'H')
            {
                goalscount_h++;
                printf("Team H has scored their %dth goal\n", goalscount_h);
                for (int j = 0; j < n_a; j++)
                {
                    if (irrigoals[arr_a[j]] == goalscount_h)
                    {
                        sem_post(&goallock[arr_a[j]]);
                    }
                }
            }
            if (chanceteam[i] == 'A')
            {
                goalscount_a++;
                printf("Team A has scored their %dth goal\n", goalscount_a);
                for (int j = 0; j < n_h; j++)
                {
                    if (irrigoals[arr_h[j]] == goalscount_a)
                    {
                        sem_post(&goallock[arr_h[j]]);
                    }
                }
            }
        }
        else
        {
            if (chanceteam[i] == 'H')
            {
                printf("Team H missed the chance to score their %dth goal\n", goalscount_h + 1);
            }
            if (chanceteam[i] == 'A')
            {
                printf("Team A missed the chance to score their %dth goal\n", goalscount_a + 1);
            }
        }

        time = chancetime[i];
    }
    return NULL;
}

//###########FILE CHANGE ./main_folder/POLAVARAPU NEERAJ_305878_assignsubmission_file_/2020101026_assignment_5/q2/headers.h ####################//

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#define numberofpersons_max 10000

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_RESET "\x1b[0m"

#define numberofcourses_max 10000
#define numberofstudents_max 10000
#define numberoflabs_max 10000
#define numberoftas_max 10000

struct queue
{
    int data;
    struct queue *next;
};

struct queue *front[numberofcourses_max];
struct queue *rear[numberofcourses_max];

int numberof_h;
int numberof_a;
int numberof_n;

int spectime;

char nameofperson[numberofpersons_max][10000];
char teamofperson[numberofpersons_max];
int jointime[numberofpersons_max];
int patiencetime[numberofpersons_max];
int irrigoals[numberofpersons_max];

int chances;
int numberofpersons;

char chanceteam[10000];
int chancetime[10000];
float chanceprob[10000];

sem_t mutex_a;
sem_t mutex_h;
sem_t mutex_n;
sem_t goallock[1000];

void scaninput();
void *person(void *arg);

struct timespec tm[1000];
void *match(void *arg);

int goalscount_a;
int goalscount_h;

int arr_a[10000];
int arr_h[10000];

int n_a;
int n_h;

int groupsize[10000];
int groupid[10000];
int numberofgroups;

int groupcount[10000];

pthread_mutex_t mutexgrouplock[10000];
pthread_cond_t c[10000];

int cond[10000];
