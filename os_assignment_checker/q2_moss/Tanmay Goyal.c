
//###########FILE CHANGE ./main_folder/Tanmay Goyal_305861_assignsubmission_file_/q2/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <semaphore.h>
#include <pthread.h>
#include <errno.h>

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

#define ll long long
#define PI 3.14159265
#define br printf("\n")
#define fo(i, n) for (int i = 0; i < n; i++)
#define Fo(i, k, n) for (int i = k; k < n ? i < n : i > n; k < n ? i += 1 : i -= 1)
#define file_read                     \
    freopen("input.txt", "r", stdin); \
    freopen("output.txt", "w", stdout);
int m = 1e9 + 7;

#define MAX_GROUPS 1000
#define MAX_NAME 100
#define MAX_CHANCES 100

struct zone
{
    int capacity;
    int filled;
};

struct people
{
    int gid;
    char *pname;
    // time_t reached_time;
    // time_t patience_time;
    int reached_time;
    int patience_time;
    time_t seat_alloc_time;
    int check;
    char type;
    char zone;
    struct timespec ts;
    int enraged_val;
    int exit_val;
};

struct group
{
    int gid;
    int gsize;
    struct people *ppl_data;
};

struct chance
{
    char adv;
    int elapsed_time;
    float prob;
};
time_t start_time;
typedef struct zone zone;
typedef struct people people;
typedef struct group group;
typedef struct chance chance;

group group_data[MAX_GROUPS];
chance chance_data[MAX_CHANCES];
zone zh, za, zn;
pthread_mutex_t mutex_zh = PTHREAD_MUTEX_INITIALIZER, mutex_za = PTHREAD_MUTEX_INITIALIZER, mutex_zn = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t check = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_check = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t goal_check = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t goal_cond = PTHREAD_COND_INITIALIZER;
int chance_h = 0, chance_a = 0;
int succ_h = 0, succ_a = 0;
int spec_time;
int goalchances = 0;

void *init_spec(void *arg)
{
    people *spec = (people *)arg;
    time_t curr = time(NULL);
    if (spec->reached_time > (curr - start_time))
        sleep(spec->reached_time - curr + start_time);
    // sleep(spec->reached_time);
    printf(RED "%s has reached the stadium\n" RESET, spec->pname);
    spec->check = 0;
    spec->exit_val = 0;
    if (spec->type == 'H')
    {
        do
        {
            if (zh.capacity != zh.filled)
            {
                spec->zone = 'H';
                spec->seat_alloc_time = time(NULL);
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_zh);
                zh.filled++;
                pthread_mutex_unlock(&mutex_zh);
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                spec->check = 1;
                break;
            }
            else if (zn.capacity != zn.filled)
            {
                spec->zone = 'N';
                spec->seat_alloc_time = time(NULL);
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_zn);
                zn.filled++;
                pthread_mutex_unlock(&mutex_zn);
                spec->check = 1;
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                break;
            }
            pthread_mutex_lock(&mutex_check);
            pthread_cond_wait(&check, &mutex_check);
            pthread_mutex_unlock(&mutex_check);

        } while (time(NULL) - spec->seat_alloc_time < spec->patience_time);
    }
    else if (spec->type == 'A')
    {
        do
        {
            if (za.capacity != za.filled)
            {
                spec->seat_alloc_time = time(NULL);
                spec->zone = 'A';
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_za);
                za.filled++;
                pthread_mutex_unlock(&mutex_za);
                spec->check = 1;
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                break;
            }
            pthread_mutex_lock(&mutex_check);
            pthread_cond_wait(&check, &mutex_check);
            pthread_mutex_unlock(&mutex_check);

        } while (time(NULL) - spec->seat_alloc_time < spec->patience_time);
    }
    else
    {
        do
        {
            if (zh.capacity != zh.filled)
            {
                spec->seat_alloc_time = time(NULL);
                spec->zone = 'H';
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_zh);
                zh.filled++;
                pthread_mutex_unlock(&mutex_zh);
                spec->check = 1;
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                break;
            }
            else if (zn.capacity != zn.filled)
            {
                spec->seat_alloc_time = time(NULL);
                spec->zone = 'N';
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_zn);
                zn.filled++;
                pthread_mutex_unlock(&mutex_zn);
                spec->check = 1;
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                break;
            }
            else if (za.capacity != za.filled)
            {
                spec->seat_alloc_time = time(NULL);
                spec->zone = 'A';
                clock_gettime(CLOCK_REALTIME, &spec->ts);
                pthread_mutex_lock(&mutex_za);
                za.filled++;
                pthread_mutex_unlock(&mutex_za);
                spec->check = 1;
                printf(MAGENTA "%s has got a seat in zone %c\n" RESET, spec->pname, spec->zone);
                break;
            }
            // printf("%s\n", spec->pname);
            pthread_mutex_lock(&mutex_check);
            pthread_cond_wait(&check, &mutex_check);
            pthread_mutex_unlock(&mutex_check);
        } while (time(NULL) - spec->seat_alloc_time < spec->patience_time);
    }
    // spec->ts.tv_sec = spec->seat_alloc_time / CLOCKS_PER_SEC + spec_time;
    if (spec->check == 1)
    {
        // printf("I want to exit %s\n", spec->pname);
        spec->ts.tv_sec += spec_time;
        do
        {
            if (spec->type == 'H' && succ_a >= spec->enraged_val)
            {
                printf(GREEN "%s is leaving due to bad defensive performance of his team\n" RESET, spec->pname);
                spec->exit_val = 1;
                break;
            }
            else if (spec->type == 'A' && succ_h >= spec->enraged_val)
            {
                printf(GREEN "%s is leaving due to bad defensive performance of his team\n" RESET, spec->pname);
                spec->exit_val = 1;
                break;
            }
            pthread_mutex_lock(&goal_check);
            int ret_val = pthread_cond_timedwait(&goal_cond, &goal_check, &spec->ts);
            pthread_mutex_unlock(&goal_check);
            if (ret_val == ETIMEDOUT)
            {
                printf(YELLOW "%s watched the match for %d seconds and is leaving\n" RESET, spec->pname, spec_time);
                spec->exit_val = 1;
                break;
            }
        } while (true);
        if (spec->zone == 'H')
        {
            pthread_mutex_lock(&mutex_zh);
            zh.filled--;
            pthread_mutex_unlock(&mutex_zh);
            pthread_mutex_lock(&mutex_check);
            pthread_cond_signal(&check);
            pthread_mutex_unlock(&mutex_check);
        }
        else if (spec->zone == 'N')
        {
            pthread_mutex_lock(&mutex_zn);
            zn.filled--;
            pthread_mutex_unlock(&mutex_zn);
            pthread_mutex_lock(&mutex_check);
            pthread_cond_signal(&check);
            pthread_mutex_unlock(&mutex_check);
        }
        else
        {
            pthread_mutex_lock(&mutex_za);
            za.filled--;
            pthread_mutex_unlock(&mutex_za);
            pthread_mutex_lock(&mutex_check);
            pthread_cond_signal(&check);
            pthread_mutex_unlock(&mutex_check);
        }
    }
    else
    {
        printf(RED "%s couldn't get a seat\n" RESET, spec->pname);
    }
    printf(BLUE "%s is leaving for dinner\n" RESET, spec->pname);
    return NULL;
}

void *init_goals()
{
    fo(i, goalchances)
    {
        // if (i != 0)
        // {
        //     sleep(chance_data[i].elapsed_time - chance_data[i - 1].elapsed_time);
        // }
        // else
        // {
        //     sleep(chance_data[i].elapsed_time);
        // }
        chance c = chance_data[i];
        if (c.elapsed_time > time(NULL) - start_time)
            sleep(c.elapsed_time - (time(NULL) - start_time));

        if (c.adv == 'H')
        {
            chance_h++;
            if (c.prob >= ((double)rand() / RAND_MAX))
            {
                succ_h++;
                if (succ_h == 1)
                {
                    printf(CYAN "Team H scored their 1st goal\n" RESET);
                }
                else if (succ_h == 2)
                {
                    printf(CYAN "Team H scored their 2nd goal\n" RESET);
                }
                else if (succ_h == 3)
                {
                    printf(CYAN "Team H scored their 3rd goal\n" RESET);
                }
                else
                {
                    printf(CYAN "Team H scored their %dth goal\n" RESET, succ_h);
                }
                pthread_mutex_lock(&goal_check);
                pthread_cond_broadcast(&goal_cond);
                pthread_mutex_unlock(&goal_check);
            }
            else
            {
                if (chance_h == 1)
                {
                    printf(CYAN "Team H missed the chance to score their 1st goal\n" RESET);
                }
                else if (chance_h == 2)
                {
                    printf(CYAN "Team H missed the chance to score their 2nd goal\n" RESET);
                }
                else if (chance_h == 3)
                {
                    printf(CYAN "Team H missed the chance to score their 3rd goal\n" RESET);
                }
                else
                {
                    printf(CYAN "Team H missed the chance to score their %dth goal\n" RESET, chance_h);
                }
            }
        }
        else
        {
            chance_a++;
            if (c.prob >= ((double)rand() / RAND_MAX))
            {
                succ_a++;
                if (succ_a == 1)
                {
                    printf(CYAN "Team A scored their 1st goal\n" RESET);
                }
                else if (succ_a == 2)
                {
                    printf(CYAN "Team A scored their 2nd goal\n" RESET);
                }
                else if (succ_a == 3)
                {
                    printf(CYAN "Team A scored their 3rd goal\n" RESET);
                }
                else
                {
                    printf(CYAN "Team A scored their %dth goal\n" RESET, succ_a);
                }
                pthread_mutex_lock(&goal_check);
                pthread_cond_broadcast(&goal_cond);
                pthread_mutex_unlock(&goal_check);
            }
            else
            {
                if (chance_a == 1)
                {
                    printf(CYAN "Team A missed the chance to score their 1st goal\n" RESET);
                }
                else if (chance_a == 2)
                {
                    printf(CYAN "Team A missed the chance to score their 2nd goal\n" RESET);
                }
                else if (chance_a == 3)
                {
                    printf(CYAN "Team A missed the chance to score their 3rd goal\n" RESET);
                }
                else
                {
                    printf(CYAN "Team A missed the chance to score their %dth goal\n" RESET, chance_a);
                }
            }
        }
    }
    return NULL;
}
int main()
{
    srand(time(NULL));
    scanf("%d %d %d", &zh.capacity, &za.capacity, &zn.capacity);
    zh.filled = 0;
    za.filled = 0;
    zn.filled = 0;

    scanf("%d", &spec_time);
    int num_groups;
    int num_ppl = 0;
    scanf("%d", &num_groups);
    fo(i, num_groups)
    {
        scanf("%d", &group_data[i].gsize);
        group_data[i].gid = i;
        group_data[i].ppl_data = (people *)malloc(sizeof(people) * group_data[i].gsize);
        fo(j, group_data[i].gsize)
        {
            people p;
            p.pname = (char *)malloc(sizeof(char) * MAX_NAME);
            p.gid = i;
            scanf("%s", p.pname);
            getchar();
            scanf("%c", &p.type);
            scanf("%d", &p.reached_time);
            scanf("%d", &p.patience_time);
            scanf("%d", &p.enraged_val);
            group_data[i].ppl_data[j] = p;
            num_ppl++;
        }
    }
    scanf("%d", &goalchances);
    getchar();
    fo(i, goalchances)
    {
        chance c;
        c.adv = getchar();
        scanf("%d", &c.elapsed_time);
        scanf("%f", &c.prob);
        chance_data[i] = c;
        getchar();
    }
    pthread_t ppl_threads[num_ppl];
    pthread_t goals;
    int k = 0;
    start_time = time(NULL);
    pthread_create(&goals, NULL, init_goals, NULL);
    fo(i, num_groups)
    {
        fo(j, group_data[i].gsize)
        {
            pthread_create(&ppl_threads[k], NULL, init_spec, &group_data[i].ppl_data[j]);
            k++;
        }
    }
    pthread_join(goals, NULL);
    k = 0;
    fo(i, num_groups)
    {
        fo(j, group_data[i].gsize)
        {
            pthread_join(ppl_threads[k], NULL);
            k++;
        }
    }
}