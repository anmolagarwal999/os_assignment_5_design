
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/spectator.c ####################//

#include "main.h"

void* handle_person(void* arg)
{
    int rc;
    struct timespec ts;
    struct timeval tp;    
    person* human = (person*)arg;
    clock_t init = before;
    clock_t difference;
    do
    {
        difference = clock() - init;
        difference /= CLOCKS_PER_SEC;
    } while (difference < human->T);
    // printf("%ld %d\n",difference,human->T);
    printf(BLU "%s has reached the stadium\n" RESET,human->name);
    human->entered = 1;
    pthread_mutex_lock(speclock+human->id);
    rc =  gettimeofday(&tp, NULL);
    ts.tv_sec  = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += (human->P);
    rc = pthread_cond_timedwait(seat_cond+human->id,speclock+human->id,&ts);
    if (rc==ETIMEDOUT && human->seat==0)
    {
        printf(MAG "%s could not get a seat\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        sleep(1);
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        return NULL;
    }
    
    if (human->team=='A')
    {
        rc =  gettimeofday(&tp, NULL);
        ts.tv_sec  = tp.tv_sec;
        ts.tv_nsec = tp.tv_usec * 1000;
        ts.tv_sec += spectating_time;
        if (h_goals>=human->goals)
        {
            printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
        if (rc==ETIMEDOUT)
        {
            printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    else if (human->team=='H')
    {
        rc =  gettimeofday(&tp, NULL);
        ts.tv_sec  = tp.tv_sec;
        ts.tv_nsec = tp.tv_usec * 1000;
        ts.tv_sec += spectating_time;
        if (a_goals>=human->goals)
        {
            printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
        if (rc==ETIMEDOUT)
        {

            printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
            if (human->zone=='A')
            {
                pthread_mutex_lock(zonelock);
                A.curr_cap--;
                pthread_mutex_unlock(zonelock);
            }
            else if (human->zone=='H')
            {
                pthread_mutex_lock(zonelock+1);
                H.curr_cap--;
                pthread_mutex_unlock(zonelock+1);
            }
            else if (human->zone=='N')
            {
                pthread_mutex_lock(zonelock+2);
                N.curr_cap--;
                pthread_mutex_unlock(zonelock+2);
            }  
            
            printf(GRN "%s is leaving for dinner\n" RESET,human->name);
            pthread_mutex_unlock(speclock+human->id);
            return NULL;
        }
        printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    rc = pthread_cond_timedwait(enraged+human->id,speclock+human->id,&ts);
    if (rc==ETIMEDOUT)
    {
        printf(YEL "%s watched the match for %d seconds and is leaving\n" RESET,human->name,spectating_time);
        if (human->zone=='A')
        {
            pthread_mutex_lock(zonelock);
            A.curr_cap--;
            pthread_mutex_unlock(zonelock);
        }
        else if (human->zone=='H')
        {
            pthread_mutex_lock(zonelock+1);
            H.curr_cap--;
            pthread_mutex_unlock(zonelock+1);
        }
        else if (human->zone=='N')
        {
            pthread_mutex_lock(zonelock+2);
            N.curr_cap--;
            pthread_mutex_unlock(zonelock+2);
        }  
        
        printf(GRN "%s is leaving for dinner\n" RESET,human->name);
        pthread_mutex_unlock(speclock+human->id);
        return NULL;
    }
    printf(CYN "%s is leaving due to the bad defensive performance of his team\n" RESET,human->name);
    if (human->zone=='A')
    {
        pthread_mutex_lock(zonelock);
        A.curr_cap--;
        pthread_mutex_unlock(zonelock);
    }
    else if (human->zone=='H')
    {
        pthread_mutex_lock(zonelock+1);
        H.curr_cap--;
        pthread_mutex_unlock(zonelock+1);
    }
    else if (human->zone=='N')
    {
        pthread_mutex_lock(zonelock+2);
        N.curr_cap--;
        pthread_mutex_unlock(zonelock+2);
    }    
    
    printf(GRN "%s is leaving for dinner\n" RESET,human->name);
    pthread_mutex_unlock(speclock+human->id);
    return NULL;
}
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/main.c ####################//

#include "main.h"

int rng(int a, int b)
{ // [a, b]
    int dif = b - a + 1;
    int rnd = rand() % dif;
    return (a + rnd);
}

int main()
{
    srand(time(0));
    pthread_mutex_init(&glock,NULL);
    A.id = 0;
    H.id = 1;
    N.id = 2;
    for (int i = 0; i < 3; i++)
    {
        pthread_mutex_init(zonelock+i,NULL);   
    }
    a_goals = 0;
    h_goals = 0;
    scanf("%d%d%d", &H.max_cap, &A.max_cap, &N.max_cap);
    H.curr_cap = 0;
    A.curr_cap = 0;
    N.curr_cap = 0;
    H.zone_id = 'H';
    N.zone_id = 'N';
    A.zone_id = 'A';
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);
    num_people = 0;
    for (int i = 0; i < num_groups; i++)
    {
        int k;
        scanf("%d", &k);

        for (int j = 0; j < k; j++)
        {
            slist[num_people + j].group_no = i;
            slist[num_people + j].seat = 0;
            slist[num_people + j].id = num_people + j;
            slist[num_people+j].entered = 0;
            scanf("%s%s%d%d%d", slist[num_people + j].name, &slist[num_people + j].team, &slist[num_people + j].T, &slist[num_people + j].P, &slist[num_people + j].goals);
            // printf("%s %c %d %d %d\n",slist[num_people + j].name, slist[num_people + j].team, slist[num_people + j].T, slist[num_people + j].P, slist[num_people + j].goals);
            pthread_mutex_init(speclock + num_people + j, NULL);
            pthread_cond_init(seat_cond + num_people + j, NULL);
            pthread_cond_init(enraged + num_people + j, NULL);

        }
        num_people += k;
    }
    scanf("%d", &G);
    for (int i = 0; i < G; i++)
    {
        scanf("%s%d%lf", &glist[i].team, &glist[i].time, &glist[i].prob);
    }
    before = clock();

    pthread_create(&a_zon, NULL, handle_zone, &A);
    pthread_create(&h_zon, NULL, handle_zone, &H);
    pthread_create(&n_zon, NULL, handle_zone, &N);
    pthread_create(&gol, NULL, handle_goals, glist);

    for (int i = 0; i < num_people; i++)
    {
        pthread_create(spectator + i, NULL, handle_person, slist + i);
    }

    for (int i = 0; i < num_people; i++)
    {
        pthread_join(spectator[i], NULL);
    }

    printf(RED "SIMULATION OVER!!!\n" RESET);

    return 0;
}

//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/goals.c ####################//

#include "main.h"

void *handle_goals(void *arg)
{
    gchance *glist = (gchance *)arg;
    for (int i = 0; i < G; i++)
    {
        clock_t init = before;
        clock_t difference;
        do
        {
            difference = clock() - init;
            difference /= CLOCKS_PER_SEC;
        } while (difference < glist[i].time);
        
        int x = rand() % 100;
        double prob = glist[i].prob * 100.00f;
        if (x <= prob)
        {
            pthread_mutex_lock(&glock);
            if (glist[i].team == 'A')
            {
                a_goals++;
                if (a_goals%10==1)
                {
                    printf("Team %c has scored their %dst goal\n",glist[i].team,a_goals);
                }
                else if (a_goals%10==2)
                {
                    printf("Team %c has scored their %dnd goal\n",glist[i].team,a_goals);                    
                }
                else if (a_goals%10==3)
                {
                    printf("Team %c has scored their %drd goal\n",glist[i].team,a_goals);
                }
                else
                {
                    printf("Team %c has scored their %dth goal\n",glist[i].team,a_goals);
                }
                for (int j = 0; j < num_people; j++)
                {
                    if (slist[j].team=='H' && slist[j].goals<=a_goals )
                    {
                        pthread_cond_signal(enraged+j);
                    }
                }
                
                
            }
            else if(glist[i].team == 'H')
            {
                h_goals++;
                if (h_goals%10==1)
                {
                    printf("Team %c has scored their %dst goal\n",glist[i].team,h_goals);
                }
                else if (h_goals%10==2)
                {
                    printf("Team %c has scored their %dnd goal\n",glist[i].team,h_goals);                    
                }
                else if (h_goals%10==3)
                {
                    printf("Team %c has scored their %drd goal\n",glist[i].team,h_goals);
                }
                else
                {
                    printf("Team %c has scored their %dth goal\n",glist[i].team,h_goals);
                }
                for (int j = 0; j < num_people; j++)
                {
                    if (slist[j].team=='A' && slist[j].goals<=h_goals )
                    {
                        pthread_cond_signal(enraged+j);
                    }
                }
            }
            
            pthread_mutex_unlock(&glock);
        }
        else
        {
            pthread_mutex_lock(&glock);
            if (glist[i].team == 'A')
            {
                if (a_goals%10==1)
                {
                    printf("Team %c missed the chance to score their %dnd goal\n",glist[i].team,a_goals+1);
                }
                else if (a_goals%10==2)
                {
                    printf("Team %c missed the chance to score their %drd goal\n",glist[i].team,a_goals+1);                    
                }
                else if (a_goals%10==0)
                {
                    printf("Team %c missed the chance to score their %dst goal\n",glist[i].team,a_goals+1);
                }
                else
                {
                    printf("Team %c missed the chance to score their %dth goal\n",glist[i].team,a_goals+1);
                }
                
            }
            else if(glist[i].team == 'H')
            {
                if (h_goals%10==1)
                {
                    printf("Team %c missed the chance to score their %dnd goal\n",glist[i].team,h_goals+1);
                }
                else if (h_goals%10==2)
                {
                    printf("Team %c missed the chance to score their %drd goal\n",glist[i].team,h_goals+1);                    
                }
                else if (h_goals%10==0)
                {
                    printf("Team %c missed the chance to score their %dst goal\n",glist[i].team,h_goals+1);
                }
                else
                {
                    printf("Team %c missed the chance to score their %dth goal\n",glist[i].team,h_goals+1);
                }
            }
            
            pthread_mutex_unlock(&glock);
        }
        
    }
    return NULL;
}
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/main.h ####################//

#ifndef __MAIN__H__
#define __MAIN__H__

#include "colors.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <semaphore.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <stdbool.h>

#define debug(x) printf("#%d : %d\n",x,x);
#define MAX_SPECTATORS 512
#define MAX_GROUPS 512
#define MAX_LENGTH 512
#define GOALING_CHANCES 1024

clock_t before;

int spectating_time;
int h_goals;
pthread_mutex_t glock;
int a_goals;
int num_groups;
int num_people;
int G;

typedef struct zone
{
    int id;
    int max_cap;
    char zone_id;
    int curr_cap;
}zone;

typedef struct spectator
{
    int id;
    int group_no;
    int entered;
    char name[MAX_LENGTH];
    char team;
    char zone;
    int T;
    int P;
    int goals;
    int seat;
}person;

typedef struct goal_chance
{
    char team;
    int time;
    double prob;
}gchance;

pthread_t spectator[MAX_SPECTATORS];
pthread_mutex_t speclock[MAX_SPECTATORS];
pthread_cond_t seat_cond[MAX_SPECTATORS];
pthread_cond_t enraged[MAX_SPECTATORS];
pthread_t a_zon;
pthread_t h_zon;
pthread_t n_zon;
pthread_t gol;
zone A;
zone H;
zone N;
pthread_mutex_t zonelock[3];
person slist[MAX_SPECTATORS];
gchance glist[GOALING_CHANCES];

void* handle_person(void* arg);
void* handle_zone(void* arg);
void* handle_goals(void* arg);


#endif  //!__MAIN__H__
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/colors.h ####################//

#ifndef __COLORS__H__
#define __COLORS__H__

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m" 

#endif  //!__COLORS__H__
//###########FILE CHANGE ./main_folder/Sanyam Shah_305979_assignsubmission_file_/2020101012_Assignment_5_Sanyam_Shah/q2/zone.c ####################//

#include "main.h"

void *handle_zone(void *arg)
{
    zone *curr_zone = (zone *)arg;
    while (1)
    {
        for (int i = 0; i < num_people; i++)
        {
            if (curr_zone->curr_cap < curr_zone->max_cap && slist[i].seat == 0 && slist[i].entered == 1)
            {
                int x = rand()%2;
                if (x==1)
                {
                    pthread_mutex_lock(zonelock + curr_zone->id);
                    if (slist[i].team == curr_zone->zone_id || slist[i].team == 'N' || (curr_zone->zone_id == 'N' && slist[i].team == 'H'))
                    {
                        pthread_mutex_lock(speclock + i);
                        slist[i].zone = curr_zone->zone_id;
                        slist[i].seat = 1;
                        curr_zone->curr_cap++;
                        printf(BLU "%s has got a seat in zone %c\n" RESET, slist[i].name, curr_zone->zone_id);
                        pthread_mutex_unlock(speclock + i);
                        pthread_cond_signal(seat_cond + i);
                    }
                    pthread_mutex_unlock(zonelock + curr_zone->id);
                }
                else
                {
                    pthread_mutex_lock(zonelock + curr_zone->id);
                    if ((curr_zone->zone_id == 'N' && slist[i].team == 'H') || slist[i].team == curr_zone->zone_id || slist[i].team == 'N')
                    {
                        pthread_mutex_lock(speclock + i);
                        slist[i].zone = curr_zone->zone_id;
                        slist[i].seat = 1;
                        curr_zone->curr_cap++;
                        printf(BLU "%s has got a seat in zone %c\n" RESET, slist[i].name, curr_zone->zone_id);
                        pthread_mutex_unlock(speclock + i);
                        pthread_cond_signal(seat_cond + i);
                    }
                    pthread_mutex_unlock(zonelock + curr_zone->id);
                }
                
                
            }
        }
    }
}