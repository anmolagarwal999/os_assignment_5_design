
//###########FILE CHANGE ./main_folder/SAI VENU GOPAL TALLAPRAGADA_305954_assignsubmission_file_/q2/q2.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_BLACK   "\x1b[30m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
struct person
{
    char name[256];
    char zone[2];
    int time;
    int p_time;
    int num_goals;
    int arrived;
    char got_seat_in_zone[2];
    int waiting;
};
struct goals
{
    char team[2];
    int time;
    float prob;
};
int X;
int actual_time = 1;
int A_GOAL = 0, H_GOAL = 0;
int total_number_of_people;
int H_capacity, A_capacity, N_capacity, g_scoring;
struct person people[1000];
struct goals chances[1000];
bool seated[1000];
sem_t seming[1000];
sem_t waiting[1000];
int ended = 0;
sem_t end;
int get_value(float p)
{
    double val = (double)rand() / RAND_MAX;
    return val < p;
}
void *time_function()
{
    while (1)
    {
        if(ended == 1 + total_number_of_people)
        {
            return NULL;
        }
        actual_time++;
        sleep(1);
    }
}
void rage_quit(char x)
{
    for (int i = 0; i < total_number_of_people; i++)
    {
        if (people[i].arrived == 1 && people[i].waiting == 0 && people[i].num_goals != -1)
        {
            if (x != people[i].zone[0])
            {
                if (x == 'A' && A_GOAL >= people[i].num_goals)
                {
                    sem_post(&waiting[i]);
                }
                if (x == 'H' && H_GOAL >= people[i].num_goals)
                {
                    sem_post(&waiting[i]);
                }
            }
        }
    }
}
void *person_function(void *ptr)
{
    int index1 = *(int *)ptr;
    // wait till arrival actual_time
    while (actual_time < people[index1].time)
    {
        sleep(1);
    }
    printf(ANSI_COLOR_BLUE "t = %d: %s has reached the stadium\n"ANSI_COLOR_RESET, actual_time, people[index1].name); // person with name arrived
    // conditions for seat status if not found sleep til p_actual_time or till vacant
    people[index1].arrived = 1;
    people[index1].waiting = 1;
    if (people[index1].zone[0] == 'A')
    {
        if (A_capacity > 0)
        {
            A_capacity--;
            seated[index1] = true;
            people[index1].got_seat_in_zone[0] = 'A';
            people[index1].waiting = 0;
            printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
        }
        else
        {
            struct timespec timingspec;
            clock_gettime(CLOCK_REALTIME, &timingspec);
            timingspec.tv_sec += people[index1].p_time;
            if (sem_timedwait(&seming[index1], &timingspec) == -1)
            {
                printf(ANSI_COLOR_BLUE "t = %d: %s couldn’t get a seat\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                people[index1].waiting = 0;
                ended++;
                return NULL;
            }
            else
            {
                people[index1].waiting = 0;
            }
        }
    }
    else
    {
        if (people[index1].zone[0] == 'N')
        {
            if (N_capacity > 0)
            {
                N_capacity--;
                seated[index1] = true;
                people[index1].got_seat_in_zone[0] = 'N';
                people[index1].waiting = 0;
                printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone N\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
            }
            else
            {
                if (A_capacity > 0)
                {
                    A_capacity--;
                    seated[index1] = true;
                    people[index1].got_seat_in_zone[0] = 'A';
                    people[index1].waiting = 0;
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone A\n"ANSI_COLOR_RESET, actual_time, people[index1].name);
                }
                else if (H_capacity > 0)
                {
                    H_capacity--;
                    seated[index1] = true;
                    people[index1].got_seat_in_zone[0] = 'H';
                    people[index1].waiting = 0;
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone H\n"ANSI_COLOR_RESET, actual_time, people[index1].name);
                }
                else
                {
                    struct timespec timingspec;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    timingspec.tv_sec += people[index1].p_time;
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        printf(ANSI_COLOR_BLUE "t = %d:%s couldn’t get a seat\n"ANSI_COLOR_RESET, actual_time, people[index1].name);
                        people[index1].waiting = 0;
                        return NULL;
                        ended++;
                    }
                    else
                    {
                        people[index1].waiting = 0;
                    }
                }
            }
        }
        else
        {
            if (H_capacity > 0)
            {
                H_capacity--;
                seated[index1] = true;
                people[index1].got_seat_in_zone[0] = 'H';
                people[index1].waiting = 0;
                printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone H\n"ANSI_COLOR_RESET, actual_time, people[index1].name);
            }
            else
            {
                if (N_capacity > 0)
                {
                    N_capacity--;
                    seated[index1] = true;
                    people[index1].got_seat_in_zone[0] = 'N';
                    people[index1].waiting = 0;
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone N\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                }
                else
                {
                    struct timespec timingspec;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    timingspec.tv_sec += people[index1].p_time;
                    int valing = sem_timedwait(&seming[index1],&timingspec);
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        printf(ANSI_COLOR_BLUE "t = %d:%s couldn’t get a seat\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                        people[index1].waiting = 0;
                        ended++;
                        return NULL;
                    }
                    else
                    {
                        people[index1].waiting = 0;
                    }
                }
            }
        }
    }
    struct timespec timedspec;
    clock_gettime(CLOCK_REALTIME, &timedspec);
    timedspec.tv_sec += X;
    if (sem_timedwait(&waiting[index1], &timedspec) != -1)
    {
        printf(ANSI_COLOR_CYAN "t = %d: %s is leaving due to bad defensive performance of his team\n"ANSI_COLOR_RESET, actual_time, people[index1].name); // rage quit
    }
    else
    {
        printf(ANSI_COLOR_MAGENTA "t = %d: %s watched the match for %d seconds and is leaving\n"ANSI_COLOR_RESET , actual_time, people[index1].name, X); // normally exitted
    }
    char c = people[index1].got_seat_in_zone[0];
    int flag = 0;
    for (int i = 0; i < total_number_of_people; i++)
    {
        if (i == index1)
        {
            continue;
        }
        if (people[i].arrived == 1 && people[i].waiting == 1)
        {
            if (c == 'A')
            {
                if (people[i].zone[0] == 'A')
                {
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET , actual_time, people[i].name, c);
                    people[i].got_seat_in_zone[0] = 'A';
                    //printf("%s sempost working\n", people[i].name);
                    sem_post(&seming[i]);
                    people[i].waiting = 0;
                    //got seat
                    flag = 1;
                    break;
                }
                if (people[i].zone[0] == 'N')
                {
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET, actual_time, people[i].name, c);
                    people[i].got_seat_in_zone[0] = 'A';
                    //printf("%s sempost working\n", people[i].name);
                    people[i].waiting = 0;
                    sem_post(&seming[i]);
                    flag = 1;
                    break;
                }
            }
            else
            {
                if (c == 'N')
                {
                    if (people[i].zone[0] == 'H')
                    {
                        printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET  , actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'N';
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        //got seat
                        flag = 1;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'N';
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        flag = 1;
                        break;
                    }
                }
                else
                {
                    if (people[i].zone[0] == 'H')
                    {
                        printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'H';
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        flag = 1;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        printf(ANSI_COLOR_GREEN"t = %d: %s has got a seat in zone %c\n"ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'H';
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        flag = 1;
                        break;
                    }
                }
            }
        }
    }
    if(flag == 0)
    {
        if(c == 'A')
        {
            A_capacity++;
        }
        else
        {
            if(c == 'N')
            {
                N_capacity++;
            }
            else
            {
                H_capacity++;
            }
        }
    }
    ended++;
    printf(ANSI_COLOR_WHITE"t = %d: %s is leaving for dinner\n"ANSI_COLOR_RESET,actual_time,people[index1].name);
}
void *goal_timing()
{
    int count = 0;
    for (int i = 0; i < g_scoring; i++)
    {
        while (actual_time < chances[i].time)
        {
            sleep(1);
        }
        int x = get_value(chances[i].prob);
        //printf("Chance : %d\n", x);
        if (chances[i].team[0] == 'A')
        {
            A_GOAL += x;
            if (x == 1)
            {
                printf(ANSI_COLOR_YELLOW "t = %d: Team A has scored their %d goal\n"ANSI_COLOR_RESET, actual_time, A_GOAL);
                char x0 = 'A';
                rage_quit(x0);
                continue;
            }
            else
            {
                printf(ANSI_COLOR_BLACK"t = %d: Team A missed the chance to score their %d goal\n"ANSI_COLOR_RESET, actual_time, A_GOAL + 1);
                continue;
            }
        }
        else
        {
            H_GOAL += x;
            if (x == 1)
            {
                printf(ANSI_COLOR_YELLOW "t = %d: Team H has scored their %d goal\n"ANSI_COLOR_RESET , actual_time, H_GOAL);
                char x0 = 'H';
                rage_quit(x0);
                continue;
            }
            else
            {
                printf(ANSI_COLOR_BLACK"t = %d: Team H missed the chance to score their %d goal\n"ANSI_COLOR_RESET, actual_time, H_GOAL + 1);
                continue;
            }
        }
        sleep(1);
    }
    ended++;
    return NULL;
}
int main()
{
    pthread_mutex_t locks[1000];
    for (int i = 0; i < 1000; i++)
    {
        pthread_mutex_init(&locks[i], NULL);
    }
    srand(time(0));
    scanf("%d %d %d", &H_capacity, &A_capacity, &N_capacity);
    scanf("%d", &X);
    int groups;
    scanf("%d", &groups);
    int total_no_of_people = 0;
    int count = 0;
    for (int i = 0; i < groups; i++)
    {
        int number_of_people;
        scanf("%d", &number_of_people);
        for (int j = 0; j < number_of_people; j++)
        {
            scanf("%s %s %d %d %d", people[count].name, people[count].zone, &people[count].time, &people[count].p_time, &people[count].num_goals);
            count++;
            total_no_of_people++;
        }
    }
    total_number_of_people = total_no_of_people;
    scanf("%d", &g_scoring);
    for (int i = 0; i < g_scoring; i++)
    {
        scanf("%s %d %f", chances[i].team, &chances[i].time, &chances[i].prob);
    }
    pthread_t persons[total_no_of_people];
    for (int i = 0; i < total_no_of_people; i++)
    {
        pthread_create(&persons[i], NULL, person_function, &i);
        usleep(100);
    }
    pthread_t goals_scored, timing;
    pthread_create(&goals_scored, NULL, goal_timing, NULL);
    pthread_create(&timing, NULL, time_function, NULL);
    for (int i = 0; i < total_no_of_people; i++)
    {
        pthread_join(persons[i], NULL);
    }
    pthread_join(goals_scored, NULL);
    pthread_join(timing, NULL);
    for (int i = 0; i < 1000; i++)
    {
        pthread_mutex_destroy(&locks[i]);
    }
}