
//###########FILE CHANGE ./main_folder/Kushagra Kharbanda_305971_assignsubmission_file_/2020101002_assignment_5/q2/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define MAX_NAME_LENGTH 20

time_t starting_time;
time_t current_time;

struct Zone
{
    int max_capacity;
    int num_seats_filled;
};
typedef struct Zone Zone;
Zone zone_H, zone_N, zone_A;

// add state variable and shuffle places
struct spectator
{
    char name[MAX_NAME_LENGTH];
    char type;
    int arrival_time;
    int patience_limit;
    int max_goal_limit;
    char current_zone;
    int group_id;
};
typedef struct spectator spectator;

struct Group
{
    int group_id;
    int group_size;
    spectator *spectators;
    pthread_mutex_t mutex_group;
    pthread_cond_t cond_group;
    int at_exit;
};
typedef struct Group group;
group *groups;

struct Goal
{
    char type;
    float prob;
    int time_since_start;
};
typedef struct Goal goal;

// some gloabal variables
int spectating_time, num_groups, num_goals;
int total_specs = 0;
int goals_H = 0, goals_A = 0;

// Locks and semaphores
sem_t sem_H_N, sem_H_N_A, A;
pthread_mutex_t mutex_H, mutex_A, mutex_N;
pthread_mutex_t mutex_H_goal, mutex_A_goal;
pthread_cond_t cond_H_goal, cond_A_goal;

// change colour names at the end
#define RED     "\x1B[31m"
#define GREEN   "\x1B[32m"
#define YELLOW  "\x1B[33m"
#define BLUE    "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN    "\x1B[36m"
#define RESET   "\x1B[0m"

void conditional_sem_wait(char bitmask[])
{
    int sem_val;
    if(strcmp(bitmask, "HNA") == 0)
    {
        sem_getvalue(&sem_H_N_A, &sem_val);
        if (sem_val > 0)
        {
            sem_wait(&sem_H_N_A);
        }
    }
    else if(strcmp(bitmask, "HN") == 0)
    {
        sem_getvalue(&sem_H_N, &sem_val);
        if (sem_val > 0)
        {
            sem_wait(&sem_H_N);
        }
    }
    else if(strcmp(bitmask, "A") == 0)
    {
        sem_getvalue(&A, &sem_val);
        if (sem_val > 0)
        {
            sem_wait(&A);
        }
    }
}

void *start_spectator(void *arg)
{
    struct timespec timespec_struct1, timespec_struct2;
    spectator spect = *(spectator *)arg;
    // sleep till arrival time
    sleep(spect.arrival_time);
    printf(BLUE "t=%ld : %s has reached the stadium\n" RESET, time(NULL) - starting_time, spect.name);

    // waiting for a seat
    clock_gettime(CLOCK_REALTIME, &timespec_struct1);
    timespec_struct1.tv_sec = timespec_struct1.tv_sec + spect.patience_limit;
    
    if (spect.type == 'H')
    {
        int ret_value;
        while (ret_value = sem_timedwait(&sem_H_N, &timespec_struct1) == -1) ;
        if (ret_value == -1)
        {
            printf(YELLOW "t=%ld : %s could not get a seat\n" RESET, time(NULL) - starting_time, spect.name);
            goto exit_ceremony;
        }
        if (ret_value == 0)
        {
            pthread_mutex_lock(&mutex_H);
            pthread_mutex_lock(&mutex_N);
            if (zone_H.max_capacity - zone_H.num_seats_filled > 0)
            {
                if(zone_N.max_capacity - zone_N.num_seats_filled > 0)
                {
                    if (rand()%2 == 0)
                    {
                        zone_N.num_seats_filled++;
                        spect.current_zone = 'N';                      
                    }
                    else
                    {
                        zone_H.num_seats_filled++;
                        spect.current_zone = 'H';
                    }
                }
                else
                {
                    zone_H.num_seats_filled++;
                    spect.current_zone = 'H';
                }
            }
            else if (zone_N.max_capacity - zone_N.num_seats_filled > 0)
            {
                zone_N.num_seats_filled++;
                spect.current_zone = 'N';          
            }
            conditional_sem_wait("HNA");
            pthread_mutex_unlock(&mutex_N);
            pthread_mutex_unlock(&mutex_H);
            printf(GREEN "t=%ld : %s has got a seat in zone %c\n" RESET, time(NULL) - starting_time, spect.name, spect.current_zone);
        }

        // spectating the match
        clock_gettime(CLOCK_REALTIME, &timespec_struct2);
        timespec_struct2.tv_sec = timespec_struct2.tv_sec + spectating_time;
        
        pthread_mutex_lock(&mutex_A_goal);
        int initial_time = time(NULL) - starting_time;
        int cond_ret_value;
        int gone = 0;
        while (goals_A < spect.max_goal_limit && cond_ret_value != ETIMEDOUT)
        {
            cond_ret_value = pthread_cond_timedwait(&cond_A_goal, &mutex_A_goal, &timespec_struct2);
        }
        int final_time = time(NULL) - starting_time;
        pthread_mutex_unlock(&mutex_A_goal);
        if ((final_time - initial_time) >= spectating_time || cond_ret_value == ETIMEDOUT)
        {
            printf(CYAN "t=%ld : %s watched the match for %d seconds and is leaving\n" RESET, time(NULL) - starting_time, spect.name, spectating_time);
        }
        else
        {
            printf(MAGENTA "t=%ld : %s is leaving due to bad performance of his team\n" RESET, time(NULL) - starting_time, spect.name);
        }
        
        if (spect.current_zone == 'N')
        {
            pthread_mutex_lock(&mutex_N);
            zone_N.num_seats_filled--;
            pthread_mutex_unlock(&mutex_N);
            gone = 1;
        }
        else
        {
            pthread_mutex_lock(&mutex_H);
            zone_H.num_seats_filled--;
            pthread_mutex_unlock(&mutex_H);
            gone = 1;
        }
        sem_post(&sem_H_N);
        sem_post(&sem_H_N_A);
    }

    if (spect.type == 'N')
    {
        int ret_value, error_num;
        while (ret_value = sem_timedwait(&sem_H_N_A, &timespec_struct1) == -1) ;
        if (ret_value == -1)
        {
            printf(YELLOW "t=%ld : %s could not get a seat\n" RESET, time(NULL) - starting_time, spect.name);
            goto exit_ceremony;
        }
        if (ret_value == 0)
        {
            pthread_mutex_lock(&mutex_H);
            pthread_mutex_lock(&mutex_N);
            pthread_mutex_lock(&mutex_A);
            int gone = 0;
            if (zone_H.max_capacity - zone_H.num_seats_filled > 0)
            {
                if(zone_N.max_capacity - zone_N.num_seats_filled > 0)
                {
                    if(zone_A.max_capacity - zone_A.num_seats_filled > 0)
                    {
                        int rand_prob = rand() % 3;
                        if (rand_prob == 0)
                        {
                            zone_N.num_seats_filled++;
                            spect.current_zone = 'N';
                            conditional_sem_wait("HN");
                        }
                        else if (rand_prob == 1)
                        {
                            zone_H.num_seats_filled++;
                            spect.current_zone = 'H';
                            conditional_sem_wait("HN");
                        }
                        else
                        {
                            zone_A.num_seats_filled++;
                            spect.current_zone = 'A';
                            conditional_sem_wait("A");
                        }
                    }
                    else
                    {
                        if (rand()%2 == 0)
                        {
                            zone_H.num_seats_filled++;
                            spect.current_zone = 'H';
                            conditional_sem_wait("HN");
                        }
                        else
                        {
                            zone_N.num_seats_filled++;
                            spect.current_zone = 'N';
                            conditional_sem_wait("HN");
                        }
                    }
                }
                else
                {
                    if(zone_A.max_capacity - zone_A.num_seats_filled > 0)
                    {
                        if (rand()%2 == 0)
                        {
                            zone_H.num_seats_filled++;
                            spect.current_zone = 'H';
                            conditional_sem_wait("HN");
                        }
                        else
                        {
                            zone_N.num_seats_filled++;
                            spect.current_zone = 'A';
                            conditional_sem_wait("A");
                        }
                    }
                    else
                    {
                        zone_H.num_seats_filled++;
                        spect.current_zone = 'H';
                        int sem_val;
                        conditional_sem_wait("HN");
                    }
                }
            }
            else if (zone_N.max_capacity - zone_N.num_seats_filled > 0 && zone_A.max_capacity - zone_A.num_seats_filled > 0)
            {
                if(zone_A.max_capacity - zone_A.num_seats_filled > 0)
                {
                    if (rand() % 2 == 0)
                    {
                        zone_N.num_seats_filled++;
                        spect.current_zone = 'N';
                        int sem_val;
                        conditional_sem_wait("HN");
                    }
                    else
                    {
                        zone_A.num_seats_filled++;
                        spect.current_zone = 'A';
                        conditional_sem_wait("A");
                    }
                }
                else
                {
                    zone_N.num_seats_filled++;
                    spect.current_zone = 'N';
                    int sem_val;
                    conditional_sem_wait("HN");
                }
            }
            else if (zone_A.max_capacity - zone_A.num_seats_filled > 0)
            {
                zone_A.num_seats_filled++;
                spect.current_zone = 'A';
                conditional_sem_wait("A");
            }
            pthread_mutex_unlock(&mutex_A);
            pthread_mutex_unlock(&mutex_N);
            pthread_mutex_unlock(&mutex_H);
            printf(GREEN "t=%ld : %s has got a seat in zone %c\n" RESET, time(NULL) - starting_time, spect.name, spect.current_zone);
        }

        // Watching the match
        clock_gettime(CLOCK_REALTIME, &timespec_struct2);
        timespec_struct2.tv_sec = timespec_struct2.tv_sec + spectating_time;
        int gone = 0;
        sleep(spectating_time);
        printf(CYAN "t=%ld : %s watched the match for %d seconds and is leaving\n" RESET, time(NULL) - starting_time, spect.name, spectating_time);
        if (spect.current_zone == 'N')
        {
            pthread_mutex_lock(&mutex_N);
            zone_N.num_seats_filled--;
            pthread_mutex_unlock(&mutex_N);
            gone = 1;
            sem_post(&sem_H_N);           
        }
        else if (spect.current_zone == 'H')
        {
            pthread_mutex_lock(&mutex_H);
            zone_H.num_seats_filled--;
            pthread_mutex_unlock(&mutex_H);
            gone = 1;
            sem_post(&sem_H_N);
        }
        else if(spect.current_zone == 'A')
        {
            pthread_mutex_lock(&mutex_A);
            zone_A.num_seats_filled--;
            pthread_mutex_unlock(&mutex_A);
            gone = 1;
            sem_post(&A);
        }
        sem_post(&sem_H_N_A);
    }

    if (spect.type == 'A')
    {
        int ret_value;
        while (ret_value = sem_timedwait(&A, &timespec_struct1) == -1);
        if (ret_value == -1)
        {
            printf(YELLOW "t=%ld : %s could not get a seat\n" RESET, time(NULL) - starting_time, spect.name);
            goto exit_ceremony;
        }
        else if (ret_value == 0)
        {
            pthread_mutex_lock(&mutex_A);
            zone_A.num_seats_filled++;
            pthread_mutex_unlock(&mutex_A);
            conditional_sem_wait("HNA");
            spect.current_zone = 'A';
            printf(GREEN "t=%ld : %s has got a seat in zone %c\n" RESET, time(NULL) - starting_time, spect.name, spect.current_zone);
        }

        // Spectating the match
        clock_gettime(CLOCK_REALTIME, &timespec_struct2);
        timespec_struct2.tv_sec = timespec_struct2.tv_sec + spectating_time;
        
        pthread_mutex_lock(&mutex_H_goal);
        int initial_time = time(NULL) - starting_time;
        int cond_ret_value;
        while (goals_H < spect.max_goal_limit && cond_ret_value != ETIMEDOUT)
        {
            cond_ret_value = pthread_cond_timedwait(&cond_H_goal, &mutex_H_goal, &timespec_struct2);
        }
        int final_time = time(NULL) - starting_time;
        pthread_mutex_unlock(&mutex_H_goal);
        if ((final_time - initial_time) >= spectating_time || cond_ret_value == ETIMEDOUT)
        {
            printf(CYAN "t=%ld : %s watched the match for %d seconds and is leaving\n" RESET, time(NULL) - starting_time, spect.name, spectating_time);
        }
        else
        {
            printf(MAGENTA "t=%ld : %s is leaving due to bad performance of his team\n" RESET, time(NULL) - starting_time, spect.name);
        }
        sem_post(&A);
        sem_post(&sem_H_N_A);
    }

exit_ceremony:
    printf(RED "t=%ld : %s is waiting for their friends at the exit\n" RESET, time(NULL) - starting_time, spect.name);
    pthread_mutex_lock(&groups[spect.group_id].mutex_group);
    groups[spect.group_id].at_exit++;
    pthread_mutex_unlock(&groups[spect.group_id].mutex_group);
    pthread_cond_signal(&groups[spect.group_id].cond_group);
}

void *start_goals(void *args)
{
    goal *goal_struct = (goal *)args;
    // iterating over all the goals
    for (int i = 0; i < num_goals; i++)
    {
        if (goal_struct[i].time_since_start - current_time - time(NULL) + starting_time > 0)
        {
            sleep(goal_struct[i].time_since_start - current_time - time(NULL) + starting_time);
        }

        float prob = ((float)(rand()%10000)) / (float)10000;
        if (prob <= goal_struct[i].prob)
        {
            if (goal_struct[i].type == 'A')
            {
                pthread_mutex_lock(&mutex_A_goal);
                goals_A++;
                printf("t=%ld : Team %c has scored their %dth goal\n", time(NULL) - starting_time, goal_struct[i].type, goals_A);
                pthread_cond_broadcast(&cond_A_goal);
                pthread_mutex_unlock(&mutex_A_goal);
            }
            else
            {
                pthread_mutex_lock(&mutex_H_goal);
                goals_H++;
                printf("t=%ld : Team %c has scored their %dth goal\n", time(NULL) - starting_time, goal_struct[i].type, goals_H);
                pthread_cond_broadcast(&cond_H_goal);
                pthread_mutex_unlock(&mutex_H_goal);
            }
        }
        else
        {
            if(goal_struct[i].type == 'A')
            {
                printf("t=%ld : Team %c missed the chance to score their %dth goal\n", time(NULL) - starting_time, goal_struct[i].type, goals_A+1);
            }
            else
            {
                printf("t=%ld : Team %c missed the chance to score their %dth goal\n", time(NULL) - starting_time, goal_struct[i].type, goals_H+1);
            }
        }
    }
}
void* start_group(void* args)
{
    int id = *(int*) args;
    // Wait for all people of group to leave
    pthread_mutex_lock(&groups[id].mutex_group);
    while (groups[id].at_exit < groups[id].group_size)
    {
        pthread_cond_wait(&groups[id].cond_group, &groups[id].mutex_group);
    }
    pthread_mutex_unlock(&groups[id].mutex_group);

    printf(RED "t=%ld : Group %d is leaving for dinner\n" RESET, time(NULL) - starting_time, (id + 1));
}

int main()
{
    srand(time(NULL));
    scanf("%d %d %d %d %d", &zone_H.max_capacity, &zone_N.max_capacity, &zone_A.max_capacity, &spectating_time, &num_groups);

    if(pthread_mutex_init(&mutex_H, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_mutex_init(&mutex_N, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_mutex_init(&mutex_A, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_mutex_init(&mutex_H_goal, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_mutex_init(&mutex_A_goal, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_cond_init(&cond_H_goal, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(pthread_cond_init(&cond_A_goal, NULL) != 0)
    {
        perror("init failed");
        exit(-1);
    }

    groups = (group*)malloc(sizeof(group) * num_groups);
    for (int i = 0; i < num_groups; i++)
    {
        int size_of_group;
        scanf("%d", &size_of_group);
        groups[i].group_size = size_of_group;
        groups[i].group_id = i;
        groups[i].at_exit = 0;
        groups[i].spectators = (spectator *)malloc(sizeof(spectator) * size_of_group);
        if(pthread_mutex_init(&groups[i].mutex_group, NULL) != 0)
        {
            perror("init failed");
            exit(-1);
        }
        if(pthread_cond_init(&groups[i].cond_group, NULL) != 0)
        {
            perror("init failed");
            exit(-1);
        }
        for (int j = 0; j < groups[i].group_size; j++)
        {
            scanf("%s %c", groups[i].spectators[j].name, &groups[i].spectators[j].type);
            scanf("%d %d %d", &groups[i].spectators[j].arrival_time, &groups[i].spectators[j].patience_limit, &groups[i].spectators[j].max_goal_limit);
            groups[i].spectators[j].current_zone = 'A';
            groups[i].spectators[j].group_id = i;
        }
    }

    scanf("%d", &num_goals);
    goal goals[num_goals];
    goal *goals_topass = (goal *)malloc(sizeof(goal) * num_goals);

    // Initialising semaphores on basis of capacities
    if(sem_init(&sem_H_N, 0, zone_H.max_capacity + zone_N.max_capacity) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(sem_init(&sem_H_N_A, 0, zone_H.max_capacity + zone_N.max_capacity + zone_A.max_capacity) != 0)
    {
        perror("init failed");
        exit(-1);
    }
    if(sem_init(&A, 0, zone_A.max_capacity) != 0)
    {
        perror("init failed");
        exit(-1);
    }

    for (int i = 0; i < num_goals; i++)
    {
        scanf(" %c %d %f", &goals[i].type, &goals[i].time_since_start, &goals[i].prob);
        goals_topass[i] = goals[i];
    }

    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < groups[i].group_size; j++)
        {
            total_specs++;
        }
    }

    starting_time = time(NULL);
    pthread_t spectator_threads[total_specs];
    // Creating spectator threads
    for (int i = 0, spec_index = 0; i < num_groups; i++)
    {
        for (int j = 0; j < groups[i].group_size; j++, spec_index++)
        {
            pthread_create(&spectator_threads[spec_index], NULL, start_spectator, &groups[i].spectators[j]);
        }
    }
    // Creating group threads
    pthread_t group_threads[num_groups];
    for(int i = 0; i < num_groups; i++)
    {
        pthread_create(&group_threads[i], NULL, start_group, &groups[i].group_id);
    }

    // creating goal threads
    pthread_t goal_threads;
    pthread_create(&goal_threads, NULL, start_goals, (void *)goals_topass);
    
    // Joining threads
    for (int i = 0; i < total_specs; i++)
    {
        pthread_join(spectator_threads[i], NULL);
    }
    pthread_join(goal_threads, NULL);

    // freeing memory
    free(goals_topass);
    for (int i = 0; i < num_groups; i++)
    {
        free(groups[i].spectators);
    }
    return 0;
}