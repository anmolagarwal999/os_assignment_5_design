
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/group.h ####################//

#include "main.h"

#ifndef __GROUP_H
#define __GROUP_H

void initGroup(int i);

void createPersonThreads(int i);

#endif // __GROUP_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/main.c ####################//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "group.h"
#include "team.h"

void init(){
	srand(time(0));

	per_iter = 0;

	zones = (Zone **) malloc(sizeof(Zone *) * 3);

	for(int i = 0; i < 3; i++){
		Zone * z = (Zone *) malloc(sizeof(Zone));

		z->uid = i;

		if(i==0){
			z->name = 'H';
			z->capacity = c_H;
		}
		else if(i==1){
			z->name = 'A';
			z->capacity = c_A;
		}
		else if(i==2){
			z->name = 'N';
			z->capacity = c_N;
		}

		zones[i] = z;
	}

	groups = (Group **) malloc(sizeof(Group *) * G);

	for(int i = 0; i < G; i++){
		initGroup(i);
	}

	scanf("%d", &C);

	teams = (Team **) malloc(sizeof(Team *) * 2);
	for(int i = 0; i < 3; i++){
		Team * t = (Team *) malloc(sizeof(Team));
		t->uid = i;

		if(i==0){
			t->name = 'H';
		}
		else if(i==1){
			t->name = 'A';
		}

		t->iter = 0;
		t->goals = 0;

		teams[i] = t;
	}

	for(int i = 0; i < C; i++){
		initTeam();
	}

	team_t = (pthread_t *) malloc(sizeof(pthread_t) * 2);

	for(int i = 0; i < 2; i++){
		createTeamThreads(i);
	}

	person_t = (pthread_t *) malloc(sizeof(pthread_t) * tot_persons);

	for(int i = 0; i < G; i++){
		createPersonThreads(i);
	}
}

int main(){

	tot_persons = 0;

	scanf("%d %d %d %d %d", &c_H, &c_A, &c_N, &X, &G);
	init();

	for(int i = 0; i < tot_persons; i++)
        pthread_join(person_t[i], 0);

    for(int i = 0; i < 3; i++)
        pthread_join(team_t[i], 0);

    return 0;

}

//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/team.h ####################//

#include "main.h"

#ifndef __TEAM_H
#define __TEAM_H

void initTeam();

void createTeamThreads(int i);

#endif // __TEAM_H
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/group.c ####################//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include "main.h"
#include "group.h"

void * personRunner(void * a) {
    Person * p = (Person *)a;

    sleep(p->arr_time);
    // SPEC EVENT 1
    printf(ANSI_COLOR_RED "t=%d: %s has reached the stadium\n" ANSI_COLOR_RESET, p->arr_time, p->name);

    int wait_time = 0;
    int spectate_time = 0;

    if(p->fan=='H'){
        // wait till zones are empty
        while(zones[0]->capacity == 0 && zones[2]->capacity == 0 && wait_time<p->patience){
            wait_time++;
            sleep(1);
        }
        wait_time++;
        sleep(1);
        if(wait_time > p->patience){
            // SPEC EVENT 3
            printf(ANSI_COLOR_YELLOW "t=%d: %s could not get a seat\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            sleep(1);
            p->atExit = 1;
            // SPEC EVENT 6
            printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+1, p->name);
        }
        else{
            // SPEC EVENT 2
            if(zones[0]->capacity >0){
                zones[0]->capacity--;
                p->curr_zone = 0;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone H\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }
            else if(zones[2]->capacity >0){
                zones[2]->capacity--;
                p->curr_zone = 2;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone N\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }

            while(teams[1]->goals < p->num_goals && spectate_time < X){
                spectate_time++;
                sleep(1);
            }
            // SPEC EVENT 5
            if(teams[1]->goals >= p->num_goals){
                zones[p->curr_zone]++;
                printf(ANSI_COLOR_MAGENTA "t=%d: %s is leaving due to bad performance of his team\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time, p->name);
                sleep(1);
                p->atExit = 1;
                // SPEC EVENT 6
                printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time+1, p->name);
            }
            // SPEC EVENT 4
            else if(spectate_time>=X){
                zones[p->curr_zone]++;
                printf(ANSI_COLOR_YELLOW "t=%d: %s watched the match for %d seconds and is leaving\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time, p->name, X);
                sleep(1);
                p->atExit = 1;
                // SPEC EVENT 6
                printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time+1, p->name);
            }

        }
    }
    else if(p->fan=='A'){
        // wait till zones are empty
        while(zones[1]->capacity == 0 && wait_time<p->patience){
            wait_time++;
            sleep(1);
        }
        wait_time++;
        sleep(1);
        if(wait_time > p->patience){
            // SPEC EVENT 3
            printf(ANSI_COLOR_YELLOW "t=%d: %s could not get a seat\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            sleep(1);
            // SPEC EVENT 6
            printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+1, p->name);
        }
        else{
            // SPEC EVENT 2
            if(zones[1]->capacity >0){
                zones[1]->capacity--;
                p->curr_zone = 1;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }

            while(teams[0]->goals < p->num_goals && spectate_time < X){
                spectate_time++;
                sleep(1);
            }
            // SPEC EVENT 5
            if(teams[0]->goals >= p->num_goals){
                zones[p->curr_zone]++;
                printf(ANSI_COLOR_MAGENTA "t=%d: %s is leaving due to bad performance of his team\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time, p->name);
                sleep(1);
                p->atExit = 1;
                // SPEC EVENT 6
                printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time+1, p->name);
            }
            // SPEC EVENT 4
            else if(spectate_time>=X){
                zones[p->curr_zone]++;
                printf(ANSI_COLOR_CYAN "t=%d: %s watched the match for %d seconds and is leaving\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time, p->name, X);
                sleep(1);
                p->atExit = 1;
                // SPEC EVENT 6
                printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time+1, p->name);
            }
        }
    }
    if(p->fan=='N'){
        // wait till zones are empty
        while(zones[0]->capacity == 0 && zones[1]->capacity == 0 && zones[2]->capacity == 0 && wait_time<p->patience){
            wait_time++;
            sleep(1);
        }
        wait_time++;
        sleep(1);
        if(wait_time > p->patience){
            // SPEC EVENT 3
            printf(ANSI_COLOR_YELLOW "t=%d: %s could not get a seat\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            sleep(1);
            p->atExit = 1;
            // SPEC EVENT 6
            printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+1, p->name);
        }
        else{
            // SPEC EVENT 2
            if(zones[0]->capacity >0){
                zones[0]->capacity--;
                p->curr_zone = 0;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone H\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }
            else if(zones[1]->capacity >0){
                zones[1]->capacity--;
                p->curr_zone = 1;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }
            else if(zones[2]->capacity >0){
                zones[2]->capacity--;
                p->curr_zone = 2;
                printf(ANSI_COLOR_YELLOW "t=%d: %s has got a seat in zone N\n" ANSI_COLOR_RESET, p->arr_time+wait_time, p->name);
            }

            while(spectate_time < X){
                spectate_time++;
                sleep(1);
            }
            // SPEC EVENT 4
            if(spectate_time>=X){
                zones[p->curr_zone]++;
                printf(ANSI_COLOR_CYAN "t=%d: %s watched the match for %d seconds and is leaving\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time, p->name, X);
                sleep(1);
                p->atExit = 1;
                // SPEC EVENT 6
                printf(ANSI_COLOR_BLUE "t=%d: %s is waiting for their friends at the exit\n" ANSI_COLOR_RESET, p->arr_time+wait_time+spectate_time+1, p->name);
            }
        }
    }

    if(p->atExit){
        bool waitForFriends = false;

        for(int i = 0; i < groups[p->groupID]->k; i++){
            if(!groups[p->groupID]->persons[i]->atExit){
                waitForFriends = true;
                break;
            }
        }

        if(!waitForFriends && groups[p->groupID]->left!=1){
            // BONUS GROUP EVENT
            groups[p->groupID]->left = 1;
            printf("t=%d: Group %d is leaving for dinner\n", p->arr_time+wait_time+spectate_time+1, p->groupID);
        }
    }

    return 0;
}

void initGroup(int i) {
    Group * g = (Group *) malloc(sizeof(Group));

    g->uid = i;
    g->left = 0;

    scanf("%d", &g->k);

    tot_persons += g->k;

    g->persons = (Person **) malloc(sizeof(Person *) * g->k);
    for(int j = 0; j < g->k; j++){
        g->persons[j] = (Person *) malloc(sizeof(Person));
        scanf("%s %c %d %d %d", g->persons[j]->name, &g->persons[j]->fan, &g->persons[j]->arr_time, &g->persons[j]->patience, &g->persons[j]->num_goals);
        g->persons[j]->atExit = 0;
        g->persons[j]->groupID = i;
        g->persons[j]->curr_zone = -1;
    }

    groups[i] = g;
}

void createPersonThreads(int i) {
    for(int j = 0; j<groups[i]->k; j++){
        pthread_create(&person_t[per_iter], 0, personRunner, groups[i]->persons[j]);
        per_iter++;
    }
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/team.c ####################//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include "main.h"
#include "team.h"

void * teamRunner(void * a) {
    Team * t = (Team *)a;

    int wait_time = 0;

    for(int i = 0; i < t->iter; i++){
    	sleep(t->goal_time[i] - wait_time);

        wait_time += t->goal_time[i];

    	bool scores = (rand() % 100) < (t->chance[i] * 100);

    	if(scores){
    		t->goals++;
    		// TEAM EVENT 1
    		if(t->goals==1){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c have scored their %dst goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals);
    		}
    		else if(t->goals==2){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c have scored their %dnd goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals);
    		}
    		else if(t->goals==3){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c have scored their %drd goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals);
    		}
    		else if(t->goals>3){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c have scored their %dth goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals);
    		}
    	}
    	else{
    		// TEAM EVENT 2
    		if(t->goals+1==1){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c missed the chance to score their %dst goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals+1);
    		}
    		else if(t->goals+1==2){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c missed the chance to score their %dnd goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals+1);
    		}
    		else if(t->goals+1==3){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c missed the chance to score their %drd goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals+1);
    		}
    		else if(t->goals+1>3){
    			printf(ANSI_COLOR_GREEN "t=%d: Team %c missed the chance to score their %dth goal\n" ANSI_COLOR_RESET, t->goal_time[i], t->name, t->goals+1);
    		}
    	}
    }
    

    return 0;
}

void initTeam() {

	char t_name;
	int t_time;
	double t_prob;

    scanf(" %c %d %lf", &t_name, &t_time, &t_prob);

    if(t_name=='H'){
    	teams[0]->goal_time[teams[0]->iter] = t_time;
    	teams[0]->chance[teams[0]->iter] = t_prob;
    	teams[0]->iter++;
    }
    else if(t_name=='A'){
    	teams[1]->goal_time[teams[1]->iter] = t_time;
    	teams[1]->chance[teams[1]->iter] = t_prob;
    	teams[1]->iter++;
    }
}

void createTeamThreads(int i) {
    pthread_create(&team_t[i], 0, teamRunner, teams[i]);
}
//###########FILE CHANGE ./main_folder/PRAJNEYA KUMAR_305939_assignsubmission_file_/2019114011/q2/main.h ####################//

#include <pthread.h>

#ifndef __MAIN_H
#define __MAIN_H

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct Zone{
    int uid;

    char name;
    int capacity;
} Zone;

typedef struct Person {
    int uid;

    char name[50];
    char fan;
    int arr_time;
    int patience;
    int num_goals;

    int atExit;
    int groupID;
    int curr_zone;

} Person;

typedef struct Group {
    int uid;

    int k;
    Person ** persons;

    int left;
} Group;

typedef struct Team {
    int uid;

    int iter;
    int goals;

    char name;
    int goal_time[100];
    double chance[100];
} Team;

Group ** groups;
Team ** teams;
Zone ** zones;

pthread_t * person_t,
          * team_t;

int G,           // number of Groups
    X,           // spectate time
    c_H,         // capacity of Zone H
    c_A,         // capacity of Zone A
    c_N,         // capacity of Zone N
    C,           // Number of chances
    tot_persons, // Total number of persons
    per_iter;    // Iterator for persons thread

#endif // __MAIN_H