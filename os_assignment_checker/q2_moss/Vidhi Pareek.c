
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/people.h ####################//

#ifndef PEOPLE_H
#define PEOPLE_H
typedef struct people 
{
    char name[100];
    char team;
    int arrive;
    int left;
    int entry_time;
    int seated;
    int cur_seat;
    int patience_time;
    int goals; 
    pthread_mutex_t people_mutex;
    pthread_cond_t people_cond;
}people;
people all_people[500];
int total_people , count_people;
void* people_init(void*input);
#endif

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/people.c ####################//

#include "headers.h"
void *people_init(void *input)
{
    people *p = (people *)input;
    sleep(p->entry_time);
    printf(ANSI_COLOR_RED "t=%ld : %s has reached the stadium" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name);
    struct timespec timeToWait;
    clock_gettime(CLOCK_REALTIME, &timeToWait);
    timeToWait.tv_sec += p->patience_time;
    pthread_mutex_lock(&p->people_mutex);
    p->arrive=1;
    p->seated=0;
    int rt = pthread_cond_timedwait(&p->people_cond, &p->people_mutex, &timeToWait);
    pthread_mutex_unlock(&p->people_mutex);
    //printf("%s --- %d ---- %d\n",p->name , rt , p->seated);
    if (rt != ETIMEDOUT)
    {
        char zone;
        if (p->cur_seat == H)
        {
            zone = 'H';
        }
        else if (p->cur_seat == A)
        {
            zone = 'A';
        }
        else if (p->cur_seat == N)
        {
            zone = 'N';
        }
        
        printf(ANSI_COLOR_GREEN "t=%ld : %s has got a seat in zone %c" ANSI_COLOR_RESET "\n", time(NULL)-start , p->name, zone);
        clock_gettime(CLOCK_REALTIME, &timeToWait);
        timeToWait.tv_sec += spec_time;
        pthread_mutex_lock(&p->people_mutex);
        int rt2 = pthread_cond_timedwait(&p->people_cond, &p->people_mutex, &timeToWait);
        pthread_mutex_unlock(&p->people_mutex);
        if (rt2 == 0)
        {
            printf(ANSI_COLOR_MAGENTA "t=%ld : %s is leaving due to bad defensive performance of his team" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name);
            pthread_mutex_lock(&total_people_mutex);
            count_people--;
            p->left = 1;
            pthread_mutex_unlock(&total_people_mutex);
            pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
            all_zones[p->cur_seat].capacity++;
            pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);
            printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
            return NULL;
        }
        else if (rt2 == ETIMEDOUT)
        {
            printf(ANSI_COLOR_BLUE "t=%ld : %s watched the match for %d seconds and is leaving" ANSI_COLOR_RESET "\n",time(NULL)-start, p->name, spec_time);
            pthread_mutex_lock(&total_people_mutex);
            count_people--;
            p->left = 1;
            pthread_mutex_unlock(&total_people_mutex);
            
            pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
            //printf("comes 2\n");
            all_zones[p->cur_seat].capacity++;
            pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);
            printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
            return NULL;
        }
        else
        {
            perror("Error");
            return NULL;
        }
    }
    else if (rt == ETIMEDOUT)
    {
        printf(ANSI_COLOR_YELLOW "t=%ld : %s couldn't get a seat" ANSI_COLOR_RESET "\n", time(NULL)-start , p->name);
        pthread_mutex_lock(&total_people_mutex);
        count_people--;
        p->left = 1;
        pthread_mutex_unlock(&total_people_mutex);
        /*
        pthread_mutex_lock(&all_zones[p->cur_seat].zone_mutex);
        printf("comes 2\n");
        all_zones[p->cur_seat].capacity++;
        pthread_mutex_unlock(&all_zones[p->cur_seat].zone_mutex);*/
        printf(ANSI_COLOR_CYAN "t=%ld : %s is leaving for dinner\n" ANSI_COLOR_RESET ,time(NULL)-start , p->name);
        return NULL;
    }
    else
    {
        perror("Error");
        return NULL;
    }
    
    return NULL;
}

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/main.c ####################//

#include "headers.h"
void checkinput()
{
    printf("%d %d %d\n", cap_a, cap_h, cap_n);
    for (int i = 0; i < 3; i++)
    {
        printf("%c %d\n", all_zones[i].zone, all_zones[i].capacity);
    }
    for (int i = 0; i < total_people; i++)
    {
        printf("%s %c %d %d %d %d %d %d %d\n", &all_people[i].name[0], all_people[i].team, all_people[i].arrive, all_people[i].left, all_people[i].entry_time, all_people[i].seated, all_people[i].cur_seat, all_people[i].patience_time, all_people[i].goals);
    }
    for (int i = 0; i < total_goals; i++)
    {
        printf("%c %d %f\n", all_goals[i].team, all_goals[i].time, all_goals[i].prob);
    }
    // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
}
int main(void)
{
    scanf("%d %d %d %d", &cap_h, &cap_a, &cap_n, &spec_time);
    for (int i = 0; i < 3; i++)
    {
        if (i == 0)
        {
            all_zones[i].capacity = cap_h;
            all_zones[i].zone = 'H';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
        else if (i == 1)
        {
            all_zones[i].capacity = cap_a;
            all_zones[i].zone = 'A';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
        else if (i == 2)
        {
            all_zones[i].capacity = cap_n;
            all_zones[i].zone = 'N';
            pthread_mutex_init(&all_zones[i].zone_mutex, NULL);
        }
    }
    int num_grps, num_ppl, i = 0;
    scanf("%d", &num_grps);
    while (num_grps--)
    {
        int num_ppl;
        scanf("%d", &num_ppl);
        total_people += num_ppl;
        while (num_ppl--)
        {
            scanf("%s %c %d %d %d", &all_people[i].name[0], &all_people[i].team, &all_people[i].entry_time, &all_people[i].patience_time, &all_people[i].goals);
            all_people[i].arrive = 0;
            all_people[i].left = 0;
            all_people[i].seated = 0;
            all_people[i].cur_seat = -1;
            pthread_mutex_init(&all_people[i].people_mutex, NULL);
            pthread_cond_init(&all_people[i].people_cond, NULL);
            // if (i == 2)
            //     printf("%s %c %d %d %d %d %d %d %d\n", &all_people[i].name[0], all_people[i].team, all_people[i].arrive, all_people[i].left, all_people[i].entry_time, all_people[i].seated, all_people[i].cur_seat, all_people[i].patience_time, all_people[i].goals);
            i++;
        }
        // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
    }
    count_people = total_people;
    scanf("%d", &total_goals);
    int goals = total_goals;
    i = 0;
    while (goals--)
    {
        scanf(" %c %d %f", &all_goals[i].team, &all_goals[i].time, &all_goals[i].prob);
        i++;
    }
    // printf("%s %c %d %d %d %d %d %d %d\n", &all_people[2].name[0], all_people[2].team, all_people[2].arrive, all_people[2].left, all_people[2].entry_time, all_people[2].seated, all_people[2].cur_seat, all_people[2].patience_time, all_people[2].goals);
    // checkinput();
    // return 0;
    pthread_mutex_init(&goal_mutex, NULL);
    pthread_cond_init(&goal_cond, NULL);
    pthread_t all_people_threads[total_people];
    start = time(NULL);
    for (int i = 0; i < total_people; i++)
    {
        pthread_create(&all_people_threads[i], NULL, people_init, &all_people[i]);
    }
    pthread_t all_zones_threads[3];
    for (int i = 0; i < 3; i++)
    {
        pthread_create(&all_zones_threads[i], NULL, zone_init, &all_zones[i]);
    }
    pthread_t goal;
    pthread_create(&goal, NULL, goal_init, all_goals);
    pthread_t goal_update;
    pthread_create(&goal_update, NULL, update_goal_info, NULL);
    pthread_join(goal, NULL);
    pthread_join(goal_update, NULL);
    for (int i = 0; i < total_people; i++)
    {
        pthread_join(all_people_threads[i], NULL);
    }
    for (int i = 0; i < 3; i++)
    {
        pthread_join(all_zones_threads[i], NULL);
    }
    return 0;
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/team.h ####################//

#ifndef TEAM_H
#define TEAM_H
typedef struct team 
{
    char zone;
    int capacity;  
}zone;
zone * all_zones;
void* zone_init(void*input);
#endif

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/try.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
pthread_mutex_t fakeMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t fakeCond = PTHREAD_COND_INITIALIZER;

void mywait(int timeInMs)
{
    struct timespec timeToWait;
    struct timeval now;
    int rt;

    gettimeofday(&now,NULL);


    timeToWait.tv_sec = now.tv_sec+5;
    timeToWait.tv_nsec = (now.tv_usec+1000UL*timeInMs)*1000UL;

    pthread_mutex_lock(&fakeMutex);
    rt = pthread_cond_timedwait(&fakeCond, &fakeMutex, &timeToWait);
    pthread_mutex_unlock(&fakeMutex);
    printf("\nDone\n");
}

void* fun(void* arg)
{
    printf("\nIn thread\n");
    mywait(1000);
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/main2.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
pthread_cond_t c = PTHREAD_COND_INITIALIZER;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
time_t start;
int goals[2];
int goals_num;
typedef struct person
{
    char name[100];
    char fan;
    int reach_time;
    int patience_time;
    int num_goals;
} person;
typedef struct goal
{
    char team;
    int time;
    float prob;
} teams;
void *people_alot(void *input)
{
    person *p = (person *)(input);
    sleep(p->reach_time - (time(NULL) - start));
    pthread_mutex_lock(&m);
    printf(ANSI_COLOR_RED "%s %d REACHED" ANSI_COLOR_RESET "\n", p->name, time(NULL) - start);
    pthread_mutex_unlock(&m);
}
void * goal_count(void * input)
{
    teams* t = (teams*)(input);
    for(int i=0 ; i<goals_num ; i++)
    {
        t->time
    }
}
int main()
{
    int cap_a, cap_h, cap_n, spec_time;
    scanf("%d %d %d %d", &cap_h, &cap_a, &cap_n, &spec_time);
    int num_grps, num_ppl;
    person *prsn = NULL;
    teams *team = NULL;
    scanf("%d", &num_grps);
    int ppl = 0, i = 0;
    while (num_grps--)
    {
        int num_ppl;
        scanf("%d", &num_ppl);
        ppl += num_ppl;
        if (prsn == NULL)
        {
            prsn = (person *)(calloc(ppl, sizeof(person)));
        }
        else
        {
            prsn = (person *)(realloc(prsn, ppl));
        }
        while (num_ppl--)
        {
            scanf("%s %c %d %d %d", &prsn[i].name, &prsn[i].fan, &prsn[i].reach_time, &prsn[i].patience_time, &prsn[i].num_goals);
            i++;
        }
    }
    int num_goals;
    i = 0;
    scanf("%d", &num_goals);
    goals_num = num_goals;
    team = (teams *)(calloc(num_goals, sizeof(teams)));
    // printf("%d\n",num_goals);
    while (num_goals--)
    {
        scanf(" %c %d %f", &team[i].team, &team[i].time, &team[i].prob);
        // printf("%d\n",num_goals);
        i++;
    }
    start = time(NULL);
    pthread_t people_threads[ppl];
    for (int i = 0; i < ppl; i++)
        pthread_create(&people_threads[i], NULL, people_alot, prsn + i);
    pthread_t goal;
    pthread_create(&goal, NULL, goal_count, team);    
    for (int i = 0; i < ppl; i++)
        pthread_join(people_threads[i], NULL);
    pthread_join(goal, NULL);    
    return 0;
}

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/goal.h ####################//

#ifndef GOAL_H
#define GOAL_H
typedef struct goal
{
    char team;
    int time;  
    float prob;
}goal;
goal all_goals[50];
int goal_h , goal_a;
int a_goal , h_goal;
int missed_h , missed_a;
pthread_mutex_t goal_mutex;
pthread_cond_t goal_cond;
int total_goals;
void* goal_init(void*input);
void* update_goal_info(void *input);
#endif

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/goal.c ####################//

#include "headers.h"
void *goal_init(void *input)
{
    int prev =0;
    goal *g = (goal *)(input);
    for (int i = 0; i < total_goals; i++)
    {
        g=&all_goals[i];
        struct timespec wait;
        clock_gettime(CLOCK_REALTIME, &wait);
        wait.tv_sec += g->time - prev ;
        prev = g->time;
        pthread_mutex_lock(&goal_mutex);
        int rt_val = pthread_cond_timedwait(&goal_cond, &goal_mutex, &wait);
        pthread_mutex_unlock(&goal_mutex);
        float random_value = ((float)rand() * 1.0) / RAND_MAX;
        if (rt_val == ETIMEDOUT)
        {
            //printf("\n\n--%c--\n\n",g->team);
            if (g->team == 'H')
            {
                h_goal++;
                if (random_value <= g->prob)
                {
                    goal_h++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team H scored their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start , h_goal);
                }
                else
                {
                    missed_h++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team H missed the chance to score their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start , h_goal);
                }
            }
            else
            {
                a_goal++;
                if (random_value <= g->prob)
                {
                    goal_a++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team A scored their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start ,a_goal);
                }
                else
                {
                    missed_a++;
                    printf(ANSI_COLOR_YELLOW "t=%ld : Team A missed the chance to score their %dth goal" ANSI_COLOR_RESET "\n", time(NULL)-start ,a_goal);
                }
            }
        }
    }
    return NULL;
}

void *update_goal_info(void *input)
{
    while (count_people)
    {
        for (int i = 0; i < total_people; i++)
        {
            if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 1)
            {
                if (all_people[i].team == 'H' && goal_a == all_people[i].goals)
                {
                    pthread_mutex_lock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
                if (all_people[i].team == 'A' && goal_h == all_people[i].goals)
                {
                    pthread_mutex_lock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
        }
    }
    return NULL;
}
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/headers.h ####################//

#ifndef HEADERS_H
#define HEADERS_H
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <semaphore.h>
#include "people.h"
#include "zone.h"
#include "goal.h"
#define H 0
#define A 1
#define N 2
int cap_a , cap_h , cap_n , spec_time;
pthread_mutex_t total_people_mutex;
time_t start;
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
#endif
//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/zone.h ####################//

#ifndef ZONE_H
#define ZONE_H
typedef struct zone 
{
    char zone;
    int capacity;  
    pthread_mutex_t zone_mutex;
}zone;
zone all_zones[3];
void* zone_init(void*input);
#endif

//###########FILE CHANGE ./main_folder/Vidhi Pareek_305868_assignsubmission_file_/q2/zone.c ####################//

#include "headers.h"
void *zone_init(void *input)
{
    zone *z = (zone *)(input);
    //sleep(1);
    //  printf("%c\n",z->zone);
    while (count_people)
    {
        for (int i = 0; i < total_people; i++)
        {
            // printf("here1");

            // select by team

            if ((z - all_zones) == 1)
            {
                // printf("here2");
                // if (strcmp(all_people[i].name, "Ayush") == 0)
                //     printf("%d %d %d %c\n", all_people[i].arrive , all_people[i].seated , z->capacity , all_people[i].team);
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity != 0 && all_people[i].team != 'H')
                {
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 1;
                    z->capacity--;
                    //printf("t=%ld : %s arrived in zone %d\n", time(NULL) - start , all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                    
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
                
            }
            else if ((z - all_zones) == 0)
            {
                // printf("here3");
                // if (strcmp(all_people[i].name, "Rachit") == 0)
                //     printf("%d %d %d %c\n", all_people[i].arrive , all_people[i].seated , z->capacity , all_people[i].team);
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity > 0 && all_people[i].team != 'A')
                {
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    // if (strcmp(all_people[i].name, "Rachit") == 0)
                    //     printf("%s ", all_people[i].name);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 0;
                    z->capacity--;
                    //printf("%s arrived in zone %d\n", all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
            if ((z - all_zones) == 2)
            {
                // printf("here1");
                pthread_mutex_lock(&all_people[i].people_mutex);
                if (all_people[i].arrive == 1 && all_people[i].left == 0 && all_people[i].seated == 0 && z->capacity != 0)
                {
                    // printf("enters here\n");
                    //printf("%s %d %d %d %c\n", all_people[i].name, all_people[i].arrive, all_people[i].seated, z->capacity, all_people[i].team);
                    all_people[i].seated = 1;
                    all_people[i].cur_seat = 2;
                    z->capacity--;
                    //printf("%s arrived in zone %d\n", all_people[i].name, all_people[i].cur_seat);
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                    pthread_cond_signal(&all_people[i].people_cond);
                }
                else
                {
                    pthread_mutex_unlock(&all_people[i].people_mutex);
                }
            }
        }
    }
    return NULL;
}