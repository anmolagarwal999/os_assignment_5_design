
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/spectator.c ####################//

#include <unistd.h>
#include <stdio.h>
#include "spectator.h"
#include "globals.h"
#include "goal.h"

pthread_cond_t homeFans;
pthread_cond_t awayFans;

pthread_mutex_t searchingLock;
pthread_cond_t searchingCV;
int searching = 0;

void fans_init() {
    Pthread_cond_init(&homeFans, NULL);
    Pthread_cond_init(&awayFans, NULL);
}

void *spectator_process(void *input) {
    Spectator *spectator = (Spectator *) input;
    sleep(spectator->entryTime);
    printf(C_SPECTATOR "%s has reached the stadium\n" RESET, spectator->name);
    pthread_t seatAcquireThreads[3];
    if (spectator->type == HOME || spectator->type == NEUTRAL) {
        SeatGrab sg;
        sg.spectator = spectator;
        sg.seatType = HOME;
        Pthread_create(&seatAcquireThreads[HOME], NULL, seat_acquire, (void *) &sg);
    }
    if (spectator->type == AWAY || spectator->type == NEUTRAL) {
        SeatGrab sg;
        sg.spectator = spectator;
        sg.seatType = AWAY;
        Pthread_create(&seatAcquireThreads[AWAY], NULL, seat_acquire, (void *) &sg);
    }
    if (spectator->type == NEUTRAL) {
        SeatGrab sg;
        sg.spectator = spectator;
        sg.seatType = NEUTRAL;
        Pthread_create(&seatAcquireThreads[NEUTRAL], NULL, seat_acquire, (void *) &sg);
    }
    Pthread_mutex_init(&searchingLock, NULL);
    Pthread_cond_init(&searchingCV, NULL);

    Pthread_mutex_lock(&spectator->seatLock);

    Pthread_mutex_lock(&searchingLock);
    searching = 1;
    Pthread_cond_broadcast(&searchingCV);
    Pthread_mutex_unlock(&searchingLock);

    struct timespec ts_;
    clock_gettime(CLOCK_REALTIME, &ts_);
    ts_.tv_sec += spectator->patienceTime;
    pthread_cond_timedwait(&spectator->seatCV, &spectator->seatLock, &ts_);
    Pthread_mutex_unlock(&spectator->seatLock);
    if (spectator->seatType == -1) {
        printf(C_SPECTATOR2 "%s couldn't get a seat\n" RESET, spectator->name);
    }
    else {
        char c;
        switch(spectator->seatType) {
            case HOME:
                c = 'H';
                break;
            case AWAY:
                c = 'A';
                break;
            default:
                c = 'N';
        }
        printf(C_SPECTATOR "%s has got a seat in zone %c\n" RESET, spectator->name, c);
        if (spectator->type == NEUTRAL) {
            sleep(spectatingTime);
            printf(C_SPECTATOR2 "%s watched the match for %d seconds and is leaving\n" RESET, spectator->name, spectatingTime);
        } else {
            struct timespec ts;
            clock_gettime(CLOCK_REALTIME, &ts);
            ts.tv_sec += spectatingTime;
            int decidedToLeave = 0;
            while (!decidedToLeave) {
                Pthread_mutex_lock(&Scoreboard.scoreboardLock);
                if (Scoreboard.score[spectator->type ^ 1] >= spectator->goalLimit) {
                    printf(C_SPECTATOR2 "%s is leaving due to the bad defensive performance of his team\n" RESET, spectator->name);
                    decidedToLeave = 1;
                    Pthread_mutex_unlock(&Scoreboard.scoreboardLock);
                    continue;
                }
                int rc = 0;
                if (spectator->type == HOME)
                    rc = pthread_cond_timedwait(&homeFans, &Scoreboard.scoreboardLock, &ts);
                else
                    rc = pthread_cond_timedwait(&awayFans, &Scoreboard.scoreboardLock, &ts);
                if (rc != 0) {
                    decidedToLeave = 1;
                    printf(C_SPECTATOR2 "%s watched the match for %d seconds and is leaving\n" RESET, spectator->name, spectatingTime);
                } else if (Scoreboard.score[spectator->type ^ 1] >= spectator->goalLimit) {
                    printf(C_SPECTATOR2 "%s is leaving due to the bad defensive performance of his team\n" RESET, spectator->name);
                    decidedToLeave = 1;
                }
                Pthread_mutex_unlock(&Scoreboard.scoreboardLock);
            }
        }
        sem_post(&zoneSeats[spectator->seatType]);
    }
    printf(C_SPECTATOR "%s is waiting for their friends at the exit\n" RESET, spectator->name);
    return NULL;
}

void *seat_acquire(void *input) {
    SeatGrab *sg = (SeatGrab *) input;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += sg->spectator->patienceTime;
    Pthread_mutex_lock(&searchingLock);
    while (!searching)
        Pthread_cond_wait(&searchingCV, &searchingLock);
    Pthread_mutex_unlock(&searchingLock);

    if (sem_timedwait(&zoneSeats[sg->seatType], &ts) != 0) {
        return NULL;
    }
    Pthread_mutex_lock(&sg->spectator->seatLock);
    if (sg->spectator->seatType != -1) {
        sem_post(&zoneSeats[sg->seatType]);
        Pthread_mutex_unlock(&sg->spectator->seatLock);
        return NULL;
    }
    sg->spectator->seatType = sg->seatType;
    Pthread_cond_signal(&sg->spectator->seatCV);
    Pthread_mutex_unlock(&sg->spectator->seatLock);
    return NULL;
}
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/wrapper.h ####################//

#ifndef PARALLELISM_WRAPPER_H
#define PARALLELISM_WRAPPER_H

#include <pthread.h>

int Pthread_create(pthread_t *nt, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);
int Pthread_join(pthread_t thread, void **retval);
int Pthread_cancel(pthread_t thread);
int Pthread_mutex_lock(pthread_mutex_t *p);
int Pthread_mutex_unlock(pthread_mutex_t *p);
int Pthread_mutex_init(pthread_mutex_t *p, const pthread_mutexattr_t *ptr);
int Pthread_cond_init(pthread_cond_t *p, const pthread_condattr_t *ptr);
int Pthread_cond_signal(pthread_cond_t *p);
int Pthread_cond_broadcast(pthread_cond_t *p);
int Pthread_cond_wait(pthread_cond_t *p, pthread_mutex_t *x);
int Pthread_cond_timedwait(pthread_cond_t *p, pthread_mutex_t *x, const struct timespec* t);

#endif //PARALLELISM_WRAPPER_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/globals.h ####################//

#ifndef EL_CLASSICO_EXPERIENCE_GLOBALS_H
#define EL_CLASSICO_EXPERIENCE_GLOBALS_H

#include "semaphore.h"

enum zone {HOME, AWAY, NEUTRAL};
extern sem_t *zoneSeats;

#define C_GROUP     "\x1b[35m"
#define C_SPECTATOR   "\x1b[32m"
#define C_GOAL  "\x1b[33m"
#define C_SPECTATOR2 "\x1b[36m"
#define RESET   "\x1b[0m"

#endif //EL_CLASSICO_EXPERIENCE_GLOBALS_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/group.h ####################//

#ifndef EL_CLASSICO_EXPERIENCE_GROUP_H
#define EL_CLASSICO_EXPERIENCE_GROUP_H

#include "spectator.h"

typedef struct group {
    int size;
    Spectator *members;
    int id;
} Group;

extern Group *all_groups;

void group_init(int n);
void group_fill(Spectator **members, int n);

void *group_process(void *input);

#endif //EL_CLASSICO_EXPERIENCE_GROUP_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/main.c ####################//

#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "globals.h"
#include "group.h"
#include "goal.h"
#include "wrapper.h"

int numGoals;
int spectatingTime;
sem_t *zoneSeats;

int main() {
    int capacity[3];
    scanf("%d %d %d", &capacity[HOME], &capacity[AWAY], &capacity[NEUTRAL]);
    scanf("%d", &spectatingTime);
    int numGroups;
    scanf("%d", &numGroups);
    group_init(numGroups);
    for (int i = 0; i < numGroups; i++) {
        Group *currGroup = &all_groups[i];
        scanf("%d", &currGroup->size);
        group_fill(&currGroup->members, currGroup->size);
        currGroup->id = i;
        for (int j = 0; j < currGroup->size; j++) {
            char zoneType;
            Spectator *currMember = &currGroup->members[j];
            scanf("%s %c %d %d %d", currMember->name, &zoneType, &currMember->entryTime, &currMember->patienceTime,
                  &currMember->goalLimit);
            int validInput = 0;
            while (!validInput) {
                switch (zoneType) {
                    case 'A':
                        currMember->type = AWAY;
                        validInput = 1;
                        break;
                    case 'H':
                        currMember->type = HOME;
                        validInput = 1;
                        break;
                    case 'N':
                        currMember->type = NEUTRAL;
                        validInput = 1;
                        break;
                    default:
                        fprintf(stderr, "Invalid zone type entered. Enter the valid zone type: ");
                }
                currMember->groupID = i;
            }
        }
    }
    scanf("%d", &numGoals);
    goal_init(numGoals);
    int prefixTime = 0;
    for (int i = 0; i < numGoals; i++) {
        Goal *currGoal = &all_goals[i];
        char teamType;
        scanf("%c", &teamType);
        scanf("%c %d %lf", &teamType, &currGoal->delta, &currGoal->chance);
        int validInput = 0;
        while (!validInput) {
            switch (teamType) {
                case 'A':
                    currGoal->team = AWAY;
                    validInput = 1;
                    break;
                case 'H':
                    currGoal->team = HOME;
                    validInput = 1;
                    break;
                default:
                    fprintf(stderr, "Invalid team entered. Enter valid team (H/A): ");
            }
        }
        if (i != 0) currGoal->delta -= prefixTime;
        prefixTime += currGoal->delta;
    }
    zoneSeats = (sem_t *) malloc(3 * sizeof(sem_t));
    assert(zoneSeats);
    for (int i = 0; i < 3; i++)
        sem_init(&zoneSeats[i], 0, capacity[i]);
    scoreboard_init();
    fans_init();

    //simulation start
    pthread_t goalThread;
    Pthread_create(&goalThread, NULL, goal_process, all_goals);

    pthread_t *groupThreads = NULL;
    groupThreads = (pthread_t *) malloc(numGroups * sizeof(pthread_t));
    assert(groupThreads);
    for (int i = 0; i < numGroups; i++)
        Pthread_create(&groupThreads[i], NULL, group_process, (Group *) &all_groups[i]);

    for (int i = 0; i < numGroups; i++)
        Pthread_join(groupThreads[i], NULL);
    Pthread_join(goalThread, NULL);
    return 0;
}
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/group.c ####################//

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "group.h"
#include "globals.h"

Group *all_groups = NULL;

void group_init(int n) {
    all_groups = (Group *) malloc(n * sizeof(Group));
    assert(all_groups);
}

void group_fill(Spectator **members, int n) {
    *members = (Spectator *) malloc(n * sizeof(Spectator));
    assert(*members);
    for (int i = 0; i < n; i++) {
        (*members)[i].name = (char *) malloc(MAX_NAME_LEN);
        assert((*members)[i].name);
        Pthread_mutex_init(&(*members)[i].seatLock, NULL);
        Pthread_cond_init(&(*members)[i].seatCV, NULL);
        (*members)[i].seatType = -1;
    }
}

void *group_process(void *input) {
    Group *group = (Group *) input;
    pthread_t *memberThreads = (pthread_t *) malloc(group->size * sizeof(pthread_t));
    assert(memberThreads);
    for (int i = 0; i < group->size; i++) {
        Pthread_create(&memberThreads[i], NULL, spectator_process, (void *) &group->members[i]);
    }
    for (int i = 0; i < group->size; i++) {
        Pthread_join(memberThreads[i], NULL);
    }
    printf(C_GROUP "Group %d is leaving for dinner\n" RESET, group->id);
    return NULL;
}
//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/wrapper.c ####################//

#include <assert.h>
#include "wrapper.h"

int Pthread_create(pthread_t *nt, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg) {
    int rc = pthread_create(nt, attr, start_routine, arg);
    assert(rc == 0);
    return 0;
}

int Pthread_join(pthread_t thread, void **retval) {
    int rc = pthread_join(thread, retval);
    assert(rc == 0);
    return 0;
}

int Pthread_cancel(pthread_t thread) {
    int rc = pthread_cancel(thread);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_lock(pthread_mutex_t *p) {
    int rc = pthread_mutex_lock(p);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_unlock(pthread_mutex_t *p) {
    int rc = pthread_mutex_unlock(p);
    assert(rc == 0);
    return 0;
}

int Pthread_mutex_init(pthread_mutex_t *p, const pthread_mutexattr_t *ptr) {
    int rc = pthread_mutex_init(p, ptr);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_init(pthread_cond_t *p, const pthread_condattr_t *ptr) {
    int rc = pthread_cond_init(p, ptr);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_signal(pthread_cond_t *p) {
    int rc = pthread_cond_signal(p);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_broadcast(pthread_cond_t *p) {
    int rc = pthread_cond_broadcast(p);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_wait(pthread_cond_t *p, pthread_mutex_t *x) {
    int rc = pthread_cond_wait(p, x);
    assert(rc == 0);
    return 0;
}

int Pthread_cond_timedwait(pthread_cond_t *p, pthread_mutex_t *x, const struct timespec* t) {
    int rc = pthread_cond_timedwait(p, x, t);
    assert(rc == 0);
    return 0;
}

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/spectator.h ####################//

#ifndef EL_CLASSICO_EXPERIENCE_SPECTATOR_H
#define EL_CLASSICO_EXPERIENCE_SPECTATOR_H

#include "wrapper.h"

#define MAX_NAME_LEN 256

typedef struct spectator {
    char *name;
    int type;
    int entryTime;
    int patienceTime;
    int goalLimit;
    int groupID;
    pthread_mutex_t seatLock;
    pthread_cond_t seatCV;
    int seatType;
} Spectator;

typedef struct seatGrab {
    Spectator *spectator;
    int seatType;
} SeatGrab;

extern int spectatingTime;

extern pthread_cond_t homeFans;
extern pthread_cond_t awayFans;

void fans_init();
void *spectator_process(void *input);
void *seat_acquire(void *input);

#endif //EL_CLASSICO_EXPERIENCE_SPECTATOR_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/goal.h ####################//

#ifndef EL_CLASSICO_EXPERIENCE_GOAL_H
#define EL_CLASSICO_EXPERIENCE_GOAL_H

typedef struct goal {
    int team;
    int delta;
    double chance;
} Goal;

struct scoreboard {
    int score[2];
    pthread_mutex_t scoreboardLock;
};

extern int numGoals;
extern Goal *all_goals;

extern struct scoreboard Scoreboard;

void goal_init(int n);
void scoreboard_init();
void *goal_process(void *input);

#endif //EL_CLASSICO_EXPERIENCE_GOAL_H

//###########FILE CHANGE ./main_folder/Pramod Budramane_305830_assignsubmission_file_/q2/goal.c ####################//

#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "goal.h"
#include "wrapper.h"
#include "globals.h"
#include "spectator.h"

Goal *all_goals = NULL;
struct scoreboard Scoreboard;

void goal_init(int n) {
    all_goals = (Goal *) malloc(n * sizeof(Goal));
    assert(all_goals);
}

void scoreboard_init() {
    Scoreboard.score[HOME] = 0;
    Scoreboard.score[AWAY] = 0;
    Pthread_mutex_init(&Scoreboard.scoreboardLock, NULL);
}

void *goal_process(void *input) {
    for (int i = 0; i < numGoals; i++) {
        Goal *currGoal = &all_goals[i];
        sleep(currGoal->delta);
        double prob = (double) rand() / RAND_MAX;
        if (prob <= currGoal->chance) {
            Pthread_mutex_lock(&Scoreboard.scoreboardLock);
            Scoreboard.score[currGoal->team]++;

            if (currGoal->team == HOME)
                Pthread_cond_broadcast(&awayFans);
            else
                Pthread_cond_broadcast(&homeFans);
            Pthread_mutex_unlock(&Scoreboard.scoreboardLock);
            printf(C_GOAL "Team %c have scored their %dth goal\n" RESET, (currGoal->team == HOME ? 'H': 'A'), Scoreboard.score[currGoal->team]);
        }
        else {
            printf(C_GOAL "Team %c missed the chance to score their %dth goal\n" RESET, (currGoal->team == HOME ? 'H': 'A'), Scoreboard.score[currGoal->team] + 1);
        }
    }
    return NULL;
}