
//###########FILE CHANGE ./main_folder/Anusha Nath Roy_305788_assignsubmission_file_/q2/server.cpp ####################//

// ------------- headers --------------------
#include  <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>

#include <semaphore.h>

#define SERVER_PORT 8001
using namespace std;

// ---------------- colors -------------------------
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


//----------------- utilities -------------------------
#define MAX_STR_IP 400
#define ll long long int
const ll buff_sz = 1048576;
#define MAX_CLIENTS 100

pthread_mutex_t print_terminal = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t q_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t q_hasEntries_mutex = PTHREAD_COND_INITIALIZER;
queue<int> Q;

// client request struct
struct dict_node{
    pthread_mutex_t c_lock;         // lock
    string client_command;          // the client command
    bool deleted;                   // deleted 1=>deleted 0=>not deleted
    bool has_value;                 // has value 1=>key-pair exists
};

typedef struct dict_node dict_node;
vector <dict_node> dict_nodes(106);


// a function to calculate the e-th power of a number n
ll power(ll n, ll e)
{
    ll answer = 1;
    for (ll i = 0; i < e; i++)
    {
        answer *= n;
    }
    return answer;
}

int stoiu(string stringv){
    
    int n;
    int parts = 0;
    for (int i = 0; i < stringv.length(); i++)
    {
        n = stringv[stringv.length() - 1 - i] - '0';
        parts += n * power(10, i);
    }
    return parts;
}


void send_string_on_socket(int fd, const string &s)
{
    printf("\n Message sent: %s\n", s.c_str());
    int sent_bytes = write(fd, s.c_str(), s.length());
    if (sent_bytes < 0)
    {
        printf(ANSI_COLOR_RED"server error: no bytes sent to client"ANSI_COLOR_RESET"\n");
    }
}


string handle_connection(int client_socket_fd)
{
        
    string b;
    b.resize(buff_sz);
    int read_bytes = read(client_socket_fd, &b[0], buff_sz - 1);
    if (read_bytes <= 0)
    {
        printf(ANSI_COLOR_RED"Cannot read!\n"ANSI_COLOR_RESET); 
        return NULL;
    }
    b[read_bytes] = '\0';
    b.resize(read_bytes);
    pthread_mutex_lock(&print_terminal);
    printf("\nServer has read: client socket: %d\n", client_socket_fd);
    printf("\nstring: %s\n\n", b.c_str());
    pthread_mutex_unlock(&print_terminal);
    return b;
  
}


void handle_client_requests(int client_socket_fd, int id){
    
    string command = handle_connection(client_socket_fd);
    string output = to_string(id) + ":";
    istringstream ss(command);
    string word,word1; 
    ss >> word1;
    // worker_thread_id = gettid();
    
    if(word1 == "insert"){
        int key;
        string value;
        ss >> key;
        ss >> value;

        pthread_mutex_lock(&dict_nodes[key].c_lock);
        if(!(dict_nodes[key].has_value)){
            dict_nodes[key].client_command = value;
            dict_nodes[key].has_value = true;
            output+= "Insertion successful";
        }
        else{
            output+= "Key already exists";

        }
        pthread_mutex_unlock(&dict_nodes[key].c_lock);
        send_string_on_socket(client_socket_fd,output);
    }

    else if(word1 == "delete"){
        int key;
        string value;
        ss >> key;

        pthread_mutex_lock(&dict_nodes[key].c_lock);
        if(dict_nodes[key].has_value){
            dict_nodes[key].has_value = false;
            output+= "Deletion successful";
        }
        else{
            output+= "No such key exists";
        }
        pthread_mutex_unlock(&dict_nodes[key].c_lock);
        send_string_on_socket(client_socket_fd,output);
    }

    else if(word1 == "update"){
        int key;
        string value;
        ss >> key;
        ss >> value;

        pthread_mutex_lock(&dict_nodes[key].c_lock);
        if(dict_nodes[key].has_value){
            dict_nodes[key].client_command = value;
            dict_nodes[key].has_value = true;
            output+= dict_nodes[key].client_command;
        }
        else{
            output+= "Key does not exist";
        }
        pthread_mutex_unlock(&dict_nodes[key].c_lock);
        send_string_on_socket(client_socket_fd,output);     
    }
    else if(word1 == "concat"){
        int key1, key2;
        ss >> key1;
        ss >> key2;

        pthread_mutex_lock(&dict_nodes[key1].c_lock);
        pthread_mutex_lock(&dict_nodes[key2].c_lock);
        if(dict_nodes[key1].has_value && dict_nodes[key2].has_value){
            string temp1 = dict_nodes[key1].client_command;
            string temp2 = dict_nodes[key2].client_command;
            dict_nodes[key1].client_command = temp1 + temp2;
            dict_nodes[key2].client_command = temp2 + temp1;
            output+= dict_nodes[key2].client_command;
        }
        else{
            output+= "Concat failed as at least one of the keys does not exist";
        }
        pthread_mutex_unlock(&dict_nodes[key2].c_lock);
        pthread_mutex_unlock(&dict_nodes[key1].c_lock);
        send_string_on_socket(client_socket_fd,output); 
    }
    else if (word1 == "fetch"){
        int key;
        ss >> key;

        pthread_mutex_lock(&dict_nodes[key].c_lock);
        if(dict_nodes[key].has_value){
            output+= dict_nodes[key].client_command;
        }
        else{
            output+= "Key does not exist";
        }
        pthread_mutex_unlock(&dict_nodes[key].c_lock);
        send_string_on_socket(client_socket_fd,output);     
    }
    // while (ss >> word) 
    // {
    //     // print the read word
    //     cout << word << "\n";
    // }
    
}

void *worker_thread_handler(void *(arg))
{
    int id = *(int *)arg;
    while(1){
        pthread_mutex_lock(&q_mutex);

        while( Q.empty() ){ // can use a if statement but to prevent any errors while
            pthread_cond_wait(&q_hasEntries_mutex, &q_mutex);
        }

        int client_socket_fd = Q.front();
        Q.pop();
        pthread_mutex_unlock(&q_mutex);
        handle_client_requests(client_socket_fd, id);

    }
    return NULL;
}

//main

int main(int argc, char *argv[]){

    if(argc != 2){
        printf(ANSI_COLOR_RED"Error: Invalid Argument! \nServer has stopped listening on PORT %d ......\n"ANSI_COLOR_RESET, SERVER_PORT);
        exit(-1);
    }

    string worker = argv[1];
    int worker_num = stoiu(worker);
    int temp_worker_number = worker_num;
    pthread_t worker_threads[worker_num];
    //printf("ola");
    int number_of_dictionaries = 105;
    while(number_of_dictionaries--){
        pthread_mutex_init(&dict_nodes[number_of_dictionaries].c_lock, NULL);
        dict_nodes[number_of_dictionaries].client_command = "";
        dict_nodes[number_of_dictionaries].deleted = 0;
        dict_nodes[number_of_dictionaries].has_value = 0;      
    }

    while(temp_worker_number--){
        int *passed_index = new int;
        *passed_index = temp_worker_number;
        pthread_create(&worker_threads[temp_worker_number], NULL, worker_thread_handler,(void *)passed_index);
    }


    //connecting to server
    struct sockaddr_in serv_addr_obj, client_addr_obj;
    int serv_addr_obj_size = sizeof(serv_addr_obj);
    bzero((char *)&serv_addr_obj,serv_addr_obj_size );
    socklen_t clilen = sizeof(client_addr_obj);

    int socket_fd_intial = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd_intial < 0){
        fprintf(stderr, ANSI_COLOR_RED "Welcome socket could not be created!"ANSI_COLOR_RESET);
        exit(-1);
    }

    serv_addr_obj.sin_family = AF_INET;
    serv_addr_obj.sin_addr.s_addr = INADDR_ANY;
    serv_addr_obj.sin_port = htons(SERVER_PORT);

    if (bind(socket_fd_intial, (struct sockaddr *)&serv_addr_obj, serv_addr_obj_size) < 0)
    {
        perror("Error on bind on welcome socket: ");
        exit(-1);
    }

    listen(socket_fd_intial, MAX_CLIENTS);
    printf(ANSI_COLOR_CYAN"Assignment5 by Anusha: \n Welcome to server! \n Server is listening on PORT %d ......\n"ANSI_COLOR_RESET, SERVER_PORT);

    while(1){
            
            printf(ANSI_COLOR_YELLOW"Waiting for a new client to request for a connection......\n"ANSI_COLOR_RESET);
            int client_socket_fd = accept(socket_fd_intial, (struct sockaddr *)&client_addr_obj, &clilen);
            if (client_socket_fd < 0)
            {
                perror(ANSI_COLOR_RED"server error: accept()"ANSI_COLOR_RESET);
                exit(-1);
            }

            printf(ANSI_COLOR_CYAN "New client connected from port number %d and IP %s with client_socket_fd: %d\n"ANSI_COLOR_RESET, ntohs(client_addr_obj.sin_port), inet_ntoa(client_addr_obj.sin_addr), client_socket_fd);
            
            //prevent busy wait
            pthread_mutex_lock(&q_mutex);
            Q.push(client_socket_fd);
            pthread_mutex_unlock(&q_mutex);
            pthread_cond_signal(&q_hasEntries_mutex);
        }

        close(socket_fd_intial);
        //pthread_cond_destroy(&q_hasEntries_mutex);
        printf(ANSI_COLOR_CYAN"Server has stopped listening on PORT %d ......\n"ANSI_COLOR_RESET, SERVER_PORT);
        return 0;
    }
//###########FILE CHANGE ./main_folder/Anusha Nath Roy_305788_assignsubmission_file_/q2/client.cpp ####################//

// ------------- headers --------------------

#include  <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <queue>
#include <vector>
#include <tuple>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>

#include <semaphore.h>

#define SERVER_PORT 8001
using namespace std;

// ---------------- colors -------------------------
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


//----------------- utilities -------------------------
#define MAX_STR_IP 400
#define ll long long int
const ll buff_sz = 1048576;

pthread_mutex_t print_terminal = PTHREAD_MUTEX_INITIALIZER;

// client request struct
struct c_request{

    pthread_mutex_t c_lock;         // lock
    string client_command;          // the client command
    string client_ip_command;       // the terminal p command
    pthread_t client_thread;        // the client thread
    int sno;                        // serial number
    int client_fd;                  // client file descriptor
    bool in_thread;                 // bool in thread
    int c_time;                     // required time

};
typedef struct c_request c_request;
vector <c_request> c_requests;

// a function to calculate the e-th power of a number n
ll power(ll n, ll e)
{
    ll answer = 1;
    for (ll i = 0; i < e; i++)
    {
        answer *= n;
    }
    return answer;
}

int stoiu(string stringv){
    
    int n;
    int parts = 0;
    for (int i = 0; i < stringv.length(); i++)
    {
        n = stringv[stringv.length() - 1 - i] - '0';
        parts += n * power(10, i);
    }
    return parts;
}

int send_socket(int fd, const string &s)
{
    //printf("send <%d> : <%s>\n", fd, s.c_str());
    int sent_bytes = write(fd, s.c_str(), s.length());
    return sent_bytes;
}

string read_socket(int fd)
{
   string b;
   b.resize(buff_sz);
   int read_bytes = read(fd,&b[0],buff_sz-1);

   if(read_bytes < 0){
       cerr << ANSI_COLOR_RED "cannot read!";
       return NULL;
   }

   b[read_bytes] = '\0';
   b.resize(read_bytes);
   return b;
}


int get_socket_fd()
{
    int socket_v = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_v < 0)
    {
        fprintf(stderr, ANSI_COLOR_RED "Socket could not be created!");
        exit(-1);
    }
    
    return socket_v;
}

void *client_thread_handler(void *(arg)){
    int index = *(int*) arg;

    // connection
    int temp_socket_fd = get_socket_fd();
    c_requests[index].client_fd = temp_socket_fd;

    struct sockaddr_in server_obj;
    int server_object_size = sizeof(server_obj);
    memset(&server_obj, 0, server_object_size); // Zero out structure
    server_obj.sin_family = AF_INET;
    server_obj.sin_port = htons(SERVER_PORT); // convert to big-endian order
    
    if (connect(temp_socket_fd, (struct sockaddr *)&server_obj, server_object_size) < 0)
    {
        fprintf(stderr, ANSI_COLOR_RED "Socket could not be connected!");
        exit(-1);
    }

    //sleeping required time
    sleep(c_requests[index].c_time);

    pthread_mutex_lock(&c_requests[index].c_lock);
    if(send_socket(c_requests[index].client_fd,c_requests[index].client_command)<0) {fprintf(stderr, ANSI_COLOR_RED "could not send message to server!"); return NULL;}
    pthread_mutex_unlock(&c_requests[index].c_lock);    

    pthread_mutex_lock(&c_requests[index].c_lock);
    string response = read_socket(c_requests[index].client_fd);
    pthread_mutex_unlock(&c_requests[index].c_lock);    

    pthread_mutex_lock(&print_terminal);
    printf("%d:", index);
    if(response.c_str()){
        printf("%s\n", response.c_str());
    }
    pthread_mutex_unlock(&print_terminal);
    return NULL;
}

int main(int argc, char *argv[])
{

    string input_lines_str;
    int input_lines = 0;
    getline(cin,input_lines_str);
    input_lines = stoiu(input_lines_str);
    int client_num = input_lines;
    int ip_i = 0;
    

    // take inputs
    while(input_lines--){

        string string_input;
        getline(cin, string_input);
        //cout<< "****** check1:" << string_input << endl;

        c_request temp_client_ip;

        temp_client_ip.client_ip_command = string_input;
        temp_client_ip.sno = ip_i;
        ip_i ++;
        
        int space_divider = string_input.find(' ');
        string temp_string_time = string_input.substr(0, space_divider);
        temp_client_ip.c_time = stoiu(temp_string_time);
        temp_client_ip.client_command = string_input.substr(space_divider + 1, string_input.length());

        //cout<< "****** check2:" << temp_client_ip.client_command << endl;
        pthread_mutex_init(&temp_client_ip.c_lock, NULL);
        // temp_client_ip.c_lock = PTHREAD_MUTEX_INITIALIZER;
        temp_client_ip.in_thread = 0;

        c_requests.push_back(temp_client_ip);
    }

    // create threads
    for(int i=0; i < client_num; i++){
        int *passed_index = new int;
        *passed_index = i;
        pthread_create(&c_requests[i].client_thread, NULL, client_thread_handler, (void *)passed_index);
    }

    for(int i=0; i< client_num; i++){
        pthread_join(c_requests[i].client_thread, NULL);
    }

    return 0;
}