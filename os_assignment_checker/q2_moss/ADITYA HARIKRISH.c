
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q2/utils.c ####################//

#include "utils.h"

void printOrdinalNumber(unsigned n) {
    if (n % 10 == 1 && n % 100 != 11)
        printf("st");
    else if (n % 10 == 2 && n % 100 != 12)
        printf("nd");
    else if (n % 10 == 3 && n % 100 != 13)
        printf("rd");
    else
        printf("th");
}
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q2/main.c ####################//

#include "utils.h"

unsigned num_people;
int64_t SpectatingTime;
Zone H, A, N;

int main() {
    scanf("%u %u %u", &(H.capacity), &(A.capacity), &(N.capacity));
    // scanf("%u", &num_people);
    scanf("%" PRId64, &SpectatingTime);

    return 0;
}
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q2/header.h ####################//

#ifndef HEADER_H
#define HEADER_H

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <grp.h>
#include <inttypes.h>
#include <limits.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#endif
//###########FILE CHANGE ./main_folder/ADITYA HARIKRISH_305818_assignsubmission_file_/q2/utils.h ####################//

#ifndef UTILS_H
#define UTILS_H

#include "header.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#define HOME 1
#define AWAY 2
#define NEUTRAL 3

typedef struct Zone {
    unsigned capacity;
    unsigned occupiedSeats;
} Zone;

typedef struct Person {
    uint64_t timeOfEntering, patienceValue;
    int type;  // HOME / AWAY / NEUTRAL
} Person;

extern unsigned num_people;
extern int64_t SpectatingTime;  // max time people will watch the match for

extern Zone H, A, N;

void printOrdinalNumber(unsigned n);  // Prints 'th' or 'nd' or 'st' depending on n. Eg: if n==1 then st because 1st

// Defining colours
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BRIGHT_GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define BRIGHT_BLUE "\x1b[94m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

// Some macros
#define PthreadCreate(newthread, attr, funcPtr, arg)          \
    if (pthread_create(newthread, attr, funcPtr, arg) != 0) { \
        perror("pthread_create() error");                     \
        exit(1);                                              \
    }

#define PthreadCondInit(cond, attr)            \
    if (pthread_cond_init(&cond, NULL) != 0) { \
        perror("pthread_cond_init() error");   \
        exit(1);                               \
    }

#define after_malloc_check(x)                  \
    if (x == NULL) {                           \
        perror("Failed to allocate memory\n"); \
        exit(EXIT_FAILURE);                    \
    }

#endif