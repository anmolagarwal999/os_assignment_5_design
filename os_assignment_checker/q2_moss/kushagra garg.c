
//###########FILE CHANGE ./main_folder/kushagra garg_305908_assignsubmission_file_/2019113020_assignment_5/q2/people.h ####################//

typedef struct people {
    char name[20];
    int id;
    int enter_time;
    char team_fan;
    int patience_val;    
    int goal_limit;
    int group_no;
    int group_cardinality;
    char current_zone;
} people;
//###########FILE CHANGE ./main_folder/kushagra garg_305908_assignsubmission_file_/2019113020_assignment_5/q2/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "people.h"
#include "goal_chance.h"

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

// variables
int max_capacity_H;
int max_capacity_A;
int max_capacity_N;

int current_capacity_H;
int current_capacity_A;
int current_capacity_N;

int no_people;
int no_group;
int spectating_time;
int no_goal_chances;
int goals[2];
int *group_array;
char *current_zone;

people *people_array;
goal_chance *goal_chance_array;

//semaphores
sem_t zone_H;
sem_t zone_A;
sem_t zone_N;

// threads
pthread_t *people_thread;
pthread_t goal_chance_thread;

// locks and conditions

pthread_mutex_t lock_goal[2];
pthread_mutex_t *lock_person_zone;
pthread_mutex_t *lock_group_array;

pthread_cond_t cond_goal_scored[2];
pthread_cond_t *cond_zone_change;
pthread_cond_t *cond_found_friends;

void *zone_H_fun(void *i)
{
    int id = *((int *)i);

    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    t.tv_sec += people_array[id].patience_val;

    int err = sem_timedwait(&zone_H, &t);
    pthread_mutex_lock(&lock_person_zone[id]);

    if (err == -1)
    {
        if (people_array[id].current_zone == 'E')
            people_array[id].current_zone = 'D';
        pthread_mutex_unlock(&lock_person_zone[id]);
        return NULL;
    }
    else
    {
        if (people_array[id].current_zone == 'E' || people_array[id].current_zone == 'D')
        {
            printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone H" ANSI_COLOR_RESET "\n", people_array[id].name, people_array[id].team_fan);
            people_array[id].current_zone = 'H';
        }
        else
            sem_post(&zone_H);

        pthread_mutex_unlock(&lock_person_zone[id]);

        return NULL;
    }
}

void *zone_N_fun(void *i)
{
    int id = *((int *)i);

    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    t.tv_sec += people_array[id].patience_val;

    int err = sem_timedwait(&zone_N, &t);
    pthread_mutex_lock(&lock_person_zone[id]);

    if (err == -1)
    {
        if (people_array[id].current_zone == 'E')
        {
            people_array[id].current_zone = 'D';
        }
        pthread_mutex_unlock(&lock_person_zone[id]);
        // free(i);

        return NULL;
    }
    else
    {
        if (people_array[id].current_zone == 'E' || people_array[id].current_zone == 'D')
        {
            printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone N" ANSI_COLOR_RESET "\n", people_array[id].name, people_array[id].team_fan);
            people_array[id].current_zone = 'N';
        }
        else
            sem_post(&zone_N);

        pthread_mutex_unlock(&lock_person_zone[id]);
        // free(i);

        return NULL;
    }
}

void *zone_A_fun(void *i)
{
    int id = *((int *)i);

    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    t.tv_sec += people_array[id].patience_val;

    int err = sem_timedwait(&zone_A, &t);
    pthread_mutex_lock(&lock_person_zone[id]);

    if (err == -1)
    {
        if (people_array[id].current_zone == 'E')
            people_array[id].current_zone = 'D';
        pthread_mutex_unlock(&lock_person_zone[id]);
        // free(i);

        return NULL;
    }
    else
    {
        if (people_array[id].current_zone == 'E' || people_array[id].current_zone == 'D')
        {
            printf(ANSI_COLOR_GREEN "%s (%c) got a seat in zone A" ANSI_COLOR_RESET "\n", people_array[id].name, people_array[id].team_fan);
            people_array[id].current_zone = 'A';
        }
        else
            sem_post(&zone_A);

        pthread_mutex_unlock(&lock_person_zone[id]);
        // free(i);

        return NULL;
    }
}

void *people_in_game_fun(void *i)
{
    int id = *((int *)i);

    if (people_array[id].team_fan == 'H')
    {
        pthread_mutex_lock(&lock_goal[1]);
        while (goals[1] < people_array[id].goal_limit && goals[1] >= 0)
        {
            pthread_cond_wait(&cond_goal_scored[1], &lock_goal[1]);
        }
        if (goals[1] >= people_array[id].goal_limit)
        {
            printf(ANSI_COLOR_GREEN "%s is leaving due to bad performance of his team" ANSI_COLOR_RESET "\n", people_array[id].name);
        }
        pthread_mutex_unlock(&lock_goal[1]);
        pthread_cond_broadcast(&cond_zone_change[id]);
    }
    // if the person is a A fan, then look at the goals of the Home team
    else if (people_array[id].team_fan == 'A')
    {
        pthread_mutex_lock(&lock_goal[0]);
        // wait until the goals of the opposing team doesn't enrage
        // thsi person, and until the match isn't over
        while (goals[0] < people_array[id].goal_limit && goals[0] >= 0)
        {
            pthread_cond_wait(&cond_goal_scored[0], &lock_goal[0]);
        }
        if (goals[0] >= people_array[id].goal_limit)
        {
            printf(ANSI_COLOR_RED "%s is leaving due to bad performance of his team" ANSI_COLOR_RESET "\n", people_array[id].name);
        }
        pthread_mutex_unlock(&lock_goal[0]);
        pthread_cond_broadcast(&cond_zone_change[id]);
    }
    // free(i);

    return NULL;
}

void *people_fun(void *i)
{
    //enter stadium

    int id = *((int *)i);

    int *arg = malloc(sizeof(*arg));
    *arg = id;

    pthread_t thread;

    sleep(people_array[id].enter_time);
    printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n", people_array[id].name);

    pthread_t thread_H;
    pthread_t thread_A;
    pthread_t thread_N;

    //find seat
    if (people_array[id].team_fan == 'N')
    {
        pthread_create(&thread_H, NULL, zone_H_fun, arg);
        pthread_create(&thread_A, NULL, zone_A_fun, arg);
        pthread_create(&thread_N, NULL, zone_N_fun, arg);
        pthread_join(thread_H, NULL);
        pthread_join(thread_A, NULL);
        pthread_join(thread_N, NULL);
    }

    else if (people_array[id].team_fan == 'A')
    {
        pthread_create(&thread_A, NULL, zone_A_fun, arg);
        pthread_join(thread_A, NULL);
    }
    else if (people_array[id].team_fan == 'H')
    {
        pthread_create(&thread_H, NULL, zone_H_fun, arg);
        pthread_create(&thread_N, NULL, zone_N_fun, arg);
        pthread_join(thread_H, NULL);
        pthread_join(thread_N, NULL);
    }

    pthread_mutex_lock(&lock_person_zone[id]);
    int flag = 0;
    if (people_array[id].current_zone == 'D' || people_array[id].current_zone == 'G')
    {
        printf(ANSI_COLOR_BLUE "%s did not find a seat in any of the zones" ANSI_COLOR_RESET "\n", people_array[id].name);
        // printf(ANSI_COLOR_MAGENTA "%s left the stadium" ANSI_COLOR_RESET "\n", people_array[id].name);
        pthread_mutex_unlock(&lock_person_zone[people_array[id].id]);
        flag = 1;
    }
    if (flag == 0)
    {
        pthread_mutex_unlock(&lock_person_zone[id]);

        pthread_create(&thread, NULL, people_in_game_fun, arg);

        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += spectating_time;

        pthread_mutex_lock(&lock_person_zone[id]);
        if (pthread_cond_timedwait(&cond_zone_change[id], &lock_person_zone[id], &ts) == ETIMEDOUT)
        {
            printf(ANSI_COLOR_YELLOW "%s watched the match for %d seconds and is leaving" ANSI_COLOR_RESET "\n", people_array[id].name, spectating_time);
        }
        pthread_mutex_unlock(&lock_person_zone[id]);

        pthread_mutex_lock(&lock_person_zone[id]);
        if (people_array[id].current_zone == 'H')
        {
            sem_post(&zone_H);
        }
        if (people_array[id].current_zone == 'A')
        {
            sem_post(&zone_A);
        }
        if (people_array[id].current_zone == 'N')
        {
            sem_post(&zone_N);
        }
        people_array[id].current_zone = 'G';
        pthread_mutex_unlock(&lock_person_zone[id]);
    }
        pthread_mutex_lock(&lock_group_array[people_array[id].group_no]);
        group_array[people_array[id].group_no] += 1;
    
    if (group_array[people_array[id].group_no] == people_array[id].group_cardinality)
    {
        pthread_cond_broadcast(&cond_found_friends[people_array[id].group_no]);
        printf(ANSI_COLOR_CYAN "Group %d is leaving\n" ANSI_COLOR_RESET, people_array[id].group_no);
    }
    else
    {
        printf("%s is waiting for their friends at the exit\n", people_array[id].name);
        pthread_cond_wait(&cond_found_friends[people_array[id].group_no], &lock_group_array[people_array[id].group_no]);
    }

    pthread_mutex_unlock(&lock_group_array[people_array[id].group_no]);

    printf(ANSI_COLOR_MAGENTA "%s left the stadium" ANSI_COLOR_RESET "\n", people_array[id].name);
    free(i);

    return NULL;
}

void input()
{
    scanf("%d %d %d", &max_capacity_H, &max_capacity_A, &max_capacity_N);
    scanf("%d", &spectating_time);
    scanf("%d", &no_group);
    no_people = 0;

    group_array = malloc(sizeof(int) * no_group);

    for (int i = 0; i < no_group; i++)
        group_array[i] == 0;

    people_array = malloc(sizeof(people) * no_group * 30);

    for (int i = 0; i < no_group; i++)
    {
        int no_people_in_group;
        scanf("%d", &no_people_in_group);

        // no_people += no_people_in_group;
        // printf("### no_people: %d\n", no_people_in_group);
        for (int j = 0; j < no_people_in_group; j++)
        {
            char zone;
            int enter_time;
            int patience_val;
            int goal_limit;

            scanf("%s %c %d %d %d", people_array[no_people].name, &zone, &enter_time, &patience_val, &goal_limit);

            people_array[no_people].team_fan = zone;
            people_array[no_people].id = no_people;
            people_array[no_people].current_zone = 'E';
            people_array[no_people].enter_time = enter_time;
            people_array[no_people].goal_limit = goal_limit;
            people_array[no_people].group_no = i;
            people_array[no_people].patience_val = patience_val;
            people_array[no_people].group_cardinality = no_people_in_group;

            no_people++;
        }
    }

    scanf("%d", &no_goal_chances);

    goal_chance_array = malloc(sizeof(goal_chance) * no_goal_chances);

    int prev_time = 0;
    for (int i = 0; i < no_goal_chances; i++)
    {
        char goal_chance_team;
        int time_of_goal;
        double prob_of_goal;
        scanf("%s%d%lf", &goal_chance_team, &time_of_goal, &prob_of_goal);

        goal_chance_array[i].no = i;
        goal_chance_array[i].prob = prob_of_goal;
        goal_chance_array[i].team_name = goal_chance_team;
        goal_chance_array[i].time = time_of_goal;
        goal_chance_array[i].wait_time = time_of_goal - prev_time;

        if (goal_chance_team == 'H')
            goal_chance_array[i].team = 0;
        else
            goal_chance_array[i].team = 1;
    }
}

void initialize()
{
    //initialize semaphore
    sem_init(&zone_H, 0, max_capacity_H);
    sem_init(&zone_N, 0, max_capacity_N);
    sem_init(&zone_A, 0, max_capacity_A);

    //initialize thread
    people_thread = malloc(sizeof(pthread_t) * no_people);

    //initialize locks and CV
    cond_zone_change = malloc(sizeof(pthread_cond_t) * no_people);
    cond_found_friends = malloc(sizeof(pthread_cond_t) * no_group);
    lock_person_zone = malloc(sizeof(pthread_mutex_t) * no_people);
    lock_group_array = malloc(sizeof(pthread_mutex_t) * no_group);
}

int main()
{
    input();
    initialize();
    //initialize semaphore

    // printf("#### no_people: %d\n", no_people);

    for (int i = 0; i < no_people; i++)
    {
        int *arg = malloc(sizeof(*arg));
        *arg = i;

        pthread_create(&people_thread[i], NULL, people_fun, arg);
    }
    for (int i = 0; i < no_goal_chances; i++)
    {
        sleep(goal_chance_array[i].wait_time);

        // choose whether the team has scored a goal or not
        float x = (float)rand() / (float)(RAND_MAX / 1.0);
        if (goal_chance_array[i].prob > x)
        {
            // team has scored a goal

            pthread_mutex_lock(&lock_goal[goal_chance_array[i].team]);
            goals[goal_chance_array[i].team]++;
            printf(ANSI_COLOR_CYAN "Team %c has scored goal number %d" ANSI_COLOR_RESET "\n", goal_chance_array[i].team_name, goals[goal_chance_array[i].team]);
            pthread_mutex_unlock(&lock_goal[goal_chance_array[i].team]);
        }
        else
        {
            printf(ANSI_COLOR_CYAN "Team %c missed their chance to score a goal" ANSI_COLOR_RESET "\n", goal_chance_array[i].team_name);
        }
        // broadcast after every goal scoring chance, even if the team did not score
        pthread_cond_broadcast(&cond_goal_scored[goal_chance_array[i].team]);
    }
    for (int i = 0; i < no_people; i++)
    {
        pthread_join(people_thread[i], NULL);
    }

    return 0;
}

//###########FILE CHANGE ./main_folder/kushagra garg_305908_assignsubmission_file_/2019113020_assignment_5/q2/goal_chance.h ####################//

typedef struct goal_chance {
    int no;
    int team;
    char team_name;
    int time;
    int wait_time;
    double prob;
} goal_chance;