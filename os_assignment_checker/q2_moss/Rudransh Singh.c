
//###########FILE CHANGE ./main_folder/Rudransh Singh_305844_assignsubmission_file_/q2/structures.h ####################//

#ifndef STRUCTURES__H
#define STRUCTURES__H
typedef struct spectator spectator; // struct for spectator 
struct spectator{
    char name_of_spectator[100]; // name of the spectator 
    char type_of_spectator; // type of the spectator ( whether home (H) or Neutral (N) or Away (A) )
    int time_to_reach_stadium; // time to reach the stadium
    int patience; // patience of the spectator( time after which he leaves if he doesn't get a seat )
    int rage_lim; // number of goals his team can concede before he leaves

};

typedef struct group group; // struct for group

struct group{
    int group_number; // group number
    int group_size; // group size
    spectator *group_member; // array of pointers to the spectators in the group
};

typedef struct goals goals; // struct for goals

struct goals{
    int time_of_goal; // time of the goal
    double probability_of_goal; // probability of the goal
    char team_id; // team id of the team that could score the goal
};

#endif

//###########FILE CHANGE ./main_folder/Rudransh Singh_305844_assignsubmission_file_/q2/main.c ####################//

//simulating a stadium using threads

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include "structures.h"
#include "colors.h"
//function for taking input
//semaphores for home , Neutral, and away zones
enum spec_type{H, N, A};
int homecapacity, awaycapacity, neutralcapacity; // capacity of each zone
int spectating_time; //time for spectating
int num_of_groups; //number of groups
int num_of_goals; // number of goals
group* group_struct; //array of groups
goals* golazo; //array of goals
sem_t home_zone, neutral_zone, away_zone;
pthread_t goal_thread;// thread for goals
pthread_cond_t goal_scored_cond; // condition for goal scored
pthread_mutex_t goal_scored_mutex; // mutex for goal scored
int goal_scored[2]; //array for goals scored (0 is home and 1 is away)

pthread_cond_t seats_available_for_zone[3]; //condition variables for seats available for each zone ( will be signaled by zone thread, and waited on by spectator threads)
pthread_mutex_t seats_available_mutex[3]; //mutex for seats available for each zone


pthread_t zone_thread[3]; //thread for zones


void* spectator_sim(void* arg)
{
    spectator* spec = (spectator*)arg;

    //sleep until they reach the stadium
    sleep(spec->time_to_reach_stadium);
    // now the spectator has reached the stadium
    printf(RED"%s has reached the stadium\n",spec->name_of_spectator);
    int spectator_type;
    if(spec->type_of_spectator=='H')
    {
        spectator_type = 0;
    }
    else if(spec->type_of_spectator=='N')
    {
        spectator_type = 1;
    }
    else
    {
        spectator_type = 2;
    }
    char spectator_name[100];
    strcpy(spectator_name,spec->name_of_spectator);
    int allocated_zone; // the zone allocated to the spectator
    struct timeval now;
    struct timespec timeout;
    gettimeofday(&now, NULL);
    timeout.tv_sec = now.tv_sec + spec->patience; // time that the spectator will wait to get a seat in the stadium
    //do a timed wait on the condition variable for the zone that the spectator is in
    pthread_mutex_lock(&seats_available_mutex[spectator_type]);
    int istimeout=pthread_cond_timedwait(&seats_available_for_zone[spectator_type], &seats_available_mutex[spectator_type], &timeout);
    pthread_mutex_unlock(&seats_available_mutex[spectator_type]);
    if(istimeout==ETIMEDOUT) // if the spectator didn't receive a signal in the specified time period
    {
        printf(MAGENTA"%s couldn't get a seat\n",spec->name_of_spectator);
        return NULL;
    }
    else
    {
        if(spectator_type==0)// if the spectator is a home fan
        {
            // do a sem trywait on the home zone semaphore, and if it succeeds, then the spectator can enter the home zone, and zone is set to 0
            if(sem_trywait(&home_zone)==0)
            {
                allocated_zone=0;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
            else if(sem_trywait(&neutral_zone)==0)
            {
                allocated_zone=1;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
        }
        if(spectator_type==1)
        {
            if(sem_trywait(&home_zone)==0)
            {
                allocated_zone=0;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
            else if(sem_trywait(&neutral_zone)==0)
            {
                allocated_zone=1;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
            else if(sem_trywait(&away_zone)==0)
            {
                allocated_zone=2;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
        }
        if(spectator_type==2)
        {
            if(sem_trywait(&away_zone)==0)
            {
                allocated_zone=2;
                printf(MAGENTA"%s got a seat in zone %c\n",spectator_name,spec->type_of_spectator);
            }
        }
        //once it has got a seat, it will now watch the watch till the spectate time
        struct timespec timeout2;
        struct timeval now2;
        gettimeofday(&now2, NULL);
        timeout2.tv_sec = now2.tv_sec + spectating_time;
        int opposition_team;
        if(spec->type_of_spectator=='H')
        {
            opposition_team=1;
        }
        else
        {
            opposition_team=0;
        }
        if(spec->rage_lim==-1) // if the spectator is a neutral fan then he doesn't get upset by the scoreline, only sleeps till the spectate time
        {
            sleep(spectating_time);
            printf(GREEN"%s watched the match for %d seconds and is leaving\n",spec->name_of_spectator,spectating_time);
            printf(BLUE"%s is leaving for dinner\n",spec->name_of_spectator);
            if(allocated_zone==0)
                sem_post(&home_zone);
            else if(allocated_zone==1)
                sem_post(&neutral_zone);
            else
                sem_post(&away_zone);
            pthread_exit(NULL);
        }
        else
        {
            while(goal_scored[opposition_team]<=spec->rage_lim)
            {
                //do a cond time wait on goals cond
                pthread_mutex_lock(&goal_scored_mutex);
                int istimeout2=pthread_cond_timedwait(&goal_scored_cond, &goal_scored_mutex, &timeout2);
                pthread_mutex_unlock(&goal_scored_mutex);
                if(istimeout2==ETIMEDOUT) // if the spectator has used up their spectating time
                {
                    printf(GREEN"%s watched the match for %d seconds and is leaving\n",spec->name_of_spectator,spectating_time);
                    printf(BLUE"%s is leaving for dinner\n",spec->name_of_spectator);
                    if(allocated_zone==0)
                        sem_post(&home_zone);
                    else if(allocated_zone==1)
                        sem_post(&neutral_zone);
                    else
                        sem_post(&away_zone);
                    pthread_exit(NULL);
                }
                if(goal_scored[opposition_team]>=spec->rage_lim)
                {
                    printf(GREEN"%s is leaving due to the bad defensive performance of his team\n",spec->name_of_spectator);
                    printf(BLUE"%s is leaving for dinner\n",spec->name_of_spectator);
                    if(allocated_zone==0)
                        sem_post(&home_zone);
                    else if(allocated_zone==1)
                        sem_post(&neutral_zone);
                    else
                        sem_post(&away_zone);
                    pthread_exit(NULL);
                }
            }
            //if he didn't even enter while loop, means that the team was already losing, so he leaves immediately
            printf(GREEN"%s watched the match for %d seconds and is leaving\n",spec->name_of_spectator,spectating_time);
            printf(BLUE"%s is leaving for dinner\n",spec->name_of_spectator);
            pthread_exit(NULL);
        }
    }
}

void* zone_sim(void* arg)
{
    int* index=(int*)arg;
    int val;

    while(1)
    {
        //do a sem wait on the semaphore for the zone
        //if i==0 then sem wait on home zone
        //if i==1 then sem wait on neutral zone
        //if i==2 then sem wait on away zone
        if(*index==0)
        {
            sem_getvalue(&home_zone,&val);
            if(val>=1)
            {
                //broadcast home and neutral fans
                pthread_cond_signal(&seats_available_for_zone[0]);
                pthread_cond_signal(&seats_available_for_zone[1]);
            }
        }
        else if (*index==1)
        {
            sem_getvalue(&neutral_zone,&val);
            if(val>=1)
            {
                //broadcast home and neutral fans
                pthread_cond_signal(&seats_available_for_zone[0]);
                pthread_cond_signal(&seats_available_for_zone[1]);
            }
        }
        else
        {
            sem_getvalue(&away_zone,&val);
            if(val>=1)
            {
                //broadcast all away and neutral fans
                pthread_cond_signal(&seats_available_for_zone[2]);
                pthread_cond_signal(&seats_available_for_zone[1]);
            }
        }
        
    }
}

void* goal_sim(void* arg)
{
    for(int i=0;i<num_of_goals;i++)
    {
        int team_num;
        if(golazo[i].team_id=='H')
            team_num=0;
        else
            team_num=1;
        if(i==0)
        {
            sleep(golazo[i].time_of_goal);
            //choose a random number between 0 and 1 using drand48
            double random_number = rand()%100;
            if(random_number < golazo[i].probability_of_goal*100)// this means goal has been scored
            {
                //increment the goals scored
                printf(GREEN"Team %c have scored their %d th goal\n",golazo[i].team_id,goal_scored[team_num]+1);
                goal_scored[team_num]++;
                //broadcast to all spectators that the goal has been scored
                pthread_cond_broadcast(&goal_scored_cond);
            }
            else
            {
                printf(RED"Team %c have missed their %d th goal\n",golazo[i].team_id,goal_scored[team_num]+1);
            }
        }
        else
        {
            sleep(golazo[i].time_of_goal-golazo[i-1].time_of_goal);
            //choose a random number between 0 and 1 using drand48
            double random_number = rand()%100;
            if(random_number < golazo[i].probability_of_goal*100)// this means goal has been scored
            {
                //increment the goals scored
                printf(GREEN"Team %c have scored their %d th goal\n",golazo[i].team_id,goal_scored[team_num]+1);
                goal_scored[team_num]++;
                //broadcast to all spectators that the goal has been scored
                pthread_cond_broadcast(&goal_scored_cond);
            }
            else
            {
                printf(RED"Team %c have missed their %d th goal\n",golazo[i].team_id,goal_scored[team_num]+1);
            }
        }

    }
}


void simulation()
{
    //initialize condition variables and mutexes
    pthread_mutex_init(&goal_scored_mutex,NULL);
    pthread_cond_init(&goal_scored_cond,NULL);
    for(int i=0;i<3;i++)
    {
        pthread_cond_init(&seats_available_for_zone[i],NULL);
        pthread_mutex_init(&seats_available_mutex[i],NULL);
    }

    //first take input of zone capacity
    scanf("%d %d %d", &homecapacity, &awaycapacity, &neutralcapacity);
    //initializing semaphores
    sem_init(&home_zone, 0, homecapacity);
    sem_init(&neutral_zone, 0, neutralcapacity);
    sem_init(&away_zone, 0, awaycapacity);
    int val;
    sem_getvalue(&home_zone, &val);
    sem_getvalue(&neutral_zone, &val);
    sem_getvalue(&away_zone, &val);
    // total people
    int total_people_attending=0;
    //scanf spectating time( time after which the  spectators will leave the stadium)
    scanf("%d", &spectating_time);
    //scanf number of groups
    scanf("%d", &num_of_groups);
    //allocate memory for group struct
    group_struct = (group*)malloc(num_of_groups * sizeof(group));
    //take input for each group 
    for (int i = 0; i < num_of_groups; i++)
    {
        group_struct[i].group_number=i;
        scanf("%d",&group_struct[i].group_size);
        group_struct[i].group_member=(spectator*)malloc(group_struct[i].group_size * sizeof(spectator));
        for(int j=0;j < group_struct[i].group_size;j++)
        {
            total_people_attending++;
            //take input of people in group
            scanf("%s %c %d %d %d",group_struct[i].group_member[j].name_of_spectator,&group_struct[i].group_member[j].type_of_spectator,&group_struct[i].group_member[j].time_to_reach_stadium,&group_struct[i].group_member[j].patience,&group_struct[i].group_member[j].rage_lim);
        }
    }
    //take input of number of goals
    scanf("%d", &num_of_goals);
    //allocate memory for goals
    golazo = (goals*)malloc(num_of_goals * sizeof(goals));
    //take input for each goal
    for (int i = 0; i < num_of_goals; i++)
    {
       scanf(" %c %d %lf",&golazo[i].team_id,&golazo[i].time_of_goal,&golazo[i].probability_of_goal);
    }
    
    // we will now make a thread for every single person who attends

    pthread_t spectatorthreads[total_people_attending];

    int count=0;
    for(int i=0;i<num_of_groups;i++)
    {
        for(int j=0;j<group_struct[i].group_size;j++)
        {
            pthread_create(&spectatorthreads[count],NULL,spectator_sim,&group_struct[i].group_member[j]);
            count++;
        }
    }
    for(int i=0;i<3;i++)
    {
        int *a=(int*)malloc(sizeof(int));
        *a=i;
        pthread_create(&zone_thread[i],NULL,zone_sim,a);
    }
    pthread_create(&goal_thread,NULL,goal_sim,NULL);

    pthread_join(goal_thread,NULL);
    for(int i=0;i<total_people_attending;i++)
    {
        pthread_join(spectatorthreads[i],NULL);
    }
    for(int i=0;i<3;i++)
    {
        pthread_cancel(zone_thread[i]);
    }
}
int main()
{
    srand(time(NULL));
    simulation();
    //wait for goal thread
}

//###########FILE CHANGE ./main_folder/Rudransh Singh_305844_assignsubmission_file_/q2/colors.h ####################//

#ifndef COLORS__H
#define COLORS__H
# define RESET  "\x1b[0m"
# define BRIGHT  "\x1b[1m"
# define DIM  "\x1b[2m"
# define UNDERSCORE  "\x1b[4m"
# define BLINK  "\x1b[5m"
# define REVERSE  "\x1b[7m"
# define HIDDEN  "\x1b[8m"
# define BLACK  "\x1b[30m"
# define RED  "\x1b[31m"
# define GREEN  "\x1b[32m"
# define YELLOW  "\x1b[33m"
# define BLUE  "\x1b[34m"
# define MAGENTA  "\x1b[35m"
# define CYAN  "\x1b[36m"
# define WHITE  "\x1b[37m"
# define BGBLACK  "\x1b[40m"
# define BGRED  "\x1b[41m"
# define BGGREEN  "\x1b[42m"
# define BGYELLOW  "\x1b[43m"
# define BGBLUE  "\x1b[44m"
# define BGMAGENTA  "\x1b[45m"
# define BGCYAN  "\x1b[46m"
# define BGWHITE  "\x1b[47m"
#endif // !COLORS__H

