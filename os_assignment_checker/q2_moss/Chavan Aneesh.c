
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q2/main.c ####################//

#include "defs.h"
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

int* p_home_score = NULL;                           // ints storing the score
int* p_away_score = NULL;
pthread_mutex_t mutex_score = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t cond_goal_scored = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_goal_scored = PTHREAD_MUTEX_INITIALIZER;

// cond variables that zone threads broadcast when a seat opens up
int seat_free_in_zone[3];
pthread_cond_t cond_seat_free[3];
pthread_mutex_t mutex_seat_free[3];

int num_tokens;
Goal* goals = NULL;
Zone zones[3];

Group* groups = NULL;
int X;

int main(void){
//-------------------------------------init entities--------------------------------------------------------------------------

    // init scores
    p_home_score = (int*) malloc(sizeof(int));
    p_away_score = (int*) malloc(sizeof(int));
    *p_home_score = 0;
    *p_away_score = 0;
    pthread_mutex_init(&mutex_score, NULL);

    pthread_cond_init(&cond_goal_scored, NULL);
    pthread_mutex_init(&mutex_goal_scored, NULL);

    // init zones and related sems/mutexes/cvs
    for(int i = 0; i < 3; i++){
        seat_free_in_zone[i] = 0;
        pthread_cond_init(&cond_seat_free[i], NULL);
        pthread_mutex_init(&mutex_seat_free[i], NULL);

        zones[i].team = i;
        zones[i].p_cnt_seated = (int*) malloc(sizeof(int));
        *zones[i].p_cnt_seated = 0;

        pthread_mutex_init(&zones[i].mutex_seated, NULL);
        sem_init(&zones[i].fan_leaves, 0, 0);
    }

//--------------------------------------parse input-------------------------------------------------------
    scanf("%d %d %d", &(zones[home].capacity), &(zones[away].capacity), &(zones[neutral].capacity));
    scanf("%d", &X);

    // parse groups
    int num_groups;
    scanf("%d", &num_groups);
    groups = (Group*) malloc(sizeof(Group) * num_groups);

    for(int i = 0; i < num_groups; i++){
        int members;
        scanf("%d", &members);
        groups[i].id = i;
        groups[i].list = (Fan*) malloc(sizeof(Fan) * members);
        groups[i].members = members;
        
        groups[i].p_waiting = (int*) malloc(sizeof(int));

        groups[i].members_waiting = (sem_t*) malloc(sizeof(sem_t) * members);

        Fan* fl = groups[i].list;

        for(int j = 0; j < members; j++){
            sem_init(&groups[i].members_waiting[j], 0, 0);

            sem_init(&groups[i].list[j].waiting, 0, 0);

            char name[100];
            char team;
            int P, T, delay;
            scanf("%s %c %d %d %d", name, &team, &delay, &P, &T);

            fl[j].id = j;
            fl[j].delay = delay;
            fl[j].group_num = i;
            fl[j].patience = P;
            fl[j].tilt = T;

            fl[j].name = (char*) malloc(strlen(name) + 1);
            strcpy(fl[j].name, name);

            switch(team){
                case 'H':
                    fl[j].team = home;
                    break;
                case 'A':
                    fl[j].team = away;
                    break;
                case 'N':
                    fl[j].team = neutral;
                    break;
                default:
                    printf("Error in reading fan %d,%d\n", i, j);
                    assert(0);
                    break;
            }
        }
    }

    // parse goals
    scanf("%d\n", &num_tokens);
    goals = (Goal*) malloc(sizeof(Goal) * num_tokens);

    for(int i = 0; i < num_tokens; i++){
        char buf, team;                     // buf is to read the extra \n
        int time;
        float prob;

        scanf("\n%c %d %f", &team, &time, &prob);

        if (team == 'H')
            goals[i].team = home;
        else if (team == 'A')
            goals[i].team = away;
        else{
            printf("%c read\n", team);
            assert(0);
        }

        goals[i].time = time;
        goals[i].prob = prob;
    }


//-------------------------------------init threads--------------------------------------------------------------------------                                        
    pthread_t zone_tids[3];
    for (int i = 0; i < 3; i++){
        pthread_create(&zone_tids[i], NULL, zone_thread, (void*) &zones[i].team);

        printf("Zone %d created\n", i);
    }
    
    pthread_t** fan_tids = (pthread_t**) malloc(sizeof(pthread_t) * num_groups);
    pthread_t* group_tids = (pthread_t*) malloc(sizeof(pthread_t) * num_groups);

    for (int i = 0; i < num_groups; i++){
        fan_tids[i] = (pthread_t*) malloc(sizeof(pthread_t) * groups[i].members);
        Group g = groups[i];

        printf("group %d created\n", i);

        pthread_create(&group_tids[i], NULL, group_thread, (void*) &groups[i].id);

        for(int j = 0; j < groups[i].members; j++){

            int *coords = (int*) malloc(sizeof(int) * 2);
            coords[0] = i; coords[1] = j; 

            int err = pthread_create(&fan_tids[i][j], NULL, fan_thread, (void*) coords);
            if(err != 0){
                perror("Fan thread");
            }

            printf("Fan %s created\n", g.list[j].name);
        }
    }

    pthread_t game_tid;
    pthread_create(&game_tid, NULL, game_thread, NULL);

//-------------------------------------wait and cleanup--------------------------------------------------------------------------                        

    for (int i = 0; i < num_groups; i++){
        pthread_join(group_tids[i], NULL);
        for(int j = 0; j < groups[i].members; j++)
            pthread_join(fan_tids[i][j], NULL);
    }

    printf("The simulation is over, all fans have exited\n");

    pthread_cancel(game_tid);
    for(int i = 0; i < 3; i++)
        pthread_cancel(zone_tids[i]);

    // cleanup

    free(p_away_score);
    free(p_home_score);
    pthread_mutex_destroy(&mutex_score);

    pthread_cond_destroy(&cond_goal_scored);
    pthread_mutex_destroy(&mutex_goal_scored);

    for(int i = 0; i < 3; i++){
        pthread_cond_destroy(&cond_seat_free[i]);
        pthread_mutex_destroy(&mutex_seat_free[i]);

        free(zones[i].p_cnt_seated);
        pthread_mutex_destroy(&zones[i].mutex_seated);

        sem_destroy(&zones[i].fan_leaves);
    }    

    for(int i = 0; i < num_groups; i++){
        Fan* fl = groups[i].list;
        for(int j = 0; j < groups[i].members; j++){
            free(fl[j].name);
            sem_destroy(&groups[i].members_waiting[j]);

        }
        free(fan_tids[i]);
        free(fl);

        free(groups[i].members_waiting);
    }

    free(group_tids);
    free(fan_tids);

    free(groups);
    free(goals);
}
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q2/threads.c ####################//

#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

// extern all global variables from main.c
extern int* p_home_score;
extern int* p_away_score;
extern pthread_mutex_t mutex_score;

extern pthread_cond_t cond_goal_scored;
extern pthread_mutex_t mutex_goal_scored;

// whether there is a seat free for some group of fans
extern int seat_free_in_zone[3];

// signals a group of fans if a seat is free for them
extern pthread_cond_t cond_seat_free[3];
extern pthread_mutex_t mutex_seat_free[3];

extern Goal* goals;
extern Zone zones[3];
extern Group* groups;
extern int num_tokens;
extern int X;

// simulates goals and wakes up spectators watching
void* game_thread(void* v){
    printf(GAME_COLOR"The match has started\n"NORMAL_COLOR);

    srand(time(NULL));

    // wait until each token
    for(int cnt_token = 0; cnt_token < num_tokens; cnt_token++){
        if (cnt_token == 0)
            sleep(goals[cnt_token].time);
        else{
            int dead_time = goals[cnt_token].time - goals[cnt_token-1].time;
            sleep(dead_time);
        }

        // goal scored?
        float roll = (float) rand()/RAND_MAX;

        if (roll <= goals[cnt_token].prob){
            ++(*(goals[cnt_token].team == home ? p_home_score : p_away_score));

            printf(GAME_COLOR"The %s team has scored! The score is %d-%d\n"NORMAL_COLOR, 
                     (goals[cnt_token].team == home ? "home" : "away"), *p_home_score, *p_away_score);
        }
        else {
            printf(GAME_COLOR"The %s team missed their a chance at a goal! The score is still %d-%d\n"NORMAL_COLOR, 
                   (goals[cnt_token].team == home ? "home" : "away"), *p_home_score, *p_away_score);
        }

        pthread_mutex_lock(&mutex_goal_scored);
        pthread_cond_broadcast(&cond_goal_scored);
        pthread_mutex_unlock(&mutex_goal_scored);
    }
}

// signals fans that a ticket is available
void* zone_thread(void *vp_z_id){
    int zone_id = *(int*) vp_z_id;
    Zone *z = &zones[zone_id];
    printf(ZONE_COLOR"Zone %d starting\n"NORMAL_COLOR, zone_id);

    // keep broadcasting until the zone is full
    while(1){
        pthread_mutex_lock(&z->mutex_seated);
        if (*z->p_cnt_seated < z->capacity){
            pthread_mutex_unlock(&z->mutex_seated);

            // alert all appropirate threads that a seat is free
            wakeup_fans(z->team);
        }
        else {
            // zone is filled, exit while loop
            assert(*z->p_cnt_seated <= z->capacity);
            break;
        }
    }

    // printf(ZONE_COLOR"Zone filled, entering wait mode %d\n"NORMAL_COLOR, z->team);

    // zone now waits for a fan to leave before broadcasting cond
    pthread_mutex_unlock(&z->mutex_seated);
    while(1){
        sem_wait(&z->fan_leaves);

        printf(ZONE_COLOR"A fan has left zone %d\n"NORMAL_COLOR, z->team);
        pthread_mutex_lock(&z->mutex_seated);
        (*z->p_cnt_seated)--;
        pthread_mutex_unlock(&z->mutex_seated);

        wakeup_fans(z->team);
    }
}

void wakeup_fans(int zone_id){
    switch(zone_id){
        case home:
        case neutral:
            pthread_mutex_lock(&mutex_seat_free[home]);
            pthread_cond_broadcast(&cond_seat_free[home]);
            pthread_mutex_unlock(&mutex_seat_free[home]);
            
            pthread_mutex_lock(&mutex_seat_free[neutral]);
            pthread_cond_broadcast(&cond_seat_free[neutral]);
            pthread_mutex_unlock(&mutex_seat_free[neutral]);

            break;

        case away:
            pthread_mutex_lock(&mutex_seat_free[away]);
            pthread_cond_broadcast(&cond_seat_free[away]);
            pthread_mutex_unlock(&mutex_seat_free[away]);
            
            pthread_mutex_lock(&mutex_seat_free[neutral]);
            pthread_cond_broadcast(&cond_seat_free[neutral]);
            pthread_mutex_unlock(&mutex_seat_free[neutral]);
            
            break;

        default:
            printf("incorrect team for wakeup_fans\n");
            assert(0);
    }
}

void* fan_thread(void *vp_fan_coords){
    int* coords = (int*) vp_fan_coords;
    Fan *f = &groups[coords[0]].list[coords[1]];

    printf("(%d,%d)\n", f->group_num, f->id);

    sleep(f->delay);
    printf(FAN_COLOR"%s (%d,%d) has reached the stadium\n"NORMAL_COLOR,
            f->name, f->group_num, f->id);
    
    // fan casually approaches ticketbooth after delay, set timespec to wait until patience runs out
    int seated = -1, err = 0;
    struct timespec wait_until;
    clock_gettime(CLOCK_REALTIME, &wait_until);
    wait_until.tv_sec += f->patience;

    // wait until a seat is available or times out
    while(seated == -1){
        // the entire process is a critical section, mutex_seat_free used for this
        pthread_mutex_lock(&mutex_seat_free[f->team]);
        err = pthread_cond_timedwait(&cond_seat_free[f->team], &mutex_seat_free[f->team], &wait_until);

        if (err == 0){
            // seat is available, try and look for a seat in allowed zones
            if (f->team == home || f->team == neutral){
                int available = 0;
                pthread_mutex_lock(&zones[home].mutex_seated);
                if(*zones[home].p_cnt_seated  < zones[home].capacity){
                    // seat available, buying it rn
                    (*zones[home].p_cnt_seated)++;
                    seated = home;
                }
                pthread_mutex_unlock(&zones[home].mutex_seated);

                if (seated == -1){
                    pthread_mutex_lock(&zones[neutral].mutex_seated);
                    if(*zones[neutral].p_cnt_seated  < zones[neutral].capacity){
                        // seat available, buying it rn
                        (*zones[neutral].p_cnt_seated)++;
                        seated = neutral;
                    }
                    pthread_mutex_unlock(&zones[neutral].mutex_seated);
                }
            }

            if (f->team == away || f->team == neutral){
                if (seated == -1){
                    pthread_mutex_lock(&zones[away].mutex_seated);
                    if(*zones[away].p_cnt_seated  < zones[away].capacity){
                        // seat available, buying it rn
                        (*zones[away].p_cnt_seated)++;
                        seated = away;
                    }
                    pthread_mutex_unlock(&zones[away].mutex_seated);
                }
            }
        }
        else if (err == ETIMEDOUT){
            pthread_mutex_unlock(&mutex_seat_free[f->team]);
            break;
        }
        else {
            printf("fan %d group %d timed cond wait error\n", f->id, f->group_num);
            assert(0);
        }

        pthread_mutex_unlock(&mutex_seat_free[f->team]);
    }   

    // fan has been seated in zone 'seated'
    if (seated != -1){
        
        switch(seated){
            case home:
                printf(ZONE_COLOR"%s has been seated in the home zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            case neutral:
                printf(ZONE_COLOR"%s has been seated in the neutral zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            case away:
                printf(ZONE_COLOR"%s has been seated in the away zone (%d)\n"NORMAL_COLOR,
                        f->name, seated);
                break;
            default:
                assert(0);
                break;
        }

        // prepare timespec to mark leaving time
        int time_to_go = 0;
        struct timespec watch_until;
        clock_gettime(CLOCK_REALTIME, &watch_until);
        watch_until.tv_sec += X;

        // loop to simulate watching the match
        while(1){
            // check if team is inting
            if (f->team == home || f->team == away){
                pthread_mutex_lock(&mutex_score);
                int frustrated = *(f->team == home ? p_away_score : p_home_score) >= f->tilt;
                pthread_mutex_unlock(&mutex_score);

                if (frustrated){
                    printf(FAN_COLOR"%s is leaving zone %d due to his teams poor performance\n"NORMAL_COLOR, f->name, seated);
                    sem_post(&zones[seated].fan_leaves);
                    break;
                }
            }

            // wait until a goal is scored, check for ragequit or timeout
            pthread_mutex_lock(&mutex_goal_scored);
            time_to_go = pthread_cond_timedwait(&cond_goal_scored, &mutex_goal_scored, &watch_until);

            if(time_to_go == ETIMEDOUT){
                pthread_mutex_unlock(&mutex_goal_scored);
                printf(FAN_COLOR"%s watched the match for %d seconds and is leaving zone %d\n"NORMAL_COLOR, f->name, X, seated);
                sem_post(&zones[seated].fan_leaves);
                break;
            }
            // if not timed out, loops and checks the score again
            pthread_mutex_unlock(&mutex_goal_scored);
        }
    }

    // no seat, fan has timed out and is leaving
    else {
        assert(err == ETIMEDOUT);
        printf(FAN_COLOR"%s couldn't get a seat\n"NORMAL_COLOR,
            f->name);
    }

    printf(FAN_LEAVES_COLOR"%s is waiting for their group\n"NORMAL_COLOR, f->name);

    sem_post(&groups[f->group_num].members_waiting[f->id]);    

    
    sem_wait(&f->waiting);
    // pthread_mutex_unlock(&groups[f->group_num].mutex_waiting);

    return NULL;
}

void* group_thread(void *vp_g_id){
    int group_id = *(int*) vp_g_id;

    Group G = groups[group_id];

    for (int i = 0; i < G.members; i++){
        sem_wait(&G.members_waiting[i]);
    }

    printf(GROUP_COLOR"Group %d is leaving\n"NORMAL_COLOR, group_id + 1);

    for (int i = 0; i < G.members; i++){
        sem_post(&G.list[i].waiting);
    }

    return NULL;
}
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q2/defs.h ####################//

#ifndef __DEFS__
#define __DEFS__

#include <pthread.h>
#include <semaphore.h>

#define FAN_COLOR               "\x1b[31m"
#define FAN_LEAVES_COLOR       "\x1b[34m"
#define ZONE_COLOR                    "\x1b[32m"
#define GROUP_COLOR                "\x1b[33m"
#define GAME_COLOR                   "\x1b[35m"
#define NORMAL_COLOR                "\x1b[0m"


enum teams{home, away, neutral};

struct s_goal_token{
    int team;
    int time;
    float prob;
};
typedef struct s_goal_token Goal;

struct s_zone{
    int team;                          // 'H' or 'A' or 'N'
    int capacity;                       

    // int* p_cnt_waiting;                 // how many are waiting for a ticket
    // pthread_mutex_t mutex_waiting;       

    int* p_cnt_seated;                  // how many people seated
    pthread_mutex_t mutex_seated;       // respective mutex

    sem_t fan_leaves;                    // wakes the thread up when a fan leaves
                                        // used to evaluate whether the cnd_variables are to be signalled
};
typedef struct s_zone Zone;

struct s_fan{
    int id;

    int team;
    char* name;
    int delay;                          // T 
    int patience;                       // P
    int tilt;

    int group_num;

    sem_t waiting;
};
typedef struct s_fan Fan;

struct s_group{
    int id;

    Fan* list;
    int members;            // number ofpeople

    sem_t* members_waiting;

    int* p_waiting;
    pthread_mutex_t mutex_waiting;
    pthread_cond_t cond_waiting;
};
typedef struct s_group Group;

void print_group(Group G);
void print_fan(Fan f);
void print_zones(Zone* p_z);
void print_goals(Goal *p_g, int num_goals);

void* zone_thread(void *vp_z_id);           // contains the zone
void* fan_thread(void *vp_fan_coords);      // int[2] containing group id and fan id
void* game_thread(void*);
void* group_thread(void *vp_g_id);

void wakeup_fans(int zone_id);

#endif

/*

2 1 2
4
3
3
A N 3 2 -1
B H 1 3 2
C A 2 1 4
4
D H 1 2 4
E N 2 1 -1
F A 1 2 1
G N 3 1 -1
4
t1 H 1 2 8
t2 H 1 2 8
t3 H 1 2 8
t4 H 1 2 8
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4

*/

/*

H.cap  A.cap   N.cap
patience
group_num

group 1 size
name team delay patience tilt
.
.
.
last group
...
<Few lines describing the  last group>

num tokens
1st token team   time   prob

*/
//###########FILE CHANGE ./main_folder/Chavan Aneesh_305783_assignsubmission_file_/2020111018/q2/printfns.c ####################//

#include "defs.h"
#include <stdio.h>
#include <stdlib.h>

void print_fan(Fan f){
    printf("%s, team: %d\n", f.name, f.team);
    printf("delay: %d   patience: %d   tilt: %d\n", f.delay, f.patience, f.tilt);
    printf("group %d\n", f.group_num);
}

void print_group(Group G){
    printf("%d members\n", G.members);
    for(int i = 0; i < G.members; i++){
        print_fan(G.list[i]);
        printf("\n");
    }
}

void print_zones(Zone *p_z){
    for(int i = 0; i < 3; i++){
        printf("Team: %d, capacity: %d\n", p_z[i].team, p_z[i].capacity);
        pthread_mutex_lock(&p_z[i].mutex_seated);
        printf("%d seated currently\n\n", *p_z[i].p_cnt_seated);
        pthread_mutex_unlock(&p_z[i].mutex_seated);
    }
}

void print_goals(Goal *p_g, int num_goals){
    for(int i = 0; i < num_goals; i++){
        Goal g = p_g[i];
        printf("team: %d, time: %d, prob: %f\n", g.team, g.time, g.prob);
    }
}