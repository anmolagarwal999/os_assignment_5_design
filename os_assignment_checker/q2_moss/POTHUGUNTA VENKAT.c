
//###########FILE CHANGE ./main_folder/POTHUGUNTA VENKAT_305809_assignsubmission_file_/2020101076_assignment_5/q2/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int h,a,n,x,num_groups,p_count=0,g,count_h=0,count_a=0,count_n=0;
int time1 = 0;


struct groups{
	int no_of_people;
	int start_no;
	int end_no;
};

struct chances{
	char group;
	int elapsed_time;
	double probability;
};

struct people{
	char name[30];
	char supporting_name;
	int reached_time;
	int patience_time;
	int max_goals_diff;
	char his_status;
	char seat_allocatedZone;
};

struct people peeps[1000];

void print_P_reached(struct people peep){
	printf(ANSI_COLOR_RED "%s has reached the stadium" ANSI_COLOR_RESET "\n",peep.name);
}

void print_P_gotSeat(struct people peep){
	printf(ANSI_COLOR_GREEN "%s has got seat in zone %c" ANSI_COLOR_RESET "\n",peep.name,peep.seat_allocatedZone);
}

void print_P_didntGetSeat(struct people peep){
	printf(ANSI_COLOR_YELLOW "%s couldn't get a seat" ANSI_COLOR_RESET "\n",peep.name);
	printf(ANSI_COLOR_YELLOW "%s is leaving for dinner" ANSI_COLOR_RESET "\n",peep.name);
}

void print_P_watchedX(struct people peep){
	printf(ANSI_COLOR_YELLOW "%s watched the match for %d seconds and is leaving" ANSI_COLOR_RESET "\n",peep.name,x);
	printf(ANSI_COLOR_YELLOW "%s is leaving for dinner" ANSI_COLOR_RESET "\n",peep.name);
}

void print_P_leavingBad(struct people peep){
	printf(ANSI_COLOR_BLUE "%s is leaving due to bad defensive performance of his team" ANSI_COLOR_RESET "\n",peep.name);
	printf(ANSI_COLOR_BLUE "%s is leaving for dinner" ANSI_COLOR_RESET "\n",peep.name);
}

void print_team_goal(char c, int goal_count){
	printf(ANSI_COLOR_MAGENTA "%c have scored their %d goal" ANSI_COLOR_RESET "\n",c,goal_count);
}

void print_team_goal_missed(char c,int goal_count){
	printf(ANSI_COLOR_CYAN "%c missed the chance to score their %dth goal" ANSI_COLOR_RESET "\n",c,goal_count);
}

void* people_reach_func(){
	for(int i = 0 ; i < p_count; i++){
		if(peeps[i].reached_time == time1){
			print_P_reached(peeps[i]);
		}	
	}
}

int main(){

	scanf("%d %d %d",&h,&a,&n);
	count_a = a;
	count_h = h;
	count_n = n;
	scanf("%d",&x);
	scanf("%d",&num_groups);

	struct groups group[num_groups];

	for(int j = 0 ; j < num_groups; j++){
		int num;
		scanf("%d",&num);
		for(int k = 0; k < num; k++){
			scanf("%s %c %d %d %d",peeps[p_count].name,&peeps[p_count].supporting_name,&peeps[p_count].reached_time,&peeps[p_count].patience_time,&peeps[p_count].max_goals_diff);
			p_count++;
		}
	}

	scanf("%d",&g);

	struct chances chance[g];

	for(int j = 0; j < g; j++){
		scanf("%c %d %lf",&chance[j].group,&chance[j].elapsed_time,&chance[j].probability);
	}
	// print_P_gotSeat(peeps[0]);
	int loop = p_count;
	while(loop--){
		pthread_t th[4];
		if(pthread_create(&th[1], NULL, &people_reach_func, NULL)!= 0){
			perror("failed to create a thread\n");
		}
		if(pthread_join(th[1], NULL)!= 0){
			perror("failed to join\n");
		}
		sleep(1);
		time1++;
	}
	return 0;
}

/*
2 1 2
3
2
3
Vibhav N 3 2 -1
Sarthak H 1 3 2
Ayush A 2 1 4
4
Rachit H 1 2 4
Roshan N 2 1 -1
Adarsh A 1 2 1
Pranav N 3 1 -1
5
H 1 1
A 2 0.95
A 3 0.5
H 5 0.85
H 6 0.4
*/

