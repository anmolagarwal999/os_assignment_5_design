
//###########FILE CHANGE ./main_folder/ANKITH VARUN JAYARAM_305859_assignsubmission_file_/q2/main.c ####################//

#include "entities.h"
#include <stdio.h>
#include <stdlib.h>

// Run simulation
void runSim()
{
    // Create spectator threads
    for(int i = 0 ; i < num_groups ; i++)
    {
        Groups[i]->spectator_thread_ids = (pthread_t *) malloc(Groups[i]->num_people*sizeof(pthread_t));        
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
        {
            pthread_t curr_td;
            pthread_create(&curr_td,NULL,spectatorSim,(void *)Groups[i]->people[j]);
            Groups[i]->spectator_thread_ids[j] = curr_td;
        }
    }

    // Create chance thread
    pthread_t chances_thread_id;
    pthread_create(&chances_thread_id,NULL,chanceSim,NULL);

    // Join spectator threads
    for(int i = 0 ; i < num_groups ; i++)
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
            pthread_join(Groups[i]->spectator_thread_ids[j],NULL);

    // Join chance thread
    pthread_join(chances_thread_id,NULL);
}

// Take input and initialise all entities of the simulation 
void input()
{
    H = (Zone) malloc(sizeof(zone));
    A = (Zone) malloc(sizeof(zone));
    N = (Zone) malloc(sizeof(zone));
    
    scanf("%d %d %d",&H->num_seats,&A->num_seats,&N->num_seats);
    scanf("%d",&spectating_time);
    
    scanf("%d",&num_groups);
    Groups = (Group *) malloc(num_groups*sizeof(group));
    for(int i = 0 ; i < num_groups ; i++)
    {
        Groups[i] = (Group) malloc(sizeof(group));
        scanf("%d",&Groups[i]->num_people);
        Groups[i]->people = (Spectator *) malloc(Groups[i]->num_people*sizeof(spectator));
        Groups[i]->num_people_exited = 0;
        pthread_mutex_init(&Groups[i]->exit_lock,NULL);
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
        {
            Groups[i]->people[j] = (Spectator) malloc(sizeof(spectator));
            scanf("%s %c %d %d %d",Groups[i]->people[j]->name,&Groups[i]->people[j]->fanbase,&Groups[i]->people[j]->arrival_time,&Groups[i]->people[j]->patience_time,&Groups[i]->people[j]->humiliation_threshold);
            Groups[i]->people[j]->grpid = i;
            Groups[i]->people[j]->is_watching = 0;
            Groups[i]->people[j]->id = j;
            sem_init(&Groups[i]->people[j]->wait_seat,0,0);
            sem_init(&Groups[i]->people[j]->spectate,0,0);
            sem_init(&Groups[i]->people[j]->wait_gate,0,0);
        }
    }

    scanf("%d",&num_chances);
    Chances = (Chance *) malloc(num_chances*sizeof(chance));
    for(int i = 0 ; i < num_chances ; i++)
    {
        Chances[i] = (Chance) malloc(sizeof(chance));
        scanf("\n%c %d %f",&Chances[i]->team,&Chances[i]->time,&Chances[i]->conversion_prob);
    }

    H->name = 'H';
    H->num_seats_left = H->num_seats;

    A->name = 'A';
    A->num_seats_left = A->num_seats;

    N->name = 'N';
    N->num_seats_left = N->num_seats;

    num_waiting = 0;
    pthread_mutex_init(&Spect_lock,NULL);

    scoreline[0] = 0;
    scoreline[1] = 0;
}

int main()
{
    input();
    runSim();
}
//###########FILE CHANGE ./main_folder/ANKITH VARUN JAYARAM_305859_assignsubmission_file_/q2/entities.h ####################//

#ifndef __ENTITIES_H__
#define __ENTITIES_H__

#include <pthread.h>
#include <semaphore.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_BLACK   "\x1b[30m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Structure representing the entity spectator
typedef struct spectator{
    int id;                                         // Spectator ID of group
    char name[30];                                  // Name of person
    char fanbase;                                   // Home, Away or Neutral fan
    int arrival_time;                               // Time of arrival at the stadium
    int patience_time;                              // Patience value of person
    int humiliation_threshold;                      // Max no of opposition goals a spectator can bear watching
    int grpid;                                      // Group ID to which person belongs
    int wait_index;                                 // Index at which person is waiting for a seat in the stadium
    int cur_time;                                   // Number of seconds elapsed since start of simulation
    int is_watching;                                // State of spectator
    
    sem_t wait_seat;                                // Semaphore used to wait for vacant seat in stadium
    sem_t spectate;                                 // Semaphore used to spectate match
    sem_t wait_gate;                                // Semaphore used to wait at the exit gate
}spectator,*Spectator;

typedef struct group
{
    int num_people;                                 // Number of people in the group
    int num_people_exited;                          // Number of people waiting at the exit gate
    Spectator *people;                              // Array of persons/spectators
    pthread_t *spectator_thread_ids;                // Array of person/spectator thread IDs
    pthread_mutex_t exit_lock;                      // Mutex lock to ensure thread safety of group objects
}group,*Group;

typedef struct chance
{
    char team;                                      // Home or Away team
    int time;                                       // Time in seconds after which a goal scoring chance arises
    float conversion_prob;                          // Probability of conversion into goal
}chance,*Chance;

typedef struct zone{
    char name;                                      // Name of the zone (H,N,A)
    int num_seats;                                  // Max no of seats available in the zone
    int num_seats_left;                             // No of vacant seats left
}zone,*Zone;

int spectating_time;                                
int num_groups;
int num_chances;
int scoreline[2];                                   // Scoreline in the format {Home goals,Away goals}

Group *Groups;
Chance *Chances;
Zone H,N,A;                                         // Home, Away and Neutral zones
Spectator Waiting_list[30];                         // List of people waiting for a vacant seat in the stadium
int num_waiting;                                    // Number of waiting people

pthread_mutex_t Spect_lock;                         // Mutex lock to ensure thread safety of global variables

void *spectatorSim(void *spect);
void *chanceSim(void *arg);

#endif
//###########FILE CHANGE ./main_folder/ANKITH VARUN JAYARAM_305859_assignsubmission_file_/q2/entities.c ####################//

#include "entities.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Generates a random floating point number between 0 and 1
float randomChanceGenerator()
{
    srand(time(NULL));
    int x = rand();
    int y = (rand() % x) + 1;
    return (float) y / (float) (x + 1); 
}

// Randomly choose a preferred zone
int chooseZone(char fanbase)
{
    srand(time(NULL));
    if(fanbase == 'H')
    {
        if(H->num_seats_left > 0 && N->num_seats_left > 0)
            return rand() % 2;
        else if(H->num_seats_left > 0)
            return 0;
        else if(N->num_seats_left > 0)
            return 1;
        else
            return -1;
    }
    else if(fanbase == 'N')
    {
        if(H->num_seats_left > 0 && N->num_seats_left > 0 && A->num_seats_left > 0)
            return rand() % 3;
        else if(H->num_seats_left > 0 && N->num_seats_left > 0)
            return rand() % 2;
        else if(N->num_seats_left > 0 && A->num_seats_left > 0)
            return (rand() % 2) + 1;
        else if(H->num_seats_left > 0 && A->num_seats_left > 0)
            if(rand() % 2 == 1)
                return 2;
            else
                return 0;
        else if(H->num_seats_left > 0)
            return 0;
        else if(N->num_seats_left > 0)
            return 1;
        else if(A->num_seats_left > 0)
            return 2;
        else
            return -1;
    }
    else if(A->num_seats > 0)
        return 2;
    else
        return -1;
}

// Add a person to waiting list
void addToWaiting(Spectator Spect)
{
    pthread_mutex_lock(&Spect_lock);
    Spect->wait_index = num_waiting;
    Waiting_list[num_waiting++] = Spect;
    pthread_mutex_unlock(&Spect_lock);
}

// Remove a person from waiting list
void deleteFromWaiting(Spectator Spect)
{   
    pthread_mutex_lock(&Spect_lock);
    for(int j = Spect->wait_index ; j < num_waiting - 1 ; j++)
        Waiting_list[j] = Waiting_list[j + 1];
    num_waiting--;
    pthread_mutex_unlock(&Spect_lock);
}

// Check if a person in waiting list is eligible to fill vacancy in zone
void updateWaitingList(char zone, int time)
{
    for(int i = 0 ; i < num_waiting ; i++)
    {
        if(Waiting_list[i]->fanbase == 'N' || (Waiting_list[i]->fanbase == 'H' && zone != 'A') || (Waiting_list[i]->fanbase == 'A' && zone == 'A'))
        {
            pthread_mutex_lock(&Spect_lock);
            Waiting_list[i]->cur_time = time;
            sem_post(&Waiting_list[i]->wait_seat);      // Stop waiting for vacant seat
            pthread_mutex_unlock(&Spect_lock);
            return;
        }
    }
}

void *spectatorSim(void *spect)
{
    Spectator Spect = (Spectator) spect;
    Spect->cur_time = 0;

    // Person arrives at the stadium
    sleep(Spect->arrival_time);
    Spect->cur_time += Spect->arrival_time;
    printf(ANSI_COLOR_RED "t=%d : %s has arrived at the stadium\n",Spect->cur_time,Spect->name);
    Spect->cur_time++;
    sleep(1);

    Zone zones[3] = {H,N,A};
    int i,s;
    struct timespec ts;

    // Choose a zone to get a seat in
    pthread_mutex_lock(&Spect_lock);
    i = chooseZone(Spect->fanbase);
    if(i != -1)
    {
        zones[i]->num_seats_left--;
    }
    pthread_mutex_unlock(&Spect_lock);

    // No seat is available
    if(i == -1)
    {
        addToWaiting(Spect);
        clock_gettime(CLOCK_REALTIME,&ts);
        ts.tv_sec += Spect->patience_time;
        s = sem_timedwait(&Spect->wait_seat,&ts);       // Wait for a vacant seat
        deleteFromWaiting(Spect);
    }
    else
        s = 0;
   
    // Semaphore wait_seat timed out ; Person's patience value is exceeded
    if(s == -1)
    {
        Spect->cur_time += Spect->patience_time;
        printf(ANSI_COLOR_CYAN "t=%d : %s could not get a seat\n",Spect->cur_time,Spect->name);
    }
    else
    { 
        // Choose a zone to get a seat in
        pthread_mutex_lock(&Spect_lock);
        if(i == -1)
        {
            i = chooseZone(Spect->fanbase);
            zones[i]->num_seats_left--;
        }
        pthread_mutex_unlock(&Spect_lock);
        
        printf(ANSI_COLOR_BLUE "t=%d : %s has got a seat in zone %c\n",Spect->cur_time,Spect->name,zones[i]->name);
        Spect->is_watching = 1;

        // Leave match if humiliation threshold is reached
        if((Spect->fanbase == 'H' && scoreline[1] >= Spect->humiliation_threshold) || (Spect->fanbase == 'A' && scoreline[0] >= Spect->humiliation_threshold))
            sem_post(&Spect->spectate);         

        clock_gettime(CLOCK_REALTIME,&ts);
        ts.tv_sec += spectating_time;
        s = sem_timedwait(&Spect->spectate,&ts);    // Start spectating match

        // Semaphore spectate timed out ; spectating time has been reached
        if(s == -1)
        {
            Spect->cur_time += spectating_time;
            printf(ANSI_COLOR_GREEN "t=%d : %s watched the match for %d seconds and is leaving\n",Spect->cur_time,Spect->name,spectating_time);
        }

        // Humiliation threshold is reached and person leaves match
        else
        {
            printf(ANSI_COLOR_WHITE "t=%d : %s is leaving due to bad performance of their team\n",Spect->cur_time,Spect->name);   
        }
        Spect->is_watching = 0;

        pthread_mutex_lock(&Spect_lock);
        zones[i]->num_seats_left++;                                 
        pthread_mutex_unlock(&Spect_lock);
        updateWaitingList(zones[i]->name,Spect->cur_time);      // A seat is vacant ; fill the vacancy
    }

    // Reach exit gate
    Spect->cur_time++;
    sleep(1);
    printf(ANSI_COLOR_MAGENTA "t=%d : %s is waiting for their friends at the exit\n",Spect->cur_time,Spect->name);
    
    pthread_mutex_lock(&Groups[Spect->grpid]->exit_lock);
    Groups[Spect->grpid]->num_people_exited++;
    pthread_mutex_unlock(&Groups[Spect->grpid]->exit_lock);

    // Check if the person is the last one to exit in their group
    if(Groups[Spect->grpid]->num_people_exited == Groups[Spect->grpid]->num_people)
    {
        for(int i = 0 ; i < Groups[Spect->grpid]->num_people ; i++)
            sem_post(&Groups[Spect->grpid]->people[i]->wait_gate);      // Leave the stadium
        printf(ANSI_COLOR_BLACK "t=%d : Group %d is leaving for dinner\n",Spect->cur_time,Spect->grpid + 1);
    }

    sem_wait(&Spect->wait_gate);    // Wait for rest of the group
    return NULL;
}

// Check if any spectator has reached their humiliation threshold
void checkSpectator(int cur_time)
{
    for(int i = 0 ; i < num_groups ; i++)
        for(int j = 0 ; j < Groups[i]->num_people ; j++)
            if(Groups[i]->people[j]->is_watching == 1 && ((Groups[i]->people[j]->fanbase == 'H' && scoreline[1] >= Groups[i]->people[j]->humiliation_threshold) || (Groups[i]->people[j]->fanbase == 'A' && scoreline[0] >= Groups[i]->people[j]->humiliation_threshold)))
            {
                Groups[i]->people[j]->cur_time = cur_time;
                sem_post(&Groups[i]->people[j]->spectate);          // Stop spectating
            }
}

void *chanceSim(void *arg)
{
    int cur_time = 0;
    for(int i = 0 ; i < num_chances ; i++)
    {
        // Sleep till a chance arises
        if(cur_time < Chances[i]->time)
            sleep(Chances[i]->time - cur_time);
        else
            sleep(cur_time - Chances[i]->time);

        cur_time = Chances[i]->time;

        int j;
        if(Chances[i]->team == 'H')
            j = 0;
        else
            j = 1;

        // Check if the chance is converted and update scoreline accordingly
        if(randomChanceGenerator() < Chances[i]->conversion_prob)
        {
            printf(((scoreline[j] + 1 == 1) ? ANSI_COLOR_YELLOW "t=%d : Team %c have scored their %dst goal\n" : ((scoreline[j] + 1 == 2) ? ANSI_COLOR_YELLOW "t=%d : Team %c have scored their %dnd goal\n" : ((scoreline[j] + 1 == 3) ? ANSI_COLOR_YELLOW "t=%d : Team %c have scored their %drd goal\n" : ANSI_COLOR_YELLOW "t=%d : Team %c have scored their %dth goal\n"))),cur_time,Chances[i]->team,scoreline[j] + 1);
            scoreline[j]++;
            checkSpectator(cur_time);
        }
        else
            printf(((scoreline[j] + 1 == 1) ? ANSI_COLOR_YELLOW  "t=%d : Team %c missed the chance to score their %dst goal\n" : ((scoreline[j] + 1 == 2) ? ANSI_COLOR_YELLOW "t=%d : Team %c missed the chance to score their %dnd goal\n" : ((scoreline[j] + 1 == 3) ? ANSI_COLOR_YELLOW "t=%d : Team %c missed the chance to score their %drd goal\n" : ANSI_COLOR_YELLOW "t=%d : Team %c missed the chance to score their %dth goal\n"))),cur_time,Chances[i]->team,scoreline[j] + 1);
    }
    return NULL;
}