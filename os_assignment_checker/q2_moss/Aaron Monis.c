
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/group.h ####################//

#ifndef GROUP_H
#define GROUP_H

#include <pthread.h>

typedef struct person person;
typedef struct group group;

struct person {
    char name[100];
    char HNA;
    int time;
    int patience;
    int goals;
    pthread_mutex_t *mutex;
    int seating;
};

struct group {
    int id;
    int num_members;
    person *members_list;
    pthread_mutex_t *mutex;
};

group *groups_list;
pthread_mutex_t *student_list_lock;

void *group_thread_init(group *g);
void groups_init(int n);
void *person_init(person *p);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/main.c ####################//

#include "global.h"

extern int zone_1, zone_2, zone_3;
extern int spectate_time;
extern pthread_mutex_t *mutex_1;
extern pthread_mutex_t *mutex_2;
extern pthread_mutex_t *mutex_3;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &zone_1, &zone_2, &zone_3);
    scanf("%d", &spectate_time);
    int number_of_groups;
    scanf("%d", &number_of_groups);
    groups_init(number_of_groups);
    int number_of_chances;
    scanf("%d", &number_of_chances);
    match_init(number_of_chances);
    mutex_1 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_2 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_3 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(mutex_1, NULL);
    pthread_mutex_init(mutex_2, NULL);
    pthread_mutex_init(mutex_3, NULL);
    pthread_t *groups = (pthread_t *)malloc(number_of_groups * sizeof(pthread_t));
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_create(&groups[i], NULL, group_thread_init, (void *)&groups_list[i]);
    }
    pthread_t *matchup = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create(matchup, NULL, match_thread_init, (void *)m);
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_join(groups[i], NULL);
    }
    pthread_join(*matchup, NULL);
    // Wait for all the group threads and the match to finish
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/group.c ####################//

#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void groups_init(int n)
{
    // Initialize the groups
    groups_list = (group *)malloc(n * sizeof(group));
    for (int i = 0; i < n; i++)
    {
        groups_list[i].id = i + 1;
        scanf("%d", &groups_list[i].num_members);
        groups_list[i].members_list = (person *)malloc(groups_list[i].num_members * sizeof(person));
        char str[1000];
        for (int j = 0; j < groups_list[i].num_members; j++)
        {
            // Take the input for all groups concerining their inforamtion
            // Also get the information about each member of the group
            scanf("%s %c %d %d %d", groups_list[i].members_list[j].name, &groups_list[i].members_list[j].HNA, &groups_list[i].members_list[j].time, &groups_list[i].members_list[j].patience, &groups_list[i].members_list[j].goals);
            groups_list[i].members_list[j].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
            pthread_mutex_init(groups_list[i].members_list[j].mutex, NULL);
        }
        groups_list[i].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(groups_list[i].mutex, NULL);
    }
}
void *group_thread_init(group *g)
{
    // Start a thread for each member and simulate it completely
    pthread_t *member_threads = (pthread_t *)malloc(sizeof(pthread_mutex_t) * g->num_members);
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_create(&member_threads[i], NULL, person_init, &g->members_list[i]);
    }
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_join(member_threads[i], NULL);
    }
    printf(BGRN "Group %d is leaving for dinner\n" ANSI_RESET, g->id); // Once we have finished all threads we can make the group leave all at once for dinner

    return NULL;
}
void *person_init(person *p)
{
    int t = 0;
    while (p->time > 0) // Wait for arrival at the stadium
    {
        sleep(1);
        p->time--;
        t++;
    }
    printf(BGRN "%s has reached the stadium\n" ANSI_RESET, p->name); // Arrived
    // Wait for a seat to be alloted and exhaust the patience as well
    // If the patience is exhaused go to the exit (gate)
    // Search for a seat based on preference
    if (p->HNA == 'H')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else if (p->HNA == 'N')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            pthread_mutex_lock(mutex_2);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_2 > 0)
            {
                zone_2--;
                p->seating = 2;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_2);
            if (zone_2 > 0)
            {
                zone_2--;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                p->seating = 2;
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    int watch_time = spectate_time;
    // Spectate the match and leave after it
    while (watch_time--)
    {
        // If the team plays terribly leave
        if ((p->HNA == 'H' && m->away_score >= p->goals) || (p->HNA == 'A' && m->home_score >= p->goals))
        {
            printf(BBLK "%s is leaving due to the bad defensive performance of his team\n" ANSI_RESET, p->name);
            goto exit;
        }
        sleep(1);
        t++;
    }
    // display that the person has left after watching for some time
    printf(BBLU "%s watched the match for %d seconds and is leaving\n" ANSI_RESET, p->name, spectate_time);
    if (p->seating == 1) // Indicate that the seat where the person was is now available
    {
        zone_1++;
    }
    else if (p->seating == 2)
    {
        zone_2++;
    }
    else
    {
        zone_3++;
    }
exit:;
    sleep(1);
    t++;
    printf(BCYN "%s is waiting for their friends at the exit\n" ANSI_RESET, p->name); // Wait at the exit for friends

    return NULL;
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/global.h ####################//

#ifndef GLOBAL_H
#define GLOBAL_H

#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

// #include "course.h"
#include "match.h"
#include "group.h"

void *shareMem(size_t size);
int random_number(int min, int max);
int zone_1, zone_2, zone_3;
pthread_mutex_t *mutex_1; 
pthread_mutex_t *mutex_2;
pthread_mutex_t * mutex_3;
int spectate_time;

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/match.h ####################//

#ifndef MATCH_H
#define MATCH_H

#include <pthread.h>

typedef struct match match;

struct match {
    int number_of_chances;
    int *time;
    double *chance;
    char *HNA;
    int home_score;
    int away_score;
    pthread_mutex_t *mutex;
};

pthread_mutex_t *student_list_lock;
match *m;

void *match_thread_init(match *m);
void match_init(int n);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/OS_Assignment-5/q2/match.c ####################//

#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void match_init(int n)
{
    // Init match details
    m = (match *)malloc(sizeof(match));
    m->number_of_chances = n;
    m->time = (int *)malloc(sizeof(int) * n);
    m->chance = (double *)malloc(sizeof(double) * n);
    m->HNA = (char *)malloc(sizeof(char) * n);
    for(int i=0;i<n;i++)
    {
        scanf(" %c %d %lf", &m->HNA[i], &m->time[i], &m->chance[i]);
    }
    m->home_score = 0;
    m->away_score = 0;
}
void *match_thread_init(match *m)
{
    int t = 0;
    int i = 0;
    // Go over all the chances that occured
    while (i < m->number_of_chances)
    {
        while (t < m->time[i]) // Wait for the chance to occur
        {
            t++;
            sleep(1);
        }
        // Make a random choice to indicate if the goal was scored
        int n = random_number(1, 100);
        int chance = m->chance[i] * 100;
        if (n <= chance)
        {
            int s;
            // Increase the correspnding score if the goal was scored
            if (m->HNA[i] == 'H')
            {
                m->home_score += 1;
                s = m->home_score;
            }
            else
            {
                m->away_score += 1;
                s = m->away_score;
            }
            printf(BYEL "Team %c have scored their %d" ANSI_RESET, m->HNA[i], s);
            switch (s)
            {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
            }
            printf(BYEL " goal\n" ANSI_RESET);
        }
        else
        {
            // display a missed chance
            int s;
            if (m->HNA[i] == 'H')
            {
                s = m->home_score;
            }
            else
            {
                s = m->away_score;
            }
            printf(BCYN "Team %c missed the chance to score their %d" ANSI_RESET, m->HNA[i], s + 1);
            switch (s + 1)
            {
                case 0:
                    printf(BCYN "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BCYN "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BCYN "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BCYN "rd" ANSI_RESET);
                    break;
                default:
                    printf(BCYN "th" ANSI_RESET);
                    break;
            }
            printf(BCYN " goal\n" ANSI_RESET);
        }
        i++;
    }
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/group.h ####################//

#ifndef GROUP_H
#define GROUP_H

#include <pthread.h>

typedef struct person person;
typedef struct group group;

struct person {
    char name[100];
    char HNA;
    int time;
    int patience;
    int goals;
    pthread_mutex_t *mutex;
    int seating;
};

struct group {
    int id;
    int num_members;
    person *members_list;
    pthread_mutex_t *mutex;
};

group *groups_list;
pthread_mutex_t *student_list_lock;

void *group_thread_init(group *g);
void groups_init(int n);
void *person_init(person *p);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/main.c ####################//

#include "global.h"

extern int zone_1, zone_2, zone_3;
extern int spectate_time;
extern pthread_mutex_t *mutex_1;
extern pthread_mutex_t *mutex_2;
extern pthread_mutex_t *mutex_3;

void *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (void *)shmat(shm_id, NULL, 0);
}
int random_number(int min, int max)
{
    return rand() % (max - min + 1) + min;
}
int main(void)
{
    srand(time(NULL));
    scanf("%d %d %d", &zone_1, &zone_2, &zone_3);
    scanf("%d", &spectate_time);
    int number_of_groups;
    scanf("%d", &number_of_groups);
    groups_init(number_of_groups);
    int number_of_chances;
    scanf("%d", &number_of_chances);
    match_init(number_of_chances);
    mutex_1 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_2 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    mutex_3 = (pthread_mutex_t *)shareMem(sizeof(pthread_mutex_t));
    pthread_mutex_init(mutex_1, NULL);
    pthread_mutex_init(mutex_2, NULL);
    pthread_mutex_init(mutex_3, NULL);
    pthread_t *groups = (pthread_t *)malloc(number_of_groups * sizeof(pthread_t));
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_create(&groups[i], NULL, group_thread_init, (void *)&groups_list[i]);
    }
    pthread_t *matchup = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create(matchup, NULL, match_thread_init, (void *)m);
    for (int i = 0; i < number_of_groups; i++)
    {
        pthread_join(groups[i], NULL);
    }
    pthread_join(*matchup, NULL);
    // Wait for all the group threads and the match to finish
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/group.c ####################//

#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void groups_init(int n)
{
    // Initialize the groups
    groups_list = (group *)malloc(n * sizeof(group));
    for (int i = 0; i < n; i++)
    {
        groups_list[i].id = i + 1;
        scanf("%d", &groups_list[i].num_members);
        groups_list[i].members_list = (person *)malloc(groups_list[i].num_members * sizeof(person));
        char str[1000];
        for (int j = 0; j < groups_list[i].num_members; j++)
        {
            // Take the input for all groups concerining their inforamtion
            // Also get the information about each member of the group
            scanf("%s %c %d %d %d", groups_list[i].members_list[j].name, &groups_list[i].members_list[j].HNA, &groups_list[i].members_list[j].time, &groups_list[i].members_list[j].patience, &groups_list[i].members_list[j].goals);
            groups_list[i].members_list[j].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
            pthread_mutex_init(groups_list[i].members_list[j].mutex, NULL);
        }
        groups_list[i].mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(groups_list[i].mutex, NULL);
    }
}
void *group_thread_init(group *g)
{
    // Start a thread for each member and simulate it completely
    pthread_t *member_threads = (pthread_t *)malloc(sizeof(pthread_mutex_t) * g->num_members);
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_create(&member_threads[i], NULL, person_init, &g->members_list[i]);
    }
    for (int i = 0; i < g->num_members; i++)
    {
        pthread_join(member_threads[i], NULL);
    }
    printf(BGRN "Group %d is leaving for dinner\n" ANSI_RESET, g->id); // Once we have finished all threads we can make the group leave all at once for dinner

    return NULL;
}
void *person_init(person *p)
{
    int t = 0;
    while (p->time > 0) // Wait for arrival at the stadium
    {
        sleep(1);
        p->time--;
        t++;
    }
    printf(BGRN "%s has reached the stadium\n" ANSI_RESET, p->name); // Arrived
    // Wait for a seat to be alloted and exhaust the patience as well
    // If the patience is exhaused go to the exit (gate)
    // Search for a seat based on preference
    if (p->HNA == 'H')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else if (p->HNA == 'N')
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_1);
            pthread_mutex_lock(mutex_3);
            pthread_mutex_lock(mutex_2);
            if (zone_1 > 0)
            {
                zone_1--;
                p->seating = 1;
                printf(BMAG "%s has got a seat in zone H\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_2 > 0)
            {
                zone_2--;
                p->seating = 2;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            else if (zone_3 > 0)
            {
                zone_3--;
                p->seating = 3;
                printf(BMAG "%s has got a seat in zone N\n" ANSI_RESET, p->name);
                pthread_mutex_unlock(mutex_1);
                pthread_mutex_unlock(mutex_3);
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_1);
            pthread_mutex_unlock(mutex_3);
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    else
    {
        while (true)
        {
            if (p->patience == 0)
            {
                printf(BRED "%s could not get a seat\n" ANSI_RESET, p->name);
                goto exit;
            }
            pthread_mutex_lock(mutex_2);
            if (zone_2 > 0)
            {
                zone_2--;
                printf(BMAG "%s has got a seat in zone A\n" ANSI_RESET, p->name);
                p->seating = 2;
                pthread_mutex_unlock(mutex_2);
                break;
            }
            pthread_mutex_unlock(mutex_2);
            sleep(1);
            t++;
            p->patience--;
        }
    }
    int watch_time = spectate_time;
    // Spectate the match and leave after it
    while (watch_time--)
    {
        // If the team plays terribly leave
        if ((p->HNA == 'H' && m->away_score >= p->goals) || (p->HNA == 'A' && m->home_score >= p->goals))
        {
            printf(BBLK "%s is leaving due to the bad defensive performance of his team\n" ANSI_RESET, p->name);
            goto exit;
        }
        sleep(1);
        t++;
    }
    // display that the person has left after watching for some time
    printf(BBLU "%s watched the match for %d seconds and is leaving\n" ANSI_RESET, p->name, spectate_time);
    if (p->seating == 1) // Indicate that the seat where the person was is now available
    {
        zone_1++;
    }
    else if (p->seating == 2)
    {
        zone_2++;
    }
    else
    {
        zone_3++;
    }
exit:;
    sleep(1);
    t++;
    printf(BCYN "%s is waiting for their friends at the exit\n" ANSI_RESET, p->name); // Wait at the exit for friends

    return NULL;
}
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/global.h ####################//

#ifndef GLOBAL_H
#define GLOBAL_H

#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

// #include "course.h"
#include "match.h"
#include "group.h"

void *shareMem(size_t size);
int random_number(int min, int max);
int zone_1, zone_2, zone_3;
pthread_mutex_t *mutex_1; 
pthread_mutex_t *mutex_2;
pthread_mutex_t * mutex_3;
int spectate_time;

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/match.h ####################//

#ifndef MATCH_H
#define MATCH_H

#include <pthread.h>

typedef struct match match;

struct match {
    int number_of_chances;
    int *time;
    double *chance;
    char *HNA;
    int home_score;
    int away_score;
    pthread_mutex_t *mutex;
};

pthread_mutex_t *student_list_lock;
match *m;

void *match_thread_init(match *m);
void match_init(int n);

#endif
//###########FILE CHANGE ./main_folder/Aaron Monis_305842_assignsubmission_file_/2020101129_assignment_5/OS_Assignment-5/q2/match.c ####################//

#include "global.h"

#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

void match_init(int n)
{
    // Init match details
    m = (match *)malloc(sizeof(match));
    m->number_of_chances = n;
    m->time = (int *)malloc(sizeof(int) * n);
    m->chance = (double *)malloc(sizeof(double) * n);
    m->HNA = (char *)malloc(sizeof(char) * n);
    for(int i=0;i<n;i++)
    {
        scanf(" %c %d %lf", &m->HNA[i], &m->time[i], &m->chance[i]);
    }
    m->home_score = 0;
    m->away_score = 0;
}
void *match_thread_init(match *m)
{
    int t = 0;
    int i = 0;
    // Go over all the chances that occured
    while (i < m->number_of_chances)
    {
        while (t < m->time[i]) // Wait for the chance to occur
        {
            t++;
            sleep(1);
        }
        // Make a random choice to indicate if the goal was scored
        int n = random_number(1, 100);
        int chance = m->chance[i] * 100;
        if (n <= chance)
        {
            int s;
            // Increase the correspnding score if the goal was scored
            if (m->HNA[i] == 'H')
            {
                m->home_score += 1;
                s = m->home_score;
            }
            else
            {
                m->away_score += 1;
                s = m->away_score;
            }
            printf(BYEL "Team %c have scored their %d" ANSI_RESET, m->HNA[i], s);
            switch (s)
            {
                case 0:
                    printf(BYEL "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BYEL "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BYEL "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BYEL "rd" ANSI_RESET);
                    break;
                default:
                    printf(BYEL "th" ANSI_RESET);
                    break;
            }
            printf(BYEL " goal\n" ANSI_RESET);
        }
        else
        {
            // display a missed chance
            int s;
            if (m->HNA[i] == 'H')
            {
                s = m->home_score;
            }
            else
            {
                s = m->away_score;
            }
            printf(BCYN "Team %c missed the chance to score their %d" ANSI_RESET, m->HNA[i], s + 1);
            switch (s + 1)
            {
                case 0:
                    printf(BCYN "th" ANSI_RESET);
                    break;
                case 1:
                    printf(BCYN "st" ANSI_RESET);
                    break;
                case 2:
                    printf(BCYN "nd" ANSI_RESET);
                    break;
                case 3:
                    printf(BCYN "rd" ANSI_RESET);
                    break;
                default:
                    printf(BCYN "th" ANSI_RESET);
                    break;
            }
            printf(BCYN " goal\n" ANSI_RESET);
        }
        i++;
    }
}