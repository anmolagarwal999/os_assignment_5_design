
//###########FILE CHANGE ./main_folder/ROMAHARSHAN PUSAPATI_305781_assignsubmission_file_/Assignment_5/q2/q2.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>

#define ANSI_COLOR_BLACK "\033[0;30m"
#define ANSI_COLOR_RED "\033[0;31m"
#define ANSI_COLOR_GREEN "\033[0;32m"
#define ANSI_COLOR_YELLOW "\033[0;33m"
#define ANSI_COLOR_BLUE "\033[0;34m"
#define ANSI_COLOR_PURPLE "\033[0;35m"
#define ANSI_COLOR_CYAN "\033[0;36m"
#define ANSI_COLOR_RESET "\x1b[0m"
struct goals
{
    char team[2];
    int time;
    float prob;
};
#define MAXIMUM 100
#define SMALLEST 3
int gm;
#define NOT 0
#define YE 1
struct person
{
    char name[256];
    char zone[2];
    int time;
    int p_time;
    int num_goals;
    int arrived;
    char got_seat_in_zone[2];
    int waiting;
};
int the_string_len = 0;
int X;
int actual_time = 1;
int the_bytes_input = 0;
int A_GOAL = 0, H_GOAL = 0;
int len_maximum;
int total_number_of_people;
int H_capacity, A_capacity, N_capacity, g_scoring;
struct person people[1000];
int len_minimum;
struct goals chances[1000];
bool seated[1000];
int ml;
sem_t seming[1000];
sem_t waiting[1000];
int mg = 0;
int ended = 0;
sem_t end;

int get_value(float p)
{
    len_maximum = YE * NOT + 4;
    double val = (double)rand() / RAND_MAX;
    return val < p;
}

void *time_function()
{
    the_string_len++;
    while (1)
    {
        if (ended == 1 + total_number_of_people)
        {
            len_maximum = YE * NOT + 4;
            return NULL;
        }
        actual_time++;
        the_string_len++;
        sleep(1);
    }
}

void rage_quit(char x)
{
    for (int i = 0; i < total_number_of_people; i++)
    {
        len_minimum = the_bytes_input - 1;
        if (people[i].arrived == 1 && people[i].waiting == 0 && people[i].num_goals != -1)
        {
            the_bytes_input = NOT+YE-1;
            if (x != people[i].zone[0])
            {
                the_string_len++;
                if (x == 'A' && A_GOAL >= people[i].num_goals)
                {
                    sem_post(&waiting[i]);
                }
                the_bytes_input++;
                if (x == 'H' && H_GOAL >= people[i].num_goals)
                {
                    sem_post(&waiting[i]);
                }
            }
            the_bytes_input = NOT + YE - 1;
        }
    }
}

void *person_function(void *ptr)
{
    int index1 = *(int *)ptr;
    // wait till arrival actual_time
    the_bytes_input = NOT+YE-1;
    while (actual_time < people[index1].time)
    {
        sleep(1);
    }
    the_bytes_input++;
    printf(ANSI_COLOR_RED "t = %d: %s has reached the stadium\n" ANSI_COLOR_RESET, actual_time, people[index1].name); // person with name arrived
    // conditions for seat status if not found sleep til p_actual_time or till vacant
    people[index1].arrived = 1;
    ml++;
    people[index1].waiting = 1;
    len_maximum = YE*NOT + 4;
    if (people[index1].zone[0] == 'A')
    {
        if (A_capacity > 0)
        {
            len_minimum = the_bytes_input - 1;
            A_capacity--;
            seated[index1] = true;
            people[index1].got_seat_in_zone[0] = 'A';
            len_maximum = YE*NOT + 4;
            people[index1].waiting = 0;
            printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
        }
        else
        {
            the_string_len++;
            struct timespec timingspec;
            clock_gettime(CLOCK_REALTIME, &timingspec);
            timingspec.tv_sec += people[index1].p_time;
            len_maximum = the_string_len - 1;
            if (sem_timedwait(&seming[index1], &timingspec) == -1)
            {
                len_maximum = YE*NOT + 4;
                printf(ANSI_COLOR_YELLOW "t = %d: %s couldn’t get a seat\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                people[index1].waiting = 0;
                ended++;
                the_string_len++;
                return NULL;
            }
            else
            {
                the_bytes_input = NOT+YE-1;
                people[index1].waiting = 0;
            }
        }
    }
    else
    {
        the_bytes_input++;
        if (people[index1].zone[0] == 'N')
        {
            if (N_capacity > 0)
            {
                the_string_len++;
                N_capacity--;
                seated[index1] = true;
                people[index1].got_seat_in_zone[0] = 'N';
                len_maximum = YE*NOT + 4;
                people[index1].waiting = 0;
                printf(ANSI_COLOR_BLUE "t = %d: %s has got a seat in zone N\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
            }
            else
            {
                the_string_len++;
                if (A_capacity > 0)
                {
                    A_capacity--;
                    the_string_len++;
                    seated[index1] = true;
                    people[index1].got_seat_in_zone[0] = 'A';
                    the_bytes_input++;
                    people[index1].waiting = 0;
                    printf(ANSI_COLOR_CYAN "t = %d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                }
                else if (H_capacity > 0)
                {
                    len_minimum = len_maximum + len_minimum;
                    H_capacity--;
                    seated[index1] = true;
                    people[index1].got_seat_in_zone[0] = 'H';
                    mg++;
                    people[index1].waiting = 0;
                    printf(ANSI_COLOR_BLACK "t = %d: %s has got a seat in zone H\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
                }
                else
                {
                    gm++;
                    struct timespec timingspec;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    timingspec.tv_sec += people[index1].p_time;
                    mg++;
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        the_bytes_input++;
                        printf("t = %d:%s couldn’t get a seat\n", actual_time, people[index1].name);
                        people[index1].waiting = 0;
                        the_string_len++;
                        return NULL;
                        ended++;
                    }
                    else
                    {
                        the_bytes_input++;
                        people[index1].waiting = 0;
                    }
                }
            }
        }
        else
        {
            if (H_capacity > 0)
            {
                len_maximum = YE*NOT + 4;
                H_capacity--;
                seated[index1] = true;
                people[index1].got_seat_in_zone[0] = 'H';
                the_bytes_input++;
                people[index1].waiting = 0;
                printf(ANSI_COLOR_PURPLE "t = %d: %s has got a seat in zone H\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
            }
            else
            {
                the_bytes_input = NOT+YE-1;
                if (N_capacity > 0)
                {
                    N_capacity--;
                    seated[index1] = true;
                    the_bytes_input++;
                    people[index1].got_seat_in_zone[0] = 'N';
                    people[index1].waiting = 0;
                    the_string_len++;
                    printf("t = %d: %s has got a seat in zone N\n", actual_time, people[index1].name);
                }
                else
                {
                    mg++;  
                    struct timespec timingspec;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    timingspec.tv_sec += people[index1].p_time;
                    gm++;
                    printf("%s is waiting for seat\n", people[index1].name);
                    int valing = sem_timedwait(&seming[index1], &timingspec);
                    the_bytes_input++;
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        the_bytes_input++;
                        printf("t = %d:%s couldn’t get a seat\n", actual_time, people[index1].name);
                        people[index1].waiting = 0;
                        ended++;
                        mg++;
                        return NULL;
                    }
                    else
                    {
                        the_bytes_input++;
                        people[index1].waiting = 0;
                    }
                }
            }
            the_bytes_input++;
        }
    }
    struct timespec timedspec;
    clock_gettime(CLOCK_REALTIME, &timedspec);
    timedspec.tv_sec += X;
    mg++;
    if (sem_timedwait(&waiting[index1], &timedspec) != -1)
    {
        printf(ANSI_COLOR_BLACK "t = %d: %s is leaving due to bad performance of his team\n" ANSI_COLOR_RESET, actual_time, people[index1].name); // rage quit
    }
    else
    {
        len_maximum = YE*NOT + 4;
        printf(ANSI_COLOR_CYAN "t = %d: %s watched the match for %d seconds and is leaving\n" ANSI_COLOR_RESET, actual_time, people[index1].name, X); // normally exitted
    }
    char c = people[index1].got_seat_in_zone[0];
    int flag = 0;
    len_maximum = YE*NOT + 4;
    for (int i = 0; i < total_number_of_people; i++)
    {
        //printf("%s\n",people[index1].name);
        //printf("%c %s %d %d\n",c,people[i].name,people[i].arrived,people[i].waiting);
        if (i == index1)
        {
            len_minimum = len_maximum + len_minimum;
            continue;
        }
        if (people[i].arrived == 1 && people[i].waiting == 1)
        {
            gm++;
            if (c == 'A')
            {
                if (people[i].zone[0] == 'A')
                {
                    len_maximum = the_string_len - 1;
                    printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                    people[i].got_seat_in_zone[0] = 'A';
                    //printf("%s sempost working\n", people[i].name);
                    len_minimum = the_bytes_input - 1;
                    sem_post(&seming[i]);
                    people[i].waiting = 0;
                    //got seat
                    mg++;
                    flag = 1;
                    break;
                }
                if (people[i].zone[0] == 'N')
                {
                    printf(ANSI_COLOR_BLUE "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                    people[i].got_seat_in_zone[0] = 'A';
                    len_minimum = the_bytes_input - 1;
                    //printf("%s sempost working\n", people[i].name);
                    people[i].waiting = 0;
                    sem_post(&seming[i]);
                    flag = 1;
                    mg++;
                    break;
                }
            }
            else
            {
                if (c == 'N')
                {
                    int kl;
                    if (people[i].zone[0] == 'H')
                    {
                        printf(ANSI_COLOR_PURPLE "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'N';
                        mg++;
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        //got seat
                        flag = 1;
                        gm++;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        printf(ANSI_COLOR_BLUE "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'N';
                        the_bytes_input++;
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        the_bytes_input++;
                        flag = 1;
                        break;
                    }
                }
                else
                {
                    mg++;
                    if (people[i].zone[0] == 'H')
                    {
                        printf(ANSI_COLOR_PURPLE "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'H';
                        len_maximum = YE*NOT + 4;  
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        the_bytes_input++;
                        flag = 1;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        the_bytes_input = NOT+YE-1;
                        printf(ANSI_COLOR_BLUE "t = %d: %s has got a seat in zone %c\n" ANSI_COLOR_RESET, actual_time, people[i].name, c);
                        people[i].got_seat_in_zone[0] = 'H';
                        len_maximum = the_string_len - 1;
                        people[i].waiting = 0;
                        //printf("%s sempost working\n", people[i].name);
                        sem_post(&seming[i]);
                        the_bytes_input = NOT+YE-1;
                        flag = 1;
                        break;
                    }
                }
            }
            gm++;
        }
    }
    if (flag == 0)
    {
        if (c == 'A')
        {
            mg++;
            A_capacity++;
        }
        else
        {
            if (c == 'N')
            {
                the_string_len++;
                N_capacity++;
            }
            else
            {
                gm++;
                H_capacity++;
            }
        }
    }
    the_bytes_input++;
    ended++;
}

void *goal_timing()
{
    int count = 0;
    for (int i = 0; i < g_scoring; i++)
    {
        the_bytes_input++;
        while (actual_time < chances[i].time)
        {
            sleep(1);
        }
        int x = get_value(chances[i].prob);
        the_string_len++;
        //printf("Chance : %d\n", x);
        if (chances[i].team[0] == 'A')
        {
            A_GOAL += x;
            the_string_len++;
            if (x == 1)
            {
                the_bytes_input++;
                printf(ANSI_COLOR_YELLOW "t = %d: Team A has scored their %d goal\n" ANSI_COLOR_RESET, actual_time, A_GOAL);
                char x0 = 'A';
                rage_quit(x0);
                continue;
            }
            else
            {
                the_string_len++;
                printf("t = %d: Team A missed the chance to score their %d goal\n", actual_time, A_GOAL + 1);
                continue;
            }
        }
        else
        {
            len_maximum = YE*NOT + 4;
            H_GOAL += x;
            if (x == 1)
            {
                printf(ANSI_COLOR_YELLOW "t = %d: Team H has scored their %d goal\n" ANSI_COLOR_RESET, actual_time, H_GOAL);
                char x0 = 'H';
                mg++;
                rage_quit(x0);
                continue;
            }
            else
            {
                the_bytes_input++;
                printf("t = %d: Team H missed the chance to score their %d goal\n", actual_time, H_GOAL + 1);
                continue;
            }
        }
        sleep(1);
    }
    ended++;
    return NULL;
}

int main()
{
    pthread_mutex_t locks[1000];
    the_string_len++;
    for (int i = 0; i < 1000; i++)
    {
        mg++;
        pthread_mutex_init(&locks[i], NULL);
    }
    srand(time(0));
    len_maximum = the_string_len - 1;
    scanf("%d %d %d", &H_capacity, &A_capacity, &N_capacity);
    scanf("%d", &X);
    int groups;
    the_bytes_input++;
    scanf("%d", &groups);
    int total_no_of_people = 0;
    int count = 0;
    the_bytes_input++;
    for (int i = 0; i < groups; i++)
    {
        int number_of_people;
        scanf("%d", &number_of_people);
        the_string_len++;
        for (int j = 0; j < number_of_people; j++)
        {
            the_bytes_input++;
            scanf("%s %s %d %d %d", people[count].name, people[count].zone, &people[count].time, &people[count].p_time, &people[count].num_goals);
            count++;
            len_maximum = YE*NOT + 4;
            total_no_of_people++;
            the_bytes_input++;
        }
    }
    total_number_of_people = total_no_of_people;
    scanf("%d", &g_scoring);
    mg++;
    for (int i = 0; i < g_scoring; i++)
    {
        gm++;
        scanf("%s %d %f", chances[i].team, &chances[i].time, &chances[i].prob);
        the_string_len = the_string_len + NOT; 
    }
    pthread_t persons[total_no_of_people];
    for (int i = 0; i < total_no_of_people; i++)
    {
        the_bytes_input++;
        pthread_create(&persons[i], NULL, person_function, &i);
        usleep(100);
    }
    pthread_t goals_scored, timing;
    len_minimum = len_maximum + len_minimum;
    pthread_create(&goals_scored, NULL, goal_timing, NULL);
    pthread_create(&timing, NULL, time_function, NULL);
    mg++;
    for (int i = 0; i < total_no_of_people; i++)
    {
        pthread_join(persons[i], NULL);
        gm++;
    }
    pthread_join(goals_scored, NULL);
    pthread_join(timing, NULL);
    the_string_len = the_string_len + NOT; 
    for (int i = 0; i < 1000; i++)
    {
        pthread_mutex_destroy(&locks[i]);
    }
}
