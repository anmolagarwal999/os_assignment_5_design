
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/group.h ####################//

#ifndef __GROUP_H__
#define __GROUP_H__

#include "user.h"
#include "person.h"

struct group
{
    int number;
    int number_of_persons;
    struct person *people;

    int num_people_waiting_at_exit;
    pthread_mutex_t wait_exit_lock;
    pthread_cond_t wait_exit_cond;
    pthread_t tid;
};

typedef struct group group;
group *group_list;

void *simulate_group(void *arg);
void increament_exiting_cnt(int group_number);
void increament_group_exit_cnt();

#endif

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/stimer.h ####################//

#ifndef __STIMER_H__
#define __STIMER_H__

#include "user.h"


extern pthread_mutex_t stimer_lock ;
extern pthread_cond_t stimer_cond;

extern bool start_clock;
extern pthread_mutex_t start_clock_lock;
extern pthread_cond_t start_clock_cond;

void increament_timer(int arg);
void simulate_timer();

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/main.c ####################//

#include "user.h"
#include <signal.h>

int zone_H_limit;
int zone_A_limit;
int zone_N_limit;

int num_persons_zone_H;
int num_persons_zone_A;
int num_persons_zone_N;

int number_of_goal_chances;

int stimer = -1;
int SPECTATING_TIME_X;
int NUMBER_OF_GROUPS;

pthread_mutex_t stimer_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t stimer_cond = PTHREAD_COND_INITIALIZER;

int number_of_groups;
int home_team_score;
int away_team_score;
int num_exited_groups;

pthread_mutex_t num_exited_group_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t num_exited_group_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t zone_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t zone_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t group_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t group_cond = PTHREAD_COND_INITIALIZER;

int main()
{
    // printf("hello\n");
    init_all_threads();
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/person.c ####################//

#include "person.h"

void *simulate_person(void *arg)
{
    person *person_x = (person *)arg;
    pthread_mutex_lock(&stimer_lock);
    while (stimer != person_x->time)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);

    // printf(BLUE_COLOR "person time = %d\n" RESET_COLOR, person_x->time);
    printf(RED_COLOR "%s has reached the stadium\n" RESET_COLOR, person_x->name);

    // // waiting time part near ticket counter
    // struct timespec abs_time;

    // clock_gettime(CLOCK_REALTIME, &abs_time);
    // abs_time.tv_sec += person_x->patience;
    // abs_time.tv_nsec += person_x->patience * 1000000000;
    // // wait for ticket
    // int error;
    // pthread_mutex_lock(&person_x->zone_lock);
    // while (person_x->zone_x == 'X')
    // {
    //     increament_zone_cnt(person_x);
    //     error = pthread_cond_timedwait(&person_x->zone_cond, &person_x->zone_lock, &abs_time);
    // }
    // if (error == ETIMEDOUT)
    // {
    //     printf(PINK_COLOR "Person %s could not get a seat\n" RESET_COLOR, person_x->zone_x, person_x->name);
    // }
    // else
    // {
    //     pthread_mutex_unlock(&person_x->zone_lock);
    //     printf(PINK_COLOR "%s has got a seat in zone %c\n" RESET_COLOR, person_x->zone_x, person_x->name, person_x->zone_x);
    // }

    person_x->start = stimer;
    increament_zone_cnt(person_x);
    if (person_x->got_seat)
    {
        person_x->in_stadium = true;
        // printf("watching\n");
        person_x->start = stimer;
        person_x->end = person_x->start + SPECTATING_TIME_X;
        pthread_mutex_lock(&stimer_lock);
        while (stimer != person_x->end && !person_x->at_exit_gate)
        {
            pthread_cond_wait(&stimer_cond, &stimer_lock);
        }
        person_x->in_stadium = false;
        pthread_mutex_unlock(&stimer_lock);
        if (!person_x->at_exit_gate)
        {
            decreament_zone_cnt(person_x);
            person_x->at_exit_gate = true;
            printf(GREEN_COLOR "%s watched the match for %d seconds and is leaving\n" RESET_COLOR, person_x->name, SPECTATING_TIME_X);
        }
    }

    //*************** BONUS PART
    // wait for others
    printf(BLUE_COLOR "%s is waiting for their friends at the exit\n" RESET_COLOR, person_x->name);
    increament_exiting_cnt(person_x->group);
    //**************** BONUS

    // printf(GREEN_COLOR "%s is leaving for dinner\n" RESET_COLOR, person_x->name);
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/group.c ####################//

#include "group.h"

//********************* BONUS PART
void increament_group_exit_cnt()
{
    pthread_mutex_lock(&num_exited_group_lock);
    num_exited_groups++;
    pthread_mutex_unlock(&num_exited_group_lock);
}

void increament_exiting_cnt(int group_number)
{
    pthread_mutex_lock(&group_lock);
    pthread_mutex_lock(&group_list[group_number].wait_exit_lock);
    group_list[group_number].num_people_waiting_at_exit++;
    pthread_cond_signal(&group_list[group_number].wait_exit_cond);
    pthread_mutex_unlock(&group_list[group_number].wait_exit_lock);
    pthread_mutex_unlock(&group_lock);
}

void *simulate_group(void *arg)
{
    group *group_x = (group *)arg;
    pthread_mutex_lock(&stimer_lock);
    while (stimer == -1)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);
    pthread_mutex_lock(&group_x->wait_exit_lock);
    while (group_x->num_people_waiting_at_exit != group_x->number_of_persons)
    {
        // printf("group %d has %d people at exit\n",group_x->number+1,group_x->num_people_waiting_at_exit);
        pthread_cond_wait(&group_x->wait_exit_cond, &group_x->wait_exit_lock);
    }
    pthread_mutex_unlock(&group_x->wait_exit_lock);
    printf(BLUE_COLOR "Group %d leaving for dinner\n" RESET_COLOR, group_x->number + 1);
    increament_group_exit_cnt();
}

//************ BONUS
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/stimer.c ####################//

#include "stimer.h"
#include <signal.h>

void increament_timer(int arg)
{
    pthread_mutex_lock(&stimer_lock);
    stimer++;
    pthread_cond_broadcast(&stimer_cond);
    pthread_mutex_unlock(&stimer_lock);
    // printf("timer  = %d\n", stimer);
}

void simulate_timer()
{
    signal(SIGALRM, increament_timer);
    while (num_exited_groups != NUMBER_OF_GROUPS)
    {
        alarm(1);
        pause();
    }
    printf(CYAN_COLOR "All groups have exited the stadium...\n" RESET_COLOR);
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/user.c ####################//

#include "user.h"

int encode_zone(char ch)
{
    if (ch == 'H')
    {
        return ZONE_H;
    }
    else if (ch == 'A')
    {
        return ZONE_A;
    }
    else if (ch == 'N')
    {
        return ZONE_N;
    }
}

void init_all_threads()
{
    int max_time = 0;
    home_team_score = 0;
    away_team_score = 0;
    num_exited_groups = 0;
    // printf("--\n");
    scanf("%d %d %d", &zone_H_limit, &zone_A_limit, &zone_N_limit);
    // printf("%d %d %d", zone_H_limit,zone_A_limit, zone_N_limit);
    //  printf("--\n");
    scanf("%d", &SPECTATING_TIME_X);
    scanf("%d", &NUMBER_OF_GROUPS);

    // printf("groups = %d\n", NUMBER_OF_GROUPS);
    //  printf("--alloc\n");

    group_list = (group *)malloc(sizeof(group) * NUMBER_OF_GROUPS);
    //   sleep(5);
    char ch;
    for (int i = 0; i < NUMBER_OF_GROUPS; i++)
    {
        scanf("%d", &group_list[i].number_of_persons);
        // printf("i = %d number of person = %d\n", i, group_list[i].number_of_persons);
        group_list[i].people = (person *)malloc(sizeof(person) * group_list[i].number_of_persons);

        //******************* BONUS PART
        group_list[i].num_people_waiting_at_exit = 0;
        pthread_mutex_init(&group_list[i].wait_exit_lock, NULL);
        pthread_cond_init(&group_list[i].wait_exit_cond, NULL);
        group_list[i].number = i;
        //******************** BONUS

        for (int j = 0; j < group_list[i].number_of_persons; j++)
        {
            pthread_mutex_init(&group_list[i].people->zone_lock, NULL);
            pthread_cond_init(&group_list[i].people->zone_cond, NULL);
            group_list[i].people[j].zone_x = 'X';
            group_list[i].people[j].at_exit_gate = false;
            group_list[i].people[j].group = i;
            // printf("done-%d\n", j);
            scanf("%s %c %d %d %d", group_list[i].people[j].name, &ch, &group_list[i].people[j].time, &group_list[i].people[j].patience, &group_list[i].people[j].num_goals_to_enrage);
            // printf("%s %c %d %d %d\n", group_list[i].people[j].name, ch, group_list[i].people[j].time, group_list[i].people[j].patience, group_list[i].people[j].num_goals_to_enrage);
            group_list[i].people[j].fan_type = encode_zone(ch);
            pthread_create(&group_list[i].people[j].tid, NULL, simulate_person, &group_list[i].people[j]);
            if (group_list[i].people[j].time > max_time)
            {
                max_time = group_list[i].people[j].time;
            }
        }
        // BONUS PART
        pthread_create(&group_list[i].tid, NULL, simulate_group, &group_list[i]);
        // BONUS
    }
    scanf("%d\n", &number_of_goal_chances);
    //  printf("num  goals = %d\n",number_of_goal_chances);
    goal_list = (goal *)malloc(sizeof(goal) * number_of_goal_chances);
    for (int i = 0; i < number_of_goal_chances; i++)
    {
        scanf("%c %d %f\n", &goal_list[i].team, &goal_list[i].time, &goal_list[i].probability);
        //  printf("%c %d %f\n", goal_list[i].team, goal_list[i].time, goal_list[i].probability);
        // simulate goals
        pthread_create(&goal_list[i].tid, NULL, simulate_goal, &goal_list[i]);
    }
    // printf("done-\n");
    simulate_timer(); // last persons arrival time and should be  chosen by cpu to run in +5 sec
    printf(GREEN_COLOR "exiting...\n" RESET_COLOR);
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/goal.h ####################//

#ifndef __GOAL_H__
#define __GOAL_H__

#include "user.h"

#define GOAL_PROBABILITY 0.5

struct goal
{
    char team;
    int time;
    float probability;
    pthread_t tid;
};

typedef struct goal goal;

extern int number_of_goal_chances;
goal *goal_list;

extern int home_team_score;
extern int away_team_score;

void *simulate_goal(void *arg);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/person.h ####################//

#ifndef __PERSON_H_
#define __PERSON_H_

#include "user.h"

struct person
{
    char name[30];
    int time;
    int group;
    int fan_type;
    int patience;
    int num_goals_to_enrage;
    pthread_t tid;
    char zone_x;
    pthread_mutex_t zone_lock;
    pthread_cond_t zone_cond;
    clock_t start;
    clock_t end;
    bool got_seat;
    bool at_exit_gate;
    bool in_stadium;
};

typedef struct person person;

enum
{
    HOME_FAN,
    AWAY_FAN,
    NEUTRAL_FAN
};

void *simulate_person(void *arg);

#endif
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/goal.c ####################//

#include "goal.h"

void *simulate_goal(void *arg)
{
    goal *goal_x = (goal *)arg;
    pthread_mutex_lock(&stimer_lock);
    while (stimer != goal_x->time)
    {
        pthread_cond_wait(&stimer_cond, &stimer_lock);
    }
    pthread_mutex_unlock(&stimer_lock);

    if (goal_x->probability >= GOAL_PROBABILITY)
    {
        if (goal_x->team == 'H')
        {
            home_team_score++;
            printf("Team %c has scored their goal number %d\n", goal_x->team, home_team_score);
        }
        else
        { // goal_x->team == 'A'
            away_team_score++;
            printf("Team %c has scored their goal number %d\n", goal_x->team, away_team_score);
        }

        // check for enraged spectators
        if (home_team_score > away_team_score)
        {
            int difference = home_team_score - away_team_score;
            for (int i = 0; i < NUMBER_OF_GROUPS; i++)
            {
                for (int j = 0; j < group_list[i].number_of_persons; j++)
                {
                    if (group_list[i].people[j].in_stadium && group_list[i].people[j].fan_type == AWAY_FAN && difference >= group_list[i].people[j].num_goals_to_enrage)
                    {
                        group_list[i].people[j].at_exit_gate = true;
                        printf(GREEN_COLOR "%s is leaving due to bad performance of his team\n" RESET_COLOR, group_list[i].people[j].name);
                    }
                }
            }
        }
        else if (home_team_score < away_team_score)
        {
            int difference = away_team_score - home_team_score;
            for (int i = 0; i < NUMBER_OF_GROUPS; i++)
            {
                for (int j = 0; j < group_list[i].number_of_persons; j++)
                {
                    if (group_list[i].people[j].in_stadium && group_list[i].people[j].fan_type == HOME_FAN && difference >= group_list[i].people[j].num_goals_to_enrage)
                    {
                        group_list[i].people[j].at_exit_gate = true;
                        printf(GREEN_COLOR "%s is leaving due to bad performance of his team\n" RESET_COLOR, group_list[i].people[j].name);
                    }
                }
            }
        }
    }
    else
    {
        printf("Team %c missed the chance to score their goal number %d\n", goal_x->team, (goal_x->team == 'H' ? home_team_score : away_team_score) + 1);
    }
}
//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/zone.h ####################//

#ifndef _ZONE_H_
#define _ZONE_H_

#include "user.h"
#include "person.h"

extern int zone_H_limit;
extern int zone_A_limit;
extern int zone_N_limit;

extern int num_persons_zone_H;
extern pthread_mutex_t zone_H_cnt_lock;
extern pthread_cond_t zone_H_cnt_cond ;

extern int num_persons_zone_A;
extern pthread_mutex_t zone_A_cnt_lock;
extern pthread_cond_t zone_A_cnt_cond ;

extern int num_persons_zone_N;
extern pthread_mutex_t zone_N_cnt_lock;
extern pthread_cond_t zone_N_cnt_cond ;

enum
{
    ZONE_H,
    ZONE_A,
    ZONE_N
};
typedef struct person person;
void increament_zone_cnt(person *p);
void decreament_zone_cnt(person *p);

#endif

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/zone.c ####################//

#include "zone.h"
#include "person.h"

void increament_zone_cnt(person *p) // use first come first serve
{
    // printf("--%c--\n",p->zone_x);
    pthread_mutex_lock(&zone_lock);
    pthread_mutex_lock(&p->zone_lock);
    p->end = stimer;
    switch (p->fan_type)
    {
    case HOME_FAN:
        if (num_persons_zone_H < zone_H_limit)
        {
            p->zone_x = 'H';
            num_persons_zone_H++;
        }
        else if (num_persons_zone_N < zone_N_limit)
        {
            p->zone_x = 'N';
            num_persons_zone_N++;
        }
        break;
    case AWAY_FAN:
        if (num_persons_zone_A < zone_A_limit)
        {
            p->zone_x = 'A';
            num_persons_zone_A++;
        }
        break;
    case NEUTRAL_FAN:
        if (num_persons_zone_H < zone_H_limit)
        {
            p->zone_x = 'H';
            num_persons_zone_H++;
        }
        else if (num_persons_zone_A < zone_A_limit)
        {
            p->zone_x = 'A';
            num_persons_zone_A++;
        }
        else if (num_persons_zone_N < zone_N_limit)
        {
            p->zone_x = 'N';
            num_persons_zone_N++;
        }
        break;
    }
    // sleep(3);

    if (p->zone_x != 'X' && (p->end - p->start) <= p->patience)
    {
        p->got_seat = true;
        printf(PINK_COLOR "%s has got a seat in zone %c\n" RESET_COLOR, p->name, p->zone_x);
    }
    else
    {
        if (p->zone_x == 'H')
        {
            num_persons_zone_H--;
        }
        else if (p->zone_x == 'A')
        {
            num_persons_zone_A--;
        }
        else if (p->zone_x == 'N')
        {
            num_persons_zone_N--;
        }
        printf(PINK_COLOR "%s could not get a seat\n" RESET_COLOR, p->name);
    }
    pthread_mutex_unlock(&p->zone_lock);
    pthread_mutex_unlock(&zone_lock);
}

void decreament_zone_cnt(person *p) // use first come first serve
{
    pthread_mutex_lock(&zone_lock);

    switch (p->zone_x)
    {
    case 'H':
        if (num_persons_zone_H < zone_H_limit)
        {
            num_persons_zone_H--;
        }
        break;
    case 'A':
        if (num_persons_zone_A < zone_A_limit)
        {
            num_persons_zone_A--;
        }
        break;
    case 'N':
        if (num_persons_zone_N < zone_N_limit)
        {
            num_persons_zone_N--;
        }
        break;
    }
    pthread_mutex_unlock(&zone_lock);
}

//###########FILE CHANGE ./main_folder/Miryala Narayana Reddy_305795_assignsubmission_file_/q2/user.h ####################//

#ifndef __USER_H__
#define __USER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>


#include "stimer.h"
#include "person.h"
#include "zone.h"
#include "group.h"
#include "goal.h"

#define RED_COLOR "\x1b[31m"
#define GREEN_COLOR "\x1b[32m"
#define YELLOW_COLOR "\x1b[33m"
#define BLUE_COLOR "\x1b[34m"
#define PINK_COLOR "\x1b[35m"
#define CYAN_COLOR "\x1b[36m"
#define RESET_COLOR "\x1b[0m"

extern int SPECTATING_TIME_X;
extern int NUMBER_OF_GROUPS;
extern int stimer;
extern pthread_mutex_t stimer_lock;
extern pthread_cond_t stimer_cond;
extern int number_of_groups;

extern int zone_H_limit;
extern int zone_A_limit;
extern int zone_N_limit;

extern int num_persons_zone_H;
extern int num_persons_zone_A;
extern int num_persons_zone_N;

extern int number_of_goal_chances;

extern int home_team_score;
extern int away_team_score;
extern int num_exited_groups;

extern pthread_mutex_t num_exited_group_lock ;
extern pthread_cond_t num_exited_group_cond ;
extern pthread_mutex_t zone_lock ;
extern pthread_cond_t zone_cond;
extern pthread_mutex_t group_lock ;
extern pthread_cond_t group_cond ;

void init_all_threads();

#endif
