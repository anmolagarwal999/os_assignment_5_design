
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/semaphoreP.h ####################//

#include <semaphore.h>

typedef sem_t Semaphore;

Semaphore *make_semaphore(int value);

void semaphore_wait(Semaphore *semaphore);

void semaphore_signal(Semaphore *semaphore);
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/main.c ####################//

#include "utils.h"
#include "person.h"
#include "goals.h"

void input()
{
    scanf("%lld %lld %lld\n", &zones[0].capacity, &zones[1].capacity, &zones[2].capacity);
    zones[0].current = 0, zones[1].current = 0, zones[2].current = 0;
    zones[0].score = 0, zones[1].score = 0, zones[2].score = 0;
    zones[0].pos = 0, zones[1].pos = 0, zones[2].pos = 0;

    scanf("%lld\n", &X);
    scanf("%lld\n", &num_groups);

    int c = 0;
    for (int i = 0; i < num_groups; i++)
    {
        scanf("%lld", &groups[i].size);
        for (int j = 0; j < groups[i].size; j++)
        {
            scanf("%s %c %lld %lld %lld\n", persons[c].name, &persons[c].c_support, &persons[c].time_reached, &persons[c].patience, &persons[c].enragedScore);
            persons[c].groupNo = i;
            persons[c].gotSeated = 0;
            persons[c].stadiumReached = 0;
            if (persons[c].c_support == 'H')
            {
                persons[c].support = 0;
            }
            else if (persons[c].c_support == 'A')
            {
                persons[c].support = 1;
            }
            else if (persons[c].c_support == 'N')
            {
                persons[c].support = 2;
            }
            c++;
        }
    }

    scanf("%lld\n", &G);
    for (int i = 0; i < G; i++)
    {
        scanf("%s %lld %f", goals[i].team, &goals[i].time, &goals[i].probability);
        goals[i].probability = goals[i].probability * 100;
        if (strcmp(goals[i].team, "H") == 0)
        {
            goals[i].zone = 0;
        }
        else if (strcmp(goals[i].team, "A") == 0)
        {
            goals[i].zone = 1;
        }
    }

    zones1[0]='H';
    zones1[1]='A';
    zones1[2]='N';
}

void *sleepingFunc(void *arg)
{
    timer = 0;
    while (1) 
    {
        pthread_mutex_lock(&time_lock[timer + 1]);
        sleep(1);
        timer++;
        pthread_cond_broadcast(&timeC[timer]);
        pthread_mutex_lock(&time_lock[timer + 1]);
    }
}

int main()
{
    srand(time(0));
    input();

    // creating thread for timer
    pthread_create(&sleeper, NULL, sleepingFunc, NULL);

    // for H
    zoneAs = make_semaphore(zones[1].capacity);
    zoneHs = make_semaphore(zones[0].capacity);
    zoneNs = make_semaphore(zones[2].capacity);


    // sem_t zone_empty[3];
    // sem_t zone_full[3];
    // sem_t zone_mutex[3];
//     sem_init(&zone_empty[0], 0, zones[0].capacity); // CAPACITY are empty
//    // sem_init(&zone_full[0], 0, 0);                  // 0 are full
//     sem_init(&zone_mutex[0], 0, 1);                 // mutex

//     // for A
//     sem_init(&zone_empty[1], 0, zones[1].capacity); // CAPACITY are empty
//     //sem_init(&zone_full[1], 0, 0);                  // 0 are full
//     sem_init(&zone_mutex[1], 0, 1);                 // mutex

//     // for N
//     sem_init(&zone_empty[2], 0, zones[2].capacity); // CAPACITY are empty
//     //sem_init(&zone_full[2], 0, 0);                  // 0 are full
//     sem_init(&zone_mutex[2], 0, 1);                 // mutex

    for(int i = 0;i<100;i++){
        pthread_mutex_init(&person_lock[i],NULL);
    }
    for(int i = 0;i<2;i++){
        pthread_mutex_init(&zone_lock[i],NULL);
    }
    for(int i = 0;i<2;i++){
        pthread_mutex_init(&zone1_lock[i],NULL);
    }

    
    //creating thread for spectators
    int c = 0,id=0;
    for (int l = 0; l < num_groups; l++)
    {
        for (int j = 0; j < groups[l].size; j++)
        {
            if (persons[id].support == 0)
            {   
                pthread_t curr_tid1;
                struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input->id = id;
                thread_input->wantedZone = 0;
                pthread_create(&curr_tid1, NULL, personStimulater, (void *)(thread_input));
                personThreadArr[c] = curr_tid1;
                c++;

               
                pthread_t curr_tid2;
                struct thread_details *thread_input2 = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input2->id = id;
                thread_input2->wantedZone = 2;
                pthread_create(&curr_tid2, NULL, personStimulater, (void *)(thread_input2));
                personThreadArr[c] = curr_tid2;
                c++;
            }

            if (persons[id].support == 1)
            {
                
                pthread_t curr_tid1;
                struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input->id = id;
                thread_input->wantedZone = 1;
                pthread_create(&curr_tid1, NULL, personStimulater, (void *)(thread_input));
                personThreadArr[c] = curr_tid1;
                c++;
            }

            if (persons[id].support == 2)
            {
                
                //printf("id:%d 0 support:%c\n", id,persons[id].c_support);
                pthread_t curr_tid1;
                struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input->id = id;
                thread_input->wantedZone = 0;
                pthread_create(&curr_tid1, NULL, personStimulater, (void *)(thread_input));
                personThreadArr[c] = curr_tid1;
                c++;

                //printf("id:%d 1 support:%c\n", id,persons[id].c_support);
                pthread_t curr_tid2;
                struct thread_details *thread_input2 = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input2->id = id;
                thread_input2->wantedZone = 1;
                pthread_create(&curr_tid2, NULL, personStimulater, (void *)(thread_input2));
                personThreadArr[c] = curr_tid2;
                c++;

                //printf("id:%d 2 support:%c\n", id,persons[id].c_support);
                pthread_t curr_tid3;
                struct thread_details *thread_input3 = (struct thread_details *)(malloc(sizeof(struct thread_details)));
                thread_input3->id = id;
                thread_input3->wantedZone = 2;
                pthread_create(&curr_tid3, NULL, personStimulater, (void *)(thread_input3));
                personThreadArr[c] = curr_tid3;
                c++;
            }
            id++;
        }
    }

    // creating threads for goals
    for (int l = 0; l < G; l++)
    {
        pthread_t curr_tid;
        struct thread_details *thread_input = (struct thread_details *)(malloc(sizeof(struct thread_details)));
        thread_input->id = l;
        pthread_create(&curr_tid, NULL, goalStimulater, (void *)(thread_input));
        goalThreadArr[l] = curr_tid;
    }

    // ------------------------------------------------------------ -------------------- -------------------- --------------------

    for (int l = 0; l < c; l++)
    {
        pthread_join(personThreadArr[l], NULL);
    }

    for (int l = 0; l < G; l++)
    {
        pthread_join(goalThreadArr[l], NULL);
    }
}

//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/person.c ####################//

#include "person.h"
#include "utils.h"

void *personStimulater(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    int wantedZone = ((struct thread_details *)arg)->wantedZone;
    //printf("t=%lld : id reached : %d name : %s wantedZone:%d\n", timer, id, persons[id].name,wantedZone);

    stadium_reached(id, wantedZone);
    gettingSeat(id, wantedZone);
    if(persons[id].gotSeated==2){
        //printf("exiting\n");
        return NULL;
    }
    exiting(id, wantedZone);
    //printf("EXITING FOR ID:%d WANTED:%d\n",id,wantedZone);
    return NULL;
}
 
void stadium_reached(int id, int wantedZone)
{
    //printf("id:%d wantedZone:%d support:%c\n", id,wantedZone, persons[id].c_support);

    pthread_mutex_lock(&person_lock[id]);
    pthread_mutex_lock(&time_lock[persons[id].time_reached]); 
    //printf("id:%d wantedZone:%d sr:%lld\n",id,wantedZone,persons[id].stadiumReached);
    if (persons[id].stadiumReached == 0)
    {
        while (timer != persons[id].time_reached)
        {
            pthread_cond_wait(&timeC[persons[id].time_reached], &time_lock[persons[id].time_reached]);
        }
        persons[id].stadiumReached = 1;
        printf(CYAN "t=%lld : %s has reached the stadium\n" RESET, timer, persons[id].name);
    }

    pthread_mutex_unlock(&time_lock[persons[id].time_reached]);
    pthread_mutex_unlock(&person_lock[id]);
}

void gettingSeat(int id, int wantedZone)
{

    if (wantedZone == 0)
    {
        //printf("0 id : %d wanted zone :%d \n",id,wantedZone);
        pthread_mutex_lock(&zone1_lock[wantedZone]);
        semaphore_wait(zoneHs);
        if (zones[wantedZone].current < zones[wantedZone].capacity)
        {
            //printf("id:%d wantedZone:%d current:%lld capacity:%lld support:%c\n", id, wantedZone, zones[wantedZone].current, zones[wantedZone].capacity, persons[id].c_support);
            if (persons[id].gotSeated == 0)
            {
                if (timer >= persons[id].patience + persons[id].time_reached)
                {
                    persons[id].gotSeated = 2;
                    printf(YELLOW "t=%lld : %s couldn’t get a seat\n" RESET, timer, persons[id].name);
                }
                else
                {
                    persons[id].gotSeated = 1;
                    persons[id].currentZoneSeated = wantedZone;
                    zones[wantedZone].current = zones[wantedZone].current + 1;
                    persons[id].seatedTime = timer;
                    printf(GREEN "t=%lld : %s has got a seat in zone %c\n" RESET, timer, persons[id].name, zones1[wantedZone]);
                }
            }
            else
            {
                semaphore_signal(zoneHs);
            }
        }
        pthread_mutex_unlock(&zone1_lock[wantedZone]);
    }

    if (wantedZone == 1)
    {
        //printf("1 id : %d wanted zone :%d \n",id,wantedZone);
        pthread_mutex_lock(&zone1_lock[wantedZone]);
        semaphore_wait(zoneAs);
        if (zones[wantedZone].current < zones[wantedZone].capacity)
        {
            //printf("id:%d wantedZone:%d current:%lld capacity:%lld support:%c\n", id, wantedZone, zones[wantedZone].current, zones[wantedZone].capacity, persons[id].c_support);
            if (persons[id].gotSeated == 0)
            {
                if (timer >= persons[id].patience + persons[id].time_reached)
                {
                    persons[id].gotSeated = 2;
                    printf(YELLOW "t=%lld : %s couldn’t get a seat\n" RESET, timer, persons[id].name);
                }
                else
                {
                    persons[id].gotSeated = 1;
                    persons[id].currentZoneSeated = wantedZone;
                    zones[wantedZone].current = zones[wantedZone].current + 1;
                    persons[id].seatedTime = timer;
                    printf(GREEN "t=%lld : %s has got a seat in zone %c\n" RESET, timer, persons[id].name, zones1[wantedZone]);
                }
            }
            else
            {
                semaphore_signal(zoneAs);
            }
        }
        pthread_mutex_unlock(&zone1_lock[wantedZone]);
    }

    if (wantedZone == 2)
    {
        //printf("2 id : %d wanted zone :%d \n",id,wantedZone);
        pthread_mutex_lock(&zone1_lock[wantedZone]);
        semaphore_wait(zoneNs);
        if (zones[wantedZone].current < zones[wantedZone].capacity)
        {
            //printf("id:%d wantedZone:%d current:%lld capacity:%lld support:%c\n", id, wantedZone, zones[wantedZone].current, zones[wantedZone].capacity, persons[id].c_support);
            if (persons[id].gotSeated == 0)
            {
                if (timer >= persons[id].patience + persons[id].time_reached)
                {
                    persons[id].gotSeated = 2;
                    printf(YELLOW "t=%lld : %s couldn’t get a seat\n" RESET, timer, persons[id].name);
                }
                else
                {
                    persons[id].gotSeated = 1;
                    persons[id].currentZoneSeated = wantedZone;
                    zones[wantedZone].current = zones[wantedZone].current + 1;
                    persons[id].seatedTime = timer;
                    printf(GREEN "t=%lld : %s has got a seat in zone %c\n" RESET, timer, persons[id].name, zones1[wantedZone]);
                }
            }
            else
            {
                semaphore_signal(zoneNs);
            }
        }
        pthread_mutex_unlock(&zone1_lock[wantedZone]);
    }
}

void exiting(int id, int wantedZone)
{
    ll support = persons[id].support;
    ll zoneSeated = persons[id].currentZoneSeated;
    if (persons[id].support == 0 && persons[id].currentZoneSeated == wantedZone)
    {
        while (1)
        {
            if (timer >= persons[id].seatedTime + X)
            {
                persons[id].timeWatched = timer - persons[id].seatedTime;
                printf(RED "t=%lld : %s watched the match for %lld seconds and is leaving\n" RESET, timer, persons[id].name, persons[id].timeWatched);
                zones[support].current--;       
                break;
            }

            if (zones[1].score >= persons[id].enragedScore)
            {
                printf(RED "t=%lld : %s is leaving due to the bad defensive performance of his team\n" RESET, timer, persons[id].name);
                zones[support].current--; 
                break;
            }
        }
        semaphore_signal(zoneHs);
    }
    if (persons[id].support == 1 && persons[id].currentZoneSeated == wantedZone)
    {
        while (1)
        {
            if (timer >= persons[id].seatedTime + X)
            {
                persons[id].timeWatched = timer - persons[id].seatedTime;
                printf(RED "t=%lld : %s watched the match for %lld seconds and is leaving\n" RESET, timer, persons[id].name, persons[id].timeWatched);
                zones[support].current--;           
                break;
            }

            if (zones[0].score >= persons[id].enragedScore)
            {
                printf(RED "t=%lld : %s is leaving due to the bad defensive performance of his team\n" RESET, timer, persons[id].name);
                zones[support].current--;
                break;
            }
        }
        semaphore_signal(zoneAs);
    }
    if (persons[id].support == 2 && persons[id].currentZoneSeated == wantedZone)
    {
        while (1)
        {
            if (timer >= persons[id].seatedTime + X)
            {
                persons[id].timeWatched = timer - persons[id].seatedTime;
                printf(RED "t=%lld : %s watched the match for %lld seconds and is leaving\n" RESET, timer, persons[id].name, persons[id].timeWatched);
                zones[support].current--;
                break;
            }
        }
        semaphore_signal(zoneNs);
    }
}


//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/semaphoreP.c ####################//

#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "semaphoreP.h"

typedef sem_t Semaphore;

Semaphore *make_semaphore(int value){
          Semaphore *semaphore = (Semaphore *) malloc(sizeof(Semaphore));
          semaphore = sem_open("/semaphore", O_CREAT, 0644, value);
          sem_unlink("/semaphore");
          return semaphore;
}

void semaphore_wait(Semaphore *semaphore){
          sem_wait(semaphore);
}

void semaphore_signal(Semaphore *semaphore){
          sem_post(semaphore);
}
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/goals.h ####################//

#ifndef goal
#define goal

void* goalStimulater(void* arg);
void goal_scored(int id);

#endif
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/utils.h ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>
#include "semaphoreP.h"

typedef long long int ll;

ll X; // spectating time
ll num_groups;
ll G; 
ll timer;

pthread_t personThreadArr[1000];   // array of threads for each course
pthread_t goalThreadArr[1000];     // array of threads for each student
pthread_t sleeper;

Semaphore* zoneAs;
Semaphore* zoneHs;
Semaphore* zoneNs;

char zones1[3];

pthread_mutex_t position_lock[2];
pthread_mutex_t zone_lock[2];
pthread_mutex_t zone1_lock[2];
pthread_mutex_t time_lock[1000];
pthread_mutex_t person_lock[100];
pthread_cond_t timeC[1000];

struct Group{
    ll size;   // no of ppl in grp
    char names[100][100];
    char support[100];
    ll time_reached[100];
    ll patience[100];
    ll enragedScore[100];

};

struct Person{
    ll groupNo;
    char name[100];
    char c_support; // (H,A,N)
    ll time_reached;
    ll patience;
    ll enragedScore;
    
    ll stadiumReached;

    ll support;    // which team he supports(0,1,2)
    ll currentZoneSeated;
    char c_currentZoneSeated;
    ll alreadyExisted;
     
    ll gotSeated;
    ll seatedTime;
    ll timeWatched;
};

struct Zone{
    ll capacity;
    ll current;
    ll score;
    ll pos;
};

struct Goal{
    char team[20];
    ll time;
    float probability;
    ll pos;
    char position[20];
    ll zone;
};

struct thread_details
{
    int id;
    ll wantedZone;   // which zone he is trying to be in
};

struct Group groups[1000];
struct Goal goals[100];
struct Person persons[1000];
struct Zone zones[3];


#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"


#define RESET   "\x1b[0m"

//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/goals.c ####################//

#include "goals.h"
#include "utils.h"
 


void position_giver(ll id)
{
    pthread_mutex_lock(&position_lock[goals[id].zone]);
    zones[goals[id].zone].pos = zones[goals[id].zone].pos+1;
    ll pos = zones[goals[id].zone].pos;
    if (pos == 1)
    {
        strcpy(goals[id].position, "1st");
    }
    if (pos == 2)
    {
        strcpy(goals[id].position, "2nd"); 
    }
    if (pos == 3)
    {
        strcpy(goals[id].position, "3rd");   
    }
    if (pos >= 4)
    {   
        char str[100], random[100];
        strcpy(random, "th");
        sprintf(str, "%lld", pos);
        strcat(str, random);
        strcpy(goals[id].position, str);
    }
    pthread_mutex_unlock(&position_lock[goals[id].zone]);
}

void *goalStimulater(void *arg)
{
    int id = ((struct thread_details *)arg)->id;
    float r = (rand() % 100);
    
    pthread_mutex_lock(&time_lock[goals[id].time]);
    while (timer != goals[id].time)
    {
        pthread_cond_wait(&timeC[goals[id].time], &time_lock[goals[id].time]);
    }
    position_giver(id);
    if (r < goals[id].probability)
    {
        printf(MAGENTA "t=%lld : Team %s has scored their %s goal\n" RESET, timer, goals[id].team, goals[id].position);
        zones[goals[id].zone].score++;
    }
    else
    {
        printf(MAGENTA "t=%lld : Team %s missed the chance to score their %s goal\n" RESET, timer, goals[id].team, goals[id].position);
    }
    pthread_mutex_unlock(&time_lock[goals[id].time]);

    return NULL;
}
//###########FILE CHANGE ./main_folder/Khush Patel_305944_assignsubmission_file_/2020101119_assignment_5/q2/person.h ####################//

#ifndef person
#define person

void* personStimulater(void* arg);
void stadium_reached(int id,int wantedZone);
void gettingSeat(int id,int wantedZone);
void exiting(int id,int wantedZone);

#endif