
//###########FILE CHANGE ./main_folder/AASISH ESWAR PANGANAMALA_305812_assignsubmission_file_/2020101049/q2/realq.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>
struct goals
{
    char team[2];
    int time;
    float prob;
};
int X;
#define YS 1
int actual_time = 1;
#define NE 0
int A_GOAL = 0, H_GOAL = 0;

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
struct person
{
    char name[256];
    char zone[2];
    int time;
    int p_time;
    int num_goals;
    int arrived;
    char got_seat_in_zone[2];
    int waiting;
};
#define MINI 5
int total_number_of_people;
#define MAX_LEN 20
int H_capacity, A_capacity, N_capacity, g_scoring;
int inc=0;
struct person people[1000];
int dec=0;
struct goals chances[1000];
int min =0;
bool seated[1000];
int noof_steps = 0;
sem_t seming[1000];
int total_count =0;
sem_t waiting[1000];
int loop_count;
int ended = 0;
int condition =0;
sem_t end;
int ender= 0;
int get_value(float p)
{
    float z;
    z = p-1;
    double val = (double)rand() / RAND_MAX;
    z++;
    return val < p;
}
void *time_function()
{
    int timers =0;
    while (1)
    {
        ender = ender +1;
        if(ended == 1 + total_number_of_people)
        {
            ender++;
            return NULL;
        }
        actual_time++;
        sleep(1);
        condition++;
    }
    timers++;
}
void rage_quit(char x)
{
    condition =0;
    loop_count =0;
    for (int i = 0; i < total_number_of_people; i++)
    {
        ender= 0;
        loop_count++;
        if (people[i].arrived == 1 && people[i].waiting == 0 && people[i].num_goals != -1)
        {
            condition++;
            if (x != people[i].zone[0])
            {
                condition++;
                if (x == 'A' && A_GOAL >= people[i].num_goals)
                {
                    total_count = loop_count + NE;
                    sem_post(&waiting[i]);
                    loop_count = (YS-1);
                }
                if (x == 'H' && H_GOAL >= people[i].num_goals)
                {
                    loop_count = loop_count + YS;
                    sem_post(&waiting[i]);
                    condition = (NE+1)*(YS-1);
                }
                ender++;
            }
            ender++;
        }
    }
}
void *person_function(void *ptr)
{
    int timers =0;
    int index1 = *(int *)ptr;
    ender= 0;
    min=0;
    while (actual_time < people[index1].time)
    {
        timers++;
        sleep(1);
    }
    ender =ender+2;
    min++;
    printf(ANSI_COLOR_RED "t = %d: %s has reached the stadium\n"ANSI_COLOR_RESET, actual_time, people[index1].name);
    condition=0;
    people[index1].arrived = 1;
    loop_count=0;
    people[index1].waiting = 1;
    total_count=0;
    if (people[index1].zone[0] == 'A')
    {
        condition++;
        if (A_capacity > 0)
        {
            ender= 0;
            A_capacity--;
            seated[index1] = true;
            total_count = loop_count + NE;
            people[index1].got_seat_in_zone[0] = 'A';
            loop_count = loop_count + YS;
            people[index1].waiting = 0;
            noof_steps = noof_steps + loop_count;
            printf(ANSI_COLOR_GREEN "t = %d: %s has got a seat in zone A\n" ANSI_COLOR_RESET, actual_time, people[index1].name);
            ender++;
        }
        else
        {
            condition =0;
            struct timespec timingspec;
            timers =0;
            clock_gettime(CLOCK_REALTIME, &timingspec);
            noof_steps = noof_steps + loop_count;
            timingspec.tv_sec += people[index1].p_time;
            condition =0;
            if (sem_timedwait(&seming[index1], &timingspec) == -1)
            {
                condition++;
                printf("t = %d: %s couldn’t get a seat\n", actual_time, people[index1].name);
                total_count = loop_count + NE;
                people[index1].waiting = 0;
                ender = ender +1;
                ended++;
                return NULL;
            }
            else
            {
                condition++;
                people[index1].waiting = 0;
                total_count = (NE);
            }
            condition = (NE+1)*(YS-1);
        }
    }
    else
    {
        condition =0;
        if (people[index1].zone[0] == 'N')
        {
            inc=0;
            condition++;
            if (N_capacity > 0)
            {
                ender=0;
                condition = condition + inc;
                N_capacity--;
                seated[index1] = true;
                loop_count = (YS-1);
                people[index1].got_seat_in_zone[0] = 'N';
                condition = (NE+1)*(YS-1);
                people[index1].waiting = 0;
                noof_steps = noof_steps + loop_count;
                printf("t = %d: %s has got a seat in zone N\n", actual_time, people[index1].name);
                ender++;
            }
            else
            {
                total_count++;
                if (A_capacity > 0)
                {
                    timers=0;
                    A_capacity--;
                    condition++;
                    seated[index1] = true;
                    noof_steps++;
                    people[index1].got_seat_in_zone[0] = 'A';
                    total_count = loop_count + NE;
                    people[index1].waiting = 0;
                    noof_steps = noof_steps + loop_count;
                    printf("t = %d: %s has got a seat in zone A\n", actual_time, people[index1].name);
                    ender = ender +1;
                }
                else if (H_capacity > 0)
                {
                    condition=0;
                    H_capacity--;
                    condition++;
                    seated[index1] = true;
                    noof_steps = noof_steps + loop_count;
                    people[index1].got_seat_in_zone[0] = 'H';
                    total_count = loop_count + NE;
                    people[index1].waiting = 0;
                    timers++;
                    printf("t = %d: %s has got a seat in zone H\n", actual_time, people[index1].name);
                }
                else
                {
                    timers=0;
                    struct timespec timingspec;
                    noof_steps = noof_steps + loop_count;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    ender = ender +1;
                    timingspec.tv_sec += people[index1].p_time;
                    timers++;
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        min=0;
                        printf("t = %d:%s couldn’t get a seat\n", actual_time, people[index1].name);
                        total_count = (NE);
                        people[index1].waiting = 0;
                        condition = condition + min;
                        return NULL;
                        ended++;
                    }
                    else
                    {
                        condition++;
                        people[index1].waiting = 0;
                        ender = ender + condition;
                    }
                    timers =0;
                }
            }
        }
        else
        {
            condition=0;
            if (H_capacity > 0)
            {
                ender=0;
                H_capacity--;
                ender =ender+condition;
                seated[index1] = true;
                condition++;
                people[index1].got_seat_in_zone[0] = 'H';
                noof_steps = noof_steps + loop_count;
                people[index1].waiting = 0;
                total_count++;
                printf("t = %d: %s has got a seat in zone H\n", actual_time, people[index1].name);
            }
            else
            {
                noof_steps = 0;
                if (N_capacity > 0)
                {
                    dec=0;
                    N_capacity--;    
                    condition = condition + dec;
                    seated[index1] = true;
                    loop_count = loop_count + YS;
                    people[index1].got_seat_in_zone[0] = 'N';
                    noof_steps = noof_steps + loop_count;
                    people[index1].waiting = 0;
                    loop_count++;
                    printf("t = %d: %s has got a seat in zone N\n", actual_time, people[index1].name);
                    ender++;
                }
                else
                {
                    timers =0;
                    struct timespec timingspec;
                    timers++;
                    clock_gettime(CLOCK_REALTIME, &timingspec);
                    condition = (NE+1)*(YS-1);
                    timingspec.tv_sec += people[index1].p_time;
                    noof_steps = noof_steps + loop_count;
                    printf("%s is waiting for seat\n", people[index1].name);
                    total_count = loop_count + NE;
                    int valing = sem_timedwait(&seming[index1],&timingspec);
                    loop_count = loop_count + YS;
                    if (sem_timedwait(&seming[index1], &timingspec) == -1)
                    {
                        total_count = (NE);
                        printf("t = %d:%s couldn’t get a seat\n", actual_time, people[index1].name);
                        noof_steps = (YS-1);
                        people[index1].waiting = 0;
                        ender = ender +1;
                        ended++;
                        return NULL;
                    }
                    else
                    {
                        timers++;
                        people[index1].waiting = 0;
                    }
                    
                }
            }
        }
    }
    timers =0;
    struct timespec timedspec;
    timers++;
    clock_gettime(CLOCK_REALTIME, &timedspec);
    noof_steps++;
    timedspec.tv_sec += X;
    if (sem_timedwait(&waiting[index1], &timedspec) != -1)
    {
        total_count++;
        printf("t = %d: %s is leaving due to bad performance of his team\n", actual_time, people[index1].name);
        condition++;
    }
    else
    {
        condition++;
        printf("t = %d: %s watched the match for %d seconds and is leaving\n", actual_time, people[index1].name, X);
    }
    noof_steps = noof_steps + loop_count;
    char c = people[index1].got_seat_in_zone[0];
    total_count = loop_count + NE;
    int flag = 0;
    loop_count = (YS-1);
    for (int i = 0; i < total_number_of_people; i++)
    {
        ender= 0;
        if (i == index1)
        {
            ender++;
            continue;
        }
        min=0;
        if (people[i].arrived == 1 && people[i].waiting == 1)
        {
            ender = ender + min;
            if (c == 'A')
            {
                total_count++;
                if (people[i].zone[0] == 'A')
                {
                    loop_count++;
                    printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                    total_count = loop_count + NE;
                    people[i].got_seat_in_zone[0] = 'A';
                    noof_steps = noof_steps + loop_count;
                    sem_post(&seming[i]);
                    condition = condition + inc;
                    people[i].waiting = 0;
                    loop_count = loop_count + YS;
                    flag = 1;
                    ender++;
                    break;
                }
                if (people[i].zone[0] == 'N')
                {
                    printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                    loop_count = loop_count + YS;
                    people[i].got_seat_in_zone[0] = 'A';
                    total_count = loop_count + NE;
                    people[i].waiting = 0;
                    noof_steps++;
                    sem_post(&seming[i]);
                    ender++;
                    flag = 1;
                    ender = ender + min;
                    break;
                }
            }
            else
            {
                timers =0;
                if (c == 'N')
                {
                    condition =0;
                    if (people[i].zone[0] == 'H')
                    {
                        loop_count =0;
                        printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                        noof_steps = noof_steps + loop_count;
                        people[i].got_seat_in_zone[0] = 'N';
                        total_count++;
                        people[i].waiting = 0;
                        total_count = loop_count + NE;
                        sem_post(&seming[i]);
                        total_count = (NE);
                        flag = 1;
                        ender++;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        condition = condition + inc;
                        printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                        noof_steps = noof_steps + loop_count;
                        people[i].got_seat_in_zone[0] = 'N';
                        ender = ender + min;
                        people[i].waiting = 0;
                        noof_steps = noof_steps + loop_count;
                        loop_count = loop_count + YS;
                        sem_post(&seming[i]);
                        flag = 1;
                        min++;
                        break;
                    }
                }
                else
                {
                    condition =0;
                    if (people[i].zone[0] == 'H')
                    {
                        total_count = loop_count + NE;
                        printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                        noof_steps = noof_steps + loop_count;
                        people[i].got_seat_in_zone[0] = 'H';
                        ender = ender + min;
                        people[i].waiting = 0;
                        loop_count = loop_count + YS;
                        sem_post(&seming[i]);
                        ender++;
                        flag = 1;
                        break;
                    }
                    if (people[i].zone[0] == 'N')
                    {
                        noof_steps = 0;
                        printf("t = %d: %s has got a seat in zone %c\n", actual_time, people[i].name, c);
                        loop_count = loop_count + YS;
                        people[i].got_seat_in_zone[0] = 'H';
                        total_count = loop_count + NE;
                        people[i].waiting = 0;
                        timers =0;
                        sem_post(&seming[i]);
                        total_count++;
                        flag = 1;
                        min++;
                        break;
                    }
                }
            }
        }
    }
    if(flag == 0)
    {
        dec=0;
        if(c == 'A')
        {
            inc=0;
            A_capacity++;
            condition++;
        }
        else
        {
            total_count++;
            if(c == 'N')
            {
                timers =0;
                N_capacity++;
            }
            else
            {
                ender= 0;
                H_capacity++;
                ender = ender +1;
            }
        }
        timers++;
    }
    ended++;
    ender = ender +1;
}
void *goal_timing()
{
    int timers =0;
    int count = 0;
    for (int i = 0; i < g_scoring; i++)
    {   
        loop_count =0;
        while (actual_time < chances[i].time)
        {
            loop_count++;
            sleep(1);
        }
        ender = ender + min;
        int x = get_value(chances[i].prob);
        noof_steps = noof_steps + loop_count;
        if (chances[i].team[0] == 'A')
        {
            A_GOAL += x;
            ender++;
            timers++;
            if (x == 1)
            {
                condition = (NE+1)*(YS-1);
                printf("t = %d: Team A has scored their %d goal\n", actual_time, A_GOAL);
                loop_count = (YS-1);
                char x0 = 'A';
                total_count = (NE);
                rage_quit(x0);
                noof_steps = (YS-1);
                continue;
            }
            else
            {
                noof_steps = noof_steps + loop_count;
                printf("t = %d: Team A missed the chance to score their %d goal\n", actual_time, A_GOAL + 1);
                timers++;
                continue;
            }
        }
        else
        {
            condition=0;
            H_GOAL += x;
            timers =0;
            if (x == 1)
            {
                condition = condition + inc;
                printf("t = %d: Team H has scored their %d goal\n", actual_time, H_GOAL);
                total_count = loop_count + NE;
                char x0 = 'H';
                condition = condition + inc;
                rage_quit(x0);
                ender = ender + min;
                continue;
            }
            else
            {
                total_count = loop_count + NE;
                printf("t = %d: Team H missed the chance to score their %d goal\n", actual_time, H_GOAL + 1);
                ender++;
                continue;
            }
        }
        timers++;
        sleep(1);
    }
    ended++;
    ender++;
    return NULL;
}
int main()
{
    int timers =0;
    min=0;
    pthread_mutex_t locks[1000];
    condition =0;
    loop_count =0;
    for (int i = 0; i < 1000; i++)
    {
        loop_count = loop_count + YS;
        pthread_mutex_init(&locks[i], NULL);
    }
    srand(time(0));
    timers++;
    scanf("%d %d %d", &H_capacity, &A_capacity, &N_capacity);
    loop_count = loop_count + YS;
    scanf("%d", &X);
    ender = ender +1;
    int groups;
    inc=0;
    scanf("%d", &groups);
    dec=0;
    int total_no_of_people = 0;
    loop_count =0;
    int count = 0;
    timers =0;
    for (int i = 0; i < groups; i++)
    {
        total_count = loop_count + NE;
        int number_of_people;
        loop_count++;
        scanf("%d", &number_of_people);
        condition++;
        for (int j = 0; j < number_of_people; j++)
        {
            total_count++;
            scanf("%s %s %d %d %d", people[count].name, people[count].zone, &people[count].time, &people[count].p_time, &people[count].num_goals);
            ender = ender + min;
            count++;
            ender = ender +1;
            total_no_of_people++;
        }
        ender++;
        timers++;
    }
    noof_steps = noof_steps + loop_count;
    total_number_of_people = total_no_of_people;
    ender = ender + min;
    scanf("%d", &g_scoring);
    timers++;
    for (int i = 0; i < g_scoring; i++)
    {
        noof_steps = noof_steps + loop_count;
        scanf("%s %d %f", chances[i].team, &chances[i].time, &chances[i].prob);
    }
    loop_count = loop_count + YS;
    pthread_t persons[total_no_of_people];
    loop_count =0;
    for (int i = 0; i < total_no_of_people; i++)
    {
        loop_count++;
        pthread_create(&persons[i], NULL, person_function, &i);
        timers++;
        usleep(100);
        ender = ender +1;
    }
    condition = condition + inc;
    pthread_t goals_scored, timing;
    noof_steps = noof_steps + loop_count;
    pthread_create(&goals_scored, NULL, goal_timing, NULL);
    total_count = loop_count + NE;
    pthread_create(&timing, NULL, time_function, NULL);
    loop_count = loop_count + YS;
    for (int i = 0; i < total_no_of_people; i++)
    {
        condition = condition + inc;
        pthread_join(persons[i], NULL);
        loop_count++;
    }
    noof_steps = noof_steps + loop_count;
    pthread_join(goals_scored, NULL);
    condition = condition + inc;
    pthread_join(timing, NULL);
    loop_count =0;
    for (int i = 0; i < 1000; i++)
    {
        loop_count = loop_count + YS;
        pthread_mutex_destroy(&locks[i]);
        loop_count++;
    }
}