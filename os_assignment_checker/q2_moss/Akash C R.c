
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/people.h ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "colors.h"
#include "datatypes.h"
#include "states.h"

void * execute_people(void * people_element);

void * execute_seats();

//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/people.c ####################//

#include "people.h"


extern pthread_mutex_t time_lock;
extern pthread_cond_t time_cond;

void * execute_people(void * people_element)
{	
	people * peeps = (people *)people_element;

	pthread_mutex_init(&peeps->s_lock, NULL);
	pthread_cond_init(&peeps->s_cond, NULL);

	pthread_mutex_lock(&time_lock);
	while(timer < peeps->reaching_time)
	{
		pthread_cond_wait(&time_cond, &time_lock);
	}
	//pthread_mutex_unlock(&time);

	//pthread_mutex_lock(&peeps->s_lock);
	peeps->status = P_RCHD;
	//pthread_mutex_unlock(&peeps->s_lock);

	printf(Reached);
	printf("Person %s has reached the stadium\n", peeps->name);
	printf(RESET);

	pthread_mutex_unlock(&time_lock);

	int x = 1;
	while(1)
	{
		pthread_mutex_lock(&peeps->s_lock);

		if(peeps->status == P_RCHD)
		{
			pthread_cond_wait(&peeps->s_cond, &peeps->s_lock);
		}
		else if(peeps->status == P_GS)
		{
			printf(GotSeat);
			if(x)//ye baar baar print ho rha tha isliye extra var
			printf("Person %s has got a seat in zone %c\n", peeps->name, peeps->seatzone);
			printf(RESET);
			x = 0;
			pthread_cond_wait(&peeps->s_cond, &peeps->s_lock);

		}
		else if(peeps->status == P_CGS)
		{
			printf(DidNotGetSeat);
			printf("Person %s could not get a seat\n", peeps->name);
			printf(RESET);	
			peeps->status = P_WAIT;
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);

		}
		else if(peeps->status == P_WH)
		{
			printf(WatchedHappy);
			printf("Person %s watched the match for %d seconds and is leaving\n", peeps->name, spect_time);
			printf(RESET);
			peeps->status = P_WAIT;
			pthread_mutex_lock(&lock_seat);
			filled_seats--;
			if(peeps->seatzone == 'H')
			{
				homezone.seats_filled--;
				peeps->seatzone = 0;
			}
			else if(peeps->seatzone == 'N')
			{
				neutralzone.seats_filled--;
				peeps->seatzone = 0;
			}
			else
			{
				awayzone.seats_filled--;
				peeps->seatzone = 0;
			}
			pthread_mutex_unlock(&lock_seat);
			pthread_cond_signal(&cond_seat);
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);

		}
		else if(peeps->status == P_WUH)
		{
			printf(WatchedSad);
			printf("Person %s is leaving due to the bad defensive performance of his team\n", peeps->name);
			printf(RESET);
			peeps->status = P_WAIT;
			pthread_mutex_lock(&lock_seat);
			filled_seats--;
			if(peeps->seatzone == 'H')
			{
				homezone.seats_filled--;
				peeps->seatzone = 0;
			}
			else if(peeps->seatzone == 'N')
			{
				neutralzone.seats_filled--;
				peeps->seatzone = 0;
			}
			else
			{
				awayzone.seats_filled--;
				peeps->seatzone = 0;
			}
			pthread_mutex_unlock(&lock_seat);
			pthread_cond_signal(&cond_seat);
			printf(WaitingFriends);
			printf("Person %s is waiting for their friends at the exit\n", peeps->name);
			printf(RESET);


		}
		else if(peeps->status == P_WAIT)
		{			
			pthread_mutex_unlock(&peeps->s_lock);
			peeps->exit = 1;
			pthread_barrier_wait(&ptrtobarriers[peeps->group-1]);
			//group-1 bcoz its 1 based indexing;
			if(pthread_mutex_trylock(&ptrtogrpprint[peeps->group-1]) == 0)
			{
				printf(GroupLeave);
				printf("Group %d is leaving for dinner\n", peeps->group);
				printf(RESET);
			}
			break;
		}




		pthread_mutex_unlock(&peeps->s_lock);
		usleep(50000);
	}

	pthread_mutex_destroy(&peeps->s_lock);
	pthread_cond_destroy(&peeps->s_cond);

}


void * execute_seats()
{
	while(1)
	{	
		pthread_mutex_lock(&lock_seat);
		if(filled_seats == total_seats)
			pthread_cond_wait(&cond_seat, &lock_seat);

		int send = 0;
		for(int i = 0; i < total_people; i++)
		{
			if(arr_people[i].exit == 1)continue;
			send = 0;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_RCHD)
			{	
				send = 1;
				if(arr_people[i].team[0] == 'H')
				{
					if(homezone.seats_filled < homezone.total_seats)
					{
						homezone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'H';
						filled_seats++;
					}
					else if(neutralzone.seats_filled < neutralzone.total_seats)
					{
						neutralzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'N';
						filled_seats++;
					}
				}
				else if(arr_people[i].team[0] == 'A')
				{
					if(awayzone.seats_filled < awayzone.total_seats)
					{
						awayzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'A';
						filled_seats++;
					}
				}
				else
				{
					if(neutralzone.seats_filled < neutralzone.total_seats)
					{
						neutralzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'N';
						filled_seats++;
					}
					else if(homezone.seats_filled < homezone.total_seats)
					{
						homezone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'H';
						filled_seats++;
					}
					else if(awayzone.seats_filled < awayzone.total_seats)
					{
						awayzone.seats_filled++;
						arr_people[i].status = P_GS;
						arr_people[i].seatzone = 'A';
						filled_seats++;
					}
				}
			}
			pthread_mutex_unlock(&arr_people[i].s_lock);
			if(send)
				pthread_cond_signal(&arr_people[i].s_cond);

			if(filled_seats == total_seats)break;
		}
		
		pthread_mutex_unlock(&lock_seat);
		usleep(50000);
	}
	
}


//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/states.h ####################//

#ifndef _STATES
#define _STATES

// Different states of the person

#define P_RCHD	6
#define P_GS	1
#define P_CGS	2
#define P_WH	3
#define P_WUH	4
#define P_WAIT	5



#endif
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/main.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "colors.h"
#include "datatypes.h"
#include "states.h"

#include "people.h"


extern int capacity_h, capacity_a, capacity_n;
extern int spect_time;
extern int num_groups;
extern int total_people;
extern int chances;

pthread_mutex_t time_lock;
pthread_cond_t time_cond;

pthread_t timethread;

//since game shd be checked at each second , I am coding it inside time tracker only.
void * time_tracker()
{	
	sleep(1);
	while(1)
	{
		pthread_mutex_lock(&time_lock);
		timer++;
		printf(RED);
		printf("Time elapsed is %d.............\n", timer);		
		printf(RESET);
//goal chance thing starts here.
		if(ptrgoal_chance[present_chance].time == timer)
		{
			int chance = (int)(ptrgoal_chance[present_chance].prob * 100);
			//printf("Chance is : %d\n", chance);
			if(rand()%100 < chance)
			{
				char team = ptrgoal_chance[present_chance].team[0];
				if(team == 'H')
				{
					homegoals++;
					printf(GoalScored);
					printf("Team " Home_Team " have scored goal number %d\n",homegoals);
					printf(RESET);
				}
				else 
				{
					awaygoals++;
					printf(GoalScored);
					printf("Team " Away_Team " have scored goal number %d\n",awaygoals);
					printf(RESET);
				}

				present_chance++;
			}
			else
			{	
				char team = ptrgoal_chance[present_chance].team[0];
				if(team == 'H')
				{					
					printf(GoalMissed);
					printf("Team " Home_Team " missed the chance to score goal number %d\n",homegoals+1);
					printf(RESET);
				}
				else 
				{					
					printf(GoalMissed);
					printf("Team " Away_Team " missed the chance to score goal number %d\n",awaygoals+1);
					printf(RESET);
				}
				present_chance++;
			}

			//checking if goals exceeded the tolerate limit;
			for(int i = 0; i < total_people; i++)
			{
				if(arr_people[i].exit == 1)continue;
				pthread_mutex_lock(&arr_people[i].s_lock);
				if(arr_people[i].status == P_GS)
				{
					if(arr_people[i].team[0] == 'H' && awaygoals >= arr_people[i].goals_limit)
					{
						arr_people[i].status = P_WUH;
					}
					else if(arr_people[i].team[0] == 'A' && homegoals >= arr_people[i].goals_limit)
					{
						arr_people[i].status = P_WUH;
					}
				}			
				pthread_mutex_unlock(&arr_people[i].s_lock);
				pthread_cond_signal(&arr_people[i].s_cond);
			}
		}
//patience checking thing starts here.
		int send = 0;
		for(int i = 0; i < total_people; i++)
		{	
			if(arr_people[i].exit == 1)continue;
			send = 0;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_RCHD)
			{	
				send = 1;
				int wait = timer - arr_people[i].reaching_time;
				if(wait > arr_people[i].patience_time)
				{
					arr_people[i].status = P_CGS;
				}
			}			
			pthread_mutex_unlock(&arr_people[i].s_lock);
			if(send)
			pthread_cond_signal(&arr_people[i].s_cond);
		}
//match watching time limit;
		for(int i = 0; i < total_people; i++)
		{	
			if(arr_people[i].exit == 1)continue;
			pthread_mutex_lock(&arr_people[i].s_lock);
			if(arr_people[i].status == P_GS)
			{
				int watch = timer - arr_people[i].entry_time;
				if(watch >= spect_time)
				{
					arr_people[i].status = P_WH;
				}
			}
			pthread_mutex_unlock(&arr_people[i].s_lock);
			pthread_cond_signal(&arr_people[i].s_cond);
		}


		pthread_mutex_unlock(&time_lock);
		pthread_cond_broadcast(&time_cond);
		sleep(1);
	}
}


int main()
{	
// input part starts here
	scanf("%d %d %d", &homezone.total_seats, &awayzone.total_seats, &neutralzone.total_seats);
	scanf("%d %d", &spect_time, &num_groups);

	homezone.seats_filled = 0;
	neutralzone.seats_filled = 0;
	awayzone.seats_filled = 0;

	total_people = 0;
	int peepindex = 0;

	int groups[num_groups]; // just to store number of memb in each group
	int grpindex = 0;
	for(int i = 0; i < num_groups; i++)
	{
		int k;
		scanf("%d", &k);
		total_people+=k;

		groups[grpindex] = k;
		grpindex++;

		for(int j = 0; j < k; j++)
		{
			scanf("%s", arr_people[peepindex].name );
			scanf("%s %d %d", arr_people[peepindex].team, &arr_people[peepindex].reaching_time, &arr_people[peepindex].patience_time);
			scanf("%d", &arr_people[peepindex].goals_limit);
			arr_people[peepindex].id = peepindex;
			arr_people[peepindex].group = i+1;
			arr_people[peepindex].friends = k;			//considering himself as friend too.
			arr_people[peepindex].status = -1;
			arr_people[peepindex].exit = 0;
			peepindex++;
		}
	}

	
	scanf("%d", &chances);
	goal_chance arr_goal_chance[chances];
	ptrgoal_chance = arr_goal_chance;

	for(int i = 0; i < chances; i++)
	{
		scanf("%s",	arr_goal_chance[i].team);
		scanf("%d", &arr_goal_chance[i].time);
		scanf("%lf", &arr_goal_chance[i].prob);
	}
//////////////////////////////////input part ends here
///*************************************************
// processing part starts here.
// game ka thread;
// time ka thread;
// people ka thread;

	pthread_mutex_t grpprint[num_groups];
	ptrtogrpprint = grpprint;
	for(int i = 0; i < num_groups; i++)
	{
		pthread_mutex_init(&grpprint[i], NULL);
	}
	pthread_barrier_t arr_barriers[num_groups];
	ptrtobarriers = arr_barriers;
	for(int i = 0; i < num_groups; i++)
	{
		pthread_barrier_init(&arr_barriers[i], NULL, groups[i]);
	}

	for(int i = 0; i < num_groups; i++)
	{

	}
	srand(time(NULL));
	pthread_mutex_init(&lock_seat, NULL);
	pthread_cond_init(&cond_seat, NULL);
	filled_seats = 0;

	homegoals = 0;
	awaygoals = 0;
	present_chance = 0;
	//total_seats;
	total_seats = (homezone.total_seats + awayzone.total_seats + neutralzone.total_seats);
	
	timer = 0;
	printf(RED);
	printf("Time elapsed is %d.............\n", timer);
	printf(RESET);
	pthread_cond_init(&time_cond, NULL);
	pthread_mutex_init(&time_lock, NULL);
	

	pthread_t people_threads[total_people];
	

	for(int i = 0; i < total_people; i++)
		if(pthread_create(&people_threads[i], NULL, &execute_people, (void *)(&arr_people[i])) != 0)
			perror("Failed to create people thread");

	
	pthread_create(&timethread, NULL, &time_tracker, NULL);

	pthread_t seats;
	pthread_create(&seats, NULL, &execute_seats, NULL);




//iswale join ko comment kardo baad mein
//pthread_join(timethread, NULL);
	
	for(int i = 0; i < total_people; i++)
		if(pthread_join(people_threads[i], NULL) != 0)
			perror("Failed to join people thread");

	for(int i = 0; i < num_groups; i++)
	{
		pthread_barrier_destroy(&arr_barriers[i]);
	}

	pthread_cond_destroy(&time_cond);
	pthread_mutex_destroy(&time_lock);

	pthread_mutex_destroy(&lock_seat);
	pthread_cond_destroy(&cond_seat);

	return 0;
}
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/colors.h ####################//

#ifndef _COLORS
#define _COLORS

#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define RESET "\033[0m"

#define Reached RESET
#define GotSeat GREEN
#define DidNotGetSeat MAGENTA
#define WatchedHappy YELLOW
#define WatchedSad MAGENTA
#define WaitingFriends CYAN
#define GroupLeave BLUE

#define GoalScored GREEN
#define GoalMissed MAGENTA

#endif
//###########FILE CHANGE ./main_folder/Akash C R_305804_assignsubmission_file_/assignment 5/q2/datatypes.h ####################//

#ifndef _DATATYPES
#define _DATATYPES
#include <pthread.h>

#define Home_Team "FC Messilona" 
#define Away_Team "Benzdrid CF"

typedef struct zone zone;
typedef struct people people;
typedef struct goal_chance goal_chance;


struct zone
{
	int seats_filled;
	int total_seats;

};

struct people
{
	int id;
	int group;
	int friends;
	char name[100];
	char team[1];				//Can take either of the H, A, N values
	int reaching_time;
	int patience_time;
	int goals_limit;
	int entry_time;			//should be initialised if seats allocated
	int status;
	char seatzone;
	pthread_mutex_t s_lock;
	pthread_cond_t s_cond;

	int exit;				//he should exit the simulation.
};

struct goal_chance
{
	char team[1];
	int time;			// time is secs after which chance is there;
	double prob;
};

//global variables defined here:
int spect_time;
int num_groups;

int total_people;
int chances;
int total_seats;
int timer;

people arr_people[200];
goal_chance * ptrgoal_chance;
zone homezone;
zone awayzone;
zone neutralzone;

pthread_mutex_t lock_seat;
int filled_seats;
pthread_cond_t cond_seat;

int homegoals;
int awaygoals;
int present_chance;

pthread_barrier_t * ptrtobarriers;
pthread_mutex_t * ptrtogrpprint;
#endif