
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/include/global.h ####################//

#include "matchsim.h"

static const int num_zones = 3;

extern struct timespec start;
extern Match MatchInfo;

extern int spectating_time;
extern int num_groups;
extern int num_chances;

//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/include/utils.h ####################//

#ifndef __SIM_UTILS
#define __SIM_UTILS

#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define PINK "\x1B[35m"
#define CYAN "\x1B[36m"
#define WHITE "\x1B[37m"
#define RESET "\x1B[0m"

typedef struct pair{
	void *first;
	void *second;
} pair;

int get_time();
struct timespec getTimespec(int t);
void cprintf(const char *color, const char *format, ...);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/include/matchsim.h ####################//

#ifndef __SIM_MATCH
#define __SIM_MATCH

#include <pthread.h>
#include <semaphore.h>

typedef struct Match{
	int home_score;
	int away_score;

	pthread_cond_t home_c;
	pthread_cond_t away_c;
	pthread_mutex_t lock;
	pthread_cond_t HN_free;
	pthread_cond_t A_free;
	pthread_cond_t HAN_free;

	pthread_mutex_t redundant_lock;
	pthread_cond_t redundant_cond;
} Match;

typedef struct GoalInfo{
	int team_id;
	int time;
	double prob;
} GoalInfo;

void init_match();
void read_chances(GoalInfo *Chances);
void *sim_match(void *arg);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/include/zones.h ####################//

#ifndef __SIM_ZONES
#define __SIM_ZONES

enum { ZONE_H, ZONE_A, ZONE_N };

typedef struct Zone{
	char name;
	int capacity;
	sem_t free;
} Zone;

void read_zones(Zone *Zones);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/include/person.h ####################//

#ifndef __SIM_PERSON
#define __SIM_PERSON

enum { WAITING, SPECTATING, LEAVING };

typedef struct Person{
	char *name;
	int type;
	int arrival_time;
	int patience_time;
	int R;

	int state;
	int zone_id;
	sem_t *semptr;
} Person;

typedef struct Group{
	sem_t people_left;
	int id;
	pthread_cond_t exit_sim;
	int size;
	Person *people;
} Group;

void read_groups(Group *Groups);
void *spawn_spec(void *arg);

#endif
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/src/utils.c ####################//

#include <time.h>
#include <stdarg.h>
#include <stdio.h>

#include "../include/utils.h"
#include "../include/global.h"

int get_time(){
	struct timespec finish;
	clock_gettime(CLOCK_REALTIME, &finish);
	long long sec = (finish.tv_sec - start.tv_sec);
	long long nsec = (finish.tv_nsec - start.tv_nsec);
	if(sec > 0 && nsec < 0) return sec-1;
	else if(sec < 0 && nsec > 0) return sec + 1;
	return sec;
}

struct timespec getTimespec(int t){
	struct timespec retval;
	retval.tv_sec = start.tv_sec + t;
	retval.tv_nsec = start.tv_nsec;
	return retval;
}

void cprintf(const char *color, const char *format, ...) {
    printf("%s", color);

    va_list argptr;
    va_start(argptr, format);
    vprintf(format, argptr);
    va_end(argptr);

    printf("%s", RESET);
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/src/person.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>

#include "../include/global.h"
#include "../include/matchsim.h"
#include "../include/person.h"
#include "../include/zones.h"
#include "../include/utils.h"

void create_group(Group *g, int people){
	sem_init(&(g->people_left), 0, 0);
	pthread_cond_init(&(g->exit_sim), NULL);
	g->people = malloc(sizeof(Person) * people);
	g->size = people;
}

int cmpfunc(const void *a, const void *b){
	Person *A = (Person*) a;
	Person *B = (Person*) b;
	return A->arrival_time > B->arrival_time;
}

void read_groups(Group *Groups){

	int LUT[26] = {['A'-'A'] = ZONE_A, ['H'-'A'] = ZONE_H, ['N'-'A'] = ZONE_N};

	for(int i=0; i<num_groups; i++){
		int p; scanf("%d", &p);
		Group *G = &Groups[i];
		G->id = i;
		create_group(G, p);
		
		char buf[1024];
		for(int j=0; j<p; j++){
			Person *P = &(G->people[j]);
			char z;
			scanf("%s %c %d %d %d", buf, &z, &(P->arrival_time), &(P->patience_time), &(P->R));
			P->name = strdup(buf);
			P->type = LUT[z-'A'];
			P->state = WAITING;
			P->zone_id = -1;
			P->semptr = &G->people_left;
		}
		qsort(G->people, G->size, sizeof(Person), cmpfunc);
	}
}

int sz_avail[3] = {	[ZONE_H] = 2, [ZONE_A] = 1, [ZONE_N] = 3 };
int avail[3][3] = {	
					[ZONE_H] = { ZONE_H, ZONE_N, -1 }, 
					[ZONE_A] = { ZONE_A, -1, -1 },
					[ZONE_N] = { ZONE_H, ZONE_A, ZONE_N }
				  };
pthread_cond_t *cond_avail[3] = { 
									[ZONE_H] = &MatchInfo.HN_free,
									[ZONE_A] = &MatchInfo.A_free,
									[ZONE_N] = &MatchInfo.HAN_free,
								};

void *sim_spec(void *arg){
	pair *parg = (pair*) arg;
	Person *p = parg->first;
	Zone *zones = parg->second;

	cprintf(CYAN, "t=%d: %s has reached the stadium\n", p->arrival_time, p->name);

	struct timespec patience = getTimespec(p->arrival_time + p->patience_time);
	while(p->state == WAITING){

		for(int i=0; i < sz_avail[p->type]; i++){
			int zid = avail[p->type][i];

			if(sem_trywait(&zones[zid].free) == 0){
				cprintf(GREEN, "t=%d: %s has got a seat in zone %c\n", get_time(), p->name, zones[zid].name);
				p->state = SPECTATING;
				p->zone_id = zid;
				break;
			}
		}

		if(p->state == SPECTATING) break;
		pthread_mutex_lock(&MatchInfo.lock);
		if(pthread_cond_timedwait(cond_avail[p->type], &MatchInfo.lock, &patience) != 0){
			p->state = LEAVING;
		}
		pthread_mutex_unlock(&MatchInfo.lock);
	}

	if(p->state == LEAVING) {
		cprintf(RED, "t=%d: %s couldn't get a seat\n", get_time(), p->name);
		sem_post(p->semptr);
		pthread_exit(NULL);
	}

	struct timespec endspec = getTimespec(get_time() + spectating_time);
	while(p->state == SPECTATING && p->type != ZONE_N){
		pthread_mutex_lock(&MatchInfo.lock);
		if((p->type == ZONE_H && MatchInfo.away_score >= p->R) || (p->type == ZONE_A && MatchInfo.home_score >= p->R)){
			pthread_mutex_unlock(&MatchInfo.lock);
			p->state = LEAVING;
			cprintf(YELLOW, "t=%d: Person %s is leaving due to the bad defensive performance of his team\n", get_time(), p->name);
			if(p->type == ZONE_H)
				pthread_cond_broadcast(&MatchInfo.HN_free);
			else
				pthread_cond_broadcast(&MatchInfo.A_free);
			break;
		}
		if(p->type == ZONE_A){
			if(pthread_cond_timedwait(&MatchInfo.home_c, &MatchInfo.lock, &endspec) != 0){
				p->state = LEAVING;
				cprintf(BLUE, "t=%d: Person %s watched the match for %d seconds and is leaving\n", get_time(), p->name, spectating_time);
				pthread_cond_broadcast(&MatchInfo.A_free);
			}
			pthread_mutex_unlock(&MatchInfo.lock);
		}
		else if(p->type == ZONE_H){
			if(pthread_cond_timedwait(&MatchInfo.away_c, &MatchInfo.lock, &endspec) != 0){
				p->state = LEAVING;
				cprintf(BLUE, "t=%d: Person %s watched the match for %d seconds and is leaving\n", get_time(), p->name, spectating_time);
				pthread_cond_broadcast(&MatchInfo.HN_free);
			}
			pthread_mutex_unlock(&MatchInfo.lock);
		}
	}

	pthread_mutex_t redundant_l;
	pthread_cond_t redundant_c;
	pthread_mutex_init(&redundant_l, NULL);
	pthread_cond_init(&redundant_c, NULL);

	if(p->state == SPECTATING && p->type == ZONE_N){
		pthread_mutex_lock(&redundant_l);
		// Will timeout
		pthread_cond_timedwait(&redundant_c, &redundant_l, &endspec);
		pthread_mutex_unlock(&redundant_l);
		cprintf(BLUE, "t=%d: Person %s watched the match for %d seconds and is leaving\n", get_time(), p->name, spectating_time);
		pthread_cond_broadcast(&MatchInfo.HAN_free);
		p->state = LEAVING;
	}

	sem_post(p->semptr);
	// cprintf(PINK, "t=%d: Person %s is leaving for dinner\n", get_time(), p->name);
	return NULL;	
}

void *spawn_spec(void *arg){
	pair *parg = (pair*) arg;
	Group *group = parg->first;
	Zone *zones = parg->second;

	int cur = 0;
	for(int time = 0; ; time++, sleep(1)){

		if(cur == group->size) break;

		while(cur < group->size && group->people[cur].arrival_time == time){
			pthread_t *sim_spec_t = malloc(sizeof(pthread_t));
			pair *argptr = malloc(sizeof(pair));
			argptr->first = &(group->people[cur]);
			argptr->second = zones;
			pthread_create(sim_spec_t, NULL, sim_spec, argptr);
			cur++;
		}
	}

	int ct = 0;
	while(sem_wait(&group->people_left) == 0){
		ct++;
		if(ct == group->size) break;
	}
	cprintf(YELLOW, "t=%d: Group %d is leaving for dinner\n", get_time(), group->id);
	pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/src/matchsim.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#include "../include/global.h"
#include "../include/matchsim.h"
#include "../include/person.h"
#include "../include/zones.h"
#include "../include/utils.h"

void read_chances(GoalInfo *Chances){

	int LUT[26] = {['A'-'A'] = ZONE_A, ['H'-'A'] = ZONE_H};

	for(int i=0; i<num_chances; i++){
		GoalInfo *C = &Chances[i];
		char z;
		scanf("%c %d %lf ", &z, &C->time, &C->prob);
		C->team_id = LUT[z-'A'];
	}
}

void init_match(){
	clock_gettime(CLOCK_REALTIME, &start);

	MatchInfo.home_score = 0;
	MatchInfo.away_score = 0;

	pthread_mutex_init(&MatchInfo.lock, NULL);
	pthread_cond_init(&MatchInfo.home_c, NULL);
	pthread_cond_init(&MatchInfo.away_c, NULL);
	
	pthread_mutex_init(&MatchInfo.redundant_lock, NULL);
	pthread_cond_init(&MatchInfo.redundant_cond, NULL);
}

void *sim_match(void *arg){
	GoalInfo *Chances = (GoalInfo*) arg;

	for(int i=0; i<num_chances; i++){
		struct timespec t = getTimespec(Chances[i].time);
		pthread_mutex_lock(&MatchInfo.redundant_lock);
		pthread_cond_timedwait(&MatchInfo.redundant_cond, &MatchInfo.redundant_lock, &t);
		pthread_mutex_unlock(&MatchInfo.redundant_lock);
		if(Chances[i].prob >= 0.5){
			pthread_mutex_lock(&MatchInfo.lock);
			switch(Chances[i].team_id){
				case ZONE_H:
					MatchInfo.home_score++;
					cprintf(WHITE, "t=%d: Team H have scored their %d(th/st/nd) goal\n", Chances[i].time, MatchInfo.home_score);
					pthread_cond_broadcast(&MatchInfo.home_c);
				break;
				case ZONE_A:
					MatchInfo.away_score++;
					cprintf(WHITE, "t=%d: Team A have scored their %d(th/st/nd) goal\n", Chances[i].time, MatchInfo.away_score);
					pthread_cond_broadcast(&MatchInfo.away_c);
				break;
			}
			pthread_mutex_unlock(&MatchInfo.lock);
		}
		else{
			cprintf(WHITE, "t=%d: Team %c missed the chance to score their %d(th/st/nd) goal\n", Chances[i].time, ((Chances[i].team_id == ZONE_A) ? 'A' : 'H'), MatchInfo.home_score);
		}
		
	}
	return NULL;
}
//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/src/zones.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#include "../include/global.h"
#include "../include/matchsim.h"
#include "../include/person.h"
#include "../include/zones.h"

void read_zones(Zone *Zones){
	scanf("%d", &Zones[ZONE_H].capacity);
	sem_init(&Zones[ZONE_H].free, 0, Zones[ZONE_H].capacity);
	Zones[ZONE_H].name = 'H';

	scanf("%d", &Zones[ZONE_A].capacity);
	sem_init(&Zones[ZONE_A].free, 0, Zones[ZONE_A].capacity);
	Zones[ZONE_A].name = 'A';

	scanf("%d", &Zones[ZONE_N].capacity);
	sem_init(&Zones[ZONE_N].free, 0, Zones[ZONE_N].capacity);
	Zones[ZONE_N].name = 'N';
}


//###########FILE CHANGE ./main_folder/A Kishore Kumar_305963_assignsubmission_file_/2020101126/q2/src/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#include "../include/global.h"
#include "../include/matchsim.h"
#include "../include/person.h"
#include "../include/zones.h"
#include "../include/utils.h"

struct timespec start = {0};
int spectating_time = 0;
int num_groups = 0;
int num_chances = 0;
Match MatchInfo = {0};

pthread_t run_game, *spawn_spec_t;

int main(void){

	Zone *Zones = malloc(sizeof(Zone)*num_zones);
	read_zones(Zones);

	scanf("%d ", &spectating_time);

	scanf("%d ", &num_groups);
	Group *Groups = malloc(sizeof(Group)*num_groups);
	read_groups(Groups);

	scanf("%d ", &num_chances);
	GoalInfo *Chances = malloc(sizeof(GoalInfo)*num_chances);
	read_chances(Chances);

	init_match();
	spawn_spec_t = malloc(sizeof(pthread_t)*num_groups);
	for(int i=0; i<num_groups; i++){
		pair *spawn_spec_arg = malloc(sizeof(pair));
		spawn_spec_arg->first = &Groups[i];
		spawn_spec_arg->second = Zones;
		pthread_create(&spawn_spec_t[i], NULL, spawn_spec, spawn_spec_arg);
	}
	pthread_create(&run_game, NULL, sim_match, Chances);
	pthread_join(run_game, NULL);
	for(int i=0; i<num_groups; i++){
		pthread_join(spawn_spec_t[i], NULL);
	}
}