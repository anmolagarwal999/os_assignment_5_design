
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/include/libraries.h ####################//

#ifndef LIB_H
#define LIB_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

// Colors
#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"

// Types
typedef uint Time;

// Person Structure
typedef struct stPerson tPerson;
struct stPerson
{
    int dump[10];
    char Name[100];
    char SupportTeam;
    char SeatedZone;
    Time ArrivalTime;
    Time Patience;
    int EnrageNum;
    int status;
    pthread_mutex_t PersonLock;
};

// Seat Structure
typedef struct stSeat tSeat;
struct stSeat
{
    tPerson Person;
    int i;
    int j;
};

// Zone Structure
typedef struct stZone tZone;
struct stZone
{
    char Type;
    uint Capacity;
    uint NumSpectators;
    tPerson* Spectator;
    tSeat* Seat;
    pthread_mutex_t* SeatLocks;
};
tZone Zone[3];

// Group Structure
typedef struct stGroup tGroup;
struct stGroup
{
    int dump[10];
    int k;
    int Waiting;
    tPerson* Person;
    pthread_t th[];
};
tGroup* Group;

// Goal Structure
typedef struct stGoal tGoal;
struct stGoal
{
    int dump[30];
    char Team;
    Time GoalTime;
    float GoalProb;
    pthread_mutex_t GoalLock;
};
tGoal* Goal;

// Person info structure
struct stPersonID
{
    int dump[10];
    int groupNo;
    int personNo;
};
typedef struct stPersonID tPersonID;

#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/include/variables.h ####################//

#ifndef VAR_H
#define VAR_H

#define HOME_TEAM "FC Messilona"
#define AWAY_TEAM "Benzdrid CF"

#define HOME 0
#define AWAY 1
#define NEUT 2

#define REACHED 11
#define WAITING 12
#define EXIT    13

int X;               // Spectating time
int num_groups;      // Total number of groups
int num_people;
int G;               // Number of goal scoring chances
time_t startTime;
time_t maxTime;

int Goals[2];

pthread_mutex_t lock;
pthread_t* goal_thread;
pthread_cond_t cond_seat_freed;

#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/include/functions.h ####################//

#ifndef FUN_H
#define FUN_H

// Input
void input();
    void inputZones();
    void inputGroups();
    void inputGoals();
    void inputPersons(int i);
    void printZone(int i);
    void printGroup(int i);

// Leave
void leaveHOME();
void leaveAWAY();
void leaveEnrage(int T, int Z);

// Seat
void seat(int i, int j, int seatZone, int seatNum);
int seatAvailable(int i);
void noSeat(int G, int P);
int probHome();
int probNeut();
int probAway();
char getZoneAsChar(int team);
int getZoneAsInt(char team);

// Goal
void* goal_function(void* arg);
    char* getGoalSuffix(int G);

// Person
void* person_function(void* arg);
    void reach(int i, int j);
    void patience(int G, int P);
    void dinner(int i);

// Main
void initialize();
void join();

// Utility
void flushSTDIN();
void a();
void b();
void c();
void d();
int Prob(float n);
float R();

#endif
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/utility.c ####################//

#include "../include/libraries.h"
#include "../include/functions.h"
#include "../include/variables.h"

void flushSTDIN()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Debugging
void a()
{
    printf("------ Fish\n");
}
void b()
{
    printf("------ Shark\n");
}
void c()
{
    printf("------ Start\n");
}
void d()
{
    printf("------ End\n");
}
void coord(int i, int j)
{
    printf("(%d, %d) %s\n", i+1, j+1, Group[i].Person[j].Name);
}

int getZoneAsInt(char c)
{
    switch(c)
    {
        case 'H': return HOME;
        case 'A': return AWAY;
        case 'N': return NEUT;
    }
}

char getZoneAsChar(int zone)
{
    switch(zone)
    {
        case HOME: return 'H';
        case AWAY: return 'A';
        case NEUT: return 'N';
    }
}

int Prob(float n)
{
    if(n == 1) return 1;

    srand(time(NULL) % 30);
    float p = (float)rand() / (float)RAND_MAX;
    return (p < n);
}
// Random number between 0 and 1
float R()
{
    srand(time(NULL) % 30);
    float p = (float)rand() / (float)RAND_MAX;
    return p;
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/main.c ####################//

// Each person is a thread
// Each seat is a lock

// Libraries
#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

void main()
{
    printf(COLOR_YELLOW "Simulation has started!\n" COLOR_RESET);
    input();
    printf("------------------------------------------------------------------------\n");

    tPersonID PersonID[num_people];
    int c = 0;
    startTime = time(NULL);
    
    // Person Threads
    for (int i = 0; i < num_groups; i++)
    {
        // Each group has k members, hence a thread array of size k
        Group[i].th[Group[i].k];

        // Pass (Group-No, Person-No) to each thread to identify the person
        for (int j = 0; j < Group[i].k; j++)
        {
            PersonID[c].groupNo = i;
            PersonID[c].personNo = j;
            pthread_create(&Group[i].th[j], NULL, person_function, &PersonID[c++]);
            usleep(50);
        }
    }

    // Goal Threads
    for(int i = 0; i < G; i++)
    {
        pthread_create(&goal_thread[i], NULL, goal_function, &i);
        usleep(50);
    }

    // Join threads
    join();

    printf("------------------------------------------------------------------------\n");
}

void initialize()
{
    num_people = 0;
    maxTime = 0;
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&cond_seat_freed, NULL);
}

void join()
{
    // Join person threads
    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < Group[i].k; j++)
        {
            pthread_join(Group[i].th[j], NULL);
            // pthread_exit(Group[i].th[j]);
        }
    }

    // // Join goal threads
    for(int i = 0; i < G; i++)
    {
        pthread_join(goal_thread[i], NULL);
        // pthread_exit(goal_thread[i]);
    }
    exit(0);
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/seat.c ####################//

#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

int seatAvailable(int teamNum)
{
    for(int i = 0; i < Zone[teamNum].Capacity; i++)
    {
        tSeat S = Zone[teamNum].Seat[i];
        // printf("S.Person.Name = %s\n", S.Person.Name);
        if(!strlen(S.Person.Name))
        {
            return i;
        }
    }
    return -1;
}

void noSeat(int G, int P)
{
    printf(COLOR_YELLOW "%s could not get a seat\n" COLOR_RESET, Group[G].Person[P].Name);
}

void seat(int i, int j, int team, int s)
{
    Zone[team].Seat[s].Person = Group[i].Person[j];
    Zone[team].Seat[s].i = i;
    Zone[team].Seat[s].j = j;

    char ch = getZoneAsChar(team);

    pthread_mutex_lock(&lock);
    printf(COLOR_YELLOW "%s has got a seat in zone %c\n" COLOR_RESET, 
            Group[i].Person[j].Name,
            ch);
    pthread_mutex_unlock(&lock);

    if(Group[i].Person[j].SupportTeam == 'H')
        leaveHOME();
    if(Group[i].Person[j].SupportTeam == 'A')
        leaveAWAY();


    pthread_mutex_unlock(&Zone[team].SeatLocks[s]);
    sleep(X);
    pthread_mutex_lock(&Zone[team].SeatLocks[s]);

    if(Group[i].Person[j].status == WAITING)
        return;

    printf(COLOR_MAGENTA "%s watched the match for %d seconds and is leaving\n" COLOR_RESET, Group[i].Person[j].Name, X);
    printf("%s is waiting for their friends at the exit\n", Group[i].Person[j].Name);

    Zone[team].NumSpectators--;
    Zone[team].Seat[i].Person.Name[0] = '\0';
    Group[i].Person[j].status = WAITING;
    Group[i].Waiting++;

    pthread_mutex_lock(&Group[i].Person[j].PersonLock);
    pthread_cond_signal(&cond_seat_freed);
    pthread_mutex_unlock(&Group[i].Person[j].PersonLock);

    dinner(i);
}

int probHome()
{
    // Home --> Home & Neutral
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int sum = spaceH + spaceN;

    float probH = (float)spaceH/(float)(sum);
    if(Prob(probH))
        return HOME;
    return NEUT;
}
int probNeut()
{
    // Neutral --> Home & Neutral & Away
    int spaceH = Zone[HOME].Capacity - Zone[HOME].NumSpectators;
    int spaceN = Zone[NEUT].Capacity - Zone[NEUT].NumSpectators;
    int spaceA = Zone[AWAY].Capacity - Zone[AWAY].NumSpectators;
    int sum = spaceH + spaceN + spaceA;

    float probH = (float)spaceH/(float)(sum);
    float probN = (float)spaceN/(float)(sum);
    float probA = (float)spaceA/(float)(sum);

    float random = R();
    if(random < probH)
        return HOME;
    if(random < probH + probN)
        return NEUT;
    return AWAY;
}
int probAway()
{
    // Away --> Away
    return AWAY;
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/person.c ####################//

#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

void *person_function(void *arg)
{
    // Extract structure
    tPersonID s = *(tPersonID*)arg;
    int i = s.groupNo;
    int j = s.personNo;

    // Get person's team
    char team = Group[i].Person[j].SupportTeam;
    int teamNum = getZoneAsInt(team);

    reach(i, j);

    // Decide person's zone
    time_t arrivalTime = time(NULL);
    int seatZone;
    int c;
    int flag = 0;
    int timeOut = 1;

    do
    {
        switch(teamNum)
        {
            case HOME: seatZone = probHome(); break;
            case NEUT: seatZone = probNeut(); break;
            case AWAY: seatZone = probAway(); break;
        }

        // Find the first available seat
        c = seatAvailable(seatZone);
        
        if(c < 0 && !flag)
        {
            noSeat(i, j);
            flag = 1;
        }
        if(c >= 0)
        {
            // seat_freed = 0;
            timeOut = 0;
            break;
        }

        // pthread_cond_wait(&cond_seat_freed, &lock);
        // pthread_cond_timedwait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c], Group[i].Person[j].Patience);
        // pthread_cond_wait(&cond_seat_freed, &Zone[seatZone].SeatLocks[c]);
        pthread_cond_wait(&cond_seat_freed, &Group[i].Person[j].PersonLock);

    }while(time(NULL) - arrivalTime <= Group[i].Person[j].Patience);

    if(timeOut)
    {
        patience(i, j);
        return NULL;
    }

    pthread_mutex_lock(&Zone[seatZone].SeatLocks[c]);
    seat(i, j, seatZone, c);
    pthread_mutex_unlock(&Zone[seatZone].SeatLocks[c]);

    return NULL;
}

void reach(int i, int j)
{
    sleep(Group[i].Person[j].ArrivalTime);
    printf(COLOR_CYAN "%s has reached the stadium\n" COLOR_RESET, Group[i].Person[j].Name);
}

void patience(int G, int P)
{
    printf("%s is waiting for their friends at the exit\n", Group[G].Person[P].Name);
    Group[G].Person[P].status = WAITING;
    Group[G].Waiting++;
    dinner(G);
}

void dinner(int i)
{
    // Some members of the group are still watching the match
    if(Group[i].Waiting < Group[i].k)
        return;
    
    // Everyone from the group is waiting, they leave for dinner
    printf(COLOR_BLUE "Group %d is leaving for dinner\n" COLOR_RESET, i+1);
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/leave.c ####################//

#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

void leaveAWAY()
{
    usleep(50);

    // AWAY supportes can be seated only in AWAY zone
    leaveEnrage(AWAY, AWAY);
}

void leaveHOME()
{
    usleep(50);

    // HOME supportes can be seated in HOME and NEUT zones
    leaveEnrage(HOME, HOME);    
    leaveEnrage(HOME, NEUT);
}

// Look for enraged people supporting team T in zone Z
void leaveEnrage(int T, int Z)
{
    // Iterate through all the seats in zone Z
    for(int i = 0; i < Zone[Z].Capacity; i++)
    {
        tSeat S = Zone[Z].Seat[i];
        tPerson P = S.Person;
        
        // Ignore neutral people
        if(Zone[Z].Seat[i].Person.SupportTeam == 'N')
            continue;

        if(!strlen(P.Name))
            continue;

        // Ignore people who have left
        if(Group[S.i].Person[S.j].status == WAITING)
            continue;

        // Check if not enraged (1-T gives the opposite team).
        if(Goals[1-T] < P.EnrageNum)
            continue;

        usleep(50);
        printf(COLOR_RED "%s is leaving due to bad performance of his team\n" COLOR_RESET, Zone[Z].Seat[i].Person.Name);
        printf("%s is waiting for their friends at the exit\n", Zone[Z].Seat[i].Person.Name);

        Zone[Z].NumSpectators--;
        Zone[Z].Seat[i].Person.Name[0] = '\0';
        Group[S.i].Person[S.j].status = WAITING;
        Group[S.i].Waiting++;


        // Send a signal so that threads of people who could not get a
        // seat can start looking for a seat after a person has left
        // pthread_mutex_lock(&Group[S.i].Person[S.j].PersonLock);
        pthread_cond_broadcast(&cond_seat_freed);
        // pthread_mutex_unlock(&Group[S.i].Person[S.j].PersonLock);

    }
}

//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/input.c ####################//

#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

void input()
{
    initialize();
    inputZones();

    printf(COLOR_BLUE "Spectating Time: " COLOR_RESET);
    scanf("%d", &X);

    // Groups, Persons
    printf(COLOR_RED "Number of groups: " COLOR_RESET);
    scanf("%d", &num_groups);
    inputGroups();

    // Goals
    printf(COLOR_GREEN "\nNumber of goal scoring chances: " COLOR_RESET);
    scanf("%d", &G);
    goal_thread = (pthread_t*)malloc(G * sizeof(pthread_t));
    inputGoals();
}

void inputZones()
{
    Zone[HOME].Type = 'H';         // Home
    Zone[AWAY].Type = 'A';         // Away
    Zone[NEUT].Type = 'N';         // Neutral

    // Input zone capacities
    printf(COLOR_BLUE "Home | Away | Neutral\n" COLOR_RESET);
    scanf("%d %d %d", &Zone[0].Capacity, &Zone[1].Capacity, &Zone[2].Capacity);

    // Iterate through all the zones
    for(int i = 0; i < 3; i++)
    {
        // Intialize zones. Create locks for every seat in a zone
        Zone[i].NumSpectators = 0;
        Zone[i].Spectator = (tPerson*)malloc(Zone[i].Capacity * sizeof(tPerson));
        Zone[i].Seat = malloc(Zone[i].Capacity * sizeof(tSeat));
        Zone[i].SeatLocks = malloc(Zone[i].Capacity * sizeof(pthread_mutex_t));
        
        // Initialize the lock for each seat in a zone
        for(int j = 0; j < Zone[i].Capacity; j++)
        {
            pthread_mutex_init(&Zone[i].SeatLocks[j], NULL);
        }
    }
}

void inputGroups()
{
    Group = (tGroup*)malloc(num_groups * sizeof(tGroup));

    // Iterate through the groups
    for(int i = 0; i < num_groups; i++)
    {
        flushSTDIN();
        printf(COLOR_RED "\n(Group %d)\n" COLOR_RESET, i+1);

        // Number of people in group i
        printf(COLOR_RED "Number: " COLOR_RESET);
        scanf("%d", &Group[i].k);
        num_people += Group[i].k;

        // Allocate memory for all persons in a group
        Group[i].Person = (tPerson*)malloc(Group[i].k * sizeof(tPerson));

        // Initially, no one is waiting
        Group[i].Waiting = 0;
        
        // Input persons in the group
        inputPersons(i);
    }
}

void inputPersons(int i)
{
    // Iterate through all the persons in the group
    for(int j = 0; j < Group[i].k; j++)
    {
        // Prefix
        printf(COLOR_RED "P%d: " COLOR_RESET, j+1);

        // Input person variables
        scanf("%s %c %d %d %d", 
                &Group[i].Person[j].Name,
                &Group[i].Person[j].SupportTeam,
                &Group[i].Person[j].ArrivalTime,
                &Group[i].Person[j].Patience,
                &Group[i].Person[j].EnrageNum);

        if(Group[i].Person[j].ArrivalTime > maxTime)
            maxTime = Group[i].Person[j].ArrivalTime;
        
        pthread_mutex_init(&Group[i].Person[j].PersonLock , NULL);
    }
}

void inputGoals()
{
    Goal = (tGoal*)malloc(G * sizeof(tGoal));

    for(int i = 0; i < G; i++)
    {
        pthread_mutex_init(&Goal[i].GoalLock, NULL);
        flushSTDIN();

        // Prefix
        printf(COLOR_GREEN "G%d: " COLOR_RESET, i+1);

        // Input goal variables
        scanf("%c %d %f", 
                &Goal[i].Team,
                &Goal[i].GoalTime,
                &Goal[i].GoalProb);

        if(Goal[i].GoalTime > maxTime)
            maxTime = Goal[i].GoalTime;
    }   
    printf("\n");
}
//###########FILE CHANGE ./main_folder/Abhay Patil_305941_assignsubmission_file_/2020101022_assignment_5/q2/src/goal.c ####################//

#include "../include/libraries.h"
#include "../include/variables.h"
#include "../include/functions.h"

void *goal_function(void* arg)
{
    int i = *(int*)arg;
    sleep(Goal[i].GoalTime);
    usleep(500);

    char* suffix = malloc(2);

    // Goal scored
    if(Prob(Goal[i].GoalProb))
    {
        // HOME team scores
        if(Goal[i].Team == 'H')
        {
            Goals[HOME]++;
            strcpy(suffix, getGoalSuffix(Goals[HOME]));
            printf(COLOR_GREEN "Team FC Messilona has scored their %d%s goal!\n" COLOR_RESET, Goals[HOME], suffix);

            // People supporting AWAY team may leave
            leaveAWAY();
        }
        // AWAY team scores
        else
        {
            Goals[AWAY]++;
            strcpy(suffix, getGoalSuffix(Goals[AWAY]));
            printf(COLOR_GREEN "Team Benzdrid CF has scored their %d%s goal!\n" COLOR_RESET, Goals[AWAY], suffix);

            // People supporting HOME team may leave
            leaveHOME();
        }
    }
    // Goal Missed
    else
    {
        if(Goal[i].Team == 'H')
        {
            printf(COLOR_GREEN "Team FC Messilona has missed the chance to score their %d%s goal\n" COLOR_RESET,
                Goals[HOME]+1, getGoalSuffix(Goals[HOME]+1));
        }
        else
        {
            printf(COLOR_GREEN "Team Benzdrid CF has missed the chance to score their %d%s goal\n" COLOR_RESET,
                Goals[AWAY]+1, getGoalSuffix(Goals[AWAY]+1));
        }
    }

    if(Goal[i].GoalTime == maxTime)
        exit(0);

    return NULL;
}

char* getGoalSuffix(int Goals)
{
    int secondLastDigit = (Goals/10) % 10;

    // Handle excpetions: 11th, 12th and 13th
    if(secondLastDigit == 1)
        return "th";

    switch(Goals)
    {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
    }
    return "th";
}