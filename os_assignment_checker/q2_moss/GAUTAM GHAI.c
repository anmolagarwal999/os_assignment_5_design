
//###########FILE CHANGE ./main_folder/GAUTAM GHAI_305794_assignsubmission_file_/2020101020_assignment_5/q2/q2.c ####################//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
typedef long long lli;

typedef struct person{
    char name[50];
    lli time;
    char type_of_fan[2];
    lli patience_value;
    //lli x; //spectating time
    lli angry_goals; // no. of goals enuf to make a person angy, -1 for neutral fans
}person;

typedef struct group{
    person *spectators;
}group;

lli cap_zone_H,cap_zone_A,cap_zone_N,spectating_time,num_groups,num_goal_chances,num_fans_in_stadium;

typedef struct goal{
    char team[30];
    lli time_elapsed;
    double probability;
}goal;

lli home_team_goals, away_team_goals;

int main()
{
    home_team_goals=0,away_team_goals=0,num_fans_in_stadium=0;
    scanf("%lld %lld %lld",&cap_zone_H,&cap_zone_A,&cap_zone_N);
    scanf("%lld",&spectating_time);
    scanf("%lld",&num_groups);
    group groups[num_groups];
    for(int i=0;i<num_groups;i++)
    {
        lli num_spectators;
        scanf("%lld",&num_spectators);
        groups[i].spectators=(person*)malloc(sizeof(person)*num_spectators);
        for(int j=0;j<num_spectators;j++)
        {
            scanf("%s %s %lld %lld %lld",groups[i].spectators->name,groups[i].spectators->type_of_fan,&groups[i].spectators->time,&groups[i].spectators->patience_value,&groups[i].spectators->angry_goals);
        }
    }

    scanf("%lld",&num_goal_chances);
    goal goals[num_goal_chances];
    for(int i=0;i<num_goal_chances;i++)
    {
        scanf("%s %lld %lf",goals[i].team,&goals[i].time_elapsed,&goals[i].probability);
    }
    
}