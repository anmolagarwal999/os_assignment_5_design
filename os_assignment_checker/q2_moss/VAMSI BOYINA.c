
//###########FILE CHANGE ./main_folder/VAMSI BOYINA_305953_assignsubmission_file_/q2/q2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

long long int t = 0;
char name[100][20];
int arriving_time[100];
char support[100][2];
int patience[100];
int R_goals[100];
char goal_team[100][2];
int goal_time[100];
float goal_probability[100];
pthread_t person_id[100];
pthread_t time_counter,goal_counter;
int H, A, N, X;
int H_counter = 0, A_counter = 0, N_counter = 0;
int no_of_H_goals = 0, no_of_A_goals = 0;
int end_of_threads=0, create_of_threads = 0;
int no_audience=0;
char zone[100][2];
int waiting_status[100]={0};
int all_threads=0;
sem_t waiting[100];
sem_t watching[100];
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

//---------------------------------------------------------------------------------------------------------------------
void reached_stadium(int i){
    printf(ANSI_COLOR_RED "t=%lld: %s has reached the stadium\n" ANSI_COLOR_RED, t, name[i]);
}
void got_seat(int i){
    printf(ANSI_COLOR_MAGENTA"t=%lld: %s has got a seat in zone %c\n"ANSI_COLOR_MAGENTA, t, name[i], zone[i][0]);
}
void couldnt_seat(int i){
    printf(ANSI_COLOR_YELLOW"t=%lld: %s couldn’t get a seat\n"ANSI_COLOR_YELLOW, t, name[i]);
}
void watched_xmins(int i){
    printf(ANSI_COLOR_GREEN"t=%lld: %s watched the match for %d seconds and is leaving\n"ANSI_COLOR_GREEN, t, name[i], X);
}
void bad_team_performance(int i){
    printf(ANSI_COLOR_GREEN"t=%lld: %s is leaving due to the bad defensive performance of his team\n"ANSI_COLOR_GREEN, t, name[i]);
}
//**********************************************************************************************************
void Home_team_scored_yth_goal(){
    no_of_H_goals++;
    printf(ANSI_COLOR_RESET"t=%lld: Team H have scored their %dth goal\n"ANSI_COLOR_RESET, t, no_of_H_goals);
}
void Away_team_scored_yth_goal(){
    no_of_A_goals++;
    printf(ANSI_COLOR_RESET"t=%lld: Team A have scored their %dth goal\n"ANSI_COLOR_RESET, t, no_of_A_goals);
}
void Home_missed_yth_goal(){
    printf(ANSI_COLOR_RESET"t=%lld: Team H missed the chance to score their %dth goal\n"ANSI_COLOR_RESET, t, no_of_H_goals + 1);
}
void Away_missed_yth_goal(){
    printf(ANSI_COLOR_RESET"t=%lld: Team A missed the chance to score their %dth goal\n"ANSI_COLOR_RESET, t, no_of_A_goals + 1);
}

void person_leaving_simulation(int i){
    printf(ANSI_COLOR_YELLOW"t=%lld: %s is leaving for dinner\n"ANSI_COLOR_YELLOW, t, name[i]);
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void *person_activity(void *arg)
{
    int *i_ptr = (int *)arg;
    int i = *i_ptr;
    if (support[i][0] == 'N')
    {
        if (H_counter >= H && A_counter >= A && N_counter >= N)
        {
            waiting_status[i] = 1;
            struct timespec abs_time;
            clock_gettime(CLOCK_REALTIME, &abs_time);
            abs_time.tv_sec += patience[i];
            int j = sem_timedwait(&waiting[i], &abs_time);
            if (j == -1)
            {
                waiting_status[i] = 0;
                couldnt_seat(i);
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if (j == 0)
            {
                waiting_status[i] = 0;
                got_seat(i);
            }
        }
        else {
            if(N_counter<N)
            {   
                zone[i][0]='N';
                got_seat(i);
                pthread_mutex_lock(&lock);
                N_counter++;
                pthread_mutex_unlock(&lock);
                struct timespec watch_time;
                clock_gettime(CLOCK_REALTIME, &watch_time);
                watch_time.tv_sec += X;
                int j = sem_timedwait(&watching[i], &watch_time);
                if (j == -1)
                {
                    watched_xmins(i);
                }
                else if (j == 0)
                {
                    bad_team_performance(i);
                }
                pthread_mutex_lock(&lock);
                N_counter--;
                pthread_mutex_unlock(&lock);
                for(int j=0; j<no_audience;j++)
                {
                    if(t>arriving_time[j] && waiting_status[j]==1)
                    {
                        if(support[j][0] == 'N')
                        {
                            sem_post(&waiting[j]);
                        }
                    }
                }
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if(H_counter < H)
            {
                zone[i][0] = 'H';
                got_seat(i);
                pthread_mutex_lock(&lock);
                N_counter++;
                pthread_mutex_unlock(&lock);
                struct timespec watch_time;
                clock_gettime(CLOCK_REALTIME, &watch_time);
                watch_time.tv_sec += X;
                int j = sem_timedwait(&watching[i], &watch_time);
                if (j == -1)
                {
                    watched_xmins(i);
                }
                else if (j == 0)
                {
                    bad_team_performance(i);
                }
                pthread_mutex_lock(&lock);
                N_counter--;
                pthread_mutex_unlock(&lock);
                for(int j=0; j<no_audience;j++)
                {
                    if(t>arriving_time[j] && waiting_status[j]==1)
                    {
                        if(support[j][0] == 'N')
                        {
                            sem_post(&waiting[j]);
                        }
                    }
                }
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if(A_counter < A)
            {
                zone[i][0] = 'A';
                got_seat(i);
                pthread_mutex_lock(&lock);
                N_counter++;
                pthread_mutex_unlock(&lock);
                struct timespec watch_time;
                clock_gettime(CLOCK_REALTIME, &watch_time);
                watch_time.tv_sec += X;
                int j = sem_timedwait(&watching[i], &watch_time);
                if (j == -1)
                {
                    watched_xmins(i);
                }
                else if (j == 0)
                {
                    bad_team_performance(i);
                }
                pthread_mutex_lock(&lock);
                N_counter--;
                pthread_mutex_unlock(&lock);
                for(int j=0; j<no_audience;j++)
                {
                    if(t>arriving_time[j] && waiting_status[j]==1)
                    {
                        if(support[j][0] == 'N')
                        {
                            sem_post(&waiting[j]);
                        }
                    }
                }
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
        }
    }
    if (support[i][0] == 'H')
    {
        if(H_counter >= H && N_counter >= N)
        {
            waiting_status[i] = 1;
            struct timespec abs_time;
            clock_gettime(CLOCK_REALTIME, &abs_time);
            abs_time.tv_sec += patience[i];
            int j = sem_timedwait(&waiting[i], &abs_time);
            if (j == -1)
            {
                waiting_status[i] = 0;
                couldnt_seat(i);
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if (j == 0)
            {
                waiting_status[i] = 0;
                got_seat(i);
                pthread_mutex_lock(&lock);
                H_counter++;
                pthread_mutex_unlock(&lock);
            }
        }
        else {
            if(H_counter < H)
            {
                zone[i][0] = 'H';
                got_seat(i);
                pthread_mutex_lock(&lock);
                H_counter++;
                pthread_mutex_unlock(&lock);
                struct timespec watch_time;
                clock_gettime(CLOCK_REALTIME, &watch_time);
                watch_time.tv_sec += X;
                int j = sem_timedwait(&watching[i], &watch_time);
                if (j == -1)
                {
                    watched_xmins(i);
                }
                else if (j == 0)
                {
                    bad_team_performance(i);
                }
                pthread_mutex_lock(&lock);
                H_counter--;
                pthread_mutex_unlock(&lock);
                for(int j=0; j<no_audience;j++)
                {
                    if(t>arriving_time[j] && waiting_status[j]==1)
                    {
                        if(support[j][0] == 'H')
                        {
                            sem_post(&waiting[j]);
                        }
                    }
                }
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if(N_counter<N)
            {   
                zone[i][0]='N';
                got_seat(i);
                pthread_mutex_lock(&lock);
                H_counter++;
                pthread_mutex_unlock(&lock);
                struct timespec watch_time;
                clock_gettime(CLOCK_REALTIME, &watch_time);
                watch_time.tv_sec += X;
                int j = sem_timedwait(&watching[i], &watch_time);
                if (j == -1)
                {
                    watched_xmins(i);
                }
                else if (j == 0)
                {
                    bad_team_performance(i);
                }
                pthread_mutex_lock(&lock);
                H_counter--;
                pthread_mutex_unlock(&lock);
                for(int j=0; j<no_audience;j++)
                {
                    if(t>arriving_time[j] && waiting_status[j]==1)
                    {
                        if(support[j][0] == 'H')
                        {
                            sem_post(&waiting[j]);
                        }
                    }
                }
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
        }
    }
    if (support[i][0] == 'A')
    {
        if(A_counter <A)
        {
            zone[i][0] = 'A';
            got_seat(i);
            pthread_mutex_lock(&lock);
            A_counter++;
            pthread_mutex_unlock(&lock);
            struct timespec watch_time;
            clock_gettime(CLOCK_REALTIME, &watch_time);
            watch_time.tv_sec += X;
            int j = sem_timedwait(&watching[i], &watch_time);
            if (j == -1)
            {
                watched_xmins(i);
            }
            else if (j == 0)
            {
                bad_team_performance(i);
            }
            pthread_mutex_lock(&lock);
            A_counter--;
            pthread_mutex_unlock(&lock);
            for(int j=0; j<no_audience;j++)
            {
                if(t>arriving_time[j] && waiting_status[j]==1)
                {
                    if(support[j][0] == 'A')
                    {
                        sem_post(&waiting[j]);
                    }
                }
            }
            pthread_mutex_lock(&lock);
            end_of_threads++;
            pthread_mutex_unlock(&lock);
            person_leaving_simulation(i);
            return NULL;
        }
        else
        {
            waiting_status[i] = 1;
            struct timespec abs_time;
            clock_gettime(CLOCK_REALTIME, &abs_time);
            abs_time.tv_sec += patience[i];
            int j = sem_timedwait(&waiting[i], &abs_time);
            if (j == -1)
            {
                waiting_status[i] = 0;
                couldnt_seat(i);
                pthread_mutex_lock(&lock);
                end_of_threads++;
                pthread_mutex_unlock(&lock);
                person_leaving_simulation(i);
                return NULL;
            }
            else if (j == 0)
            {
                waiting_status[i] = 0;
                got_seat(i);
                pthread_mutex_lock(&lock);
                A_counter++;
                pthread_mutex_unlock(&lock);
            }
        }
    }
    pthread_mutex_lock(&lock);
    end_of_threads++;
    pthread_mutex_unlock(&lock);
}
void Send_player_out_R(char team,int No_of_goals)
{
    for(int i=0;i<no_audience;i++)
    {
        if(R_goals[i]==No_of_goals && team==support[i][0] && t>arriving_time[i] && waiting_status[i]==0)
        sem_post(&watching[i]);
    }
}
void* goal_function(void* arg)
{
    int no_of_goal_events = *(int *)arg;

    for (int i = 0; i < no_of_goal_events; i++)
    {
        
        while(t<goal_time[i])
        {
            sleep(1);
        }
        int number = (rand() % 100);
        int upper_bound = (int)(goal_probability[i] * 100);
        if (number < upper_bound) //true for probability
        {
            if (goal_team[i][0] == 'H')
            {
                Home_team_scored_yth_goal();
                Send_player_out_R(goal_team[i][0],no_of_H_goals);
            }
            else if (goal_team[i][0] == 'A')
            {
                Away_team_scored_yth_goal();
                Send_player_out_R(goal_team[i][0],no_of_A_goals);
            }
        }
        else
        {
            if (goal_team[i][0] == 'H')
            {
                Home_missed_yth_goal();
            }
            else if (goal_team[i][0] == 'A')
            {
                Away_missed_yth_goal();
            }
        }
    }
    pthread_mutex_lock(&lock);
    end_of_threads++;
    pthread_mutex_unlock(&lock);
}
void* time_loop(void* arg)
{
    while(1)
    {
        if(end_of_threads == no_audience+1)
        break;

        t++;
        sleep(1);

    }
}
int main()
{
    scanf("%d %d %d", &H, &A, &N);
    scanf("%d", &X);
    int num_grps;
    scanf("%d", &num_grps);
    int grp[num_grps];
    int j = 0;
    for (int i = 0; i < num_grps; i++)
    {
        scanf("%d", &grp[i]);
        for (int k = 0; k < grp[i]; k++)
        {
            scanf("%s %s %d %d %d", name[j], support[j], &arriving_time[j], &patience[j], &R_goals[j]);
            j++;
        }
    }
    no_audience = j;
    int num_goal_events;
    scanf("%d", &num_goal_events);
    for (int i = 0; i < num_goal_events; i++)
    {
        scanf("%s %d %f", goal_team[i], &goal_time[i], &goal_probability[i]);
    }
    /*for(int i = 0; i < no_audience; i++)
    {
        printf("%s %s %d %d %d\n ",name[i],support[i],arriving_time[i],patience[i],R_goals[i]);
    }
    for(int i=0;i<num_goal_events; i++)
    {
        printf("%s %d %f\n",goal_team[i],goal_time[i],goal_probability[i]);
    }*/
    //pthread_mutex_init(&lock,NULL);
    //---------------------------------------------------------------------------------------------------
    
        pthread_create(&time_counter,NULL,time_loop,NULL);

        pthread_create(&goal_counter,NULL,goal_function,&num_goal_events);

        for (int i = 0; i < no_audience; i++)
        {
            if (arriving_time[i] == t)
            {
                reached_stadium(i);
                pthread_create(&person_id[i], NULL, person_activity, &i);
            }
        }
        pthread_join(time_counter,NULL);

        //for(int i=0;i<no_audience;i++)
        {
        //    pthread_join(person_id[i],NULL);
        }
        //pthread_join(goal_counter,NULL);
    
}