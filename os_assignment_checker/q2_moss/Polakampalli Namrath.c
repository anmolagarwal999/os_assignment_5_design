
//###########FILE CHANGE ./main_folder/Polakampalli Namrath_305814_assignsubmission_file_/2020101063_assignment_5/q2/q2.c ####################//

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <semaphore.h>

#define NAME_SIZE 100
#define MAX_PEOPLE 100

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_MAGENTA    "\x1b[34m"
#define ANSI_COLOR_BLUE "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct
{
    char name[NAME_SIZE];
    char Type[2];       // type of fan
    int Time;           // time of arrival
    int patience;       //time to wait before leaving
    int opp_goal_limit; //to make them mad
    int seated;         //if they are seated
} details;

typedef struct
{
    char name[NAME_SIZE];
    char Type[2];       // type of fan
    int Time;           // time of arrival
    int patience;       //time to wait before leaving
    int opp_goal_limit; //to make them mad
    int seated;         //if they are seated
    bool arrived;
} person_struct;

typedef struct goal
{
    char Type[2];
    int time;
    int goal_or_not;
    float probability;
} goal_struct;
//person array for every person info to store
//zones struct for every zone info
typedef struct
{
    char type[2];
    int capacity;
    int current;
} zones;
// seat struct

typedef struct person_specifics
{
    int i;
    int j;
} person_specifics;

//Global Variables
details Grp_details[MAX_PEOPLE][MAX_PEOPLE];
zones zones_array[3];
person_struct person_array[MAX_PEOPLE];
pthread_mutex_t lock_array[1000];
sem_t sem_array[1000];
sem_t sem_array_goal[1000];
person_struct watching_list[MAX_PEOPLE];
goal_struct goal_array[MAX_PEOPLE];
int counter; //to find total people count
int home_goals, away_goals;
int X;

void seat_realloc(char type)
{
    if (type == 'H')
    {
        for (int i = 0; i < counter; i++)
        {
            if ((person_array[i].Type[0] == 'H' || person_array[i].Type[0] == 'N') && (person_array[i].seated == 0) && (person_array[i].arrived))
            {
                printf(ANSI_COLOR_YELLOW"%s has got a seat in zone H\n"ANSI_COLOR_RESET, person_array[i].name);
                person_array[i].seated = 1;
                sem_post(&sem_array[i]);
                zones_array[0].current++;
                break;
            }
        }
    }
    else if (type == 'A')
    {
        for (int i = 0; i < counter; i++)
        {
            if ((person_array[i].Type[0] == 'A' || person_array[i].Type[0] == 'N') && (person_array[i].seated == 0) && (person_array[i].arrived))
            {
                printf(ANSI_COLOR_YELLOW"%s has got a seat in zone A\n"ANSI_COLOR_RESET, person_array[i].name);
                person_array[i].seated = 1;
                sem_post(&sem_array[i]);
                zones_array[1].current++;
                break;
            }
        }
    }
    else if (type == 'N')
    {
        for (int i = 0; i < counter; i++)
        {
            if ((person_array[i].seated == 0) && (person_array[i].Type[0] == 'H' || person_array[i].Type[0] == 'N') && (person_array[i].arrived))
            {
                printf(ANSI_COLOR_YELLOW"%s has got a seat in zone N\n"ANSI_COLOR_RESET, person_array[i].name);
                person_array[i].seated = 1;
                sem_post(&sem_array[i]);
                zones_array[2].current++;
                break;
            }
        }
    }
}

//Function Prototypes
void *person_function(void *arg)
{
    int i = *(int *)arg;
    char seating_type;
    //printf("i and j are %d %d\n",i,j);
    //identified by i and j
    int t_arrival = person_array[i].Time; //arrival time
    sleep(t_arrival);

    printf(ANSI_COLOR_RED"%s has reached the stadium\n"ANSI_COLOR_RESET, person_array[i].name);
    char type = person_array[i].Type[0];
    //sleep(1);
    //use lock here//
    pthread_mutex_lock(&lock_array[i]);
    if (type == 'H')
    {
        if (zones_array[0].current < zones_array[0].capacity)
        {
            zones_array[0].current++;
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone H\n"ANSI_COLOR_RESET, person_array[i].name);
            person_array[i].seated = true;
            seating_type = 'H';
        }
        else if (zones_array[2].current < zones_array[2].capacity)
        {
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone N\n"ANSI_COLOR_RESET, person_array[i].name);
            seating_type = 'N';
            zones_array[2].current++;
            person_array[i].seated = true;
        }
    }
    else if (type == 'N')
    {
        if (zones_array[2].current < zones_array[2].capacity)
        {
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone N\n"ANSI_COLOR_RESET, person_array[i].name);
            seating_type = 'N';
            zones_array[2].current++;
            person_array[i].seated = true;
        }
        else if (zones_array[1].current < zones_array[1].capacity)
        {
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone A\n"ANSI_COLOR_RESET, person_array[i].name);
            seating_type = 'A';
            zones_array[1].current++;
            person_array[i].seated = true;
        }
        else if (zones_array[0].current < zones_array[0].capacity)
        {
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone H\n"ANSI_COLOR_RESET, person_array[i].name);
            seating_type = 'H';
            zones_array[0].current++;
            person_array[i].seated = true;
        }
    }
    else if (type == 'A')
    {
        if (zones_array[1].current < zones_array[1].capacity)
        {
            printf(ANSI_COLOR_YELLOW"%s has got a seat in zone A\n"ANSI_COLOR_RESET, person_array[i].name);
            seating_type = 'A';
            zones_array[1].current++;
            person_array[i].seated = true;
        }
    }
    //release lock here//
    pthread_mutex_unlock(&lock_array[i]);
    //if occupied then look to finish in X time or leave due to max goals
    if (person_array[i].seated == true)
    {
        //add to watching list
    }
    else
    {
        //sem_timed_wait(&sem_array[i]);
        person_array[i].arrived = true;
        struct timespec patience_struct;
        clock_gettime(CLOCK_REALTIME, &patience_struct);
        patience_struct.tv_sec += person_array[i].patience - 1;
        patience_struct.tv_sec += 1;
        if (sem_timedwait(&sem_array[i], &patience_struct) == -1)
        {
            printf(ANSI_COLOR_MAGENTA"%s could not get a seat\n"ANSI_COLOR_RESET, person_array[i].name);
            person_array[i].seated = -1;
            return NULL;
        }
        //add to watching list
        //make bool true
        //if it exits the else loop it must have obtained a seat from seat_release function
    }
    //if not occupied then look to get a seat within patience time

    //sem_timedwait(&sem_array[i]); //time worth X seconds
    struct timespec X_struct;
    clock_gettime(CLOCK_REALTIME, &X_struct);
    X_struct.tv_sec += X - 1;
    X_struct.tv_sec += 1;
    if (sem_timedwait(&sem_array_goal[i], &X_struct) != -1) // -1 is timed wait else signal
    {
        printf(ANSI_COLOR_MAGENTA"%s is leaving due to bad performance of his team \n"ANSI_COLOR_RESET, person_array[i].name);
        person_array[i].seated = -1;

        pthread_mutex_lock(&lock_array[i]);
        if (seating_type == 'H')
        {
            zones_array[0].current--;
        }
        else if (seating_type == 'A')
        {
            zones_array[1].current--;
        }
        else if (seating_type == 'N')
        {
            zones_array[2].current--;
        }
        pthread_mutex_unlock(&lock_array[i]);

        seat_realloc(seating_type);
        return NULL;
    }
    printf(ANSI_COLOR_MAGENTA"%s watched the match for %d seconds and is leaving\n"ANSI_COLOR_RESET, person_array[i].name, X);
    person_array[i].seated = -1;
    //decrement the seat zone curr
    //lock
    pthread_mutex_lock(&lock_array[i]);
    if (seating_type == 'H')
    {
        zones_array[0].current--;
    }
    else if (seating_type == 'A')
    {
        zones_array[1].current--;
    }
    else if (seating_type == 'N')
    {
        zones_array[2].current--;
    }
    pthread_mutex_unlock(&lock_array[i]);
    //unlock
    seat_realloc(seating_type);
    return NULL;
}

void goal_check(char type)
{
    //checks if any of the opposite type spectators are to be removed
    if (type == 'H')
    {
        for (int i = 0; i < counter; i++)
        {
            //parse thru all persons
            if ((person_array[i].Type[0] == 'A') && (person_array[i].seated == 1) && (home_goals >= person_array[i].opp_goal_limit))
            {
                //printf("%s of type %s has been posted\n",person_array[i].name,person_array[i].)
                sem_post(&sem_array_goal[i]); //send signal to release to that person who is curretnly in timed wait for X seconds
            }
        }
    }
    else if (type == 'A')
    {
        for (int i = 0; i < counter; i++)
        {
            //parse thru all persons
            if ((person_array[i].Type[0] == 'H') && (person_array[i].seated == 1) && (away_goals >= person_array[i].opp_goal_limit))
            {
                sem_post(&sem_array[i]); //send signal to release to that person who is curretnly in timed wait for X seconds
            }
        }
    }
}

void *goal_function(void *arg)
{
    int chance_count = *(int *)arg;
    for (int i = 0; i < chance_count; i++)
    {
        int time_chance;
        if (i > 0)
        {
            time_chance = goal_array[i].time - goal_array[i - 1].time;
        }
        else
        {
            time_chance = goal_array[0].time;
        }
        sleep(time_chance); //wait until ur chance time comes
        int number = (rand() % 100);
        int upper_bound = (int)(goal_array[i].probability * 100);
        //insert lock here
        if (number < upper_bound) //true for probability
        {

            goal_array[i].goal_or_not = true;
            if (goal_array[i].Type[0] == 'H')
            {
                home_goals++;
                printf(ANSI_COLOR_GREEN"%s has scored their %d th goal\n"ANSI_COLOR_RESET, goal_array[i].Type, home_goals);
            }
            else if (goal_array[i].Type[0] == 'A')
            {
                away_goals++;
                printf(ANSI_COLOR_GREEN"%s has scored their %d th goal\n"ANSI_COLOR_RESET, goal_array[i].Type, away_goals);
            }

            //unlock here
            //printf("type passed to goal_check is %c\n",goal_array[i].Type[0]);
            goal_check(goal_array[i].Type[0]);
        }
        else
        {
            printf(ANSI_COLOR_GREEN"%s has missed\n"ANSI_COLOR_RESET, goal_array[i].Type);
            goal_array[i].goal_or_not = false;
            //unlock here
            //goal_check(goal_array[i].Type[0]);
        }
    }
}

int main()
{
    int H_capacity, A_capacity, N_capacity;
    home_goals = 0;
    away_goals = 0;
    scanf("%d %d %d", &H_capacity, &A_capacity, &N_capacity);
    scanf("%d", &X);
    int grp_count;
    scanf("%d", &grp_count);

    int people_array[grp_count]; //array of no.of people in each group
    counter = 0;
    for (int i = 0; i < grp_count; i++)
    {
        int people_count;
        scanf("%d", &people_count);
        people_array[i] = people_count; //keeping track of per group people count
        for (int j = 0; j < people_count; j++)
        {
            scanf("%s %s %d %d %d", Grp_details[i][j].name, Grp_details[i][j].Type, &Grp_details[i][j].Time, &Grp_details[i][j].patience, &Grp_details[i][j].opp_goal_limit);
            strcpy(person_array[counter].name, Grp_details[i][j].name);
            strcpy(person_array[counter].Type, Grp_details[i][j].Type);
            person_array[counter].Time = Grp_details[i][j].Time;
            person_array[counter].patience = Grp_details[i][j].patience;
            person_array[counter].opp_goal_limit = Grp_details[i][j].opp_goal_limit;
            person_array[counter].seated = 0;

            counter++;
        }
    }
    //array to store goal chances created---
    int chance_count;
    scanf("%d", &chance_count);
    //printf("chance count is %d\n", chance_count);
    for (int i = 0; i < chance_count; i++)
    {
        scanf("%s %d %f", goal_array[i].Type, &goal_array[i].time, &goal_array[i].probability);
        fflush(stdout);
    }
    //initialising zones

    zones_array[0].capacity = H_capacity;
    zones_array[1].capacity = A_capacity;
    zones_array[2].capacity = N_capacity;
    zones_array[0].type[0] = 'H';
    zones_array[0].type[1] = '\0';
    zones_array[1].type[0] = 'A';
    zones_array[1].type[1] = '\0';
    zones_array[2].type[0] = 'N';
    zones_array[2].type[1] = '\0';
    zones_array[0].current = 0;
    zones_array[1].current = 0;
    zones_array[2].current = 0;

    pthread_t thread[1000];
    printf("\n");
    for (int i = 0; i < 1000; i++)
    {
        pthread_mutex_init(&lock_array[i], NULL);
    }
    //locks initialised

    for (int i = 0; i < counter; i++)
    {
        pthread_create(&thread[i], NULL, person_function, &i); //thread for every person
        usleep(100);
    }

    pthread_create(&thread[counter], NULL, goal_function, &chance_count);

    for (int i = 0; i < counter; i++)
    {
        pthread_join(thread[i], NULL);
    }
    pthread_join(thread[counter], NULL);
    return 0;
}