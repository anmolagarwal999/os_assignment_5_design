
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q2/globals.h ####################//

#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#include "spec.h"

extern sem_t H, A, N;
extern spec **SPECTATORS;
extern sem_t **spec_locks;
extern sem_t **spec_wait;

extern pthread_cond_t **match_wait;
extern pthread_mutex_t **spectator;

extern int g;    //number of goal events
extern int X, G; // spectating timer, number of groups
extern pthread_cond_t **group_wait;

#define BOLD "\033[1m"
#define NO_BOLD "\033[22m"
#define RED "\033[38;5;1m"
#define GREEN "\033[38;5;121m"
#define ORANGE "\033[38;5;209m"
#define PURPLE "\033[38;5;205m"
#define LIGHT_PINK "\033[38;5;225m"
#define LIGHT_PURPLE "\033[38;5;213m"
#define YELLOW "\033[38;5;223m"
#define RESET "\033[0m"
#define BLUE "\033[0;34m"

extern int *groups;

typedef struct goals goals;

struct goals
{
    float prob;
    char team;
    int time;
};

void *init_timer(void *arg);
void *init_spectators(void *arg);
extern goals *GOALS;
extern time_t clock_time;

#endif
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q2/main.c ####################//

#include "globals.h"

sem_t H, A, N;
spec **SPECTATORS;
int *groups;
goals *GOALS;

time_t clock_time;
sem_t **spec_locks;
sem_t **spec_wait;
int goal_index = 0;
int G_H = 0, G_A = 0;

pthread_cond_t **group_wait;

int X, G; // spectating timer, number of groups
int g;    //number of goal events

pthread_cond_t **match_wait;
pthread_mutex_t **spectator;

void *init_timer(void *arg)
{
    struct timespec ts;
    time_t timer;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        exit(0);
    }
    timer = ts.tv_sec;
    time_t origin = timer;
    goal_index = 0;
    while (1)
    {
        if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
        {
            perror("error");
            exit(0);
        }
        if (ts.tv_sec - timer >= 1)
        {
            clock_time = ts.tv_sec - origin;
            //printf("%ld\n", clock_time);
            timer = ts.tv_sec;
            if (goal_index < g)
            {
                if (clock_time == GOALS[goal_index].time)
                {

                    if (GOALS[goal_index].team == 'A')
                    {
                        srand(time(NULL));
                        if (((float)rand() / (float)RAND_MAX) <= GOALS[goal_index].prob)
                        {
                            G_A += 1;
                            printf("t=%ld: Team A has scored their %d goal\n", clock_time, G_A);
                        }
                        else
                        {
                            printf("t=%ld: Team A has missed the chance to score their %d goal\n", clock_time, G_A + 1);
                        }
                    }
                    else if (GOALS[goal_index].team == 'H')
                    {
                        srand(time(NULL));
                        if (((float)rand() / (float)RAND_MAX) <= GOALS[goal_index].prob)
                        {
                            G_H += 1;
                            printf("t=%ld: Team H has scored their %d goal\n", clock_time, G_H);
                        }
                        else
                        {
                            printf("t=%ld: Team H has missed the chance to score their %d goal\n", clock_time, G_H + 1);
                        }
                    }

                    goal_index++;
                }
            }
            for (int i = 0; i < G; i++)
            {
                for (int j = 0; j < groups[i]; j++)
                {
                    if (clock_time >= (SPECTATORS[i][j].etime))
                    {
                        while (SPECTATORS[i][j].state == watching_match)
                        {
                            pthread_cond_signal(&match_wait[i][j]);
                        }
                    }
                    else
                    {
                        if (SPECTATORS[i][j].zone == 2)
                        {
                            if (SPECTATORS[i][j].R == G_A)
                            {
                                while (SPECTATORS[i][j].state == watching_match)
                                {
                                    pthread_mutex_lock(&spectator[i][j]);
                                    SPECTATORS[i][j].enraged = 1;
                                    pthread_mutex_unlock(&spectator[i][j]);
                                    pthread_cond_signal(&match_wait[i][j]);
                                }
                            }
                        }
                        else if (SPECTATORS[i][j].zone == 1)
                        {
                            if (SPECTATORS[i][j].R == G_H)
                            {
                                while (SPECTATORS[i][j].state == watching_match)
                                {
                                    pthread_mutex_lock(&spectator[i][j]);
                                    SPECTATORS[i][j].enraged = 1;
                                    pthread_mutex_unlock(&spectator[i][j]);
                                    pthread_cond_signal(&match_wait[i][j]);
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < G; i++)
            {
                int cond = 0;
                for (int j = 0; j < groups[i]; j++)
                {
                    if (SPECTATORS[i][j].state == waiting_friends)
                    {
                        cond = 1;
                    }
                    else
                    {
                        cond = 0;
                        break;
                    }
                }
                if (cond == 1)
                {
                    for (int j = 0; j < groups[i]; j++)
                    {
                        while (SPECTATORS[i][j].state == waiting_friends)
                            pthread_cond_signal(&group_wait[i][j]);
                    }
                    printf(BOLD LIGHT_PINK "t=%ld: Group %d is leaving for dinner\n" RESET, clock_time, i + 1);
                }
            }
        }
    }
}

int main()
{

    int n1, n2, n3;
    int total = 0;
    scanf("%d %d %d", &n1, &n2, &n3);
    sem_init(&H, 0, n1);
    sem_init(&A, 0, n2);
    sem_init(&N, 0, n3);
    scanf("%d", &X);
    scanf("%d", &G);
    SPECTATORS = malloc(sizeof(spec *) * G);
    groups = malloc(sizeof(int) * G);
    spec_locks = malloc(sizeof(sem_t *) * G);
    spec_wait = malloc(sizeof(sem_t *) * G);
    match_wait = malloc(sizeof(pthread_cond_t *) * G);
    spectator = malloc(sizeof(pthread_mutex_t *) * G);
    group_wait = malloc(sizeof(pthread_cond_t) * G);
    for (int i = 0; i < G; i++)
    {
        int k; //number of ppl in the grp
        scanf("%d", &k);
        groups[i] = k;
        total += k;
        SPECTATORS[i] = malloc(sizeof(spec) * k);
        spec_locks[i] = malloc(sizeof(sem_t) * k);
        spec_wait[i] = malloc(sizeof(sem_t) * k);
        match_wait[i] = malloc(sizeof(pthread_cond_t) * k);
        spectator[i] = malloc(sizeof(pthread_mutex_t) * k);
        group_wait[i] = malloc(sizeof(pthread_cond_t) * k);

        for (int j = 0; j < k; j++)
        {
            char zone;
            SPECTATORS[i][j].name = malloc(sizeof(char) * 100);
            sem_init(&spec_locks[i][j], 0, 1);
            sem_init(&spec_wait[i][j], 0, 0);
            pthread_cond_init(&match_wait[i][j], NULL);
            pthread_mutex_init(&spectator[i][j], NULL);
            pthread_cond_init(&group_wait[i][j], NULL);

            scanf("%s %c %d %d %d", SPECTATORS[i][j].name, &zone, &SPECTATORS[i][j].ctime, &SPECTATORS[i][j].P, &SPECTATORS[i][j].R);
            if (zone == 'H')
                SPECTATORS[i][j].zone = 2;
            if (zone == 'A')
                SPECTATORS[i][j].zone = 1;
            if (zone == 'N')
                SPECTATORS[i][j].zone = 3;
            SPECTATORS[i][j].group = i;
            SPECTATORS[i][j].state = not_reached;
            SPECTATORS[i][j].id = j;
            SPECTATORS[i][j].enraged = 0;
        }
    }
    scanf("%d", &g);
    GOALS = malloc(sizeof(goals) * g);
    for (int i = 0; i < g; i++)
    {
        getchar();
        scanf("%c %d %f", &GOALS[i].team, &GOALS[i].time, &GOALS[i].prob);
    }

    pthread_t t;
    pthread_create(&t, NULL, init_timer, NULL);

    pthread_t **threads;
    threads = malloc(sizeof(pthread_t *) * G);

    for (int i = 0; i < G; i++)
    {
        threads[i] = malloc(sizeof(pthread_t) * groups[i]);
        for (int j = 0; j < groups[i]; j++)
        {
            int rc = pthread_create(&threads[i][j], NULL, init_spectators, &SPECTATORS[i][j]);
            assert(rc == 0);
        }
    }
    for (int i = 0; i < G; i++)
    {
        for (int j = 0; j < groups[i]; j++)
        {
            pthread_join(threads[i][j], NULL);
        }
    }
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q2/globals.c ####################//

#include "globals.h"

sem_t H, A, N;
spec **SPECTATORS;
int *groups;
goals *GOALS;

time_t clock_time;
sem_t **spec_locks;
sem_t **spec_wait;

pthread_cond_t **group_wait;

int g; //number of goal events
int X, G;

void *enterzoneH(void *arg)
{
    spec *s = (spec *)arg;

    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        perror("error");
        exit(0);
    }
    ts.tv_sec += (s->ctime - clock_time + s->P);
    int result = sem_timedwait(&H, &ts);
    int res = sem_trywait(&spec_locks[s->group][s->id]);
    if (res == 0 && result == 0)
    {
        if (s->state == waiting_seat)
        {
            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = watching_match;
            SPECTATORS[s->group][s->id].atime = clock_time;
            SPECTATORS[s->group][s->id].etime = SPECTATORS[s->group][s->id].atime + X;
            printf(BOLD BLUE "t=%ld: %s has got  a seat in zone H\n" RESET, clock_time, s->name);
            pthread_cond_wait(&match_wait[s->group][s->id], &spectator[s->group][s->id]);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }

        if (SPECTATORS[s->group][s->id].state == watching_match)
        {
            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = reached_gate;
            if (SPECTATORS[s->group][s->id].enraged == 1)
            {
                printf(BOLD RED "t=%ld: %s is leaving due to bad defensive performance of his team\n" RESET, clock_time, SPECTATORS[s->group][s->id].name);
            }
            else
                printf(BOLD RED "t=%ld: %s watched the match for %d seconds and is leaving\n" RESET, clock_time, SPECTATORS[s->group][s->id].name, X);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }
    }

    if (result == 0)
    {
        /*int value;
        sem_getvalue(&H, &value);
        printf("%s before H = %d ", s->name, value);*/
        sem_post(&H);
        //sem_getvalue(&H, &value);
        //printf("%s after H = %d ", s->name, value);
    }

    pthread_exit(NULL);
}
void *enterzoneA(void *arg)
{
    

    spec *s = (spec *)arg;
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        perror("error");
        exit(0);
    }
    ts.tv_sec += (s->ctime - clock_time + s->P);
    int result = sem_timedwait(&A, &ts);
    int res = sem_trywait(&spec_locks[s->group][s->id]);
    if (res == 0 && result == 0)
    {
        if (s->state == waiting_seat)
        {
            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = watching_match;
            SPECTATORS[s->group][s->id].atime = clock_time;
            SPECTATORS[s->group][s->id].etime = SPECTATORS[s->group][s->id].atime + X;
            printf(BOLD BLUE "t=%ld: %s has got  a seat in zone A\n" RESET, clock_time, s->name);
            pthread_cond_wait(&match_wait[s->group][s->id], &spectator[s->group][s->id]);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }

        if (SPECTATORS[s->group][s->id].state == watching_match)
        {

            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = reached_gate;
            if (SPECTATORS[s->group][s->id].enraged == 1)
            {
                printf(BOLD RED "t=%ld: %s is leaving due to bad defensive performance of his team\n" RESET, clock_time, SPECTATORS[s->group][s->id].name);
            }
            else
                printf(BOLD RED "t=%ld: %s watched the match for %d seconds and is leaving\n" RESET, clock_time, SPECTATORS[s->group][s->id].name, X);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }
    }

    if (result == 0)
    {
        /*int value;
        sem_getvalue(&A, &value);
        printf("%s before A = %d ", s->name, value);*/
        sem_post(&A);
        //sem_getvalue(&A, &value);
        //printf("%s after A = %d ", s->name, value);
    }

    pthread_exit(NULL);
}
void *enterzoneN(void *arg)
{
    spec *s = (spec *)arg;

    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        perror("error");
        exit(0);
    }
    ts.tv_sec += (s->ctime - clock_time + s->P);
    int result = sem_timedwait(&N, &ts);
    int res = sem_trywait(&spec_locks[s->group][s->id]);
    if (res == 0 && result == 0)
    {
        if (s->state == waiting_seat)
        {
            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = watching_match;
            SPECTATORS[s->group][s->id].atime = clock_time;
            SPECTATORS[s->group][s->id].etime = SPECTATORS[s->group][s->id].atime + X;
            printf(BOLD BLUE "t=%ld: %s has got  a seat in zone N\n" RESET, clock_time, s->name);
            pthread_cond_wait(&match_wait[s->group][s->id], &spectator[s->group][s->id]);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }

        if (SPECTATORS[s->group][s->id].state == watching_match)
        {
            pthread_mutex_lock(&spectator[s->group][s->id]);
            SPECTATORS[s->group][s->id].state = reached_gate;
            if (SPECTATORS[s->group][s->id].enraged == 1)
            {
                printf(BOLD RED "t=%ld: %s is leaving due to bad defensive performance of his team\n" RESET, clock_time, SPECTATORS[s->group][s->id].name);
            }
            else
                printf(BOLD RED "t=%ld: %s watched the match for %d seconds and is leaving\n" RESET, clock_time, SPECTATORS[s->group][s->id].name, X);
            pthread_mutex_unlock(&spectator[s->group][s->id]);
        }
    }

    if (result == 0)
    {
        /*int value;
        sem_getvalue(&N, &value);
        printf("%s before N = %d ", s->name, value);*/
        sem_post(&N);
        //sem_getvalue(&N, &value);
        //printf("%s after N = %d ", s->name, value);
    }

    pthread_exit(NULL);
}

void wait_seat(spec *arg)
{
    if (arg->zone == 1)
    {
        pthread_t t1;
        pthread_create(&t1, NULL, enterzoneA, arg);
        pthread_join(t1, NULL);
    }
    else if (arg->zone == 2)
    {
        pthread_t t1, t2;
        pthread_create(&t1, NULL, enterzoneH, arg);
        pthread_create(&t2, NULL, enterzoneN, arg);
        pthread_join(t1, NULL);
        pthread_join(t2, NULL);
    }
    else if (arg->zone == 3)
    {
        pthread_t t1, t2, t3;
        pthread_create(&t1, NULL, enterzoneH, arg);
        pthread_create(&t2, NULL, enterzoneA, arg);
        pthread_create(&t3, NULL, enterzoneN, arg);
        pthread_join(t1, NULL);
        pthread_join(t2, NULL);
        pthread_join(t3, NULL);
    }
    if (SPECTATORS[arg->group][arg->id].state == waiting_seat)
    {

        pthread_mutex_lock(&spectator[arg->group][arg->id]);
        SPECTATORS[arg->group][arg->id].state = reached_gate;
        pthread_mutex_unlock(&spectator[arg->group][arg->id]);
        printf(BOLD YELLOW "t=%ld: %s couldn't get a seat\n" RESET, clock_time, arg->name);
    }
    if (SPECTATORS[arg->group][arg->id].state == reached_gate)
    {
        pthread_mutex_lock(&spectator[arg->group][arg->id]);
        SPECTATORS[arg->group][arg->id].state = waiting_friends;
        printf(BOLD LIGHT_PURPLE "t=%ld: %s is waiting for their friends at the exit\n" RESET, clock_time, arg->name);
        pthread_cond_wait(&group_wait[arg->group][arg->id], &spectator[arg->group][arg->id]);
        SPECTATORS[arg->group][arg->id].state = exited;
        pthread_mutex_unlock(&spectator[arg->group][arg->id]);
        pthread_exit(NULL);
    }
}

void *init_spectators(void *arg)
{
    spec *s = (spec *)arg;

    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        perror("error");
        exit(0);
    }
    ts.tv_sec += s->ctime;
    sem_timedwait(&spec_wait[s->group][s->id], &ts);
    printf(BOLD GREEN "t=%ld: %s reached the stadium\n" RESET, clock_time, s->name);

    pthread_mutex_lock(&spectator[s->group][s->id]);
    SPECTATORS[s->group][s->id].state = waiting_seat;
    pthread_mutex_unlock(&spectator[s->group][s->id]);

    while (s->state == waiting_seat)
    {
        wait_seat(s);
    }
    pthread_exit(NULL);
}
//###########FILE CHANGE ./main_folder/ANIRUDH KAUSHIK_305852_assignsubmission_file_/q2/spec.h ####################//

#ifndef SPEC_H
#define SPEC_H

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

sem_t state[20];

typedef struct spec spec;
enum spec_state
{
    not_reached,
    waiting_seat,
    watching_match,
    reached_gate,
    waiting_friends,
    exited
    
};

struct spec
{
    char *name;
    int zone;
    int P;
    int ctime; //creation time/arrival time
    int atime; //seat allocation time
    int etime; //exit time
    int group;
    enum spec_state state;
    int R;
    int id;
    int enraged;
};

#endif