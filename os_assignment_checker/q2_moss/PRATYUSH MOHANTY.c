
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/Person.c ####################//

#include "global.h"
#include "Person.h"

void take_personInput()
{
    for (int i = 0; i < num_groups; i++)
    {
        groups[i].groupID = i;

        int no_of_people;
        scanf("%d", &no_of_people);

        groups[i].no_of_people = no_of_people;
        groups[i].person_in_grp = (person_ADT *)malloc(sizeof(person_ADT) * no_of_people);
        for (int j = 0; j < no_of_people; j++)
        {
            scanf("%s %c %d %d %d", groups[i].person_in_grp[j].name, &groups[i].person_in_grp[j].fanof_H_A_N, &groups[i].person_in_grp[j].reach_time, &groups[i].person_in_grp[j].patience_time, &groups[i].person_in_grp[j].enrage_value);

            // printf("_name=%s_ %c %d %d %d\n", groups[i].person_in_grp[j].name, groups[i].person_in_grp[j].fanof_H_A_N, groups[i].person_in_grp[j].reach_time, groups[i].person_in_grp[j].patience_time, groups[i].person_in_grp[j].enrage_value);

            groups[i].person_in_grp[j].groupID = i;
            groups[i].person_in_grp[j].is_at_exit = false;
            groups[i].person_in_grp[j].is_enraged = false;
            groups[i].person_in_grp[j].seat_receive_time = INVALID;
            groups[i].person_in_grp[j].seat_receive_zone = INVALID;
            groups[i].person_in_grp[j].seat_receive_zone = INVALID;
        }
    }
}
void create_personThread()
{
    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < groups[i].no_of_people; j++)
        {
            if (pthread_create(&groups[i].person_in_grp[j].personThread, NULL, person_thread_func, &groups[i].person_in_grp[j]) != 0)
            {
                printf("Error creating personThread\n");
                exit(1);
            }
            pthread_mutex_init(&groups[i].person_in_grp[j].person_mutex, NULL);
        }
    }
}
void join_personThread()
{
    for (int i = 0; i < num_groups; i++)
    {
        for (int j = 0; j < groups[i].no_of_people; j++)
        {
            pthread_join(groups[i].person_in_grp[j].personThread, NULL);
            pthread_mutex_destroy(&groups[i].person_in_grp[j].person_mutex);
        }
    }
}
struct timespec get_absoluteTime_1(int given_time)
{
    int time_now = time(NULL) - simulation_startTime;

    struct timeval val;
    struct timespec spec;

    int timeInMs = (given_time - (time_now + 1)) * 1000;
    gettimeofday(&val, NULL);

    spec.tv_sec = time(NULL) + timeInMs / 1000;
    spec.tv_nsec = val.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
    spec.tv_sec += spec.tv_nsec / (1000 * 1000 * 1000);
    spec.tv_nsec %= (1000 * 1000 * 1000);
}
struct timespec get_absoluteTime_2(int given_time)
{
    int time_now = time(NULL) - simulation_startTime;

    struct timeval val;
    struct timespec spec;

    int timeInMs = (given_time - (time_now)) * 1000;
    gettimeofday(&val, NULL);

    spec.tv_sec = time(NULL) + timeInMs / 1000;
    spec.tv_nsec = val.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
    spec.tv_sec += spec.tv_nsec / (1000 * 1000 * 1000);
    spec.tv_nsec %= (1000 * 1000 * 1000);
}
int allocate_seat(person_ADT *person)
{
    char fanof_H_A_N = person->fanof_H_A_N;
    if (fanof_H_A_N == 'H')
    {
        pthread_mutex_lock(&zone_mutex);
        if (zone_H_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_H_capacity--;
            person->seat_receive_zone = 'H';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone H\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        if (zone_N_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_N_capacity--;
            person->seat_receive_zone = 'H';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone N\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        pthread_mutex_unlock(&zone_mutex);
    }
    else if (fanof_H_A_N == 'A')
    {
        pthread_mutex_lock(&zone_mutex);
        if (zone_A_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_A_capacity--;
            person->seat_receive_zone = 'A';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone A\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        pthread_mutex_unlock(&zone_mutex);
    }
    else if (fanof_H_A_N == 'N')
    {
        pthread_mutex_lock(&zone_mutex);
        if (zone_H_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_H_capacity--;
            person->seat_receive_zone = 'H';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone H\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        if (zone_N_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_N_capacity--;
            person->seat_receive_zone = 'H';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone N\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        if (zone_A_capacity > 0)
        {
            int time_now = time(NULL) - simulation_startTime;

            zone_A_capacity--;
            person->seat_receive_zone = 'A';

            person->seat_receive_time = time_now;
            WHITE
            printf("t = %ld: %s has got a seat in zone A\n", time(NULL) - simulation_startTime, person->name);
            RESET
        }
        if (person->seat_receive_time != INVALID)
        {
            pthread_mutex_unlock(&zone_mutex);
            return INVALID;
        }
        pthread_mutex_unlock(&zone_mutex);
    }
    else
    {
        printf("Error in fanof_H_A_N\n");
        return INVALID;
        // exit(1);
    }
    return VALID;
}
int check_if_seatAllocated(person_ADT *person)
{
    if (person->seat_receive_time == INVALID)
    {
        pthread_mutex_lock(&zone_mutex);

        struct timespec abs_time = get_absoluteTime_1(person->patience_time + person->reach_time);

        if (pthread_cond_timedwait(&zone_cond, &zone_mutex, &abs_time) == 0)
        {
            pthread_mutex_unlock(&zone_mutex);
            return VALID;
        }
        else
        {
            pthread_mutex_unlock(&zone_mutex);

            int time_now = time(NULL) - simulation_startTime;
            int person_leave_time = person->reach_time + person->patience_time;

            if (time_now > person_leave_time)
            {
                PURPLE
                printf("%s could not get a seat\n", person->name);
                RESET
                return INVALID;
            }
        }
    }
    return VALID;
}
void leave_dueTo_badPerformance(person_ADT *person)
{
    if (person->seat_receive_time == INVALID)
    {
        return;
    }
    else
    {
        if (person->fanof_H_A_N == 'N')
        {
            sleep(spectating_time);
        }
        else if (person->fanof_H_A_N == 'H')
        {
            while (person->enrage_value > num_of_goalsBy_A)
            {
                pthread_mutex_lock(&scoreboard_mutex);
                struct timespec abs_time = get_absoluteTime_2(spectating_time + person->seat_receive_time);

                if (pthread_cond_timedwait(&scoreboard_cond, &scoreboard_mutex, &abs_time) == 0)
                {
                    pthread_mutex_unlock(&scoreboard_mutex);
                    continue;
                }
                else
                {
                    pthread_mutex_unlock(&scoreboard_mutex);
                    break;
                }
                pthread_mutex_unlock(&scoreboard_mutex);
            }
            if (person->enrage_value <= num_of_goalsBy_A)
            {
                person->is_enraged = true;
                printf("%d - E, %d - A, %d - H\n", person->enrage_value, num_of_goalsBy_A, num_of_goalsBy_H);
                RED
                    printf("t = %ld: %s is leaving due to the bad defensive performance of his team\n", time(NULL) - simulation_startTime, person->name);
                RESET
                return;
            }
        }
        else if (person->fanof_H_A_N == 'A')
        {
            while (person->enrage_value > num_of_goalsBy_H)
            {
                pthread_mutex_lock(&scoreboard_mutex);
                struct timespec abs_time = get_absoluteTime_2(spectating_time + person->seat_receive_time);

                if (pthread_cond_timedwait(&scoreboard_cond, &scoreboard_mutex, &abs_time) == 0)
                {
                    pthread_mutex_unlock(&scoreboard_mutex);
                    continue;
                }
                else
                {
                    pthread_mutex_unlock(&scoreboard_mutex);
                    break;
                }
                pthread_mutex_lock(&scoreboard_mutex);
            }
            if (person->enrage_value <= num_of_goalsBy_H)
            {
                person->is_enraged = true;
                RED
                    printf("t = %ld: %s is leaving due to the bad defensive performance of his team\n", time(NULL) - simulation_startTime, person->name);
                RESET
                return;
            }
        }
        else
        {
            RED
                printf("Error in leave_dueTo_badPerformance\n");
            RESET
            // exit(1);
        }
    }
}
void free_seat(person_ADT *person)
{
    int time_now = time(NULL) - simulation_startTime;

    pthread_mutex_lock(&zone_mutex);
    if (person->seat_receive_zone == 'H')
    {
        zone_H_capacity++;
        MAGENTA
        printf("t = %d: %s watched the match for %d seconds and is leaving\n", time_now, person->name, time_now);
        RESET
    }
    else if (person->seat_receive_zone == 'A')
    {
        zone_A_capacity++;
        MAGENTA
        printf("t = %d: %s watched the match for %d seconds and is leaving\n", time_now, person->name, time_now);
        RESET
    }
    else if (person->seat_receive_zone == 'N')
    {
        zone_N_capacity++;
        MAGENTA
        printf("t = %d: %s watched the match for %d seconds and is leaving\n", time_now, person->name, time_now);
        RESET
    }
    else
    {
        RED
            printf("Error in free_seat\n");
        RESET
        exit(1);
    }
    pthread_cond_broadcast(&zone_cond);
    pthread_mutex_unlock(&zone_mutex);

    person->is_at_exit = true;

    pthread_mutex_lock(&group_mutex);
    groups[person->groupID].no_of_people_at_exit++;

    CYAN
    printf("t = %ld: %s is waiting for their friends at the exit\n", time(NULL) - simulation_startTime, person->name);
    RESET

    if (groups[person->groupID].no_of_people_at_exit == groups[person->groupID].no_of_people)
    {
        GREEN
        printf("t = %ld: Group %d is leaving for dinner\n", time(NULL) - simulation_startTime, person->groupID + 1);
        RESET
    }
    pthread_cond_broadcast(&group_cond);
    pthread_mutex_unlock(&group_mutex);
}
void *person_thread_func(void *arg)
{

    person_ADT *person = (person_ADT *)arg;
    int reach_time = person->reach_time;

    sleep(reach_time);
    GREEN
    printf("t = %ld: %s has reached the stadium\n", time(NULL) - simulation_startTime, person->name);
    RESET

    while (true)
    {
        // CHANGE
        sleep(1);
        if (allocate_seat(person) == INVALID)
            break;

        // CHANGE
        sleep(1);
        if (check_if_seatAllocated(person) == VALID)
            continue;
        else
            return NULL;
    }

    leave_dueTo_badPerformance(person);
    free_seat(person);
    return NULL;
}
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/main.c ####################//

#include "global.h"

void init_simulation()
{
    pthread_mutex_init(&zone_mutex, NULL);
    pthread_mutex_init(&scoreboard_mutex, NULL);
    pthread_mutex_init(&group_mutex, NULL);
    
    pthread_cond_init(&zone_cond, NULL);
    pthread_cond_init(&scoreboard_cond, NULL);
    pthread_cond_init(&group_cond, NULL);

    simulation_startTime = time(NULL);
    num_of_goalsBy_A = 0;
    num_of_goalsBy_H = 0;
}
int main(int argc, char const *argv[])
{
    srand(time(NULL));
    groups = NULL;
    scoreBoard = NULL;

    scanf("%d %d %d", &zone_H_capacity, &zone_A_capacity, &zone_N_capacity);
    scanf("%d", &spectating_time);
    scanf("%d", &num_groups);

    groups = (group_ADT *)malloc(sizeof(group_ADT) * num_groups);
    take_personInput();

    scanf("%d", &num_of_goalscoring_chances);

    scoreBoard = (scoreBoard_ADT *)malloc(sizeof(scoreBoard_ADT));
    scoreBoard->goals_scored = (goal_ADT *)malloc(sizeof(goal_ADT) * num_of_goalscoring_chances);

    take_scoreGoalInput();
    // for (int i = 0; i < num_of_goalscoring_chances; i++)
    // {
    //     printf("%c %d %f\n", (scoreBoard->goals_scored[i]).team, (scoreBoard->goals_scored[i]).time_elapsed, (scoreBoard->goals_scored[i]).probability);
    // }
    init_simulation();

    create_personThread();
    create_scoreGoalThread();

    join_scoreGoalThread();
    join_personThread();
}

//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/Goals.h ####################//

#ifndef _SCORE_BOARD_H_
#define _SCORE_BOARD_H_

typedef struct
{
    char team;
    int time_elapsed;
    float probability;
} goal_ADT;
typedef struct
{
    goal_ADT *goals_scored;
} scoreBoard_ADT;

pthread_t scoreGoalThread;
int num_of_goalsBy_A;
int num_of_goalsBy_H;

void take_scoreGoalInput();

void create_scoreGoalThread();
void join_scoreGoalThread();

void *scoreGoal_thread_func(void *arg);

#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/Person.h ####################//

#ifndef _PERSON_H_
#define _PERSON_H_

typedef struct
{
    int groupID;
    char name[50];
    char fanof_H_A_N;

    int reach_time;
    int patience_time;
    
    int enrage_value;
    int is_enraged;

    int seat_receive_time;
    char seat_receive_zone;

    int is_at_exit;
    pthread_t personThread;
    pthread_mutex_t person_mutex;
} person_ADT;
typedef struct
{
    int groupID;
    int no_of_people;
    int no_of_people_at_exit;
    person_ADT *person_in_grp;
} group_ADT;

void take_personInput();

void create_personThread();
void join_personThread();

void *person_thread_func(void *arg);

#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/global.h ####################//

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "Person.h"
#include "Goals.h"

#define RED printf("\033[0;31m");
#define GREEN printf("\033[0;32m");
#define PURPLE printf("\033[0;35m");
#define YELLOW printf("\033[0;33m");
#define WHITE printf("\033[0;37m");
#define MAGENTA printf("\033[0;35m");
#define CYAN printf("\033[0;36m");
#define RESET printf("\033[0m");

int zone_H_capacity;
int zone_A_capacity;
int zone_N_capacity;

time_t simulation_startTime;

pthread_mutex_t group_mutex;
pthread_mutex_t zone_mutex;
pthread_mutex_t scoreboard_mutex;

pthread_cond_t group_cond;
pthread_cond_t zone_cond;
pthread_cond_t scoreboard_cond;

int num_groups;
int spectating_time;
int num_of_goalscoring_chances;

group_ADT *groups;
scoreBoard_ADT *scoreBoard;

#define VALID 1
#define INVALID -1

#define true 1
#define false 0

#endif
//###########FILE CHANGE ./main_folder/PRATYUSH MOHANTY_305848_assignsubmission_file_/q2/Goals.c ####################//

#include "global.h"
#include "Goals.h"

void take_scoreGoalInput()
{
    for (int i = 0; i < num_of_goalscoring_chances; i++)
    {
        char team;
        int time_elapsed;
        float probability;
        scanf("\n%c %d %f", &team, &time_elapsed, &probability);

        (scoreBoard->goals_scored[i]).team = team;
        (scoreBoard->goals_scored[i]).time_elapsed = time_elapsed;
        (scoreBoard->goals_scored[i]).probability = probability;
        // printf("%c %d %f\n", (scoreBoard->goals_scored[i]).team, (scoreBoard->goals_scored[i]).time_elapsed, (scoreBoard->goals_scored[i]).probability);
    }
}
void create_scoreGoalThread()
{
    if (pthread_create(&scoreGoalThread, NULL, scoreGoal_thread_func, &scoreBoard) != 0)
    {
        printf("Error creating goalThread\n");
        exit(1);
    }
}
void join_scoreGoalThread()
{
    pthread_join(scoreGoalThread, NULL);
}
void *scoreGoal_thread_func(void *arg)
{
    scoreBoard_ADT *scoreBoardtemp = (scoreBoard_ADT *)arg;
    for (int i = 0; i < num_of_goalscoring_chances; i++)
    {
        int prev_time_elapsed = 0;
        if (i > 0)
        {
            prev_time_elapsed = scoreBoard->goals_scored[i - 1].time_elapsed;
        }
        int curr_time_elapsed = scoreBoard->goals_scored[i].time_elapsed;
        sleep(curr_time_elapsed - prev_time_elapsed);

        if (scoreBoard->goals_scored[i].probability * 100 > rand() % 100)
        {
            pthread_mutex_lock(&scoreboard_mutex);
            if (scoreBoard->goals_scored[i].team == 'H')
            {
                YELLOW
                printf("t = %ld: Team %c has scored their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_H + 1);
                RESET
                num_of_goalsBy_H++;
            }
            else
            {
                YELLOW
                printf("t = %ld: Team %c has scored their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_A + 1);
                RESET
                num_of_goalsBy_A++;
            }
            pthread_cond_broadcast(&scoreboard_cond);
            pthread_mutex_unlock(&scoreboard_mutex);
        }
        else
        {
            RED if (scoreBoard->goals_scored[i].team == 'H')
                printf("t = %ld: Team %c missed the chance to score their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_H + 1);
            else printf("t = %ld: Team %c missed the chance to score their %dth goal\n", time(NULL) - simulation_startTime, scoreBoard->goals_scored[i].team, num_of_goalsBy_A + 1);
            RESET
        }
    }
}