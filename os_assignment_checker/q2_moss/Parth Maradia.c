
//###########FILE CHANGE ./main_folder/Parth Maradia_305821_assignsubmission_file_/q2/q2.c ####################//

// gcc q2.c -lpthread
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

// color codes
#define ANSI_CYAN "\033[1;36m"
#define ANSI_RED "\033[1;31m"
#define ANSI_GREEN "\033[1;32m"
#define ANSI_YELLOW "\033[1;33m"
#define ANSI_BLUE "\033[1;34m"
#define ANSI_MAGENTA "\033[1;35m"
#define ANSI_DEFAULT "\033[0m"
#define initi NULL

typedef int long long ll;

// structs of entities
typedef struct zones {  // 3 zones Home, Neutral, Away
    ll seats;
    ll filled;
}zones;

typedef struct spec{    // spectator struct
    char name[50];
    char type;
    ll time;
    ll rage_lim;
    ll patience;
    ll waiting;
    ll seated;
    ll exiting;
    char zone_alloted;
}spec;

typedef struct grp{     // group struct
    ll size;
    spec specs[500];
}grp;

typedef struct goal_prob{   // goal probability struct
    char type;
    ll start_time;
    float prob;
}goal_prob;

// function initialization 
void *spec_sim(void *arg);  
void *goal_sim(void *arg);
void init_mutex();
void init_sem();
void init_zones();
void init_specs(ll i, ll j);

ll num_groups, spectating_time, num_chances;
zones zoneA, zoneH, zoneN;  // zones for every catergory of fan

grp grps[500];      // array of groups

// keep track of goals and spectators
ll Home_goals = 0, Away_goals = 0, total_specs = 0;
//semaphores 
sem_t H_Empty, N_Empty, A_Empty, HN, HNA, A;
// mutex thread
pthread_mutex_t Away_lock, Home_lock, Neutral_lock,Home_goal_lock, Away_goal_lock;
// condition variables
pthread_cond_t Hgoal_cond, Agoal_cond;
// time variables
time_t start_time, current_time;

time_t time_from_start()    // function to get time from start
{
    current_time = time(NULL);
    return current_time - start_time;
}

void *spec_sim(void *arg)   // spectator simulation
{
    spec s = *(spec *)arg;
    // Arrives at the stadium
    sleep(s.time);
    printf(ANSI_DEFAULT "t=%ld : %s has reached the stadium\n", time_from_start(), s.name);

    struct timespec tspec1, tspec2;
    clock_gettime(CLOCK_REALTIME, &tspec1); // get current value of clock and stores it in variable

    tspec1.tv_sec += s.patience;        // adds patience to the time

    if (s.type == 'H')  // for home
    {
        
        ll k; 
        
        while ((k = sem_timedwait(&HN, &tspec1)) == -1 && errno != ETIMEDOUT) // waits for semaphore
        {
            continue;
        }

        if ((k < 0) && errno == ETIMEDOUT)  // couldnt find seat
        {
            printf(ANSI_MAGENTA "t=%ld : %s could not get a seat\n", time_from_start(), s.name);
            s.waiting = 0;
            s.seated = 0;
            goto exit_gate;
        }
        else if (!k)    // gets seat
        {
            char seat_zone;
            pthread_mutex_lock(&Home_lock);     // locks mutex for Home
            pthread_mutex_lock(&Neutral_lock);  // locks mutex for Neutral
            int val;

            if (zoneH.seats > zoneH.filled || zoneN.seats > zoneN.filled) 
            {
                bool flag1 = false, flag2 = false;  
                // sets flags according to availability of seats
                if(zoneH.seats - zoneH.filled)flag1 = true;
                if(zoneN.seats - zoneN.filled)flag2 = true;

                if(flag1 && flag2){     // if both are available, choose randomly
                    if(rand() % 2)
                        flag2 = true;
                    else
                        flag1 = false;
                }
                
                if (flag2)              // Neutral Zone Seat
                {
                    seat_zone = 'N';
                    zoneN.filled+=1;
                    sem_getvalue(&HNA, &val);
                    if (val)
                        sem_wait(&HNA);
                }
                else                    // Home Zone Seat
                {
                    seat_zone = 'H';
                    zoneH.filled+= 1;
                    sem_getvalue(&HNA, &val);
                    if (val)
                        sem_wait(&HNA);
                }
            }
            
            printf(ANSI_BLUE "t=%ld : %s has got a seat in zone %c\n", time_from_start(), s.name, s.zone_alloted);
            
            // unlock threads
            pthread_mutex_unlock(&Neutral_lock);    
            pthread_mutex_unlock(&Home_lock);
            
            // update spectator's attributes
            s.zone_alloted = seat_zone;
            s.seated = 1;
            s.time = time_from_start();
            s.waiting = 0;
        }

        // Watching the match

        clock_gettime(CLOCK_REALTIME, &tspec2); // store current time in variable
        
        tspec2.tv_sec += spectating_time;       // add spectating time to current time
        
        if (s.type == 'A')  // for Away fans
        {
            ll j;
            pthread_mutex_lock(&Away_goal_lock);    // locks mutex for Away goal
            
            ll t0 = time_from_start();          
            
            while (Away_goals < s.rage_lim && j != ETIMEDOUT)   // waits/sleeps thread till away goal condition broadcasted
                j = pthread_cond_timedwait(&Agoal_cond, &Away_goal_lock, &tspec2);
            
            ll t1 = time_from_start();  
            
            pthread_mutex_unlock(&Away_goal_lock);  // unlocks mutex for Away goal
            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finishes watch time so leaves seat
            {
                s.seated = 0;
                printf(ANSI_GREEN "t=%ld : %s watched the match for %lld seconds and is leaving\n", time_from_start(), s.name, spectating_time);
            }
            else  // gets angry
            {
                printf(ANSI_GREEN "t=%ld : %s is leaving due to poor performance of his team\n" , time_from_start(), s.name);
                s.seated = 0;
            }
        }
        else if (s.type == 'N') // neutral fan
        {
            sleep(spectating_time);
            s.seated = 0;
            printf(ANSI_GREEN "t=%ld : %s has watched the match for %lld seconds and is leaving :(\n", time_from_start(), s.name, spectating_time);
        }
        else // home fan
        {
            ll j;
            pthread_mutex_lock(&Home_goal_lock);
            ll t0 = time_from_start();

            while (Home_goals < s.rage_lim && j != ETIMEDOUT)   // waits/sleeps thread till home goal condition broadcasted
                j = pthread_cond_timedwait(&Hgoal_cond, &Home_goal_lock, &tspec2);
            
            ll t1 = time_from_start();
            
            pthread_mutex_unlock(&Home_goal_lock);
            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finishes watch time
            {
                s.seated = 0;
                printf(ANSI_GREEN "t=%ld : %s watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
            }
            else    // gets angry
            {
                printf(ANSI_GREEN "t=%ld : %s is leaving due to poor performance of his team\n", time_from_start(), s.name);
                s.seated = 0;
            }
        }

        // updates zones capacity filled
        if (s.zone_alloted == 'N')      
        {
            pthread_mutex_lock(&Neutral_lock);
            zoneN.filled-=1;
            pthread_mutex_unlock(&Neutral_lock);
            sem_post(&HNA);
            sem_post(&HN);
        }
        else if(s.zone_alloted == 'A'){
            pthread_mutex_lock(&Away_lock);
            zoneA.filled-=1;
            pthread_mutex_unlock(&Away_lock);
            sem_post(&HNA);
            sem_post(&A);
        }
        else
        {
            pthread_mutex_lock(&Home_lock);
            zoneH.filled-=1;
            pthread_mutex_unlock(&Home_lock);
            sem_post(&HN);
            sem_post(&HNA);
        }
    }

    if (s.type == 'N')  // for Neutral fan
    {

        ll k;

        while ((k = sem_timedwait(&HNA, &tspec1)) == -1 && errno != ETIMEDOUT)  
        {
            continue;
        }
        
        int val;
        
        if (!k)
        {
            char seat_zone;
            // locks home , neutral, away mutexes
            pthread_mutex_lock(&Home_lock);
            pthread_mutex_lock(&Neutral_lock);
            pthread_mutex_lock(&Away_lock);

            if (zoneH.seats > zoneH.filled || zoneN.seats > zoneN.filled || zoneA.seats - zoneA.filled > 0)
            {
                bool flag1 = false, flag2 = false, flag3 = false;
                // sets flags according to availability of seats
                if(zoneH.seats - zoneH.filled)flag1 = true;
                if(zoneN.seats - zoneN.filled)flag2 = true;
                if(zoneA.seats - zoneA.filled)flag3 = true;

                if (flag1 && flag2 && flag3){   // if all are available, choose randomly
                    ll randn = rand() % 3;
                    if(randn == 0){flag1 = true;flag2 = false;flag3 = false;}
                    else if(randn == 1){flag2 = true;flag1 = false;flag3 = false;}
                    else {flag3 = true;flag1 = false;flag2 = false;}
                }
                else if(flag1 && flag2 && !flag3){  // if only home and neutral are available, choose randomly
                    ll randn = rand() % 2;
                    if(randn == 0){flag1 = true;flag2 = false;}
                    else {flag2 = true;flag1 = false;}
                }
                else if(flag1 && !flag2 && flag3){  // if only home and away are available, choose randomly
                    ll randn = rand() % 2;
                    if(randn == 0){flag1 = true;flag3 = false;}
                    else {flag3 = true;flag1 = false;}
                }
                else if(!flag1 && flag2 && flag3){  // if only neutral and away are available, choose randomly
                    ll randn = rand() % 2;
                    if(randn == 0){flag2 = true;flag3 = false;}
                    else {flag3 = true;flag2 = false;}
                }
                // allots seats based on above flags selection
                if (flag1)      // home
                {
                    seat_zone = 'H';    
                    zoneH.filled += 1;
                    sem_getvalue(&HN, &val);
                    if (val > 0)
                        sem_wait(&HN);
                }
                else if (flag2)     // neutral
                {
                    seat_zone = 'N';
                    zoneN.filled += 1;
                    sem_getvalue(&HN, &val);
                    if (val > 0)
                        sem_wait(&HN);
                }
                else if(flag3)    // away
                {
                    seat_zone = 'A';
                    zoneA.filled += 1;
                    sem_getvalue(&A, &val);
                    if (val > 0)
                        sem_wait(&A);
                }
            }
            
            printf(ANSI_BLUE "t=%ld : %s has got a seat in zone %c\n", time_from_start(), s.name, s.zone_alloted);
            // unlock all mutex locks
            pthread_mutex_unlock(&Away_lock);
            pthread_mutex_unlock(&Neutral_lock);
            pthread_mutex_unlock(&Home_lock);
            // update attributes of spectator
            s.zone_alloted = seat_zone;
            s.waiting = 0;
            s.time = time_from_start();
            s.seated = 1;
        }
        {
            printf(ANSI_MAGENTA "t=%ld : %s could not get a seat\n",time_from_start(), s.name);
            s.waiting = 0;
            s.seated = 0;
            goto exit_gate;
        }

        clock_gettime(CLOCK_REALTIME, &tspec2);     // stores current time to variable

        tspec2.tv_sec += spectating_time;           // adds watch time to current time
        
        if (s.type == 'H')      // for home fans
        {
            ll j;
            pthread_mutex_lock(&Home_goal_lock);    

            ll t0 = time_from_start();

            while (Home_goals < s.rage_lim && j != ETIMEDOUT)   // waits for home goal contition to be broadcasted
                j = pthread_cond_timedwait(&Hgoal_cond, &Home_goal_lock, &tspec2);
            
            ll t1 = time_from_start();
            
            pthread_mutex_unlock(&Home_goal_lock);

            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finished watching match for watch time
            {
                s.seated = 0;
                printf(ANSI_GREEN "t=%ld : %s watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
            }
            else    // gets angry
            {
                printf(ANSI_GREEN "t=%ld : %s is leaving due to poor performance of his team\n", time_from_start(), s.name);
                s.seated = 0;
            }
            
        }
        else if (s.type == 'N')    // for neutral fans, finishes watch time and leaves 
        {
            sleep(spectating_time);
            s.seated = 0;
            printf(ANSI_BLUE "t=%ld : %s has watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
        }
        else        // for away fans
        {
            ll j;

            pthread_mutex_lock(&Away_goal_lock);
            
            ll t0 = time_from_start();
            
            while (Away_goals < s.rage_lim && j != ETIMEDOUT)   // waits for away goal contition to be broadcasted
                j = pthread_cond_timedwait(&Agoal_cond, &Away_goal_lock, &tspec2);
            
            ll t1 = time_from_start();
            
            pthread_mutex_unlock(&Away_goal_lock);
            
            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finished watching match for watch time
            {
                s.seated = 0;
                printf(ANSI_BLUE "t=%ld : %s watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
            }
            else // gets angry
            {
                printf(ANSI_RED "t=%ld : %s is leaving due to poor performance of his team\n" , time_from_start(), s.name);
                s.seated = 0;
            }
        }

        // zone allocation and seats filled capacity update
        if (s.zone_alloted == 'N')
        {
            pthread_mutex_lock(&Neutral_lock);
            zoneN.filled-=1;
            pthread_mutex_unlock(&Neutral_lock);
            sem_post(&HN);
            sem_post(&HNA);
        }
        else if (s.zone_alloted == 'H')
        {
            pthread_mutex_lock(&Home_lock);
            zoneH.filled-=1;
            pthread_mutex_unlock(&Home_lock);
            sem_post(&HN);
            sem_post(&HNA);
        }
        else
        {
            pthread_mutex_lock(&Away_lock);
            zoneA.filled-=1;
            pthread_mutex_unlock(&Away_lock);
            sem_post(&A);
            sem_post(&HNA);
        }
    }

    if (s.type == 'A')  // for away 
    {
        ll k;

        while ((k = sem_timedwait(&A, &tspec1)) == -1 && errno != ETIMEDOUT)
        {
            continue;
        }

        int val;
        
        if (!k)     // if got a seat
        {
            // increament seats filled
            pthread_mutex_lock(&Away_lock);
            zoneA.filled+=1;
            pthread_mutex_unlock(&Away_lock);
            
            sem_getvalue(&HNA, &val); // stores value of HNA semaphore to val

            if (val)            // waits if HNA semaphore is not zero
                sem_wait(&HNA);
            
            // updates attributes of spectator
            s.zone_alloted = 'A';   
            s.seated = 1;
            s.time = time_from_start();
            s.waiting = 0;
    
            printf(ANSI_GREEN "t=%ld : %s has got a seat in zone %c\n" , time_from_start(), s.name, s.zone_alloted);
        }
        else if ((k == -1) && errno == ETIMEDOUT)       // didnt get a seat
        {
            printf(ANSI_RED "t=%ld : %s could not get a seat\n" , time_from_start(), s.name);
            s.waiting = 0;
            s.seated = 0;
            goto exit_gate;
        }

        clock_gettime(CLOCK_REALTIME, &tspec2);     // stores current time to variable
        
        tspec2.tv_sec += spectating_time;           // adds watch time to current time
        
        if (s.type == 'A')      // for away fans  
        {
            ll j;
            pthread_mutex_lock(&Away_goal_lock);
            
            ll t0 = time_from_start();

            while (Away_goals < s.rage_lim && j != ETIMEDOUT)        // waits for away goal contition to be broadcasted
                j = pthread_cond_timedwait(&Agoal_cond, &Away_goal_lock, &tspec2);
            
            ll t1 = time_from_start();
            
            pthread_mutex_unlock(&Away_goal_lock);
            
            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT)      // finished watching match for watch time
            {
                s.seated = 0;
                printf(ANSI_BLUE "t=%ld : %s watched the match for %lld seconds and is leaving\n", time_from_start(), s.name, spectating_time);
            }
            else   // gets angry
            {
                printf(ANSI_RED "t=%ld : %s is leaving due to poor performance of his team\n" , time_from_start(), s.name);
                s.seated = 0;
            }
        }
        else if (s.type == 'H')     // for home fans
        {
            ll j;
            
            pthread_mutex_lock(&Home_goal_lock);
            
            ll t0 = time_from_start();
            
            while (Home_goals < s.rage_lim && j != ETIMEDOUT)       // waits for home goal contition to be broadcasted
                j = pthread_cond_timedwait(&Hgoal_cond, &Home_goal_lock, &tspec2);
            
            ll t1 = time_from_start();
            
            pthread_mutex_unlock(&Home_goal_lock);
            
            if ((t1 - t0) >= spectating_time || j == ETIMEDOUT) // finished watching match for watch time
            {
                s.seated = 0;
                printf(ANSI_GREEN "t=%ld : %s watched the match for %lld seconds and is leaving\n" , time_from_start(), s.name, spectating_time);
            }
            else    // gets angry
            {
                printf(ANSI_RED "t=%ld : %s is leaving due to poor performance of his team\n" , time_from_start(), s.name);
                s.seated = 0;
            }
        }
        else // Neutral fan finished watching match for watch time
        {
            sleep(spectating_time);
            s.seated = 0;
            printf(ANSI_BLUE "t=%ld : %s watched the match for %lld seconds and is leaving\n", time_from_start(), s.name, spectating_time);
        }

        // unlock semaphores
        sem_post(&A);
        sem_post(&HNA);
    }
// exits stadium and updaes attributes
exit_gate:
    s.exiting = 1;
    printf(ANSI_YELLOW "t=%ld : %s is leaving for dinner\n" , time_from_start(), s.name);
    printf(ANSI_MAGENTA"t=%ld : reached exit gate\n", time_from_start());
}

void *goal_sim(void *args)          // goal simulation
{
    goal_prob *g = (goal_prob *)args;   // stores arguments to g

    for (ll i = 0; i < num_chances; i++)
    {
        ll current_time = time_from_start(),time_to_wait = g[i].start_time - current_time;  // stores current time and wait time
       
        if (time_to_wait)           // waits if wait time is not zero
            sleep(time_to_wait);

        float max = (float)RAND_MAX;    // stores max value of random number generator
        float randn = (float)rand();    // stores random number
        float probability = randn / max; // stores probability of goal in float
        
        char goals_count[20] = "";      // stores goals count in words

        if (g[i].type == 'H')       // home goal count
        {
            pthread_mutex_lock(&Home_goal_lock);
            bool f = true;
            for(int i = 0;i < 4;i++){
                if(Home_goals == i){
                    f = false;
                    if(i == 2)strcat(goals_count, "3rd");
                    else if(i == 1)strcat(goals_count, "2nd");
                    else if(i == 0)strcat(goals_count, "1st");
                    break;
                }
            }
            if(f){
                char char_num = Home_goals+'0';
                strcat(goals_count, &char_num);
                strcat(goals_count,"th");
            }
            pthread_mutex_unlock(&Home_goal_lock);
        }
        else    // away goal count
        {
            pthread_mutex_lock(&Away_goal_lock);
            bool f = true;
            for(int i = 0;i < 4;i++){
                if(Away_goals == i){
                    f = false;
                    if(i == 2)strcat(goals_count, "3rd");
                    else if(i == 1)strcat(goals_count, "2nd");
                    else if(i == 0)strcat(goals_count, "1st");
                    break;
                }
            }
            if(f){
                char char_num = Away_goals+'0';
                strcat(goals_count, &char_num);
                strcat(goals_count,"th");
            }
            pthread_mutex_unlock(&Away_goal_lock);
        }
        // checks if goal is scored
        if (probability <= g[i].prob)
        {
            char team[20] = "";
            if(g[i].type == 'H')strcpy(team, "Home");
            else strcpy(team, "Away");

            printf(ANSI_CYAN "t=%ld : %s Team has scored their %s goal\n", time_from_start(), team, goals_count);
            // updates goal count and broadcasts goal condition
            if (g[i].type == 'A')
            {
                pthread_mutex_lock(&Away_goal_lock);
                Away_goals+=1;
                pthread_cond_broadcast(&Agoal_cond);
                pthread_mutex_unlock(&Away_goal_lock);
                
            }
            else
            {
                pthread_mutex_lock(&Home_goal_lock);
                Home_goals+=1;
                pthread_cond_broadcast(&Hgoal_cond);
                pthread_mutex_unlock(&Home_goal_lock);
            }
        }
        else        // team missed goal chance
        {
            printf(ANSI_CYAN"t=%ld : Team %c missed the chance to score their %s goal\n", time_from_start(), g[i].type, goals_count);
        }
    }
}

void init_grp(ll i , ll j){     // initializes group
    grps[i].specs[j].time = 0;
    grps[i].specs[j].waiting = 1;
    grps[i].specs[j].seated = 0;
    grps[i].specs[j].exiting = 0;
}

void init_mutex(){   // initializes mutexes
    pthread_mutex_init(&Home_goal_lock, initi); // Lock for accesing goals
    pthread_mutex_init(&Away_goal_lock, initi);

    pthread_mutex_init(&Home_lock, initi); // Lock for accessing zone
    pthread_mutex_init(&Neutral_lock, initi);
    pthread_mutex_init(&Away_lock, initi);
}

void init_sem(){    // initializes semaphores
    sem_init(&HN, 0, zoneH.seats + zoneN.seats);
    sem_init(&HNA, 0, zoneH.seats + zoneN.seats + zoneA.seats);
    sem_init(&A, 0, zoneA.seats);
}

void init_cond(){   // initializes condition variables
    pthread_cond_init(&Hgoal_cond, initi);
    pthread_cond_init(&Agoal_cond, initi);
}

int main()
{
    srand(time(NULL));
    // TAKES INPUT BASED ON CONDITIONS GIVEN
    scanf("%lld %lld %lld %lld %lld", &zoneH.seats , &zoneN.seats , &zoneA.seats, &spectating_time, &num_groups);
    
    for (ll i = 0; i < num_groups; i++)
    {
        ll size_of_grp;
        scanf("%lld", &size_of_grp);
        for (ll j = 0; j < size_of_grp; j++)
        {
            init_grp(i, j);
            char type;
            ll time,pat,rage;            
            scanf("%s %c %lld %lld %lld", grps[i].specs[j].name, &type, &time, &pat, &rage);
            grps[i].specs[j].type = type;
            grps[i].specs[j].time = time;
            grps[i].specs[j].patience = pat;
            grps[i].specs[j].rage_lim = rage;
        }
        grps[i].size = size_of_grp;
    }
    scanf("%lld", &num_chances);
    // goal prob arrays
    goal_prob goal_chnc[num_chances], chance_arr[500];

    // takes in goal probabilities input
    for (ll i = 0; i < num_chances; i++)
    {
        char type;
        ll stime;
        float prob;
        scanf(" %c %lld %f", &type, &stime, &prob);
        goal_chnc[i].type = type;
        goal_chnc[i].start_time = stime;
        goal_chnc[i].prob = prob;
        chance_arr[i] = goal_chnc[i];
    }
    // initializing mutex, sem, conditional variables
    init_mutex();
    init_sem();
    init_cond();

    // threads for spectators and goals
    pthread_t spec_thread[total_specs], Goals;
    start_time = time(NULL);   // stores current time
    
    // creates threads for spectators
    for (ll i = 0; i < num_groups; i++)
    {
        for (ll j = 0; j < grps[i].size; j++)
        {
            pthread_create(&spec_thread[total_specs], initi, spec_sim, &grps[i].specs[j]);
            total_specs++;
        }
    }
    // creates thread for goals
    pthread_create(&Goals, initi, goal_sim, (void *)chance_arr);
    // joins spectator threads
    for (ll i = 0; i < total_specs; i++)
    {
        pthread_join(spec_thread[i], initi);
    }
    // joins goal thread
    pthread_join(Goals, initi);

    return 0;
}