
//###########FILE CHANGE ./main_folder/Chandan Shrivastava_305787_assignsubmission_file_/q2/2.c ####################//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define ANSI_RESET "\x1b[0m"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

long long int init_time;
int goals_a;
int goals_h;

typedef struct grp_member
{
	char name[50];
	char area;
	int t_reach, t_patience, goal_patience, waiting_or_not, left_or_not, reached_or_not;
	long long int seat_alloc_time;

} grp_member;

typedef struct zone_stat
{
	int zone_cap_n, zone_cap_h, zone_cap_a;
	int zone_cap_n_curr, zone_cap_h_curr, zone_cap_a_curr;

} zone_stat;

typedef struct goal_chance
{
	int chance_no;
	char team[50];
	int time_of_goal;
	float probability;
} goal_chance;

typedef struct tot_stats
{
	int spec_thresh, tot_groups, total_people, tot_chance;

} tot_stats;

grp_member grp_arr[1000];
goal_chance goal_c[1000];
zone_stat zones;
tot_stats total_stats;

void *goalscorer(void *inp)
{
	while (1)
	{
		int curr_time = time(NULL);
		for (int i = 0; i < total_stats.tot_chance; i++)
		{
			if (curr_time - init_time >= goal_c[i].time_of_goal && goal_c[i].chance_no > 0)
			{
				pthread_mutex_lock(&mutex);
				curr_time = time(NULL);
				srand(time(0));
				int num = rand() % 6;
				if (goal_c[i].probability >= 0.75 && num >= 1)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.50 && num >= 2)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability >= 0.25 && num >= 3)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else if (goal_c[i].probability < 0.25 && num >= 4)
				{
					printf(BCYN "%s scored a goal in %d attempt\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
					if (strcmp(goal_c[i].team, "H"))
						goals_h++;
					else if (strcmp(goal_c[i].team, "A"))
						goals_a++;
				}
				else
				{
					printf(BCYN "%s missed their %d chance to score\n" ANSI_RESET, goal_c[i].team, goal_c[i].chance_no);
				}
				goal_c[i].chance_no = 0;
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *bad_perf(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].goal_patience <= goals_h && grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team A\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].goal_patience <= goals_a && grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				printf(BGRN "%s left the stadium because of bad performance of team H\n" ANSI_RESET, grp_arr[i].name);
				grp_arr[i].left_or_not = 1;
				if (grp_arr[i].area == 'N')
				{
					zones.zone_cap_n_curr++;
				}
				else if (grp_arr[i].area == 'A')
				{
					zones.zone_cap_a_curr++;
				}
				else if (grp_arr[i].area == 'H')
				{
					zones.zone_cap_h_curr++;
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *exit_sim(void *inp)
{
	while (1)
	{
		int count = 0;
		for (int i = 0; i < total_stats.total_people; i++)
		{
			if (grp_arr[i].left_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				count++;
				pthread_mutex_unlock(&mutex);
			}
		}
		if (count == total_stats.total_people)
		{
			pthread_mutex_lock(&mutex);
			printf(BBLK"All the people left the stadium\n"ANSI_RESET);
			exit(0);
			pthread_mutex_unlock(&mutex);
		}
	}
}

void *person_timeout(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (grp_arr[i].t_patience < curr_time - init_time + grp_arr[i].t_reach && grp_arr[i].waiting_or_not == 1)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s didnt get a seat after %d second and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
			else if (grp_arr[i].t_patience < curr_time - grp_arr[i].seat_alloc_time && grp_arr[i].waiting_or_not == 0)
			{
				pthread_mutex_lock(&mutex);
				if (grp_arr[i].left_or_not < 1)
				{
					printf(BBLU "%s watched the match for %d seconds and left\n" ANSI_RESET, grp_arr[i].name, grp_arr[i].t_patience);
					grp_arr[i].left_or_not = 1;
					if (grp_arr[i].area == 'N')
					{
						zones.zone_cap_n_curr++;
					}
					else if (grp_arr[i].area == 'A')
					{
						zones.zone_cap_a_curr++;
					}
					else if (grp_arr[i].area == 'H')
					{
						zones.zone_cap_h_curr++;
					}
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *reach_stadium(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 0 && grp_arr[i].left_or_not < 1)
			{
				pthread_mutex_lock(&mutex);
				grp_arr[i].waiting_or_not = 1;
				grp_arr[i].reached_or_not = 1;
				printf(BRED "%s reached the stadium\n" ANSI_RESET, grp_arr[i].name);
				pthread_mutex_unlock(&mutex);
			}
		}
	}
}

void *alloc_zone(void *inp)
{
	while (1)
	{
		for (int i = 0; i < total_stats.total_people; i++)
		{
			int curr_time = time(NULL);
			if (curr_time - init_time >= grp_arr[i].t_reach && grp_arr[i].reached_or_not == 1)
			{
				if (grp_arr[i].waiting_or_not == 1)
				{
					pthread_mutex_lock(&mutex);
					if (grp_arr[i].area == 'N' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'H' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_h_curr > 0)
						{
							zones.zone_cap_h_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in H\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					else if (grp_arr[i].area == 'A' && grp_arr[i].left_or_not < 1)
					{
						if (zones.zone_cap_a_curr > 0)
						{
							zones.zone_cap_a_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in A\n" ANSI_RESET, grp_arr[i].name);
						}
						else if (zones.zone_cap_n_curr > 0)
						{
							zones.zone_cap_n_curr--;
							grp_arr[i].waiting_or_not = 0;
							grp_arr[i].seat_alloc_time = curr_time;
							printf(BMAG "%s has got a seat in Stadium in N\n" ANSI_RESET, grp_arr[i].name);
						}
					}
					pthread_mutex_unlock(&mutex);
				}
			}
		}
	}
}

int main(void)
{

	time_t seconds;
	init_time = time(NULL);

	int tot_people;
	scanf("%d %d %d", &zones.zone_cap_h, &zones.zone_cap_a, &zones.zone_cap_n);
	zones.zone_cap_h_curr = zones.zone_cap_h;
	zones.zone_cap_a_curr = zones.zone_cap_a;
	zones.zone_cap_n_curr = zones.zone_cap_n;
	scanf("%d", &total_stats.spec_thresh);
	scanf("%d", &total_stats.tot_groups);
	for (int i = 0; i < total_stats.tot_groups; i++)
	{
		scanf("%d", &tot_people);
		for (int j = 0; j < tot_people; j++)
		{
			grp_arr[total_stats.total_people].waiting_or_not = -1;
			grp_arr[total_stats.total_people].left_or_not = -1;
			grp_arr[total_stats.total_people].reached_or_not = 0;
			scanf("%s %c %d %d %d", grp_arr[total_stats.total_people].name, &grp_arr[total_stats.total_people].area, &grp_arr[total_stats.total_people].t_reach, &grp_arr[total_stats.total_people].t_patience, &grp_arr[total_stats.total_people].goal_patience);
			total_stats.total_people++;
		}
	}
	scanf("%d", &total_stats.tot_chance);
	for (int i = 0; i < total_stats.tot_chance; i++)
	{
		goal_c[i].chance_no = i + 1;
		scanf("%s %d %f", goal_c[i].team, &goal_c[i].time_of_goal, &goal_c[i].probability);
	}

	// printf("1 %d %d %d\n", zones.zone_cap_h, zones.zone_cap_a, zones.zone_cap_n);
	// printf("2 %d\n", total_stats.spec_thresh);
	// printf("3 %d\n", total_stats.tot_groups);
	// printf("4 %d\n", tot_people);
	// printf("5 %d\n", total_stats.tot_chance);
	// printf("6 %d\n", total_stats.total_people);
	// for (int i = 0; i < total_stats.total_people;i++)
	// {
	// 	printf("7 %s\t %c\t %d\t %d\t %d\t %d\t %d\t %d\n", grp_arr[i].name, grp_arr[i].area, grp_arr[i].t_reach, grp_arr[i].t_patience, grp_arr[i].waiting_or_not,grp_arr[i].goal_patience,grp_arr[i].left_or_not,grp_arr[i].reached_or_not);
	// }
	// for (size_t i = 0; i < total_stats.tot_chance; i++)
	// {
	// 	printf("8 %s %d %f\n", goal_c[i].team, goal_c[i].time_of_goal, goal_c[i].probability);
	// }

	pthread_t tid1;
	pthread_t tid2;
	pthread_t tid3;
	pthread_t tid4;
	pthread_t tid5;
	pthread_t tid6;
	void *a;
	pthread_create(&tid1, NULL, alloc_zone, &a);
	pthread_create(&tid2, NULL, reach_stadium, &a);
	pthread_create(&tid3, NULL, person_timeout, &a);
	pthread_create(&tid4, NULL, exit_sim, &a);
	pthread_create(&tid5, NULL, bad_perf, &a);
	pthread_create(&tid6, NULL, goalscorer, &a);
	pthread_exit(NULL);
}