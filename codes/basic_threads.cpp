
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/wait.h>
//##########################################
#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef long long LL;

void *print_message_function(void *ptr);
LL counter = 0;
/*

Counter experiment succeeds. 
If 10k threads have been spawned, still if main code exits before the last thread exits, then the later threads do not get a chance to print ot the terminal.
Eg: only 9k threads may get a chance to print since main() ezited after their creation but before their termination
*/
const int times = 2;
int main()
{
    queue<pthread_t> q;
    // char *message1 = "Thread 1";
    // char *message2 = "Thread 2";
    int iret1, iret2;

    /* Create independent threads each of which will execute function */
    for (int i = 0; i < times; i++)
    {
        cout << "time is " << i << endl;
        pthread_t thread1;
        iret1 = pthread_create(&thread1, NULL, print_message_function, NULL);
        // q.push(thread1);
        pthread_detach(thread1);
        // iret2 = pthread_create(&thread2, NULL, print_message_function,NULL);
    }

    /* Wait till threads are complete before main continues. Unless we  */
    /* wait we run the risk of executing an exit which will terminate   */
    /* the process and all threads before the threads have completed.   */

    // pthread_join(thread1, NULL);

    cout << "Finally exiting" << endl;
    ;
    exit(0);
}

int flag=4;
//0 -> free
//1 -> someone else is there
void *print_message_function(void *ptr)
{
    //////////////////
    while (flag >0)
    {
        //
    }
    //counter++;
    sem_wait(flag);
    {
    }
    sem_signal(flag);

    //////////////////////
    // sleep(10);
    cout << "Counter is " << counter << endl;
}


//| 1 2 3 4  | 5 6 7 8 |